//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$PE_PARTICLEEDITOR_DEFAULT_FILENAME = "art/gfx/particles/managedParticleData.cs";

//=============================================================================================
//    PE_ParticleEditor.
//=============================================================================================

//---------------------------------------------------------------------------------------------

function PE_ParticleEditor::doSystemSave( %this )
{
	%this.doEmitterSave();
	%this.doParticleSave();
}

function PE_ParticleEditor::refreshParticleList( %this )
{

	PE_ParticleEditor.guiSync(true);
}
