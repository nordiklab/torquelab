//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function ParticleEditor::create( %this )
{
 info("Plugin","->","Initializing","Particle Editor" );
	execParticleEd(true);
	//Lab.createPlugin("ParticleEditor","Particle Editor");
	Lab.addPluginGui("ParticleEditor",ParticleEditorTools);
	Lab.addPluginToolbar("ParticleEditor",ParticleEditorToolbar);
	ParticleEditorPlugin.superClass = "WEditorPlugin";
	ParticleEditorPlugin.customPalette = "SceneEditorPalette";
	ParticleEditorPlugin.sharedPalette = "SceneEditor";
	%map = Lab.addPluginMap("ParticleEditor");
	%map.bindCmd( keyboard, "1", "EWorldEditorNoneModeBtn.performClick();", "" );  // Select
	%map.bindCmd( keyboard, "2", "EWorldEditorMoveModeBtn.performClick();", "" );  // Move
	%map.bindCmd( keyboard, "3", "EWorldEditorRotateModeBtn.performClick();", "" );  // Rotate
	%map.bindCmd( keyboard, "4", "EWorldEditorScaleModeBtn.performClick();", "" );  // Scale
	ParticleEditorPlugin.map = %map;
	//new ScriptObject( ParticleEditor );

	new PersistenceManager( PE_ParticleEditor_NotDirtyParticle );
	new PersistenceManager( PE_EmitterSaver );
	new PersistenceManager( PE_ParticleSaver );
	new SimSet( PE_UnlistedParticles );
	new SimSet( PE_UnlistedEmitters );
	new SimSet( PE_AllParticles );
}

//------------------------------------------------------------------------------

function ParticleEditor::destroy( %this )
{

}

//----------------------------------------------------------------
// Initialization and shutdown code for particle editor plugin.

$TLab_PluginName_["ParticleEditor"] = "Particle Editor";
//---------------------------------------------------------------------------------------------
//==============================================================================

//------------------------------------------------------------------------------
//==============================================================================
function execParticleEd(%loadgui)
{
	if (%loadgui)
	{
		exec( "./gui/ParticleEditorTools.gui" );
		exec( "tlab/plugins/particleEditor/gui/ParticleEditorToolbar.gui" );
	}

	exec( "tlab/plugins/particleEditor/ParticleEditorPlugin.cs" );
	exec( "tlab/plugins/particleEditor/ParticleEditorParams.cs" );
	execPattern( "tlab/plugins/particleEditor/scripts/*.cs" );
	execPattern( "tlab/plugins/particleEditor/particle/*.cs" );
	execPattern( "tlab/plugins/particleEditor/emitter/*.cs" );

}
//------------------------------------------------------------------------------
//==============================================================================
function destroyParticleEditor()
{
}
//------------------------------------------------------------------------------
