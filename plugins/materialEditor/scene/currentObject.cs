//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function MaterialEditorTools::prepareActiveObject( %this, %override )
{
	%obj = $Lab::materialEditorList;
	$MEP_BaseObjectPath = "";
	$MEP_BaseObject = "";

	if (isObject(MaterialEditorTools.currentObject))
	{
		%obj = MaterialEditorTools.currentObject;
		$MEP_BaseObject = MaterialEditorTools.currentObject;
		$MEP_BaseObjectPath  = MaterialEditorTools.currentObject.shapeName;
	}

	if ( MaterialEditorTools.currentObject == %obj && !%override)
		return;

	// TSStatics and ShapeBase objects should have getModelFile methods
	if ( %obj.isMethod( "getModelFile" ) )
	{
		MaterialEditorTools.currentObject = %obj;
		SubMaterialSelector.clear();
		MaterialEditorTools.currentMeshMode = "Model";
		MaterialEditorTools.setMode();

		for(%j = 0; %j < MaterialEditorTools.currentObject.getTargetCount(); %j++)
		{
			%target = MaterialEditorTools.currentObject.getTargetName(%j);
			%count = SubMaterialSelector.getCount();

			if (strFind(%target,"ColorEffectR") && $MatEd_HideColorMaterial)
				continue;

			SubMaterialSelector.add(%target);
		}
	}
	else     // Other classes that support materials if possible
	{
		%canSupportMaterial = false;

		for( %i = 0; %i < %obj.getFieldCount(); %i++ )
		{
			%fieldName = %obj.getField(%i);

			if ( %obj.getFieldType(%fieldName) !$= "TypeMaterialName" )
				continue;

			if ( !%canSupportMaterial )
			{
				MaterialEditorTools.currentObject = %obj;
				SubMaterialSelector.clear();
				SubMaterialSelector.add(%fieldName, 0);
			}
			else
			{
				%count = SubMaterialSelector.getCount();
				SubMaterialSelector.add(%fieldName, %count);
			}

			%canSupportMaterial = true;
		}

		if ( !%canSupportMaterial ) // Non-relevant classes get returned
			return;

		MaterialEditorTools.currentMeshMode = "EditorShape";
		MaterialEditorTools.setMode();
	}

	%id = SubMaterialSelector.findText( MaterialEditorTools.currentMaterial.mapTo );

	if ( %id != -1 )
		SubMaterialSelector.setSelected( %id );
	else
		SubMaterialSelector.setSelected(0);
}
//------------------------------------------------------------------------------

//==============================================================================
// Clear and Refresh Material
function MatEd::setActiveObject(%this,%obj)
{

	//%obj = $Lab::materialEditorList;
	$MEP_BaseObjectPath = "";
	$MEP_BaseObject = "";

	if (isObject(MaterialEditorTools.currentObject))
	{
		$MEP_BaseObject = MaterialEditorTools.currentObject;
		$MEP_BaseObjectPath  = MaterialEditorTools.currentObject.shapeName;
	}

	if ( MaterialEditorTools.currentObject == %obj)
		return;

	// TSStatics and ShapeBase objects should have getModelFile methods
	if ( %obj.isMethod( "getModelFile" ) )
	{
		MaterialEditorTools.currentObject = %obj;
		SubMaterialSelector.clear();
		MaterialEditorTools.currentMeshMode = "Model";

		for(%j = 0; %j < MaterialEditorTools.currentObject.getTargetCount(); %j++)
		{
			%target = MaterialEditorTools.currentObject.getTargetName(%j);
			%count = SubMaterialSelector.getCount();

			if (strFind(%target,"ColorEffectR") && $MatEd_HideColorMaterial)
				continue;

			SubMaterialSelector.add(%target);
		}

		%sourcePath = MaterialEditorTools.currentObject.getModelFile();

		if ( %sourcePath !$= "" )
		{
			MatEdTargetMode-->selMaterialShape.ToolTip = %sourcePath;
			%sourceName = fileName(%sourcePath);
			MatEdTargetMode-->selMaterialShape.setText(%sourceName);
			MatEd_ActiveProperties-->selMaterialName.setText(MaterialEditorTools.currentMaterial.name);
		}
	}
	else     // Other classes that support materials if possible
	{
		%canSupportMaterial = false;

		//Search through all fields to find one of type TypeMaterialName
		for( %i = 0; %i < %obj.getFieldCount(); %i++ )
		{
			%fieldName = %obj.getField(%i);

			if ( %obj.getFieldType(%fieldName) !$= "TypeMaterialName" )
				continue;

			if ( !%canSupportMaterial )
			{
				MaterialEditorTools.currentObject = %obj;
				SubMaterialSelector.clear();
				SubMaterialSelector.add(%fieldName, 0);
			}
			else
			{
				%count = SubMaterialSelector.getCount();
				SubMaterialSelector.add(%fieldName, %count);
			}

			%canSupportMaterial = true;
		}

		if ( !%canSupportMaterial ) // Non-relevant classes get returned
			return;

		MaterialEditorTools.currentMeshMode = "EditorShape";
		MaterialEditorTools.setMode();
		%info = MaterialEditorTools.currentObject.getClassName();
		MatEdTargetMode-->selMaterialShape.ToolTip = %info;
		MatEdTargetMode-->selMaterialShape.setText(%info);
		MatEd_ActiveProperties-->selMaterialName.setText(MaterialEditorTools.currentMaterial.name);

	}

	//Always select the first material when object change
	SubMaterialSelector.setSelected(0);
	return;

	%id = SubMaterialSelector.findText( MaterialEditorTools.currentMaterial.mapTo );

	if ( %id != -1 )
		SubMaterialSelector.setSelected( %id );
	else
		SubMaterialSelector.setSelected(0);

}
//------------------------------------------------------------------------------
