//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function MaterialEditor::create( %this )
{ 
  info("Plugin","->","Initializing","Material Editor");
	execMEP(true);
	//exec("./gui/profiles.cs");
	$MatEd = newScriptObject("MatEd");
	//Lab.createPlugin("MaterialEditor","Material Editor");
	MaterialEditorPlugin.superClass = "WEditorPlugin";
	MaterialEditorPlugin.sharedPalette = "SceneEditor";
	Lab.addPluginGui("MaterialEditor",MaterialEditorTools); //Tools renamed to Gui to store stuff
	Lab.addPluginDlg("MaterialEditor",matEd_cubemapEditor);
	Lab.addPluginDlg("MaterialEditor",matEd_addCubemapWindow);
	Lab.addPluginDlg("MaterialEditor",MaterialEditorDialogs);
	Lab.addPluginToolbar("MaterialEditor",MaterialEditorToolbar);
	new PersistenceManager(matEd_PersistMan);
}

//------------------------------------------------------------------------------

function MaterialEditor::destroy( %this )
{
   
}

//----------------------------------------------------------------
$TLab_PluginName_MaterialEditor = "Material Editor";
$MEP_NoTextureImage = "tlab/plugins/materialEditor/assets/unavailable";
$MaterialEditor_AutoUpdateMap = true;
$MaterialEditor_PBRMode = true;

//------------------------------------------------------------------------------
//==============================================================================
function execMEP(%loadGui)
{
	if (%loadGui)
	{
		// Load MaterialEditor Guis
		exec("tlab/plugins/materialEditor/gui/matEd_cubemapEditor.gui");
		exec("tlab/plugins/materialEditor/gui/matEd_addCubemapWindow.gui");
		exec("tlab/plugins/materialEditor/gui/matEdNonModalGroup.gui");
		exec("tlab/plugins/materialEditor/gui/MaterialEditorToolbar.gui");
		exec("tlab/plugins/materialEditor/gui/MaterialEditorTools.gui");
		exec("tlab/plugins/materialEditor/gui/MaterialEditorDialogs.gui");

	}

	// Load Client Scripts.
	exec("./MaterialEditorPlugin.cs");
	execPattern("tlab/plugins/materialEditor/scripts/*.cs");
	execPattern("tlab/plugins/materialEditor/scene/*.cs");
	execPattern("tlab/plugins/materialEditor/material/*.cs");
	execPattern("tlab/plugins/materialEditor/guiScripts/*.cs");
}
//==============================================================================
function destroyMaterialEditor()
{
}
//------------------------------------------------------------------------------
function matEdLog(%text,%a1, %a2, %a3, %a4,%a5, %a6, %a7, %a8,%a9, %a10, %a11)
{
}
