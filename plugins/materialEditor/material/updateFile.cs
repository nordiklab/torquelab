//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function MaterialEditorTools::selectMatFile( %this )
{
	%file = getFile( "*.cs|All Files (*.*)|*.*|",MaterialEditorTools.currentMaterial.getFileName());

	if (!isFile(%file))
	{
		warnLog("Invalid file selected:",%file);
		return;
	}

	if (MaterialEditorTools.currentMaterial.getFileName() $= %file)
	{
		warnLog("File is the same as current:",%file,"Current:",MaterialEditorTools.currentMaterial.getFileName());
		return;
	}

	MaterialEditorTools.currentMaterial.setFilename(%file);
	MaterialEditorTools.setMaterialDirty();
	%this.setActiveMaterialFile(MaterialEditorTools.currentMaterial);
}
//------------------------------------------------------------------------------

$Cfg_MaterialEditor_RoughSuffix = $Cfg_MaterialEditor_SmoothnessSuffix;
//==============================================================================
// AutoUpdate Check - Check for files matching default map ending and load the
// file to the texture map if not already set (Unless force AutoUpdate set to true)
function MaterialEditorTools::CheckAutoUpdateMap( %this,%type, %texture,%layer)
{
	if (!$MaterialEditor_AutoUpdateMap)
		return;

	%curMat = MaterialEditorTools.currentMaterial;
	%curLayer = MaterialEditorTools.currentLayer;
	%fileName = fileBase(%texture);
	eval("%removeSuffix = $Cfg_MaterialEditor_"@%type@"Suffix;");
	%fileName = strReplace(%fileName,%removeSuffix,"");
	%ext = fileExt(%texture);
	%folder = filePath(%texture)@"/";
	%pathBase = strreplace(%folder@%fileName,"//","/");
	matLog("CheckAutoUpdateMap Type",%type,"Text",%texture,"PathBase:",%pathBase);

	if ($Cfg_MaterialEditor_AutoAddNormal && %type !$="Normal")
	{
		%map = $MEP_MapType["Normal"];
		%currentMap = %curMat.getFieldValue(%map,%curLayer);

		if (isFile(%currentMap))
		{
			info("Normal auto-update skipped because it's already set to:",%currentMap);
		}
		else
		{
			%suffix = $Cfg_MaterialEditor_NormalSuffix;
			%testFile = %pathBase@%suffix@%ext ;

			if (isImageFile(%testFile) && !$AutoUpdateSkip)
				%this.updateTextureMap("Normal",%testFile);

		}
	}

	if (SceneEditorCfg.AutoAddDiffuse && %type !$="Diffuse")
	{
		%map = $MEP_MapType["Diffuse"];
		%currentMap = %curMat.getFieldValue(%map,%curLayer);

		if (isFile(%currentMap))
		{
			info("Diffuse auto-update skipped because it's already set to:",%currentMap);
		}
		else
		{
			%suffix = $Cfg_MaterialEditor_DiffuseSuffix;
			%testFile = %pathBase@%suffix@%ext;

			if (isImageFile(%testFile) && !$AutoUpdateSkip)
				%this.updateTextureMap("diffuse",%testFile);

		}
	}

	if ($Cfg_MaterialEditor_AutoAddSpecular && %type !$="Specular" && !$MaterialEditor_PBRMode)
	{
		%suffix = $Cfg_MaterialEditor_SpecularSuffix;
		%testFile = %pathBase@%suffix@%ext;
		matLog("Specular Check:",%testFile);

		if (isImageFile(%testFile))
			%this.updateTextureMap("specular",%testFile);

	}

	if ($MaterialEditor_PBRMode && %type !$="Composite")
	{
		if ($Cfg_MaterialEditor_AutoAddAO && %type !$="AO")
		{
			%map = $MEP_MapType["AO"];
			%currentMap = %curMat.getFieldValue(%map,%curLayer);

			if (isFile(%currentMap))
			{
				info("AO auto-update skipped because it's already set to:",%currentMap);
			}
			else
			{
				%suffix = $Cfg_MaterialEditor_AOSuffix;
				%testFile = %pathBase@%suffix@%ext;
				matLog("AO Check:",%testFile);

				if (isImageFile(%testFile))
					%this.updateTextureMap("ao",%testFile);

			}

		}

		if ($Cfg_MaterialEditor_AutoAddSmoothness && %type !$="Smoothness")
		{
			%map = $MEP_MapType["Smoothness"];
			%currentMap = %curMat.getFieldValue(%map,%curLayer);

			if (isFile(%currentMap))
			{
				info("Smoothness auto-update skipped because it's already set to:",%currentMap);
			}
			else
			{
				%suffix = $Cfg_MaterialEditor_SmoothnessSuffix;
				%testFile = %pathBase@%suffix@%ext;

				if (isImageFile(%testFile))
					%this.updateTextureMap("rough",%testFile);

			}
		}

		if ($Cfg_MaterialEditor_AutoAddMetalness && %type !$="Metalness")
		{
			%map = $MEP_MapType["Metalness"];
			%currentMap = %curMat.getFieldValue(%map,%curLayer);

			if (isFile(%currentMap))
			{
				info("Metalness auto-update skipped because it's already set to:",%currentMap);

			}
			else
			{
				%suffix = $Cfg_MaterialEditor_MetalnessSuffix;
				%testFile = %folder @%fileName@%suffix@%ext;

				if (isImageFile(%testFile))
					%this.updateTextureMap("metal",%testFile);
			}
		}

		if ($Cfg_MaterialEditor_AutoAddComposite)
		{
			%suffix = $Cfg_MaterialEditor_CompositeSuffix;
			%map = $MEP_MapType["Composite"];
			%currentMap = %curMat.getFieldValue(%map,%curLayer);
			%noCompCheck = false;

			//ONLY Update composite if no pbr single map added
			foreach$(%pbrType in "aoMap roughMap metalMap")
			{
				%pbrMap = %curMat.getFieldValue(%pbrType,%curLayer);

				if (isImageFile(%pbrMap))
				{
					%noCompCheck = true;
					break;
				}

			}

			if (isFile(%currentMap))
			{
				info("Composite auto-update skipped because it's already set to:",%currentMap);
			}
			else
			{
				%testFile = %folder @%fileName@%suffix@%ext;

				if (isImageFile(%testFile))
					%this.updateTextureMap("Composite",%testFile);
			}
		}
	}
}
//------------------------------------------------------------------------------

