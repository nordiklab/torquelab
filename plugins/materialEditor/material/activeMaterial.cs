//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$MatEdDblUpdate = false;

//==============================================================================
// Helper functions to help load and update the preview and active material
//------------------------------------------------------------------------------
//==============================================================================
// Finds the selected line in the material list, then makes it active in the editor.
function MaterialEditorTools::prepareActiveMaterial(%this, %material, %override)
{
	// If were not valid, grab the first valid material out of the materialSet
	if ( !isObject(%material) )
	{
		%material = MaterialSet.getObject(0);
		%isNew = true;
	}

	// Check made in order to avoid loading the same material. Overriding
	// made in special cases
	if (%material $= MaterialEditorTools.lastMaterial && !%override)
	{
		info("What if 2 model use the same material?");
		//return;
	}
	else
	{
		if (MaterialEditorTools.materialDirty )
		{
			MaterialEditorTools.showSaveDialog( %material );
			return;
		}

		MaterialEditorTools.setActiveMaterial(%material,%isNew);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Updates the preview material to use the same properties as the selected material,
// and makes that material active in the editor.
//MaterialEditorTools.setActiveMaterial(test_cuboidTestF);
function MaterialEditorTools::setActiveMaterial( %this, %material,%isNew )
{
	// Warn if selecting a CustomMaterial (they can't be properly previewed or edited)
	if ( isObject( %material ) && %material.isMemberOfClass( "CustomMaterial" ) )
	{
		LabMsgOK( "Warning", "The selected Material (" @ %material.getName() @
		          ") is a CustomMaterial, and cannot be edited using the Material Editor." );
		return;
	}

	if (strFind(%material.getFilename(),"tlab/plugins/materialEditor") || %material.getFilename() $= "")
	{
		%material.setFilename($Pref::MaterialEditorTools::DefaultMaterialFile);
	}

	MaterialEditorTools.currentMaterial = %material;
	MaterialEditorTools.lastMaterial = %material;
	%this.setActiveMaterialFile(%material);
	// we create or recreate a material to hold in a pristine state
	singleton Material(notDirtyMaterial)
	{
		mapTo = "matEd_previewMat";
		diffuseMap[0] = "tlab/plugins/materialEditor/assets/matEd_mappedMat";
	};
	// Converts the texture files into absolute paths.
	MatLab.convertTextureFields(MaterialEditorTools.currentMaterial);
	//MaterialEditorTools.convertTextureFields(MaterialEditorTools.currentMaterial);
	// If we're allowing for name changes, make sure to save the name seperately
	%this.originalName = MaterialEditorTools.currentMaterial.name;
	// Copy materials over to other references
	MaterialEditorTools.copyMaterials( MaterialEditorTools.currentMaterial, materialEd_previewMaterial );
	MaterialEditorTools.copyMaterials( MaterialEditorTools.currentMaterial, notDirtyMaterial );
	MaterialEditorTools.guiSync( materialEd_previewMaterial );
	// Necessary functionality in order to render correctly
	materialEd_previewMaterial.flush();
	materialEd_previewMaterial.reload();
	MaterialEditorTools.currentMaterial.flush();
	MaterialEditorTools.currentMaterial.reload();
	MaterialEditorTools.setMaterialNotDirty();

	if (%isNew)
		MaterialEditorTools.currentMaterial.metalness[0] = "0";
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::setActiveMaterialFile( %this, %material )
{
	MatEd_ActiveProperties-->selMaterialFile.setText(%material.getFilename());
	MatEd_ActiveProperties-->selMaterialFile.setText(%material.getFilename());
}
//==============================================================================
function MaterialEditorTools::updateActiveMaterial(%this, %propertyField, %value, %isSlider, %onMouseUp)
{
	MaterialEditorTools.setMaterialDirty();

	if (%value $= "")
		%value = "\"\"";

	// Here is where we handle undo actions with slider controls. We want to be able to
	// undo every onMouseUp; so we overrite the same undo action when necessary in order
	// to achieve this desired effect.
	%last = Editor.getUndoManager().getUndoAction(Editor.getUndoManager().getUndoCount() - 1);

	if ((%last != -1) && (%last.isSlider) && (!%last.onMouseUp))
	{
		%last.field = %propertyField;
		%last.isSlider = %isSlider;
		%last.onMouseUp = %onMouseUp;
		%last.newValue = %value;
	}
	else
	{
		%action = %this.createUndo(ActionUpdateActiveMaterial, "Update Active Material");
		%action.material = MaterialEditorTools.currentMaterial;
		%action.object = MaterialEditorTools.currentObject;
		%action.field = %propertyField;
		%action.isSlider = %isSlider;
		%action.onMouseUp = %onMouseUp;
		%action.newValue = %value;
		eval( "%action.oldValue = " @ MaterialEditorTools.currentMaterial @ "." @ %propertyField @ ";");
		%action.oldValue = "\"" @ %action.oldValue @ "\"";
		MaterialEditorTools.submitUndo( %action );
	}

	if (!isObject(materialEd_previewMaterial))
		MaterialEditorTools.establishMaterials();

	eval("materialEd_previewMaterial." @ %propertyField @ " = " @ %value @ ";");
	materialEd_previewMaterial.flush();
	materialEd_previewMaterial.reload();

	if (MaterialEditorTools.livePreview == true)
	{
		eval("MaterialEditorTools.currentMaterial." @ %propertyField @ " = " @ %value @ ";");
		MaterialEditorTools.currentMaterial.flush();
		MaterialEditorTools.currentMaterial.reload();
	}

}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateActiveMaterialName(%this, %name)
{
	%action = %this.createUndo(ActionUpdateActiveMaterialName, "Update Active Material Name");
	%action.material =  MaterialEditorTools.currentMaterial;
	%action.object = MaterialEditorTools.currentObject;
	%action.oldName = MaterialEditorTools.currentMaterial.getName();
	%action.newName = %name;
	MaterialEditorTools.submitUndo( %action );
	MaterialEditorTools.currentMaterial.setName(%name);
	// Some objects (ConvexShape, DecalRoad etc) reference Materials by name => need
	// to find and update all these references so they don't break when we rename the
	// Material.
	MaterialEditorTools.updateMaterialReferences( MissionGroup, %action.oldName, %action.newName );
}

//==============================================================================
// SubMaterial(Material Target) -- Supports different ways to grab the
// material from the dropdown list. We're here either because-
// 1. We have switched over from another editor with an object locked in the
//    $Lab::materialEditorList variable
// 2. We have selected an object using the Object Editor via the Material Editor
//==============================================================================
function SubMaterialSelector::onSelect( %this )
{
	%material = "";

	if ( MaterialEditorTools.currentMeshMode $= "Model" )
		%material = getMapEntry( %this.getText() );
	else
		%material = MaterialEditorTools.currentObject.getFieldValue( %this.getText() );

	%origMat = %material;

	if (%material$="")
		%origMat = %material = %this.getText();

	// if there is no material attached to that objects material field or the
	// object does not have a valid method to grab a material
	if ( !isObject( %material ) )
	{
		// look for a newMaterial name to grab
		// addiitonally, convert "." to "_" in case we have something like: "base.texname" as a material name
		// at the end we will have generated material name: "base_texname_mat"
		%material = getUniqueName( strreplace(%material, ".", "_") @ "_mat" );
		new Material(%material)
		{
			diffuseMap[0] = %origMat;
			mapTo = %origMat;
			parentGroup = RootGroup;
		};
		%material.setFileName(MatEd_ActiveProperties-->selMaterialFile.getText());
		eval( "MaterialEditorTools.currentObject." @ strreplace(%this.getText(),".","_") @ " = " @ %material @ ";");

		if ( MaterialEditorTools.currentObject.isMethod("postApply") )
			MaterialEditorTools.currentObject.postApply();
	}

	MaterialEditorTools.prepareActiveMaterial( %material.getId() );
}
//------------------------------------------------------------------------------

//==============================================================================
function MaterialEditorTools::changeLayer( %this, %layer )
{
	if ( MaterialEditorTools.currentLayer == getWord(%layer, 1) )
		return;

	MaterialEditorTools.currentLayer = getWord(%layer, 1);
	MaterialEditorTools.guiSync( materialEd_previewMaterial );
}
//------------------------------------------------------------------------------
