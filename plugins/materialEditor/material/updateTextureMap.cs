//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$MEP_MapType["Normal"] = "normalMap";
$MEP_MapType["Diffuse"] = "diffuseMap";
$MEP_MapType["Specular"] = "specularMap";
$MEP_MapType["Smoothness"] = "roughMap";
$MEP_MapType["Metalness"] = "metalMap";
$MEP_MapType["AO"] = "aoMap";
$MEP_MapType["Composite"] = "specularMap";

//==============================================================================
// Generic map type texture update
function MaterialEditorTools::selectTextureMap( %this, %type )
{
	%defaultFile = %this.getTextureMap(%type);

	if (!isFile(%defaultFile))
		%defaultPath = filePath(MaterialEditorTools.getTextureMap("diffuse"));

	%texture = MatLab.selectMapFile(MaterialEditorTools.currentMaterial,%type,MaterialEditorTools.currentLayer);

	if ( isImageFile( %texture) )
	{
		if ( %this.updateTextureMap(%type,%texture))
			%this.CheckAutoUpdateMap(%type,%texture,MaterialEditorTools.currentLayer);
	}
}
//==============================================================================
// Generic map type texture update
function MaterialEditorTools::updateTextureMap( %this, %type, %texture )
{
	%texture = strreplace(%texture,"//","/");

	if (!%this.isValidTexture(%texture))
	{
		warnLog(%type,"Update map source aborted due to invalid texture",%texture);
		return false;
	}

	%bitmapCtrl = MaterialEditorPropertiesWindow.findObjectByInternalName( %type @ "MapDisplayBitmap", true );
	%textCtrl = MaterialEditorPropertiesWindow.findObjectByInternalName( %type @ "MapNameText", true );
	%shortTextCtrl = MaterialEditorPropertiesWindow.findObjectByInternalName( %type @ "FileNameText", true );

	if (isImageFile(%texture) && isObject(%bitmapCtrl))
		%bitmapCtrl.setBitmap(%texture);

	if (isObject(%textCtrl))
		%textCtrl.setText(%texture);

	if (isObject(%shortTextCtrl))
		%shortTextCtrl.setText(fileName(%texture));

	%updMap = %type @ "Map";

	if ($MEP_MapType[%type] !$= "")
		%updMap = $MEP_MapType[%type];

	%layer = MaterialEditorTools.currentLayer;
	MaterialEditorTools.updateActiveMaterial(%updMap @ "[" @ %layer @ "]","\"" @ %texture @ "\"");
	return true;
}
//------------------------------------------------------------------------------
//==============================================================================
// Generic map type texture update
function MaterialEditorTools::clearTextureMap( %this, %type )
{
	%bitmapCtrl = MaterialEditorPropertiesWindow.findObjectByInternalName( %type @ "MapDisplayBitmap", true );
	%textCtrl = MaterialEditorPropertiesWindow.findObjectByInternalName( %type @ "MapNameText", true );

	if (isObject(%bitmapCtrl))
		%bitmapCtrl.setBitmap($MEP_NoTextureImage);

	if (isObject(%textCtrl))
		%textCtrl.setText("None");

	%updMap = %type @ "Map";

	if ($MEP_MapType[%type] !$= "")
		%updMap = $MEP_MapType[%type];

	%layer = MaterialEditorTools.currentLayer;
	MaterialEditorTools.updateActiveMaterial(%updMap @ "[" @ %layer @ "]","");
}
//------------------------------------------------------------------------------
//==============================================================================
// MaterialEditorTools.isValidTexture("art/models/buildings/XBase/textures/osWall01/osWall01_met.png");
//getBitmapInfo("art/models/buildings/XBase/textures/osWall01/osWall01_met.png");
function MaterialEditorTools::isValidTexture( %this, %texture)
{
	if (!isImageFile(%texture))
		return false;

	//If DDS, we can't validate it's format...
	if (fileExt(%texture) $= ".dds")
		return true;

	if (!isFunction("getBitmapFormat"))
	{
		%info = getBitmapInfo(%texture);

		if ( getField(%info,2) < 3)
		{
			warnLog("Invalid Non-RGB texture (byte/pixels):",getField(%info,2),"For texture:",%texture);
			return false;
		}

		return true;
	}

	%format = getBitmapFormat(%texture);

	if (%format < 10)
	{
		warnLog("Invalid texture format:",%format,"For texture:",%texture);
		return false;
	}

	return true;
}
//==============================================================================
// MaterialEditorTools.getTextureMap("diffuse")
function MaterialEditorTools::getTextureMap( %this, %type,%defaultIfEmpty )
{
	%updMap = %type @ "Map";

	if ($MEP_MapType[%type] !$= "")
		%updMap = $MEP_MapType[%type];

	%texture = MaterialEditorTools.currentMaterial.getFieldValue(%updMap,MaterialEditorTools.currentLayer);

	if (isImageFile(%texture))
		return %texture;

	if (%defaultIfEmpty)
		return $MEP_NoTextureImage;

	return "";
}
//------------------------------------------------------------------------------
