//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//------------------------------------------------------------------------------
// Accumulation
function MaterialEditorTools::updateAccuCheckbox(%this, %value)
{
	MaterialEditorTools.updateActiveMaterial("accuEnabled[" @ MaterialEditorTools.currentLayer @ "]", %value);
	%mat = MaterialEditorTools.currentMaterial;
	MaterialEditorTools.guiSync( materialEd_previewMaterial );
}

//==============================================================================
// Special Map variable updates
//==============================================================================
//==============================================================================
function MaterialEditorTools::updateDetailScale(%this,%newScale)
{
	%layer = MaterialEditorTools.currentLayer;
	%detailScale = "\"" @ %newScale SPC %newScale @ "\"";
	MaterialEditorTools.updateActiveMaterial("detailScale[" @ %layer @ "]", %detailScale);
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateDetailNormalStrength(%this,%newStrength)
{
	%layer = MaterialEditorTools.currentLayer;
	%detailStrength = "\"" @ %newStrength @ "\"";
	MaterialEditorTools.updateActiveMaterial("detailNormalMapStrength[" @ %layer @ "]", %detailStrength);
}
//------------------------------------------------------------------------------

//PBR Script End
//==============================================================================
function MaterialEditorTools::updateRotationOffset(%this, %isSlider, %onMouseUp)
{
	%layer = MaterialEditorTools.currentLayer;
	%X = MaterialEditorPropertiesWindow-->RotationTextEditU.getText();
	%Y = MaterialEditorPropertiesWindow-->RotationTextEditV.getText();
	MaterialEditorPropertiesWindow-->RotationCrosshair.setPosition(45*mAbs(%X)-2, 45*mAbs(%Y)-2);
	MaterialEditorTools.updateActiveMaterial("rotPivotOffset[" @ %layer @ "]","\"" @ %X SPC %Y @ "\"",%isSlider,%onMouseUp);
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateRotationSpeed(%this, %isSlider, %onMouseUp)
{
	%layer = MaterialEditorTools.currentLayer;
	%speed = MaterialEditorPropertiesWindow-->RotationSpeedTextEdit.getText();
	MaterialEditorTools.updateActiveMaterial("rotSpeed[" @ %layer @ "]",%speed,%isSlider,%onMouseUp);
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateScrollOffset(%this, %isSlider, %onMouseUp)
{
	%layer = MaterialEditorTools.currentLayer;
	%X = MaterialEditorPropertiesWindow-->ScrollTextEditU.getText();
	%Y = MaterialEditorPropertiesWindow-->ScrollTextEditV.getText();
	MaterialEditorPropertiesWindow-->ScrollCrosshair.setPosition( -(23 * %X)+20, -(23 * %Y)+20);
	MaterialEditorTools.updateActiveMaterial("scrollDir[" @ %layer @ "]","\"" @ %X SPC %Y @ "\"",%isSlider,%onMouseUp);
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateScrollSpeed(%this, %isSlider, %onMouseUp)
{
	%layer = MaterialEditorTools.currentLayer;
	%speed = MaterialEditorPropertiesWindow-->ScrollSpeedTextEdit.getText();
	MaterialEditorTools.updateActiveMaterial("scrollSpeed[" @ %layer @ "]",%speed,%isSlider,%onMouseUp);
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateWaveType(%this)
{
	for( %radioButton = 0; %radioButton < MaterialEditorPropertiesWindow-->WaveButtonContainer.getCount(); %radioButton++ )
	{
		if ( MaterialEditorPropertiesWindow-->WaveButtonContainer.getObject(%radioButton).getValue() == 1 )
			%type = MaterialEditorPropertiesWindow-->WaveButtonContainer.getObject(%radioButton).waveType;
	}

	%layer = MaterialEditorTools.currentLayer;
	MaterialEditorTools.updateActiveMaterial("waveType[" @ %layer @ "]", %type);
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateWaveAmp(%this, %isSlider, %onMouseUp)
{
	%layer = MaterialEditorTools.currentLayer;
	%amp = MaterialEditorPropertiesWindow-->WaveTextEditAmp.getText();
	MaterialEditorTools.updateActiveMaterial("waveAmp[" @ %layer @ "]", %amp, %isSlider, %onMouseUp);
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateWaveFreq(%this, %isSlider, %onMouseUp)
{
	%layer = MaterialEditorTools.currentLayer;
	%freq = MaterialEditorPropertiesWindow-->WaveTextEditFreq.getText();
	MaterialEditorTools.updateActiveMaterial("waveFreq[" @ %layer @ "]", %freq, %isSlider, %onMouseUp);
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateSequenceFPS(%this, %isSlider, %onMouseUp)
{
	%layer = MaterialEditorTools.currentLayer;
	%fps = MaterialEditorPropertiesWindow-->SequenceTextEditFPS.getText();
	MaterialEditorTools.updateActiveMaterial("sequenceFramePerSec[" @ %layer @ "]", %fps, %isSlider, %onMouseUp);
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateSequenceSSS(%this, %isSlider, %onMouseUp)
{
	%layer = MaterialEditorTools.currentLayer;
	%sss = 1 / MaterialEditorPropertiesWindow-->SequenceTextEditSSS.getText();
	MaterialEditorTools.updateActiveMaterial("sequenceSegmentSize[" @ %layer @ "]", %sss, %isSlider, %onMouseUp);
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateAnimationFlags(%this)
{
	MaterialEditorTools.setMaterialDirty();
	%single = true;

	if (MaterialEditorPropertiesWindow-->RotationAnimation.getValue() == true)
	{
		if (%single == true)
			%flags = %flags @ "$Rotate";
		else
			%flags = %flags @ " | $Rotate";

		%single = false;
	}

	if (MaterialEditorPropertiesWindow-->ScrollAnimation.getValue() == true)
	{
		if (%single == true)
			%flags = %flags @ "$Scroll";
		else
			%flags = %flags @ " | $Scroll";

		%single = false;
	}

	if (MaterialEditorPropertiesWindow-->WaveAnimation.getValue() == true)
	{
		if (%single == true)
			%flags = %flags @ "$Wave";
		else
			%flags = %flags @ " | $Wave";

		%single = false;
	}

	if (MaterialEditorPropertiesWindow-->ScaleAnimation.getValue() == true)
	{
		if (%single == true)
			%flags = %flags @ "$Scale";
		else
			%flags = %flags @ " | $Scale";

		%single = false;
	}

	if (MaterialEditorPropertiesWindow-->SequenceAnimation.getValue() == true)
	{
		if (%single == true)
			%flags = %flags @ "$Sequence";
		else
			%flags = %flags @ " | $Sequence";

		%single = false;
	}

	if (%flags $= "")
		%flags = "\"\"";

	%action = %this.createUndo(ActionUpdateActiveMaterialAnimationFlags, "Update Active Material");
	%action.material = MaterialEditorTools.currentMaterial;
	%action.object = MaterialEditorTools.currentObject;
	%action.layer = MaterialEditorTools.currentLayer;
	%action.newValue = %flags;
	%oldFlags = MaterialEditorTools.currentMaterial.getAnimFlags(MaterialEditorTools.currentLayer);

	if (%oldFlags $= "")
		%oldFlags = "\"\"";

	%action.oldValue = %oldFlags;
	MaterialEditorTools.submitUndo( %action );
	eval("materialEd_previewMaterial.animFlags[" @ MaterialEditorTools.currentLayer @ "] = " @ %flags @ ";");
	materialEd_previewMaterial.flush();
	materialEd_previewMaterial.reload();

	if (MaterialEditorTools.livePreview == true)
	{
		eval("MaterialEditorTools.currentMaterial.animFlags[" @ MaterialEditorTools.currentLayer @ "] = " @ %flags @ ";");
		MaterialEditorTools.currentMaterial.flush();
		MaterialEditorTools.currentMaterial.reload();
	}
}
//------------------------------------------------------------------------------
//==============================================================================

//These two functions are focused on object/layer specific functionality
function MaterialEditorTools::updateColorMultiply(%this,%color)
{
	%propName = "diffuseColor[" @ MaterialEditorTools.currentLayer @ "]";
	%this.syncGuiColor(MaterialEditorPropertiesWindow-->colorTintSwatch, %propName, %color);
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateSpecularCheckbox(%this,%value)
{
	MaterialEditorTools.updateActiveMaterial("pixelSpecular[" @ MaterialEditorTools.currentLayer @ "]", %value);
	MaterialEditorTools.guiSync( materialEd_previewMaterial );
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateSpecular(%this, %color)
{
	%propName = "specular[" @ MaterialEditorTools.currentLayer @ "]";
	%this.syncGuiColor(MaterialEditorPropertiesWindow-->specularColorSwatch, %propName, %color);
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateSubSurfaceColor(%this, %color)
{
	%propName = "subSurfaceColor[" @ MaterialEditorTools.currentLayer @ "]";
	%this.syncGuiColor(MaterialEditorPropertiesWindow-->subSurfaceColorSwatch, %propName, %color);
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateEffectColor0(%this, %color)
{
	%this.syncGuiColor(MaterialEditorPropertiesWindow-->effectColor0Swatch, "effectColor[0]", %color);
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateEffectColor1(%this, %color)
{
	%this.syncGuiColor(MaterialEditorPropertiesWindow-->effectColor1Swatch, "effectColor[1]", %color);
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateBehaviorSound(%this, %type, %sound)
{
	%defaultId = -1;
	%customName = "";

	switch$ (%sound)
	{
		case "<Soft>":
			%defaultId = 0;

		case "<Hard>":
			%defaultId = 1;

		case "<Metal>":
			%defaultId = 2;

		case "<Snow>":
			%defaultId = 3;

		default:
			%customName = %sound;
	}

	%this.updateActiveMaterial(%type @ "SoundId", %defaultId);
	%this.updateActiveMaterial("custom" @ %type @ "Sound", %customName);
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateSoundPopup(%this, %type, %defaultId, %customName)
{
	%ctrl = MaterialEditorPropertiesWindow.findObjectByInternalName( %type @ "SoundPopup", true );

	switch (%defaultId)
	{
		case 0:
			%name = "<Soft>";

		case 1:
			%name = "<Hard>";

		case 2:
			%name = "<Metal>";

		case 3:
			%name = "<Snow>";

		default:
			if (%customName $= "")
				%name = "<None>";
			else
				%name = %customName;
	}

	%r = %ctrl.findText(%name);

	if (%r != -1)
		%ctrl.setSelected(%r, false);
	else
		%ctrl.setText(%name);
}
//------------------------------------------------------------------------------
//==============================================================================
//These two functions are focused on environment specific functionality
function MaterialEditorTools::updateLightColor(%this, %color)
{
	matEd_previewObjectView.setLightColor(%color);
	matEd_lightColorPicker.color = %color;
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updatePreviewBackground(%this,%color)
{
	matEd_previewBackground.color = %color;
	MaterialPreviewBackgroundPicker.color = %color;
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateAmbientColor(%this,%color)
{
	matEd_previewObjectView.setAmbientLightColor(%color);
	matEd_ambientLightColorPicker.color = %color;
}
//------------------------------------------------------------------------------

//==============================================================================
// Global Material Options

function MaterialEditorTools::updateReflectionType( %this, %type )
{
	if ( %type $= "None" )
	{
		MaterialEditorPropertiesWindow-->matEd_cubemapEditBtn.setVisible(0);
		//Reset material reflection settings on the preview materials
		MaterialEditorTools.updateActiveMaterial( "cubeMap", "" );
		MaterialEditorTools.updateActiveMaterial( "dynamicCubemap", false );
		MaterialEditorTools.updateActiveMaterial( "planarReflection", false );
	}
	else
	{
		if (%type $= "cubeMap")
		{
			MaterialEditorPropertiesWindow-->matEd_cubemapEditBtn.setVisible(1);
			MaterialEditorTools.updateActiveMaterial( %type, materialEd_previewMaterial.cubemap );
		}
		else
		{
			MaterialEditorTools.updateActiveMaterial( %type, true );
		}
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::updateMaterialReferences( %this, %obj, %oldName, %newName )
{
	if ( %obj.isMemberOfClass( "SimSet" ) )
	{
		// invoke on children
		%count = %obj.getCount();

		for ( %i = 0; %i < %count; %i++ )
			%this.updateMaterialReferences( %obj.getObject( %i ), %oldName, %newName );
	}
	else
	{
		%objChanged = false;
		// Change all material fields that use the old material name
		%count = %obj.getFieldCount();

		for( %i = 0; %i < %count; %i++ )
		{
			%fieldName = %obj.getField( %i );

			if ( ( %obj.getFieldType( %fieldName ) $= "TypeMaterialName" ) && ( %obj.getFieldValue( %fieldName ) $= %oldName ) )
			{
				eval( %obj @ "." @ %fieldName @ " = " @ %newName @ ";" );
				%objChanged = true;
			}
		}

		EWorldEditor.isDirty |= %objChanged;

		if ( %objChanged && %obj.isMethod( "postApply" ) )
			%obj.postApply();
	}
}
//------------------------------------------------------------------------------
