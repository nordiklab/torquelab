//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function MEP_PBREdit::onValidate(%this)
{
	matLog("MEP_PBREdit::onValidate(%this)",%this,"Value",%this.getValue());
	%type = getWord(%this.internalName,0);
	%type = strreplace(%this.internalName,"TextEdit","");
	MaterialEditorTools.updateMaterialPBR(%type, %this.getValue());
	%slider =  MaterialEditorTools.findObjectByInternalName(%type@"Slider",true);

	if (!isObject(%slider))
		return;

	%slider.setValue(%this.getValue());
	//%slider = %container.findObjectByInternalName(%type SPC"Slider",true);
	//%slider.setValue(%this.getValue());
}
//------------------------------------------------------------------------------
//==============================================================================
function MEP_PBRSlider::onSliderValidate(%this)
{
	%type = getWord(%this.internalName,0);
	%type = strreplace(%this.internalName,"Slider","");
	MaterialEditorTools.updateMaterialPBR(%type, %this.getValue());

	%textEdit =  MaterialEditorTools.findObjectByInternalName(%type @"TextEdit",true);

	if (!isObject(%textEdit))
		return;

	//%textEdit = %container.findObjectByInternalName(%type @"TextEdit",true);
	%textEdit.setValue(%this.getValue());
}
//------------------------------------------------------------------------------

//==============================================================================
function MaterialEditorTools::updateMaterialPBR(%this, %propertyField,%value)
{
	matLog(" MaterialEditorTools::updateMaterialPBR(%this, %propertyField, %value, %isSlider, %onMouseUp)", %this, %propertyField, %value);
	%mat = MaterialEditorTools.currentMaterial;
	%layer = MaterialEditorTools.currentLayer;
	%isDirty = LabObj.set(%mat,%propertyField,%value,%layer);

	if (%isDirty)
		MaterialEditorTools.setMaterialDirty();

	%field = %propertyField@"[" @%layer @ "]";
	eval("materialEd_previewMaterial." @ %field @ " = " @ %value @ ";");
	materialEd_previewMaterial.flush();
	materialEd_previewMaterial.reload();
}

//==============================================================================
// PBR Script - Set/Get PBR CHannels
//==============================================================================
//==============================================================================
// Set PBR Channel in selectors
function MaterialEditorTools::setRoughChan(%this, %value)
{
	MaterialEditorTools.updateActiveMaterial("SmoothnessChan[" @ MaterialEditorTools.currentLayer @ "]", %value);
	MaterialEditorTools.guiSync( materialEd_previewMaterial );
}
//------------------------------------------------------------------------------
function MaterialEditorTools::setAOChan(%this, %value)
{
	MaterialEditorTools.updateActiveMaterial("aoChan[" @ MaterialEditorTools.currentLayer @ "]", %value);
	MaterialEditorTools.guiSync( materialEd_previewMaterial );
}
//------------------------------------------------------------------------------
function MaterialEditorTools::setMetalChan(%this, %value)
{
	MaterialEditorTools.updateActiveMaterial("metalChan[" @ MaterialEditorTools.currentLayer @ "]", %value);
	MaterialEditorTools.guiSync( materialEd_previewMaterial );
}
//------------------------------------------------------------------------------
//==============================================================================
// Get PBR Channels
function MaterialEditorTools::getRoughChan(%this, %channel)
{
	%guiElement = "roughChanBtn" @ %channel;
	%guiElement.setStateOn(true);
}
//------------------------------------------------------------------------------
function MaterialEditorTools::getAOChan(%this, %channel)
{
	%guiElement = "AOChanBtn" @ %channel;
	%guiElement.setStateOn(true);
}
//------------------------------------------------------------------------------
function MaterialEditorTools::getMetalChan(%this, %channel)
{
	%guiElement = "metalChanBtn" @ %channel;
	%guiElement.setStateOn(true);
}
//------------------------------------------------------------------------------
function MaterialEditorTools::saveCompositeMap(%this)
{
	%saveAs = "";
	%defaultPath = MaterialEditorTools.lastTexturePath;
	%dlg = new SaveFileDialog()
	{
		Filters        = "png";
		DefaultPath    = %defaultPath;
		ChangePath     = false;
		OverwritePrompt   = true;
	};

	%ret = %dlg.Execute();

	if(%ret)
	{
		// Immediately override/set the levelsDirectory
		// EditorSettings.setValue( "art/shapes/textures", collapseFilename(filePath( %dlg.FileName )) );
		%saveAs = %dlg.FileName;
	}

	%material = MaterialEditorTools.currentMaterial;
	%layer = %this.currentLayer;

	%roughMap = %material.roughMap[%layer];
	%aoMap = %material.aoMap[%layer];
	%metalMap = %material.metalMap[%layer];

	%smooth = %material.SmoothnessChan[%layer];
	%ao = %material.AOChan[%layer];
	%metal = %material.metalChan[%layer];

	%channelKey = %smooth SPC %ao SPC %metal SPC 3;
	error("Storing: \"" @ %roughMap @"\" \""@  %aoMap @"\" \""@ %metalMap @"\" \""@ %channelKey @"\" \""@ %saveAs @"\"");

	if (fileExt(%saveAs) !$= ".png")
	{
		%saveAs = filePath(%saveAs)@"/"@fileBase(%saveAs)@".png";
	}

	info("SaveComposite",%roughMap,%aoMap,%metalMap,"",%channelKey, %saveAs);
	//saveCompositeTexture(%roughMap,%aoMap,%metalMap,"",%channelKey, %saveAs);
	%dlg.delete();
}

