//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$MEP_ShowMap["Diffuse"] = true;

//==============================================================================
function MatEd::generateMapPills(%this)
{

	MEP_TextureMapStack.clear();

	foreach$(%map in $MatEd_TextureMaps)
	{
		%pill = cloneObject(MEP_MapContainerSrc,"MEP_"@%map@"Container",%map,MEP_TextureMapStack);
		%bitmap = %pill-->bitmap;
		%bitmap.internalName = %map @ "MapDisplayBitmap";

		%bitmapType = %pill-->bitmapType;
		%bitmapType.command = "MaterialEditorTools.selectTextureMap($ThisControl.internalName);";
		%bitmapType.internalName = %map;

		%fileName = %pill-->mapFileName;
		%fileName.internalName = %map @ "MapNameText";

		%file = %pill-->mapFile;
		%file.internalName = %map @ "FileNameText";

		%pill-->mapName.text = %map@":";

		%colorTintSwatch = %pill-->colorTintSwatchSrc;
		%colorTintSwatch.internalName = "colorTintSwatch";
		%colorTitle = %pill-->colorTitle;

		if (%map !$= "Diffuse")
		{
			delObj(%colorTintSwatch);
			delObj(%colorTitle);
		}

		%pill-->editIcon.command = "MaterialEditorTools.selectTextureMap($ThisControl.internalName);";
		%pill-->deleteIcon.command = "MaterialEditorTools.clearTextureMap($ThisControl.internalName);";
		%pill-->editIcon.internalName = %map;
		%pill-->deleteIcon.internalName = %map;
		%pill.visible = $MEP_ShowMap[%map];
	}
}
//------------------------------------------------------------------------------
