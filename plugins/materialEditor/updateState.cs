//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function MaterialEditorTools::onPreEditorSave(%this)
{
	MaterialEditorTools.currentLayer = "0";
	MaterialEditorTools.currentMaterial = "noshape_NoShape";
}
//------------------------------------------------------------------------------
//==============================================================================
// Clear and Refresh Material
function MaterialEditorTools::onPostEditorSave(%this)
{

}
//------------------------------------------------------------------------------
