//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$MatEd_HideColorMaterial = true;

$MatEd_RolloutExpanded["TextureMaps"] = 1;
$MatEd_RolloutExpanded["PBR"] = 0;
$MatEd_RolloutExpanded["Tranparency"] = 0;
$MatEd_RolloutExpanded["MaterialDamage"] = 0;
$MatEd_RolloutExpanded["Accumulation"] = 0;
$MatEd_RolloutExpanded["lighting"] = 0;
$MatEd_RolloutExpanded["animation"] = 0;
$MatEd_RolloutExpanded["advanced"] = 0;
