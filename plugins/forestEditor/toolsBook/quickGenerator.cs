//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function ForestEd::initQuickGenerator( %this )
{
	FEP_ToolsQuickGenerator-->groupName.setText("");
	FEP_ToolsQuickGenerator-->sourceFolder.setText("");
	FEP_ToolsQuickGenerator-->prefix.setText("");
}
//==============================================================================
function ForestEd::generateForestData( %this, %quick )
{
	%baseFolder = FEP_ToolsQuickGenerator-->sourceFolder.getText();
	%name = FEP_ToolsQuickGenerator-->groupName.getText();
	%prefix = FEP_ToolsQuickGenerator-->prefix.getText();

	if (%baseFolder $= "" || %name $= "" || %prefix $= "" )
		return;

	if (%quick)
		%prefix = "Qck";

	%settingContainer =FEP_ToolsSettingsGenerator;
	buildForestDataFromFolder(%baseFolder,%name,%prefix,%settingContainer,$FEP_GeneratorDeleteExistBrush,$FEP_GeneratorLevelItemsMode);
	FEP_Manager.updateBrushData();
	FEP_Manager.setDirty(true);
}
//------------------------------------------------------------------------------
