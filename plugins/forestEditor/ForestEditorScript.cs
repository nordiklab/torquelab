//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function ForestEditorPlugin::onEditMenuSelect( %this, %editMenu )
{
	%hasSelection = false;
	%selTool = ForestTools->SelectionTool;

	if ( ForestEditorGui.getActiveTool() == %selTool )
		if ( %selTool.getSelectionCount() > 0 )
			%hasSelection = true;

	%editMenu.enableItem( 3, %hasSelection ); // Cut
	%editMenu.enableItem( 4, %hasSelection ); // Copy
	%editMenu.enableItem( 5, %hasSelection ); // Paste
	%editMenu.enableItem( 6, %hasSelection ); // Delete
	%editMenu.enableItem( 8, %hasSelection ); // Deselect
}

//==============================================================================
// Callbacks Handlers - Called on specific editor actions
//==============================================================================

function ForestEditorPlugin::handleDelete( %this )
{
	ForestTools->SelectionTool.deleteSelection();
}

function ForestEditorPlugin::handleDeselect( %this )
{
	ForestTools->SelectionTool.clearSelection();
}

function ForestEditorPlugin::handleCut( %this )
{
	ForestTools->SelectionTool.cutSelection();
}

function ForestEditorPlugin::handleCopy( %this )
{
	ForestTools->SelectionTool.copySelection();
}

function ForestEditorPlugin::handlePaste( %this )
{
	ForestTools->SelectionTool.pasteSelection();
}

function ForestEditorPlugin::handlePaletteClick( %this,%mode )
{

	switch$(%mode)
	{
		case "select":
			GlobalGizmoProfile.mode = "None";
			EditorGuiStatusBar.setInfo("Selection arrow.");
			ForestEditorGui.setActiveTool(ForestTools->SelectionTool);

		case "move":
			GlobalGizmoProfile.mode = "Move";
			%cmdCtrl = "CTRL";

			if ($platform $= "macos")
				%cmdCtrl = "CMD";

			ForestEditorGui.setActiveTool(ForestTools->SelectionTool);
			EditorGuiStatusBar.setInfo("Move selection.  SHIFT while dragging duplicates objects.  " @ %cmdCtrl @ " to toggle soft snap.  ALT to toggle grid snap.");

		case "rotate":
			GlobalGizmoProfile.mode = "Rotate";
			EditorGuiStatusBar.setInfo("Rotate selection.");
			ForestEditorGui.setActiveTool(ForestTools->SelectionTool);

		case "scale":
			GlobalGizmoProfile.mode = "Scale";
			EditorGuiStatusBar.setInfo("Scagfgle selection.");
			ForestEditorGui.setActiveTool(ForestTools->SelectionTool);

		case "erase":
			EditorGuiStatusBar.setInfo("Erase selection.");
			ForestEditorGui.setActiveTool(ForestTools->BrushTool);
			ForestTools->BrushTool.mode = "Erase";

		case "EraseSelected":
			EditorGuiStatusBar.setInfo("EraseSelected selection.");
			ForestEditorGui.setActiveTool(ForestTools->BrushTool);
			ForestTools->BrushTool.mode = "EraseSelected";

		case "Paint":
			EditorGuiStatusBar.setInfo("Paint selection.");
			ForestEditorGui.setActiveTool(ForestTools->BrushTool);
			ForestTools->BrushTool.mode = "Paint";

	}

}
