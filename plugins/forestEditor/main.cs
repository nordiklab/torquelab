//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function ForestEditor::create( %this )
{
   info("Plugin","->","Initializing","Forest Editor");
	//$FEP_BrushSet = newSimSet("FEP_BrushSet");
	execFEP(true);

	if (!isObject(ForestEd))
		new scriptObject(ForestEd);

   %plugin = ForestEditorPlugin;
	//Lab.createPlugin("ForestEditor");
	//Add the different editor GUIs to the LabEditor
	%plugin.addEditor("ForestEditor",ForestEditorGui);
	%plugin.addGui("ForestEditor",   ForestEditorTools);
	%plugin.addToolbar("ForestEditor",ForestEditorToolbar);
	%plugin.addPalette("ForestEditor",   ForestEditorPaletteStack);
	%plugin.addDlg("ForestEditor",   ForestEditorDialogs);
	ForestEditorPlugin.commonPalette = "Select Move Rotate Scale";
	ForestEditorPlugin.editorGui = ForestEditorGui;
	ForestEditorPalleteWindow.position = getWord($pref::Video::mode, 0) - 209  SPC getWord(EditorGuiToolbar.extent, 1)-1;
	new SimSet(ForestTools)
	{
		new ForestBrushTool(ForestToolBrush)
		{
			internalName = "BrushTool";
			toolTip = "Paint Tool";
			buttonImage = "tlab/plugins/forest/images/brushTool";
		};
		new ForestSelectionTool(ForestToolSelection)
		{
			internalName = "SelectionTool";
			toolTip = "Selection Tool";
			buttonImage = "tlab/plugins/forest/images/selectionTool";
		};
	};
	%map = Lab.addPluginMap("ForestEditor");
	//%map = new ActionMap();
	%map.bindCmd( keyboard, "1", "ForestEditorSelectModeBtn.performClick();", "" ); // Select
	%map.bindCmd( keyboard, "2", "ForestEditorMoveModeBtn.performClick();", "" );   // Move
	%map.bindCmd( keyboard, "3", "ForestEditorRotateModeBtn.performClick();", "" ); // Rotate
	%map.bindCmd( keyboard, "4", "ForestEditorScaleModeBtn.performClick();", "" );  // Scale
	%map.bindCmd( keyboard, "5", "ForestEditorPaintModeBtn.performClick();", "" );  // Paint
	%map.bindCmd( keyboard, "6", "ForestEditorEraseModeBtn.performClick();", "" );  // Erase
	%map.bindCmd( keyboard, "7", "ForestEditorEraseSelectedModeBtn.performClick();", "" );  // EraseSelected
	//%map.bindCmd( keyboard, "backspace", "ForestEditorGui.onDeleteKey();", "" );
	//%map.bindCmd( keyboard, "delete", "ForestEditorGui.onDeleteKey();", "" );
	ForestEditorPlugin.map = %map;
  // initTerrainEditor();
}

//------------------------------------------------------------------------------

function ForestEditor::destroy( %this )
{
}

//----------------------------------------------------------------
$TLab_PluginName_["ForestEditor"] = "Forest Editor";
singleton GuiControlProfile (ForestEditorProfile)
{
	canKeyFocus = true;
	category = "Editor";
	fontColors[4] = "Magenta";
	fontColors[9] = "255 0 255 255";
	fontColorLink = "Magenta";
};

function execFEP(%loadGui)
{
	if (%loadGui)
	{
		exec( "./gui/forestEditorGui.gui" );
		exec( "./gui/ForestEditorTools.gui" );
		exec( "./gui/ForestEditorToolbar.gui" );
		exec( "./gui/forestEditorPalette.gui" );
		exec( "tlab/plugins/forestEditor/gui/forestEditorDialogs.gui" );
	}

	// Load Client Scripts.
	exec( "tlab/plugins/forestEditor/forestEditorGui.cs" );
	exec( "./tools.cs" );
	exec( "tlab/plugins/forestEditor/ForestEditorPlugin.cs" );
	exec( "tlab/plugins/forestEditor/ForestEditorSave.cs" );
	exec( "tlab/plugins/forestEditor/ForestEditorScript.cs" );
	execPattern("tlab/plugins/forestEditor/scripts/*.cs");
	execPattern("tlab/plugins/forestEditor/dialogs/*.cs");
	execPattern("tlab/plugins/forestEditor/toolsBook/*.cs");
}
function destroyForestEditor()
{
}

// NOTE: debugging helper.
function reinitForest()
{
	exec( "./main.cs" );
	exec( "./forestEditorGui.cs" );
	exec( "./tools.cs" );
}

