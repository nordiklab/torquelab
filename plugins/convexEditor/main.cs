//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$TLab_PluginName_["DecalEditor"] = "Decal Editor";
//------------------------------------------------------------------------------

function ConvexEditor::create( %this )
{
     info("ConvexEditor::->","Initializing","Convex Editor plugin" );
  	exec( "./gui/convexEditorGui.gui" );
	exec( "./gui/ConvexEditorTools.gui" );
	exec( "./gui/convexEditorToolbar.gui" );
	exec( "tlab/plugins/convexEditor/gui/convexEditorPalette.gui" );
	exec( "./convexEditorGui.cs" );
	exec( "tlab/plugins/convexEditor/ConvexEditorPlugin.cs" );
	exec( "tlab/plugins/convexEditor/ConvexEditorParams.cs" );
	//Lab.createPlugin("ConvexEditor");
	%map = Lab.addPluginEditor("ConvexEditor",ConvexEditorGui);
	Lab.addPluginGui("ConvexEditor",    ConvexEditorTools);
	//Lab.addPluginGui("ConvexEditor",   ConvexEditorOptionsWindow);
	//Lab.addPluginGui("ConvexEditor",   ConvexEditorTreeWindow);
	Lab.addPluginToolbar("ConvexEditor",ConvexEditorToolbar);
	Lab.addPluginPalette("ConvexEditor",   ConvexEditorPalette);
	ConvexEditorPlugin.editorGui = ConvexEditorGui;
	ConvexEditorPlugin.commonPalette = "Select Move Rotate Scale";
	// Note that we use the WorldEditor's Toolbar.
	%map = Lab.addPluginMap("ConvexEditor");
	//%map = new ActionMap();
	%map.bindCmd( keyboard, "1", "ConvexEditorNoneModeBtn.performClick();", "" );  // Select
	%map.bindCmd( keyboard, "2", "ConvexEditorMoveModeBtn.performClick();", "" );  // Move
	%map.bindCmd( keyboard, "3", "ConvexEditorRotateModeBtn.performClick();", "" );// Rotate
	%map.bindCmd( keyboard, "4", "ConvexEditorScaleModeBtn.performClick();", "" ); // Scale
	ConvexEditorPlugin.map = %map;
	// ConvexEditorPlugin.initSettings();
  // initTerrainEditor();
}

//------------------------------------------------------------------------------

function ConvexEditor::destroy( %this )
{
  
}

