//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function ConvexEditorPlugin::initDefaultCfg( %this,%cfgArray )
{
	%cfgArray.group[1] = "General settings";
	%cfgArray.setVal("MaterialName",       "Grid512_OrangeLines_Mat" TAB "MaterialName" TAB "TextEdit" TAB "" TAB "ConvexEditorGui" TAB "1");
}
