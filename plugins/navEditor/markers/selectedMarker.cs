//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
/*
  new StaticShape(Sector6_Droid1) {
         isAIControlled = "0";
         dataBlock = "AIPlayerMarker";
         hidden = "1";
            Behavior = "GuardBehavior";
            block = "Droid1";
            botBelongsToMe = "38194";
            delayRespawn = "149318";
            leash = "1";
            paceDist = "0";
            respawnCount = "0";
            respawnCounter = "0";
            sidestepDist = "5";
            team = "7";
            Weapon = "DroidGun5Weapon";
      };
*/
$NavEdMarkerEditFields = "Behavior block botBelongsToMe delayRespawn leash paceDist respawnCount respawnCounter sidestepDist team Weapon";
//==============================================================================
// NavEditor used rollout callback to set current edit mode
//==============================================================================
//==============================================================================
function NavEd::buildMarkerFields(%this,%markerObj)
{

	%stack = NavEdMarkerSelectedInfo-->StackStart;
	%stack.clear();

	%array = NavEdMarkerSelectedInfo-->ArrayMid;
	%array.clear();

	Widget.dataPreset("datawidth 75",1);

	%dataStr = "internalName block" TAB "superClass NavEdMarkerDropDown" TAB "name NavEdMarkerBlockMenu";
	%setupStr = "postEval NavEd.updateAIDatablockMenu();";
	%stack.buildWidget("AI Block","Dropdown",%dataStr,%setupStr);

	%dataStr = "internalName weapon" TAB "superClass NavEdMarkerDropDown" TAB "name NavEdMarkerWeaponMenu";
	%setupStr = "postEval NavEd.updateAIWeaponMenu();";
	%stack.buildWidget("Weapon","Dropdown",%dataStr,%setupStr);

	%dataStr = "internalName behavior" TAB "superClass NavEdMarkerDropDown";
	%setupStr = "postEval %this.cloneSimSet(allBehaviorsSet,\"\",\"ABehaviorA\");";
	%stack.buildWidget("Behavior","Dropdown",%dataStr,%setupStr);

	Widget.dataPreset("style small" TAB "skeleton infoTop",1);

	%dataStr = "internalName botBelongsToMe" TAB "superClass NavEdMarkerEdit";
	%array.buildWidget("Owner","Edit",%dataStr,"dataWidth 20");

	%dataStr = "internalName leash" TAB "superClass NavEdMarkerEdit";
	%array.buildWidget("leash","Edit",%dataStr,"dataWidth 20");

	%dataStr = "internalName delayRespawn" TAB "superClass NavEdMarkerEdit";
	%array.buildWidget("delayRespawn","Edit",%dataStr,"dataWidth 20");

	%dataStr = "internalName paceDist" TAB "superClass NavEdMarkerEdit";
	%array.buildWidget("paceDist","Edit",%dataStr,"dataWidth 20");

	%dataStr = "internalName respawnCount" TAB "superClass NavEdMarkerEdit";
	%array.buildWidget("Respawns","Edit",%dataStr,"dataWidth 20");

	%dataStr = "internalName respawnCounter" TAB "superClass NavEdMarkerEdit";
	%array.buildWidget("Counter","Edit",%dataStr,"dataWidth 20");

	%dataStr = "internalName sidestepDist" TAB "superClass NavEdMarkerEdit";
	%array.buildWidget("side Dist","Edit",%dataStr,"dataWidth 20");

	%dataStr = "internalName team" TAB "superClass NavEdMarkerEdit";
	%array.buildWidget("team","Edit",%dataStr,"dataWidth 15");

	Widget.clearPreset();
}
//------------------------------------------------------------------------------
//==============================================================================
function NavEd::setActiveMarker(%this,%markerObj)
{
	NavEd.activeMarker = %markerObj;
	%stack = NavEdMarkerSelectedInfo;

	if (isObject(%markerObj))
	{
		%stack.callOnChildrenNoRecurse("setVisible",1);

		foreach$(%field in $NavEdMarkerEditFields)
		{
			%value = %markerObj.getFieldValue(%field);
			%ctrl = NavEdMarkerSelectedInfo.findObjectByInternalName(%field,true);

			if (!isObject(%ctrl))
				continue;

			%ctrl.setTypeValue(%value);
		}
	}
	else
	{
		%stack.callOnChildrenNoRecurse("setVisible",0);
	}

}
//------------------------------------------------------------------------------
//NavEd.activeMarker = NavEdAIMarkerSet.getObject(0);

//==============================================================================
function NavEd::setMarkerField(%this,%field,%value)
{

	if (!isObject(NavEd.activeMarker))
	{
		%this.setActiveMarker();
		return;
	}

	LabObj.update(NavEd.activeMarker,%field,%value);
}
//------------------------------------------------------------------------------
//==============================================================================
function NavEdMarkerDropDown::onSelect(%this,%id,%text)
{

	NavEd.setMarkerField(%this.internalName,%text);

}
//------------------------------------------------------------------------------

//==============================================================================
function NavEdMarkerEdit::onValidate(%this)
{

	NavEd.setMarkerField(%this.internalName,%this.getText());

}
//------------------------------------------------------------------------------
//==============================================================================
function NavEd_MarkerList::onSelect(%this,%index,%text)
{

	%obj = %this.getItemObject(%index);
	NavEd.setActiveMarker(%obj);
}
//------------------------------------------------------------------------------

//==============================================================================
function NavEd_MarkerList::onUnselect( %this,%index,%text)
{
	NavEd.setActiveMarker();
}
//------------------------------------------------------------------------------
