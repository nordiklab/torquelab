//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function NavInspector::inspect(%this, %obj)
{
	%name = "";

	if (isObject(%obj))
		%name = %obj.getName();
	else
		NavFieldInfoControl.setText("");

	Parent::inspect(%this, %obj);
}

function NavInspector::onInspectorFieldModified(%this, %object, %fieldName, %arrayIndex, %oldValue, %newValue)
{
	// Same work to do as for the regular WorldEditor Inspector.
	Inspector::onInspectorFieldModified(%this, %object, %fieldName, %arrayIndex, %oldValue, %newValue);
}

function NavInspector::onFieldSelected(%this, %fieldName, %fieldTypeStr, %fieldDoc)
{
	NavFieldInfoControl.setText("<font:ArialBold:14>" @ %fieldName @ "<font:ArialItalic:14> (" @ %fieldTypeStr @ ") " NL "<font:Arial:14>" @ %fieldDoc);
}

function NavTreeView::onInspect(%this, %obj)
{
	NavInspector.inspect(%obj);
}

function NavTreeView::onSelect(%this, %obj)
{
	NavInspector.inspect(%obj);
	NavEditorGui.onObjectSelected(%obj);
}
function NavEd::saveToFile(%this,%obj,%file)
{
	if (%obj $= "")
		%obj = NavEditorGui.selectedObject;

	if (%file $= "")
		%file = strReplace($LevelFile,".mis",".nav");

	if (!isObject(%obj))
		return;

	LabObj.update(%obj,"fileName",%file);
	%obj.save();
}
function NavEd_NavMeshMenu::onSelect(%this,%id,%text)
{
	NavEditorGui.selectObject(%id);
}

function NavEditorGui::selectObject(%this, %obj)
{
	NavTreeView.clearSelection();
	//Scene.selectObject(%obj);
	EWorldEditor.selectObject(%obj);

	if (isObject(%obj))
		NavTreeView.selectItem(%obj);

	%this.onObjectSelected(%obj);
}

function NavEditorGui::onObjectSelected(%this, %obj)
{
	if (isObject(%this.selectedObject))
		%this.deselect();

	%this.selectedObject = %obj;

	if (isObject(%obj))
	{
		%this.selectMesh(%obj);
		NavInspector.inspect(%obj);
	}
}
