//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function NavEdTestEdit::onValidate(%this)
{
	$NavEd_Test[%this.internalName]= %this.getText();

}
function NavEd::initTestData(%this)
{

	if (!isObject(NavEditorGui.spawnDatablock ))
		NavEditorGui.spawnDatablock  = "";

	NavEditorGui.spawnClass = "AIPlayer";
	NavEd_SpawnDatablockMenu.clear();
	%aiList = getDatablockClassList("PlayerData");

	foreach$(%id in %aiList)
	{
		//if (%id.team $= "")
		// continue;
		if (%defaultData $= "")
			%defaultData = %id;

		NavEd_SpawnDatablockMenu.add(%id.getName(),%id);
	}

	if (!isObject(NavEditorGui.spawnDatablock ))
		NavEditorGui.spawnDatablock  = %defaultData.getName();

	NavEd_SpawnDatablockMenu.setText(NavEditorGui.spawnDatablock);
}
function NavEd_SpawnDatablockMenu::onSelect(%this,%id,%text)
{
	NavEditorGui.spawnDatablock  =%text;
}

//==============================================================================
// Plugin Object Params - Used set default settings and build plugins options GUI
//==============================================================================

function NavMeshTestFlagButton::onClick(%this)
{
	NavEditorGui.updateTestFlags();
}

function NavEditorGui::updateTestFlags(%this)
{
	if (isObject(%this.getPlayer()))
	{
		%properties = NavEditorOptionsWindow-->TestProperties;
		%player = %this.getPlayer();
		$NavBot = %player;
		%player.allowWwalk = %properties->LinkWalkFlag.isStateOn();
		%player.allowJump = %properties->LinkJumpFlag.isStateOn();
		%player.allowDrop = %properties->LinkDropFlag.isStateOn();
		%player.allowLedge = %properties->LinkLedgeFlag.isStateOn();
		%player.allowClimb = %properties->LinkClimbFlag.isStateOn();
		%player.allowTeleport = %properties->LinkTeleportFlag.isStateOn();
		%this.isDirty = true;
	}
}
function NavEditorGui::setFollowObject(%this,%object)
{

	if (!isObject(%object))
		return;

	$NavEd_TestFollowObject = %object;
	NavEd_ModeRolloutBox-->test-->followObject.setText($NavEd_TestFollowObject);

}
function NavEditorGui::followObject(%this)
{
	if (!isObject($NavEd_TestFollowObject))
	{
		$NavEd_TestFollowObject = LocalClientConnection.player;
		NavEd_ModeRolloutBox-->test-->followObject.setText($NavEd_TestFollowObject);
	}

	%radius = NavEd_ModeRolloutBox-->test-->FollowRadius.getText();

	if (isObject($NavEd_TestFollowObject))
		%this.getPlayer().followObject($NavEd_TestFollowObject, %radius);
}
/*
if (%this.getMode() $= "TestMode" && isObject(%this.getPlayer())) {
	%obj = LocalClientConnection.player;
	%text = NavEd_ModeRolloutBox-->test->FollowObject.getText();

	if (%text !$= "") {
		eval("%obj = " @ %text);

		if (!isObject(%obj))
			MessageBoxOk("Error", "Cannot find object" SPC %text);
	}

	if (isObject(%obj))
		%this.getPlayer().followObject(%obj, NavEditorOptionsWindow-->TestProperties->FollowRadius.getText());
}
*/
//}
