//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function NavEditorGui::createCoverPoints(%this)
{
	if (isObject(%this.getMesh()))
	{
		%this.getMesh().createCoverPoints();
		%this.isDirty = true;
	}
}

function NavEditorGui::deleteCoverPoints(%this)
{
	if (isObject(%this.getMesh()))
	{
		NavEditorGui.getMesh().deleteCoverPoints();
		%this.isDirty = true;
	}
}

function NavEditorGui::findCover(%this)
{
	if (%this.getMode() $= "TestMode" && isObject(%this.getPlayer()))
	{
		%pos = LocalClientConnection.getControlObject().getPosition();
		%text = NavEditorOptionsWindow-->TestProperties->CoverPosition.getText();

		if (%text !$= "")
			%pos = eval(%text);

		%this.getPlayer().findCover(%pos, NavEditorOptionsWindow-->TestProperties->CoverRadius.getText());
	}
}
