//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function NavEditor::create( %this )
{
 info("Plugin","->","Initializing","Nav Editor");
	execNavEd(true);
	$NavEd = newScriptObject("NavEd");
	//exec("./gui/profiles.cs");
	//Lab.createPlugin("NavEditor","Nav Editor");
	NavEditorPlugin.superClass = "WEditorPlugin";
	Lab.addPluginEditor("NavEditor",NavEditorGui); //Tools renamed to Gui to store stuff
	%map = Lab.addPluginMap("NavEditor");
	Lab.addPluginGui("NavEditor",NavEditorTools); //Tools renamed to Gui to store stuff
	//Lab.addPluginDlg("NavEditor",CreateNewNavMeshDlg);
	Lab.addPluginDlg("NavEditor",NavEditorConsoleDlg);
	Lab.addPluginToolbar("NavEditor",NavEditorToolbar);
	Lab.addPluginPalette("NavEditor",NavEditorPalette);
}

//------------------------------------------------------------------------------

function NavEditor::destroy( %this )
{
 
}

//----------------------------------------------------------------
$TLab_PluginName_["NavEditor"] = "Nav Editor";
//==============================================================================

//------------------------------------------------------------------------------
//==============================================================================
function execNavEd(%loadGui)
{
	if (%loadGui)
	{
		// Load MaterialLab Guis
		exec("tlab/plugins/navEditor/gui/NavEditorConsoleDlg.gui");
		exec("tlab/plugins/navEditor/gui/CreateNewNavMeshDlg.gui");
		exec("tlab/plugins/navEditor/gui/NavEditorToolbar.gui");
		exec("tlab/plugins/navEditor/gui/NavEditorTools.gui");
		exec("tlab/plugins/navEditor/gui/NavEditorGui.gui");
		exec("tlab/plugins/navEditor/gui/NavEditorPalette.gui");
	}

	// Load Client Scripts.
	exec("./NavEditorPlugin.cs");
	execPattern("tlab/plugins/navEditor/scripts/*.cs");
	execPattern("tlab/plugins/navEditor/markers/*.cs");
	execPattern("tlab/plugins/navEditor/editor/*.cs");
}
//==============================================================================
function destroyMaterialLab()
{
}
//------------------------------------------------------------------------------

$Nav::WalkFlag = 1 << 0;
$Nav::SwimFlag = 1 << 1;
$Nav::JumpFlag = 1 << 2;
$Nav::LedgeFlag = 1 << 3;
$Nav::DropFlag = 1 << 4;
$Nav::ClimbFlag = 1 << 5;
$Nav::TeleportFlag = 1 << 6;
