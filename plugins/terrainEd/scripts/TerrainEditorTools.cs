//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function TEManager::getCurrentHeight(%this,%id)
{
	%avgHeight = ETerrainEditor.lastAverageHeight;
	%value = mFloatLength(%avgHeight,2);
	ETerrainEditor.setHeightVal = %value;

	EWTerrainEditToolbar-->setHeight.text = %value;
}
//------------------------------------------------------------------------------
