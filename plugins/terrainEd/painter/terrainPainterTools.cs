//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$TPT_SubMaterialSuffix = "_1";
//==============================================================================
function TerrainPainterTools::browseDefaultTexturesFolder( %this)
{
	%startFrom = %this.defaultTexturesFolder;

	if (%startFrom $= "")
		%startFrom = "art/";

	getFolderName("","TerrainPainterTools.onSelectDefaultTexturesFolder",%startFrom);
}
//------------------------------------------------------------------------------

//==============================================================================
function TerrainPainterTools::onSelectDefaultTexturesFolder( %this, %folder)
{
	%this.defaultTexturesFolder = %folder;
	%this-->defaultTexturesFolder.setText(%folder);
}
//------------------------------------------------------------------------------

//==============================================================================
function TerrainPainterTools::setDefaultTexturesFolder( %this, %folder)
{
	%this.defaultTexturesFolder = %folder;
	%this-->defaultTexturesFolder.setText(%folder);
}
//------------------------------------------------------------------------------

//==============================================================================
// Create GroundCover Terrain Material CLone
//==============================================================================
