//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function TerrainEd::create( %this )
{

	info("Plugin","->","Initializing","Terrain Editor" );

	if (!isObject(TerrainEditorTools))
	{
		execTerrainEd(true);
		//Add the plugin GUI elements
		//----------------------------------------------
		// Terrain Editor Plugin
		//Lab.createPlugin("TerrainEd","Terrain Editor");
		Lab.addPluginToolbar("TerrainEd",EWTerrainEditToolbar);
		Lab.addPluginGui("TerrainEd",TerrainEditorTools);
		Lab.addPluginPalette("TerrainEd",TerrainEditorPalette);
		Lab.addPluginDlg("TerrainEd",TerrainEditorDialogs);
		//Lab.addPluginDlg("TerrainEd",TerrainMaterialDlg);
		TerrainEdPlugin.PM = new PersistenceManager();
		TerrainEdPlugin.setEditorMode("Terrain");
	}

	//----------------------------------------------

	newSimSet("FilteredTerrainMaterialsSet");
	TerrainMaterialDlg-->materialFilter.setText("");
	//Create scriptobject for paint generator
	$TEAutoPainter = newScriptObject("TEAutoPainter");
	$TerrainManager = newScriptObject("TEManager");
	$TerrainEditorInitDone = true;
	$TEPainter = newScriptObject("TEPainter");
}

//------------------------------------------------------------------------------

function execTerrainEd(%loadGui)
{
	//----------------------------------------------
	// Terrain Editor GUIs
	if (%loadGui )
	{
		exec("tlab/plugins/terrainEd/gui/TerrainCreatorGui.gui" );
		exec("tlab/plugins/terrainEd/gui/TerrainImportGui.gui" );
		exec("tlab/plugins/terrainEd/gui/TerrainExportGui.gui" );
		exec("tlab/plugins/terrainEd/gui/TerrainEditorVSettingsGui.gui");
		exec("tlab/plugins/terrainEd/gui/TerrainEditorPalette.gui");
		exec("tlab/plugins/terrainEd/gui/EWTerrainEditToolbar.gui");
		exec("tlab/plugins/terrainEd/gui/TerrainEditorDialogs.gui");
		exec("tlab/plugins/terrainEd/gui/TerrainEditorTools.gui");
		exec("tlab/plugins/terrainEd/gui/TerrainMaterialDlg.gui");
		exec("tlab/plugins/terrainEd/gui/TerrainAutoPaintGui.gui");
	
	}

	exec("tlab/plugins/terrainEd/gui/TerrainImportGui.cs");
	exec("tlab/plugins/terrainEd/gui/TerrainExportGui.cs");
	exec("tlab/plugins/terrainEd/gui/TerrainCreatorGui.cs" );

	exec("tlab/plugins/terrainEd/TerrainEdPlugin.cs");
	execPattern("tlab/plugins/terrainEd/scripts/*.cs");
	execPattern("tlab/plugins/terrainEd/brushTool/*.cs");
	execPattern("tlab/plugins/terrainEd/painter/*.cs");
	execPattern("tlab/plugins/terrainEd/terrainMaterials/*.cs");
	execPattern("tlab/plugins/terrainEd/autoPainter/*.cs");

}

