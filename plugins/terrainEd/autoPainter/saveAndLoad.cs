//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$TEAutoPainter_FileFilter = "Lab Painter Layers|*.painter.cs";
$TEAutoPainter_SaveDialogMode = false;
$TEAutoPainter_LayerPath = "tlab/plugins/terrainEd/painterLayers";
//==============================================================================
// Set the layer group file folder base (Editor or Level)
function TEAutoPainter::setLayersFolderBase(%this,%menu)
{
	TEAutoPainter.folderBase = %menu.getText();
}
//------------------------------------------------------------------------------

//==============================================================================
// Save painter layers set
//==============================================================================
//==============================================================================
// Save all the layers to a file
function TEAutoPainter::quickSaveLayerGroup(%this,%saveLoadedGroup)
{
	%layersPath = "tlab/plugins/terrainEd/painterLayers/";
	%fileName = TEAutoPainter_Window-->groupName.getText();
	%folders = TEAutoPainter_Window-->groupFolders.getText();

	if (%fileName $= "")
		%fileName = "Default";

	%saveTmp = %layersPath@%folders@"/"@%fileName@".painter.cs";
	%saveTo = strreplace(%saveTmp,"//","/");
	%this.saveLayerGroupHandler(%saveTo);
}
//------------------------------------------------------------------------------
//==============================================================================
// Save all the layers to a file
function TEAutoPainter::saveLayerGroup(%this)
{
	%baseFile = $TEAutoPainter_LayerPath @"/default.painter.cs";
	getSaveFilename($TEAutoPainter_FileFilter, "TEAutoPainter.saveLayerGroupHandler", %baseFile);
}
//------------------------------------------------------------------------------
//==============================================================================
// Callback for the saved file
function TEAutoPainter::saveLayerGroupHandler( %this,%filename )
{
	%filename = makeRelativePath( %filename, getMainDotCsDir() );

	if (strStr(%filename, ".") == -1)
		%filename = %filename @ ".painter.cs";

	%clone = TEAutoPainter_LayerGroup.deepClone();
	delObj("TEAutoPainter_LayerGroup_Saved");
	%clone.setName("TEAutoPainter_LayerGroup_Saved");
	// Create a file object for writing
	%fileWrite = new FileObject();
	%fileWrite.OpenForWrite(%filename);
	%fileWrite.writeObject(%clone);
	%fileWrite.close();
	%fileWrite.delete();
	info("Terrain painter layers save to file:",%filename);

	if (TEAutoPainter_Explorer.isAwake())
		TEAutoPainter.getSavedGroupsData();
}
//------------------------------------------------------------------------------

//==============================================================================
// Load painter layer set
//==============================================================================

//==============================================================================
// Load all the layers saved in a file
function TEAutoPainter::loadLayerGroup(%this)
{
	%baseFile = $TEAutoPainter_LayerPath @"/default.painter.cs";
	getLoadFilename($TEAutoPainter_FilerFilter, "TEAutoPainter.loadLayerGroupHandler",%baseFile);
}
//------------------------------------------------------------------------------
//==============================================================================
// Callback for file loader
function TEAutoPainter::loadLayerGroupHandler(%this,%file)
{
	if ( !isScriptFile( %file ) )
	{
		warnLog("loadLayerGroupHandler got invalid file:",%file);
		return;
	}

	delObj(TEAutoPainter_LayerGroup_Saved);
	%file = expandFilename(%file);
	exec(%file);

	if (!isObject(TEAutoPainter_LayerGroup_Saved))
	{
		warnLog("Invalid layer group file. Can't find the object TEAutoPainter_LayerGroup_Saved");
		return;
	}

	%file = makeRelativePath(%file);
	%fileName = strreplace(fileBase(%file),".painter","");
	%path = filePath(%file);
	%folders = strreplace(%path,$TEAutoPainter_LayerPath,"");

	if (getSubStr(%folders,0,1) $= "/")
		%folders = getSubStr(%folders,1);

	TEAutoPainter_Window-->groupFolders.setText(%folders);
	TEAutoPainter_Window-->groupName.setText(%fileName);
	TEAutoPainter.loadedGroupFile = %file;
	TEAutoPainter_Window-->saveGroupButton.active = 1;

	//if (!isObject(TEAutoPainter_LayerGroup))
	//new SimGroup( TEAutoPainter_LayerGroup );
	//TerrainAutoPaintGui.deleteLayerGroup();
	if ( !$TEAutoPainter_AppendLoadedLayers)
	{
		//TerrainAutoPaintGui.deleteLayerGroup();
		TEAutoPainter_LayerGroup.deleteAllObjects();
	}
	else
	{
		TEAutoPainter_Window-->saveGroupButton.active = false;
	}

	foreach(%layer in TEAutoPainter_LayerGroup_Saved)
	{
		%addLayers = strAddWord(%addLayers,%layer.getId());
	}

	foreach$(%layer in %addLayers)
	{
		TEAutoPainter_LayerGroup.add(%layer);
	}

	TEAutoPainter_LayerGroup.dumpData();
	delObj(TEAutoPainter_LayerGroup_Saved);
	%mats = ETerrainEditor.getMaterials();
	TEAutoPainter_StackLayers.clear();

	foreach(%layer in TEAutoPainter_LayerGroup)
	{
		%layer.matInternalName = strreplace(%layer.matInternalName,"*","");

		if ($CreateMissing)
		{
			if (recordFind(%mats,%layer.matInternalName) $= "-1")
			{
				%result = TEAutoPainter.addTerrainMaterialLayer(%layer.matInternalName);
				%mats = ETerrainEditor.getMaterials();
			}
		}

		%layer.pill = "";
		TerrainAutoPaintGui.addLayerPill(%layer,true);
	}
}
//------------------------------------------------------------------------------
