//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$TEAutoPainter_DefaultHeightMin = 0;
$TEAutoPainter_DefaultHeightMax = 1000;
$TEAutoPainter_DefaultSlopeMin = 0;
$TEAutoPainter_DefaultSlopeMax = 90;
$TEAutoPainter_DefaultCoverage = 100;
$Lab::TerrainPainter::ValidateHeight = "0";
$TEAutoPainter_Coverage = 100;

$TEAutoPainter_CreateMissingMaterials = false;

$TEAutoPainter_AppendLoadedLayers = false;
$TerrainAutoPaintGui_Initialized = false;
//==============================================================================
// Multi material automatic terrain painter
//==============================================================================

//==============================================================================
function TerrainAutoPaintGui::onWake(%this)
{
	if (!$TerrainAutoPaintGui_Initialized)
		TEAutoPainter.init();

	TEAutoPainter_Window-->saveGroupButton.visible = 0;
	TEAutoPainter_GenerateProgressWindow.setVisible(false);
	TEAutoPainter_PillSample.setVisible(false);
	TEAutoPainter.checkLayersStackGroup();

	foreach(%layer in TEAutoPainter_LayerGroup)
	{
		%this.updateLayerPill(%layer);
		%this.updateLayerMaterialMenu(%layer);
	}

	//TEAutoPainter.map.push();
	Lab.hidePluginTools();
}
//------------------------------------------------------------------------------
//==============================================================================
function TerrainAutoPaintGui::onSleep(%this)
{
	//TEAutoPainter.map.pop();
	Lab.showPluginTools();
}
//------------------------------------------------------------------------------
//==============================================================================
function TEAutoPainter::toggleOptions(%this)
{
	TEAutoPainter_Options.visible = !TEAutoPainter_Options.visible;
	TerrainAutoPaintGui-->optionsIcon.setStateOn(TEAutoPainter_Options.visible);
}
//------------------------------------------------------------------------------
//==============================================================================
function TEAutoPainter::addTerrainMaterialLayer(%this,%matInternalName)
{
	%mat = TerrainMaterialSet.findObjectByInternalName( %matInternalName );

	if (!isObject(%mat))
		return false;

	ETerrainEditor.addMaterial(%matInternalName);
	EPainter.updateLayers();
	ETerrainEditor.updateMaterial(  EPainterStack.getCount(), %mat.getInternalName() );
	return true;
}
//------------------------------------------------------------------------------

//==============================================================================
function TerrainAutoPaintGui::editLayer(%this, %buttonCtrl)
{
	%this.openLayerSettings(%buttonCtrl.layerObj);
}
//------------------------------------------------------------------------------
//==============================================================================
function TerrainAutoPaintGui::deleteLayer(%this, %buttonCtrl)
{
	%layer = %buttonCtrl.layerObj;
	TEAutoPainter_LayerGroup.remove(%layer);
	%buttonCtrl.layerObj.pill.delete();
	%layer.delete();
}
//------------------------------------------------------------------------------
//==============================================================================
function TerrainAutoPaintGui::deleteLayerGroup(%this)
{
	TEAutoPainter_LayerGroup.clear();
	TEAutoPainter_StackLayers.clear();
	TEAutoPainter_Window-->saveGroupButton.active = false;
}
//------------------------------------------------------------------------------
//==============================================================================
function TerrainAutoPaintGui::deleteInvalidLayers(%this)
{
	foreach(%pill in TEAutoPainter_StackLayers)
	{
		%layer = %pill.layerObj;

		if (strFind(%layer.matInternalName,"*"))
			%removeList = strAddWord(%removeList,%layer.getId());
	}

	foreach$(%layer in %removeList)
	{
		delObj(%layer.pill);
		delObj(%layer);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Copy/Paste layer parameters system
//==============================================================================
//==============================================================================
function TEAutoPainter_FieldCopyButton::onClick(%this)
{
	%layer = %this.layerObj;
	%pill = %layer.pill;
	%wordsData = strreplace(%this.internalName,"_"," ");
	%field = getWord(%wordsData,0);
	%action = getWord(%wordsData,1);

	if (%action $= "Copy")
	{
		TEAutoPainter.clipBoard = %layer.getFieldValue(%field);
	}
	else if (%action $= "Paste")
	{
		if (TEAutoPainter.clipBoard $= "")
			return;

		%layer.setFieldValue(%field,TEAutoPainter.clipBoard);
		%ctrl = %pill.findObjectByInternalName(%field,true);
		%ctrl.setText(TEAutoPainter.clipBoard);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
function TEAutoPainter::setCurrentHeight(%this,%id)
{
	if (TEAutoPainter.height[%id] $= "")
		return;

	TEAutoPainter.clipBoard = TEAutoPainter.height[%id];
}
//------------------------------------------------------------------------------
//==============================================================================
function TEAutoPainter::pasteCurrentHeight(%this,%id)
{
	if (TEAutoPainter.clipBoard $= "")
		return;

	TEAutoPainter.height[%id] = TEAutoPainter.clipBoard;
	$TEAutoPainter_ValueStore[%id] = TEAutoPainter.clipBoard;
	eval("TEAutoPainter_StoredValues-->v"@%id@".setText(\""@TEAutoPainter.clipBoard@"\");");
}
//------------------------------------------------------------------------------
