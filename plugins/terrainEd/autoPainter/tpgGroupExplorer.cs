//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function TEAutoPainter_Explorer::onWake(%this)
{
	TEAutoPainter.getSavedGroupsData();
}
//------------------------------------------------------------------------------

//==============================================================================
function TEAutoPainter_Explorer::loadSelectedLayerGroup(%this)
{
	%file = TEAutoPainter_Explorer.selectedFile;

	if (!isFile(%file))
	{
		warnLog("Invalid file selected:",%file);
		return;
	}

	TEAutoPainter.loadLayerGroupHandler(%file);
	TEAutoPainter_ExplorerInfo-->groupName.text = filebase(%file);
}
//------------------------------------------------------------------------------
//==============================================================================
function TEAutoPainter_Explorer::toggleGroupInfo(%this,%checkbox)
{
	%showInfo = %checkbox.isStateOn();
	TEAutoPainter_ExplorerInfo.visible = %showInfo;
}
//------------------------------------------------------------------------------

//==============================================================================
function TEAutoPainter::getSavedGroupsData(%this)
{
	%basePath = "tlab/plugins/terrainEd/painterLayers";
	%searchFolder = %basePath @ "/*.cs";
	%folderColor = "\c2";
	TEAutoPainter_SavedGroupTree.clear();
	TEAutoPainter_Explorer.selectedFile = "";

	//Now go through each files again to add a brush with latest items
	for(%file = findFirstFile(%searchFolder); %file !$= ""; %file = findNextFile(%searchFolder))
	{
		%fileName = fileBase(%file);
		%fileName = strreplace(%fileName,".painter","");
		%path = filePath(%file);
		%relPath = strreplace(%path,%basePath@"/","");
		%relPath = strreplace(%relPath,%basePath,"");
		%folderOrder = strreplace(%relPath,"/","\t");
		%targetId = 0;

		for(%i = 0; %i < getFieldCount(%folderOrder); %i++)
		{
			%folder = trim(getField(%folderOrder,%i));
			%childId = TEAutoPainter_SavedGroupTree.findChildItemByName(%targetId,%folderColor@%folder);

			if (%childId <= 0)
			{
				%childId = TEAutoPainter_SavedGroupTree.insertItem(%targetId,%folderColor@%folder,"Folder","",0,0);
			}

			%targetId = %childId;
		}

		%itemId = TEAutoPainter_SavedGroupTree.insertItem(%targetId,%fileName,%file,"",0,0);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
function TEAutoPainter::selectSavedGroupFile(%this,%file)
{
	TEAutoPainter_Explorer.selectedFile = %file;
	delObj(TEAutoPainter_LayerGroup_Saved);
	%file = expandFilename(%file);
	exec(%file);

	if (!isObject(TEAutoPainter_LayerGroup_Saved))
	{
		warnLog("Invalid layer group file. Can't find the object TEAutoPainter_LayerGroup_Saved");
		return;
	}

	%count = TEAutoPainter_LayerGroup_Saved.getCount();
	TEAutoPainter_ExplorerInfo-->groupName.text = filebase(%file)@" (\c2"@%count@"\c0)";
	%textList = TEAutoPainter_ExplorerInfo-->selectedLayers;
	%textList.clear();

	foreach(%layer in TEAutoPainter_LayerGroup_Saved)
	{
		%heightMax = %layer.heightMax;

		if (%heightMax >= $TEAutoPainter_DefaultHeightMax)
			%heightMax = $TEAutoPainter_DefaultHeightMax;

		%heightTab = mCeil(%layer.heightMin) @"/"@mCeil(%heightMax);
		%slopeTab = mCeil(%layer.slopeMin) @"/"@mCeil(%layer.slopeMax);
		%textList.addRow(%id++,%layer.matInternalName TAB "\c2"@%heightTab TAB "\c3"@%slopeTab);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Terrain Paint Generator - Layer Validation Functions
//==============================================================================

//==============================================================================
function TEAutoPainter_SavedGroupTree::onSelect(%this,%itemId)
{
	%text = TEAutoPainter_SavedGroupTree.getItemText(%itemId);
	%value = %this.getItemValue(%itemId);
	%sibling = %this.getNextSibling(%itemId);

	if (%value $= "Folder")
		return;

	if (!isFile(%value))
		return;

	TEAutoPainter.selectSavedGroupFile(%value);
}
//------------------------------------------------------------------------------

//==============================================================================
function TEAutoPainter_SavedGroupTree::onMouseUp(%this,%itemId,%clicks)
{
	if (%clicks > 1)
		TEAutoPainter_Explorer.loadSelectedLayerGroup();
}
//------------------------------------------------------------------------------
