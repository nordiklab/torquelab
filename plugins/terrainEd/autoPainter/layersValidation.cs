//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Terrain Paint Generator - Layer Validation Functions
//==============================================================================
function TEAutoPainter::reportValidationErrors(%this)
{
	if ($TEAutoPainter_LayerValidationErrors !$="")
		LabMsgOk(%layer.internalName SPC "validation failed",$TEAutoPainter_LayerValidationErrors);

	$TEAutoPainter_LayerValidationErrors = "";
}
//==============================================================================
// Validate all setting for a single layer
function TEAutoPainter::validateLayerObjSetting(%this,%layer,%doPostValidation)
{
	%validated = false;
	%layer.failedSettings = "";
	$TEAutoPainter_LayerValidationErrors = "";
	%this.validateLayerSetting("heightMin",%layer,true);
	%this.validateLayerSetting("heightMax",%layer,true);
	%this.validateLayerSetting("slopeMin",%layer,true);
	%this.validateLayerSetting("slopeMax",%layer,true);
	%this.validateLayerSetting("coverage",%layer,true);
	%this.reportValidationErrors();

	if (%doPostValidation && %layer.failedSettings $= "")
		%validated = %this.postLayerValidation(%layer);
}
//------------------------------------------------------------------------------
//==============================================================================
// Validate a single setting for a layer
function TEAutoPainter::validateLayerSetting(%this,%setting,%layer,%fullValidation)
{
	if (!isObject(%layer))
	{
		//%error = "Invalid layer submitted for validation. There's no such object:\c2" SPC %layer;
		//warnLog(%error,"Setting:",%setting);
		return;
		$TEAutoPainter_LayerValidationErrors = strAddRecord($TEAutoPainter_LayerValidationErrors,%error);
		%failed = true;
	}

	%layer.isValidated = false;
	%layer.setFieldValue(%setting,"");
	eval("%ctrl = %layer.pill-->"@%setting@";");

	if (!isObject(%ctrl))
	{
		%error = %layer.internalName SPC "layer doesn't have a valid field for:" SPC %setting @ ". Please delete it and report the issue.";
		$TEAutoPainter_LayerValidationErrors = strAddRecord($TEAutoPainter_LayerValidationErrors,%error);
		%failed = true;
	}
	else
	{
		%value = %ctrl.getValue();

		if (!strIsNumeric(%value))
		{
			%error = %setting SPC " is not a numeric value, please change it before being able to generate the layers";
			$TEAutoPainter_LayerValidationErrors = strAddRecord($TEAutoPainter_LayerValidationErrors,%error);
			%failed = true;
		}

		if (%value $= "")
		{
			%error = %setting SPC " is not a numeric value, please change it before being able to generate the layers";
			$TEAutoPainter_LayerValidationErrors = strAddRecord($TEAutoPainter_LayerValidationErrors,%error);
			%failed = true;
		}
	}

	if (%failed)
	{
		%layer.failedSettings = strAddWord(%layer.failedSettings,%setting);

		if (!%fullValidation)
			%this.reportValidationErrors();

		return;
	}

	switch$(%setting)
	{
		case "heightMin":
			if ($Lab::TerrainPainter::ValidateHeight)
			{
				if (%value < $TEAutoPainter_DefaultHeightMin)
					%value = $TEAutoPainter_DefaultHeightMin;
			}

		case "heightMax":
			if ($Lab::TerrainPainter::ValidateHeight)
			{
				if (%value < $TEAutoPainter_DefaultHeightMax)
					%value = $TEAutoPainter_DefaultHeightMax;
			}

		case "slopeMin":
			if (%value < 0)
				%value = "0";

		case "slopeMax":
			if (%value > 90)
				%value = "90";

		case "coverage":
			%value = mClamp(%value,"0","99");
	}

	%ctrl.setValue(%value);
	%layer.setFieldValue(%setting,%value);
}
//------------------------------------------------------------------------------
//==============================================================================
// Post validate the settings of a layer globally
function TEAutoPainter::postLayerValidation(%this,%layer)
{
	foreach$(%field in $TEAutoPainter_ValidateFields)
	{
		if (%layer.getFieldValue(%field) $="")
		{
			%error = %setting SPC "is not validated";
			%errors = strAddRecord(%errors,%error);
		}
	}

	if (%errors !$="")
	{
		LabMsgOk("Layer post validation failed",%errors);
		return false;
	}

	if (%layer.getFieldValue("heightMin") > %layer.getFieldValue("heightMax"))
	{
		%layer.setFieldValue("heightMin",%layer.getFieldValue("heightMax"));
		%report = "Height min was higher than Height max. The height minimum is changed to fit the maximum.";
		%reports = strAddRecord(%reports,%report);
	}

	if (%reports !$="")
	{
		LabMsgOk("Layer post validation succeed with warnings",%reports);
	}

	%layer.fieldsValidated = true;
	%layer.isValidated = true;
	return true;
}
//------------------------------------------------------------------------------
