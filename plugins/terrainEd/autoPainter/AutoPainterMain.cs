//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Terrain Paint Generator Globals Define
//------------------------------------------------------------------------------

$TEAutoPainter_ValidateFields = "heightMin heightMax slopeMin slopeMax coverage";
//------------------------------------------------------------------------------

//==============================================================================
// Initialize the Terrain Paint Generator
function TEAutoPainter::init(%this)
{
	if ( !isObject( TEAutoPainter_LayerGroup ) )
	{
		new SimGroup( TEAutoPainter_LayerGroup  );
	}

	TEAutoPainter_Window-->saveGroupButton.active = false;
	TEAutoPainter_StoredValues-->v1.setText("");
	TEAutoPainter_StoredValues-->v2.setText("");
	TEAutoPainter_StoredValues-->v3.setText("");
	TEAutoPainter_StoredValues-->v4.setText("");
	TEAutoPainter_StackLayers.clear();
	$TerrainAutoPaintGui_Initialized = true;
	TEAutoPainter_StepModeCheckbox.setStateOn(false);
}
//------------------------------------------------------------------------------
//==============================================================================
// Initialize the Terrain Paint Generator
function TEAutoPainter::exec(%this)
{
	exec("tlab/plugins/terrainEd/gui/TerrainAutoPaintGui.cs");
	exec("tlab/plugins/terrainEd/scripts/paintGenerator.cs");
}
//------------------------------------------------------------------------------
//==============================================================================
// Make sure the stack and group have valid object and are synced
function TEAutoPainter::checkLayersStackGroup(%this)
{
	foreach(%pill in TEAutoPainter_StackLayers)
	{
		if (isObject(%pill.layerObj))
			%addList = strAddWord(%addList,%pill.layerObj);
		else
			%badPill = strAddWord(%badPill,%pill);
	}

	foreach(%layer in TEAutoPainter_LayerGroup)
	{
		%inStack = strFind(%addList,%layer.getId());

		if (!strFind(%addList,%layer.getId()))
			%deleteLayers = strAddWord(%deleteLayers,%layer);
	}

	foreach$(%pill in %badPill)
		%pill.delete();

	foreach$(%layer in %deleteLayers)
		%layer.delete();
}
//------------------------------------------------------------------------------

