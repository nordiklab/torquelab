//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$TEAutoPainter_AutoStepGeneration = "1";
$TEAutoPainter_StepGenerationMode = "1";
//==============================================================================
// Terrain Paint Generator - Terrain Paint Generation Functions
//==============================================================================

//==============================================================================
// Generate all the layers in the layer group
function TEAutoPainter::generateLayerGroup(%this)
{
	//First make sure layers data is right
	TEAutoPainter.checkLayersStackGroup();

	foreach(%layer in TEAutoPainter_LayerGroup)
	{
		%layer.inactive = false;
		%layer.failedSettings = "";

		if ( !%layer.activeCtrl.isStateOn())
		{
			%layer.inactive = true;
			continue;
		}

		%layer.fieldsValidated = false;
		%validated = %this.validateLayerObjSetting(%layer,true);

		if (strFind(%layer.matInternalName,"*"))
			%layer.failedSettings = "Invalid material assigned to layer, please select a valid from the menu";

		if (%layer.failedSettings !$= "")
		{
			%validationgFailed = true;
			%badLayers = strAddRecord(%badLayers,%layer.internalName SPC "Bad fields:" SPC %layer.failedSettings);
		}
	}

	if (%validationgFailed)
	{
		if (%badLayers !$="" )
		{
			LabMsgOk("Generation aborted!","Some layers have fail the validation. Here's the list:\c2" SPC  %badLayers @ ". Please fix them before attempting generation again.");
		}

		return;
	}

	show(TEAutoPainter_GenerateProgressWindow);
	TEAutoPainter_GenerateLayerStack.clear();

	foreach(%ctrl in TEAutoPainter_StackLayers)
	{
		%layer = %ctrl.layerObj;

		if (%layer.inactive)
			continue;

		%heightMin = %layer.getFieldValue("heightMin");
		%heightMax = %layer.getFieldValue("heightMax");
		%slopeMin = %layer.getFieldValue("slopeMin");
		%slopeMax = %layer.getFieldValue("slopeMax");
		%coverage = %layer.getFieldValue("coverage");
		%pill = cloneObject(TEAutoPainter_GenerateInfoPill_v2,"",%layer.internalName,TEAutoPainter_GenerateLayerStack);
		%pill.internalName = %layer.internalName;
		%pill-->id.text = "#"@%layerId++ @ "-";
		%pill-->material.text = %layer.matInternalName;
		%pill-->slopeMinMax.setText(%slopeMin SPC "/" SPC %slopeMax);
		%pill-->heightMinMax.text = %heightMin SPC "/" SPC %heightMax;
		%pill-->coverage.text = %coverage;
		%pill-->duration.text = "pending";
	}

	if (%layerId < 1)
	{
		LabMsgOk("No active layers","There's no active layers to generate terrain materials, operation aborted");
		hide(TEAutoPainter_GenerateProgressWindow);
		return;
	}

	TEAutoPainter_GenerateProgressWindow-->cancelButton.visible = 1;
	TEAutoPainter_GenerateProgressWindow-->reportButton.text = "Start process";
	TEAutoPainter_GenerateProgressWindow-->reportButton.active = 1;
	$TEAutoPainter_GenerationStatus = "Pending";
	TEAutoPainter_GenerateProgressWindow-->reportText.text = %layerId SPC "layers  are ready to be processed";
	//LabMsgYesNo("Early development feature","The terrain pain generator is still in early development and can cause the engine to freeze. "@
//					"We recommend you to save your work before proceeding with automated painting. Are you sure you want to start the painting process?","TEAutoPainter.schedule(1000,\"startGeneration\");");
	//%this.schedule(1000,"startGeneration");
}
function TEAutoPainter::toggleStepMode(%this,%checkbox)
{
	return;

	if (%checkbox.isStateOn())
		$TEAutoPainter_AutoStepGeneration = "0";
	else
		$TEAutoPainter_AutoStepGeneration = "1";
}

//------------------------------------------------------------------------------
function TEAutoPainter_ReportButton::onClick(%this)
{
	switch$($TEAutoPainter_GenerationStatus)
	{
		case "Pending":
			%this.text = "Processing";
			%this.active = false;
			TEAutoPainter_GenerateProgressWindow-->cancelButton.visible = 0;
			TEAutoPainter.startGeneration();

		case "Completed":
			hide(TEAutoPainter_GenerateProgressWindow);
	}
}

//==============================================================================
// Start the generation process now that everything is validated
function TEAutoPainter::startGeneration(%this)
{
	if ($TEAutoPainter_Generating) return;

	TEAutoPainter.generationStartTime = $Sim::Time;
	$TEAutoPainter_Generating = true;
	TEAutoPainter.generatorSteps = "";

	foreach(%ctrl in TEAutoPainter_StackLayers)
	{
		%layer = %ctrl.layerObj;

		if (%layer.inactive)
			continue;

		TEAutoPainter.generatorSteps = strAddWord(TEAutoPainter.generatorSteps,%layer.getId());
	}

	$TEAutoPainter_Generating = false;
	//TEAutoPainter_GenerateProgressWindow.setVisible(false);
	%this.doGenerateLayerStep(200);
}
//------------------------------------------------------------------------------

function TEAutoPainter::doGenerateLayerStep(%this,%delay)
{
	%layer = getWord(TEAutoPainter.generatorSteps,0);

	if (%layer $= "")
		return;

	%pill = TEAutoPainter_GenerateLayerStack.findObjectByInternalName(%layer.internalName,true);
	%pill-->duration.text = "Processing";
	TEAutoPainter_GenerateProgressWindow-->reportText.text ="Processing layer:" SPC %layer.matInternalName @"." SPC getWordCount(TEAutoPainter.generatorSteps) SPC "left to process.";
	TEAutoPainter.generatorSteps = removeWord(TEAutoPainter.generatorSteps,0);
	$TEAutoPainter_LayerStartTime = $Sim::Time;
	%this.schedule(%delay,"generateLayer",%layer,true);
}
//==============================================================================
// Tell the engine to generate a layer with approved settings
function TEAutoPainter::generateLayer(%this,%layer,%stepMode)
{
	ETerrainEditor.paintIndex = %layer.matIndex;
	%heightMin = %layer.getFieldValue("heightMin");
	%heightMax = %layer.getFieldValue("heightMax");
	%slopeMin = %layer.getFieldValue("slopeMin");
	%slopeMax = %layer.getFieldValue("slopeMax");
	%coverage = %layer.getFieldValue("coverage");
	%terrain = ETerrainEditor.getActiveTerrain();
	%heightMin_w = getWord(%terrain.position,2) + %heightMin;
	%heightMax_w = getWord(%terrain.position,2) + %heightMax;

	if (%stepMode)
		info("Step Painting terrain with Mat Index",%layer.matIndex,"Name",%layer.matInternalName,"Height and Slope",%heightMin_w@"("@%heightMin@")", %heightMax_w@"("@%heightMax@")", %slopeMin, %slopeMax,"Coverage",%coverage);
	else
		info("Painting terrain with Mat Index",%layer.matIndex,"Name",%layer.matInternalName,"Height and Slope",%heightMin, %heightMax, %slopeMin, %slopeMax,"Coverage",%coverage);

	ETerrainEditor.autoMaterialLayer(%heightMin_w, %heightMax_w, %slopeMin, %slopeMax,%coverage);
	%this.generateLayerCompleted(%layer);
}
//------------------------------------------------------------------------------

//==============================================================================
// Tell the engine to generate a layer with approved settings
function TEAutoPainter::generateLayerCompleted(%this,%layer)
{
	TEAutoPainter.generationTotalTime = $Sim::Time - TEAutoPainter.generationStartTime;
	%layerTime = $Sim::Time - $TEAutoPainter_LayerStartTime;
	%pill = TEAutoPainter_GenerateLayerStack.findObjectByInternalName(%layer.internalName,true);
	%pill-->duration.text = mFloatLength(%layerTime,2) SPC "sec";
	//Get next layer step, if empty, process is completed
	%nextStep = getWord(TEAutoPainter.generatorSteps,0);

	if (%nextStep !$= "")
	{
		//Call next step now if we are in Auto Step Generation mode, else wait for next step confirmation
		if (!TEAutoPainter_StepModeCheckbox.isStateOn())
			TEAutoPainter.doGenerateLayerStep(200);
		else
			LabMsgYesNo(%layer.matInternalName SPC "step completed","Do you want to proceed with next step:" SPC getWord(TEAutoPainter.generatorSteps,0).matInternalName SPC "?","TEAutoPainter.doGenerateLayerStep(500);","TEAutoPainter.generateProcessCompleted();");
	}
	else
	{
		%this.generateProcessCompleted();
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Tell the engine to generate a layer with approved settings
function TEAutoPainter::generateProcessCompleted(%this)
{
	if (TEAutoPainter.generatorSteps !$= "")
		%result = "Process cancelled. ("@getWordCount(TEAutoPainter.generatorSteps)@" unprocessed).";
	else
		%result = "All layers have been processed.";

	TEAutoPainter.generatorSteps = "";
	$TEAutoPainter_GenerationStatus = "Completed";
	TEAutoPainter_GenerateProgressWindow-->reportText.text = %result SPC "Process time:\c1 " @ TEAutoPainter.generationTotalTime @ " sec";
	TEAutoPainter_GenerateProgressWindow-->reportButton.text = "Close report";
	TEAutoPainter_GenerateProgressWindow-->reportButton.active = 1;
}
//------------------------------------------------------------------------------
