//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
/*
%cfgArray.setVal("FIELD",       "DEFAULT" TAB "TEXT" TAB "TextEdit" TAB "" TAB "");
*/
//==============================================================================

function TerrainEdPlugin::initParamsArray( %this,%cfgArray )
{
	%cfgArray.group[%gid++] = "Action values";
	%cfgArray.setVal("maxBrushSize",       "40 40" TAB "maxBrushSize" TAB "TextEdit" TAB "" TAB "ETerrainEditor" TAB %gid);
	%cfgArray.setVal("BrushSize",       "10 10" TAB "brushSize" TAB "TextEdit" TAB "" TAB "TEP_BrushManager.validateBrushSize(*val*);" TAB %gid);
	%cfgArray.setVal("BrushType",       "box" TAB "brushType" TAB "TextEdit" TAB "" TAB "ETerrainEditor.setBrushType(*val*);" TAB %gid);
	%cfgArray.setVal("BrushPressure",       "1" TAB "brushPressure" TAB "TextEdit" TAB "" TAB "TEP_BrushManager.validateBrushPressure(*val*);" TAB %gid);
	%cfgArray.setVal("BrushSoftness",       "1" TAB "brushSoftness" TAB "TextEdit" TAB "" TAB "TEP_BrushManager.validateBrushSoftness(*val*);" TAB %gid);
	%cfgArray.setVal("BrushSetHeight",       "1" TAB "Brush set height" TAB "TextEdit" TAB "" TAB "TEP_BrushManager.validateBrushSetHeight(*val*);" TAB %gid);
	%cfgArray.setVal("BrushSetHeightRange",       "0 100" TAB "Brush set height range" TAB "TextEdit" TAB "" TAB "TEP_BrushManager.brushSetHeightRange(*val*);" TAB %gid);
	%cfgArray.group[%gid++] = "Action values";
	%cfgArray.setVal("adjustHeightVal",       "10" TAB "adjustHeightVal" TAB "TextEdit" TAB "" TAB "ETerrainEditor"  TAB %gid);
	%cfgArray.setVal("setHeightVal",       "100" TAB "setHeightVal" TAB "TextEdit" TAB "" TAB "ETerrainEditor" TAB %gid);
	%cfgArray.setVal("scaleVal",       "1" TAB "scaleVal" TAB "TextEdit" TAB "" TAB "ETerrainEditor" TAB %gid);
	%cfgArray.setVal("smoothFactor",       "0.1" TAB "smoothFactor" TAB "TextEdit" TAB "" TAB "ETerrainEditor" TAB %gid);
	%cfgArray.setVal("noiseFactor",       "1.0" TAB "noiseFactor" TAB "TextEdit" TAB "" TAB "ETerrainEditor" TAB %gid);
	%cfgArray.setVal("softSelectRadius",       "50" TAB "softSelectRadius" TAB "TextEdit" TAB "" TAB "ETerrainEditor" TAB %gid);
	%cfgArray.setVal("softSelectFilter",       "1.000000 0.833333 0.666667 0.500000 0.333333 0.166667 0.000000" TAB "softSelectFilter" TAB "TextEdit" TAB "" TAB "ETerrainEditor" TAB %gid);
	%cfgArray.setVal("softSelectDefaultFilter",       "1.000000 0.833333 0.666667 0.500000 0.333333 0.166667 0.000000" TAB "softSelectDefaultFilter" TAB "TextEdit" TAB "" TAB "ETerrainEditor" TAB %gid);
	%cfgArray.setVal("slopeMinAngle",       "0" TAB "slopeMinAngle" TAB "TextEdit" TAB "" TAB "ETerrainEditor.setSlopeLimitMinAngle(*val*);" TAB %gid);
	%cfgArray.setVal("slopeMaxAngle",       "90" TAB "slopeMaxAngle" TAB "TextEdit" TAB "" TAB "ETerrainEditor.setSlopeLimitMaxAngle(*val*);" TAB %gid);
	%cfgArray.group[%gid++] = "Default values";
	%cfgArray.setVal("DefaultBrushSize",       "8 8" TAB "Default brush size" TAB "TextEdit" TAB "" TAB "TerrainEdPlugin" TAB %gid);
	%cfgArray.setVal("DefaultBrushType",       "box" TAB "Default brush type" TAB "TextEdit" TAB "" TAB "TerrainEdPlugin" TAB %gid);
	%cfgArray.setVal("DefaultBrushPressure",       "43" TAB "Default brush pressure" TAB "TextEdit" TAB "" TAB "TerrainEdPlugin" TAB %gid);
	%cfgArray.setVal("DefaultBrushSoftness",       "43" TAB "Default brush softness" TAB "TextEdit" TAB "" TAB "TerrainEdPlugin" TAB %gid);
	%cfgArray.setVal("DefaultBrushSetHeight",       "100" TAB "Default brush set height" TAB "TextEdit" TAB "" TAB "TerrainEdPlugin" TAB %gid);
}

function TerrainEdPlugin::initBinds( %this )
{
	//%map = new ActionMap();
	%map = Lab.addPluginMap("TerrainEd");
	%map.bindCmd( keyboard, "lalt", "TerrainManager.getCurrentHeight();", "" );// +1 Brush Size
	%map.bindCmd( keyboard, "1", "LabPalettes.pushButton(brushAdjustHeight);", "" );    //Grab Terrain
	%map.bindCmd( keyboard, "2", "LabPalettes.pushButton(raiseHeight);", "" );     // Raise Height
	%map.bindCmd( keyboard, "3", "LabPalettes.pushButton(lowerHeight);", "" );     // Lower Height
	%map.bindCmd( keyboard, "4", "LabPalettes.pushButton(smoothHeight);", "" );    // Average Height
	%map.bindCmd( keyboard, "5", "LabPalettes.pushButton(smoothSlope);", "" );    // Smooth Slope
	%map.bindCmd( keyboard, "6", "LabPalettes.pushButton(paintNoise);", "" );      // Noise
	%map.bindCmd( keyboard, "7", "LabPalettes.pushButton(flattenHeight);", "" );   // Flatten
	%map.bindCmd( keyboard, "8", "LabPalettes.pushButton(setHeight);", "" );       // Set Height
	%map.bindCmd( keyboard, "9", "LabPalettes.pushButton(setEmpty);", "" );    // Clear Terrain
	%map.bindCmd( keyboard, "0", "LabPalettes.pushButton(clearEmpty);", "" );  // Restore Terrain
	%map.bindCmd( keyboard, "v", "EWTerrainEditToolbarBrushType->ellipse);", "" );// Circle Brush
	%map.bindCmd( keyboard, "b", "EWTerrainEditToolbarBrushType->box);", "" );// Box Brush
	%map.bindCmd( keyboard, "=", "TEP_BrushManager.keyboardModifyBrushSize(1);", "" );// +1 Brush Size
	%map.bindCmd( keyboard, "+", "TEP_BrushManager.keyboardModifyBrushSize(1);", "" );// +1 Brush Size
	%map.bindCmd( keyboard, "-", "TEP_BrushManager.keyboardModifyBrushSize(-1);", "" );// -1 Brush Size
	%map.bindCmd( keyboard, "[", "TEP_BrushManager.keyboardModifyBrushSize(-5);", "" );// -5 Brush Size
	%map.bindCmd( keyboard, "]", "TEP_BrushManager.keyboardModifyBrushSize(5);", "" );// +5 Brush Size
	//%map.bindCmd( keyboard, "]", "TerrainBrushPressureTextEditContainer->textEdit.text += 5", "" );// +5 Pressure
	//%map.bindCmd( keyboard, "[", "TerrainBrushPressureTextEditContainer->textEdit.text -= 5", "" );// -5 Pressure
	//%map.bindCmd( keyboard, "'", "TerrainBrushSoftnessTextEditContainer->textEdit.text += 5", "" );// +5 Softness
	//%map.bindCmd( keyboard, ";", "TerrainBrushSoftnessTextEditContainer->textEdit.text -= 5", "" );// -5 Softness
	//TerrainEdPlugin.map = %map;
}

//------------------------------------------------------------------------------
// TerrainEdPlugin
//------------------------------------------------------------------------------
//function TerrainEdPlugin::onPluginCreated( %this){}
function TerrainEdPlugin::onPluginLoaded( %this )
{
	ETerrainEditor.isDirty = false;
	ETerrainEditor.isMissionDirty = false;
	ETerrainEditor.init();
	%this.initBinds();
	Lab.terrainMenu = new PopupMenu()
	{
		superClass = "MenuBuilder";
		barTitle = "Terrain";
		item[0] = "Smooth Heightmap" TAB "" TAB "ETerrainEditor.onSmoothHeightmap();";
	};
	TerrainEdPlugin.setParam("BrushSize",TerrainEdPlugin.defaultBrushSize);
	TerrainEdPlugin.setParam("BrushPressure",TerrainEdPlugin.defaultBrushPressure);
	TerrainEdPlugin.setParam("BrushSoftness",TerrainEdPlugin.defaultBrushSoftness);
	TerrainEdPlugin.setParam("BrushSetHeight",TerrainEdPlugin.defaultBrushSetHeight);
	TEP_BrushManager.init();
	
	//Cleanup some possible wrong images stored in GUIs
	EPainter.reset4MatPreview();
}

function TerrainEdPlugin::onActivated( %this )
{
	Parent::onActivated( %this );
	ETerrainEditor.switchAction( %action );
	EWTerrainEditToolbar-->ellipse.performClick(); // Circle Brush
	//Lab.menuBar.insert( , Lab.menuBar.dynamicItemInsertPos );
// Add our menu.
	TerrainEditorTools.visible = 1;
	EPainter.setDisplayModes();
	//Lab.insertWorldMenu(Lab.terrainMenu);
	EditorGui.bringToFront( ETerrainEditor );
	ETerrainEditor.setVisible( true );
	ETerrainEditor.attachTerrain();
	ETerrainEditor.makeFirstResponder( true );
	EWTerrainEditToolbar.setVisible( true );
	ETerrainEditor.onBrushChanged();
	//return;
	ETerrainEditor.setup();

	EditorGuiStatusBar.setSelection("");
}

function TerrainEdPlugin::onDeactivated( %this )
{
	Parent::onDeactivated( %this );

	EWTerrainEditToolbar.setVisible( false );
	ETerrainEditor.setVisible( false );
	//Lab.removeWorldMenu(Lab.terrainMenu);
}

function TerrainEdPlugin::handlePaletteClick(%this,%mode)
{
	ETerrainEditor.switchAction( %mode );
}
