
new SimGroup(TEAutoPainter_LayerGroup_Saved)
{
	canSave = "1";
	canSaveDynamicFields = "1";

	new ScriptObject()
	{
		internalName = "Layer_132";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "103598";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "1000";
		heightMin = "0";
		Inactive = "0";
		isValidated = "1";
		matIndex = "2";
		matInternalName = "ter_forest_mixed_01";
		matObject = "67335";
		pill = "103596";
		slopeMax = "17";
		slopeMin = "0";
		useTerrainHeight = "0";
	};
	new ScriptObject()
	{
		internalName = "Layer_185";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "103625";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "1000";
		heightMin = "0";
		Inactive = "0";
		isValidated = "1";
		matIndex = "0";
		matInternalName = "ter_sand_dirty_01";
		matObject = "67364";
		pill = "103623";
		slopeMax = "29";
		slopeMin = "17";
	};
	new ScriptObject()
	{
		internalName = "Layer_186";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "103652";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "1000";
		heightMin = "0";
		Inactive = "0";
		isValidated = "1";
		matIndex = "4";
		matInternalName = "ter_rock_rough_02";
		matObject = "67350";
		pill = "103650";
		slopeMax = "90";
		slopeMin = "29";
	};
	new ScriptObject()
	{
		internalName = "layer_1";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "103770";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "1000";
		heightMin = "0";
		Inactive = "0";
		isValidated = "1";
		matIndex = "5";
		matInternalName = "ter_rock_rough_01_side";
		matObject = "67349";
		pill = "103768";
		slopeMax = "90";
		slopeMin = "40";
	};
	new ScriptObject()
	{
		internalName = "layer_2";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "103801";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "600";
		heightMin = "450";
		Inactive = "0";
		isValidated = "0";
		matIndex = "3";
		matInternalName = "ter_dirt_gravel_05";
		matObject = "67332";
		pill = "103799";
		slopeMax = "4";
		slopeMin = "0";
	};
};
