
new SimGroup(TEAutoPainter_LayerGroup_Saved)
{
	canSave = "1";
	canSaveDynamicFields = "1";

	new ScriptObject()
	{
		internalName = "layer_1";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "78705";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "1000";
		heightMin = "0";
		Inactive = "0";
		isValidated = "1";
		matIndex = "0";
		matInternalName = "ter_grass_lush_01";
		matObject = "57744";
		pill = "78703";
		slopeMax = "90";
		slopeMin = "0";
	};
	new ScriptObject()
	{
		internalName = "layer_2";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "78749";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "1000";
		heightMin = "0";
		Inactive = "0";
		isValidated = "1";
		matIndex = "4";
		matInternalName = "ter_slope_rubble_01";
		matObject = "57777";
		pill = "78747";
		slopeMax = "26";
		slopeMin = "11";
	};
	new ScriptObject()
	{
		internalName = "layer_3";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "78777";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "1000";
		heightMin = "0";
		Inactive = "0";
		isValidated = "1";
		matIndex = "2";
		matInternalName = "ter_rubble_waste_01";
		matObject = "57767";
		pill = "78775";
		slopeMax = "11";
		slopeMin = "0";
	};
	new ScriptObject()
	{
		internalName = "layer_4";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "78805";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "1000";
		heightMin = "0";
		Inactive = "0";
		isValidated = "1";
		matIndex = "3";
		matInternalName = "ter_rock_plain_01";
		matObject = "57763";
		pill = "78803";
		slopeMax = "90";
		slopeMin = "26";
	};
	new ScriptObject()
	{
		internalName = "layer_5";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "78845";
		coverage = "45";
		fieldsValidated = "1";
		heightMax = "1000";
		heightMin = "0";
		Inactive = "0";
		isValidated = "1";
		matIndex = "4";
		matInternalName = "ter_slope_rubble_01";
		matObject = "57777";
		pill = "78843";
		slopeMax = "33";
		slopeMin = "26";
	};
	new ScriptObject()
	{
		internalName = "layer_6";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "78873";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "100";
		heightMin = "0";
		Inactive = "0";
		isValidated = "0";
		matIndex = "1";
		matInternalName = "ter_gravel_plain_01";
		matObject = "57748";
		pill = "78871";
		slopeMax = "45";
		slopeMin = "8";
	};
};
