
new SimGroup(TEAutoPainter_LayerGroup_Saved)
{
	canSave = "1";
	canSaveDynamicFields = "1";

	new ScriptObject()
	{
		internalName = "layer_1";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "32611";
		coverage = "80";
		fieldsValidated = "1";
		heightMax = "550";
		heightMin = "300";
		Inactive = "1";
		isValidated = "0";
		matIndex = "4";
		matInternalName = "ow_DirtyMixA1";
		matObject = "17603";
		pill = "32609";
		slopeMax = "28";
		slopeMin = "6";
	};
	new ScriptObject()
	{
		internalName = "layer_2";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "32655";
		coverage = "50";
		fieldsValidated = "1";
		heightMax = "534";
		heightMin = "330";
		Inactive = "1";
		isValidated = "0";
		matIndex = "3";
		matInternalName = "ow_XeresRockSharpA";
		matObject = "17612";
		pill = "32653";
		slopeMax = "34";
		slopeMin = "22";
	};
	new ScriptObject()
	{
		internalName = "layer_3";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "32712";
		coverage = "80";
		fieldsValidated = "1";
		heightMax = "550";
		heightMin = "300";
		Inactive = "1";
		isValidated = "0";
		matIndex = "4";
		matInternalName = "ow_DirtyMixA1";
		matObject = "17603";
		pill = "32710";
		slopeMax = "28";
		slopeMin = "6";
	};
	new ScriptObject()
	{
		internalName = "layer_4";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "32756";
		coverage = "45";
		fieldsValidated = "1";
		heightMax = "520";
		heightMin = "380";
		Inactive = "1";
		isValidated = "1";
		matIndex = "0";
		matInternalName = "ow_XeresDryA";
		matObject = "17609";
		pill = "32754";
		slopeMax = "29";
		slopeMin = "12";
	};
	new ScriptObject()
	{
		internalName = "layer_5";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "32800";
		coverage = "77";
		fieldsValidated = "1";
		heightMax = "550";
		heightMin = "440";
		Inactive = "0";
		isValidated = "0";
		matIndex = "5";
		matInternalName = "ow_BrownCliffA_V";
		matObject = "17614";
		pill = "32798";
		slopeMax = "90";
		slopeMin = "32";
	};
};
