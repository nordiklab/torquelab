
new SimGroup(TEAutoPainter_LayerGroup_Saved)
{
	canSave = "1";
	canSaveDynamicFields = "1";

	new ScriptObject()
	{
		internalName = "layer_1";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "40968";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "100";
		heightMin = "0";
		Inactive = "0";
		isValidated = "1";
		matIndex = "0";
		matInternalName = "tn_SandSoft";
		matObject = "17683";
		pill = "40966";
		slopeMax = "8.5";
		slopeMin = "0";
	};
	new ScriptObject()
	{
		internalName = "layer_2";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "40996";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "100";
		heightMin = "0";
		Inactive = "0";
		isValidated = "1";
		matIndex = "3";
		matInternalName = "ow_XeresDryA";
		matObject = "17666";
		pill = "40994";
		slopeMax = "23.5";
		slopeMin = "8.5";
	};
	new ScriptObject()
	{
		internalName = "layer_3";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "41024";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "100";
		heightMin = "0";
		Inactive = "0";
		isValidated = "1";
		matIndex = "1";
		matInternalName = "tn_RockNobel";
		matObject = "17677";
		pill = "41022";
		slopeMax = "40";
		slopeMin = "23.5";
	};
	new ScriptObject()
	{
		internalName = "layer_4";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "41052";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "100";
		heightMin = "0";
		Inactive = "0";
		isValidated = "1";
		matIndex = "2";
		matInternalName = "ow_BrownCliffA_V";
		matObject = "17671";
		pill = "41050";
		slopeMax = "90";
		slopeMin = "40";
	};
};
