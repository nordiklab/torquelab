
new SimGroup(TEAutoPainter_LayerGroup_Saved)
{
	canSave = "1";
	canSaveDynamicFields = "1";

	new ScriptObject()
	{
		internalName = "Layer_185";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "49522";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "60";
		heightMin = "0";
		Inactive = "0";
		isValidated = "1";
		matIndex = "1";
		matInternalName = "tw_GrassA01";
		matObject = "21752";
		pill = "49520";
		slopeMax = "9";
		slopeMin = "0";
	};
	new ScriptObject()
	{
		internalName = "Layer_186";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "49552";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "60";
		heightMin = "0";
		Inactive = "0";
		isValidated = "1";
		matIndex = "2";
		matInternalName = "tw_GrassMeadowA01";
		matObject = "21754";
		pill = "49550";
		slopeMax = "18";
		slopeMin = "9";
	};
	new ScriptObject()
	{
		internalName = "Layer_187";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "49582";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "60";
		heightMin = "0";
		Inactive = "0";
		isValidated = "1";
		matIndex = "0";
		matInternalName = "tw_SandA01";
		matObject = "21766";
		pill = "49580";
		slopeMax = "26";
		slopeMin = "18";
	};
	new ScriptObject()
	{
		internalName = "Layer_188";
		canSave = "1";
		canSaveDynamicFields = "1";
		activeCtrl = "49618";
		coverage = "99";
		fieldsValidated = "1";
		heightMax = "60";
		heightMin = "0";
		Inactive = "0";
		isValidated = "0";
		matIndex = "3";
		matInternalName = "tw_RockA01";
		matObject = "21756";
		pill = "49616";
		slopeMax = "90";
		slopeMin = "26";
	};
};
