//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function RiverInspector::inspect( %this, %obj )
{
	%name = "";

	if ( isObject( %obj ) )
		%name = %obj.getName();
	else
		RiverFieldInfoControl.setText( "" );

	//RiverInspectorNameEdit.setValue( %name );
	Parent::inspect( %this, %obj );
}

function RiverInspector::onInspectorFieldModified( %this, %object, %fieldName, %arrayIndex, %oldValue, %newValue )
{
	// Same work to do as for the regular WorldEditor Inspector.
	Inspector::onInspectorFieldModified( %this, %object, %fieldName, %arrayIndex, %oldValue, %newValue );
}

function RiverInspector::onFieldSelected( %this, %fieldName, %fieldTypeStr, %fieldDoc )
{
	RiverFieldInfoControl.setText( "<font:ArialBold:14>" @ %fieldName @ "<font:ArialItalic:14> (" @ %fieldTypeStr @ ") " NL "<font:Arial:14>" @ %fieldDoc );
}

