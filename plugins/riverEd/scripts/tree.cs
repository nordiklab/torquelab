//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Manage selected Road Nodes Data
//==============================================================================
function RiverTreeView::onInspect(%this, %obj)
{
	RiverInspector.inspect(%obj);
}
