//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function RiverEditor::create( %this )
{
 
 info("Plugin","->","Initializing","River Editor");
	execRiverEd(true);
	// Add ourselves to EditorGui, where all the other tools reside
	//Lab.createPlugin("RiverEditor","River Editor");
	Lab.addPluginEditor("RiverEditor",RiverEditorGui);
	Lab.addPluginGui("RiverEditor",RiverEditorTools);
	Lab.addPluginToolbar("RiverEditor",RiverEditorToolbar);
	Lab.addPluginPalette("RiverEditor",   RiverEditorPaletteStack);
	Lab.addPluginDlg("RiverEditor",RiverEditorDialogs);
	RiverEditorPlugin.editorGui = RiverEditorGui;
	$RiverEd = newScriptObject("RiverEd");
	%map = Lab.addPluginMap("RiverEditor");
	//%map = new ActionMap();
	%map.bindCmd( keyboard, "backspace", "RiverEditorGui.deleteNode();", "" );
	%map.bindCmd( keyboard, "1", "RiverEditorGui.prepSelectionMode();", "" );
	%map.bindCmd( keyboard, "2", "LabPalettes.pushButton(Move);", "" );
	%map.bindCmd( keyboard, "3", "LabPalettes.pushButton(Rotate);", "" );
	%map.bindCmd( keyboard, "4", "LabPalettes.pushButton(Scale);","" );
	%map.bindCmd( keyboard, "5", "LabPalettes.pushButton(AddRiver);", "" );
	%map.bindCmd( keyboard, "=", "LabPalettes.pushButton(InsertPoint);", "" );
	%map.bindCmd( keyboard, "numpadadd", "LabPalettes.pushButton(InsertPoint);", "" );
	%map.bindCmd( keyboard, "-", "LabPalettes.pushButton(RemovePoint);", "" );
	%map.bindCmd( keyboard, "numpadminus", "LabPalettes.pushButton(RemovePoint);", "" );

	%map.bindCmd( keyboard, "z", "RiverEditorShowSplineBtn.performClick();", "" );
	%map.bindCmd( keyboard, "x", "RiverEditorWireframeBtn.performClick();", "" );
	%map.bindCmd( keyboard, "v", "RiverEditorShowRoadBtn.performClick();", "" );
	RiverEditorPlugin.map = %map;
	$RiverManager = newScriptObject("RiverManager");
}

//------------------------------------------------------------------------------

function RiverEditor::destroy( %this )
{
}

//----------------------------------------------------------------
$TLab_PluginName_RiverEditor = "River Editor";

function execRiverEd(%loadGui)
{
	if (%loadGui)
	{
		exec( "tlab/plugins/riverEditor/gui/riverEditorGui.gui" );
		exec( "tlab/plugins/riverEditor/gui/RiverEditorTools.gui" );
		exec( "tlab/plugins/riverEditor/gui/riverEditorToolbar.gui" );
		exec( "tlab/plugins/riverEditor/gui/riverEditorPaletteGui.gui" );
		exec( "tlab/plugins/riverEditor/gui/riverEditorDialogs.gui" );
	}

	exec( "tlab/plugins/riverEditor/riverEditorGui.cs" );
	exec( "tlab/plugins/riverEditor/RiverEditorPlugin.cs" );
	execPattern("tlab/plugins/riverEditor/scripts/*.cs");
	execPattern("tlab/plugins/riverEditor/nodeManager/*.cs");
}

function destroyRiverEditor()
{
}

