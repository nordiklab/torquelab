//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
$MeshRoad::wireframe = true;
$MeshRoad::showSpline = true;
$MeshRoad::showReflectPlane = false;
$MeshRoad::showRoad = true;
$MeshRoad::breakAngle = 3.0;

//==============================================================================
function MeshRoadEditorGui::prepSelectionMode( %this )
{
	%mode = %this.getMode();

	if ( %mode $= "MeshRoadEditorAddNodeMode"  )
	{
		if ( isObject( %this.getSelectedRoad() ) )
			%this.deleteNode();
	}

	%this.setMode( "MeshRoadEditorSelectMode" );
	LabPalettes-->Select.setStateOn(1);
}
//------------------------------------------------------------------------------

//==============================================================================
function MeshRoadDefaultWidthSliderCtrlContainer::onWake(%this)
{
	MeshRoadDefaultWidthSliderCtrlContainer-->slider.setValue(MeshRoadDefaultWidthTextEditContainer-->textEdit.getText());
}
//------------------------------------------------------------------------------
//==============================================================================
function MeshRoadDefaultDepthSliderCtrlContainer::onWake(%this)
{
	MeshRoadDefaultDepthSliderCtrlContainer-->slider.setValue(MeshRoadDefaultDepthTextEditContainer-->textEdit.getText());
}
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function MeshRoadEditorGui::setDefaultTopMaterial(%this,%matName)
{
	MeshRoadEditorGui.setFieldValue("topMaterialName",%matName);
}
//------------------------------------------------------------------------------
//==============================================================================
function MeshRoadEditorGui::setDefaultBottomMaterial(%this,%matName)
{
	MeshRoadEditorGui.setFieldValue("bottomMaterialName",%matName);
}
//------------------------------------------------------------------------------
//==============================================================================
function MeshRoadEditorGui::setDefaultSideMaterial(%this,%matName)
{
	MeshRoadEditorGui.setFieldValue("sideMaterialName",%matName);
}
//------------------------------------------------------------------------------
/*
//==============================================================================
function EMeshRoadEditorSelectModeBtn::onClick(%this) {
	EditorGuiStatusBar.setInfo(%this.ToolTip);
}
//------------------------------------------------------------------------------
//==============================================================================
function EMeshRoadEditorAddModeBtn::onClick(%this) {
	EditorGuiStatusBar.setInfo(%this.ToolTip);
}
//------------------------------------------------------------------------------
//==============================================================================
function EMeshRoadEditorMoveModeBtn::onClick(%this) {
	EditorGuiStatusBar.setInfo(%this.ToolTip);
}
//------------------------------------------------------------------------------
//==============================================================================
function EMeshRoadEditorRotateModeBtn::onClick(%this) {
	EditorGuiStatusBar.setInfo(%this.ToolTip);
}
//------------------------------------------------------------------------------
//==============================================================================
function EMeshRoadEditorScaleModeBtn::onClick(%this) {
   Parent::onClick(%this);
	EditorGuiStatusBar.setInfo(%this.ToolTip);
}
//------------------------------------------------------------------------------
//==============================================================================
function EMeshRoadEditorInsertModeBtn::onClick(%this) {
	EditorGuiStatusBar.setInfo(%this.ToolTip);
}
//------------------------------------------------------------------------------
//==============================================================================
function EMeshRoadEditorRemoveModeBtn::onClick(%this) {
	EditorGuiStatusBar.setInfo(%this.ToolTip);
}
//------------------------------------------------------------------------------
*/
