//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function MeshRoadEditorPlugin::onPluginLoaded( %this )
{
	// Add ourselves to the Editor Settings window
}
//------------------------------------------------------------------------------
//==============================================================================
function MeshRoadEditorPlugin::onActivated( %this )
{

	LabPalettes.pushButton(AddRoad);
	EditorGui.bringToFront( MeshRoadEditorGui );
	MeshRoadEditorGui.makeFirstResponder( true );
	MeshRoadTreeView.open(ServerMeshRoadSet,true);
	// Store this on a dynamic field
	// in order to restore whatever setting
	// the user had before.
	%this.prevGizmoAlignment = GlobalGizmoProfile.alignment;
	// The DecalEditor always uses Object alignment.
	//GlobalGizmoProfile.alignment = "Object";
	Lab.setGizmoAlignment("Object");
	// Set the status bar here until all tool have been hooked up
	EditorGuiStatusBar.setInfo("Mesh road editor.");
	EditorGuiStatusBar.setSelection("");
	MRoadManager.Init();
	Parent::onActivated(%this);
	MeshRoad_TabBook.selectPage(0);
}
//------------------------------------------------------------------------------
//==============================================================================
function MeshRoadEditorPlugin::onDeactivated( %this )
{
	// Restore the previous Gizmo
	// alignment settings.
	Lab.setGizmoAlignment(%this.prevGizmoAlignment);
	//GlobalGizmoProfile.alignment = %this.prevGizmoAlignment;
	Parent::onDeactivated(%this);
}
//------------------------------------------------------------------------------
//==============================================================================
function MeshRoadEditorPlugin::onEditMenuSelect( %this, %editMenu )
{
	%hasSelection = false;

	if ( isObject( MeshRoadEditorGui.road ) )
		%hasSelection = true;

	%editMenu.enableItem( 3, false ); // Cut
	%editMenu.enableItem( 4, false ); // Copy
	%editMenu.enableItem( 5, false ); // Paste
	%editMenu.enableItem( 6, %hasSelection ); // Delete
	%editMenu.enableItem( 8, false ); // Deselect
}
//------------------------------------------------------------------------------
//==============================================================================
function MeshRoadEditorPlugin::handleDelete( %this )
{
	MeshRoadEditorGui.deleteNode();
}
//------------------------------------------------------------------------------
//==============================================================================
function MeshRoadEditorPlugin::handleEscape( %this )
{
	return MeshRoadEditorGui.onEscapePressed();
}
//------------------------------------------------------------------------------
//==============================================================================
function MeshRoadEditorPlugin::isDirty( %this )
{
	return MeshRoadEditorGui.isDirty;
}
//------------------------------------------------------------------------------
//==============================================================================
function MeshRoadEditorPlugin::onSaveMission( %this, %missionFile )
{
	if ( MeshRoadEditorGui.isDirty )
	{
		MissionGroup.save( %missionFile );
		MeshRoadEditorGui.isDirty = false;
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function MeshRoadEditorPlugin::handlePaletteClick( %this,%mode )
{

	switch$(%mode)
	{
		case "Select":
			MeshRoadEditorGui.prepSelectionMode();

		case "move":
			MeshRoadEditorGui.setMode("MeshRoadEditorMoveMode");
			EditorGuiStatusBar.setInfo("Move selection.  SHIFT while dragging duplicates objects.  " @ %cmdCtrl @ " to toggle soft snap.  ALT to toggle grid snap.");

		case "scale":
			MeshRoadEditorGui.setMode("MeshRoadEditorScaleMode");
			EditorGuiStatusBar.setInfo("Scagfgle selection.");

		case "AddRoad":
			MeshRoadEditorGui.setMode("MeshRoadEditorAddRoadMode");

		case "InsertNode":
			MeshRoadEditorGui.setMode("MeshRoadEditorInsertPointMode");

		case "RemoveNode":
			MeshRoadEditorGui.setMode("MeshRoadEditorRemovePointMode");

	}

}
