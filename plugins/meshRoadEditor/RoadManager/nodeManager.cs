//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function MRoadManager::updateRoadData(%this)
{
	%road = MRoadManager.currentRoad;

	if (!isObject(%road))
		return;

	%this.noNodeUpdate = true;

	if (MRoadManager.nodeListModeId $= "")
		MRoadManager.nodeListModeId = "0";

	%id = 0;

   %count = MeshRoadEditorGui.getNodeCount();
	while( %id < %count )
	{
		MeshRoadEditorGui.setSelectedNode(%id);
		%nodeWidth = MeshRoadEditorGui.getNodeWidth();

		if (%nodeWidth <= 0|| %nodeWidth $= "" || %nodeWidth $= "-1" )
			break;

		%nodePos = MeshRoadEditorGui.getNodePosition();
		%nodeDepth = MeshRoadEditorGui.getNodeDepth();
		%nodeNormal = MeshRoadEditorGui.getNodeNormal();
		MRoadManager.nodeData[%id] = %nodePos TAB %nodeNormal TAB %nodeWidth TAB %nodeDepth;
		%id++;
	}

	%this.updateNodeStack();
	
	return;
	%this.noNodeUpdate = false;
	%node = MeshRoadEditorGui.getSelectedNode();

	if (%node $= "-1" || %node $= "" || %node >= MeshRoadEditorGui.getNodeCount())
		MRoadManager.selectNode(0);
	else
		MRoadManager.onNodeSelected(%node);

	MeshRoadEditorOptionsWindow-->topMaterial.setText(%road.topMaterial);
	MeshRoadEditorOptionsWindow-->bottomMaterial.setText(%road.bottomMaterial);
	MeshRoadEditorOptionsWindow-->sideMaterial.setText(%road.sideMaterial);
}
//==============================================================================
// Select Node
//==============================================================================

//------------------------------------------------------------------------------
$NoNodeUpd = true;
function MRoadManager::selectNode(%this,%nodeId,%noUpdate)
{
	if (!isObject(MeshRoadEditorGui.road))
		return;

	%this.noNodeUpdate = %noUpdate;
	MeshRoadEditorGui.setSelectedNode(%nodeId);
}
//------------------------------------------------------------------------------

//==============================================================================
function MRoadManager::onNodeSelected(%this,%nodeId)
{
	if (%this.noNodeUpdate)
	{
		return;
	}

	if (%nodeId $= "-1" || %nodeId $= "")
	{
		MRoadManager.selectNode(0);
		return;
	}

	foreach(%ctrl in MREP_NodePillStack)
	{
		%active = false;

		if (%ctrl.nodeId $= %nodeId)
			%active = true;

		%ctrl-->buttonNode.setStateOn(%active);

		if (!isObject(%ctrl-->NodeDataStack))
			continue;

		if (MRoadManager.autoCollapsePill)
			%ctrl-->NodeDataStack.visible = %active;
		else
			%ctrl-->NodeDataStack.visible = 1;
	}
}
//------------------------------------------------------------------------------
function MRoadManager::getNodeCount(%this)
{
	if (!isObject( %this.currentRoad))
	   return -1;
   
   return MeshRoadEditorGui.getNodeCount();
}
