//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//------------------------------------------------------------------------------

function MeshRoadEditor::create( %this )
{
   
  info("Plugin","->","Initializing","Mesh Road Editor plugin" );
	$MRoadManager = newScriptObject("MRoadManager");
	execMREP(true);

	Lab.addPluginEditor("MeshRoadEditor",MeshRoadEditorGui);
	Lab.addPluginGui("MeshRoadEditor",   MeshRoadEditorTools);
	Lab.addPluginToolbar("MeshRoadEditor",MeshRoadEditorToolbar);
	Lab.addPluginPalette("MeshRoadEditor",   MeshRoadEditorPaletteStack);
	MeshRoadEditorPlugin.editorGui = MeshRoadEditorGui;
	%map = Lab.addPluginMap("MeshRoadEditor");
	//%map = new ActionMap();
	%map.bindCmd( keyboard, "backspace", "MeshRoadEditorGui.deleteNode();", "" );
	%map.bindCmd( keyboard, "1", "MeshRoadEditorGui.prepSelectionMode();", "" );
	%map.bindCmd( keyboard, "2", "LabPalettes.pushButton(Move);", "" );
	%map.bindCmd( keyboard, "3", "LabPalettes.pushButton(Rotate);", "" );
	%map.bindCmd( keyboard, "4", "LabPalettes.pushButton(Scale);","" );
	%map.bindCmd( keyboard, "5", "LabPalettes.pushButton(AddRoad);", "" );
	%map.bindCmd( keyboard, "=", "LabPalettes.pushButton(InsertPoint);", "" );
	%map.bindCmd( keyboard, "numpadadd", "LabPalettes.pushButton(InsertPoint);", "" );
	%map.bindCmd( keyboard, "-", "LabPalettes.pushButton(RemovePoint);", "" );
	%map.bindCmd( keyboard, "numpadminus", "LabPalettes.pushButton(RemovePoint);", "" );
	%map.bindCmd( keyboard, "z", "MeshRoadEditorShowSplineBtn.performClick();", "" );
	%map.bindCmd( keyboard, "x", "MeshRoadEditorWireframeBtn.performClick();", "" );
	%map.bindCmd( keyboard, "v", "MeshRoadEditorShowRoadBtn.performClick();", "" );
	MeshRoadEditorPlugin.map = %map;
}

//------------------------------------------------------------------------------

function MeshRoadEditor::destroy( %this )
{
  
}

//----------------------------------------------------------------
$TLab_PluginName_["MeshRoadEditor"] = "Mesh Road Editor";
singleton GuiControlProfile( MeshRoadEditorProfile )
{
	canKeyFocus = true;
	opaque = true;
	fillColor = "192 192 192 192";
	category = "Editor";
};

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//==============================================================================
function execMREP(%loadGui)
{
	//----------------------------------------------
	// Terrain Editor GUIs
	if (%loadGui)
	{
		exec( "tlab/plugins/meshRoadEditor/meshRoadEditor.cs" );
		exec( "tlab/plugins/meshRoadEditor/gui/meshRoadEditorGui.gui" );
		exec( "tlab/plugins/meshRoadEditor/gui/MeshRoadEditorTools.gui" );
		exec( "tlab/plugins/meshRoadEditor/gui/meshRoadEditorToolbar.gui");
		exec( "tlab/plugins/meshRoadEditor/gui/meshRoadEditorPaletteGui.gui");
	}

	exec( "tlab/plugins/meshRoadEditor/meshRoadEditorGui.cs" );
	exec( "tlab/plugins/meshRoadEditor/MeshRoadEditorPlugin.cs" );
	exec( "tlab/plugins/meshRoadEditor/MeshRoadEditorParams.cs" );
	execPattern( "tlab/plugins/meshRoadEditor/RoadManager/*.cs" );
	execPattern( "tlab/plugins/meshRoadEditor/Editor/*.cs" );
}
//------------------------------------------------------------------------------
//==============================================================================
function destroyMeshRoadEditor()
{
}
