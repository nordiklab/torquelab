//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// MeshRoad TreeViewCtrl
//==============================================================================

//==============================================================================
function MeshRoadTreeView::onSelect(%this, %obj)
{
	MeshRoadEditorGui.road = %obj;
	MeshRoadInspector.inspect( %obj );

	if (%obj != MeshRoadEditorGui.getSelectedRoad())
	{
		MeshRoadEditorGui.setSelectedRoad( %obj );
	}
}
//------------------------------------------------------------------------------
