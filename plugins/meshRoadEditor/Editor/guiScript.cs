//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function LabMouseToggle::onMouseDown(%this,%modifier,%mousePoint,%mouseClickCount)
{
	%target = %this.internalName;
	toggleVisible(%target);
}
//------------------------------------------------------------------------------
//==============================================================================
// Initialize default plugin settings
function MeshRoadEditorGui::onWake( %this )
{
	$MeshRoad::EditorOpen = true;
	%count = EWorldEditor.getSelectionSize();

	for ( %i = 0; %i < %count; %i++ )
	{
		%obj = EWorldEditor.getSelectedObject(%i);

		if ( %obj.getClassName() !$= "MeshRoad" )
			EWorldEditor.unselectObject(%obj);
		else
			%this.setSelectedRoad( %obj );
	}

	//%this-->TabBook.selectPage(0);
	%this.onNodeSelected(-1);
}

function MeshRoadEditorGui::onSleep( %this )
{
	$MeshRoad::EditorOpen = false;
}
//------------------------------------------------------------------------------
function MeshRoadEditorGui::showDefaultMaterialSaveDialog( %this, %toMaterial,%extra )
{
	%fromMaterial = MeshRoadEditorGui.topMaterialName;
	MeshRoadEditorGui.topMaterialName = %toMaterial.getName();
	Lab.syncConfigParamField(arMeshRoadEditorCfg.paramObj,"topMaterialName",%toMaterial.getName());
}
function MeshRoadEditorGui::paletteSync( %this, %mode )
{
	%evalShortcut = "LabPalettes-->" @ %mode @ ".setStateOn(1);";
	eval(%evalShortcut);
}
function MeshRoadEditorGui::onEscapePressed( %this )
{
	if ( %this.getMode() $= "MeshRoadEditorAddNodeMode" )
	{
		%this.prepSelectionMode();
		return true;
	}

	return false;
}
