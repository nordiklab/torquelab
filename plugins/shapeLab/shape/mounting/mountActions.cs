//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Edit the selected object material
//==============================================================================

function ShapeLab::mountCurrent( %this )
{

	%model = ShapeLabMountShapeMenu.getText();
	%node = ShapeLabMountWindow-->mountNode.getText();
	%type = ShapeLabMountWindow-->mountType.getText();
}

function ShapeLabMountWindow::mountShape( %this, %slot )
{
	%model = ShapeLabMountShapeMenu.getText();
	%node = %this-->mountNode.getText();
	%type = %this-->mountType.getText();

	if ( %model $= "Browse..." )
		%model = "core/art/shapes/octahedron.dts";

	if ( ShapeLabEditor.mountShape( %model, %node, %type, %slot ) )
	{
		%rowText = %model TAB fileName( %model ) TAB %node TAB %type;

		if ( %slot == -1 )
		{
			%id = %this.mounts++;
			%this-->mountList.addRow( %id, %rowText );
		}
		else
		{
			%id = %this-->mountList.getRowId( %slot+1 );
			%this-->mountList.setRowById( %id, %rowText );
		}

		%this-->mountList.setSelectedById( %id );
	}
	else
	{
		LabMsgOK( "Error", "Failed to mount \"" @ %model @ "\". Check the console for error messages.", "" );
	}
}

function ShapeLabMountWindow::unmountShape( %this )
{
	%row = %this-->mountList.getSelectedRow();

	if ( %row > 0 )
	{
		ShapeLabEditor.unmountShape( %row-1 );
		%this-->mountList.removeRow( %row );
		// Select the next row (if any)
		%count = %this-->mountList.rowCount();

		if ( %row >= %count )
			%row = %count-1;

		if ( %row > 0 )
			%this-->mountList.setSelectedRow( %row );
	}
}

function ShapeLabMountWindow::unmountAll( %this )
{
	ShapeLabEditor.unmountAll();
	%this-->mountList.clear();
	%this-->mountList.addRow( -1, "FullPath" TAB "Filename" TAB "Node" TAB "Type" );
	%this-->mountList.setRowActive( -1, false );
}
