//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function ShapeLabMountWindow::setMountThreadSequence( %this )
{
	%row = %this-->mountList.getSelectedRow();

	if ( %row > 0 )
	{
		ShapeLabEditor.setMountThreadSequence( %row-1, %this-->mountSeq.getText() );
		ShapeLabEditor.setMountThreadDir( %row-1, %this-->mountPlayBtn.getValue() );
	}
}

function ShapeLabMountSeqSlider::onMouseDragged( %this )
{
	%row = ShapeLabMountWindow-->mountList.getSelectedRow();

	if ( %row > 0 )
	{
		ShapeLabEditor.setMountThreadPos( %row-1, %this.getValue() );
		// Pause the sequence when the slider is dragged
		ShapeLabEditor.setMountThreadDir( %row-1, 0 );
		ShapeLabMountWindow-->mountPlayBtn.setStateOn( false );
	}
}

function ShapeLabMountWindow::toggleMountThreadPlayback( %this )
{
	%row = %this-->mountList.getSelectedRow();

	if ( %row > 0 )
		ShapeLabEditor.setMountThreadDir( %row-1, %this-->mountPlayBtn.getValue() );
}
