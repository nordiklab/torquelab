//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// ShapeLab -> Mounted Shapes
//==============================================================================

function ShapeLabMountWindow::onWake( %this )
{
	%this-->mountType.clear();
	%this-->mountType.add( "Object", 0 );
	%this-->mountType.add( "Image", 1 );
	%this-->mountType.add( "Wheel", 2 );
	%this-->mountType.setSelected( 1, false );
	%this-->mountSeq.clear();
	%this-->mountSeq.add( "<rootpose>", 0 );
	%this-->mountSeq.setSelected( 0, false );
	%this-->mountPlayBtn.setStateOn( false );

	// Only add the Browse entry the first time so we keep any files the user has
	// set up previously
	if ( ShapeLabMountShapeMenu.size() == 0 )
	{
		ShapeLabMountShapeMenu.add( "Browse...", 0 );
		ShapeLabMountShapeMenu.setSelected( 0, false );
	}
}

function ShapeLabMountWindow::isMountableNode( %this, %nodeName )
{
	return ( startswith( %nodeName, "mount" ) || startswith( %nodeName, "hub" ) );
}

function ShapeLabMountWindow::onShapeSelectionChanged( %this )
{
	%this.unmountAll();
	// Initialise the dropdown menus
	%this-->mountNode.clear();
	%this-->mountNode.add( "<origin>" );
	%count = ShapeLab.shape.getNodeCount();

	for ( %i = 0; %i < %count; %i++ )
	{
		%name = ShapeLab.shape.getNodeName( %i );

		if ( %this.isMountableNode( %name ) )
			%this-->mountNode.add( %name );
	}

	%this-->mountNode.sort();
	%this-->mountNode.setFirstSelected();
	%this-->mountSeq.clear();
	%this-->mountSeq.add( "<rootpose>", 0 );
	%this-->mountSeq.setSelected( 0, false );
}

function ShapeLabMountWindow::onMountListSelect( %this )
{
	%row = %this-->mountList.getSelectedRow();

	if ( %row > 0 )
	{
		%text = %this-->mountList.getRowText( %row );
		%shapePath = getField( %text, 0 );
		ShapeLabMountShapeMenu.setText( %shapePath );
		%this-->mountNode.setText( getField( %text, 2 ) );
		%this-->mountType.setText( getField( %text, 3 ) );
		// Fill in sequence list
		%this-->mountSeq.clear();
		%this-->mountSeq.add( "<rootpose>", 0 );
		%tss = findConstructor( %shapePath );

		if ( !isObject( %tss ) )
			%tss = createConstructor( %shapePath );

		if ( isObject( %tss ) )
		{
			%count = %tss.getSequenceCount();

			for ( %i = 0; %i < %count; %i++ )
				%this-->mountSeq.add( %tss.getSequenceName( %i ) );
		}

		// Select the currently playing sequence
		%slot = %row - 1;
		%seq = ShapeLabEditor.getMountThreadSequence( %slot );
		%id = %this-->mountSeq.findText( %seq );

		if ( %id == -1 )
			%id = 0;

		%this-->mountSeq.setSelected( %id, false );
		ShapeLabMountSeqSlider.setValue( ShapeLabEditor.getMountThreadPos( %slot ) );
		%this-->mountPlayBtn.setStateOn( ShapeLabEditor.getMountThreadPos( %slot ) != 0 );
	}
}

function ShapeLabMountWindow::updateSelectedMount( %this )
{
	%row = ShapeLabMountShapeMenu.getSelected();

	if ( %row > 0 )
		%this.mountShape( %row-1 );
}

function ShapeLabMountShapeMenu::onSelect( %this, %id, %text )
{
	if ( %text $= "Browse..." )
	{
		// Allow the user to browse for an external model file
		getLoadFilename( "DTS Files|*.dts|COLLADA Files|*.dae|Google Earth Files|*.kmz", %this @ ".onBrowseSelect", %this.lastPath );
	}
	else
	{
		// Modify the current mount
		ShapeLabMountWindow.updateSelectedMount();
	}
}

function ShapeLabMountShapeMenu::onBrowseSelect( %this, %path )
{
	%path = makeRelativePath( %path, getMainDotCSDir() );
	%this.lastPath = %path;
	%this.setText( %path );

	// Add entry if unique
	if ( %this.findText( %path ) == -1 )
		%this.add( %path );

	ShapeLabMountWindow.updateSelectedMount();
}
