//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// ShapeLab -> Trigger Editing
//==============================================================================

//==============================================================================
function SLab::updateSequenceTrigger( %this,%id )
{
	%pill = ShapeLab_SeqTriggerStack.findChild(%id,true);

	%seqName = SL_ActiveSequence.seqName;
	%frame = %pill-->triggerFrame.getText();
	%state = %pill-->triggerState.getText();
	%oldFrame = %pill-->triggerFrame.old;
	%oldState = %pill-->triggerState.old;

	if ( ( %frame >= 0 ) && ( %state != 0 ) )
		ShapeLab.doEditTrigger( %seqName, %oldFrame, %oldState, %frame, %state );

}
//------------------------------------------------------------------------------
//==============================================================================
function SLab::deleteSequenceTrigger( %this,%id )
{
	%pill = ShapeLab_SeqTriggerStack.findChild(%id,true);

	%seqName = SL_ActiveSequence.seqName;
	%frame = %pill-->triggerFrame.getText();
	%state = %pill-->triggerState.getText();

	if ( ( %frame >= 0 ) && ( %state != 0 ) )
		ShapeLab.doRemoveTrigger( %seqName, %frame, %state );

}
//------------------------------------------------------------------------------

function SLab::addSequenceTrigger( %this )
{
	// Can only add triggers if a sequence is selected
	%seqName = SL_ActiveSequence.seqName;

	if ( %seqName !$= "" )
	{
		ShapeLab.doAddTrigger( %seqName, "1","1" );
	}
}
