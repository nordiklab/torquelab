//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

/* =========================================================================
	 setSequenceGroundSpeed(name,transSpeed,rotSpeed );
   "Set the translation and rotation ground speed of the sequence.\n"
   "The ground speed of the sequence is set by generating ground transform "
   "keyframes. The ground translational and rotational speed is assumed to "
   "be constant for the duration of the sequence. Existing ground frames for "
   "the sequence (if any) will be replaced.\n"
   "@param name name of the sequence to modify\n"
   "@param transSpeed translational speed (trans.x trans.y trans.z) in "
   "Torque units per frame\n"
   "@param rotSpeed (optional) rotational speed (rot.x rot.y rot.z) in "
   "radians per frame. Default is \"0 0 0\"\n"
   "@return true if successful, false otherwise\n\n"
   "@tsexample\n"
   "%this.setSequenceGroundSpeed( \"run\", \"5 0 0\" );\n"
   "%this.setSequenceGroundSpeed( \"spin\", \"0 0 0\", \"4 0 0\" );\n"
   "@endtsexample\n" )
   -------------------------------------------------------------------------- */
//==============================================================================
function SLab::updateGroundSpeed( %this )
{
	%seqName = SL_ActiveSequence.seqName;
	%gndTrans = SL_ActiveSequence-->gndSpeedTrans.getText();
	%gndRot = SL_ActiveSequence-->gndSpeedTrans.getText();

	%gndSpeed = %gndTrans SPC %gndRot;

	if ( getWordCount(%gndSpeed) != 6)
	{
		warnLog("Invalid ground speed, must be trans.x trans.y trans.z rot.x rot.y rot.z",%gndSpeed);
		return;
	}

	ShapeLab.doEditSequenceGroundSpeed( %seqName, %gndSpeed);

}
//------------------------------------------------------------------------------
