//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// ShapeLab -> First Initialization
//==============================================================================

//==============================================================================
// Dirty state and saving
//==============================================================================
function ShapeLab::isDirty( %this )
{
	return ( isObject( %this.shape ) && ShapeLabPropWindow-->saveBtn.isActive() );
}

function ShapeLab::setDirty( %this, %dirty )
{
	if ( %dirty )
		ShapeLabPropWindow.text = "ShapeLab utilities *";
	else
		ShapeLabPropWindow.text = "ShapeLab utilities";

	ShapeLabPropWindow-->saveBtn.setActive( %dirty );
	ShapeLab_MainButtonStack-->saveBtn.setActive( %dirty );
	ShapeLabSequences-->saveBtn.setActive( %dirty );
}

function ShapeLab::saveChanges( %this )
{
	if ( isObject( ShapeLab.shape ) )
	{
		saveConstructor( ShapeLab.shape );//SaveConstructor + PersistenceManager
		ShapeLab.shape.writeChangeSet();
		ShapeLab.shape.notifyShapeChanged();      // Force game objects to reload shape
		ShapeLab.setDirty( false );
	}
}

