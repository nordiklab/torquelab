//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$ShapeLabSeqListAdv = "0";

//==============================================================================
// Load the Scene Editor Plugin scripts, load Guis if %loadgui = true
function ShapeLab::addSequencePill(%this,%seqName)
{
	hide(ShapeLab_SeqPillSrc);
	hide(ShapeLab_SeqPillAdvSrc);
	show(ShapeLab_SeqPillStack);

	if ($ShapeLabSeqListAdv)
		return %this.addAdvSequencePill(%seqName);

	%pill = cloneObject(ShapeLab_SeqPillSrc,"",%seqName,ShapeLab_SeqPillStack);

	%pill-->seqName.setText(%seqName);
	%pill-->deleteBtn.seqName = %seqName;
	%pill-->seqName.pill = %pill;
	%pill-->seqName.active = 1;
	%pill-->mouse.seqName = %seqName;

	%pill.seqName = %seqName;
	%pill.expanded = false;
	//%pill.superClass = "ShapeLab_SeqPillRollout";
	%pill.internalName = %seqName;
}
//------------------------------------------------------------------------------
//==============================================================================
// Load the Scene Editor Plugin scripts, load Guis if %loadgui = true
function SLabSeqPillArea::onMouseDown(%this)
{
	ShapeLab.setActiveSequence(%this.seqName);
}
//------------------------------------------------------------------------------

//==============================================================================
// Load the Scene Editor Plugin scripts, load Guis if %loadgui = true
function ShapeLab::addAdvSequencePill(%this,%seqName)
{
	%cyclic = ShapeLab.shape.getSequenceCyclic( %seqName );
	%blend = getField( ShapeLab.shape.getSequenceBlend( %seqName ), 0 );
	%frameCount = ShapeLab.shape.getSequenceFrameCount( %seqName );
	%priority = ShapeLab.shape.getSequencePriority( %seqName );
	%sourceData = ShapeLab.getSequenceSource( %seqName );
	%seqFrom = rtrim( getFields( %sourceData, 0, 1 ) );
	%seqStart = getField( %sourceData, 2 );
	%seqEnd = getField( %sourceData, 3 );
	%seqFromTotal = getField( %sourceData, 4 );

	%pill = cloneObject(ShapeLab_SeqPillAdvSrc,"",%seqName,ShapeLab_SeqPillStack);

	%pill-->seqName.setText(%seqName);
	%pill-->frameOut.setText(%seqEnd);
	%pill-->frameIn.setText(%seqStart);
	%pill-->frameOut.pill = %pill;
	%pill-->frameIn.pill = %pill;
	%pill-->seqName.pill = %pill;
	%pill-->seqName.active = 1;
	%pill-->frameOut.active = 1;
	%pill-->frameIn.active = 1;
	%pill-->deleteBtn.seqName = %seqName;

	%pill.seqName = %seqName;
	%pill.expanded = false;
	//%pill.superClass = "ShapeLab_SeqPillRollout";
	%pill.internalName = %seqName;
}
//------------------------------------------------------------------------------
//==============================================================================
// Load the Scene Editor Plugin scripts, load Guis if %loadgui = true
function ShapeLab_SeqListEdit::onAction(%this)
{
}
//------------------------------------------------------------------------------
//==============================================================================
// Load the Scene Editor Plugin scripts, load Guis if %loadgui = true
function ShapeLab_SeqListEdit::onActive(%this)
{
}
//------------------------------------------------------------------------------
//==============================================================================
// Load the Scene Editor Plugin scripts, load Guis if %loadgui = true
function ShapeLab_SeqListEdit::onGainFirstResponder(%this)
{
	%seqName = %this.pill.seqName;
	ShapeLab.setActiveSequence(%seqName);

}
//------------------------------------------------------------------------------

//==============================================================================
// Load the Scene Editor Plugin scripts, load Guis if %loadgui = true
function ShapeLab_SeqListEdit::onValidate(%this)
{
	%type = %this.internalName;
	%pill = %this.pill;
	%seqName = %pill.seqName;
	%seqIntName = %pill.internalName;

	switch$(%type)
	{
		case "frameIn":
			%frameCount = getWord( ShapeLabSeqSlider.range, 1 );
			// Force value to a frame index within the slider range
			%val = mRound( %this.getText() );

			if ( %val < 0 ) %val = 0;

			if ( %val > %frameCount ) %val = %frameCount;

			if ( %val >= %pill-->frameOut.getText() )
				%val = %pill-->frameOut.getText() - 1;

			%this.setText( %val );
			ShapeLab.onEditSequenceSource("",%pill);

		case "frameOut":
			%frameCount = getWord( ShapeLabSeqSlider.range, 1 );
			// Force value to a frame index within the slider range
			%val = mRound( %this.getText() );

			if ( %val < 0 ) %val = 0;

			if ( %val > %frameCount ) %val = %frameCount;

			if ( %val <= %pill-->frameIn.getText() )
				%val = %pill-->frameIn.getText() + 1;

			%this.setText( %val );
			ShapeLab.onEditSequenceSource("",%pill);

		case "seqName":
			ShapeLab.updateSequenceName(%seqName,%this.getText());
			/*
			%newName = %this.getText();

			if (%newName !$= %seqName) {
				ShapeLab.onEditSequenceName(%seqName,%newName);
				%pill.seqName = %newName;
			}*/
	}

}
//------------------------------------------------------------------------------
