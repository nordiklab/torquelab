//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function ShapeLab::updateDetail( %this )
{
	%detailCount = ShapeLab.shape.getDetailLevelCount();
	ShapeLabAdv_Details-->detailSlider.range = "0" SPC ( %detailCount-1 );

	if ( %detailCount >= 2 )
		ShapeLabAdv_Details-->detailSlider.ticks = %detailCount - 2;
	else
		ShapeLabAdv_Details-->detailSlider.ticks = 0;

	// Initialise imposter settings
	ShapeLabDetails-->bbUseImposters.setValue( ShapeLab.shape.getImposterDetailLevel() != -1 );

	// Update detail parameters
	if ( ShapeLabEditor.currentDL < %detailCount )
	{
		%settings = ShapeLab.shape.getImposterSettings( ShapeLabEditor.currentDL );
		%isImposter = getWord( %settings, 0 );
		//ShapeLabAdv_Details-->imposterInactive.setVisible( !%isImposter );
		ShapeLabDetails-->bbEquatorSteps.setText( getField( %settings, 1 ) );
		ShapeLabDetails-->bbPolarSteps.setText( getField( %settings, 2 ) );
		ShapeLabDetails-->bbDetailLevel.setText( getField( %settings, 3 ) );
		ShapeLabDetails-->bbDimension.setText( getField( %settings, 4 ) );
		ShapeLabDetails-->bbIncludePoles.setValue( getField( %settings, 5 ) );
		ShapeLabDetails-->bbPolarAngle.setText( getField( %settings, 6 ) );
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function ShapeLabDetails::updateChangedDetail( %this )
{
	%guiView = ShapeLabEditor;

// Update slider
	if ( mRound( ShapeLabAdv_Details-->detailSlider.getValue() ) != %guiView.currentDL )
		ShapeLabAdv_Details-->detailSlider.setValue( %guiView.currentDL );

	ShapeLabDetails-->detailSize.setText( %guiView.detailSize );
	ShapeLab.updateDetail();
	%id = ShapeLab_DetailTree.getSelectedItem();

	if ( ( %id <= 0 ) || ( %guiView.currentDL != ShapeLab_DetailTree.getDetailLevelFromItem( %id ) ) )
	{
		%id = ShapeLab_DetailTree.findItemByValue( %guiView.detailSize );

		if ( %id > 0 )
		{
			ShapeLab_DetailTree.clearSelection();
			ShapeLab_DetailTree.selectItem( %id );
		}
	}
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//==============================================================================
//------------------------------------------------------------------------------
// Detail Entry Selected
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function ShapeLabDetails::onSelected( %this, %name,%dl )
{
	//Remove trailing LOD to get pure name
	%baseName = stripTrailingNumber( %name );

	// Selected a detail => disable mesh controls
	ShapeLabDetails-->editMeshActive.setVisible( false );
	ShapeLabEditor.selectedObject = -1;
	ShapeLabEditor.selectedObjDetail = 0;
}
//------------------------------------------------------------------------------

//==============================================================================
//------------------------------------------------------------------------------
// Rename Detail
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function ShapeLabDetails::renameDetail( %this,%newName )
{
	if (%newName $= "")
		return;

	// Check if we are renaming a detail or a mesh
	%id = ShapeLab_DetailTree.getSelectedItem();
	%oldName = ShapeLab_DetailTree.getItemText( %id );

	if ( ShapeLab_DetailTree.isParentItem( %id ) )
	{
		// Rename the selected detail level
		%oldSize = getTrailingNumber( %oldName );
		ShapeLab.doRenameDetail( %oldName, %newName @ %oldSize );
	}
	else
	{
		// Rename the selected mesh
		ShapeLab.doRenameObject( stripTrailingNumber( %oldName ), %newName );
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// doRenameDetail Action (With Undo support)
//==============================================================================

//==============================================================================
// Rename detail
function ShapeLab::doRenameDetail( %this, %oldName, %newName )
{
	%action = %this.createAction( ActionRenameDetail, "Rename detail" );
	%action.oldName = %oldName;
	%action.newName = %newName;
	%this.doAction( %action );
}
//------------------------------------------------------------------------------
function ActionRenameDetail::doit( %this )
{
	if ( ShapeLab.shape.renameDetailLevel( %this.oldName, %this.newName ) )
	{
		ShapeLab.onDetailRenamed( %this.oldName, %this.newName );
		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
function ActionRenameDetail::undo( %this )
{
	Parent::undo( %this );

	if ( ShapeLab.shape.renameDetailLevel( %this.newName, %this.oldName ) )
		ShapeLab.onDetailRenamed( %this.newName, %this.oldName );
}
//------------------------------------------------------------------------------

//==============================================================================
//------------------------------------------------------------------------------
// Remove Detail
//------------------------------------------------------------------------------
//==============================================================================
//==============================================================================
function ShapeLabDetails::removeDetailId( %this,%id,%noUndo )
{
	if (%id $= "")
		%id = ShapeLab_DetailTree.getSelectedItem();

	if ( !ShapeLab_DetailTree.isParentItem( %id ) )
	{
		warnLog("DEPRECIATED","ShapeLabDetails::removeDetailId called to remove a detail, redirecting to removeMeshId");
		%this.removeMeshId(%id,%noUndo );
		return;
	}

	%detSize = ShapeLab_DetailTree.getItemValue( %id );
	%this.removeDetail(%detSize,%noUndo);
}
//------------------------------------------------------------------------------
//==============================================================================
function ShapeLabDetails::removeDetail( %this,%size,%noUndo )
{
	if (%size $= "" || !strIsNumeric(%size))
		return;

	if (%noUndo)
		return ShapeLab.shape.removeDetailLevel( %size );

	ShapeLab.doRemoveDetail(%size);
}
//------------------------------------------------------------------------------

//==============================================================================
// doRemoveDetail Action (With Undo support)
//==============================================================================

//==============================================================================
function ShapeLab::doRemoveDetail( %this, %size )
{
	%action = %this.createAction( ActionRemoveDetail, "Remove detail level" );
	%action.size = %size;
	%this.doAction( %action );
}
//------------------------------------------------------------------------------
function ActionRemoveDetail::doit( %this )
{
	%meshList = ShapeLab.getDetailMeshList( %this.size );

	if ( ShapeLab.shape.removeDetailLevel( %this.size ) )
	{
		%meshCount = getFieldCount( %meshList );

		for ( %i = 0; %i < %meshCount; %i++ )
			SShapeLab.onDetailRemoved( getField( %meshList, %i ) );

		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
function ActionRemoveDetail::undo( %this )
{
	Parent::undo( %this );
}
//------------------------------------------------------------------------------

//==============================================================================
//------------------------------------------------------------------------------
// Edit Detail Size
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================

//------------------------------------------------------------------------------

//==============================================================================
// doRemoveDetail Action (With Undo support)
//==============================================================================

//==============================================================================
function ShapeLab::doEditDetailSize( %this, %oldSize, %newSize )
{
	%action = %this.createAction( ActionEditDetailSize, "Edit detail size" );
	%action.oldSize = %oldSize;
	%action.newSize = %newSize;
	%this.doAction( %action );
}

function ActionEditDetailSize::doit( %this )
{
	%dl = ShapeLab.shape.setDetailLevelSize( %this.oldSize, %this.newSize );

	if ( %dl != -1 )
	{
		ShapeLab.onDetailSizeChanged( %this.oldSize, %this.newSize );
		return true;
	}

	return false;
}

function ActionEditDetailSize::undo( %this )
{
	Parent::undo( %this );
	%dl = ShapeLab.shape.setDetailLevelSize( %this.newSize, %this.oldSize );

	if ( %dl != -1 )
		ShapeLab.onDetailSizeChanged( %this.newSize, %this.oldSize );
}
//------------------------------------------------------------------------------

//==============================================================================
//------------------------------------------------------------------------------
// Edit Detail Size
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function ShapeLabDetails::updateDetailSize( %this,%value )
{
	if (%value $= "")
		%value = ShapeLabAdv_Details-->detailSize.getText();

	// Change the size of the current detail level
	%oldSize = ShapeLab.shape.getDetailLevelSize( ShapeLabEditor.currentDL );
	ShapeLab.doEditDetailSize( %oldSize, %value );
}
//------------------------------------------------------------------------------

function ShapeLabDetails::editDetailSize( %this,%newSize )
{
	%id = ShapeLab_DetailTree.getSelectedItem();

	if ( !ShapeLab_DetailTree.isParentItem( %id ) )
	{

		%name = ShapeLab_DetailTree.getItemText(%id);
		ShapeLab.doEditMeshSize(%name,%newSize);
		return;
		%size = getTrailingNumber( %name );
		%strLen = strlen(%size);
		%stockName = getSubStr(%name,0,strlen(%name)-%strLen);
		%newName = %name @ %newSize;
		ShapeLab_DetailTree.removeMeshEntry(%name);
		ShapeLab_DetailTree.addMeshEntry(%newName);
		warnLog("Changing mesh size was",%name,"Now",%newName);

	}

	// Change the size of the selected detail level
	%oldSize = ShapeLab_DetailTree.getItemValue( %id );
	ShapeLab.doEditDetailSize( %oldSize, %newSize );
}
//------------------------------------------------------------------------------

//==============================================================================
// doRemoveDetail Action (With Undo support)
//==============================================================================

//==============================================================================
function ShapeLab::doEditDetailSize( %this, %oldSize, %newSize )
{
	%action = %this.createAction( ActionEditDetailSize, "Edit detail size" );
	%action.oldSize = %oldSize;
	%action.newSize = %newSize;
	%this.doAction( %action );
}

function ActionEditDetailSize::doit( %this )
{
	%dl = ShapeLab.shape.setDetailLevelSize( %this.oldSize, %this.newSize );

	if ( %dl != -1 )
	{
		ShapeLab.onDetailSizeChanged( %this.oldSize, %this.newSize );
		return true;
	}

	return false;
}

function ActionEditDetailSize::undo( %this )
{
	Parent::undo( %this );
	%dl = ShapeLab.shape.setDetailLevelSize( %this.newSize, %this.oldSize );

	if ( %dl != -1 )
		ShapeLab.onDetailSizeChanged( %this.newSize, %this.oldSize );
}
//------------------------------------------------------------------------------
