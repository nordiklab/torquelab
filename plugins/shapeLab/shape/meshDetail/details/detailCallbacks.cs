//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function ShapeLabDetails::onToggleDetails( %this, %useDetails )
{
	ShapeLabAdv_Details-->detailSlider.setActive(%useDetails);
	//ShapeLabAdv_Details-->levelsInactive.visible = %useDetails;
}
//------------------------------------------------------------------------------
//==============================================================================
// ShapeLab -> Details Actions Callbacks
//==============================================================================

//==============================================================================
function ShapeLab::onDetailRenamed( %this, %oldName, %newName )
{
	// --- DETAILS TAB ---
	// Rename detail entry
	%id = ShapeLab_DetailTree.findItemByName( %oldName );

	if ( %id > 0 )
	{
		%size = ShapeLab_DetailTree.getItemValue( %id );
		ShapeLab_DetailTree.editItem( %id, %newName, %size );

		// Sync text if item is selected
		if ( ShapeLab_DetailTree.isItemSelected( %id ) &&
		      ( ShapeLabDetails-->meshName.getText() !$= %newName ) )
			ShapeLabDetails-->meshName.setText( stripTrailingNumber( %newName ) );
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function ShapeLab::onDetailSizeChanged( %this, %oldSize, %newSize )
{
	// --- MISC ---
	ShapeLabEditor.refreshShape();
	%dl = ShapeLab.shape.getDetailLevelIndex( %newSize );

	if ( ShapeLabAdv_Details-->detailSize.getText() $= %oldSize )
	{
		ShapeLabEditor.currentDL = %dl;
		ShapeLabAdv_Details-->detailSize.setText( %newSize );
		ShapeLabDetails-->detailSize.setText( %newSize );
	}

	// --- DETAILS TAB ---
	// Update detail entry then resort details by size
	%id = ShapeLab_DetailTree.findItemByValue( %oldSize );
	%detName = ShapeLab.shape.getDetailLevelName( %dl );
	ShapeLab_DetailTree.editItem( %id, %detName, %newSize );

	for ( %sibling = ShapeLab_DetailTree.getPrevSibling( %id );
	      ( %sibling > 0 ) && ( ShapeLab_DetailTree.getItemValue( %sibling ) < %newSize );
	      %sibling = ShapeLab_DetailTree.getPrevSibling( %id ) )
		ShapeLab_DetailTree.moveItemUp( %id );

	for ( %sibling = ShapeLab_DetailTree.getNextSibling( %id );
	      ( %sibling > 0 ) && ( ShapeLab_DetailTree.getItemValue( %sibling ) > %newSize );
	      %sibling = ShapeLab_DetailTree.getNextSibling( %id ) )
		ShapeLab_DetailTree.moveItemDown( %id );

	// Update size values for meshes of this detail
	for ( %child = ShapeLab_DetailTree.getChild( %id );
	      %child > 0;
	      %child = ShapeLab_DetailTree.getNextSibling( %child ) )
	{
		%meshName = stripTrailingNumber( ShapeLab_DetailTree.getItemText( %child ) );
		ShapeLab_DetailTree.editItem( %child, %meshName SPC %newSize, "" );
	}
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
function ShapeLab::onDetailRemoved( %this, %detailName )
{
	// --- MISC ---
	ShapeLabEditor.refreshShape();
	// --- COLLISION WINDOW ---
	// Remove object from target list if it no longer exists
	%objName = stripTrailingNumber( %detailName );

	if ( ShapeLab.shape.getObjectIndex( %objName ) == -1 )
	{
		%id = ShapeLab_CreateColRollout-->colTarget.findText( %objName );

		if ( %id != -1 )
			ShapeLab_CreateColRollout-->colTarget.clearEntry( %id );
	}

	// --- DETAILS TAB ---
	// Determine which item to select next
	%id = ShapeLab_DetailTree.findItemByName( %detailName );

	if ( %id > 0 )
	{
		%nextId = ShapeLab_DetailTree.getPrevSibling( %id );

		if ( %nextId <= 0 )
		{
			%nextId = ShapeLab_DetailTree.getNextSibling( %id );

			if ( %nextId <= 0 )
				%nextId = 2;
		}

		// Remove the entry from the tree
		%detailSize = getTrailingNumber( %detailName );
		ShapeLab_DetailTree.removeMeshEntry( %detailName, %detailSize );

		// Change selection if needed
		if ( ShapeLab_DetailTree.getSelectedItem() == -1 )
			ShapeLab_DetailTree.selectItem( %nextId );
	}
}

//==============================================================================
// IMPOSTER ACTION CALLBACKS
//==============================================================================

