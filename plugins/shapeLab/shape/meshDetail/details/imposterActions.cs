//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function ShapeLab::onImposterAdded( %this, %meshName )
{
	show(ShapeLab_ImposterWait);
	// Need to de-highlight the current material, or the imposter will have the
	// highlight effect baked in!
	ShapeLabMaterials.updateSelectedMaterial( false );
	%dl = ShapeLab.shape.addImposter( %this.newSize, %val[1], %val[2], %val[3], %val[4], %val[5], %val[6] );

	hide(ShapeLab_ImposterWait);
	// Restore highlight effect
	ShapeLabMaterials.updateSelectedMaterial( ShapeLabMaterials-->highlightMaterial.getValue() );

	if ( %dl != -1 )
	{
		ShapeLabEditor.refreshShape();
		ShapeLabEditor.currentDL = %dl;
		ShapeLabAdv_Details-->detailSize.setText( %this.newSize );
		ShapeLabDetails-->detailSize.setText( %this.newSize );
		ShapeLab.updateDetail();
		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
//==============================================================================
function ShapeLab::onImposterRemoved( %this )
{
	ShapeLabEditor.refreshShape();
	ShapeLabEditor.currentDL = 0;
	ShapeLab.updateDetail();
}
//------------------------------------------------------------------------------

//==============================================================================
//------------------------------------------------------------------------------
// Toggle Imposter for current detail
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
// Toggle details imposters
function ShapeLabDetails::onToggleImposter( %this, %useImposter )
{
	%hasImposterDetail = ( ShapeLab.shape.getImposterDetailLevel() != -1 );
	ShapeLabAdv_Details-->imposterActive.visible = %useImposter;

	ShapeLab_ImposterSettings.visible = %useImposter;

	if ( %useImposter == %hasImposterDetail )
		return;

	if ( %useImposter )
	{
		// Determine an unused detail size
		for ( %detailSize = 0; %detailSize < 50; %detailSize++ )
		{
			if ( ShapeLab.shape.getDetailLevelIndex( %detailSize ) == -1 )
				break;
		}

		// Set some initial values for the imposter
		%bbEquatorSteps = 6;
		%bbPolarSteps = 0;
		%bbDetailLevel = 0;
		%bbDimension = 128;
		%bbIncludePoles = 0;
		%bbPolarAngle = 0;
		// Add a new imposter detail level to the shape
		ShapeLab.doEditImposter( -1, %detailSize, %bbEquatorSteps, %bbPolarSteps,
		                         %bbDetailLevel, %bbDimension, %bbIncludePoles, %bbPolarAngle );
	}
	else
	{
		// Remove the imposter detail level
		ShapeLab.doRemoveImposter();
	}
}
//==============================================================================
//==============================================================================
//------------------------------------------------------------------------------
// Edit Imposter for current detail
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function ShapeLabDetails::editImposter( %this )
{
	// Modify the parameters of the current imposter detail level
	%detailSize = ShapeLab.shape.getDetailLevelSize( ShapeLabEditor.currentDL );
	%bbDimension = ShapeLabAdv_Details-->bbDimension.getText();
	%bbDetailLevel = ShapeLabAdv_Details-->bbDetailLevel.getText();
	%bbEquatorSteps = ShapeLabAdv_Details-->bbEquatorSteps.getText();
	%bbIncludePoles = ShapeLabAdv_Details-->bbIncludePoles.getValue();
	%bbPolarSteps = ShapeLabAdv_Details-->bbPolarSteps.getText();
	%bbPolarAngle = ShapeLabAdv_Details-->bbPolarAngle.getText();
	ShapeLab.doEditImposter( ShapeLabEditor.currentDL, %detailSize,
	                         %bbEquatorSteps, %bbPolarSteps, %bbDetailLevel, %bbDimension,
	                         %bbIncludePoles, %bbPolarAngle );
}
//------------------------------------------------------------------------------
//==============================================================================
// doAddMeshFromFile Action (With Undo support)
//==============================================================================
//==============================================================================
function ShapeLab::doEditImposter( %this, %dl, %detailSize, %bbEquatorSteps, %bbPolarSteps,
                                   %bbDetailLevel, %bbDimension, %bbIncludePoles, %bbPolarAngle )
{
	%action = %this.createAction( ActionEditImposter, "Edit imposter" );
	%action.oldDL = %dl;

	if ( %action.oldDL != -1 )
	{
		%action.oldSize = ShapeLab.shape.getDetailLevelSize( %dl );
		%action.oldImposter = ShapeLab.shape.getImposterSettings( %dl );
	}

	%action.newSize = %detailSize;
	%action.newImposter = "1" TAB %bbEquatorSteps TAB %bbPolarSteps TAB %bbDetailLevel TAB
	                      %bbDimension TAB %bbIncludePoles TAB %bbPolarAngle;
	%this.doAction( %action );
}

function ActionEditImposter::doit( %this )
{
	// Unpack new imposter settings
	for ( %i = 0; %i < 7; %i++ )
		%val[%i] = getField( %this.newImposter, %i );

	return ShapeLab.onImposterAdded(%this.newSize);
}

function ActionEditImposter::undo( %this )
{
	Parent::undo( %this );

	// If this was a new imposter, just remove it. Otherwise restore the old settings
	if ( %this.oldDL < 0 )
	{
		if ( ShapeLab.shape.removeImposter() )
		{
			ShapeLabEditor.refreshShape();
			ShapeLabEditor.currentDL = 0;
			ShapeLab.updateDetail();
		}
	}
	else
	{
		// Unpack old imposter settings
		for ( %i = 0; %i < 7; %i++ )
			%val[%i] = getField( %this.oldImposter, %i );

		ShapeLab.onImposterAdded(%this.oldSize);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// doRemoveImposter Action (With Undo support)
//==============================================================================

//==============================================================================
function ShapeLab::doRemoveImposter( %this )
{
	%action = %this.createAction( ActionRemoveImposter, "Remove imposter" );
	%dl = ShapeLab.shape.getImposterDetailLevel();

	if ( %dl != -1 )
	{
		%action.oldSize = ShapeLab.shape.getDetailLevelSize( %dl );
		%action.oldImposter = ShapeLab.shape.getImposterSettings( %dl );
		%this.doAction( %action );
	}
}
//------------------------------------------------------------------------------
function ActionRemoveImposter::doit( %this )
{
	if ( ShapeLab.shape.removeImposter() )
	{
		ShapeLab.onImposterRemoved();

		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
function ActionRemoveImposter::undo( %this )
{
	Parent::undo( %this );

	// Unpack the old imposter settings
	for ( %i = 0; %i < 7; %i++ )
		%val[%i] = getField( %this.oldImposter, %i );

	ShapeLab.onImposterAdded(%this.oldSize);
}
//------------------------------------------------------------------------------

