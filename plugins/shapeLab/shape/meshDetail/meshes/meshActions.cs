//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function ShapeLabDetails::getMeshSelected( %this )
{
	%id = ShapeLab_DetailTree.getSelectedItem();

	if ( ShapeLab_DetailTree.isParentItem( %id ) )
		return "";

	return ShapeLab_DetailTree.getItemText( %id );
}
//------------------------------------------------------------------------------

//==============================================================================
//------------------------------------------------------------------------------
// Mesh Entry Selected
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function ShapeLabDetails::onMeshSelected( %this, %name,%dl )
{
	//Remove trailing LOD to get pure name
	%baseName = stripTrailingNumber( %name );

	// Selected a mesh => sync mesh controls
	ShapeLabDetails-->editMeshActive.setVisible( true );

	switch$ ( ShapeLab.shape.getMeshType( %name ) )
	{
		case "normal":
			ShapeLabDetails-->bbType.setSelected( 0, false );

		case "billboard":
			ShapeLabDetails-->bbType.setSelected( 1, false );

		case "billboardzaxis":
			ShapeLabDetails-->bbType.setSelected( 2, false );
	}

	%node = ShapeLab.shape.getObjectNode( %baseName );

	if ( %node $= "" )
		%node = "<root>";

	ShapeLabDetails-->objectNode.setSelected( ShapeLabDetails-->objectNode.findText( %node ), false );
	ShapeLabEditor.selectedObject = ShapeLab.shape.getObjectIndex( %baseName );
	ShapeLabEditor.selectedObjDetail = %dl;
}
//------------------------------------------------------------------------------

//==============================================================================
//------------------------------------------------------------------------------
// Rename Object (Mesh)
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
// doRenameObject Action (With Undo support)
//==============================================================================
function ShapeLab::doRenameObject( %this, %oldName, %newName )
{
	%action = %this.createAction( ActionRenameObject, "Rename object" );
	%action.oldName = %oldName;
	%action.newName = %newName;
	%this.doAction( %action );
}
//------------------------------------------------------------------------------
function ActionRenameObject::doit( %this )
{
	if ( ShapeLab.shape.renameObject( %this.oldName, %this.newName ) )
	{
		ShapeLab.onObjectRenamed( %this.oldName, %this.newName );
		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
function ActionRenameObject::undo( %this )
{
	Parent::undo( %this );

	if ( ShapeLab.shape.renameObject( %this.newName, %this.oldName ) )
		ShapeLab.onObjectRenamed( %this.newName, %this.oldName );
}
//------------------------------------------------------------------------------

//==============================================================================
//------------------------------------------------------------------------------
// Set Object (Mesh) Parent Node
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function ShapeLabDetails::changeObjectNode( %this,%node )
{
	if (%node $= "")
		%node = %this-->objectNode.getText();

	// This command is only valid for meshes (not details)
	%id = ShapeLab_DetailTree.getSelectedItem();

	if ( !ShapeLab_DetailTree.isParentItem( %id ) )
	{
		%meshName = ShapeLab_DetailTree.getItemText( %id );
		%objName = stripTrailingNumber( %meshName );
		%node = %this-->objectNode.getText();

		if ( %node $= "<root>" )
			%node = "";

		ShapeLab.doSetObjectNode( %objName, %node );
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// doSetObjectNode Action (With Undo support)
//==============================================================================
function ShapeLab::doSetObjectNode( %this, %objName, %node )
{
	%action = %this.createAction( ActionSetObjectNode, "Set object node" );
	%action.objName = %objName;
	%action.oldNode = %this.shape.getObjectNode( %objName );
	%action.newNode = %node;
	%this.doAction( %action );
}
//------------------------------------------------------------------------------
function ActionSetObjectNode::doit( %this )
{
	if ( ShapeLab.shape.setObjectNode( %this.objName, %this.newNode ) )
	{
		ShapeLab.onObjectNodeChanged( %this.objName );
		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
function ActionSetObjectNode::undo( %this )
{
	Parent::undo( %this );

	if ( ShapeLab.shape.setObjectNode( %this.objName, %this.oldNode ) )
		ShapeLab.onObjectNodeChanged( %this.objName );
}
//------------------------------------------------------------------------------

//==============================================================================
//------------------------------------------------------------------------------
// Add Node to selected Shape
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function ShapeLabDetails::removeMeshId( %this,%id,%noUndo )
{
	if (%id $= "")
		%id = ShapeLab_DetailTree.getSelectedItem();

	if ( ShapeLab_DetailTree.isParentItem( %id ) )
	{
		warnLog("DEPRECIATED","ShapeLabDetails::removeMeshId called to remove a detail, redirecting to removeDetailId");
		%this.removeDetailId(%id,%noUndo );
		return;
	}

	%name = ShapeLabDetails.getMeshSelected();
	%this.removeMesh(%name,%noUndo);

}
//------------------------------------------------------------------------------
//==============================================================================
function ShapeLabDetails::removeMesh( %this,%name,%noUndo )
{
	if (%name $= "")
		%name = ShapeLabDetails.getMeshSelected();

	if (%name $= "")
		return false;

	//If noUndo specified, simply remove it now
	if (%noUndo)
	{
		if ( ShapeLab.shape.removeMesh( %name ) )
		{
			ShapeLab.onMeshRemoved(%name );
			return true;
		}

		return false;
	}

	ShapeLab.doRemoveShapeData( "Mesh", %name );
}
//------------------------------------------------------------------------------

//==============================================================================
// doRemoveMesh Action (With Undo support)
//==============================================================================
function ShapeLab::doRemoveMesh( %this, %meshName )
{
	%action = %this.createAction( ActionRemoveMesh, "Remove mesh" );
	%action.meshName = %meshName;
	%this.doAction( %action );
}
//------------------------------------------------------------------------------
function ActionRemoveMesh::doit( %this )
{
	if ( ShapeLab.shape.removeMesh( %this.meshName ) )
	{
		ShapeLab.onMeshRemoved( %this.meshName );
		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
function ActionRemoveMesh::undo( %this )
{
	Parent::undo( %this );
}
//------------------------------------------------------------------------------

//==============================================================================
// Mesh Removed Callback
//==============================================================================
function ShapeLab::onMeshRemoved( %this, %meshName )
{
	// --- MISC ---
	ShapeLabEditor.refreshShape();
	// --- COLLISION WINDOW ---
	// Remove object from target list if it no longer exists
	%objName = stripTrailingNumber( %meshName );

	if ( ShapeLab.shape.getObjectIndex( %objName ) == -1 )
	{
		%id = ShapeLab_CreateColRollout-->colTarget.findText( %objName );

		if ( %id != -1 )
			ShapeLab_CreateColRollout-->colTarget.clearEntry( %id );
	}

	// --- DETAILS TAB ---
	// Determine which item to select next
	%id = ShapeLab_DetailTree.findItemByName( %meshName );

	if ( %id > 0 )
	{
		%nextId = ShapeLab_DetailTree.getPrevSibling( %id );

		if ( %nextId <= 0 )
		{
			%nextId = ShapeLab_DetailTree.getNextSibling( %id );

			if ( %nextId <= 0 )
				%nextId = 2;
		}

		// Remove the entry from the tree
		%meshSize = getTrailingNumber( %meshName );
		ShapeLab_DetailTree.removeMeshItem( %id, %meshName, %meshSize );

		// Change selection if needed
		if ( ShapeLab_DetailTree.getSelectedItem() == -1 )
			ShapeLab_DetailTree.selectItem( %nextId );
	}
}
//------------------------------------------------------------------------------

//==============================================================================
//------------------------------------------------------------------------------
// Remove Node to selected Shape
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function ShapeLab::editMeshSize( %this )
{
	%newSize = ShapeLabDetails-->detailSize.getText();
	// Check if we are changing the size for a detail or a mesh
	%id = ShapeLab_DetailTree.getSelectedItem();

	if ( ShapeLab_DetailTree.isParentItem( %id ) )
	{
		// Change the size of the selected detail level
		%oldSize = ShapeLab_DetailTree.getItemValue( %id );
		ShapeLab.doEditDetailSize( %oldSize, %newSize );
	}
	else
	{
		// Change the size of the selected mesh
		%meshName = ShapeLab_DetailTree.getItemText( %id );
		ShapeLab.doEditMeshSize( %meshName, %newSize );
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// DO removeNode Action (With Undo support)
//==============================================================================

//==============================================================================
function ShapeLab::doEditMeshSize( %this, %meshName, %size )
{
	%action = %this.createAction( ActionEditMeshSize, "Edit mesh size" );
	%action.meshName = stripTrailingNumber( %meshName );
	%action.oldSize = getTrailingNumber( %meshName );
	%action.newSize = %size;
	%this.doAction( %action );
}
//------------------------------------------------------------------------------
function ActionEditMeshSize::doit( %this )
{
	if ( ShapeLab.shape.setMeshSize( %this.meshName SPC %this.oldSize, %this.newSize ) )
	{
		ShapeLab.onMeshSizeChanged( %this.meshName, %this.oldSize, %this.newSize );
		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
function ActionEditMeshSize::undo( %this )
{
	Parent::undo( %this );

	if ( ShapeLab.shape.setMeshSize( %this.meshName SPC %this.newSize, %this.oldSize ) )
		ShapeLab.onMeshSizeChanged( %this.meshName, %this.oldSize, %this.oldSize );
}
//------------------------------------------------------------------------------

//==============================================================================
//------------------------------------------------------------------------------
// Add a mesh from a file
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function ShapeLabDetails::addMeshFromFile( %this, %path )
{
	if ( %path $= "" )
	{
		getLoadFilename( "DTS Files|*.dts|COLLADA Files|*.dae|Google Earth Files|*.kmz", %this @ ".addMeshFromFile", ShapeLabDetails.lastPath );
		return;
	}

	%path = makeRelativePath( %path, getMainDotCSDir() );
	ShapeLabDetails.lastPath = %path;

	%addMode = ShapeLabDetails-->AddShapeToDetailMenu.getText();

	// Determine the detail level to use for the new geometry
	switch$(%addMode)
	{
		case "current detail":
			%size = ShapeLab.shape.getDetailLevelSize( ShapeLabEditor.currentDL );

		case "new detail":
			// Check if the file has an LODXXX hint at the end of it
			%base = fileBase( %path );
			%pos = strstr( %base, "_LOD" );

			if ( %pos > 0 )
				%size = getSubStr( %base, %pos + 4, strlen( %base ) ) + 0;
			else
				%size = 2;

			// Make sure size is not in use
			while ( ShapeLab.shape.getDetailLevelIndex( %size ) != -1 )
				%size++;
	}

	//Call the actual function which will add the shape to detail size
	ShapeLab.doAddMeshFromFile( %path, %size );
}
//------------------------------------------------------------------------------
//==============================================================================
// doAddMeshFromFile Action (With Undo support)
//==============================================================================
//==============================================================================
// Add meshes from file
function ShapeLab::doAddMeshFromFile( %this, %filename, %size )
{
	%action = %this.createAction( ActionAddMeshFromFile, "Add mesh from file" );
	%action.filename = %filename;
	%action.size = %size;
	%this.doAction( %action );
}
//------------------------------------------------------------------------------
function ActionAddMeshFromFile::doit( %this )
{
	%this.meshList = ShapeLab.addLODFromFile( ShapeLab.shape, %this.filename, %this.size, 1 );

	if ( %this.meshList !$= "" )
	{
		%count = getFieldCount( %this.meshList );

		for ( %i = 0; %i < %count; %i++ )
			ShapeLab.onMeshAdded( getField( %this.meshList, %i ) );

		ShapeLab.updateMaterialList();
		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
function ActionAddMeshFromFile::undo( %this )
{
	// Remove all the meshes we added
	%count = getFieldCount( %this.meshList );

	for ( %i = 0; %i < %count; %i ++ )
	{
		%name = getField( %this.meshList, %i );
		ShapeLab.shape.removeMesh( %name );
		ShapeLab.onMeshRemoved( %name );
	}

	ShapeLab.updateMaterialList();
}
//------------------------------------------------------------------------------

//==============================================================================
//------------------------------------------------------------------------------
// Edit Mesh Billboard
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function ShapeLab::editMeshBillboard( %this )
{
	// This command is only valid for meshes (not details)
	%id = ShapeLab_DetailTree.getSelectedItem();

	if ( !ShapeLab_DetailTree.isParentItem( %id ) )
	{
		%meshName = ShapeLab_DetailTree.getItemText( %id );
		%bbType = ShapeLabDetails-->bbType.getText();

		switch$ ( %bbType )
		{
			case "None":
				%bbType = "normal";

			case "Billboard":
				%bbType = "billboard";

			case "Z Billboard":
				%bbType = "billboardzaxis";
		}

		ShapeLab.doEditMeshBillboard( %meshName, %bbType );
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// doEditMeshBillboard Action (With Undo support)
//==============================================================================
//==============================================================================
function ShapeLab::doEditMeshBillboard( %this, %meshName, %type )
{
	%action = %this.createAction( ActionEditMeshBillboard, "Edit mesh billboard" );
	%action.meshName = %meshName;
	%action.oldType = %this.shape.getMeshType( %meshName );
	%action.newType = %type;
	%this.doAction( %action );
}
//------------------------------------------------------------------------------
function ActionEditMeshBillboard::doit( %this )
{
	if ( ShapeLab.shape.setMeshType( %this.meshName, %this.newType ) )
	{
		switch$ ( ShapeLab.shape.getMeshType( %this.meshName ) )
		{
			case "normal":
				ShapeLabDetails-->bbType.setSelected( 0, false );

			case "billboard":
				ShapeLabDetails-->bbType.setSelected( 1, false );

			case "billboardzaxis":
				ShapeLabDetails-->bbType.setSelected( 2, false );
		}

		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
function ActionEditMeshBillboard::undo( %this )
{
	Parent::undo( %this );

	if ( ShapeLab.shape.setMeshType( %this.meshName, %this.oldType ) )
	{
		%id = ShapeLab_DetailTree.getSelectedItem();

		if ( ( %id > 1 ) && ( ShapeLab_DetailTree.getItemText( %id ) $= %this.meshName ) )
		{
			switch$ ( ShapeLab.shape.getMeshType( %this.meshName ) )
			{
				case "normal":
					ShapeLabDetails-->bbType.setSelected( 0, false );

				case "billboard":
					ShapeLabDetails-->bbType.setSelected( 1, false );

				case "billboardzaxis":
					ShapeLabDetails-->bbType.setSelected( 2, false );
			}
		}
	}
}
//------------------------------------------------------------------------------
