//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function ShapeLab::onObjectRenamed( %this, %oldName, %newName )
{
	// --- DETAILS TAB ---
	// Rename tree entries for this object
	%count = ShapeLab.shape.getMeshCount( %newName );

	for ( %i = 0; %i < %count; %i++ )
	{
		%size = getTrailingNumber( ShapeLab.shape.getMeshName( %newName, %i ) );
		%id = ShapeLab_DetailTree.findItemByName( %oldName SPC %size );

		if ( %id > 0 )
		{
			ShapeLab_DetailTree.editItem( %id, %newName SPC %size, "" );

			// Sync text if item is selected
			if ( ShapeLab_DetailTree.isItemSelected( %id ) &&
			      ( ShapeLabDetails-->meshName.getText() !$= %newName ) )
				ShapeLabDetails-->meshName.setText( %newName );
		}
	}
}
//------------------------------------------------------------------------------

//==============================================================================
function ShapeLab::onObjectNodeChanged( %this, %objName )
{
	// --- MISC ---
	ShapeLabEditor.refreshShape();

	// --- DETAILS TAB ---
	// Update the node popup menu if this object is selected
	if ( ShapeLabDetails-->meshName.getText() $= %objName )
	{
		%nodeName = ShapeLab.shape.getObjectNode( %objName );

		if ( %nodeName $= "" )
			%nodeName = "<root>";

		%id = ShapeLabDetails-->objectNode.findText( %nodeName );
		ShapeLabDetails-->objectNode.setSelected( %id, false );
	}
}
//------------------------------------------------------------------------------

//==============================================================================
function ShapeLab::onMeshSizeChanged( %this, %meshName, %oldSize, %newSize )
{
	// --- MISC ---
	ShapeLabEditor.refreshShape();
	// --- DETAILS TAB ---
	// Move the mesh to the new location in the tree
	%selected = ShapeLab_DetailTree.getSelectedItem();
	%id = ShapeLab_DetailTree.findItemByName( %meshName SPC %oldSize );
	ShapeLab_DetailTree.removeMeshEntry( %meshName SPC %oldSize );
	%newId = ShapeLab_DetailTree.addMeshEntry( %meshName SPC %newSize );

	// Re-select the new entry if it was selected
	if ( %selected == %id )
	{
		ShapeLab_DetailTree.clearSelection();
		ShapeLab_DetailTree.selectItem( %newId );
	}
}
//------------------------------------------------------------------------------

//==============================================================================
function ShapeLab::onMeshAdded( %this, %meshName )
{
	// --- MISC ---
	ShapeLabEditor.refreshShape();
	ShapeLabEditor.updateNodeTransforms();

	// --- COLLISION WINDOW ---
	// Add object to target list if it does not already exist
	if ( !ShapeLab.isCollisionMesh( %meshName ) )
	{
		%objName = stripTrailingNumber( %meshName );
		%id = ShapeLab_CreateColRollout-->colTarget.findText( %objName );

		if ( %id == -1 )
			ShapeLab_CreateColRollout-->colTarget.add( %objName );
	}

	// --- DETAILS TAB ---
	%id = ShapeLab_DetailTree.addMeshEntry( %meshName );
	ShapeLab_DetailTree.clearSelection();
	ShapeLab_DetailTree.selectItem( %id );
}
