//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function ShapeLabDetails::onWake( %this )
{
	// Initialise popup menus
	%this-->bbType.clear();
	%this-->bbType.add( "None", 0 );
	%this-->bbType.add( "Billboard", 1 );
	%this-->bbType.add( "Z Billboard", 2 );
	%this-->AddShapeToDetailMenu.clear();
	%this-->AddShapeToDetailMenu.add( "current detail", 0 );
	%this-->AddShapeToDetailMenu.add( "new detail", 1 );
	%this-->AddShapeToDetailMenu.setSelected( 0, false );
	ShapeLab_DetailTree.onDefineIcons();
}
function ShapeLabDetails::onShapeSelectionChanged( %this )
{
	ShapeLabDetails.selectShape();
}
//==============================================================================
// ShapeLab -> Node Editing
//==============================================================================

//==============================================================================
// Update the GUI in response to a node being added
function ShapeLabDetailBtn::onClick( %this )
{
	%type = %this.internalName;

	switch$(%type)
	{
		case "AddNode":
			ShapeLabDetails.addNode();

		case "removeSelected":
			ShapeLabDetails.removeSelected();

		case "addMeshFromFile":
			ShapeLabDetails.addMeshFromFile();
	}
}
//------------------------------------------------------------------------------

//==============================================================================
function ShapeLabDetails::removeSelected( %this,%noUndo )
{
	%id = ShapeLab_DetailTree.getSelectedItem();

	if ( ShapeLab_DetailTree.isParentItem( %id ) )
		%this.removeDetailId(%id,%noUndo);
	else
		%this.removeMeshId(%id,%noUndo);

}
//------------------------------------------------------------------------------
