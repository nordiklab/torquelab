//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Handle a selection in the MissionGroup shape selector
function SLE_ShapeOptionsBook::onTabSelected( %this, %text,%id )
{
	ShapeLab.currentShapeOptionsPage = %id;
}
//------------------------------------------------------------------------------
