//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function ActionEditTrigger::done( %this )
{
	ShapeLab_TriggerList.updateItem(%this.oldFrame,%this.oldState,%this.frame,%this.state );

	if ( SL_ActiveSequence.seqName $= %this.seqName )
		ShapeLab.setActiveSequenceTriggers();

}
//------------------------------------------------------------------------------
//==============================================================================
//==============================================================================
// Add Trigger
//==============================================================================
//==============================================================================
// Add trigger
function ShapeLab::doAddTrigger( %this, %seqName, %frame, %state )
{
	%action = %this.createAction( ActionAddTrigger, "Add trigger" );
	%action.seqName = %seqName;
	%action.frame = %frame;
	%action.state = %state;
	%this.doAction( %action );
}
//------------------------------------------------------------------------------
//==============================================================================
function ActionAddTrigger::doit( %this )
{
	if ( ShapeLab.shape.addTrigger( %this.seqName, %this.frame, %this.state ) )
	{
		ShapeLab.setActiveSequenceTriggers();
		ShapeLabPropWindow.update_onTriggerAdded( %this.seqName, %this.frame, %this.state );
		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
//==============================================================================
function ActionAddTrigger::undo( %this )
{
	Parent::undo( %this );

	if ( ShapeLab.shape.removeTrigger( %this.seqName, %this.frame, %this.state ) )
		ShapeLabPropWindow.update_onTriggerRemoved( %this.seqName, %this.frame, %this.state );
}
//------------------------------------------------------------------------------

//==============================================================================
// Remove Trigger
//==============================================================================

//==============================================================================
// Remove trigger
function ShapeLab::doRemoveTrigger( %this, %seqName, %frame, %state )
{
	%action = %this.createAction( ActionRemoveTrigger, "Remove trigger" );
	%action.seqName = %seqName;
	%action.frame = %frame;
	%action.state = %state;
	%this.doAction( %action );
}
//------------------------------------------------------------------------------
//==============================================================================
function ActionRemoveTrigger::doit( %this )
{
	if ( ShapeLab.shape.removeTrigger( %this.seqName, %this.frame, %this.state ) )
	{
		ShapeLabPropWindow.update_onTriggerRemoved( %this.seqName, %this.frame, %this.state );
		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
//==============================================================================
function ActionRemoveTrigger::undo( %this )
{
	Parent::undo( %this );

	if ( ShapeLab.shape.addTrigger( %this.seqName, %this.frame, %this.state ) )
		ShapeLabPropWindow.update_onTriggerAdded( %this.seqName, %this.frame, %this.state );
}
//------------------------------------------------------------------------------

//==============================================================================
// Edit Trigger
//==============================================================================

//==============================================================================
// Edit trigger
function ShapeLab::doEditTrigger( %this, %seqName, %oldFrame, %oldState, %frame, %state )
{
	%action = ShapeLab.createAction( ActionEditTrigger, "Edit trigger" );
	%action.seqName = %seqName;
	%action.oldFrame = %oldFrame;
	%action.oldState = %oldState;
	%action.frame = %frame;
	%action.state = %state;
	%this.doAction( %action );
}
//------------------------------------------------------------------------------

//==============================================================================
function ActionEditTrigger::doit( %this )
{
	if ( ShapeLab.shape.addTrigger( %this.seqName, %this.frame, %this.state ) &&	ShapeLab.shape.removeTrigger( %this.seqName, %this.oldFrame, %this.oldState ) )
	{
		ShapeLab.onEditTrigger(%this.seqName,%this.oldFrame, %this.oldState, %this.frame, %this.state);

		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
//==============================================================================
function ActionEditTrigger::undo( %this )
{
	Parent::undo( %this );

	if ( ShapeLab.shape.addTrigger( %this.seqName, %this.oldFrame, %this.oldState ) && ShapeLab.shape.removeTrigger( %this.seqName, %this.frame, %this.state ) )
		ShapeLab_TriggerList.updateItem( %this.frame, %this.state, %this.oldFrame, %this.oldState );
}
//------------------------------------------------------------------------------
