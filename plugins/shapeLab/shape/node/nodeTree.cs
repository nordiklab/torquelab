//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
//Called from GuiShapeView when a node is selected
function ShapeLab::selectTreeNode( %this, %index )
{
	ShapeLab_NodeTree.clearSelection();

	if ( %index > 0 )
	{
		%name = ShapeLab.shape.getNodeName( %index );
		%id = ShapeLab_NodeTree.findItemByName( %name );

		if ( %id > 0 )
			ShapeLab_NodeTree.selectItem( %id );
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Scene Editor Params - Used set default settings and build plugins options GUI
//==============================================================================
//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
function ShapeLab::clearNodeTree( %this )
{
	ShapeLab_NodeTree.removeItem( 0 );
}
//------------------------------------------------------------------------------
//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
function sTree( %item )
{
	if (%item$= "")
		%item = ShapeLab_NodeTree.getFirstRootItem();

	//%item = ShapeLab_NodeTree.getChild(%id);
	while(%item > 0)
	{
		%checkItem = %item;
		%child = ShapeLab_NodeTree.getChild(%checkItem);

		if (%child > 0)
			sTree(%child);

		%item = ShapeLab_NodeTree.getNextSibling(%checkItem);
	}

}
//------------------------------------------------------------------------------

//==============================================================================
function ShapeLab_NodeTree::buildNodes( %this )
{

	ShapeLab_NodeTree.removeItem( 0 );
	%rootId = ShapeLab_NodeTree.insertItem( 0, "<root>", 0, "" );
	%count = ShapeLab.shape.getNodeCount();

	for ( %i = 0; %i < %count; %i++ )
	{
		%name = ShapeLab.shape.getNodeName( %i );

		if ( ShapeLab.shape.getNodeParentName( %name ) $= "" )
			ShapeLab_NodeTree.addNodeTree( %name );
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Plugin Object Callbacks - Called from TLab plugin management scripts
//==============================================================================
//==============================================================================
function ShapeLab_NodeTree::onClearSelection( %this )
{
	ShapeLab.setActiveNode("");
	//ShapeLab.onNodeSelectionChanged( -1 );
}
//------------------------------------------------------------------------------
//==============================================================================
function ShapeLab_NodeTree::onSelect( %this, %id )
{

	// Update the node name and transform controls
	if ( %id > 0 )
		%name = ShapeLab_NodeTree.getItemText( %id );
	else
		%name = "";

	ShapeLab_NodeTree.selectPress = true;
	ShapeLab.setActiveNode(%name);

	//%this.expandItem(%id,%expand);
	//ShapeLab_NodeTree.setIsExpanded(%expand);

	// Update orbit position if orbiting the selected node
	if ( ShapeLabEditor.orbitNode )
	{
		%name = %this.getItemText( %id );
		%transform = ShapeLab.shape.getNodeTransform( %name, 1 );
		ShapeLabEditor.setOrbitPos( getWords( %transform, 0, 2 ) );
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Determine the index of a node in the tree relative to its parent
function ShapeLab_NodeTree::getChildIndexByName( %this, %name )
{
	%parentName = ShapeLab.shape.getNodeParentName(%name);
	%parentId = %this.findItemByName( %parentName );

	if ( %childId <= 0 )
		return 0;   // bad!

	%childId = %this.getChild( %parentId );

	if ( %childId <= 0 )
		return 0;   // bad!

	%index = 0;

	while ( %childId != %id )
	{
		%childId = ShapeLab_NodeTree.getNextSibling( %childId );
		%index++;
	}

	return %index;
}
//------------------------------------------------------------------------------
//==============================================================================
// Add a node and its children to the node tree view
function ShapeLab_NodeTree::addNodeTree( %this, %nodeName )
{
	// Abort if already added => something dodgy has happened and we'd end up
	// recursing indefinitely
	if ( %this.findItemByName( %nodeName ) )
	{
		error( "Recursion error in ShapeLab_NodeTree::addNodeTree" );
		return 0;
	}

	// Find parent and add me to it
	%parentName = ShapeLab.shape.getNodeParentName( %nodeName );

	if ( %parentName $= "" )
		%parentName = "<root>";

	%parentId = %this.findItemByName( %parentName );
	%id = %this.insertItem( %parentId, %nodeName, 0, "" );
	// Add children
	%count = ShapeLab.shape.getNodeChildCount( %nodeName );

	for ( %i = 0; %i < %count; %i++ )
		%this.addNodeTree( ShapeLab.shape.getNodeChildName( %nodeName, %i ) );

	return %id;
}
//------------------------------------------------------------------------------
//==============================================================================
// Add a node and its children to the node tree view
function ShapeLab_NodeTree::onDeleteSelection( %this, %nodeName )
{
	%id = ShapeLab_NodeTree.getSelectedItem();

	if ( %id <= 0 )
		return;

	%name = ShapeLab_NodeTree.getItemText( %id );
	ShapeLabNodes.removeNode(%name);
}
//------------------------------------------------------------------------------

//==============================================================================
// Check if the mouse is released over another item for possible reparenting
function ShapeLab_NodeTree::onMouseUp(%this,%hitItemId)
{
	if (!ShapeLab_NodeTree.selectPress)
		return;

	ShapeLab_NodeTree.selectPress = false;
	%selected = ShapeLab_NodeTree.getSelectedItem(0);

	if(%selected $= %hitItemId || %selected $= "-1")
		return;

	//Now check if the select item is drop over a child, if so, abort reparenting
	//We need the name of the mouseUp Item Hit
	%droppedOnNode = ShapeLab_NodeTree.getItemText( %hitItemId );

	//This will return the itemID if %droppedOnNode is a child of selection or 0 if not
	if (ShapeLab_NodeTree.findChildItemByName(%selected,%droppedOnNode))
		return;

	//Proceed with reparent since it's not dropped on his own children
	%node = ShapeLab_NodeTree.getItemText( %selected );
	ShapeLab.doSetNodeParent(%node,%droppedOnNode);
}
//------------------------------------------------------------------------------

//==============================================================================
// Add a node and its children to the node tree view
function ShapeLab_NodeTree::onCellSelected( %this, %nodeName )
{

}
//------------------------------------------------------------------------------
//==============================================================================
// Add a node and its children to the node tree view
function ShapeLab_NodeTree::onCellHighlighted( %this, %nodeName )
{
	if (%this.cellHighlighted $= %nodeName)
		return;

	%this.cellHighlighted = %nodeName;

}
//------------------------------------------------------------------------------

