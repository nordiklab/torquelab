//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
//------------------------------------------------------------------------------
// Add Node to selected Shape
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function ShapeLabNodes::addNode( %this, %name )
{
	// Add a new node, using the currently selected node as the initial parent
	if ( %name $= "" )
		%name = (ShapeLab_AddNodeName.getText() !$= "") ? ShapeLab_AddNodeName.getText() : "NewNode";

	//Make sure we have a unique name
	%name = ShapeLab.getUniqueName( "node", %name );

	%id = ShapeLab_NodeTree.getSelectedItem();

	if ( %id <= 0 )
		%parent = "";
	else
		%parent = ShapeLab_NodeTree.getItemText( %id );

	ShapeLab.doAddNode( %name, %parent, "0 0 0 0 0 1 0" );
}
//------------------------------------------------------------------------------

//==============================================================================
// DO AddNode Action (With Undo support)
//==============================================================================
//==============================================================================
function ShapeLab::doAddNode( %this, %nodeName, %parentName, %transform )
{
	%action = %this.createAction( ActionAddNode, "Add node" );
	%action.nodeName = %nodeName;
	%action.parentName = %parentName;
	%action.transform = %transform;
	%this.doAction( %action );
}
//------------------------------------------------------------------------------
function ActionAddNode::doit( %this )
{
	if ( ShapeLab.shape.addNode( %this.nodeName, %this.parentName, %this.transform ) )
	{
		ShapeLab.onNodeAdded( %this.nodeName, -1 );
		return true;
	}

	return false;
}
//------------------------------------------------------------------------------
function ActionAddNode::undo( %this )
{
	Parent::undo( %this );

	if ( ShapeLab.shape.removeNode( %this.nodeName ) )
		ShapeLab.onNodeRemoved( %this.nodeName, 1 );
}
//------------------------------------------------------------------------------

//==============================================================================
//------------------------------------------------------------------------------
// Remove Node to selected Shape
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function ShapeLabNodes::removeNode( %this,%name )
{
	// Remove the node and all its children from the shape
	if (%name $= "")
	{
		%id = ShapeLab_NodeTree.getSelectedItem();

		if ( %id > 0 )
			%name = ShapeLab_NodeTree.getItemText( %id );
	}

	if (%name $= "")
		return LabMsgOk("No selected node","Please select desired node for removal and try again");

	ShapeLab.doRemoveShapeData( "Node", %name );
}
//------------------------------------------------------------------------------

//==============================================================================
// DO removeNode Action (With Undo support)
//==============================================================================

//==============================================================================
function ShapeLab::doRemoveNode( %this, %nodeName )
{
	%action = %this.createAction( ActionRemoveNode, "Remove node" );
	%action.nodeName =%nodeName;
	%action.nodeChildIndex = ShapeLab_NodeTree.getChildIndexByName( %nodeName );
	// Need to delete all child nodes of this node as well, so recursively collect
	// all of the names.
	%action.nameList = %this.getNodeNames( %nodeName, "" );
	%action.nameCount = getFieldCount( %action.nameList );

	for ( %i = 0; %i < %action.nameCount; %i++ )
		%action.names[%i] = getField( %action.nameList, %i );

	%this.doAction( %action );
}
//------------------------------------------------------------------------------
function ActionRemoveNode::doit( %this )
{
	for ( %i = 0; %i < %this.nameCount; %i++ )
		ShapeLab.shape.removeNode( %this.names[%i] );

	// Update GUI
	ShapeLab.onNodeRemoved( %this.nameList, %this.nameCount );
	return true;
}
//------------------------------------------------------------------------------
function ActionRemoveNode::undo( %this )
{
	Parent::undo( %this );
}
//------------------------------------------------------------------------------

//==============================================================================
//------------------------------------------------------------------------------
// Rename the node
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function ShapeLabNodes::renameNode( %this,%oldName )
{
	if (%oldName $= "")
	{
		%id = ShapeLab_NodeTree.getSelectedItem();

		if ( %id > 0 )
			%oldName = ShapeLab_NodeTree.getItemText( %id );
	}

	%newName = %this-->nodeName.getText();

	if ( %newName !$= "" && %oldName !$= "")
		ShapeLab.doRenameNode( %oldName, %newName );
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Rename node
function ShapeLab::doRenameNode( %this, %oldName, %newName )
{
	%action = %this.createAction( ActionRenameNode, "Rename node" );
	%action.oldName = %oldName;
	%action.newName = %newName;
	%this.doAction( %action );
}

function ActionRenameNode::doit( %this )
{
	if ( ShapeLab.shape.renameNode( %this.oldName, %this.newName ) )
	{
		ShapeLab.onNodeRenamed( %this.oldName, %this.newName );
		return true;
	}

	return false;
}

function ActionRenameNode::undo( %this )
{
	Parent::undo( %this );

	if ( ShapeLab.shape.renameNode( %this.newName, %this.oldName ) )
		ShapeLab.onNodeRenamed( %this.newName, %this.oldName );
}
//==============================================================================
//------------------------------------------------------------------------------
// Set the node parent
//------------------------------------------------------------------------------
//==============================================================================

//------------------------------------------------------------------------------
// Set node parent
function ShapeLab::doSetNodeParent( %this, %name, %parent )
{
	if ( %parent $= "<root>" )
		%parent = "";

	%action = %this.createAction( ActionSetNodeParent, "Set parent node" );
	%action.nodeName = %name;
	%action.parentName = %parent;
	%action.oldParentName = ShapeLab.shape.getNodeParentName( %name );
	%this.doAction( %action );
}

function ActionSetNodeParent::doit( %this )
{
	if ( ShapeLab.shape.setNodeParent( %this.nodeName, %this.parentName ) )
	{
		ShapeLab.onNodeParentChanged( %this.nodeName );
		return true;
	}

	return false;
}

function ActionSetNodeParent::undo( %this )
{
	Parent::undo( %this );

	if ( ShapeLab.shape.setNodeParent( %this.nodeName, %this.oldParentName ) )
		ShapeLab.onNodeParentChanged( %this.nodeName );
}

//==============================================================================
//------------------------------------------------------------------------------
// Edit Node Transform
//------------------------------------------------------------------------------
//==============================================================================
//==============================================================================
function ShapeLabNodes::updateNodeTransform( %this,%name )
{
	if (%name $= "")
	{
		%id = ShapeLab_NodeTree.getSelectedItem();

		if ( %id > 0 )
			%name = ShapeLab_NodeTree.getItemText( %id );
	}

	if (%name $= "")
		return;

	// Get the node transform from the gui
	%pos = %this-->nodePosition.getText();
	%rot = %this-->nodeRotation.getText();
	%txfm = %pos SPC %rot;
	%isWorld = ShapeLabNodes-->worldTransform.getValue();

	// Do a quick sanity check to avoid setting wildly invalid transforms
	for ( %i = 0; %i < 7; %i++ )    // "x y z aa.x aa.y aa.z aa.angle"
	{
		if ( getWord( %txfm, %i ) $= "" )
			return;
	}

	ShapeLab.doEditNodeTransform( %name, %txfm, %isWorld, -1 );
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Edit node transform
function ShapeLab::doEditNodeTransform( %this, %nodeName, %newTransform, %isWorld, %gizmoID )
{
	// If dragging the 3D gizmo, combine all movement into a single action. Undoing
	// that action will return the node to where it was when the gizmo drag started.
	%last = ShapeLabUndoManager.getUndoAction( ShapeLabUndoManager.getUndoCount() - 1 );

	if ( ( %last != -1 ) && ( %last.class $= ActionEditNodeTransform ) &&
	      ( %last.nodeName $= %nodeName ) && ( %last.gizmoID != -1 ) && ( %last.gizmoID == %gizmoID ) )
	{
		// Use the last action to do the edit, and modify it so it only applies
		// the latest transform
		%last.newTransform = %newTransform;
		%last.isWorld = %isWorld;
		%last.doit();
		ShapeLab.setDirty( true );
	}
	else
	{
		%action = %this.createAction( ActionEditNodeTransform, "Edit node transform" );
		%action.nodeName = %nodeName;
		%action.newTransform = %newTransform;
		%action.isWorld = %isWorld;
		%action.gizmoID = %gizmoID;
		%action.oldTransform = %this.shape.getNodeTransform( %nodeName, %isWorld );
		%this.doAction( %action );
	}
}

function ActionEditNodeTransform::doit( %this )
{
	ShapeLab.shape.setNodeTransform( %this.nodeName, %this.newTransform, %this.isWorld );
	ShapeLab.onNodeTransformChanged();
	return true;
}

function ActionEditNodeTransform::undo( %this )
{
	Parent::undo( %this );
	ShapeLab.shape.setNodeTransform( %this.nodeName, %this.oldTransform, %this.isWorld );
	ShapeLab.onNodeTransformChanged();
}

