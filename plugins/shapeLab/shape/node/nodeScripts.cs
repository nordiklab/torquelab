//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function ShapeLab::setActiveNode( %this, %name )
{
	ShapeLab.activeNodeName = %name;

	if (%name $= "")
	{

		// Disable delete button and edit boxes
		if ( ShapeLab.currentMainOptionsPage $= "0" )
			ShapeLabPropWindow-->deleteBtn.setActive( false );

		ShapeLabNodes-->nodeName.setActive( false );
		ShapeLabNodes-->nodePosition.setActive( false );
		ShapeLabNodes-->nodeRotation.setActive( false );
		ShapeLabNodes-->nodeName.setText( "" );
		ShapeLabNodes-->nodePosition.setText( "" );
		ShapeLabNodes-->nodeRotation.setText( "" );
		ShapeLabEditor.selectedNode = -1;
		return;

	}

	// Enable delete button and edit boxes
	if ( ShapeLab.currentMainOptionsPage $= "0" )
		ShapeLabPropWindow-->deleteBtn.setActive( true );

	ShapeLabNodes-->nodeName.setActive( true );
	ShapeLabNodes-->nodePosition.setActive( true );
	ShapeLabNodes-->nodeRotation.setActive( true );

	ShapeLabNodes-->nodeName.setText( %name );
	//Build parent node menu
	ShapeLabNodeParentMenu.build(%name);

	if ( ShapeLabNodes.isWorldTransform )
	{
		// Global transform
		%txfm = ShapeLab.shape.getNodeTransform( %name, 1 );
		ShapeLabNodes-->nodePosition.setText( getWords( %txfm, 0, 2 ) );
		ShapeLabNodes-->nodeRotation.setText( getWords( %txfm, 3, 6 ) );
	}
	else
	{
		// Local transform (relative to parent)
		%txfm = ShapeLab.shape.getNodeTransform( %name, 0 );
		ShapeLabNodes-->nodePosition.setText( getWords( %txfm, 0, 2 ) );
		ShapeLabNodes-->nodeRotation.setText( getWords( %txfm, 3, 6 ) );
	}

	ShapeLabEditor.selectedNode = ShapeLab.shape.getNodeIndex( %name );
}
//------------------------------------------------------------------------------
//==============================================================================
// ShapeLab -> Node Editing
//==============================================================================

//==============================================================================
// Update the GUI in response to a node being added
function ShapeLabNodeBtn::onClick( %this )
{
	%type = %this.internalName;

	switch$(%type)
	{
		case "AddNode":
			ShapeLabNodes.addNode();

		case "removeNode":
			ShapeLabNodes.removeNode();
	}
}
//------------------------------------------------------------------------------

//==============================================================================
function ShapeLabNodeParentMenu::build( %this, %name )
{
	ShapeLabNodeParentMenu.clear();
	// Node parent list => ancestor and sibling nodes only (can't re-parent to a descendent)
	%parentNames = ShapeLab.getNodeNames( "", "<root>", %name );
	%count = getWordCount( %parentNames );

	for ( %i = 0; %i < %count; %i++ )
		ShapeLabNodeParentMenu.add( getWord(%parentNames, %i), %i );

	%pName = ShapeLab.shape.getNodeParentName( %name );

	if ( %pName $= "" )
		%pName = "<root>";

	ShapeLabNodeParentMenu.setText( %pName );
}
//------------------------------------------------------------------------------
//==============================================================================
// Selected Node Info Scripts
//==============================================================================

//==============================================================================
function ShapeLabNodeParentMenu::onSelect( %this, %id, %text )
{
	%id = ShapeLab_NodeTree.getSelectedItem();

	if ( %id > 0 )
	{
		%name = ShapeLab_NodeTree.getItemText( %id );
		ShapeLab.doSetNodeParent( %name, %text );
	}
}
//------------------------------------------------------------------------------

function ShapeLab::toggleTransformMode( %this )
{
	ShapeLabNodes.isWorldTransform = !ShapeLabNodes.isWorldTransform;
	ShapeLabNodes.isObjectTransform = !ShapeLabNodes.isWorldTransform;
}
function ShapeLab::changeNodeTransformMode( %this, %mode )
{
	LabToolbarStack-->transformToggleBtn.setStateOn(ShapeLabNodes.isWorldTransform);
	%isWorld = (%mode $= "World") ? true : false;
	%id = ShapeLab_NodeTree.getSelectedItem();

	if ( %id > 0 )
		%nodeName = ShapeLab_NodeTree.getItemText( %id );
	else
		return;

	//ShapeLabNodes.isWorldTransform = %isWorld;
	//ShapeLabNodes.isObjectTransform = !%isWorld;
	%transform = ShapeLab.shape.getNodeTransform( %nodeName, ShapeLabNodes.isWorldTransform );
	ShapeLabNodes-->nodePosition.setText( getWords( %transform, 0, 2 ) );
	ShapeLabNodes-->nodeRotation.setText( getWords( %transform, 3, 6 ) );
}
//------------------------------------------------------------------------------
