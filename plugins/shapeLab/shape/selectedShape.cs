//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function ShapeLabDetails::selectedShapeChanged( %this )
{
	ShapeLab_DetailTree.clear();
	// --- DETAILS TAB ---
	// Add detail levels and meshes to tree
	ShapeLab_DetailTree.clearSelection();
	ShapeLab_DetailTree.removeItem( 0 );
	%root = ShapeLab_DetailTree.insertItem( 0, "<root>", "", "" );

	if (!isObject(ShapeLab.shape))
		return;

	%objCount = ShapeLab.shape.getObjectCount();

	for ( %i = 0; %i < %objCount; %i++ )
	{
		%objName = ShapeLab.shape.getObjectName( %i );
		%meshCount = ShapeLab.shape.getMeshCount( %objName );

		for ( %j = 0; %j < %meshCount; %j++ )
		{
			%meshName = ShapeLab.shape.getMeshName( %objName, %j );
			ShapeLab_DetailTree.addMeshEntry( %meshName, 1 );
		}
	}

	// Initialise object node list
	ShapeLabDetails-->objectNode.clear();
	ShapeLabDetails-->objectNode.add( "<root>" );
	%nodeCount = ShapeLab.shape.getNodeCount();

	for ( %i = 0; %i < %nodeCount; %i++ )
		ShapeLabDetails-->objectNode.add( ShapeLab.shape.getNodeName( %i ) );
}
//------------------------------------------------------------------------------
