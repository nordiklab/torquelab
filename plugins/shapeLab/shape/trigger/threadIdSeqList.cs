//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function ShapeLabThreadViewer::onAddThread( %this )
{
	ShapeLabEditor.addThread();
	ShapeLab_ThreadIdList.addRow( %this.threadID++, ShapeLab_ThreadIdList.rowCount() );
	//ShapeLab_ThreadIdList.setSelectedRow( ShapeLab_ThreadIdList.rowCount()-1 );
}

function ShapeLabThreadViewer::onRemoveThread( %this )
{
	if ( ShapeLab_ThreadIdList.rowCount() > 1 )
	{
		// Remove the selected thread
		%row = ShapeLab_ThreadIdList.getSelectedRow();
		ShapeLabEditor.removeThread( %row );
		ShapeLab_ThreadIdList.removeRow( %row );
		// Update list (threads are always numbered 0-N)
		%rowCount = ShapeLab_ThreadIdList.rowCount();

		for ( %i = %row; %i < %rowCount; %i++ )
			ShapeLab_ThreadIdList.setRowById( ShapeLab_ThreadSeqList.getRowId( %i ), %i );

		// Select the next thread
		if ( %row >= %rowCount )
			%row = %rowCount - 1;

		ShapeLab_ThreadIdList.setSelectedRow( %row );
	}
}

//==============================================================================
// Thread ID List
//==============================================================================
function ShapeLab_ThreadIdList::onSelect( %this, %row, %text )
{

	if (ShapeLabEditor.activeThread $= ShapeLab_ThreadIdList.getSelectedRow())
	{
		return;
	}

	ShapeLab.threadIdSeq[ShapeLabEditor.activeThread] = %seqName;

	if (ShapeLabEditor.activeThread !$= ShapeLab_ThreadIdList.getSelectedRow())
		ShapeLabEditor.activeThread = ShapeLab_ThreadIdList.getSelectedRow();

	// Select the active thread's sequence in the list
	%seqName = ShapeLabEditor.getThreadSequence();

	if ( %seqName $= "" )
		%seqName = "<rootpose>";
	else if ( startswith( %seqName, "__proxy__" ) )
		%seqName = ShapeLab.getUnproxyName( %seqName );

	%this.threadSeq[%row] = %seqName;
	%seqIndex = ShapeLab_ThreadSeqList.findTextIndex( %seqName );
	//if (ShapeLab_ThreadSeqList.getSelectedRow() !$= %seqIndex)
	ShapeLab_ThreadSeqList.setSelectedRow( %seqIndex );

	SL_ActiveThreadInfo-->activeThreadText.setText("Selected Thread Slot:\c1 "@ShapeLabEditor.activeThread);

	// Update the playback controls
	switch ( ShapeLabEditor.threadDirection )
	{
		case -1:
			ShapeLabPreview-->playBkwdBtn.performClick();

		case 0:
			ShapeLabPreview-->pauseBtn.performClick();

		case 1:
			ShapeLabPreview-->playFwdBtn.performClick();
	}

	ShapeLabToggleButtonValue( ShapeLabPreview-->pingpong, ShapeLabEditor.threadPingPong );
}

function SLE_ThreadTransToMenu::onSelect( %this, %id, %text )
{
}
function SLE_ThreadTransStateMenu::onSelect( %this, %id, %text )
{
}

//==============================================================================
// Thread ID Sequence List
//==============================================================================
function ShapeLab_ThreadSeqList::onSelect( %this, %row, %text )
{

	%duration = SLE_ThreadSettings-->transitionTime.getText();
	SL_ActiveThreadInfo-->activeThreadText.setText("Selected Thread Slot:\c1 "@ShapeLabEditor.activeThread);
	ShapeLabEditor.setThreadSequence( getField(%text,0), %duration, ShapeLabEditor.threadPos, 0 );
	ShapeLab.setActiveSequence( getField(%text,0));

	if (%this.threadSeq[ShapeLabEditor.activeThread] !$=  %text)
	{
		//if (ShapeLab_ThreadIdList.getSelectedId()
		//ShapeLab_ThreadIdList.setSelectedById( %this.getSelectedId() );
	}

	//ShapeLabPreview.setSequence(%text);
}
