//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$TLab_PluginName_["ShapeLab"] = "Shape Lab";
//------------------------------------------------------------------------------
// Shape Lab
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

function ShapeLab::create( %this )
{

  	new ScriptObject(SLab);
	info("Plugin","->","Initializing","Shape Lab");
	newScriptObject("ShapeLab");

	execShapeLab(true);
	//Lab.createPlugin("ShapeLab","Shape Lab");
	Lab.addPluginGui("ShapeLab",ShapeLabTools);
	Lab.addPluginEditor("ShapeLab",ShapeLabPreviewGui);
	//%map = Lab.addPluginEditor("ShapeLab",ShapeLabPreview,true);
	Lab.addPluginToolbar("ShapeLab",ShapeLabToolbar);
	Lab.addPluginPalette("ShapeLab",   ShapeLabPalette);
	Lab.addPluginDlg("ShapeLab",   ShapeLabDialogs);
	ShapeLabPlugin.commonPalette = "Select Move Rotate";
	ShapeLabPlugin.editorGui = ShapeLabEditor;
	// Add windows to editor gui
	%map = Lab.addPluginMap("ShapeLab");
	//%map = new ActionMap();
	%map.bindCmd( keyboard, "escape", "LabPluginArray->SceneEditorPalette.performClick();", "" );
	%map.bindCmd( keyboard, "1", "ShapeLabNoneModeBtn.performClick();", "" );
	%map.bindCmd( keyboard, "2", "ShapeLabMoveModeBtn.performClick();", "" );
	%map.bindCmd( keyboard, "3", "ShapeLabRotateModeBtn.performClick();", "" );
	//%map.bindCmd( keyboard, "4", "ShapeLabScaleModeBtn.performClick();", "" ); // not needed for the shape editor
	%map.bindCmd( keyboard, "n", "ShapeLabToolbar->showNodes.performClick();", "" );
	%map.bindCmd( keyboard, "t", "ShapeLabToolbar->ghostMode.performClick();", "" );
	%map.bindCmd( keyboard, "r", "ShapeLabToolbar->wireframeMode.performClick();", "" );
	%map.bindCmd( keyboard, "f", "ShapeLabToolbar->fitToShapeBtn.performClick();", "" );
	%map.bindCmd( keyboard, "g", "ShapeLabToolbar->showGridBtn.performClick();", "" );
	%map.bindCmd( keyboard, "h", "ShapeLabPropWindow->tabBook.selectPage( 2 );", "" ); // Load help tab
	%map.bindCmd( keyboard, "l", "ShapeLabPropWindow->tabBook.selectPage( 1 );", "" ); // load Library Tab
	%map.bindCmd( keyboard, "j", "ShapeLabPropWindow->tabBook.selectPage( 0 );", "" ); // load scene object Tab
	%map.bindCmd( keyboard, "SPACE", "ShapeLabPreview.togglePause();", "" );
	%map.bindCmd( keyboard, "i", "ShapeLabSequences.onEditSeqInOut(\"in\", ShapeLabSeqSlider.getValue());", "" );
	%map.bindCmd( keyboard, "o", "ShapeLabSequences.onEditSeqInOut(\"out\", ShapeLabSeqSlider.getValue());", "" );
	%map.bindCmd( keyboard, "shift -", "ShapeLabSeqSlider.setValue(ShapeLabPreview-->seqIn.getText());", "" );
	%map.bindCmd( keyboard, "shift =", "ShapeLabSeqSlider.setValue(ShapeLabPreview-->seqOut.getText());", "" );
	%map.bindCmd( keyboard, "=", "ShapeLabPreview-->stepFwdBtn.performClick();", "" );
	%map.bindCmd( keyboard, "-", "ShapeLabPreview-->stepBkwdBtn.performClick();", "" );
	ShapeLabPlugin.map = %map;

}

//------------------------------------------------------------------------------

function ShapeLab::destroy( %this )
{

}

//----------------------------------------------------------------

//==============================================================================
// Load the Scene Editor Plugin scripts, load Guis if %loadgui = true
function execShapeLab(%loadGui)
{
	if (%loadGui)
	{
		exec("tlab/plugins/shapeLab/gui/shapeLabPreviewWindow.gui");
		exec("tlab/plugins/shapeLab/gui/ShapeLabDialogs.gui");
		exec("tlab/plugins/shapeLab/gui/shapeLabToolbar.gui");
		exec("tlab/plugins/shapeLab/gui/shapeLabPalette.gui");
		exec("tlab/plugins/shapeLab/gui/ShapeLabTools.gui");
	}

	//exec("./scripts/shapeLab.cs");
	//exec("./scripts/shapeLabHints.cs");
	//exec("./scripts/shapeLabActions.cs");
	//exec("./scripts/shapeLabUtility.cs");
	//exec("tlab/plugins/shapeLab/scripts/commonFunctions.cs");
	exec("tlab/plugins/shapeLab/ShapeLabPlugin.cs");
	exec("tlab/plugins/shapeLab/ShapeLabTools.cs");
	exec("tlab/plugins/shapeLab/ShapeLabEditor.cs");
	exec("tlab/plugins/shapeLab/ShapeLab.cs");

	execPattern("tlab/plugins/shapeLab/common/*.cs");
	execPattern("tlab/plugins/shapeLab/shape/*.cs");
	execPattern("tlab/plugins/shapeLab/book/*.cs");

}
//------------------------------------------------------------------------------

function destroyShapeLab()
{
}

function ShapeLabToggleButtonValue(%ctrl, %value)
{
	if ( %ctrl.getValue() != %value )
		%ctrl.performClick();
}

function shapeLabWireframeMode()
{
	return;
	$gfx::wireframe = !$gfx::wireframe;
	ShapeLabToolbar-->wireframeMode.setStateOn($gfx::wireframe);
}
