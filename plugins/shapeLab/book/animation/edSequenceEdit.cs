//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// ShapeLab -> Sequence Editing
//==============================================================================

function ShapeLabPropWindow::onWake( %this )
{
	ShapeLab_TriggerList.triggerId = 1;
	ShapeLab_TriggerList.clear();
	ShapeLab_TriggerList.addRow( -1, "-1" TAB "Frame" TAB "Trigger" TAB "State" );
	ShapeLab_TriggerList.setRowActive( -1, false );
}

function ShapeLabPropWindow::update_onSeqSelectionChanged( %this )
{
}

// Update the GUI in response to a sequence being added
function ShapeLabPropWindow::update_onSequenceAdded( %this, %seqName, %oldIndex )
{
	// --- MISC ---
	ShapeLab.updateHints("Anim");
	ShapeLab.addSequencePill(%seqName);

}
function ShapeLabAnim_SeqPillMenu::onSelect( %this, %id, %text )
{
	if ( %text $= "Browse..." )
	{
		// Reset menu text
		%seqName = ShapeLabSequenceList.getSelectedName();
		%seqFrom = rtrim( getFields( ShapeLab.getSequenceSource( %seqName ), 0, 1 ) );
		%this.setText( %seqFrom );
		%startAt = ShapeLab.shape.baseShape; //Start at current loaded shape path

		if (isFile(ShapeLab.currentSeqPath))
			%startAt = ShapeLab.currentSeqPath;

		getLoadFilename( "Anim Files|*.dae;*.dsq|COLLADA Files|*.dae|DSQ Files|*.dsq|Google Earth Files|*.kmz", %this @ ".onBrowseSelect", %startAt );
	}
	else
	{
		ShapeLabSequences.onEditSequenceSource( %text );
	}
}

function ShapeLabAnim_SeqPillMenu::onBrowseSelect( %this, %path )
{
	%path = makeRelativePath( %path, getMainDotCSDir() );
	%this.lastPath = %path;
	%this.setText( %path );
	ShapeLabSequences.onEditSequenceSource( %path );
}
