//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// The ShapeLab uses its own UndoManager

function ShapeLabUndoManager::onUndo( %this )
{
}

function ShapeLabUndoManager::onRedo( %this )
{
}

function ShapeLabUndoManager::onAddUndo( %this )
{

}

function ShapeLabUndoManager::onRemoveUndo( %this )
{
}

function ShapeLabUndoManager::onClear( %this )
{
}

function ShapeLabUndoManager::updateUndoMenu( %this, %editMenu )
{
	Lab.updateUndoMenu(%this);
}

//------------------------------------------------------------------------------
// Helper functions for creating and applying GUI operations

function ShapeLab::createAction( %this, %class, %desc )
{
	pushInstantGroup();
	%action = new UndoScriptAction()
	{
		class = %class;
		superClass = BaseShapeLabAction;
		actionName = %desc;
		done = 0;
	};
	popInstantGroup();
	return %action;
}

function ShapeLab::doAction( %this, %action )
{

	if ( %action.doit() )
	{
		%action.done();
		ShapeLab.setDirty( true );
		%action.addToManager( ShapeLabUndoManager );
	}
	else
	{
		LabMsgOK( "Error", %action.actionName SPC "failed. Check the console for error messages.", "" );
	}
}

function BaseShapeLabAction::redo( %this )
{
	// Default redo action is the same as the doit action
	if ( %this.doit() )
	{
		ShapeLab.setDirty( true );
	}
	else
	{
		LabMsgOK( "Error", "Redo" SPC %this.actionName SPC "failed. Check the console for error messages.", "" );
	}
}

function BaseShapeLabAction::undo( %this )
{
	ShapeLab.setDirty( true );
}

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Update bounds
function ShapeLab::doSetBounds( %this )
{
	%action = %this.createAction( ActionSetBounds, "Set bounds" );
	%action.oldBounds = ShapeLab.shape.getBounds();
	%action.newBounds = ShapeLabEditor.computeShapeBounds();
	%this.doAction( %action );
}

function ActionSetBounds::doit( %this )
{
	return ShapeLab.shape.setBounds( %this.newBounds );
}

function ActionSetBounds::undo( %this )
{
	Parent::undo( %this );
	ShapeLab.shape.setBounds( %this.oldBounds );
}
