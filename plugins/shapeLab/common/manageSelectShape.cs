//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Handle a selection in the shape selector list
function ShapeLab::selectFilePath( %this, %path )
{
	ShapeLabPlugin.openShape(%path);
	return;

	// Prompt user to save the old shape if it is dirty
	if ( ShapeLab.isDirty() )
	{
		%cmd = "ColladaImportDlg.showDialog( \"" @ %path @ "\", \"ShapeLab.selectShape( \\\"" @ %path @ "\\\", ";
		LabMsgYesNoCancel( "selectFilePath Shape Modified", "Would you like to save your changes?", %cmd @ "true );\" );", %cmd @ "false );\" );" );
	}
	else
	{
		%cmd = "ShapeLab.selectShape( \"" @ %path @ "\", false );";
		ColladaImportDlg.showDialog( %path, %cmd );
	}
}

//------------------------------------------------------------------------------
//==============================================================================
// Handle a selection in the MissionGroup shape selector
function ShapeLabPlugin::onSelectObject( %this, %obj )
{
	%path = ShapeLab.getObjectShapeFile( %obj );

	if ( %path !$= "" )
		ShapeLabPropWindow.onSelect( %path );

	// Set the object type (for required nodes and sequences display)
	%objClass = %obj.getClassName();
	%hintId = -1;
	%count = ShapeLabHintGroup.getCount();

	for ( %i = 0; %i < %count; %i++ )
	{
		%hint = ShapeLabHintGroup.getObject( %i );

		if ( %objClass $= %hint.objectType )
		{
			%hintId = %hint;
			break;
		}
		else if ( isMemberOfClass( %objClass, %hint.objectType ) )
		{
			%hintId = %hint;
		}
	}

	ShapeLabHintMenu.setSelected( %hintId );
}
//------------------------------------------------------------------------------
/*//==============================================================================
// Handle a selection in the MissionGroup shape selector
function ShapeLabPlugin::onSceneTreeSelected( %this, %obj ) {
	%path = ShapeLab.getObjectShapeFile( %obj );
	if (%path !$= "")
		ShapeLabPlugin.openShape(%path);

}
//------------------------------------------------------------------------------*/
//==============================================================================
// Select Object Functions
//==============================================================================
//==============================================================================
// Select the current WorldEditor selection
function ShapeLab::selectWorldEditorShape( %this)
{
	%count = EWorldEditor.getSelectionSize();

	for (%i = 0; %i < %count; %i++)
	{
		%obj = EWorldEditor.getSelectedObject(%i);
		%shapeFile = ShapeLab.getObjectShapeFile(%obj);

		//If we have a valid shapefile, make the object the current selection
		if (%shapeFile !$= "")
		{
			//if (!isObject(ShapeLab.shape) || (ShapeLab.shape.baseShape !$= %shapeFile)) {
			//Clear the tree in case and make the current object selected
			//Scene.selectObject(%obj,true);
			//ShapeLabShapeTreeView.clearSelection();
			//ShapeLabShapeTreeView.onSelect(%obj);
			//Set the Editor shape
			ShapeLab.selectShape(%shapeFile, ShapeLab.isDirty());
			// 'fitToShape' only works after the GUI has been rendered, so force a repaint first
			Canvas.repaint();
			ShapeLabEditor.fitToShape();
			break; //Only one shape can be selected at a time so leave
			//}
		}
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Handle a selection in the shape selector list
function ShapeLabPropWindow::onSelect( %this, %path )
{
	// Prompt user to save the old shape if it is dirty
	if (ShapeLab.currentFilePath $= %path)
	{
		//	return;
	}

	if ( ShapeLab.isDirty() )
	{
		%cmd = "ColladaImportDlg.showDialog( \"" @ %path @ "\", \"ShapeLab.selectShape( \\\"" @ %path @ "\\\", ";
		LabMsgYesNoCancel( "onSelect Shape Modified", "Would you like to save your changes?", %cmd @ "true );\" );", %cmd @ "false );\" );" );
	}
	else
	{
		%cmd = "ShapeLab.selectShape( \"" @ %path @ "\", false );";
		ColladaImportDlg.showDialog( %path, %cmd );
	}
}

//------------------------------------------------------------------------------
//==============================================================================
function ShapeLab::selectShape( %this, %path, %saveOld )
{
	if (ShapeLab.currentFilePath $= %path)
	{
		//return;
	}

	if ( %saveOld )
	{
		// Save changes to a TSShapeConstructor script
		%this.saveChanges();
	}
	else if ( ShapeLab.isDirty() )
	{
		// Purge all unsaved changes
		%oldPath = ShapeLab.shape.baseShape;
		ShapeLab.shape.delete();
		ShapeLab.shape = 0;
		reloadResource( %oldPath );   // Force game objects to reload shape
	}

	ShapeLabEditor.setModel( "" );
	ShapeLab.currentFileName = "";
	ShapeLab.selectedSequence = "";

	%path = strreplace(%path,"//","/");

	// Initialise the shape preview window
	if ( !ShapeLabEditor.setModel( %path ) )
	{
		LabMsgOK( "Error", "Failed to load '" @ %path @ "'. Check the console for error messages." );
		return;
	}

	ShapeLab.currentFilePath = %path;
	ShapeLab.currentSeqPath = "";
	ShapeLab.currentFileName = fileBase(%path)@fileExt(%path);
	ShapeLabEditor.fitToShape();
	ShapeLabUndoManager.clearAll();
	ShapeLab.setDirty( false );
	// Get ( or create ) the TSShapeConstructor object for this shape
	ShapeLab.shape = findConstructor(%path );

	if ( ShapeLab.shape <= 0 )
	{
		ShapeLab.shape = createConstructor( %path );

		if ( ShapeLab.shape <= 0 )
		{
			error( "ShapeLab: Error - could not select " @ %path );
			return;
		}
	}

	// Initialise the editor windows
	ShapeLabDetails.selectedShapeChanged();
	ShapeLabMountWindow.onShapeSelectionChanged();

	ShapeLabCollisions.onShapeSelectionChanged();
	ShapeLabPropWindow.onShapeSelectionChanged();
	ShapeLabEditor.onShapeSelectionChanged();

	// Update object type hints
	ShapeLab.updateHints();
	// Update editor status bar
	EditorGuiStatusBar.setSelection( %path );

	ModelLab.setActiveShape(ShapeLab.shape);
}
//------------------------------------------------------------------------------
/*
//==============================================================================
// Handle a selection in the MissionGroup shape selector
function ShapeLabShapeTreeView::onSelect( %this, %obj ) {
	%path = ShapeLab.getObjectShapeFile( %obj );

	if ( %path !$= "" )
		ShapeLabSelectWindow.onSelect( %path );

	// Set the object type (for required nodes and sequences display)
	%objClass = %obj.getClassName();
	%hintId = -1;
	%count = ShapeLabHintGroup.getCount();

	for ( %i = 0; %i < %count; %i++ ) {
		%hint = ShapeLabHintGroup.getObject( %i );

		if ( %objClass $= %hint.objectType ) {
			%hintId = %hint;
			break;
		} else if ( isMemberOfClass( %objClass, %hint.objectType ) ) {
			%hintId = %hint;
		}
	}

	ShapeLabHintMenu.setSelected( %hintId );
}
//------------------------------------------------------------------------------
*/
//==============================================================================
// Open a Shape file
//==============================================================================

//==============================================================================
/*
function ShapeLabPlugin::open(%this, %filename) {

	// Select the new shape
	if (isObject(ShapeLab.shape) && (ShapeLab.shape.baseShape $= %filename)) {
		// Shape is already selected => re-highlight the selected material if necessary
		ShapeLabMaterials.updateSelectedMaterial(ShapeLabMaterials-->highlightMaterial.getValue());
	} else if (%filename !$= "") {
		ShapeLab.selectShape(%filename, ShapeLab.isDirty());
		// 'fitToShape' only works after the GUI has been rendered, so force a repaint first
		Canvas.repaint();
		ShapeLabEditor.fitToShape();
	}
}
*/
//==============================================================================
// Open new shape
//==============================================================================

// Update the GUI in response to the shape selection changing
function ShapeLabPropWindow::onShapeSelectionChanged( %this )
{
	// --- NODES TAB ---
	ShapeLab_NodeTree.buildNodes();

	ShapeLab.setActiveNode("");
	//ShapeLab.onNodeSelectionChanged( -1 );    // no node selected
	// --- SEQUENCES TAB ---
	ShapeLab.updateShapeSequenceData();

	/*
	ShapeLab_SeqPillStack.clear(); //Clear the new sequence lisitng stack
	ShapeLabSequenceList.clear();
	ShapeLabSequenceList.addRow( -1, "Name" TAB "Cyclic" TAB "Blend" TAB "Frames" TAB "Priority" );
	ShapeLabSequenceList.setRowActive( -1, false );
	ShapeLabSequenceList.addRow( 0, "<rootpose>" TAB "" TAB "" TAB "" TAB "" );
	%count = ShapeLab.shape.getSequenceCount();

	for ( %i = 0; %i < %count; %i++ ) {
		%name = ShapeLab.shape.getSequenceName( %i );

		// Ignore __backup__ sequences (only used by editor)
		if ( !startswith( %name, "__backup__" ) ) {
			ShapeLabSequenceList.addItem( %name );
			ShapeLab.addSequencePill(%name);
		}
	}

	*/

	// --- MATERIALS TAB ---
	ShapeLab.updateMaterialList();
}
