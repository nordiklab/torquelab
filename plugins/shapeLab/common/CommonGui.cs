//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function SLE_MainOptionsBook::onTabSelected( %this, %name, %index )
{
	ShapeLab.currentMainOptionsPage = %index;
	%this.activePage = %name;

	switch$ ( %name )
	{
		case "Seq":
			ShapeLabPropWindow-->newBtn.ToolTip = "Add new sequence";
			ShapeLabPropWindow-->newBtn.Command = "ShapeLab.onAddSequence();";
			ShapeLabPropWindow-->newBtn.setActive( true );
			ShapeLabPropWindow-->deleteBtn.ToolTip = "Delete selected sequence (cannot be undone)";
			ShapeLabPropWindow-->deleteBtn.Command = "ShapeLabSequences.onDeleteSequence();";
			ShapeLabPropWindow-->deleteBtn.setActive( true );

		case "Node":
			ShapeLabPropWindow-->newBtn.ToolTip = "Add new node";
			ShapeLabPropWindow-->newBtn.Command = "ShapeLabNodes.addNode(ShapeLab_AddNodeName.getText());";
			ShapeLabPropWindow-->newBtn.setActive( true );
			ShapeLabPropWindow-->deleteBtn.ToolTip = "Delete selected node (cannot be undone)";
			ShapeLabPropWindow-->deleteBtn.Command = "ShapeLabNodes.removeNode();";
			ShapeLabPropWindow-->deleteBtn.setActive( true );

		case "Detail":
			ShapeLabPropWindow-->newBtn.ToolTip = "";
			ShapeLabPropWindow-->newBtn.Command = "";
			ShapeLabPropWindow-->newBtn.setActive( false );
			ShapeLabPropWindow-->deleteBtn.ToolTip = "Delete the selected mesh or detail level (cannot be undone)";
			ShapeLabPropWindow-->deleteBtn.Command = "ShapeLabDetails.removeSelected();";
			ShapeLabPropWindow-->deleteBtn.setActive( true );

		case "Mat":
			ShapeLabPropWindow-->newBtn.ToolTip = "";
			ShapeLabPropWindow-->newBtn.Command = "";
			ShapeLabPropWindow-->newBtn.setActive( false );
			ShapeLabPropWindow-->deleteBtn.ToolTip = "";
			ShapeLabPropWindow-->deleteBtn.Command = "";
			ShapeLabPropWindow-->deleteBtn.setActive( false );
			// For some reason, the header is not resized correctly until the Materials tab has been
			// displayed at least once, so resize it here too
			ShapeLabMaterials-->materialListHeader.setExtent( getWord( ShapeLabMaterialList.extent, 0 ) SPC "19" );
	}
}
