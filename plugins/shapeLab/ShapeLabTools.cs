//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$Cfg_Plugins_ShapeLab_UseSimplifiedSystem = true;
//==============================================================================
// SceneEditorTools Frame Set Scripts
//==============================================================================
function ShapeLabTools::sePanelVisibility( %this )
{
	%panel = %checkbox.panel;
}

function ShapeLabTools::updatePanels( %this )
{

	%extent = 	ShapeLabTools.extent.y;
	%mainPanelSize = getWord(ShapeLabTools.rows,1);
	%rows = "0";
	%this.hideCtrl(ShapeLabSubPanel);
	%currentIndex = 0;
	%this.showCtrl(ShapeLabPropWindow,%currentIndex++);

	foreach(%ctrl in ShapeLabTools)
	{
		if (%ctrl.internalName $= "MainPanel" || !%ctrl.visible)
			continue;

		%subPanelCount++;
	}

	%subExtent = %extent - %mainPanelSize;
	%subPanelSize = %subExtent/%subPanelCount;
	%subPanelPos = %mainPanelSize;

	for(%i=1; %i<=%subPanelCount; %i++)
	{
		%rows = strAddWord(%rows,%subPanelPos);
		%subPanelPos += %subPanelSize;
	}

	%this.setRows(%rows,true);
}

//==============================================================================
function ShapeLabTools::onPreEditorSave( %this )
{
	ShapeLab_SeqPillStack.clear();
	ShapeLabNodes-->CreateNodeRollout.expanded = 0;
	ShapeLabNodes-->NodePropertiesRollout.expanded = 1;
	ShapeLabHints.expanded = 0;
}
//------------------------------------------------------------------------------
//==============================================================================
function ShapeLabTools::onPostEditorSave( %this )
{
	//EPostFxManager.moveToGui(SEP_PostFXManager_Clone);
}
//------------------------------------------------------------------------------
