//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Scene Editor Params - Used set default settings and build plugins options GUI
//==============================================================================
//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
function ShapeLabPlugin::initParamsArray( %this,%array )
{

	%array.group[%gId++] = "General settings";
	%array.setVal("PreviewColorBG",     "0 0 0 0.9"   TAB "BackgroundColor" TAB "ColorEdit" TAB "" TAB "ShapeLabPreviewGui-->previewBackground.setColor(*val*);" TAB %gid);
	%array.setVal("HighlightMaterial",   "1" TAB "HighlightMaterial" TAB "TextEdit" TAB "" TAB "" TAB %gId);
	%array.setVal("ShowNodes","1" TAB "ShowNodes" TAB "TextEdit" TAB "" TAB "" TAB %gId);
	%array.setVal("ShowBounds",       "0" TAB "ShowBounds" TAB "TextEdit" TAB "" TAB "" TAB %gId);
	%array.setVal("ShowObjBox",       "1" TAB "ShowObjBox" TAB "TextEdit" TAB "" TAB "" TAB %gId);
	%array.setVal("RenderMounts",       "1" TAB "RenderMounts" TAB "TextEdit" TAB "" TAB "" TAB %gId);
	%array.setVal("RenderCollision",       "0" TAB "RenderCollision" TAB "TextEdit" TAB "" TAB "" TAB %gId);
	%array.setVal("AdvancedWindowVisible",       "1" TAB "RenderCollision" TAB "TextEdit" TAB "" TAB "" TAB %gId);
	%array.setVal("AnimationBarVisible",       "1" TAB "RenderCollision" TAB "TextEdit" TAB "" TAB "" TAB %gId);
	%array.group[%gId++] = "Grid settings";
	%array.setVal("ShowGrid",       "1" TAB "ShowGrid" TAB "TextEdit" TAB "" TAB "ShapeLabEditor" TAB %gId);
	%array.setVal("GridSize",       "0.1" TAB "GridSize" TAB "TextEdit" TAB "" TAB "ShapeLabEditor" TAB %gId);
	%array.setVal("GridDimension",       "40 40" TAB "GridDimension" TAB "TextEdit" TAB "" TAB "ShapeLabEditor" TAB %gId);
	%array.group[%gId++] = "Sun settings";
	%array.setVal("SunDiffuseColor",       "255 255 255 255" TAB "SunDiffuseColor" TAB "TextEdit" TAB "" TAB "ShapeLabEditor" TAB %gId);
	%array.setVal("SunAmbientColor",       "180 180 180 255" TAB "SunAmbientColor" TAB "TextEdit" TAB "" TAB "ShapeLabEditor" TAB %gId);
	%array.setVal("SunAngleX",       "45" TAB "SunAngleX" TAB "TextEdit" TAB "" TAB "ShapeLabEditor" TAB %gId);
	%array.setVal("SunAngleZ",       "135" TAB "SunAngleZ" TAB "TextEdit" TAB "" TAB "ShapeLabEditor" TAB %gId);
}
//------------------------------------------------------------------------------
//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
function ShapeLabPlugin::setConfigDefault( %this,%cfg )
{
   //Key Format: Field
   //Value Format: Default TAB Display TAB DataType|option=value,more=this TAB LinkedObject TAB UpdateCallback TAB Group  TAB "Custom=Options:ELink=ThatLink" 
    //Value Fields: 0-Default 1-Display 2-DataType/Opt 3-LinkedObject 4-Group  6-"Custom=Options:ELink=ThatLink" 
 
	%cfg.group[%gId++] = "General settings";
	%cfg.set("PreviewColorBG",     "0 0 0 0.9"   TAB "BackgroundColor"   TAB "ColorEdit"   TAB "ShapeLabEditor" TAB %gid);
	%cfg.set("HighlightMaterial",   "1"          TAB "HighlightMaterial" TAB "TextEdit"    TAB "ShapeLabEditor" TAB %gId);
	%cfg.set("ShowNodes",         "1"            TAB "ShowNodes"         TAB "TextEdit"    TAB "ShapeLabEditor" TAB %gId);
	%cfg.set("ShowBounds",       "0"             TAB "ShowBounds"        TAB "TextEdit"    TAB "ShapeLabEditor" TAB %gId);
	%cfg.set("ShowObjBox",       "1"             TAB "ShowObjBox"        TAB "TextEdit"    TAB "ShapeLabEditor" TAB %gId);
	%cfg.set("RenderMounts",       "1"           TAB "RenderMounts"      TAB "TextEdit"    TAB "ShapeLabEditor" TAB %gId);
	%cfg.set("RenderCollision",       "0"        TAB "RenderCollision"   TAB "TextEdit"    TAB "ShapeLabEditor" TAB %gid);
	%cfg.set("AdvancedWindowVisible",       "1"  TAB "RenderCollision"   TAB "TextEdit"    TAB "ShapeLabEditor" TAB %gid);
	%cfg.set("AnimationBarVisible",       "1"    TAB "RenderCollision"   TAB "TextEdit"    TAB "ShapeLabEditor" TAB %gid);
	%cfg.group[%gId++] = "Grid settings";
	%cfg.set("ShowGrid",       "1"               TAB "ShowGrid"          TAB "TextEdit"    TAB "ShapeLabEditor" TAB %gid);
	%cfg.set("GridSize",       "0.1"             TAB "GridSize"          TAB "TextEdit"    TAB "ShapeLabEditor" TAB %gid);
	%cfg.set("GridDimension",       "40 40"      TAB "GridDimension"     TAB "TextEdit"    TAB "ShapeLabEditor" TAB %gid);
	%cfg.group[%gId++] = "Sun settings";
	%cfg.set("SunDiffuseColor","255 255 255 255" TAB "SunDiffuseColor"   TAB "TextEdit"    TAB "ShapeLabEditor" TAB %gid);
	%cfg.set("SunAmbientColor","180 180 180 255" TAB "SunAmbientColor"   TAB "TextEdit"    TAB "ShapeLabEditor" TAB %gid);
	%cfg.set("SunAngleX",       "45"             TAB "SunAngleX"         TAB "TextEdit"    TAB "ShapeLabEditor" TAB %gid);
	%cfg.set("SunAngleZ",       "135"            TAB "SunAngleZ"         TAB "TextEdit"    TAB "ShapeLabEditor" TAB %gid);
	%cfg.extra("SunAngleZ",       "ELink=SunAngle"  TAB "Callback=newSunAngle");
}
//------------------------------------------------------------------------------
//==============================================================================
// Plugin Object Callbacks - Called from TLab plugin management scripts
//==============================================================================

//==============================================================================
// Called when TorqueLab is launched for first time
function ShapeLabPlugin::onPluginLoaded( %this )
{
	// Add ourselves to the Editor Settings window
	ShapeLabPreview.resize( -1, 526, 593, 53 );
	// Initialise gui

	if (!isObject(ShapeLabMesh))
		new ScriptObject(ShapeLabMesh);

	//ShapeLabAdvancedWindow-->tabBook.selectPage(0);
	//ShapeLabPropWindow-->tabBook.selectPage(0);

	ShapeLabToggleButtonValue( ShapeLabToolbar-->orbitNodeBtn, 0 );
	ShapeLabToggleButtonValue( ShapeLabToolbar-->ghostMode, 0 );
	// Initialise hints menu
	ShapeLab.initHints();

	if ( !%this.isGameReady )
	{
		// Activate the Shape Lab
		// Lab.setEditor( %this, true );
		// Get editor settings (note the sun angle is not configured in the settings
		// dialog, so apply the settings here instead of in readSettings)
		ShapeLabPreviewGui.fitIntoParents();
		ShapeLabPreviewGui-->previewBackground.fitIntoParents();
		ShapeLabEditor.fitIntoParents();
		$wasInWireFrameMode = $gfx::wireframe;
		ShapeLabToolbar-->wireframeMode.setStateOn($gfx::wireframe);

		if ( GlobalGizmoProfile.getFieldValue(alignment) $= "Object" )
			ShapeLabNodes-->object.setStateOn(1);
		else
			ShapeLabNodes-->world.setStateOn(1);

		// Initialise and show the shape editor
		//ShapeLabShapeTreeView.open(MissionGroup);
		//ShapeLabShapeTreeView.buildVisibleTree(true);
		EditorGui.bringToFront(ShapeLabPreviewGui);

		LabPalettes.pushButton(move);
		// Switch to the ShapeLab UndoManager
		//%this.oldUndoMgr = Editor.getUndoManager();
		//Editor.setUndoManager( ShapeLabUndoManager );
		ShapeLabEditor.setDisplayType( Lab.cameraDisplayType );
		%this.initStatusBar();
		// Customise menu bar
		//%this.oldCamFitCmd = %this.replaceMenuCmd( "Camera", 8, "ShapeLabEditor.fitToShape();" );
		//%this.oldCamFitOrbitCmd = %this.replaceMenuCmd( "Camera", 9, "ShapeLabEditor.fitToShape();" );
	}

	%this.isGameReady = true;
	ShapeLabPreviewSwatch.setColor("0 0 0 0.9");
	ShapeLab.currentShapeOptionsPage = 0;
	ShapeLab.currentMainOptionsPage = 0;
	ShapeLab.currentAnimOptionsPage = 0;
	SLE_MainOptionsBook.selectPage(0);

}
//------------------------------------------------------------------------------
//==============================================================================
// Called when the Plugin is activated (Active TorqueLab plugin)
$SimpleActivation = false;
function ShapeLabPlugin::onActivated(%this)
{
	if ( !isObject( ShapeLabUndoManager ) )
	{
		ShapeLab.undoManager = new UndoManager( ShapeLabUndoManager )
		{
			numLevels = 200;
			internalName = "ShapeLabUndoManager";
		};
	}

	//show(ShapeLabSelectWindow);
	//show(ShapeLabPropWindow);
	//Assign the Camera fit to the GuiShapeLabPreview

	ShapeLab.initTriggerList();
	// Try to start with the shape selected in the world editor
	//if (!isObject(ShapeLab.shape))
	ShapeLab.selectWorldEditorShape();
	ShapeLabPlugin.updateAnimBar();
	ShapeLab.initCollisionPage();
	Parent::onActivated(%this,$SimpleActivation);
	ShapeLab.setDirty(ShapeLab.isDirty());
	SLE_MainOptionsBook.selectPage(ShapeLab.currentMainOptionsPage);
	SLE_ShapeOptionsBook.selectPage(ShapeLab.currentShapeOptionsPage);
	SLE_AnimOptionsBook.selectPage(ShapeLab.currentAnimOptionsPage);
	//%this.oldUndoMgr = Editor.getUndoManager();
	//Editor.setUndoManager( ShapeLabUndoManager );
	ShapeLabHints.expanded = 0;
	//Only the file browser is usefull so use it
	LabSideBar.setPage("FileBrowser");
}
//------------------------------------------------------------------------------
//==============================================================================
// Called when the Plugin is deactivated (active to inactive transition)
function ShapeLabPlugin::onDeactivated(%this)
{
	// Notify game objects if shape has been modified
	if ( ShapeLab.isDirty() )
		ShapeLab.shape.notifyShapeChanged();

	$gfx::wireframe = $wasInWireFrameMode;
	ShapeLabMaterials.updateSelectedMaterial(false);

	if ( EditorGui-->MatEdPropertiesWindow.visible )
	{
		ShapeLabMaterials.editSelectedMaterialEnd( true );
	}

	// Restore the original undo manager
	//Editor.setUndoManager( %this.oldUndoMgr );
	// Restore menu bar
	//%this.replaceMenuCmd( "Camera", 8, %this.oldCamFitCmd );
	//%this.replaceMenuCmd( "Camera", 9, %this.oldCamFitOrbitCmd );
	Parent::onDeactivated(%this);
}
//------------------------------------------------------------------------------
//==============================================================================
// Called from TorqueLab after plugin is initialize to set needed settings
function ShapeLabPlugin::onPluginCreated( %this )
{

}
//------------------------------------------------------------------------------
//==============================================================================
// Called from TorqueLab when exitting the mission
function ShapeLabPlugin::onExitMission( %this )
{
	// unselect the current shape
	ShapeLabEditor.setModel( "" );

	if (ShapeLab.shape != -1)
		delObj(ShapeLab.shape);

	if (!isObject(ShapeLab))
		return;

	ShapeLab.shape = 0;
   
   if (isObject(ShapeLabUndoManager))
	   ShapeLabUndoManager.clearAll();
	ShapeLab.setDirty( false );
	//ShapeLabSequenceList.clear();
	//ShapeLabNodeTreeView.removeItem( 0 );
	ShapeLab.setActiveNode("");
	//ShapeLab.onNodeSelectionChanged( -1 );
	ShapeLab_DetailTree.removeItem( 0 );
	ShapeLabMaterialList.clear();
	ShapeLabMountWindow-->mountList.clear();
	//ShapeLabThreadList.clear();
	ShapeLab.clearNodeTree();
	ShapeLab_ThreadIdList.clear();
	ShapeLab_ThreadSeqList.clear();
}
//------------------------------------------------------------------------------
//==============================================================================

function ShapeLabPlugin::onPreSave( %this )
{
	ShapeLabEditor.selectedNode = "-1";
	ShapeLabEditor.selectedObject = "-1";
	ShapeLabEditor.selectedObjDetail = "-1";
	ShapeLabEditor.activeThread = "-1";
}

// Replace the command field in an Editor PopupMenu item (returns the original value)
function ShapeLabPlugin::replaceMenuCmd(%this, %menuTitle, %id, %newCmd)
{

return;
	%menu = WorldEdMenu.findMenu( %menuTitle );
	%cmd = getField( %menu.item[%id], 2 );
	%menu.setItemCommand( %id, %newCmd );
	return %cmd;
}

//==============================================================================
// Called when the Plugin is activated (Active TorqueLab plugin)
function ShapeLabPlugin::preparePlugin(%this)
{
}

function ShapeLabPlugin::initStatusBar(%this)
{
	EditorGuiStatusBar.setInfo("Shape editor ( Shift Click ) to speed up camera.");
	EditorGuiStatusBar.setSelection( ShapeLab.shape.baseShape );
}

//==============================================================================
// Handle a selection in the MissionGroup shape selector
function ShapeLabPlugin::addFileBrowserMesh( %this, %file,%createCmd )
{
	ShapeLab.selectFilePath(%file);
}
//------------------------------------------------------------------------------

//==============================================================================
function ShapeLabPlugin::openShape( %this, %path, %discardChangesToCurrent )
{
//    Lab.setEditor( ShapeLabPlugin );
	if ( ShapeLab.isDirty() && !%discardChangesToCurrent )
	{
		LabMsgYesNoCancel( "ShapeLab Save Changes?",
		                   "Save changes to current shape?",
		                   "ShapeLab.saveChanges(); ShapeLabPlugin.openShape(\"" @ %path @ "\");",
		                   "ShapeLabPlugin.openShape(\"" @ %path @ "\",true);" );
		return;
	}

	ShapeLab.selectShape( %path );
	ShapeLabEditor.fitToShape();
}
//------------------------------------------------------------------------------
function ShapeLabPlugin::handlePaletteClick(%this,%mode)
{

	switch$(%mode)
	{
		case "select":
			GlobalGizmoProfile.mode = "None";
			EditorGuiStatusBar.setInfo("Selection arrow.");

		case "move":
			GlobalGizmoProfile.mode = "Move";
			%cmdCtrl = "CTRL";

			if ($platform $= "macos")
				%cmdCtrl = "CMD";

			EditorGuiStatusBar.setInfo("Move selection.  SHIFT while dragging duplicates objects.  " @ %cmdCtrl @ " to toggle soft snap.  ALT to toggle grid snap.");

		case "rotate":
			GlobalGizmoProfile.mode = "Rotate";
			EditorGuiStatusBar.setInfo("Rotate selection.");

		case "scale":
			GlobalGizmoProfile.mode = "Scale";
			EditorGuiStatusBar.setInfo("Scagfgle selection.");

		case "sun":
			ShapeLabEditor.editSun = true;
	}

}
//==============================================================================
// Called from TorqueLab after plugin is initialize to set needed settings
function ShapeLabPlugin::fitCameraToSelection( %this,%orbit )
{
	ShapeLabEditor.fitToShape();
}
//------------------------------------------------------------------------------
