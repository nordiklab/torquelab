//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function ShapeLabEditor::onShapeSelectionChanged( %this )
{
	ShapeLabEditor.refreshShape();
	ShapeLabEditor.currentDL = 0;

}
//==============================================================================
// GuiShapeEdPreview GuiControl Callbacks
//==============================================================================

//==============================================================================
function ShapeLabEditor::onDetailChanged( %this )
{
	ShapeLabDetails.updateChangedDetail();
}
//------------------------------------------------------------------------------
//==============================================================================
function ShapeLabEditor::onEditNodeTransform( %this, %node, %txfm, %gizmoID )
{
	ShapeLab.doEditNodeTransform( %node, %txfm, 1, %gizmoID );
}
//------------------------------------------------------------------------------

//==============================================================================
function ShapeLabEditor::onNodeSelected( %this, %index )
{
	ShapeLab.selectTreeNode(%index);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Shape Preview
//------------------------------------------------------------------------------

function ShapeLabPreviewGui::updatePreviewBackground( %color )
{
	ShapeLabPreviewGui-->previewBackground.color = %color;
	ShapeLabToolbar-->previewBackgroundPicker.color = %color;
}

function showShapeLabPreview()
{
	%visible = ShapeLabToolbar-->showPreview.getValue();
	ShapeLabPreviewGui.setVisible( %visible );
}
