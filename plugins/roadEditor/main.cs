//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function RoadEd::create( %this )
{  
  info("Plugin","->","Initializing","Road and Path Editor" );
	$RoadManager = newScriptObject("RoadManager");
	execREP(true);
	//Lab.createPlugin("RoadEditor","Road Editor");
	Lab.addPluginEditor("RoadEditor",RoadEditorGui);
	Lab.addPluginGui("RoadEditor",RoadEditorTools);
	//Lab.addPluginGui("RoadEditor",RoadEditorOptionsWindow);
	//Lab.addPluginGui("RoadEditor",RoadEditorTreeWindow);
	Lab.addPluginToolbar("RoadEditor",RoadEditorToolbar);
	Lab.addPluginPalette("RoadEditor",   RoadEditorPalette);
	RoadEditorPlugin.editorGui = RoadEditorGui;
	$REP = newScriptObject("REP");
	%map = Lab.addPluginMap("RoadEditor");
	//%map = new ActionMap();
	%map.bindCmd( keyboard, "backspace", "RoadEditorGui.onDeleteKey();", "" );
	%map.bindCmd( keyboard, "1", "RoadEditorGui.prepSelectionMode();", "" );
	%map.bindCmd( keyboard, "2", "LabPalettes.pushButton(Move);", "" );

	%map.bindCmd( keyboard, "4", "LabPalettes.pushButton(Scale);","" );
	%map.bindCmd( keyboard, "5", "LabPalettes.pushButton(AddRiver);", "" );
	%map.bindCmd( keyboard, "=", "LabPalettes.pushButton(InsertPoint);", "" );
	%map.bindCmd( keyboard, "numpadadd", "LabPalettes.pushButton(InsertPoint);", "" );
	%map.bindCmd( keyboard, "-", "LabPalettes.pushButton(RemovePoint);", "" );
	%map.bindCmd( keyboard, "numpadminus", "LabPalettes.pushButton(RemovePoint);", "" );

	%map.bindCmd( keyboard, "z", "RoadEditorShowSplineBtn.performClick();", "" );
	%map.bindCmd( keyboard, "x", "RoadEditorWireframeBtn.performClick();", "" );
	%map.bindCmd( keyboard, "v", "RoadEditorShowRoadBtn.performClick();", "" );
	RoadEditorPlugin.map = %map;
}

//------------------------------------------------------------------------------

function RoadEd::destroy( %this )
{
    
}

//----------------------------------------------------------------
$TLab_PluginName_["RoadEditor"] = "Road Editor";
//==============================================================================

//------------------------------------------------------------------------------
//==============================================================================
// Load all the Scripts and GUIs (if specified)
function execREP(%loadGui)
{
	if (%loadGui)
	{
		exec( "tlab/plugins/roadEditor/gui/guiProfiles.cs" );
		exec( "tlab/plugins/roadEditor/gui/roadEditorGui.gui" );
		exec( "tlab/plugins/roadEditor/gui/RoadEditorTools.gui" );
		exec( "tlab/plugins/roadEditor/gui/roadEditorToolbar.gui");
		exec( "tlab/plugins/roadEditor/gui/RoadEditorPaletteGui.gui");
	}

	exec( "tlab/plugins/roadEditor/roadEditorGui.cs" );
	exec( "tlab/plugins/roadEditor/RoadEditorPlugin.cs" );
	execPattern("tlab/plugins/roadEditor/editor/*.cs");
	execPattern("tlab/plugins/roadEditor/nodeManager/*.cs");
}
//------------------------------------------------------------------------------
//==============================================================================
function destroyRoadEditor()
{
}
//------------------------------------------------------------------------------
