//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$REP_UpdateLinkedNodes = true;
//==============================================================================
// TerrainObject Functions
//==============================================================================
function RoadManager::Init(%this)
{
}

//==============================================================================
// Node linking functions
//==============================================================================
function RoadManager::linkAll(%this,%linked)
{
	if (!%linked)
		RoadManager.linkedList = "";

	foreach(%pill in REP_NodePillStack)
	{
		%pill.linkCheck.setStateOn(%linked);

		if (%linked)
			RoadManager.linkedList = strAddWord(RoadManager.linkedList,%pill.nodeId,true);
	}
}

function RoadManager::linkInvert(%this)
{
	%startList = RoadManager.linkedList;

	foreach(%pill in REP_NodePillStack)
	{
		%linked = %pill.linkCheck.isStateOn();
		%pill.linkCheck.setStateOn(!%linked);

		if (!%linked)
			RoadManager.linkedList = strAddWord(RoadManager.linkedList,%pill.nodeId,true);
		else
			RoadManager.linkedList = strRemoveWord(RoadManager.linkedList,%pill.nodeId);
	}
}

function REP_LinkNodeCheck::onClick(%this)
{
	%linked = %this.isStateOn();
	//%this.setStateOn(!%linked);
	%pill = %this.pill;

	if (%linked)
		RoadManager.linkedList = strAddWord(RoadManager.linkedList,%pill.nodeId,true);
	else
		RoadManager.linkedList = strRemoveWord(RoadManager.linkedList,%pill.nodeId);
}

function RoadManager::updateLinkList(%this)
{
	RoadManager.linkedList = "";

	foreach(%pill in REP_NodePillStack)
	{
		if (%pill.linkCheck.isStateOn())
			RoadManager.linkedList = strAddWord(RoadManager.linkedList,%pill.nodeId,true);
	}
}
