//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function RoadManager::updateRoadData(%this)
{
  
	%road = RoadManager.currentRoad;

	if (!isObject(%road))
	{
		REP_NodePillStack.clear();
		RoadEd_TabPageNode-->roadNodePageTitle.setText("No road selected");
		return;
	}

	%this.noNodeUpdate = true;

	if (RoadManager.nodeListModeId $= "")
		RoadManager.nodeListModeId = "0";

	%id = 0;
   
	while( %id < RoadEditorGui.getNodeCount() )
	{
		RoadEditorGui.setSelectedNode(%id);
		%nodeWidth = RoadEditorGui.getNodeWidth();
      devlog("Count:",RoadEditorGui.getNodeCount(),"Id",%id,"Width",%nodeWidth);
		if (%nodeWidth $= "" || %nodeWidth $= "-1")
			break;

		%nodePos = RoadEditorGui.getNodePosition();
		RoadManager.nodeData[%id] = %nodePos TAB %nodeWidth;
		%id++;
	}

	RoadManager.updateNodeStack();
	%this.noNodeUpdate = false;
	//%node = RoadEditorGui.getSelectedNode();

	//if (%node $= "-1" || %node $= "" || %node >= RoadManager.nodeCount)
		//RoadManager.selectNode(0);
	//else
		//RoadManager.onNodeSelected(%node);
    return;  
   if(!isObject(MeshRoadEditorOptionsWindow-->topMaterial))
   {
      warnLog("MeshRoadEditorOptionsWindow-->topMaterial is missing!","update skipped");
      return;  
   }
	MeshRoadEditorOptionsWindow-->topMaterial.setText(%road.topMaterial);
	MeshRoadEditorOptionsWindow-->bottomMaterial.setText(%road.bottomMaterial);
	MeshRoadEditorOptionsWindow-->sideMaterial.setText(%road.sideMaterial);
}
//==============================================================================
// Select Node
//==============================================================================

//------------------------------------------------------------------------------
$NoNodeUpd = true;
function RoadManager::selectNode(%this,%nodeId,%noUpdate)
{
	if (!isObject(RoadEditorGui.road))
		return;

	%this.noNodeUpdate = %noUpdate;
	RoadEditorGui.setSelectedNode(%nodeId);
}
//------------------------------------------------------------------------------

//==============================================================================
function RoadManager::onNodeSelected(%this,%nodeId)
{
   devlog("RoadManager::onNodeSelected(%this,%nodeId)",%this,%nodeId);
	if (%this.noNodeUpdate)
	{
		return;
	}
   
	if (%nodeId $= "")
	{
		%nodeId = RoadEditorGui.getSelectedNode();
	}

	if (%nodeId $= "-1")
	{
		//RoadManager.selectNode(0);
		return;
	}

	foreach(%ctrl in REP_NodePillStack)
	{
		%active = false;

		if (%ctrl.nodeId $= %nodeId)
			%active = true;

		%ctrl-->buttonNode.setStateOn(%active);

		if (!isObject(%ctrl-->NodeDataStack))
			continue;

		if (RoadManager.autoCollapsePill)
			%ctrl-->NodeDataStack.visible = %active;
		else
			%ctrl-->NodeDataStack.visible = 1;

		%ctrl-->positionCtrl.visible = RoadManager.showPositionEdit;
	}
}
//------------------------------------------------------------------------------
