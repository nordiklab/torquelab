//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function RoadEditorGui::paletteSync( %this, %mode )
{
	%evalShortcut = "LabPalettes-->" @ %mode @ ".setStateOn(1);";
	eval(%evalShortcut);
}
//------------------------------------------------------------------------------
//==============================================================================
function RoadEditorGui::prepSelectionMode( %this )
{
	%mode = %this.getMode();

	if ( %mode $= "RoadEditorAddNodeMode"  )
	{
		if ( isObject( %this.getSelectedRoad() ) )
			%this.deleteNode();
	}

	%this.setMode( "RoadEditorSelectMode" );
	LabPalettes-->Select.setStateOn(1);
}
//------------------------------------------------------------------------------

//==============================================================================
// Road Palette Tools Buttons Clicks
//==============================================================================
function ERoadEditorSelectModeBtn::onClick(%this)
{
	EditorGuiStatusBar.setInfo(%this.ToolTip);
	Parent::onClick(%this);
}
//------------------------------------------------------------------------------
function ERoadEditorAddModeBtn::onClick(%this)
{
	EditorGuiStatusBar.setInfo(%this.ToolTip);
	Parent::onClick(%this);
}
//------------------------------------------------------------------------------
function ERoadEditorMoveModeBtn::onClick(%this)
{
	EditorGuiStatusBar.setInfo(%this.ToolTip);
	Parent::onClick(%this);
}
//------------------------------------------------------------------------------
function ERoadEditorScaleModeBtn::onClick(%this)
{
	EditorGuiStatusBar.setInfo(%this.ToolTip);
	Parent::onClick(%this);
}
//------------------------------------------------------------------------------
function ERoadEditorInsertModeBtn::onClick(%this)
{
	EditorGuiStatusBar.setInfo(%this.ToolTip);
	Parent::onClick(%this);
}
//------------------------------------------------------------------------------
function ERoadEditorRemoveModeBtn::onClick(%this)
{
	EditorGuiStatusBar.setInfo(%this.ToolTip);
	Parent::onClick(%this);
}
//------------------------------------------------------------------------------
