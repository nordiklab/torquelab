//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
singleton GuiControlProfile( RoadEditorProfile )
{
	canKeyFocus = true;
	opaque = true;
	fillColor = "192 192 192 192";
	category = "Editor";
	fontColors[4] = "Fuchsia";
	fontColorLink = "Fuchsia";
};
//------------------------------------------------------------------------------

//==============================================================================
singleton GuiCursor(RoadEditorMoveCursor)
{
	hotSpot = "4 4";
	renderOffset = "0 0";
	bitmapName = "art/images/cursors/macCursor";
	category = "Editor";
};
//------------------------------------------------------------------------------
//==============================================================================
singleton GuiCursor( RoadEditorMoveNodeCursor )
{
	hotSpot = "1 1";
	renderOffset = "0 0";
	bitmapName = "./Cursors/outline/drag_node_outline";
	category = "Editor";
};
//------------------------------------------------------------------------------
//==============================================================================
singleton GuiCursor( RoadEditorAddNodeCursor )
{
	hotSpot = "1 1";
	renderOffset = "0 0";
	bitmapName = "./Cursors/outline/add_to_end_outline";
	category = "Editor";
};
//------------------------------------------------------------------------------
//==============================================================================
singleton GuiCursor( RoadEditorInsertNodeCursor )
{
	hotSpot = "1 1";
	renderOffset = "0 0";
	bitmapName = "./Cursors/outline/insert_in_middle_outline";
	category = "Editor";
};
//------------------------------------------------------------------------------
//==============================================================================
singleton GuiCursor( RoadEditorResizeNodeCursor )
{
	hotSpot = "1 1";
	renderOffset = "0 0";
	bitmapName = "./Cursors/outline/widen_path_outline";
	category = "Editor";
};
//------------------------------------------------------------------------------
