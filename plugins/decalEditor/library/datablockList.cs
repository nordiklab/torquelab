//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function DecalDataList::onSelect( %this, %id, %text )
{
	%obj = %this.getItemObject( %id );
	DecalEditorGui.currentDecalData = %obj;
	%itemNum = DecalDataList.getSelectedItem();

	if ( %itemNum == -1 )
		return;

	%data = DecalDataList.getItemObject( %itemNum );
	DecalEditorGui.selectData(%data);
}
//------------------------------------------------------------------------------

function DecalEditorGui::selectData(%this, %data)
{
	$Lab::materialEditorList = %data.getId();
	DecalInspector.inspect( %data );
	DecalEditorGui.updateDecalPreview( %data.material );
}
