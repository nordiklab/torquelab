//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$TLab_PluginName_["DecalEditor"] = "Decal Editor";
//------------------------------------------------------------------------------

function DecalEditor::create( %this )
{
   
  info("DecalEditor->","Initializing","Decal Editor plugin");
	$decalDataFile = "art/textures/decals/managedDecalData.cs";
	execDecalEd(true);
	//Lab.createPlugin("DecalEditor");
	// Add ourselves to EditorGui, where all the other tools reside
	Lab.addPluginEditor("DecalEditor",   DecalEditorGui);
	Lab.addPluginGui("DecalEditor",DecalEditorTools);

	Lab.addPluginPalette("DecalEditor",   DecalEditorPalette);
	DecalEditorTabBook.selectPage( 0 );
	DecalEditorPlugin.editorGui = DecalEditorGui;
	%map = Lab.addPluginMap("DecalEditor");
	//%map = new ActionMap();
	%map.bindCmd( keyboard, "5", "EDecalEditorAddDecalBtn.performClick();", "" );
	%map.bindCmd( keyboard, "1", "EDecalEditorSelectDecalBtn.performClick();", "" );
	%map.bindCmd( keyboard, "2", "EDecalEditorMoveDecalBtn.performClick();", "" );
	%map.bindCmd( keyboard, "3", "EDecalEditorRotateDecalBtn.performClick();", "" );
	%map.bindCmd( keyboard, "4", "EDecalEditorScaleDecalBtn.performClick();", "" );
	DecalEditorPlugin.commonPalette = "Select Move Rotate Scale";
	DecalEditorPlugin.editorGui = DecalEditorGui;
	DecalEditorPlugin.map = %map;
	new PersistenceManager( DecalPMan );
}

//------------------------------------------------------------------------------

function DecalEditor::destroy( %this )
{
    info("DecalEditor::destroy",%this);
}

//----------------------------------------------------------------

function execDecalEd(%loadGui)
{
	if (%loadGui)
	{
		exec( "tlab/plugins/decalEditor/gui/decalEditorGui.gui" );
		exec( "tlab/plugins/decalEditor/gui/decalEditorTools.gui" );
		exec( "tlab/plugins/decalEditor/gui/decalEditorPalette.gui" );
	}

	exec( "tlab/plugins/decalEditor/decalPreview.cs" );
	exec( "tlab/plugins/decalEditor/decalEditorActions.cs" );
	exec( "tlab/plugins/decalEditor/DecalEditorPlugin.cs" );
	execPattern("tlab/plugins/decalEditor/instance/*.cs" );
	execPattern( "tlab/plugins/decalEditor/library/*.cs" );
}
function destroyDecalEditor()
{
}

// JCF: helper for during development
function reinitDecalEditor()
{
	exec( "./main.cs" );
	exec( "./decalEditor.cs" );
	exec( "./decalEditorGui.cs" );
}

