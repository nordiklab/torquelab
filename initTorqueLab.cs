//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// TorqueLab ->
// Copyright (c) 2018 All Right Reserved, http://nordiklab.com/
//------------------------------------------------------------------------------
//==============================================================================
// Static Globals - For Development purpose
$TorqueLab_IconSet = "Set01"; //Sub folder containing the desired icons
//==============================================================================
function initializeTorqueLab()
{
   info("TorqueLab","->","Initializing","Core Scripts");
   prepareTLObjects(); 
   
   exec( "tlab/manager/init.cs");
   initTLManager();  
   
   //Load the TorqueLab Themes Support before loading any GUIs
   exec("tlab/themes/main.cs");   
   Lab.initThemeSystem();
   
	//Start by loading the TorqueLab Loading Progress GUI to use it now
   execGui("tlab/ui/gui/editorCore/EditorLoadingGui.gui",1,1);
   exec("tlab/ui/gui/editorCore/EditorLoadingGui.cs");
	
	EditorLoadingGui.startInit();

	Lab.defaultPlugin = "SceneEditorPlugin";

	exec( "tlab/themes/Common/cursors.cs" );
	
	setEditorBinds();
	//---------------------------------------------------------------------------

	execTorqueLabScripts();	

	EditorIconRegistry::loadFromPath( "tlab/art/icons/"@$TorqueLab_IconSet@"/classes/" );
   $TorqueLab_Loaded = true;	
}
//------------------------------------------------------------------------------

//==============================================================================
// TorqueLab Editor Initialization Function
//==============================================================================
$TorqueLab_Loaded = false;

//------------------------------------------------------------------------------
function initTorqueLab(%launchEditor,%editorParam)
{  
	
	if (!$TorqueLab_Loaded)
		initializeTorqueLab();
   else
      EditorLoadingGui.startInit();

   switch$(%launchEditor)
   {
      case "Gui": 
         if (!isObject(GuiEditor))
		      Lab.loadGuiEditor(); 
		      
         ToggleGuiEdit();
      case "World":       
         if (!isObject(EWorldEditor))
		      Lab.loadWorldEditor(); 
		      
		      toggleEditor( true,true );
      default: 
         EditorLoadingGui.endInit();
   }
		
	$instantGroup = %instantGroup;
	
}
//==============================================================================
// New plugins folder base init
function Lab::loadWorldEditor(%this)
{
    info("TorqueLab","->","Initializing","World Editors");
   execSceneScripts();
  	Lab.checkLayoutCore();
	tlabInitTools();
	Lab.buildWorldMenus();
	
	// Default file path when saving from the editor (such as prefabs)
	if ($Pref::WorldEditor::LastPath $= "")	
		$Pref::WorldEditor::LastPath = getMainDotCsDir();

	//%toggle = $Scripts::ignoreDSOs;
	//$Scripts::ignoreDSOs = true;
	$ignoredDatablockSet = new SimSet();

	exec( "tlab/SceneEditor/main.cs" );
	//initializeSceneEditor();
	%sceneEd = TLModules.moduleSceneEditor;
	Lab.createPlugin("SceneEditor","Scene Editor",%sceneEd);
	TLModules.loadModule("SceneEditor");
	%modules = TLModules.moduleSetPlugins;
	foreach(%plugin in %modules)
	{       
      Lab.createPlugin(%plugin.ModuleId,%plugin.Display,%plugin);
	
	   
	}

	Lab.initConfigSystem();
	
	//devMap.bind(keyboard, "f11", toggleEditor);
	$WorldEditorLoaded = true;

	setEditorBinds();
	
	//WIP Fast - Call it from here since everything has been loaded
   Lab.checkLaunchedLayout();

   Scene.setActiveSimGroup(MissionGroup);
   //Lab.initToolbarPluginTrash();
   Scene.setDropType($Cfg_Common_Objects_dropType);
   //Reset the TLabGameGui to default state
      TLabGameGui.reset();
}

function Lab::loadGuiEditor(%this)
{
    info("TorqueLab","->","Initializing","Gui Editor");
	exec("tlab/guiEditor/main.cs");
	initializeGuiEditor();
		
	Lab.buildGuiMenus();
	setEditorBinds();
}

//==============================================================================
//TorqueLab System Files Exec
function execTorqueLabScripts()
{
	//------------------------------------------------------------------------------
	//Create Lab Editor Core Objects
	exec("tlab/system/core/commonSettings.cs");
	//===========================================================================
	// Prepare TorqueLab Layout Scripts
	exec("tlab/ui/main.cs");
	exec("tlab/system/main.cs");

	//execPattern("tlab/manager/*.cs");
	if (isFile("tlab/dev/main.cs"))
		exec("tlab/dev/main.cs");

	//---------------------------------------------------------------------------
	execPattern("tlab/system/core/classBase/*.cs","",true);
	execPattern("tlab/system/core/scripts/*.cs","ed.cs");
	execPattern("tlab/system/core/settings/*.cs","cfg.cs");
	execPattern("tlab/system/core/menus/*.cs");
	execPattern("tlab/system/core/eventManager/*.cs");

	exec("tlab/system/EditorOpen.cs");
	exec("tlab/system/EditorClose.cs");
	exec("tlab/system/EditorScript.cs");
	exec("tlab/system/EditorActivate.cs");
	exec("tlab/system/EditorCallbacks.cs");
	exec("tlab/system/TorqueLabPackage.cs");
	execPattern("tlab/system/settings/*.cs");
	execGuiDir("tlab/system/settings/");
	execPattern("tlab/system/plugins/*.cs");

   	execGui("tlab/ui/gui/messageBoxes/LabMsgBoxesGui.gui",1);
	exec("tlab/ui/gui/messageBoxes/LabMsgBoxesGui.cs");
	
   execPattern("tlab/ui/gui/common/*.cs");
	execGuiDir("tlab/ui/gui/common/");
	exec( "tlab/manager/gui/LabModuleManager.cs");
   execGui( "tlab/manager/gui/LabModuleManager.Gui",1);
}
//==============================================================================
//==============================================================================
//TorqueLab System Files Exec
function execSceneScripts()
{
	//Load the WorldEditor core scripts amd GUI
	execPattern("tlab/SceneObjects/*.cs");
	execGuiDir("tlab/SceneObjects",true);
	execPattern("tlab/sceneEditor/EWorldEditor/*.cs");

	exec("tlab/ui/gui/EditorGui.cs");
	
	//GuiEditor need it for menu issue so could be created from GuiEditor init
   if (!isObject(EditorGui))	
	   exec("tlab/ui/gui/EditorGui.gui");

	execGuiDir("tlab/ui/gui/GameLab",true);
	exec("tlab/ui/gui/editorTools/ETools.gui");
	exec("tlab/ui/gui/editorTools/ETools.cs");

	execPattern("tlab/ui/gui/GameLab/*.cs");

	execGuiDir("tlab/ui/gui/dlgs",true);
	execGuiDir("tlab/ui/gui/dialogs/",true);
	execGuiDir("tlab/ui/dialogs/",true);

	execPattern("tlab/ui/gui/dlgs/*.cs");
	execPattern("tlab/ui/gui/dialogs/*.cs");
	execPattern("tlab/ui/gui/editorTools/*.cs");

	exec("tlab/tools/tools.cs");
}
//==============================================================================

function prepareTLObjects()
{
   $LabGroup = newSimGroup(LabGroup,RootGroup);

	%instantGroup = $instantGroup;
	$instantGroup = LabGroup;

   newScriptObject("Lab",LabGroup);
	newScriptObject("Scene",LabGroup);	
	
   newScriptObject("LabEditor");
	newSimSet("EditorPluginSet",LabGroup,"EditorPlugin","ESet");
	//Group holding all Gui Sets
	newSimGroup("LabGuiTypesGroup");

	//Group holding parentless GUIs detached from editor
	newSimGroup("LabDetachedGuiGroup");

	//==============================================================================
	// LabAllGuiSet shared same GUIs of combined LabGuiSet and LabCoreGuiSet
	//==============================================================================
	//Holding all GUIs added to TorqueLab (LabGuiSet + LabCoreGuiSet)
	newSimSet("LabAllGuiSet",LabGuiTypesGroup,"LabAllGui","ESet");
	//Holding all standard GUIs added to TorqueLab
	newSimSet("LabGuiSet",LabGuiTypesGroup,"LabGui","ESet");
	//Holding all TorqueLab core GUIs - Can contain LabGuiSet Guis so need own set
	newSimSet("LabCoreGuiSet",LabGuiTypesGroup,"LabCoreGui","ESet");
	//------------------------------------------------------------------------------

	//Special Gui Set containing plugin related GUIs
	newSimSet("LabPluginGuiSet",LabGuiTypesGroup);

	LabDetachedGuiGroup.isTemporaryParent = true;
	GuiGroup.isTemporaryParent = true;
	
   if (!isObject("Lab_PM"))
		new PersistenceManager(Lab_PM);

	//if (!isObject("LabObj"))
		//$LabObj = newObjLab("LabObj");
		
		new SimGroup(ToolLabGuiGroup);
	$LabPluginGroup = newSimSet("LabPluginGroup");
	$LabModuleGroup = newSimSet("LabPluginModGroup");
	newSimSet(ToolGuiSet);

	//Create a group to keep track of all objects set
	newSimGroup(LabSceneObjectGroups);
	
   $LabParams = newScriptObject("LabParams");
	$LabParamsGroup = newSimGroup("LabParamsGroup");	
	
	%inspector = createInspectorLab();
	Lab.inspectorManager = %inspector;

}

//==============================================================================
// Loggin for PersistenceManager actions (ObjLab.logPM must be tru)
function createInspectorLab()
{
	%inspectorLab = new GuiInspector(InspectorManager)
	{
		dividerMargin = "5";
		dividerPos = "0.65";
		showCustomFields = "1";
		stackingType = "Vertical";
		horizStacking = "Left to Right";
		vertStacking = "Top to Bottom";
		padding = "1";
		dynamicSize = "1";
		dynamicNonStackExtent = "0";
		dynamicPos = "0";
		changeChildSizeToFit = "1";
		changeChildPosition = "1";
		position = "1 1";
		extent = "439 344";
		minExtent = "8 8";
		horizSizing = "width";
		vertSizing = "bottom";
		profile = "ToolsDefaultProfile";
		tooltipProfile = "ToolsToolTipProfile";
		isContainer = "1";
		superClass = "InspectorLab";
	};
	return %inspectorLab;
}
//------------------------------------------------------------------------------
