//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$LabPlugin_ButtonsFolder = "tlab/themes/DarkLab/buttons/plugin/";

//==============================================================================

$Theme_Color["Background","A"] = "32 32 32";
$Theme_Color["Background","B"] = "22 22 22";
$Theme_Color["Accent","A"] = "13 73 123";
$Theme_Color["Text","Base"] = "170 170 170";
$Theme_Color["Text","Alt"] = "250 224 180";
$Theme_Color["Icon","Active"] = "255 255 255";
$Theme_Color["Icon","Inactive"] = "98 98 98";
$Theme_Color["Icon","Alt"] = "15 15 15";
//==============================================================================
// Profiles list for colors
%txtAlt = "ToolsTextAlt _C _R";
$Theme_ColorProfiles["Text","Alt"] = %txtAlt;

//==============================================================================
// Profiles assignments lists
//==============================================================================
$Theme_Font["Base"] = "Calibri";
$Theme_Font["BaseBold"] = "Calibri Bold";
$Theme_Font["Alt"] = "Aileron";
$Theme_Font["AltBold"] = "Aileron Bold";
$Theme_FontProfiles["Base"] = "ToolsDefaultProfile ToolsMenuBarProfile ToolsTextPadEdit ToolsButtonBase";
$Theme_FontProfiles["BaseBold"] = "";
$Theme_FontProfiles["Alt"] = "";
$Theme_FontProfiles["AltBold"] = "";
//==============================================================================
// Background Color A
%list = "ToolsMenuProfile";

// ToolsBoxSection ToolsBoxSection ToolsBoxDarkC
