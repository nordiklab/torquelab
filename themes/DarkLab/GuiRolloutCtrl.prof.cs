//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// GuiRolloutCtrl Profiles
//==============================================================================
singleton GuiControlProfile(ToolsRolloutBaseA : ToolsDefaultProfile)
{
	bitmap = "tlab/themes/DarkLab/container/GuiRolloutBaseA.png";
	fontSize = "18";
	fontColors[0] = "254 254 254 255";
	fontColor = "254 254 254 255";
	textOffset = "6 0";
	border = "-5";
	autoSizeHeight = "1";
	category = "ToolsContainers";
};
singleton GuiControlProfile(ToolsRolloutBaseA_S1 : ToolsRolloutBaseA)
{
	bitmap = "tlab/themes/DarkLab/container/GuiRolloutBaseA_S1.png";
	fontSize = "15";
};
singleton GuiControlProfile(ToolsRolloutBaseB : ToolsRolloutBaseA)
{
	bitmap = "tlab/themes/DarkLab/container/GuiRolloutBaseB.png";
};

singleton GuiControlProfile(ToolsRolloutBaseB_S1 : ToolsRolloutBaseB)
{
	bitmap = "tlab/themes/DarkLab/container/GuiRolloutBaseB_S1.png";
	fontSize = "15";
	fillColor = "243 241 241 255";
};

singleton GuiControlProfile(ToolsRolloutBaseB_Alt_S1 : ToolsRolloutBaseB_S1)
{
	fontColors[0] = "254 189 60 255";
	fontColors[9] = "255 0 255 255";
	fontColor = "254 189 60 255";
};

singleton GuiControlProfile(ToolsRolloutBaseA_Alt_S1 : ToolsRolloutBaseA_S1)
{
	fontColors[0] = "247 208 125 255";
	fontColor = "247 208 125 255";
	border = "2";
	borderThickness = "6";
	borderColor = "15 0 255 234";
	opaque = "1";
	fillColorHL = "18 192 115 187";
	fillColorSEL = "32 90 178 169";
	bevelColorHL = "178 178 178 37";
	bevelColorLL = "222 255 0 230";
	fillColorNA = "18 88 46 255";
	borderColorHL = "171 245 15 255";
	borderColorNA = "111 210 254 11";
	fontColors[1] = "16 185 53 255";
	fontColors[3] = "231 231 231 255";
	fontColorHL = "16 185 53 255";
	fontColorSEL = "231 231 231 255";
	fontSize = "18";
};
