//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
//------------------------------------------------------------------------------
// Common Button Profiles
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
// ButtonBase and variations
//==============================================================================

//==============================================================================
singleton GuiControlProfile( ToolsButtonBase : ToolsDefaultProfile )
{
	fontSize = "16";
	fontType = "Calibri";
	fontColor = "254 254 254 255";
	justify = "center";
	category = "ToolsButtons";
	opaque = "1";
	border = "-2";
	fontColors[0] = "254 254 254 255";
	fontColors[2] = "200 200 200 255";
	fontColorNA = "200 200 200 255";
	bitmap = "tlab/themes/DarkLab/buttons/GuiButtonBase.png";
	hasBitmapArray = "1";
	fixedExtent = "0";
	bevelColorLL = "255 0 255 255";
	textOffset = "0 -1";
	autoSizeWidth = "1";
	autoSizeHeight = "1";
	borderThickness = "0";
	fontColors[9] = "Fuchsia";
	fontColors[6] = "Fuchsia";
	fontColors[4] = "255 0 255 255";
	fontColorLink = "255 0 255 255";
	fontColors[3] = "255 255 255 255";
	fontColorSEL = "255 255 255 255";
	fontColors[7] = "Magenta";
	//soundButtonDown = SFX_TLab_ButtonDown;
	//soundButtonUp = SFX_TLab_ButtonUp;
	tab = "1";
	fontColors[1] = "227 227 227 255";
	fontColorHL = "227 227 227 255";
	fillColorHL = "192 192 183 0";
	borderColorHL = "9 122 254 255";
	fillColor = "166 164 164 185";
	fillColorNA = "132 132 132 0";
	borderColor = "50 255 0 239";
	borderColorNA = "30 3 241 0";
	fillColorERR = "159 101 101 255";
	fillColorSEL = "74 74 76 133";
};
singleton GuiControlProfile(ToolsButtonBase_S1 : ToolsButtonBase)
{
	fontSize = "14";
	justify = "Top";
	textOffset = "0 0";
};
singleton GuiControlProfile(ToolsButtonBase_L : ToolsButtonBase)
{
	justify = "left";
};
//------------------------------------------------------------------------------

//==============================================================================
// ButtonAccent and variations - (Colored Button)
//==============================================================================

//==============================================================================
singleton GuiControlProfile(ToolsButtonAccent : ToolsButtonBase)
{
	bitmap = "tlab/themes/DarkLab/buttons/GuiButtonAccent.png";
	fontColors[1] = "254 254 254 255";
	fontColorHL = "254 254 254 255";
	fontColors[0] = "229 229 229 255";
	fontColors[2] = "201 201 201 255";
	fontColor = "229 229 229 255";
	fontColorNA = "201 201 201 255";
	textOffset = "0 1";
	border = "-2";
};
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
singleton GuiControlProfile(ToolsButtonToggle : ToolsButtonBase)
{
	bitmap = "tlab/themes/DarkLab/buttons/GuiButtonToggle.png";
	fontColors[1] = "254 254 254 255";
	fontColorHL = "254 254 254 255";
};

//==============================================================================
//GuiIconButtonProfiles - (Down - Hover bitmap order inverted)
//------------------------------------------------------------------------------

singleton GuiControlProfile(ToolsButtonIconAlt : ToolsButtonAccent)
{
	bitmap = "tlab/themes/DarkLab/buttons/GuiIconButtonToggle.png";
};

//==============================================================================
//ToolsButtonHighlight Style - Special style for highlighting stuff
//------------------------------------------------------------------------------

//==============================================================================
// GuiCheckboxCtrl Profiles
//==============================================================================

//==============================================================================
//ToolsCheckBoxBase Style
//------------------------------------------------------------------------------
singleton GuiControlProfile( ToolsCheckBoxBase : ToolsDefaultProfile )
{
	opaque = false;
	fillColor = "232 232 232";
	border = false;
	borderColor = "100 100 100";
	fontSize = "15";
	fontColor = "234 234 234 255";
	fontColorHL = "80 80 80";
	fontColorNA = "200 200 200";
	fixedExtent = 1;
	justify = "left";
	bitmap = "tlab/themes/DarkLab/buttons/GuiCheckBoxBase.png";
	hasBitmapArray = true;
	category = "ToolsButtons";
	fontColors[0] = "234 234 234 255";
	fontColors[1] = "80 80 80 255";
	fontColors[2] = "200 200 200 255";
	fontColors[4] = "Fuchsia";
	fontColorLink = "Fuchsia";
	textOffset = "0 -3";
	fontColors[6] = "Fuchsia";
	fontType = "Arial";
};
singleton GuiControlProfile(ToolsCheckBoxBase_S1 : ToolsCheckBoxBase)
{
	fontType = "Calibri";
	fontSize = "14";
};

//==============================================================================
// GuiRadioCtrl Profiles
//==============================================================================

//==============================================================================
//ToolsRadioBase Style
//------------------------------------------------------------------------------
singleton GuiControlProfile(ToolsRadioBase : ToolsDefaultProfile)
{
	fillColor = "254 253 253 255";
	fillColorHL = "221 221 221 255";
	fillColorNA = "200 200 200 255";
	fontSize = "16";
	textOffset = "-3 10";
	bitmap = "tlab/themes/DarkLab/buttons/GuiRadioBase_20.png";
	hasBitmapArray = "1";
	fontColors[0] = "250 250 250 255";
	fontColor = "250 250 250 255";
	border = "0";
	fontColors[2] = "Black";
	fontColorNA = "Black";
	justify = "Left";
	category = "Tools";
	fontColors[8] = "Magenta";
	fontColors[7] = "255 0 255 255";
	autoSizeHeight = "1";
	category = "ToolsButtons";
};

singleton GuiControlProfile(ToolsRadioBase_S1 : ToolsRadioBase)
{
	fillColor = "254 253 253 255";
	fillColorHL = "221 221 221 255";
	fillColorNA = "200 200 200 255";
	fontSize = "16";
	textOffset = "-3 10";
	bitmap = "tlab/themes/DarkLab/buttons/GuiRadioBase_16.png";
	hasBitmapArray = "1";
	fontColors[0] = "250 250 250 255";
	fontColor = "250 250 250 255";
	border = "0";
	fontColors[2] = "Black";
	fontColorNA = "Black";
	justify = "Left";
	category = "Tools";
	fontColors[8] = "Magenta";
	fontColors[7] = "255 0 255 255";
	autoSizeHeight = "1";
};

//==============================================================================
// Swatch Button Profile -> Used in stock code (Do not remove)
//==============================================================================
//------------------------------------------------------------------------------
singleton GuiControlProfile(ToolsSwatchButtonProfile : ToolsDefaultProfile)
{
	fillColor = "254 254 254 0";
	fillColorHL = "222 222 222 0";
	fillColorNA = "200 200 200 255";
	fontSize = "24";
	textOffset = "16 10";
	bitmap = "tlab/themes/DarkLab/buttons/GuiButtonBase.png";
	hasBitmapArray = "0";
	fontColors[0] = "254 254 254 0";
	fontColor = "254 254 254 0";
	border = "0";
	fontColors[2] = "Black";
	fontColorNA = "Black";
	category = "Tools";
	modal = "0";
	opaque = "0";
	fillColorSEL = "99 101 138 156";
	borderColor = "100 100 100 255";
	cursorColor = "0 0 0 79";
	fillColorERR = "255 0 0 255";
	borderThickness = "1";
	category = "ToolsButtons";
};

singleton GuiControlProfile(ToolsPaletteIcon : ToolsButtonBase)
{
	opaque = "0";
	fontColors[4] = "255 0 255 255";
	fontColorLink = "255 0 255 255";
	border = "-1";
	bitmap = "tlab/art/icons/set01/palette-assets/toolbar/GuiPaletteIcon.png";
	fillColorERR = "Red";
	canKeyFocus = "1";
	borderColorNA = "111 108 136 0";
	tab = "0";
};

singleton GuiControlProfile(ToolsIconPalette : ToolsButtonBase)
{
	bitmap = "tlab/themes/DarkLab/buttons/GuiPaletteIcon.png";
};

singleton GuiControlProfile(ToolsIconPlugin : ToolsIconPalette)
{
	bitmap = "tlab/themes/DarkLab/buttons/GuiPluginIcon.png";
};

singleton GuiControlProfile(ToolsButtonIconAlt_S1 : ToolsButtonIconAlt)
{
	fontSize = "14";
};

singleton GuiControlProfile(ToolsButtonHighlight : ToolsButtonBase)
{
	fillColor = "199 255 0 0";
	fillColorHL = "217 215 187 0";
	fillColorNA = "74 74 74 0";
	border = "0";
	borderThickness = "0";
	hasBitmapArray = "0";
	borderColor = "81 81 81 0";
	borderColorNA = "254 2 164 255";
	bevelColorHL = "36 139 27 208";
	fillColorSEL = "74 74 76 0";
	borderColorHL = "117 222 217 255";
	fontColors[1] = "41 254 3 133";
	fontColorHL = "41 254 3 133";
	opaque = "0";
	fillColorERR = "83 81 81 0";
};

singleton GuiControlProfile(ToolsButtonAccent_S1 : ToolsButtonAccent)
{
	fontSize = "13";
	fontColors[7] = "Magenta";
};
