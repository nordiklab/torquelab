//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
singleton GizmoProfile( LabGizmoProfile )
{
	// This isnt a GuiControlProfile but fits in well here.
	// Don't really have to initialize this now because that will be done later
	// based on the saved editor prefs.
	screenLength = 100;
	category = "Editor";
	gridColor = "0 156 0 80";
};
//------------------------------------------------------------------------------
singleton GizmoProfile( GlobalGizmoProfile )
{
	// This isnt a GuiControlProfile but fits in well here.
	// Don't really have to initialize this now because that will be done later
	// based on the saved editor prefs.
	screenLength = 100;
	category = "Editor";
};

singleton GizmoProfile( MatGizmoProfile )
{
	// This isnt a GuiControlProfile but fits in well here.
	// Don't really have to initialize this now because that will be done later
	// based on the saved editor prefs.
	screenLength = 100;
	category = "Editor";
	renderPlaneHashes = 0;
	renderPlane = 0;
};
singleton GizmoProfile( MatGizmoProfileA )
{
	// This isnt a GuiControlProfile but fits in well here.
	// Don't really have to initialize this now because that will be done later
	// based on the saved editor prefs.
	screenLength = 10;
	category = "Editor";
	renderPlaneHashes = 0;
};
