//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// GuiTreeViewCtrl
//==============================================================================

//==============================================================================
// ToolsTreeViewProfile Style
//------------------------------------------------------------------------------
singleton GuiControlProfile( ToolsTreeViewProfile : ToolsDefaultProfile )
{
	bitmap = "tlab/themes/DarkLab/element-assets/GuiTreeViewProfile.png";
	autoSizeHeight = "0";
	canKeyFocus = true;
	fillColor = "0 0 0 255";
	fillColorHL = "67 67 67 143";
	fillColorSEL = "39 39 41 255";
	fillColorNA = "24 24 24 255";
	fontColor = "254 238 192 255";
	fontColorHL = "22 210 254 255";
	fontColorSEL= "76 138 18 255";
	fontColorNA = "254 215 175 255";
	borderColor = "128 000 000";
	borderColorHL = "18 18 18 255";
	opaque = false;
	border = false;
	category = "ToolsListing";
	fontColors[2] = "254 215 175 255";

	fontSize = "16";
	fontColors[0] = "254 238 192 255";
	fontColors[6] = "255 0 255 255";
	fontColors[1] = "22 210 254 255";
	fillColorERR = "166 81 2 255";
	borderColorNA = "50 145 15 255";
	bevelColorHL = "32 32 32 228";
	bevelColorLL = "25 25 25 255";
	fontColors[3] = "76 138 18 255";
};
//------------------------------------------------------------------------------

singleton GuiControlProfile(ToolsTreeViewProfile_S1 : ToolsTreeViewProfile)
{
	fontSize = "14";
	bitmap = "tlab/themes/DarkLab/element-assets/GuiTreeViewProfile.png";
	fillColorHL = "227 227 227 26";
	fillColorSEL = "39 39 41 0";
	fontColors[1] = "254 166 57 255";
	fontColors[3] = "81 238 254 255";
	fontColorHL = "254 166 57 255";
	fontColorSEL = "81 238 254 255";
	fillColorERR = "213 150 3 0";
	borderColor = "83 67 67 255";
	fontColors[2] = "76 46 59 255";
	fontColors[4] = "Fuchsia";
	fontColorNA = "76 46 59 255";
	fontColorLink = "Fuchsia";
};

singleton GuiControlProfile(ToolsTreeBehaviorProfile : ToolsTreeViewProfile)
{
	bitmap = "tlab/avTools/questManager/gui/images/BehaviorTreeViewB.png";

	fontSize = "19";
	fontColors[6] = "255 0 255 255";
};
//==============================================================================
// GuiListBoxCtrl
//==============================================================================
singleton GuiControlProfile(ToolsListBox : ToolsDefaultProfile)
{

	fontSize = "18";
	fontColor = "254 222 155 255";
	fontColorHL = "0 0 0";
	fontColorSEL= "255 255 255";
	fontColorNA = "200 200 200";
	fillColorSEL = "0 24 39 255";
	fontSize = "18";
	opaque = "1";
	fillColor = "15 234 245 255";
	fillColorHL = "241 12 50 255";
	fillColorNA = "0 255 48 255";
	mouseOverSelected = "1";
	fontColors[0] = "254 222 155 255";
	fontColors[2] = "200 200 200 255";
	textOffset = "0 -2";
	fontColors[4] = "255 210 0 255";
	fontColors[5] = "9 255 0 255";
	fontColors[6] = "62 32 229 255";
	fontColorLink = "255 210 0 255";
	fontColorLinkHL = "9 255 0 255";
	category = "ToolsListing";
};

