//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
//------------------------------------------------------------------------------
// Common Container Structure: BoxContainer -> BoxSection ->BoxContent
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
// BoxContainer and variations DarkStyles - Official Boxes
//==============================================================================

//==============================================================================
singleton GuiControlProfile(ToolsBoxContainer : ToolsDefaultProfile)
{
	bitmap = "tlab/themes/DarkLab/container/GuiBoxContainer.png";
	hasBitmapArray = "1";
	border = "-2";
	fillColor = "8 8 8 255";
	fontColors[9] = "255 0 255 255";
	fontColors[0] = "243 243 243 255";
	fontColor = "243 243 243 255";
	hasBitmapArray = "1";
	opaque = "1";
	fontSize = "18";
	textOffset = "4 0";
	category = "ToolsBox";
};
//------------------------------------------------------------------------------

//==============================================================================
// BoxSection and variations - Made to be child of BoxContainer
//==============================================================================

//==============================================================================
singleton GuiControlProfile(ToolsBoxSection : ToolsDefaultProfile)
{
	bitmap = "tlab/themes/DarkLab/container/GuiBoxSection.png";
	border = "-2";
	fillColor = "8 8 8 255";
	fontColors[9] = "255 0 255 255";
	fontColors[0] = "243 243 243 255";
	fontColor = "243 243 243 255";
	hasBitmapArray = "1";
	opaque = "1";
	fontSize = "18";
	textOffset = "4 0";
	category = "ToolsBox";
};
//------------------------------------------------------------------------------
singleton GuiControlProfile(ToolsBoxSectionH1 : ToolsBoxSection)
{
	bitmap = "tlab/themes/DarkLab/container/GuiBoxSectionH1.png";
	fontSize = "21";
	textOffset = "6 2";
};
//------------------------------------------------------------------------------
singleton GuiControlProfile(ToolsBoxSectionH2 : ToolsBoxSection)
{
	bitmap = "tlab/themes/DarkLab/container/GuiBoxSectionH2.png";
	fontSize = "18";
};
//------------------------------------------------------------------------------
//==============================================================================
// BoxContent and variations - Made to be child of BoxSection
//==============================================================================

//==============================================================================
singleton GuiControlProfile(ToolsBoxContent : ToolsDefaultProfile)
{
	bitmap = "tlab/themes/DarkLab/container/GuiBoxContent.png";
	border = "-2";
	fillColor = "8 8 8 255";
	fontColors[9] = "255 0 255 255";
	fontColors[0] = "243 243 243 255";
	fontColor = "243 243 243 255";
	hasBitmapArray = "1";
	opaque = "1";
	fontSize = "18";
	textOffset = "4 0";
	category = "ToolsBox";
};
//------------------------------------------------------------------------------

//==============================================================================
// Box DarkStyles - Official Boxes
//==============================================================================
//------------------------------------------------------------------------------
// BoxDarkA Styles
//------------------------------------------------------------------------------

//==============================================================================
// BoxDiv - Blank with top divider
//==============================================================================

//==============================================================================
// ToolsPanelProfile Profiles
//==============================================================================
singleton GuiControlProfile(ToolsPanelProfile : ToolsDefaultProfile)
{
	bevelColorLL = "255 0 255 255";
	bitmap = "tlab/themes/DarkLab/container/GuiPanelProfile.png";
	opaque = "1";
	fillColor = "2 2 2 255";
	fillColorHL = "2 2 2 255";
	fillColorNA = "2 2 2 255";
	border = "2";
	borderThickness = "5";
	fontSize = "17";
	fontColors[0] = "254 254 254 255";
	fontColor = "254 254 254 255";
	textOffset = "2 8";
	category = "ToolsContainers";
};
//------------------------------------------------------------------------------

singleton GuiControlProfile(ToolsBoxContainerA : ToolsBoxContainer)
{
	bitmap = "tlab/themes/DarkLab/container/GuiBoxContainerA.png";
};

singleton GuiControlProfile(ToolsBoxContentA : ToolsBoxContent)
{
	bitmap = "tlab/themes/DarkLab/container/GuiBoxContentA.png";
};

singleton GuiControlProfile(ToolsBoxRolloutH2 : ToolsBoxSectionH2)
{
	fontColors[4] = "Magenta";
	fontColorLink = "Magenta";
	bitmap = "tlab/themes/DarkLab/container/GuiBoxRolloutH2.png";
};
