//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// GuiTextEditCtrl Profiles
//==============================================================================

//==============================================================================
//ToolsTextEdit - Base profile
singleton GuiControlProfile( ToolsTextEdit : ToolsDefaultProfile )
{
	bitmap = "tlab/themes/DarkLab/element-assets/GuiTextEditProfile.png";
	bevelColorLL = "255 0 255 255";
	fontSize = "15";
	textOffset = "2 0";
	justify = "Left";
	fontColors[0] = "252 254 252 255";
	fontColors[1] = "34 102 132 150";
	fontColors[2] = "243 243 243 255";
	fontColors[3] = "254 185 5 255";
	fontColors[9] = "255 0 255 255";
	fontColor = "252 254 252 255";
	fontColorHL = "34 102 132 150";
	fontColorNA = "243 243 243 255";
	fontColorSEL = "254 185 5 255";
	fontColors[7] = "255 0 255 255";
	fontColors[6] = "Magenta";
	hasBitmapArray = "1";
	border = "-2";
	borderThickness = "2";

	tab = "1";
	canKeyFocus = "1";
	fontColors[4] = "Fuchsia";
	fontColorLink = "Fuchsia";

	category = "ToolsText";
	fillColorNA = "138 138 138 222";
	fillColorSEL = "99 101 138 241";
	borderColor = "0 255 21 255";
	borderColorNA = "2 175 64 255";
	opaque = "1";
	fontColors[5] = "255 0 255 255";
	fontColorLinkHL = "255 0 255 255";
	borderColorHL = "3 12 250 255";
};
//------------------------------------------------------------------------------
// ToolsTextEdit_S1 - Child profile
singleton GuiControlProfile(ToolsTextEdit_S1 : ToolsTextEdit)
{
	fontSize = "14";
};
//------------------------------------------------------------------------------

//==============================================================================
// ToolsTextPadEdit - MLTextEdit profile used by TextPad and other MLTextEdit
//==============================================================================

//==============================================================================
//ToolsTextEditNum - Base profile
singleton GuiControlProfile(ToolsTextEdit_ML : ToolsTextEdit)
{
	fillColor = "238 236 241 9";
	borderColorHL = "231 231 231 236";

	fontSize = "17";
	fontColors[0] = "236 236 236 255";
	fontColors[1] = "3 3 3 255";
	fontColors[2] = "254 227 83 255";
	fontColors[3] = "0 0 2 255";
	fontColors[4] = "62 99 254 255";
	fontColor = "236 236 236 255";
	fontColorHL = "3 3 3 255";
	fontColorNA = "254 227 83 255";
	fontColorSEL = "0 0 2 255";
	fontColorLink = "62 99 254 255";
	category = "ToolsText";
	dynamicField = "defaultValue";
};
//------------------------------------------------------------------------------

//==============================================================================
// ToolsTextEditNum - TextEdit for numeric integers
//==============================================================================

//==============================================================================
//ToolsTextEditNum - Base profile
singleton GuiControlProfile( ToolsTextEditNum : ToolsTextEdit )
{
	numbersOnly = true;
};
//------------------------------------------------------------------------------

//==============================================================================
// ToolsTextEditNumSlider - TextEdit for numeric integers
//==============================================================================

//==============================================================================
//ToolsTextEditNumSlider - Base profile
singleton GuiControlProfile(ToolsTextEditNumSlider : ToolsTextEditNum)
{
	bitmap = "tlab/themes/DarkLab/element/GuiTextEditNumBox.png";
	opaque = "1";
	border = "1";
	borderThickness = "1";
	hasBitmapArray = "1";
	renderType = "4";
	fillColorHL = "97 97 106 204";
	borderColor = "57 118 175 228";
	fontColors[6] = "Magenta";
	textOffset = "4 0";
	fillColor = "32 32 32 57";
	fillColorNA = "145 145 145 117";
	fontColors[4] = "255 0 255 255";
	fontColorLink = "255 0 255 255";
	fillColorERR = "146 148 148 35";
	fontColors[0] = "254 229 178 255";
	fontColors[2] = "0 255 62 255";
	fontColor = "254 229 178 255";
	fontColorNA = "0 255 62 255";
};
//------------------------------------------------------------------------------

