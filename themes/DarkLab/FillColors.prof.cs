//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Color Panels -> Main theme colors (A-B-C)
//==============================================================================

singleton GuiControlProfile(ToolsFillBgA : ToolsDefaultProfile)
{
	fillColor = "15 15 15 255";
	opaque = "1";
	fontColors[7] = "255 0 255 255";
};

singleton GuiControlProfile(ToolsFillBgB : ToolsFillBgA)
{
	fillColor = "22 22 22 255";
};

singleton GuiControlProfile(ToolsFillBgC : ToolsFillBgB)
{
	fillColor = "32 32 32 255";
};

singleton GuiControlProfile(ToolsFillBgD : ToolsFillBgC)
{
	fillColor = "42 42 42 255";
};

//==============================================================================
// Transparent Fill Profiles with different Trans level of Darker or lighter
//==============================================================================

//------------------------------------------------------------------------------
singleton GuiControlProfile(ToolsFillDark_T50 : ToolsDefaultProfile)
{
	opaque = "1";
	fillColor = "2 2 2 127";
	border = "0";
	borderThickness = "1";
	borderColor = "101 101 101 92";
	bevelColorHL = "134 134 134 35";
	bevelColorLL = "157 159 157 19";
	borderColorNA = "30 3 241 192";
};

singleton GuiControlProfile(ToolsFillDark_T75 : ToolsFillDark_T50)
{
	fillColor = "3 3 3 191";
	border = "0";
	borderThickness = "3";
	borderColor = "255 18 0 255";
	bevelColorHL = "254 254 254 24";
	bevelColorLL = "254 254 254 227";
	borderColorHL = "3 224 48 255";
	borderColorNA = "99 99 99 56";
	fontColors[5] = "Fuchsia";
	fontColorLinkHL = "Fuchsia";
};

singleton GuiControlProfile(ToolsFillDark_T25 : ToolsFillDark_T50)
{
	fontColors[9] = "Magenta";
	fillColor = "3 3 3 63";
	border = "0";
	borderThickness = "1";
	borderColor = "254 245 199 24";
	bevelColorHL = "124 124 124 84";
	bevelColorLL = "101 101 101 0";
	borderColorNA = "30 3 240 255";
};

singleton GuiControlProfile(ToolsFillLight_T25 : ToolsDefaultProfile)
{
	fillColor = "136 136 136 16";
};

singleton GuiControlProfile(ToolsFillPreviewBG : ToolsFillBgD)
{
	fillColor = "138 138 138 255";
};

singleton GuiControlProfile(ToolsFillDnDZone : ToolsFillBgA)
{
	fillColor = "102 190 229 77";
	fontColors[3] = "255 255 255 255";
	fontColorSEL = "255 255 255 255";
	border = "1";
	borderThickness = "8";
	borderColor = "85 206 222 65";
	bevelColorHL = "227 9 129 255";
};
