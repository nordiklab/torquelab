//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// DropDown Profiles (GuiPopUpMenuCtrl)
//==============================================================================

//==============================================================================
// DropdownBasic Styles
//------------------------------------------------------------------------------
//DropdownBasic Item
//------------------------------------------------------------------------------
//DropdownBasic Menu
singleton GuiControlProfile (ToolsDropdownBase : ToolsDefaultProfile)
{
	modal = "1";
	profileForChildren = "ToolsDropdownBase_List";
	bitmap = "tlab/themes/DarkLab/element-assets/GuiDropdownBase_20.png";
	hasBitmapArray     = "1";

	opaque = "1";
	fillColor = "48 48 48 243";
	fillColorHL = "90 166 190 255";
	fillColorNA = "115 115 115 255";
	fillColorSEL = "122 173 182 255";

	fontSize = "17";
	textOffset = "4 -2";

	fontColor = "145 145 146 255";
	fontColorHL = "2 2 2 255";
	fontColorNA = "254 254 254 255";
	fontColorSEL = "2 2 2 255";
	category = "ToolsListing";
	fontColors[0] = "145 145 146 255";
	fontColors[1] = "2 2 2 255";
	fontColors[2] = "254 254 254 255";
	fontColors[3] = "2 2 2 255";
};
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//DropdownBasic List
singleton GuiControlProfile (ToolsDropdownBase_List : ToolsDropdownBase)
{
	hasBitmapArray     = "1";
	bitmap = "tlab/themes/DarkLab/container-assets/GuiScrollMain.png";
	opaque = "1";
	fontSize = "19";
	textOffset = "8 2";
	fillColorNA = "32 32 32 255";
	fillColorSEL = "208 254 254 255";
	fontColors[1] = "2 2 2 255";
	fontColors[3] = "3 25 37 255";
	fontColorHL = "2 2 2 255";
	fillColorHL = "5 5 5 255";
	fontColorSEL = "3 25 37 255";
};

singleton GuiControlProfile(ToolsDropdownBase_List_S1 : ToolsDropdownBase_List)
{
   fontSize = "14";
   tab = "0";
};
//==============================================================================
// DropdownBasic Thin Version
singleton GuiControlProfile(ToolsDropdownBase_S1 : ToolsDropdownBase)
{
	fontSize = "14";
	fillColorHL = "122 173 182 255";
	fillColorNA = "32 32 32 255";
	fillColorSEL = "2 2 2 255";
	fontColors[1] = "2 2 2 255";
	fontColors[3] = "3 25 37 255";
	fontColorHL = "2 2 2 255";
	fontColorSEL = "3 25 37 255";
	bitmap = "tlab/themes/DarkLab/element-assets/GuiDropdownBase_16.png";
	textOffset = "4 0";
	profileForChildren = "ToolsDropdownBase_List_S1";
};
//------------------------------------------------------------------------------
//==============================================================================
// DropdownBasic Thin Version
singleton GuiControlProfile(ToolsDropdownMain_20 : ToolsDropdownBase)
{
	bitmap = "tlab/themes/DarkLab/element-assets/GuiDropdownMain20.png";
	fontSize = "17";
	fillColorHL = "122 173 182 255";
	fillColorNA = "32 32 32 255";
	fillColorSEL = "2 2 2 255";
	fontColors[1] = "2 2 2 255";
	fontColors[3] = "3 25 37 255";
	fontColorHL = "2 2 2 255";
	fontColorSEL = "3 25 37 255";
	fontType = "Calibri Bold";
};
//------------------------------------------------------------------------------

//==============================================================================
// DropdownBasic Styles
//------------------------------------------------------------------------------
//DropdownBasic Item

//------------------------------------------------------------------------------
//DropdownBasic List
singleton GuiControlProfile (ToolsDropdownMain_List : ToolsDefaultProfile)
{
	hasBitmapArray     = "1";
	profileForChildren = "ToolsDropdownMain_Item";
	bitmap = "tlab/themes/DarkLab/container-assets/GuiScrollMain.png";
	category = "ToolsListing";
	opaque = "1";

};
//------------------------------------------------------------------------------
//DropdownBasic Menu
singleton GuiControlProfile (ToolsDropdownMain_20 : ToolsDropdownBase)
{
	hasBitmapArray     = "1";
	profileForChildren = "ToolsDropdownBase_List";
	bitmap = "tlab/themes/DarkLab/element-assets/GuiDropdownMain20.png";
	modal = "1";
	opaque = "1";
	fillColorHL = "122 173 182 255";
	fillColorNA = "32 32 32 255";
	fillColorSEL = "2 2 2 255";
	fontColors[1] = "2 2 2 255";
	fontColors[3] = "3 25 37 255";
	fontColorHL = "2 2 2 255";
	fontColorSEL = "3 25 37 255";
};
//------------------------------------------------------------------------------

singleton GuiControlProfile (ToolsDropdownMain_20 : ToolsDropdownBase)
{
	hasBitmapArray     = "1";
	profileForChildren = "ToolsDropdownBase_List";
	bitmap = "tlab/themes/Asgard/element/GuiDropdownBase_28.png";
	modal = "1";
	opaque = "1";
	fillColorHL = "0 173 182 255";
	fillColorNA = "0 32 32 255";
	fillColorSEL = "0 2 2 255";
	fontColors[1] = "0 2 2 255";
	fontColors[3] = "0 25 37 255";
	fontColorHL = "0 2 2 255";
	fontColorSEL = "3 25 37 255";
};
//------------------------------------------------------------------------------
//==============================================================================
// DropdownBasic Styles
//==============================================================================

singleton GuiControlProfile (IconDropdownProfile)
{
	opaque = false;
	border = -2;
	bitmap = "tlab/themes/DarkLab/element-assets/GuiDropdownBase_16.png";
	category = "Editor";
	fontColors[7] = "Fuchsia";
	fontColors[8] = "255 0 255 255";
};

singleton GuiControlProfile(ToolsDropdownMain_16 : ToolsDropdownMain_20)
{
	fontSize = "16";
	bitmap = "tlab/themes/DarkLab/element-assets/GuiDropdownMain16.png";
	justify = "Top";
};

