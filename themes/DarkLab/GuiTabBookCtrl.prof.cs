//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// GuiTabBookCtrl Profiles
//==============================================================================

//==============================================================================
//ToolsTabBookMain Style
//------------------------------------------------------------------------------
singleton GuiControlProfile( ToolsTabBookMain : ToolsDefaultProfile )
{
	hasBitmapArray = true;
	category = "ToolsContainers";
	fontColors[9] = "255 0 255 255";
	autoSizeWidth = "1";
	autoSizeHeight = "1";
	profileForChildren = "ToolsBoxDarkC_Top";
	bitmap = "tlab/themes/DarkLab/container/GuiTabBookMain.png";
	border = "0";
	borderThickness = "1";

	fontSize = "16";
	justify = "Center";
	fontColors[0] = "254 254 254 255";
	fontColor = "254 254 254 255";
	textOffset = "0 0";
	fontColors[3] = "146 146 146 255";
	fontColorSEL = "146 146 146 255";
	opaque = "1";
	fillColor = "113 113 113 0";
	borderColor = "46 46 46 102";
	fontColors[1] = "164 164 164 255";
	fontColorHL = "164 164 164 255";
	fillColorHL = "43 43 43 255";
	fillColorNA = "76 76 76 255";
	borderColorHL = "64 234 139 255";
	borderColorNA = "64 64 64 103";
	bevelColorHL = "43 43 43 204";
	bevelColorLL = "115 115 115 50";
};
//------------------------------------------------------------------------------

singleton GuiControlProfile(ToolsTabBookMain_S1 : ToolsTabBookMain)
{
	fontSize = "14";
	fontColors[0] = "254 248 217 255";
	fontColor = "254 248 217 255";
	bitmap = "tlab/themes/DarkLab/container/GuiTabBookMain_S1.png";
	justify = "Bottom";
	textOffset = "0 -1";
	opaque = "1";
	border = "1";
	fillColor = "2 2 2 94";
	borderThickness = "1";
	borderColor = "46 46 46 102";

};

singleton GuiControlProfile(ToolsTabBookAlt : ToolsTabBookMain)
{
	bitmap = "tlab/themes/DarkLab/container/GuiTabBookAlt.png";
	fillColor = "27 27 27 255";
	border = "5";
	borderColor = "101 101 101 55";
	borderColorNA = "245 36 85 255";
	bevelColorHL = "3 39 254 255";
	bevelColorLL = "5 254 57 255";
	fontType = "Calibri Bold";
	fontSize = "15";
	fontColors[0] = "254 236 180 255";
	fontColor = "254 236 180 255";
	textOffset = "0 0";
	fontColors[1] = "203 203 203 255";
	fontColors[3] = "76 236 41 255";
	fontColorHL = "203 203 203 255";
	fontColorSEL = "76 236 41 255";
	justify = "Bottom";
	borderThickness = "0";
};

singleton GuiControlProfile(ToolsTabBookAlt_S1 : ToolsTabBookAlt)
{
	bitmap = "tlab/themes/DarkLab/container/GuiTabBookAlt_S1.png";
	fillColor = "27 27 27 255";
	border = "5";
	borderColor = "101 101 101 55";
	borderColorNA = "245 36 85 255";
	bevelColorHL = "3 39 254 255";
	bevelColorLL = "5 254 57 255";
	fontType = "Calibri Bold";
	fontSize = "15";
	fontColors[0] = "254 236 180 255";
	fontColor = "254 236 180 255";
	textOffset = "0 0";
	fontColors[1] = "203 203 203 255";
	fontColors[3] = "76 236 41 255";
	fontColorHL = "203 203 203 255";
	fontColorSEL = "76 236 41 255";
	justify = "Bottom";
	borderThickness = "0";
};
