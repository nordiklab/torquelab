//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Base Inspector Profile
singleton GuiControlProfile( GuiInspectorProfile  : ToolsDefaultProfile )
{
	opaque = true;
	fillColor = "255 255 255 255";
	border = 0;
	cankeyfocus = true;
	tab = true;
	category = "Inspector";
	fontSize = "15";
	fontColors[7] = "Magenta";
};
//------------------------------------------------------------------------------
//==============================================================================
// Base Inspector Profile
singleton GuiControlProfile( InspectorNoModalProfile  : GuiInspectorProfile )
{
	modal = "0";
};
//------------------------------------------------------------------------------

singleton GuiControlProfile (InspectorPopUpMenuProfile : GuiInspectorProfile)
{
	profileForChildren = "ToolsDropdownBase_List";
	bitmap = "tlab/themes/DarkLab/element-assets/GuiDropdownBase_20.png";
	hasBitmapArray     = "1";
	autoSizeWidth = "0";
	autoSizeHeight = "0";
	fillColorNA = "71 78 95 114";
	border = "-2";
	renderType = "0";
	modal = "1";
	justify = "Left";
	fontSize = "17";
	fontColors[0] = "185 185 185 255";
	fontColors[1] = "248 189 90 255";
	fontColors[2] = "238 238 238 255";
	fontColor = "185 185 185 255";
	fontColorHL = "248 189 90 255";
	fontColorNA = "238 238 238 255";
	textOffset = "4 0";
	category = "Inspector";
	fontColors[3] = "3 25 37 255";
	fontColorSEL = "3 25 37 255";
	fillColor = "15 15 15 255";
};

//==============================================================================
//Used in SourceCode
singleton GuiControlProfile( GuiInspectorButtonProfile : GuiInspectorProfile )
{
	//border = 1;
	fontSize = "16";
	fontColor = "254 254 254 255";
	justify = "center";
	opaque = "1";
	border = "1";
	fontColors[0] = "254 254 254 255";
	fontColors[2] = "200 200 200 255";
	fontColorNA = "200 200 200 255";
	bitmap = "tlab/themes/DarkLab/buttons/GuiButtonBase.png";
	bevelColorLL = "Magenta";
	textOffset = "0 2";
	autoSizeWidth = "1";
	autoSizeHeight = "1";
	category = "Editor";
	modal = true;
};

//------------------------------------------------------------------------------
singleton GuiControlProfile( InspectorTypeButtonProfile : GuiInspectorButtonProfile )
{
	fontSize = "16";
	fontColor = "254 254 254 255";
	justify = "center";
	opaque = "1";
	border = "-2";
	fontColors[0] = "254 254 254 255";
	fontColors[2] = "200 200 200 255";
	fontColorNA = "200 200 200 255";
	bitmap = "tlab/themes/DarkLab/buttons/GuiButtonBase.png";
	hasBitmapArray = "1";
	fixedExtent = "0";
	bevelColorLL = "Fuchsia";
	textOffset = "0 0";
	autoSizeWidth = "1";
	autoSizeHeight = "1";
	modal = true;
	fontColors[1] = "Black";
	fontColorHL = "Black";
	fontColors[4] = "Magenta";
	fontColorLink = "Magenta";
};

//------------------------------------------------------------------------------

//==============================================================================
//Used in SourceCode
singleton GuiControlProfile(GuiInspectorSwatchButtonProfile : GuiInspectorButtonProfile)
{
	fillColor = "254 253 253 255";
	fillColorHL = "221 221 221 255";
	fillColorNA = "200 200 200 255";
	fontSize = "24";
	textOffset = "16 10";
	bitmap = "tlab/themes/DarkLab/buttons/GuiButtonBase.png";
	hasBitmapArray = "1";
	fontColors[0] = "253 253 253 255";
	fontColor = "253 253 253 255";
	border = "-1";
	fontColors[2] = "Black";
	fontColorNA = "Black";
	modal = true;
};

//------------------------------------------------------------------------------

//==============================================================================
// Inspector Text Profiles
//==============================================================================

//==============================================================================
// SourceCode TextEdit Profile
singleton GuiControlProfile( GuiInspectorTextEditProfile : GuiInspectorProfile )
{
	borderColor = "100 100 100 255";
	borderColorNA = "194 194 194 255";
	fillColorNA = "White";
	borderColorHL = "50 50 50 50";
	tab = "1";
	canKeyFocus = "1";
	opaque = "1";
	fillColor = "0 0 0 0";
	fillColorHL = "139 139 139 233";
	fontColors[1] = "254 254 254 255";
	fontColors[2] = "100 100 100 255";
	fontColors[3] = "16 108 87 255";
	fontColors[9] = "255 0 255 255";
	fontColorHL = "254 254 254 255";
	fontColorNA = "100 100 100 255";
	fontColorSEL = "16 108 87 255";
	fontColors[0] = "254 254 254 255";
	fontColor = "254 254 254 255";
	bitmap = "tlab/themes/DarkLab/element-assets/GuiTextEditProfile.png";
	hasBitmapArray = "1";
	modal = true;
	fontSize = "16";
	fontColors[6] = "Fuchsia";
	textOffset = "0 0";
};

//------------------------------------------------------------------------------
//==============================================================================

//------------------------------------------------------------------------------
//MeshRoad, NavEditor, River and Road uses
singleton GuiControlProfile( GuiInspectorFieldInfoMLTextProfile : GuiInspectorProfile )
{
	opaque = false;
	border = 0;
	textOffset = "0 0";
};

//==============================================================================
// Inspector Group Profiles
//==============================================================================
//==============================================================================
// Used in SourceCode
singleton GuiControlProfile( GuiInspectorGroupProfile : GuiInspectorProfile )
{
	justify = "Left";
	fontColors[0] = "241 241 241 255";
	fontColors[1] = "25 25 25 220";
	fontColors[2] = "128 128 128 255";
	fontColors[9] = "255 0 255 255";
	fontColor = "241 241 241 255";
	fontColorHL = "25 25 25 220";
	fontColorNA = "128 128 128 255";
	textOffset = "6 -2";
	bitmap = "tlab/themes/DarkLab/container/GuiRolloutBaseB_S1.png";
	opaque = "0";
	fillColor = "66 66 66 222";
	fillColorNA = "255 255 255 255";
	fontSize = "20";
	fillColorHL = "255 255 255 255";
	fontColors[3] = "43 107 206 255";
	fontColorSEL = "43 107 206 255";
	fontColors[8] = "255 0 255 255";
	hasBitmapArray = "1";
	fontType = "Calibri Bold";
	fontColors[5] = "Fuchsia";
	fontColorLinkHL = "Fuchsia";
};

//------------------------------------------------------------------------------
//==============================================================================
// Used in SourceCode
singleton GuiControlProfile( GuiInspectorStackProfile : GuiInspectorProfile )
{
	opaque = "1";
	border = false;
	fillColor = "51 51 51 255";
	fontColors[6] = "255 0 255 255";
	fillColorHL = "32 32 34 255";
	fontColors[0] = "238 238 238 255";
	fontColor = "238 238 238 255";
	fontColors[1] = "231 224 178 255";
	fontColorHL = "231 224 178 255";
};

//------------------------------------------------------------------------------
//==============================================================================
// Used in SourceCode -> Rollout for Array settings (Ex: GroundCover Layers)
singleton GuiControlProfile( GuiInspectorRolloutProfile0 : GuiInspectorProfile)
{
	// font
	fontSize = "15";
	fontColor = "254 208 132 255";
	fontColorHL = "32 100 100";
	fontColorNA = "0 0 0";
	justify = "left";
	opaque = false;
	border = 0;
	borderColor   = "190 190 190";
	borderColorHL = "156 156 156";
	borderColorNA = "64 64 64";
	bitmap = "tlab/themes/DarkLab/container/GuiRolloutBaseB_S1.png";
	textOffset = "4 0";
	fontColors[0] = "254 208 132 255";
	fontColors[1] = "32 100 100 255";
	fontColors[9] = "255 0 255 255";
	modal = true;
	fontColors[7] = "Fuchsia";
	fontColors[8] = "255 0 255 255";
};

//==============================================================================
// Inspector Fields Profiles
//==============================================================================
//==============================================================================
// Used in SourceCode
singleton GuiControlProfile( GuiInspectorFieldProfile : GuiInspectorProfile )
{
	fontType    = "Calibri";
	fontSize    = "17";
	fontColor = "254 245 192 220";
	fontColorHL = "113 113 113 255";
	fontColorNA = "190 190 190 255";
	justify = "left";
	opaque = "1";
	border = false;
	bitmap = "tlab/themes/common/assets/rollout";
	textOffset = "0 0";
	tab = "1";
	canKeyFocus = "1";
	fillColor = "21 21 21 255";
	fillColorHL = "14 74 124 255";
	fillColorNA = "15 166 36 233";
	borderColor = "90 90 90 198";
	borderColorHL = "136 136 136 255";
	borderColorNA = "29 11 206 255";
	fontColors[0] = "254 245 192 220";
	fontColors[1] = "113 113 113 255";
	fontColors[2] = "190 190 190 255";
	fontColors[9] = "255 0 255 255";
	fillColorSEL = "99 101 138 255";
	mouseOverSelected = "1";
	borderThickness = "2";
	fontColors[3] = "196 196 196 255";
	fontColorSEL = "196 196 196 255";
};

//------------------------------------------------------------------------------
//==============================================================================
// Used in SourceCode
singleton GuiControlProfile( GuiInspectorMultiFieldProfile : GuiInspectorFieldProfile )
{
	opaque = true;
	fillColor = "50 50 230 30";
	fillColorHL = "3 196 16 243";
   fillColorERR = "Red";
   fontSize = "20";
};

//------------------------------------------------------------------------------

//==============================================================================
// Used in SourceCode
singleton GuiControlProfile( GuiInspectorMultiFieldDifferentProfile : GuiInspectorMultiFieldProfile )
{
	opaque = true;
	fillColor = "50 50 230 30";
	fillColorHL = "3 196 16 243";
};

//------------------------------------------------------------------------------

//==============================================================================
// Inspector Dynamic Fields Profiles
//==============================================================================
//==============================================================================
// Used in SourceCode
singleton GuiControlProfile( GuiInspectorDynamicFieldProfile : GuiInspectorFieldProfile )
{
	border = "0";
	borderColor = "190 190 190 255";
	opaque = "0";
	fillColor = "51 51 51 255";
	fillColorHL = "32 32 34 255";
	fontColors[0] = "199 241 254 255";
	fontColors[1] = "231 224 178 255";
	fontColors[2] = "100 100 100 255";
	fontColors[3] = "43 107 206 255";
	fontColor = "199 241 254 255";
	fontColorHL = "231 224 178 255";
	fontColorNA = "100 100 100 255";
	fontColorSEL = "43 107 206 255";
	dynamicField = "defaultValue";
	fillColorNA = "244 244 244 255";
	fillColorSEL = "99 101 138 156";
	fontSize = "15";
};

//------------------------------------------------------------------------------

//==============================================================================
// Gui Inspector TYPE Profiles
//==============================================================================

//==============================================================================
// Used in SourceCode - Inspector Checkbox single profile
singleton GuiControlProfile( InspectorTypeCheckboxProfile : GuiInspectorProfile )
{
	bitmap = "tlab/themes/DarkLab/buttons/GuiCheckBoxBase.png";
	hasBitmapArray = true;
	opaque=false;
	border=false;
	textOffset = "4 0";
	modal = true;
	bevelColorLL = "Fuchsia";
	fillColor = "3 14 21 166";
	fontColors[0] = "254 243 171 255";
	fontColor = "254 243 171 255";
};

//------------------------------------------------------------------------------

//==============================================================================
// Used in SourceCode - Inspector Checkbox single profile
singleton GuiControlProfile( GuiInspectorBitMaskArrayProfile : GuiInspectorProfile )
{
	bitmap = "tlab/themes/DarkLab/buttons/GuiCheckBoxBase.png";
	hasBitmapArray = true;
	opaque=false;
	border=false;
	textOffset = "4 0";
	modal = true;
	bevelColorLL = "Fuchsia";
	fillColor = "3 14 21 166";
	fontColors[0] = "254 243 171 255";
	fontColor = "254 243 171 255";
};

//------------------------------------------------------------------------------

//==============================================================================
// Push, Pop and Toggle Dialogs
//==============================================================================

