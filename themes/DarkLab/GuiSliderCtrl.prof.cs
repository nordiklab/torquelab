//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
//==============================================================================
// Slider Profiles -> (GuiSliderCtrl)
//==============================================================================
//==============================================================================

//==============================================================================
// GuiSliderCtrl Profiles
//==============================================================================

//==============================================================================
// ToolsSliderProfile Style
//------------------------------------------------------------------------------
singleton GuiControlProfile(ToolsSliderProfile : ToolsDefaultProfile)
{
	category = "ToolsValues";
	bitmap = "tlab/themes/DarkLab/element-assets/GuiSliderProfile_S1.png";
	border = "2";
	fontSize = "14";
	hasBitmapArray = "1";
};
//------------------------------------------------------------------------------

//==============================================================================
// Used by: Animation timeline
singleton GuiControlProfile( ToolsSliderMarker : ToolsSliderProfile )
{
	bitmap = "tlab/themes/DarkLab/element-assets/GuiSliderMarker.png";
	category = "ToolsValues";
};

singleton GuiControlProfile(ToolsSliderMarkerThin_L1 : ToolsSliderMarker)
{
	bitmap = "tlab/themes/DarkLab/element/GuiSliderMarkerThin_L1.png";
	fontColors[0] = "236 236 236 0";
	fontColors[1] = "254 220 175 255";
	fontColors[2] = "32 32 32 0";
	fontColors[3] = "255 255 255 0";
	fontColor = "236 236 236 0";
	fontColorHL = "254 220 175 255";
	fontColorNA = "32 32 32 0";
	fontColorSEL = "255 255 255 0";
};

singleton GuiControlProfile(ToolsSliderBlank : ToolsSliderMarkerThin_L1)
{
	hasBitmapArray = "0";
};
