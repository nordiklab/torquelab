//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// GuiWindowCtrl Profiles
//==============================================================================

//==============================================================================
//ToolsWindowPanel Style
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

singleton GuiControlProfile(ToolsWindowPanel : ToolsDefaultProfile)
{
	fontColors[8] = "255 0 255 255";
	bitmap = "tlab/themes/DarkLab/container/GuiWindowPanel.png";

	fontSize = "18";
	fontColors[0] = "234 234 234 255";
	fontColor = "234 234 234 255";
	textOffset = "6 0";
	border = "-1";
	category = "ToolsWindows";
	fillColor = "32 32 32 255";
	borderThickness = "1";
};
singleton GuiControlProfile(ToolsWindowPanel_L1 : ToolsWindowPanel)
{
	bitmap = "tlab/themes/DarkLab/container/GuiWindowPanel_L1.png";
	fontSize = "22";

};
//==============================================================================
//ToolsWindowDialog Style
//------------------------------------------------------------------------------
singleton GuiControlProfile(ToolsWindowDialog : ToolsWindowPanel)
{
	bitmap = "tlab/themes/DarkLab/container/GuiWindowDialog.png";
	fillColor = "37 37 37 255";
	fontColors[0] = "245 245 245 255";
	fontColor = "245 245 245 255";
	textOffset = "6 0";
	fontColors[3] = "255 255 255 255";
	fontColorSEL = "255 255 255 255";
	fontColors[9] = "Magenta";
	fillColorHL = "228 228 235 255";
	fillColorNA = "255 255 255 255";
	fillColorSEL = "194 194 196 255";
	borderColor = "200 200 200 255";

	fontSize = "21";
	fontColors[7] = "Fuchsia";
	bevelColorLL = "Black";
	fontColors[5] = "255 0 255 255";
	fontColorLinkHL = "255 0 255 255";
	border = "-1";
	justify = "Left";
	borderThickness = "0";
	hasBitmapArray = "1";
};
//------------------------------------------------------------------------------

singleton GuiControlProfile(ToolsWindowBox : ToolsWindowPanel)
{
	bitmap = "tlab/themes/DarkLab/container/GuiWindowBox.png";
	fillColor = "12 12 12 255";
};
singleton GuiControlProfile(LabWindowProfile : ToolsWindowPanel)
{
	category = "Default";
};

singleton GuiControlProfile(ToolsWindowRollout : ToolsWindowPanel)
{
	bitmap = "tlab/themes/DarkLab/container/GuiWindowRollout.png";
};

singleton GuiControlProfile(ToolsWindowPanel_C : ToolsWindowPanel)
{
	fontColors[5] = "Fuchsia";
	fontColorLinkHL = "Fuchsia";
	justify = "Center";
};
