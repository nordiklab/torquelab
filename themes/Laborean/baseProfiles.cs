//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
/*
 tab = false;
   canKeyFocus = false;
   hasBitmapArray = false;
   mouseOverSelected = false;

   // fill color
   opaque = false;
   fillColor = "242 241 240";
   fillColorHL ="228 228 235";
   fillColorSEL = "98 100 137";
   fillColorNA = "255 255 255 ";

   // border color
   border = 0;
   borderColor   = "100 100 100";
   borderColorHL = "50 50 50 50";
   borderColorNA = "75 75 75";

   // font
   fontType = "Arial";
   fontSize = 14;
   fontCharset = ANSI;

   fontColor = "0 0 0";
   fontColorHL = "0 0 0";
   fontColorNA = "0 0 0";
   fontColorSEL= "255 255 255";

   // bitmap information
   bitmap = "";
   bitmapBase = "";
   textOffset = "0 0";

   // used by guiTextControl
   modal = true;
   justify = "left";
   autoSizeWidth = false;
   autoSizeHeight = false;
   returnTab = false;
   numbersOnly = false;
   cursorColor = "0 0 0 255";*/

singleton GuiControlProfile (ToolsDefaultProfile )
{
	tab = false;
	canKeyFocus = false;
	hasBitmapArray = false;
	mouseOverSelected = false;

	// fill color
	opaque = "0";
	fillColor = "243 241 241 56";
	fillColorHL ="50 50 50 255";
	fillColorSEL = "99 101 138 ";
	fillColorNA = "132 132 132 255";

	// border color
	border = "0";

	borderColor = "101 101 101 69";
	borderColorHL = "3 224 48 141";
	borderColorNA = "30 3 240 255";
	bevelColorLL = "101 101 101 0";
	bevelColorHL = "124 124 124 84";

	// font
	//fontType = "Calibri";
	fontType = "Calibri";
	fontSize = "18";
	fontCharset = ANSI;

	fontColor = "0 0 0 255";
	fontColorHL = "0 0 0 255";
	fontColorNA = "0 0 0 255";
	fontColorSEL= "255 255 255";

	// bitmap information
	bitmapBase = "";
	textOffset = "0 0";

	// used by guiTextControl
	modal = true;
	justify = "left";
	autoSizeWidth = false;
	autoSizeHeight = false;
	returnTab = false;
	numbersOnly = false;
	cursorColor = "0 0 0 255";
	fillColorERR = "Red";
	borderThickness = "0";
	category = "Tools";
	fontColors[5] = "255 0 255 255";
	fontColorLinkHL = "255 0 255 255";
	fontColors[7] = "255 0 255 255";
};

singleton GuiControlProfile( ToolsDefaultProfile_NoModal : ToolsDefaultProfile )
{
	modal = false;
	category = "Tools";
	fontColors[5] = "Fuchsia";
	fontColors[7] = "255 0 255 255";
	fontColorLinkHL = "Fuchsia";
	opaque = "0";
	fillColor = "248 248 248 0";
	fillColorERR = "255 0 0 0";
	fillColorHL = "50 50 50 0";
	fillColorNA = "132 132 132 0";
	fillColorSEL = "99 101 138 0";
	borderColor = "146 146 146 0";
	borderColorHL = "3 224 48 0";
	borderColorNA = "111 57 241 0";
	bevelColorHL = "125 125 125 0";
};

singleton GuiControlProfile (ToolsToolTipProfile : ToolsDefaultProfile)
{

	// fill color
	fillColor = "72 72 72";

	// border color
	borderColor   = "196 196 196 255";

	// font
	fontType = "Calibri";
	fontSize = "17";
	fontColor = "255 255 255 255";

	category = "Core";
	fontColors[0] = "255 255 255 255";
};

//==============================================================================
// Used by: Collada Import
singleton GuiControlProfile( ToolsProgressBitmapProfile )
{
	border = false;
	hasBitmapArray = true;
	bitmap = "tlab/themes/common/assets/rl-loadingbar";
	category = "ToolsValues";
};

//==============================================================================
// Used by:
singleton GuiControlProfile( ToolsEditorProfile )
{
	canKeyFocus = true;
	fillColor = "192 192 192 192";
	category = "Tools";
};

//==============================================================================
// Used by: AddFMOD, ColorPicker, GuiEaseEdit, UVEditor...
singleton GuiControlProfile( ToolsGroupBorderProfile )
{
	border = false;
	opaque = false;
	hasBitmapArray = true;
	bitmap = "tlab/themes/common/assets/group-border";
	category = "Tools";
};

//==============================================================================
// Used by: GuiFormClass ??
singleton GuiControlProfile( ToolsGuiFormProfile : ToolsDefaultProfile )
{
	opaque = "1";
	border = 5;
	justify = "center";
	profileForChildren = ToolsButtonBase;
	opaque = false;
	hasBitmapArray = true;
	bitmap = "tlab/themes/common/assets/button";
	category = "Tools";
};

//==============================================================================
// TorqueLab Top Menu Bar Profiles
//==============================================================================
//==============================================================================
// MenuBar Background
singleton GuiControlProfile (ToolsMenuBarProfile  : ToolsDefaultProfile)
{
	opaque = "1";
	border = "0";
	category = "Tools";
	fillColor = "22 22 22 255";
	fontSize = "20";
	fontColors[0] = "251 251 249 255";
	fontColor = "251 251 249 255";
	justify = "Bottom";
	textOffset = "22 22";
	fontColors[8] = "255 0 255 255";
	fillColorHL = "46 46 46 170";
	fillColorNA = "127 64 29 255";
	fontColors[1] = "15 255 0 255";
	fontColors[2] = "208 132 6 255";
	fontColors[3] = "240 185 39 255";
	fontColorHL = "15 255 0 255";
	fontColorNA = "208 132 6 255";
	fontColorSEL = "240 185 39 255";
	cursorColor = "255 0 255 255";
	modal = true;
	borderColorHL = "254 254 55 0";
	borderColorNA = "255 213 0 210";
	bevelColorHL = "106 106 106 255";
	bevelColorLL = "3 3 213 255";
	autoSizeWidth = "1";
	autoSizeHeight = "1";
	borderColor = "0 148 255 67";
	fontColors[7] = "255 0 255 255";
	fontColors[9] = "Fuchsia";
	fontColors[5] = "Magenta";
	fontColorLinkHL = "Magenta";
};

//------------------------------------------------------------------------------
//==============================================================================
// MenuBar Items
singleton GuiControlProfile( ToolsMenuProfile : ToolsMenuBarProfile)
{
	opaque = true;
	fillcolor = "32 32 32 255";
	fontColor = "213 213 213 255";
	fontColorHL = "64 222 254 255";
	category = "Tools";
	fillColorHL = "43 43 43 241";
	fontSize = "18";
	fontColors[0] = "213 213 213 255";
	fontColors[1] = "64 222 254 255";
	fillColorNA = "29 132 21 252";
	fillColorSEL = "11 43 190 255";
	justify = "Center";
	fontColors[2] = "102 62 27 188";
	fontColors[3] = "254 227 97 255";
	fontColorNA = "102 62 27 188";
	fontColorSEL = "254 227 97 255";
	cursorColor = "0 0 0 255";
	modal = true;
	border = "0";
	borderColor = "122 118 122 255";
	borderColorHL = "141 141 141 255";
	borderColorNA = "120 113 120 255";
	fontColors[7] = "255 0 255 255";
	bevelColorHL = "104 104 104 255";
	bevelColorLL = "157 157 157 255";
	bitmap = "tlab/themes/DarkLab/element-assets/GuiMenuBitmaps.png";
	hasBitmapArray = "1";
	fontColors[4] = "Fuchsia";
	fontColorLink = "Fuchsia";
	textOffset = "0 0";
};
//------------------------------------------------------------------------------
// Derived menu profile for Popup - By default GuiMenuBarProfile is used for SDL
// platform and was causing issues since it was missing. To allow to customized
// popup without affecting menu, there's some new profiles used by a special
// popup builder that overide default GuiMenuBarProfile (newPopupMenu(%name,%style))
// EDIT: WOuld need some code change as it's not care about profile changes
//------------------------------------------------------------------------------
/*
singleton GuiControlProfile(ToolsPopupMenuProfile : ToolsMenuBarProfile)
{
   opaque = "1";
};
//------------------------------------------------------------------------------
singleton GuiControlProfile(ToolsPopupMenuDark : ToolsMenuBarProfile)
{
   opaque = "1";
};*/
//------------------------------------------------------------------------------
singleton GuiControlProfile( GuiMenuBarProfile )
{
	fillcolor = "2 2 2 249";
	fillcolorHL = "34 34 34 255";
	borderColor = "200 200 200 66";
	borderColorHL = "162 162 162 255";
	border = "1";
	borderThickness = 1;
	opaque = true;
	mouseOverSelected = true;
	category = "Editor";
	bitmap = "tlab/themes/Common/assets/checkbox-menubar";
	borderColorNA = "64 240 243 41";
	bevelColorHL = "106 106 106 67";
	bevelColorLL = "180 180 211 255";
	fontColors[0] = "254 247 201 255";
	fontColors[1] = "87 222 243 255";
	fontColor = "254 247 201 255";
	fontColorHL = "87 222 243 255";
	fontColorNA = "99 145 153 255";
	fontSize = "18";
	fontColors[2] = "99 145 153 255";

};
//------------------------------------------------------------------------------
//==============================================================================
// Default TextPad profile for the Multiline text editors
//==============================================================================
//==============================================================================
// MenuBar Background
singleton GuiControlProfile(ToolsTextPadBox : ToolsDefaultProfile)
{

	fontSize = "21";
	fontColors[0] = "254 254 254 245";
	fontColors[3] = "254 248 234 255";
	fontColor = "254 254 254 245";
	fontColorSEL = "254 248 234 255";
	textOffset = "0 0";
	cursorColor = "102 132 203 255";
	autoSizeWidth = "1";
	autoSizeHeight = "1";
};

//------------------------------------------------------------------------------
singleton GuiControlProfile( ToolsTextPadScroll : ToolsTextPadBox )
{
	bitmap = "tlab/themes/DarkLab/container/GuiScrollProfile.png";
	opaque = "1";
	fillColor = "22 22 22 255";
	category = "Tools";
	border = "1";
	borderThickness = "1";
	borderColor = "148 155 148 43";
	borderColorHL = "50 50 50 255";
	borderColorNA = "51 51 51 255";
	bevelColorHL = "3 3 3 255";
	bevelColorLL = "12 12 12 255";
};
//------------------------------------------------------------------------------
singleton GuiControlProfile( ToolsTextPadEdit : ToolsTextPadBox )
{
	category = "Tools";
	fillColor = "215 210 204 255";
	fillColorHL = "3 87 94 141";
	fillColorSEL = "99 101 138 0";
	borderColorHL = "231 231 231 236";
	fontSize = "19";
	fontColors[0] = "2 2 2 255";
	fontColors[1] = "254 254 254 255";
	fontColors[2] = "254 227 83 255";
	fontColors[3] = "0 0 2 255";
	fontColors[4] = "60 64 85 255";
	fontColor = "2 2 2 255";
	fontColorHL = "254 254 254 255";
	fontColorNA = "254 227 83 255";
	fontColorSEL = "0 0 2 255";
	fontColorLink = "60 64 85 255";
	tab = "0";
	canKeyFocus = "1";
	autoSizeWidth = "1";
	autoSizeHeight = "1";
	opaque = "1";
	cursorColor = "37 55 83 255";
	mouseOverSelected = "1";
};

singleton GuiControlProfile(ToolsProgressBar : ToolsProgressBitmapProfile)
{
	fillColor = "16 104 153 134";
	fillColorHL = "0 44 255 255";
	fillColorNA = "255 199 0 255";
	cursorColor = "Black";
	hasBitmapArray = "0";
	opaque = "1";
	border = "0";
	borderColor = "255 234 0 255";
	borderColorHL = "39 39 39 241";
	fontType = "Calibri";
	fontSize = "26";
	justify = "Center";
	fontColors[0] = "252 252 252 255";
	fontColor = "252 252 252 255";
};

singleton GuiControlProfile(ToolsProgressBarBG : ToolsProgressBar)
{
	fillColor = "39 39 39 255";
	cursorColor = "0 0 0 255";
	borderThickness = "0";
	borderColor = "0 0 0 255";
	borderColorHL = "0 0 0 241";
	borderColorNA = "5 5 5 255";
	fontColors[0] = "0 0 0 255";
	fontColor = "0 0 0 255";
};

