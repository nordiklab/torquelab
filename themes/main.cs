//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function Lab::initThemeSystem(%this)
{
   exec("tlab/themes/defaults.cs");
   if ($Cfg_TLab_Theme $= "")
		$Cfg_TLab_Theme = $Cfg_TLab_DefaultTheme;	
   //Load default theme unless it's the current theme
   if ($Cfg_TLab_Theme !$= $Cfg_TLab_DefaultTheme)
      Lab.preloadTheme($Cfg_TLab_DefaultTheme,1);
   Lab.setThemeModule($Cfg_TLab_Theme);
}

function Lab::setActiveTheme(%this,%moduleID)
{
   %modObj = TLModules.getModuleObj(%moduleID);  
    $Cfg_TLab_Theme = %moduleID;
}

function Lab::setThemeModule(%this,%moduleID)
{
     if (!TLModules.isModule(%moduleID))
     {
         warnLog("Invalid Theme Module supplied, most be a valid Theme ModuleID. Received:",  %moduleID);
     }
     foreach(%theme in TLThemes)
	{
	   %theme.superClass = "ThemeModule";
	   if (%theme.display $= "")
	      %theme.display = %theme.moduleID;	      
      
      TLModules.unloadModule(%theme);//Will only unload if loaded
      %theme.activated = false;     
	}
	
   //Let the ModuleManager handle the dependencies (child)  
     TLModules.loadModule(%moduleID);      
     Lab.setActiveTheme(%moduleID);   
}

//This is called during the ModulePreLoad of a Theme module
function Lab::preloadTheme(%this,%theme,%noFileAssign)
{
        
   %themePath = "tlab/themes/"@%theme@"/";
   if (!isDirectory(%themePath))
   {
      warnLog("Invalid theme path provided:",%themePath,"For theme:", %theme);
      return false;
   }
    //info("Loading UI Theme:",$Cfg_TLab_Theme);
	execThemeFile(%themePath@"Settings.cs",%noFileAssign);
	execThemeFile(%themePath@"profileDefaults.cs",%noFileAssign);
	
	//Always get the baseProfiles first because .prof.cs are derived from it
	execThemeFile(%themePath@"baseProfiles.cs",%noFileAssign);

   //Now load all profiles files
	%filePathScript = %themePath@"*.prof.cs";
	for(%file = findFirstFile(%filePathScript); %file !$= ""; %file = findNextFile(%filePathScript))
	{
		execThemeFile( %file ,%noFileAssign);
	}

   //Finish by the inspector profiles to avoid conflicts
	execThemeFile(%themePath@"inspectorProfiles.cs",%noFileAssign);   
   
}

//Special exec file function for themes which will assign the correct filename Profiles
function execThemeFile(%file,%noFileAssign)
{
   if (!isFile(%file))
      return;

   exec( %file );
   if (!%noFileAssign)
      changeProfilesFilenameInFile(%file);
	
}
function changeProfilesFilenameInFile(%file)
{

  %fileObj = getFileReadObj(%file);

	if (!isObject(%fileObj)) return;

	while(!%fileObj.isEOF())
	{
		%line = trim(%fileObj.readline());
		//Get the GuiProfileControl name from line
		// singleton GuiControlProfile (ToolsDropdownMain_20 : ToolsDropdownBase)
		if (firstWord(%line) $= "Singleton")
		{
		  %dataLine =  strreplace(%line,"(","\t");
		  %dataLine =  strreplace(%dataLine,":","\t");
		  %dataLine =  strreplace(%dataLine,")","\t");
		  %dataLine =  strreplace(%dataLine,"on ","on\t");
		  %name = trim(getField(%dataLine,2));
		
		  if (isObject(%name))
		   %name.setFileName(%file);		   
		}
		
	}
	closeFileObj(%fileObj);
}

function initThemeProfilesData()
{
	//doGuiGroupAction("GLab_GameProfileMenu","clear()");
	//eLink.doEval("GLab_GameProfileMenu","clear()");
	newSimSet("ThemeProfileGroup");

	foreach(%obj in GuiDataGroup)
	{
		if (!%obj.isMemberOfClass("GuiControlProfile"))
			continue;

      %file = %obj.getFileName();
      if (getSubStr(%file,0,11) !$= "tlab/themes")
         continue;
      
   ThemeProfileGroup.add(%obj);

	}
	return ThemeProfileGroup.getId();
}
