//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
singleton GuiControlProfile (ToolsDefaultProfile )
{
	tab = false;
	canKeyFocus = false;
	hasBitmapArray = false;
	mouseOverSelected = false;

	// fill color
	opaque = "0";
	fillColor = "243 241 241 56";
	fillColorHL ="50 50 50 255";
	fillColorSEL = "99 101 138 ";
	fillColorNA = "132 132 132 255";

	// border color
	border = "0";

	borderColor = "101 101 101 69";
	borderColorHL = "3 224 48 141";
	borderColorNA = "30 3 240 255";
	bevelColorLL = "101 101 101 0";
	bevelColorHL = "124 124 124 84";

	// font
	//fontType = "Calibri";
	fontType = "Arial Black";
	fontSize = "18";
	fontCharset = ANSI;

	fontColor = "0 0 0 255";
	fontColorHL = "0 0 0 255";
	fontColorNA = "0 0 0 255";
	fontColorSEL= "255 255 255";

	// bitmap information
	bitmapBase = "";
	textOffset = "0 0";

	// used by guiTextControl
	modal = true;
	justify = "left";
	autoSizeWidth = false;
	autoSizeHeight = false;
	returnTab = false;
	numbersOnly = false;
	cursorColor = "0 0 0 255";
	fillColorERR = "Red";
	borderThickness = "0";
	category = "Tools";
	fontColors[5] = "255 0 255 255";
	fontColorLinkHL = "255 0 255 255";
	fontColors[7] = "255 0 255 255";
};

//==============================================================================

//==============================================================================
// Standard Text Profiles
//==============================================================================
singleton GuiControlProfile(ToolsTextBase : ToolsDefaultProfile)
{
	fontColor = "252 254 252 255";

	fillColor = "76 76 76 255";
	bevelColorHL = "255 0 255 255";
	justify = "Left";
	category = "ToolsText";
	fontColors[0] = "252 254 252 255";
	fontColors[1] = "252 189 81 255";
	fontColors[2] = "95 90 57 255";
	fontColorHL = "252 189 81 255";
	fontColorNA = "95 90 57 255";
	fontSize = "15";
	fontColors[4] = "238 255 0 255";
	fontColorLink = "238 255 0 255";
	opaque = "0";
	fontColors[3] = "0 255 220 255";
	fontColors[5] = "3 254 148 255";
	fontColors[6] = "3 21 254 255";
	fontColors[7] = "254 236 3 255";
	fontColors[8] = "254 3 43 255";
	fontColors[9] = "3 48 248 255";
	fontColorSEL = "0 255 220 255";
	fontColorLinkHL = "3 254 148 255";
	colorFont = "DefaultFontA";
	fillColorHL = "229 229 236 68";
	fillColorSEL = "29 136 178 6";
	borderColorHL = "50 50 50 89";
	modal = "1";
};
singleton GuiControlProfile(ToolsTextBase_C : ToolsTextBase)
{
	locked = true;
	justify = "Center";
};
singleton GuiControlProfile(ToolsTextBase_R : ToolsTextBase)
{
	locked = true;
	justify = "Right";
};
//==============================================================================
singleton GuiControlProfile(ToolsTextBase_S1 : ToolsTextBase)
{
	fontSize = "14";
	fontColors[7] = "255 0 255 255";
};
singleton GuiControlProfile(ToolsTextBase_S1_C : ToolsTextBase_S1)
{
	locked = true;
	justify = "Center";
};
singleton GuiControlProfile(ToolsTextBase_S1_R : ToolsTextBase_S1)
{
	locked = true;
	justify = "Right";
};
//------------------------------------------------------------------------------
//==============================================================================
singleton GuiControlProfile(ToolsTextBase_L1 : ToolsTextBase)
{
	fontSize = "18";
	fontColors[7] = "255 0 255 255";
};
singleton GuiControlProfile(ToolsTextBase_L1_C : ToolsTextBase_L1)
{
	locked = true;
	justify = "Center";
};
singleton GuiControlProfile(ToolsTextBase_L1_R : ToolsTextBase_L1)
{
	locked = true;
	justify = "Right";
};
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//==============================================================================
// Standard Text Header Profiles
//==============================================================================
//==============================================================================
singleton GuiControlProfile(ToolsTextBase_H1 : ToolsTextBase)
{

	fillColor = "238 236 240 255";
	bevelColorHL = "255 0 255 255";
	justify = "Left";
	category = "ToolsText";
	fontSize = "18";
	fontColors[0] = "238 236 240 255";
	fontColors[1] = "252 189 81 255";
	fontColors[2] = "254 227 83 255";
	fontColors[3] = "254 3 62 255";
	fontColors[4] = "238 255 0 255";
	fontColors[5] = "3 254 148 255";
	fontColors[6] = "3 21 254 255";
	fontColors[7] = "254 236 3 255";
	fontColors[8] = "254 3 43 255";
	fontColors[9] = "3 48 248 255";
	fontColor = "238 236 240 255";
	fontColorHL = "252 189 81 255";
	fontColorNA = "254 227 83 255";
	fontColorSEL = "254 3 62 255";
	fontColorLink = "238 255 0 255";
	fontColorLinkHL = "3 254 148 255";
	cursorColor = "Black";
	bevelColorLL = "Magenta";
};
singleton GuiControlProfile(ToolsTextBase_H1_C : ToolsTextBase_H1)
{
	locked = true;
	justify = "Center";
	fontSize = "14";
};
singleton GuiControlProfile(ToolsTextBase_H1_R : ToolsTextBase_H1)
{
	locked = true;
	justify = "Right";
};
//------------------------------------------------------------------------------
singleton GuiControlProfile(ToolsTextBase_H2 : ToolsTextBase_H1)
{
	fontSize = "18";
};
singleton GuiControlProfile(ToolsTextBase_H2_C : ToolsTextBase_H2)
{
	locked = true;
	justify = "Center";
};
singleton GuiControlProfile(ToolsTextBase_H2_R : ToolsTextBase_H2)
{
	locked = true;
	justify = "Right";
};
//------------------------------------------------------------------------------

//==============================================================================

//------------------------------------------------------------------------------
singleton GuiControlProfile( ToolsTextBase_ML : ToolsTextBase )
{
	autoSizeWidth = true;
	autoSizeHeight = true;
	border = false;
	modal = "1";
};
singleton GuiControlProfile( ToolsTextBase_List : ToolsTextBase )
{
	tab = "0";
	canKeyFocus = "0";
	category = "ToolsText";
	mouseOverSelected = "0";
	modal = "1";
	fillColor = "238 236 241 9";
	fillColorHL = "229 229 236 0";
	fillColorSEL = "99 101 138 0";
	borderColorHL = "231 231 231 236";
	fontSize = "17";
	fontColors[2] = "254 227 83 255";
	fontColors[3] = "0 0 2 255";
	fontColorNA = "254 227 83 255";
	fontColorSEL = "0 0 2 255";
};
//------------------------------------------------------------------------------
