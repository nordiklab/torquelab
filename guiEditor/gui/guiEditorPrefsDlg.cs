//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$GuiEditor::defaultGridSize = 8;
$GuiEditor::minGridSize = 3;
function GuiEditor::showPrefsDialog(%this)
{
	Canvas.pushDialog(GuiEditorPrefsDlg);
}

//---------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
// Buttons
//-----------------------------------------------------------------------------------------

function GuiEditorPrefsDlgOkBtn::onAction(%this)
{
	GuiEditor.snap2gridsize = GuiEditorPrefsDlgGridEdit.getValue();
	$Cfg_GuiEditor_Snapping_snap2GridSize = GuiEditorPrefsDlgGridEdit.getValue();
	GuiEd.setSnapToGrid(!$Cfg_GuiEditor_Snapping_snap2GridOn);
	//if ( GuiEditor.snap2grid )
	//GuiEditor.setSnapToGrid( GuiEditor.snap2gridsize );

	Canvas.popDialog( GuiEditorPrefsDlg );
}

function GuiEditorPrefsDlgCancelBtn::onAction(%this)
{
	Canvas.popDialog( GuiEditorPrefsDlg );
}

function GuiEditorPrefsDlgDefaultsBtn::onAction(%this)
{
	GuiEditorPrefsDlgGridSlider.setValue( $GuiEditor::defaultGridSize );
}

//-----------------------------------------------------------------------------------------
// Grid
//-----------------------------------------------------------------------------------------

function GuiEditorPrefsDlgGridEdit::onWake(%this)
{
	%this.setValue( GuiEditor.snap2gridsize );
}

function GuiEditorPrefsDlgGridEdit::onAction( %this )
{
	%value = %this.getValue();

	if ( %value < $GuiEditor::minGridSize )
	{
		%value = $GuiEditor::minGridSize;
		%this.setValue( %value );
	}

	GuiEditorPrefsDlgGridSlider.setValue( %value );
}

function GuiEditorPrefsDlgGridSlider::onWake(%this)
{
	%this.setValue( GuiEditor.snap2gridsize );
}

function GuiEditorPrefsDlgGridSlider::onAction(%this)
{
	%value = %this.value;

	if ( %value < $GuiEditor::minGridSize )
	{
		%value = $GuiEditor::minGridSize;
		%this.setValue( %value );
	}

	GuiEditorPrefsDlgGridEdit.setvalue( mCeil( %value ) );
}
