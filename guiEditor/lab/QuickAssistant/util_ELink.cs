//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$GuiEdUtil_Elink_FieldSourceList = "internalName superClass";
//==============================================================================
function Lab::initElinkUtil(%this)
{
	%id = -1;
	GEFD_ELinkFieldMenu.clear();

	foreach$(%field in $GuiEdUtil_Elink_FieldSourceList)
		GEFD_ELinkFieldMenu.add(%field,%id++);

	GEFD_ELinkFieldMenu.setSelected(0);

}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::setElinkFromField(%this)
{
	%field = GEFD_ELinkFieldMenu.getText();
	%prefix = GEFD_ELinkPrefix.getText();
	%suffix = GEFD_ELinkSuffix.getText();

	foreach(%guiCtrl in GuiEditor.getSelection())
	{
		%elink = %guiCtrl.getFieldValue(%field);

		if (%guiCtrl.getFieldValue(%field) $= "")
			continue;

		%elink = %prefix @  %guiCtrl.getFieldValue(%field) @ %suffix;

		%guiCtrl.setFieldValue("eLink",%elink);
	}
}
//------------------------------------------------------------------------------

