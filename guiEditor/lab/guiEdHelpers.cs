//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function Lab::updateMissingBitmap(%this,%direction)
{
   ELink.doCall("BitmapMissing","setBitmap",$Cfg_TLab_unavailableTexturePath);
   ELink.doCall("IconBitmapMissing","setBitmap",$Cfg_TLab_unavailableTexturePath);
}
//==============================================================================
function GuiEd::AlignCtrlToParent(%this,%direction)
{
	%selection =  GuiEditor.getSelection();

	while(isObject(%selection.getObject(%i)))
	{
		%control = %selection.getObject(%i);
		%parent = %control.parentGroup;

		if (!isObject(%parent))
		{
			info(%control.getName()," have no parent to be forced inside");
			%i++;
			continue;
		}

		switch$(%direction)
		{
			case "right": //Set max right of parent
				%control.position.x = %parent.extent.x - %control.extent.x - $GuiEditorAlignMargin;

			case "left": //Set max left of parent
				%control.position.x = $GuiEditorAlignMargin;

			case "top": //Set max left of parent
				%control.position.y = $GuiEditorAlignMargin;

			case "bottom": //Set max right of parent
				%control.position.y = %parent.extent.y - %control.extent.y -$GuiEditorAlignMargin;
		}

		%i++;
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditor::AlignCtrlToParent(%this,%direction)
{
	%selection =  GuiEditor.getSelection();

	foreach(%ctrl in %selection)
	{

		%ctrl.AlignCtrlToParent(%direction,$GuiEditorAlignMargin);

		%i++;
	}
}
//------------------------------------------------------------------------------

