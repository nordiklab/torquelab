//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$InGuiEditor = false;
$MLAAFxGuiEditorTemp = false;

//==============================================================================
// Toggle the GuiEditor( Close if open and open if closed)
function ToggleGuiEdit( %fromGame,%loadLast )
{

	if (Canvas.isFullscreen() && $Cfg_UI_Menu_UseNativeMenu)
	{
		LabMsgOK("Windowed Mode Required", "Please switch to windowed mode to access the GUI Editor.");
		return;
	}

	%guiEdOn = $InGuiEditor;
	GuiEditor.forceContent = "";

	//If called from a loaded game or WorldEditor
	if (%fromGame)
	{
		if ( EditorIsActive() && !GuiEditor.toggleIntoEditorGui )
		{
			if (EditorGui.isAwake())
			{
				GuiEditor.forceContent = EditorGui;
			}

			toggleEditor( true,true );

		}

		// Cancel the scheduled event to prevent the level from cycling after it's duration has elapsed.
		cancel($Game::Schedule);
	}

	if (!$InGuiEditor)
	{
		GuiEd.launchEditor(%loadLast);
	}
	else
	{
		GuiEd.closeEditor();
	}
}
//------------------------------------------------------------------------------
//GlobalActionMap.bindCmd( keyboard, "f10", "toggleGuiEdit(true);","" );
setEditorBinds();
//GlobalActionMap.bindCmd( keyboard, "ctrl f10", "toggleGuiEdit(true,true);","" );
//------------------------------------------------------------------------------

//==============================================================================
function Lab::toggleAutoLoadLastGui( %content )
{
	$pref::Editor::AutoLoadLastGui = !$pref::Editor::AutoLoadLastGui;
	info("Auto load last gui is set to:",$pref::Editor::AutoLoadLastGui);
}
//------------------------------------------------------------------------------

//==============================================================================
// Toggle the GuiEditor( Close if open and open if closed)
function GuiEd::SetInitialState( %this )
{
	GuiEdGui_BottomTabBook.selectPage($GuiEdGui_BottomTabPage);
}
//------------------------------------------------------------------------------

function GuiEditor::switchToWorldEditor( %this )
{
	%editingWorldEditor = false;

	if ( GuiEditorContent.getObject( 0 ) == EditorGui.getId() )
		%editingWorldEditor = true;

	ToggleGuiEdit();

	if ( !$missionRunning )
		EditorNewLevel();
	else if ( !%editingWorldEditor )
		toggleEditor( true );
}
