//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$Cfg_GuiEditor_NudgeBigStep = "12";
$Cfg_GuiEditor_drawBorderLines_fullBox = "1";
$Cfg_GuiEditor_drawGuides_fullBox = "1";
$Cfg_GuiEditor_Editor_lastPath = "G:/GameProjects/AlterVerse/LOCAL/Game/tlab/SceneLab/gui";
$Cfg_GuiEditor_Editor_previewResolution = "1440 900";
$Cfg_GuiEditor_EngineDevelopment_showEditorGuis = "1";
$Cfg_GuiEditor_EngineDevelopment_showEditorProfiles = "1";
$Cfg_GuiEditor_EngineDevelopment_toggleIntoEditor = "";
$Cfg_GuiEditor_GuiEditor_lastPath = "F:/Game Workplace/AlterVerse/GameGIT/tlab/ui/guiSystem/SideBar";
$Cfg_GuiEditor_GuiEditor_previewResolution = "1440 900";
$Cfg_GuiEditor_Help_documentationLocal = "";
$Cfg_GuiEditor_Help_documentationReference = "";
$Cfg_GuiEditor_Help_documentationURL = "";
$Cfg_GuiEditor_Library_viewType = "Categorized";
$Cfg_GuiEditor_Rendering_drawBorderLines = "1";
$Cfg_GuiEditor_Rendering_drawGuides = "1";
$Cfg_GuiEditor_Selection_fullBox = "1";
$Cfg_GuiEditor_Snapping_sensitivity = "2";
$Cfg_GuiEditor_Snapping_snap2Grid = "1";
$Cfg_GuiEditor_Snapping_snap2GridSize = "12";
$Cfg_GuiEditor_Snapping_snapToCanvas = "1";
$Cfg_GuiEditor_Snapping_snapToCenters = "0";
$Cfg_GuiEditor_Snapping_snapToControls = "0";
$Cfg_GuiEditor_Snapping_snapToEdges = "0";
$Cfg_GuiEditor_Snapping_snapToGuides = "0";

$Cfg_GuiEditor__idx_snap_edge = 0;
$Cfg_GuiEditor__idx_snap_center = 1;
$Cfg_GuiEditor__idx_snap_guides = 3;
$Cfg_GuiEditor__idx_snap_control = 4;
$Cfg_GuiEditor__idx_snap_canvas = 5;
$Cfg_GuiEditor__idx_snap_grid = 6;
$Cfg_GuiEditor__idx_draw_guides = 8;
$Cfg_GuiEditor__idx_select_fullbox = 21;
//---------------------------------------------------------------------------------------------

function GuiEditorGui::initSettings( %this )
{
	//Simply load the default config before user settings
	exec($TLab_GuiEd_ConfigDefault);
	exec("tlab/config.cfg.cs");
}

//---------------------------------------------------------------------------------------------

function GuiEditorGui::readSettings( %this )
{
	GuiEditor.snapToGuides = $Cfg_GuiEditor_Snapping_snapToGuides;
	GuiEditor.snapToControls = $Cfg_GuiEditor_Snapping_snapToControls;
	GuiEditor.snapToCanvas = $Cfg_GuiEditor_Snapping_snapToCanvas;
	GuiEditor.snapToEdges = $Cfg_GuiEditor_Snapping_snapToEdges;
	GuiEditor.snapToCenters = $Cfg_GuiEditor_Snapping_snapToCenters;
	GuiEditor.snapSensitivity = $Cfg_GuiEditor_Snapping_sensitivity;
	GuiEditor.snap2Grid = $Cfg_GuiEditor_Snapping_snap2Grid;
	//GuiEditor.snap2GridSize = $Cfg_GuiEditor_Snapping_snap2GridSize;
	GuiEditor.lastPath = $Cfg_GuiEditor_Editor_lastPath;
	GuiEditor.previewResolution = $Cfg_GuiEditor_Editor_previewResolution;
	GuiEditor.toggleIntoEditor = $Cfg_GuiEditor_EngineDevelopment_toggleIntoEditor;
	GuiEditor.showEditorProfiles = $Cfg_GuiEditor_EngineDevelopment_showEditorProfiles;
	GuiEditor.showEditorGuis = $Cfg_GuiEditor_EngineDevelopment_showEditorGuis;
	GuiEditorToolbox.currentViewType = $Cfg_GuiEditor_Library_viewType;
	GuiEditor.fullBoxSelection = $Cfg_GuiEditor_Selection_fullBox;
	GuiEditor.drawBorderLines = $Cfg_GuiEditor_Rendering_drawBorderLines;
	GuiEditor.drawGuides = $Cfg_GuiEditor_Rendering_drawGuides;
	GuiEditor.documentationURL = $Cfg_GuiEditor_Help_documentationURL;
	GuiEditor.documentationLocal = $Cfg_GuiEditor_Help_documentationLocal;
	GuiEditor.documentationReference = $Cfg_GuiEditor_Help_documentationReference;

	if ( $Cfg_GuiEditor_Snapping_snap2Grid )
		GuiEditor.setSnapToGrid( $Cfg_GuiEditor_Snapping_snap2GridSize );

}

//---------------------------------------------------------------------------------------------

function GuiEditorGui::writeSettings( %this )
{

	$Cfg_GuiEditor_Snapping_snapToGuides = GuiEditor.snapToGuides;
	$Cfg_GuiEditor_Snapping_snapToControls = GuiEditor.snapToControls;
	$Cfg_GuiEditor_Snapping_snapToCanvas = GuiEditor.snapToCanvas;
	$Cfg_GuiEditor_Snapping_snapToEdges = GuiEditor.snapToEdges;
	$Cfg_GuiEditor_Snapping_snapToCenters = GuiEditor.snapToCenters;
	$Cfg_GuiEditor_Snapping_sensitivity = GuiEditor.snapSensitivity;
	$Cfg_GuiEditor_Snapping_snap2Grid = GuiEditor.snap2Grid;
	$Cfg_GuiEditor_Snapping_snap2GridSize = GuiEditor.snap2GridSize;
	$Cfg_GuiEditor_Editor_lastPath = GuiEditor.lastPath;
	$Cfg_GuiEditor_Editor_previewResolution = GuiEditor.previewResolution;
	$Cfg_GuiEditor_EngineDevelopment_toggleIntoEditor = GuiEditor.toggleIntoEditor;
	$Cfg_GuiEditor_EngineDevelopment_showEditorProfiles = GuiEditor.showEditorProfiles;
	$Cfg_GuiEditor_EngineDevelopment_showEditorGuis = GuiEditor.showEditorGuis;
	$Cfg_GuiEditor_Library_viewType = GuiEditorToolbox.currentViewType;
	$Cfg_GuiEditor_Selection_fullBox = GuiEditor.fullBoxSelection;
	$Cfg_GuiEditor_Rendering_drawBorderLines = GuiEditor.drawBorderLines;
	$Cfg_GuiEditor_Rendering_drawGuides = GuiEditor.drawGuides;
	$Cfg_GuiEditor_Help_documentationURL = GuiEditor.documentationURL;
	$Cfg_GuiEditor_Help_documentationLocal = GuiEditor.documentationLocal;
	$Cfg_GuiEditor_Help_documentationReference = GuiEditor.documentationReference;

	Lab.saveConfig();

}
