//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Default Gui Saving method from editor
function GuiEditCanvas::save(%this, %selectedOnly, %noPrompt)
{
	// Get the control we should save.
	if(isObject(%selectedOnly))
	{
		%currentObject = %selectedOnly;
	}
	else if(%selectedOnly)
	{
		%selected = GuiEditor.getSelection();

		//EditorGui prevention - Selection name must be same as file name
		if(GuiEditorContent.getObject(0).getName() $= "EditorGui")
		{
			%fileNameMustMatch = %selected.getObject(0).getName();
		}

		if(!%selected.getCount())
			return;
		else if(%selected.getCount() > 1)
		{
			LabMsgOk("Invalid selection", "Only a single control hierarchy can be saved to a file.  Make sure you have selected only one control in the tree view.");
			return;
		}

		%currentObject = %selected.getObject(0);
	}
	else if(GuiEditorContent.getCount() > 0)
		%currentObject = GuiEditorContent.getObject(0);
	else
		return;

	if(%currentObject.isMethod("customGuiSave"))
	{
		%currentObject.customGuiSave();
		info(%currentObject.getName(),"called custom saving method:",%currentObject.getName()@"::customGuiSave(%this);");
		LabMsgOk(%currentObject.getName() SPC "custom save called",%currentObject.getName() SPC "called custom saving method:\c3" SPC %currentObject.getName()@"::customGuiSave(%this);");
		return;
	}

	// Store the current guide set on the control.
	if(%currentObject.preSaveFunction !$= "")
		eval(%currentObject.preSaveFunction);

	GuiEditor.writeGuides(%currentObject);
	%currentObject.canSaveDynamicFields = true; // Make sure the guides get saved out.

	// Construct a base filename.

	if(%currentObject.getName() !$= "")
		%name =  %currentObject.getName() @ ".gui";
	else
		%name = "Untitled.gui";

	// Construct a path.

	if(%selectedOnly && %currentObject != GuiEditorContent.getObject(0)
	      && %currentObject.getFileName() $= GuiEditorContent.getObject(0).getFileName())
	{
		// Selected child control that hasn't been yet saved to its own file.
		%currentFile = GuiEditor.LastPath @ "/" @ %name;
		%currentFile = makeRelativePath(%currentFile, getMainDotCsDir());
	}
	else
	{
		%currentFile = %currentObject.getFileName();

		if(%currentFile $= "")
		{
			// No file name set on control.  Force a prompt.
			%noPrompt = false;

			if(GuiEditor.LastPath !$= "")
			{
				%currentFile = GuiEditor.LastPath @ "/" @ %name;
				%currentFile = makeRelativePath(%currentFile, getMainDotCsDir());
			}
			else
				%currentFile = expandFileName(%name);
		}
		else
			%currentFile = expandFileName(%currentFile);
	}

	// Get the filename.

	if(!%noPrompt)
	{
		%filename = GuiBuilder::getSaveName(%currentFile);

		if(%filename $= "")
			return;

		if(%fileNameMustMatch !$= "" && %fileNameMustMatch !$= fileBase(%filename))
		{
			return;
		}
	}
	else
		%filename = %currentFile;

	//Call the Gui saving method with validated informations
	Lab.saveGuiToFile(%currentObject,%filename);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::saveGuiToFile(%this, %currentObject, %filename)
{
	if(!isObject(%currentObject))
		return;

	if(%currentObject.isMethod("customGuiSave"))
	{
		%currentObject.customGuiSave();
		info(%currentObject.getName(),"called custom saving method:",%currentObject.getName()@"::customGuiSave(%this);");
		return;
	}

	//If no file supplied, use the one from the Gui
	if(%filename $= "")
		%filename = %currentObject.getFileName();

	if(isWriteableFileName(%filename))
	{
		if(%currentObject.isMethod("onPreEditorSave"))
		{
			%proceed = %currentObject.onPreEditorSave();

			if (%proceed $= "ABORT" )
			{
				warnlog(%currentObject.getName(),"Saving aborted from onPreEditorSave");
				return;
			}
		}

		//If the saving Gui is set to can't save, remember it and make it savable
		//This is mostly for Editor Guis as it prevent them to be saved inside EditorGui.gui
		if(!%currentObject.canSave)
			%setCanSaveFalse = true;

		%currentObject.canSave = 1;
		//%currentObject.canSaveDynamicFields = 0;

		//Make a backup Gui file, if failed, the backup file will be empty.
		%backupFile = Lab.backupFileHome(%filename);

		//------------------------------------------------------------------------
		// Extract any existent TorqueScript before writing out to disk
		// This is a depreciated method but must be supported, Script should be in
		// neightbor script file and not embed in .gui files
		//------------------------------------------------------------------------
		%fileObject = new FileObject();
		%fileObject.openForRead(%filename);
		%skipLines = true;
		%beforeObject = true;
		// %var++ does not post-increment %var, in torquescript, it pre-increments it,
		// because ++%var is illegal.
		%lines = -1;
		%beforeLines = -1;
		%skipLines = false;

		while(!%fileObject.isEOF())
		{
			%line = %fileObject.readLine();

			if(%line $= "//--- OBJECT WRITE BEGIN ---")
				%skipLines = true;
			else if(%line $= "//--- OBJECT WRITE END ---")
			{
				%skipLines = false;
				%beforeObject = false;
			}
			else if(%skipLines == false)
			{
				if(%beforeObject)
					%beforeNewFileLines[ %beforeLines++ ] = %line;
				else
					%newFileLines[ %lines++ ] = %line;
			}
		}

		%fileObject.close();
		%fileObject.delete();

		//------------------------------------------------------------------------
		// Start writing the new file and prepend/append previous found script
		//------------------------------------------------------------------------
		%fo = new FileObject();
		%fo.openForWrite(%filename);

		// Write out the captured TorqueScript that was before the object before the object
		for(%i = 0; %i <= %beforeLines; %i++)
			%fo.writeLine(%beforeNewFileLines[ %i ]);

		%fo.writeLine("//--- OBJECT WRITE BEGIN ---");
		%fo.writeObject(%currentObject, "%guiContent = ");
		%fo.writeLine("//--- OBJECT WRITE END ---");

		// Write out captured TorqueScript below Gui object
		for(%i = 0; %i <= %lines; %i++)
			%fo.writeLine(%newFileLines[ %i ]);

		%fo.close();
		%fo.delete();

		//------------------------------------------------------------------------
		// Gui file have been written, proceed with post saving script
		//------------------------------------------------------------------------

		//------------------------------------------------------------------------
		// Corrupted Gui saving prevention (Could be removed with new backup system)
		//System to notify when a saved gui file seem to have failed (Size barely enough for base text)
		if(fileSize(%filename) < 100)
		{
			%currentObject.dump();
			warnLog("Gui file is too small after save:", fileSize(%filename),"File", %filename);

			if (!isFile(%backupFile))
			{
				warnLog("There's no backup file to restore, no action wil be taken");
			}
			else
			{
				//Change the corrupted file to .txt extension and restore backup
				%tmpFile = strReplace(%filename,".gui",".txt");
				pathCopy(%filename,%tmpFile,false);//Copy written gui to .txt
				pathCopy(%backupFile,%filename,false);//Copy backup to filename with noOverwrite to false
				warnLog("Initial Gui file restored from:",%backupFile,"Opening the failed attempt file as .txt:",%tmpFile);
				openFile(%tmpFile);//Call the opening of .txt file on the computer so it can be examined
			}
		}

		//Make sure the Gui Object filename is relative
		%currentObject.setFileName(makeRelativePath(%filename, getMainDotCsDir()));
		GuiEditorStatusBar.print("Saved file '" @ %currentObject.getFileName() @ "'");

		//Restore cant save if was set
		if(%setCanSaveFalse)
			%currentObject.canSave = "0";

		if(%currentObject.isMethod("onPostEditorSave"))
			%currentObject.onPostEditorSave();
	}
	else
		LabMsgOk("Error writing to file", "There was an error writing to file '" @ %currentFile @ "'. The file may be read-only.");

}
//---------------------------------------------------------------------------------------------

//==============================================================================
// Shorten Gui saving function for manual saving needs
function guiSave(%gui,%file)
{
	Lab.saveGuiToFile(%gui,%file);
}
//------------------------------------------------------------------------------
