//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//=============================================================================================
//    Event Handlers.
//=============================================================================================

function GuiEditCanvas::onAdd( %this )
{
	// %this.setWindowTitle("Torque Gui Editor");

	//%this.onCreateMenu();
}

//---------------------------------------------------------------------------------------------

function GuiEditCanvas::onRemove( %this )
{
	if( isObject( GuiEditorGui.menuGroup ) )
		GuiEditorGui.delete();

	// cleanup
	//%this.onDestroyMenu();
}

//---------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------

function GuiEditCanvas::onWindowClose(%this)
{
	%this.quit();
}
//---------------------------------------------------------------------------------------------

//=============================================================================================
//    Menu Commands.
//=============================================================================================

//---------------------------------------------------------------------------------------------

function GuiEditCanvas::create( %this )
{
	GuiEditorNewGuiDialog.init( "NewGui", "GuiControl" );
	Canvas.pushDialog( GuiEditorNewGuiDialog );
}

//---------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------

function GuiEditCanvas::openInTorsion( %this )
{
	if ( !GuiEditorContent.getCount() )
		return;

	%guiObject = GuiEditorContent.getObject( 0 );
	EditorOpenDeclarationInTorsion( %guiObject );
}

//---------------------------------------------------------------------------------------------

function GuiEditCanvas::open( %this )
{

	%openFileName = GuiBuilder::getOpenName();

	if ( %openFileName $= "" )
		return;

	// Make sure the file is valid.
	if ((!isFile(%openFileName)) && (!isFile(%openFileName @ ".dso")))
		return;

	%this.load( %openFileName );
}

//---------------------------------------------------------------------------------------------

