//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function GuiEd::launchEditor( %this,%loadLast )
{

	Lab.onGuiEditorOpen();

	if ( isObject(GuiEditor.forceContent))
		%initialContent = GuiEditor.forceContent;
	else if (!isObject(%initialContent))
		%initialContent = Canvas.getContent();

	if (($pref::Editor::AutoLoadLastGui || %loadLast) && isObject(GuiEd.lastGuiLoaded))
		%initialGui = GuiEd.lastGuiLoaded;
	else
		%initialGui = %initialContent;

	GuiEditor.initialGui = %initialGui;
	GuiEditor.initialContent =%initialContent;

	GuiEditContent(%initialGui);

	//Temp fix to disable MLAA when in GUI editor
	if ( isObject(MLAAFx) && MLAAFx.isEnabled==true )
	{
		MLAAFx.isEnabled = false;
		$MLAAFxGuiEditorTemp = true;
	}

	//Make sure the TorqueLab loading progress is stopped
	EditorLoadingGui.endInit();

}
//------------------------------------------------------------------------------

//==============================================================================
//    Activation.
//==============================================================================
//==============================================================================
package GuiEditor_BlockDialogs
{
	function GuiCanvas::pushDialog() {}
	function GuiCanvas::popDialog() {}
};
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditContent( %content )
{

	if ( !isObject( GuiEditCanvas ) )
		new GuiControl( GuiEditCanvas, EditorGuiGroup );

	GuiEdMap.push();
	$InGuiEditor = true;
	GuiEditor.openForEditing( %content );

}
//------------------------------------------------------------------------------

//==============================================================================
//    GuiEditorGui.
//==============================================================================
//==============================================================================
function GuiEditorGui::onWake( %this )
{

	Lab.onGuiEditorWake();
	GuiEdToggle.setStateOn( 1 );

	//Ruler experiments
	GuiEdRulerLeft.fitIntoParents( "height" );
	GuiEdRulerTop.fitIntoParents( "width" );
	GuiEdRulerLeft.AlignCtrlToParent( "left" );
	GuiEdRulerTop.AlignCtrlToParent( "top" );

	GuiEdRulerLeft.extent.y = GuiEditorScroll.extent.y;
	GuiEdRulerLeft.position.y = GuiEditorScroll.position.y;
	GuiEdRulerTop.extent = GuiEditorScroll.extent.x SPC 22;
	GuiEdRulerTop.position = GuiEditorScroll.position.x SPC 0;
	GuiEditorRegion.pushToBack(GuiEditor);

	if ( !isObject( %this->SelectControlsDlg ) )
	{
		GuiEdDialogs.add( GuiEditorSelectDlg );
		GuiEditorSelectDlg.setVisible( false );
	}

	// Attach our menus.
	if ( isObject( %this.menuGroup ) )
		for( %i = 0; %i < %this.menuGroup.getCount(); %i ++ )
			%this.menuGroup.getObject( %i ).attachToMenuBar();

	// Read settings.
	%this.initSettings();
	%this.readSettings();

	// Initialize toolbox.

	if ( !GuiEditorToolbox.isInitialized )
		GuiEditorToolbox.initialize();

	if (isObject(GuiEdMenu))
	{
		// Set up initial menu toggle states.
		
		GuiEdMenu_Snap.checkItem( $Cfg_GuiEditor__idx_snap_edge, GuiEditor.snapToEdges );
		GuiEdMenu_Snap.checkItem( $Cfg_GuiEditor__idx_snap_center, GuiEditor.snapToCenters );
		GuiEdMenu_Snap.checkItem( $Cfg_GuiEditor__idx_snap_guides, GuiEditor.snapToGuides );
		GuiEdMenu_Snap.checkItem( $Cfg_GuiEditor__idx_snap_control, GuiEditor.snapToControls );
		GuiEdMenu_Snap.checkItem( $Cfg_GuiEditor__idx_snap_canvas, GuiEditor.snapToCanvas );
		GuiEdMenu_Snap.checkItem( $Cfg_GuiEditor__idx_snap_grid, GuiEditor.snap2Grid );
		GuiEdMenu_Snap.checkItem( $Cfg_GuiEditor__idx_draw_guides, GuiEditor.drawGuides );
		GuiEdMenu_Edit.checkItem( $Cfg_GuiEditor__idx_select_fullbox, GuiEditor.fullBoxSelection );
	}

	// Sync toolbar buttons.
	GuiEditorSnapCheckBox.setStateOn( GuiEditor.snap2Grid );
	GuiEditorEdgeSnapping_btn.setStateOn( GuiEditor.snapToEdges );
	GuiEditorCenterSnapping_btn.setStateOn( GuiEditor.snapToCenters );

	//
}
//------------------------------------------------------------------------------
