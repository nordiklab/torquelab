//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function GuiEditCanvas::append(%this)
{
	// Get filename.
	%openFileName = GuiBuilder::getOpenName();

	if(%openFileName $= "" || (!isFile(%openFileName) && !isFile(%openFileName @ ".dso")))
		return;

	// Exec file.
	%oldRedefineBehavior = $Con::redefineBehavior;
	$Con::redefineBehavior = "renameNew";
	exec(%openFileName);
	$Con::redefineBehavior = %oldRedefineBehavior;

	// Find guiContent.
	if(!isObject(%guiContent))
	{
		LabMsgOk("Error loading GUI file", "The GUI content controls could not be found.  This function can only be used with files saved by the GUI editor.");
		return;
	}

	if(!GuiEditorContent.getCount())
		GuiEditor.openForEditing(%guiContent);
	else
	{
		GuiEditor.getCurrentAddSet().add(%guiContent);
		GuiEditor.readGuides(%guiContent);
		GuiEditor.onAddNewCtrl(%guiContent);
		GuiEditor.onHierarchyChanged();
	}

	GuiEditorStatusBar.print("Appended controls from '" @ %openFileName @ "'");
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditCanvas::revert(%this)
{
	if(!GuiEditorContent.getCount())
		return;

	%gui = GuiEditorContent.getObject(0);
	%filename = %gui.getFileName();

	if(%filename $= "")
		return;

	if(LabMsgOkCancel("Revert Gui", "Really revert the current Gui?  This cannot be undone.", "OkCancel", "Question") == $MROk)
		%this.load(%filename);
}
//------------------------------------------------------------------------------
