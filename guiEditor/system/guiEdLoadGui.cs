//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function GuiEditor::openForEditing( %this, %content )
{
	if ( !isObject(%content))
		return;

	if ( getGui() !$= "GuiEditorGui")
		Canvas.setContent( GuiEditorGui );

	GuiEditor.snap2Grid = $Cfg_GuiEditor_Snapping_snap2Grid;

	while( GuiEditorContent.getCount() )
		GuiEditDumpGroup.add( GuiEditorContent.getObject( 0 ) ); // get rid of anything being edited

	// Clear the current guide set and add the guides
	// from the control.
	//Mud-H Quick hack to prevent a start script error
	%this.clearGuides();
	%this.readGuides( %content );
	// Enumerate GUIs and put them into the content list.
	GuiEditorContentList.init();
	GuiEditorScroll.scrollToTop();
	activatePackage( GuiEditor_BlockDialogs );
	GuiEditorContent.add( %content );
	deactivatePackage( GuiEditor_BlockDialogs );
	GuiEditorContentList.sort();

	if (%content.getName() $= "")
		%name = "(unnamed) - " @ %content;
	else
		%name = %content.getName() @ " - " @ %content;

	GuiEditorContentList.setText(%name);
	%this.setContentControl(%content);
	// Initialize the preview resolution list and select the current
	// preview resolution.
	GuiEditorResList.init();
	%res = %this.previewResolution;

	if ( %res $= "" )
		%res = "1024 768";

	GuiEditorResList.selectFormat( %res );
	// Initialize the treeview and expand the first level.
	GuiEditorTreeView.init();
	GuiEditorTreeView.open( %content );
	GuiEditorTreeView.expandItem( 1 );

	// Initialize profiles tree.

	if ( !GuiEditorProfilesTree.isInitialized )
	{
		GuiEditorProfilesTree.init();
		GuiEditorProfilesTree.isInitialized = true;
	}

	// Create profile change manager if we haven't already.
	if ( !isObject( GuiEditorProfileChangeManager ) )
		new SimGroup( GuiEditorProfileChangeManager );

	// clear the undo manager if we're switching controls.
	if ( %this.lastContent != %content )
		GuiEditor.getUndoManager().clearAll();

	GuiEditor.setFirstResponder();
	%this.updateUndoMenu();
	%this.lastContent = %content;
	GuiEd.lastGuiLoaded = %content;
	%content.visible = 1;
	Lab.onGuiEditorOpen();
}
function GuiEditCanvas::load( %this, %filename )
{
	%newRedefineBehavior = "replaceExisting";

	if ( isDefined( "$GuiEditor::loadRedefineBehavior" ) )
	{
		// This trick allows to choose different redefineBehaviors when loading
		// GUIs.  This is useful, for example, when loading GUIs that would lead to
		// problems when loading with their correct names because script behavior
		// would immediately attach.
		//
		// This allows to also edit the GUI editor's own GUI inside itself.
		%newRedefineBehavior = $GuiEditor::loadRedefineBehavior;
	}

	// Allow stomping objects while exec'ing the GUI file as we want to
	// pull the file's objects even if we have another version of the GUI
	// already loaded.
	%oldRedefineBehavior = $Con::redefineBehavior;
	$Con::redefineBehavior = %newRedefineBehavior;
	// Load up the gui.
	exec( %fileName );
	$Con::redefineBehavior = %oldRedefineBehavior;

	// The GUI file should have contained a GUIControl which should now be in the instant
	// group. And, it should be the only thing in the group.
	if ( !isObject( %guiContent ) )
	{
		LabMsgOk( getEngineName(),
		          "You have loaded a Gui file that was created before this version.  It has been loaded but you must open it manually from the content list dropdown");
		return 0;
	}

	GuiEditor.openForEditing( %guiContent );
	GuiEditorStatusBar.print( "Loaded '" @ %filename @ "'" );
}
