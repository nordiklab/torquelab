//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function GuiEd::setSnapToGrid(%this,%state)
{

	$Cfg_GuiEditor_Snapping_snap2GridOn = %state;
	$Cfg_GuiEditor_Snapping_snap2Grid = $Cfg_GuiEditor_Snapping_snap2GridOn * $Cfg_GuiEditor_Snapping_snap2GridSize;
	GuiEditorSnapCheckBox.setValue( $Cfg_GuiEditor_Snapping_snap2GridOn );
	GuiEditor.setSnapToGrid( $Cfg_GuiEditor_Snapping_snap2Grid );
	Lab.checkMenuCodeItem("Gui","snap2Grid",$Cfg_GuiEditor_Snapping_snap2GridOn);
	info("Snap to grid set to:",$Cfg_GuiEditor_Snapping_snap2Grid,"isOn",$Cfg_GuiEditor_Snapping_snap2GridOn,"BaseSIze",$Cfg_GuiEditor_Snapping_snap2GridSize);
}
function GuiEd::toggleSnapToGrid(%this)
{
	GuiEd.setSnapToGrid(!$Cfg_GuiEditor_Snapping_snap2GridOn);
}

//=============================================================================================
//    Toolbar.
//=============================================================================================

//---------------------------------------------------------------------------------------------

function GuiEditorSnapCheckBox::onWake(%this)
{
	%snap = GuiEditor.snap2grid * GuiEditor.snap2gridsize;
	%this.setValue( %snap );
	GuiEditor.setSnapToGrid( %snap );
}

//---------------------------------------------------------------------------------------------

function GuiEditorSnapCheckBox::onAction(%this)
{
	%snap = GuiEditor.snap2gridsize * %this.getValue();
	GuiEditor.snap2grid = %this.getValue();
	GuiEditor.setSnapToGrid(%snap);
}

//---------------------------------------------------------------------------------------------

function GuiEditor::setPreviewResolution( %this, %width, %height )
{
	//Mud-H Quick hack to prevent a start script error
	if (isObject(GuiEditorRegion))
		GuiEditorRegion.resize( 0, 0, %width, %height );

	if (isObject(GuiEditorContent))
		GuiEditorContent.getObject( 0 ).resize( 0, 0, %width, %height );

	GuiEditor.previewResolution = %width SPC %height;
}

//---------------------------------------------------------------------------------------------

function GuiEditor::toggleEdgeSnap( %this )
{
	%this.snapToEdges = !%this.snapToEdges;
	Lab.checkMenuCodeItem("Gui","snapToEdges",%this.snapToEdges);
	//GuiEditCanvas.menuBar->SnapMenu.checkItem( $Cfg_GuiEditor__idx_snap_edge, %this.snapToEdges );
	GuiEditorEdgeSnapping_btn.setStateOn( %this.snapToEdges );
}

//---------------------------------------------------------------------------------------------

function GuiEditor::toggleCenterSnap( %this )
{
	%this.snapToCenters = !%this.snapToCenters;
	Lab.checkMenuCodeItem("Gui","snapToCenters",%this.snapToCenters);
	//GuiEditCanvas.menuBar->SnapMenu.checkItem( $Cfg_GuiEditor__idx_snap_center, %this.snapToCenters );
	GuiEditorCenterSnapping_btn.setStateOn( %this.snapToCenters );
}

//---------------------------------------------------------------------------------------------

function GuiEditor::toggleFullBoxSelection( %this )
{
	%this.fullBoxSelection = !%this.fullBoxSelection;
	Lab.checkMenuCodeItem("Gui","fullBoxSelection",%this.fullBoxSelection);
	//GuiEditCanvas.menuBar->EditMenu.checkItem( $Cfg_GuiEditor__idx_select_fullbox, %this.fullBoxSelection );
}

//---------------------------------------------------------------------------------------------

function GuiEditor::toggleDrawGuides( %this )
{
	%this.drawGuides= !%this.drawGuides;
	Lab.checkMenuCodeItem("Gui","drawGuides",%this.drawGuides);
	//GuiEditCanvas.menuBar->SnapMenu.checkItem( $Cfg_GuiEditor__idx_draw_guides, %this.drawGuides );
}

//---------------------------------------------------------------------------------------------

function GuiEditor::toggleGuideSnap( %this )
{
	%this.snapToGuides = !%this.snapToGuides;
	Lab.checkMenuCodeItem("Gui","snapToGuides",%this.snapToGuides);
	//GuiEditCanvas.menuBar->SnapMenu.checkItem( $Cfg_GuiEditor__idx_snap_guides, %this.snapToGuides );
}

//---------------------------------------------------------------------------------------------

function GuiEditor::toggleControlSnap( %this )
{
	%this.snapToControls = !%this.snapToControls;
	Lab.checkMenuCodeItem("Gui","snapToGuides",%this.snapToControls);
	//GuiEditCanvas.menuBar->SnapMenu.checkItem( $Cfg_GuiEditor__idx_snap_control, %this.snapToControls );
}

//---------------------------------------------------------------------------------------------

function GuiEditor::toggleCanvasSnap( %this )
{
	%this.snapToCanvas = !%this.snapToCanvas;
	Lab.checkMenuCodeItem("Gui","snapToCanvas",%this.snapToCanvas);
	//GuiEditCanvas.menuBar->SnapMenu.checkItem( $Cfg_GuiEditor__idx_snap_canvas, %this.snapToCanvas );
}

//---------------------------------------------------------------------------------------------

function GuiEditor::toggleGridSnap( %this )
{
	GuiEd.toggleSnapToGrid();
	return;
	%this.snap2Grid = !%this.snap2Grid;

	if ( !%this.snap2Grid )
		%this.setSnapToGrid( 0 );
	else
		%this.setSnapToGrid( %this.snap2GridSize );

	Lab.checkMenuCodeItem("Gui","snap2Grid",%this.snap2Grid);
	//GuiEditCanvas.menuBar->SnapMenu.checkItem( $Cfg_GuiEditor__idx_snap_grid, %this.snap2Grid );
	GuiEditorSnapCheckBox.setStateOn( %this.snap2Grid );
}

//---------------------------------------------------------------------------------------------
//=============================================================================================
//    Resolution List.
//=============================================================================================

//---------------------------------------------------------------------------------------------

function GuiEditorResList::init( %this )
{
	%this.clear();
	// Non-widescreen formats.
	%this.add( "640x480 (VGA, 4:3)", 640 );
	%this.add( "800x600 (SVGA, 4:3)", 800 );
	%this.add( "1024x768 (XGA, 4:3)", 1024 );
	%this.add( "1280x1024 (SXGA, 4:3)", 1280 );
	%this.add( "1600x1200 (UXGA, 4:3)", 1600 );
	// Widescreen formats.
	%this.add( "1280x720 (WXGA, 16:9)", 720 );
	%this.add( "1600x900 (16:9)", 900 );
	%this.add( "1920x1080 (16:9)", 1080 );
	%this.add( "1440x900 (WXGA+, 16:10)", 900 );
	%this.add( "1680x1050 (WSXGA+, 16:10)", 1050 );
	%this.add( "1920x1200 (WUXGA, 16:10)", 1200 );
}

//---------------------------------------------------------------------------------------------

function GuiEditorResList::selectFormat( %this, %format )
{
	%width = getWord( %format, 0 );
	%height = getWord( %format, 1 );

	switch( %height )
	{
		case 720:
			%this.setSelected( 720 );

		case 900:
			%this.setSelected( 900 );

		case 1050:
			%this.setSelected( 1050 );

		case 1080:
			%this.setSelected( 1080 );

		default:
			switch( %width )
			{
				case 640:
					%this.setSelected( 640 );

				case 800:
					%this.setSelected( 800 );

				case 1024:
					%this.setSelected( 1024 );

				case 1280:
					%this.setSelected( 1280 );

				case 1600:
					%this.setSelected( 1600 );

				default:
					%this.setSelected( 1200 );
			}
	}
}

//---------------------------------------------------------------------------------------------

function GuiEditorResList::onSelect( %this, %id )
{
	switch( %id )
	{
		case 640:
			GuiEditor.setPreviewResolution( 640, 480 );

		case 800:
			GuiEditor.setPreviewResolution( 800, 600 );

		case 1024:
			GuiEditor.setPreviewResolution( 1024, 768 );

		case 1280:
			GuiEditor.setPreviewResolution( 1280, 1024 );

		case 1600:
			GuiEditor.setPreviewResolution( 1600, 1200 );

		case 720:
			GuiEditor.setPreviewResolution( 1280, 720 );

		case 900:
			GuiEditor.setPreviewResolution( 1440, 900 );

		case 1050:
			GuiEditor.setPreviewResolution( 1680, 1050 );

		case 1080:
			GuiEditor.setPreviewResolution( 1920, 1080 );

		case 1200:
			GuiEditor.setPreviewResolution( 1920, 1200 );
	}
}
