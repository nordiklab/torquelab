//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function GuiEd::closeEditor( %this )
{
	Lab.onGuiEditorClose();

	Lab.lastGuiEditSource = GuiEditor.lastContent;
	GuiEditCanvas.quit();

}

//------------------------------------------------------------------------------

//    Deactivation.
function GuiEditCanvas::close( %this )
{
}

//---------------------------------------------------------------------------------------------

function GuiEditCanvas::quit( %this )
{
	%this.close();
	GuiGroup.add(GuiEditorGui);
	// we must not delete a window while in its event handler, or we foul the event dispatch mechanism
	%this.schedule(10, delete);
	$InGuiEditor = false;

	if (!isObject(GuiEditor.initialContent))
	{
		warnLog("Closing the GuiEditor and there's no valid content to load:",GuiEditor.initialContent,"Using default GuiGameMenu");
		GuiEditor.initialContent = $Cfg_TLab_defaultGui;
	}

	Canvas.setContent(GuiEditor.initialContent);

	//Temp fix to disable MLAA when in GUI editor
	if ( isObject(MLAAFx) && $MLAAFxGuiEditorTemp==true )
	{
		MLAAFx.isEnabled = true;
		$MLAAFxGuiEditorTemp = false;
	}
}

//------------------------------------------------------------------------------

function GuiEditorGui::onSleep( %this)
{
	Lab.onGuiEditorSleep();
	// If we are editing a control, store its guide state.
	%content = GuiEditor.getContentControl();

	if ( isObject( %content ) )
		GuiEditor.writeGuides( %content );

	// Store our preferences.
	GuiEditorGui.writeSettings();
}

//------------------------------------------------------------------------------

function addRuler(%gui)
{
	%ruler = new GuiEditorRuler()
	{
		position = "1 0";
		extent = "111 52";
		minExtent = "8 2";
		horizSizing = "right";
		vertSizing = "bottom";
		profile = "ToolsDefaultProfile";
		visible = "1";
		active = "1";
		tooltipProfile = "ToolsToolTipProfile";
		hovertime = "1000";
		isContainer = "1";
		superClass = "pluginBarTrash";
		canSave = "1";
		canSaveDynamicFields = "0";
	};

	%gui.add(%ruler);
}
//------------------------------------------------------------------------------
