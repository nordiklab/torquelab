//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//---------------------------------------------------------------------------------------------
/// Called before onSleep when the canvas content is changed

//=============================================================================================
//    Activation.
//============================================================================================
function GuiEditor::onGainFirstResponder(%this)
{
	%this.enableMenuItems(true);
	// JCF: don't just turn them all on!
	// Undo/Redo is only enabled if those actions exist.
	%this.updateUndoMenu();
}

//---------------------------------------------------------------------------------------------

function GuiEditor::onLoseFirstResponder(%this)
{
	GuiEditor.enableMenuItems(false);

}
//==============================================================================
//==============================================================================
//    Methods.
//==============================================================================

//==============================================================================
function GuiEditor::enableMenuItems(%this, %val)
{
	if ( !isObject( GuiEdMenu ) )
		return;

	%menu = GuiEdMenu.findMenu("Edit");
	if ( !isObject( %menu ) )
	{
	   warn("Edit menu seem missing");
		return;
	}
	%menu.enableItem( 3, %val ); // cut
	%menu.enableItem( 4, %val ); // copy
	%menu.enableItem( 5, %val ); // paste
	//%menu.enableItem( 7, %val ); // selectall
	//%menu.enableItem( 8, %val ); // deselectall
	%menu.enableItem( 9, %val ); // selectparents
	%menu.enableItem( 10, %val ); // selectchildren
	%menu.enableItem( 11, %val ); // addselectparents
	%menu.enableItem( 12, %val ); // addselectchildren
	%menu.enableItem( 15, %val ); // lock
	%menu.enableItem( 16, %val ); // hide
	%menu.enableItem( 18, %val ); // group
	%menu.enableItem( 19, %val ); // ungroup
	GuiEditCanvas.menuBar->LayoutMenu.enableAllItems( %val );
	GuiEditCanvas.menuBar->MoveMenu.enableAllItems( %val );
}

//---------------------------------------------------------------------------------------------

function GuiEditor::isFilteredClass( %this, %className )
{
	// Filter out all the internal GuiInspector classes.
	if ( startsWith( %className, "GuiInspector" ) && %className !$= "GuiInspector" )
		return true;

	// Filter out GuiEditor classes.

	if ( startsWith( %className, "GuiEditor" ) )
		return true;

	// Filter out specific classes.

	switch$( %className )
	{
		case "GuiCanvas":
			return true;

		case "GuiAviBitmapCtrl":
			return true; // For now.  Probably removed altogether.

		case "GuiArrayCtrl":
			return true; // Abstract base class really.

		case "GuiScintillaTextCtrl":
			return true; // Internal class.

		case "GuiNoMouseCtrl":
			return true; // Too odd.

		case "GuiEditCtrl":
			return true;

		case "GuiBackgroundCtrl":
			return true; // Just plain useless.

		case "GuiTSCtrl":
			return true; // Abstract base class.

		case "GuiTickCtrl":
			return true; // Abstract base class.

		case "GuiWindowCollapseCtrl":
			return true; // Legacy.
	}

	return false;
}

//---------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------

//=============================================================================================
//    GuiEditorGui.
//=============================================================================================
