//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function GuiEditor::createControl( %this, %className )
{
	%ctrl = eval( "return new " @ %className @ "();" );

	if ( !isObject( %ctrl ) )
		return;

	// Add the control.
	%this.addNewCtrl( %ctrl );
}
//------------------------------------------------------------------------------
