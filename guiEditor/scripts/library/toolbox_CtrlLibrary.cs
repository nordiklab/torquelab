//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

// Code for the toolbox tab of the Gui Editor sidebar.

//---------------------------------------------------------------------------------------------

function GuiEditorToolbox::initialize( %this )
{
	// Set up contents.
	%viewType = %this.currentViewType;

	if ( %viewType $= "" )
		%viewType = "Categorized";

	%this.currentViewType = "";
	%this.setViewType( %viewType );
	%this.isInitialized = true;
}

//---------------------------------------------------------------------------------------------

function GuiEditorToolbox::getViewType( %this )
{
	return %this.currentViewType;
}

//---------------------------------------------------------------------------------------------

function GuiEditorToolbox::setViewType( %this, %viewType )
{
	if ( %this.currentViewType $= %viewType
	                              || !%this.isMethod( "setViewType" @ %viewType ) )
		return;

	%this.clear();
	eval( %this @ ".setViewType" @ %viewType @ "();" );
}

//---------------------------------------------------------------------------------------------

function GuiEditorToolbox::setViewTypeAlphabetical( %this )
{
	%controls = enumerateConsoleClassesByCategory( "Gui" );
	%classes = new ArrayObject();

	// Collect relevant classes.

	foreach$( %className in %controls )
	{
		if ( GuiEditor.isFilteredClass( %className )
		      || !isMemberOfClass( %className, "GuiControl" ) )
			continue;

		%classes.push_back( %className );
	}

	// Sort classes alphabetically.
	%classes.sortk( true );
	// Add toolbox buttons.
	%numClasses = %classes.count();

	for( %i = 0; %i < %numClasses; %i ++ )
	{
		%className = %classes.getKey( %i );
		%ctrl = new GuiIconButtonCtrl()
		{
			profile = "ToolsButtonBase";
			extent = "128 18";
			text = %className;
			iconBitmap = EditorIconRegistry::findIconByClassName( %className );
			buttonMargin = "2 2";
			iconLocation = "left";
			textLocation = "left";
			textMargin = "24";
			AutoSize = true;
			command = "GuiEditor.createControl( " @ %className @ " );";
			useMouseEvents = true;
			className = "GuiEditorToolboxButton";
			tooltip = %className NL "\n" @ getDescriptionOfClass( %className );
			tooltipProfile = "ToolsToolTipProfile";
		};
		%this.add( %ctrl );
	}

	%classes.delete();
	%this.currentViewType = "Alphabetical";
}

//---------------------------------------------------------------------------------------------

function GuiEditorToolbox::setViewTypeCategorized( %this )
{
	// Create rollouts for each class category we have and
	// record the classes in each category in a temporary array
	// on the rollout so we can later sort the class names before
	// creating the actual controls in the toolbox.
	%controls = enumerateConsoleClassesByCategory( "Gui" );

	foreach$( %className in %controls )
	{
		if ( GuiEditor.isFilteredClass( %className )
		      || !isMemberOfClass( %className, "GuiControl" ) )
			continue;

		// Get the class's next category under Gui.
		%category = getWord( getCategoryOfClass( %className ), 1 );

		if ( %category $= "" )
			continue;

		// Find or create the rollout for the category.
		%rollout = %this.getOrCreateRolloutForCategory( %category );

		// Insert the item.

		if ( !%rollout.classes )
			%rollout.classes = new ArrayObject();

		%rollout.classes.push_back( %className );
	}

	// Go through the rollouts, sort the class names, and
	// create the toolbox controls.

	foreach( %rollout in %this )
	{
		if ( !%rollout.isMemberOfClass( "GuiRolloutCtrl" ) )
			continue;

		// Get the array with the class names and sort it.
		// Sort in descending order to counter reversal of order
		// when we later add the controls to the stack.
		%classes = %rollout.classes;
		%classes.sortk( true );
		// Add a control for each of the classes to the
		// rollout's stack control.
		%stack = %rollout-->array;
		%numClasses = %classes.count();

		for( %n = 0; %n < %numClasses; %n ++ )
		{
			%className = %classes.getKey( %n );
			%ctrl = new GuiIconButtonCtrl()
			{
				profile = "ToolsButtonBase";
				extent = "128 18";
				text = %className;
				iconBitmap = EditorIconRegistry::findIconByClassName( %className );
				buttonMargin = "2 2";
				iconLocation = "left";
				textLocation = "left";
				textMargin = "24";
				AutoSize = true;
				command = "GuiEditor.createControl( " @ %className @ " );";
				useMouseEvents = true;
				className = "GuiEditorToolboxButton";
				tooltip = %className NL "\n" @ getDescriptionOfClass( %className );
				tooltipProfile = "ToolsToolTipProfile";
			};
			%stack.add( %ctrl );
		}

		// Delete the temporary array.
		%rollout.classes = "";
		%classes.delete();
	}

	%this.currentViewType = "Categorized";
}

//---------------------------------------------------------------------------------------------

function GuiEditorToolbox::getOrCreateRolloutForCategory( %this, %category )
{
	// Try to find an existing rollout.
	%ctrl = %this.getRolloutForCategory( %category );

	if ( %ctrl != 0 )
		return %ctrl;

	// None there.  Create a new one.
	%ctrl = new GuiRolloutCtrl()
	{
		Margin = "0 0 0 0";
		DefaultHeight = "40";
		Expanded = "1";
		ClickCollapse = "1";
		HideHeader = "0";
		isContainer = "1";
		Profile = "ToolsRolloutBaseA";
		HorizSizing = "right";
		VertSizing = "bottom";
		position = "0 0";
		Extent = "421 114";
		MinExtent = "8 2";
		canSave = "1";
		Visible = "1";
		tooltipprofile = "ToolsToolTipProfile";
		hovertime = "1000";
		canSaveDynamicFields = "0";
		autoCollapseSiblings = true;
		caption = %category;
		class = "GuiEditorToolboxRolloutCtrl";
		new GuiDynamicCtrlArrayControl()
		{
			isContainer = "1";
			Profile = "ToolsDefaultProfile";
			HorizSizing = "right";
			VertSizing = "bottom";
			position = "0 0";
			Extent = "421 64";
			MinExtent = "64 64";
			canSave = "1";
			Visible = "1";
			tooltipprofile = "ToolsToolTipProfile";
			hovertime = "1000";
			canSaveDynamicFields = "0";
			padding = "6 2 4 0";
			colSpacing = "1";
			rowSpacing = "9";
			dynamicSize = true;
			autoCellSize = true;
			internalName = "array";
		};
	};
	%this.add( %ctrl );
	%ctrl.collapse();
	// Sort the rollouts by their caption.
	%this.sort( "_GuiEditorToolboxSortRollouts" );
	return %ctrl;
}

//---------------------------------------------------------------------------------------------

function GuiEditorToolbox::getRolloutForCategory( %this, %category )
{
	foreach( %obj in %this )
	{
		if ( !%obj.isMemberOfClass( "GuiRolloutCtrl" ) )
			continue;

		if ( stricmp( %obj.caption, %category ) == 0 )
			return %obj;
	}

	return 0;
}

//---------------------------------------------------------------------------------------------
