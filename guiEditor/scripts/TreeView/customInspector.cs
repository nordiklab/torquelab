//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function GuiEditorInspectFields::getDynamicGroup(%this)
{
	foreach(%ctrl in %this)
		if (%ctrl.getClassName() $= "GuiInspectorDynamicGroup")
			return %ctrl;

}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditor::addlink(%this, %value)
{
	%numObjects = GuiEditorInspectFields.getNumInspectObjects();

	for( %i = 0; %i < %numObjects; %i ++ )
	{
		%object = GuiEditorInspectFields.getInspectObject( %i );
		%object.setFieldValue("eLinkA",%value);
	}

	GuiEditorInspectFields.getDynamicGroup().inspectGroup();
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditor::addElinkButton(%this,%source)
{
	%dynamicGroup = GuiEditorInspectFields.getDynamicGroup();

	if (!isObject(%dynamicGroup))
		return;

	delObj(GuiEdELinkButton);
	%but =  new LabButtonCtrl(GuiEdELinkButton)
	{
		buttonMargin = "4 4";
		iconBitmap = "tlab/art/icons/set01/generic/12/common/sign_plus.png";
		iconLocation = "Left";
		sizeIconToButton = "0";
		makeIconSquare = "0";
		textLocation = "Left";
		textMargin = "4";
		autoSize = "0";
		text = "eLink";
		groupNum = "-1";
		buttonType = "PushButton";
		command = "GuiEditor.addlink(LinkName);";
		useMouseEvents = "0";
		position = "0 0";
		extent = "53 19";
		minExtent = "8 2";
		horizSizing = "right";
		vertSizing = "center";
		profile = "ToolsButtonAccent";
		visible = "1";
		active = "1";
		tooltipProfile = "GuiToolTipProfile";
		hovertime = "1000";
		isContainer = "0";
		canSave = "1";
		canSaveDynamicFields = "0";
	};

	%stack = %dynamicGroup.getObject(0);
	%stack.getObject(%stack.getCount()-1).add(%but);

}
//------------------------------------------------------------------------------
//==============================================================================
/*

function GuiEditor::customApply(%this, %field,%value)
{

   %numObjects = GuiEditorInspectFields.getNumInspectObjects();
   for( %i = 0; %i < %numObjects; %i ++ )
	{
		%object = GuiEditorInspectFields.getInspectObject( %i );
		%object.setFieldValue(%field,%value);
	}
	GuiEditorInspectFields.apply();

}
//==============================================================================
function GuiControl::onDefineFieldTypes(%this)
{
	%this.setFieldType("eLinkAA", "TypeString");
	%this.setFieldValue("eLink", "LinkName");
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiPopupMenuCtrl::onDefineFieldTypes(%this)
{
	%this.setFieldType("eLinkAA", "TypeString");
}
//------------------------------------------------------------------------------
*/
