//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function GuiEditor::onAddNewCtrl(%this, %ctrl)
{
	%set = new SimSet();
	%set.add(%ctrl);
	%act = UndoActionAddObject::create(%set, %this.getTrash(), GuiEditorTreeView);
	%set.delete();
	%act.addToManager(%this.getUndoManager());
	%this.updateUndoMenu();
	//GuiEditorInspectFields.update(0);
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditor::onAddNewCtrlSet(%this, %selection)
{
	%act = UndoActionAddObject::create(%selection, %this.getTrash(), GuiEditorTreeView);
	%act.addToManager(%this.getUndoManager());
	%this.updateUndoMenu();
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditor::onTrashSelection(%this, %selection)
{
	%act = UndoActionDeleteObject::create(%selection, %this.getTrash(), GuiEditorTreeView);
	%act.addToManager(%this.getUndoManager());
	%this.updateUndoMenu();
}
//------------------------------------------------------------------------------
