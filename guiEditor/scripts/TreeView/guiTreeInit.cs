//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Code for the main Gui Editor tree view that shows the hierarchy of the
// current GUI being edited.
//==============================================================================
//==============================================================================
function GuiEditorTreeView::init(%this)
{
	if ( !isObject( GuiEditorTreeView.contextMenu ) )
		%this.contextMenu = new PopupMenu()
	{
		superClass = "MenuBuilder";
		isPopup = true;
		item[ %id = 0 ] = "Rename" TAB "" TAB "GuiEditorTreeView.showItemRenameCtrl( GuiEditorTreeView.findItemByObjectId( %this.object ) );";
		item[ %id++ ] = "Delete" TAB "" TAB "GuiEditor.deleteControl( %this.object );";
		item[ %id++ ] = "-";
		item[ %id++ ] = "Locked" TAB "" TAB "%this.object.setLocked( !%this.object.locked ); GuiEditorTreeView.update();";
		item[ %id++ ] = "Hidden" TAB "" TAB "%this.object.setVisible( !%this.object.isVisible() ); GuiEditorTreeView.update();";
		item[ %id++ ] = "-";
		item[ %id++ ] = "Add New Controls Here" TAB "" TAB "GuiEditor.setCurrentAddSet( %this.object );";
		item[ %id++ ] = "Add Child Controls to Selection" TAB "" TAB "GuiEditor.selectAllControlsInSet( %this.object, false );";
		item[ %id++ ] = "Remove Child Controls from Selection" TAB "" TAB "GuiEditor.selectAllControlsInSet( %this.object, true );";
		item[ %id++ ] = "-";
		item[ %id++ ] = "Save control as" TAB "" TAB "GuiEditCanvas.save( true,false,true );";
		item[ %id++ ] = "-";
		item[ %id++ ] = "Save to templates" TAB "" TAB "WTG.addGuiTemplate(%this.object);";
		object = -1;
	};

	if ( !isObject( %this.contextMenuMultiSel ) )
		%this.contextMenuMultiSel = new PopupMenu()
	{
		superClass = "MenuBuilder";
		isPopup = true;
		item[ 0 ] = "Delete" TAB "" TAB "GuiEditor.deleteSelection();";
	};
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::update( %this )
{
	%this.updateSourceGui();
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::updateSourceGui( %this )
{
	%obj = GuiEditorContent.getObject( 0 );

	if ( !isObject( %obj ) )
		GuiEditorTreeView.clear();
	else
	{
		// Open inspector tree.
		GuiEditorTreeView.open( %obj );
		// Sync selection with GuiEditor.
		GuiEditorTreeView.clearSelection();
		%selection = GuiEditor.getSelection();
		%count = %selection.getCount();

		for( %i = 0; %i < %count; %i ++ )
			GuiEditorTreeView.addSelection( %selection.getObject( %i ) );
	}
}
//------------------------------------------------------------------------------

//==============================================================================
//=============================================================================================
//    Event Handlers.
//=============================================================================================
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::onRightMouseDown( %this, %item, %pts, %obj )
{

	if ( %this.getSelectedItemsCount() > 1 )
	{
		%popup = %this.contextMenuMultiSel;
		%popup.showPopup( Canvas );
	}
	else if ( %obj )
	{
		%popup = GuiEditorTreeView.contextMenu;
		%popup.checkItem( 3, %obj.locked );
		%popup.checkItem( 4, !%obj.isVisible() );
		%popup.enableItem( 6, %obj.isContainer );
		%popup.enableItem( 7, %obj.getCount() > 0 );
		%popup.enableItem( 8, %obj.getCount() > 0 );
		%popup.object = %obj;
		%popup.showPopup( Canvas );
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::onKeyDown( %this, %mod, %key, %obj )
{

}
//------------------------------------------------------------------------------
