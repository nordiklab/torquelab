//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Code for the main Gui Editor tree view that shows the hierarchy of the
// current GUI being edited.
//==============================================================================

//==============================================================================
/// Defines the icons to be used in the tree view control.
/// Provide the paths to each icon minus the file extension.
/// Seperate them with ':'.
/// The order of the icons must correspond to the bit array defined
/// in the GuiTreeViewCtrl.h.
function GuiEditorTreeView::onDefineIcons(%this)
{
	%icons = ":" @       // Default1
	         ":" @       // SimGroup1
	         ":" @       // SimGroup2
	         ":" @       // SimGroup3
	         ":" @       // SimGroup4
	         "tlab/art/icons/set01/default/common/hidden:" @
	         "tlab/art/icons/set01/default/lockedHandle";
	GuiEditorTreeView.buildIconTable( %icons );
}
//------------------------------------------------------------------------------
//=============================================================================================
//    ActiveGui TreeView calbacks.
//=============================================================================================
//------------------------------------------------------------------------------

//==============================================================================
function GuiEditorTreeView::onAddSelection(%this,%ctrl)
{
	GuiEditor.dontSyncTreeViewSelection = true;
	GuiEditor.addSelection( %ctrl );
	GuiEditor.dontSyncTreeViewSelection = false;
	GuiEditorTreeView.clearFirstResponder();
	GuiEditor.setFirstResponder();
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::onRemoveSelection( %this, %ctrl )
{
	GuiEditor.dontSyncTreeViewSelection = true;
	GuiEditor.removeSelection( %ctrl );
	GuiEditor.dontSyncTreeViewSelection = false;

}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::onDeleteSelection(%this)
{
	//deleteSelection fail to call onRemove for deleted object
	// so we call it from script on all object to be deleted
	%list = GuiEditorTreeView.getSelectedObjectList();

	foreach$(%gui in %list)
		%gui.onRemove();

	GuiEditor.deleteSelection();
	//GuiEditor.clearSelection();
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::onSelect( %this, %obj )
{
	if ( isObject( %obj ) )
	{
		GuiEditor.dontSyncTreeViewSelection = true;
		GuiEditor.select( %obj );
		GuiEditor.dontSyncTreeViewSelection = false;
		GuiEditorInspectFields.inspect( %obj );
	}

	GuiEditorTreeView.clearFirstResponder();
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::isValidDragTarget( %this, %id, %obj )
{
	return ( %obj.isContainer || %obj.getCount() > 0 );
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::onBeginReparenting( %this )
{
	if ( isObject( %this.reparentUndoAction ) )
		%this.reparentUndoAction.delete();

	%action = UndoActionReparentObjects::create( %this );
	%this.reparentUndoAction = %action;
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::onReparent( %this, %obj, %oldParent, %newParent )
{
	%this.reparentUndoAction.add( %obj, %oldParent, %newParent );
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorTreeView::onEndReparenting( %this )
{
	%action = %this.reparentUndoAction;
	%this.reparentUndoAction = "";

	if ( %action.numObjects > 0 )
	{
		if ( %action.numObjects == 1 )
			%action.actionName = "Reparent Control";
		else
			%action.actionName = "Reparent Controls";

		%action.addToManager( GuiEditor.getUndoManager() );
		GuiEditor.updateUndoMenu();
	}
	else
		%action.delete();
}
//------------------------------------------------------------------------------
