//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

// Core for the main Gui Editor inspector that shows the properties of
// the currently selected control.
//---------------------------------------------------------------------------------------------
//==============================================================================
// Inspector Outside calls regrouped here
//==============================================================================
//==============================================================================
function GuiEditor::refreshInspect( %this )
{
	GuiEditorInspectFields.refresh();
}
function GuiEditor::onInspectorPostFieldModification( %this,%fieldname )
{
	GuiEditorInspectFields.onInspectorPostFieldModification( %fieldname );
}
function GuiEditor::onInspectorPreFieldModification( %this,%fieldname )
{
	GuiEditorInspectFields.onInspectorPreFieldModification( %fieldname );
}
function GuiEditor::addInspect( %this, %inspectTarget )
{
	GuiEditorInspectFields.refresh();
}
function GuiEditor::refreshInspect( %this )
{
	GuiEditorInspectFields.refresh();
}
//---------------------------------------------------------------------------------------------

function GuiEditorInspectFields::addInspect( %this, %inspectTarget )
{
	Parent::addInspect(%this,%inspectTarget);
	GuiEditor.addElinkButton(addInspect);
}
function GuiEditorInspectFields::inspect( %this, %inspectTarget,%upddate )
{
	Parent::inspect(%this,%inspectTarget);

	if (isObject(%inspectTarget) && !%upddate)
		GuiEditor.addElinkButton(inspect);
}

//---------------------------------------------------------------------------------------------

function GuiEditorInspectFields::update( %this, %inspectTarget )
{
	%this.inspect( %inspectTarget );
	//GuiEditor.addElinkButton(update);
}

//=============================================================================================
//    Event Handlers.
//=============================================================================================

//---------------------------------------------------------------------------------------------

function GuiEditorInspectFields::onInspectorFieldModified( %this, %object, %fieldName, %arrayIndex, %oldValue, %newValue )
{
	// The instant group will try to add our
	// UndoAction if we don't disable it.
	pushInstantGroup();
	%nameOrClass = %object.getName();

	if ( %nameOrClass $= "" )
		%nameOrClass = %object.getClassname();

	%action = new InspectorFieldUndoAction()
	{
		actionName = %nameOrClass @ "." @ %fieldName @ " Change";
		objectId = %object.getId();
		fieldName = %fieldName;
		fieldValue = %oldValue;
		arrayIndex = %arrayIndex;
		inspectorGui = %this;
	};
	// Restore the instant group.
	popInstantGroup();
	%action.addToManager( GuiEditor.getUndoManager() );
	GuiEditor.updateUndoMenu();
}

//---------------------------------------------------------------------------------------------

function GuiEditorInspectFields::onInspectorPreFieldModification( %this, %fieldName, %arrayIndex )
{
	pushInstantGroup();
	%undoManager = GuiEditor.getUndoManager();
	%numObjects = %this.getNumInspectObjects();

	if ( %numObjects > 1 )
		%action = %undoManager.pushCompound( "Multiple Field Edit" );

	for( %i = 0; %i < %numObjects; %i ++ )
	{
		%object = %this.getInspectObject( %i );
		%nameOrClass = %object.getName();

		if ( %nameOrClass $= "" )
			%nameOrClass = %object.getClassname();

		%undo = new InspectorFieldUndoAction()
		{
			actionName = %nameOrClass @ "." @ %fieldName @ " Change";
			objectId = %object.getId();
			fieldName = %fieldName;
			fieldValue = %object.getFieldValue( %fieldName, %arrayIndex );
			arrayIndex = %arrayIndex;
			inspectorGui = %this;
		};

		if ( %numObjects > 1 )
			%undo.addToManager( %undoManager );
		else
		{
			%action = %undo;
			break;
		}
	}

	%this.currentFieldEditAction = %action;
	popInstantGroup();
}

//---------------------------------------------------------------------------------------------

function GuiEditorInspectFields::onInspectorPostFieldModification( %this )
{
	if ( %this.currentFieldEditAction.isMemberOfClass( "CompoundUndoAction" ) )
	{
		// Finish multiple field edit.
		GuiEditor.getUndoManager().popCompound();
	}
	else
	{
		// Queue single field undo.
		%this.currentFieldEditAction.addToManager( GuiEditor.getUndoManager() );
	}

	%this.currentFieldEditAction = "";
	GuiEditor.updateUndoMenu();
}

//---------------------------------------------------------------------------------------------

function GuiEditorInspectFields::onInspectorDiscardFieldModification( %this )
{
	%this.currentFieldEditAction.undo();

	if ( %this.currentFieldEditAction.isMemberOfClass( "CompoundUndoAction" ) )
	{
		// Multiple field editor.  Pop and discard.
		GuiEditor.getUndoManager().popCompound( true );
	}
	else
	{
		// Single field edit.  Just kill undo action.
		%this.currentFieldEditAction.delete();
	}

	%this.currentFieldEditAction = "";
}

//---------------------------------------------------------------------------------------------

function GuiEditorInspectFields::onFieldSelected( %this, %fieldName, %fieldTypeStr, %fieldDoc )
{
	GuiEditorFieldInfo.setText( "<font:ArialBold:14>" @ %fieldName @ "<font:ArialItalic:14> (" @ %fieldTypeStr @ ") " NL "<font:Arial:14>" @ %fieldDoc );
}

//---------------------------------------------------------------------------------------------

function GuiEditorInspectFields::onBeginCompoundEdit( %this )
{
	GuiEditor.getUndoManager().pushCompound( "Multiple Field Edits" );
}

//---------------------------------------------------------------------------------------------

function GuiEditorInspectFields::onEndCompoundEdit( %this )
{
	GuiEditor.getUndoManager().popCompound();
}
//==============================================================================
function GuiEditor::onControlInspectPreApply(%this, %object)
{
	%set = new SimSet();
	%set.add(%object);

	if ($SkipThis)
	{
		warnLog("Skipping onControlInspectPreApply", %object);
		return;
	}

	%elink = GuiEditorInspectFields.getInspectObject(0).eLink;

	if (%elink $= "linkName")
		GuiEditorInspectFields.setObjectField("elink","");

	%this.onPreEdit(%set);
	%this.pendingGenericUndoAction.actionName = "Change Properties";
	%set.delete();
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditor::onControlInspectPostApply(%this, %object)
{
	%set = new SimSet();
	%set.add(%object);
	%this.onPostEdit(%set);
	%set.delete();
	GuiEditorTreeView.update();
}
//------------------------------------------------------------------------------
