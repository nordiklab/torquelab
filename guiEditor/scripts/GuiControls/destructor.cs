//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//---------------------------------------------------------------------------------------------

function GuiEditor::deleteControl( %this, %ctrl )
{
	// Unselect.
	GuiEditor.removeSelection( %ctrl );
	// Record undo.
	%set = new SimSet()
	{
		parentGroup = RootGroup;
	};
	%set.add( %ctrl );
	%action = UndoActionDeleteObject::create( %set, %this.getTrash(), GuiEditorTreeView );
	%action.addToManager( %this.getUndoManager() );
	%this.updateUndoMenu();
	GuiEditorTreeView.update();
	%set.delete();
	// Remove.
	%this.getTrash().add( %ctrl );
}

//---------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------
