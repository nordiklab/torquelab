//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//---------------------------------------------------------------------------------------------

/// Group all GuiControls in the currenct selection set under a new GuiControl.
function GuiEditor::groupSelected( %this )
{
	%selection = %this.getSelection();

	if ( %selection.getCount() < 2 )
		return;

	// Create action.
	%action = GuiEditorGroupAction::create( %selection, GuiEditor.getContentControl() );
	%action.groupControls();
	// Update editor tree.
	%this.clearSelection();
	%this.addSelection( %action.group[ 0 ].groupObject );
	GuiEditorTreeView.update();
	// Update undo state.
	%action.addtoManager( %this.getUndoManager() );
	%this.updateUndoMenu();
}

//---------------------------------------------------------------------------------------------

/// Take all direct GuiControl instances in the selection set and reparent their child controls
/// to each of the group's parents.  The GuiControl group objects are deleted.
function GuiEditor::ungroupSelected( %this )
{
	%action = GuiEditorUngroupAction::create( %this.getSelection() );
	%action.ungroupControls();
	// Update editor tree.
	%this.clearSelection();
	GuiEditorTreeView.update();
	// Update undo state.
	%action.addToManager( %this.getUndoManager() );
	%this.updateUndoMenu();
}
//------------------------------------------------------------------------------
