//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function GuiEditor::toggleLockSelection( %this )
{
	%this.toggleFlagInAllSelectedObjects( "locked" );
}

//---------------------------------------------------------------------------------------------

function GuiEditor::toggleHideSelection( %this )
{
	%this.toggleFlagInAllSelectedObjects( "hidden" );
}

//------------------------------------------------------------------------------

function GuiEditor::selectAllControlsInSet( %this, %set, %deselect )
{
	if ( !isObject( %set ) )
		return;

	foreach( %obj in %set )
	{
		if ( !%obj.isMemberOfClass( "GuiControl" ) )
			continue;

		if ( !%deselect )
			%this.addSelection( %obj );
		else
			%this.removeSelection( %obj );
	}
}

//---------------------------------------------------------------------------------------------

function GuiEditor::toggleFlagInAllSelectedObjects( %this, %flagFieldName )
{
	// Use the inspector's code here to record undo information
	// for the field edits.

	GuiEditorInspectFields.onInspectorPreFieldModification( %flagFieldName );

	%selected = %this.getSelection();

	foreach( %object in %selected )
		%object.setFieldValue( %flagFieldName, !%object.getFieldValue( %flagFieldName ) );

	GuiEditorInspectFields.onInspectorPostFieldModification();
	GuiEditorInspectFields.refreshInspect();

}
