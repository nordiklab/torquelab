//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function GuiEditor::move(%this,%x,%y)
{
	// GuiEditor.moveSelection(%x SPC %y);

	GuiEditor.moveSelection(%x,%y);
}
function GuiEditor::onMouseModeChange(%this,%mode)
{
}

function guiEdNudgeUp(%val)
{
	if (%val)
		GuiEditor.move(0, -1);
}
function guiEdNudgeDown(%val)
{
	if (%val)
		GuiEditor.move( 0, 1);
}
function guiEdNudgeLeft(%val)
{
	if (%val)
		GuiEditor.move( -1, 0);
}
function guiEdNudgeRight(%val)
{
	if (%val)
		GuiEditor.move( 1, 0);
}

//==============================================================================
$SkipPreNudge = false;
function GuiEditor::onPreSelectionNudged(%this, %selection)
{
	if ($SkipPreNudge)
	{
		warnLog("Skipping onPreSelectionNudged", %selection);
		return;
	}

	%this.onPreEdit(%selection);
	%this.pendingGenericUndoAction.actionName = "Nudge";
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditor::onPostSelectionNudged(%this, %selection)
{
	%this.onPostEdit(%selection);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::forceCtrlInsideParent(%this)
{
	%selection =  GuiEditor.getSelection();

	while(isObject(%selection.getObject(%i)))
	{
		%control = %selection.getObject(%i);
		%parent = %control.parentGroup;

		if (!isObject(%parent))
		{
			info(%control.getName()," have no parent to be forced inside");
			%i++;
			continue;
		}

		%realExtentX = %control.position.x + %control.extent.x;

		if (%realExtentX > %parent.extent.x)
			%control.extent.x = %parent.extent.x - %control.position.x;

		%realExtentY = %control.position.y + %control.extent.y;

		if (%realExtentY > %parent.extent.y)
			%control.extent.y = %parent.extent.y - %control.position.y;

		%i++;
	}
}
//------------------------------------------------------------------------------
$GuiEditorAlignMargin = 0;

//==============================================================================
function Lab::setSelectedControlAsReference(%this)
{
	%selection =  GuiEditor.getSelection();
	%control = %selection.getObject(0);
	$GuiEditor_ReferenceControl = %control;
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::setControlReferenceField(%this,%field)
{
	%obj = $GuiEditor_ReferenceControl;

	if (!isObject(%obj))
	{
		warnLog("No reference object setted");
		return;
	}

	%field = trim(%field);

	if (%field $= "name")
		%value = ""; //Referenced name always empty since they must be unique
	else
		%value = %obj.getFieldValue(%field);

	%selection =  GuiEditor.getSelection();
	%i = 0;

	while(isObject(%selection.getObject(%i)))
	{
		%control = %selection.getObject(%i);
		%control.setFieldValue(%field,%value);
		%i++;
	}
}
//------------------------------------------------------------------------------
