//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//=============================================================================================
//    Event Handlers.
//=============================================================================================

//---------------------------------------------------------------------------------------------

function GuiEditor::onDelete(%this)
{
	GuiEditorTreeView.inspect();
	// clear out the gui inspector.
	GuiEditorInspectFields.inspect(0);
}

//---------------------------------------------------------------------------------------------

function GuiEditor::onSelectionMoved( %this, %ctrl )
{
	GuiEditorInspectFields.inspect( %ctrl );
}

//---------------------------------------------------------------------------------------------

function GuiEditor::onSelectionCtrlResized( %this, %ctrl )
{
	GuiEditorInspectFields.inspect( %ctrl );
}

//---------------------------------------------------------------------------------------------

function GuiEditor::onSelect(%this, %ctrl)
{
	if ( !%this.dontSyncTreeViewSelection )
	{
		GuiEditorTreeView.clearSelection();
		GuiEditorTreeView.addSelection( %ctrl );
	}

	GuiEditorInspectFields.inspect( %ctrl );
	GuiEditorSelectionStatus.setText( "1 Control Selected" );
	//GuiEd.templateTargetChanged(%ctrl);
}

//---------------------------------------------------------------------------------------------

function GuiEditor::onAddSelected( %this, %ctrl )
{
	if ( !%this.dontSyncTreeViewSelection )
	{
		GuiEditorTreeView.addSelection( %ctrl );
		GuiEditorTreeView.scrollVisibleByObjectId( %ctrl );
	}

	GuiEditorSelectionStatus.setText( %this.getNumSelected() @ " Controls Selected" );
	// Add to inspection set.
	GuiEditorInspectFields.addInspect( %ctrl );
	//GuiEd.templateTargetChanged(%ctrl);
}

//---------------------------------------------------------------------------------------------

function GuiEditor::onRemoveSelected( %this, %ctrl )
{
	if ( !%this.dontSyncTreeViewSelection )
		GuiEditorTreeView.removeSelection( %ctrl );

	GuiEditorSelectionStatus.setText( %this.getNumSelected() @ " Controls Selected" );
	// Remove from inspection set.
	GuiEditorInspectFields.removeInspect( %ctrl );
}

//---------------------------------------------------------------------------------------------

function GuiEditor::onClearSelected( %this )
{
	if ( !GuiEditor.dontSyncTreeViewSelection )
		GuiEditorTreeView.clearSelection();

	GuiEditorInspectFields.inspect( 0 );
	GuiEditorSelectionStatus.setText( "" );
}

//---------------------------------------------------------------------------------------------

function GuiEditor::onControlDragged( %this, %payload, %position )
{
	// Make sure we have the right kind of D&D.
	if ( !%payload.parentGroup.isInNamespaceHierarchy( "GuiDragAndDropControlType_GuiControl" ) )
		return;

	// use the position under the mouse cursor, not the payload position.
	%position = VectorSub( %position, GuiEditorContent.getGlobalPosition() );
	%x = getWord( %position, 0 );
	%y = getWord( %position, 1 );
	%target = GuiEditor.getContentControl().findHitControl( %x, %y );

	// Make sure the target is a valid parent for our payload.

	while(    ( !%target.isContainer || !%target.acceptsAsChild( %payload ) )
	          && %target != GuiEditor.getContentControl() )
		%target = %target.getParent();

	if ( %target != %this.getCurrentAddSet() )
		%this.setCurrentAddSet( %target );
}

//---------------------------------------------------------------------------------------------

function GuiEditor::onControlDropped(%this, %payload, %position)
{
	// Make sure we have the right kind of D&D.
	if ( !%payload.parentGroup.isInNamespaceHierarchy( "GuiDragAndDropControlType_GuiControl" ) )
		return;

	%pos = %payload.getGlobalPosition();
	%x = getWord(%pos, 0);
	%y = getWord(%pos, 1);
	%this.addNewCtrl(%payload);
	%payload.setPositionGlobal(%x, %y);
	%this.setFirstResponder();
}

//---------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------

function GuiEditor::onHierarchyChanged( %this )
{
   GuiEditorInspectFields.inspect();
	//GuiEditorTreeView.inspect();
}

//---------------------------------------------------------------------------------------------

function GuiEditor::onMouseModeChange( %this )
{
	GuiEditorStatusBar.setText( GuiEditorStatusBar.getMouseModeHelp() );
}
