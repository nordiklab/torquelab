//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
//=============================================================================================
//    Sidebar.
//=============================================================================================

//---------------------------------------------------------------------------------------------

function GuiEditorTabBook::onWake( %this )
{
	if ( !isObject( "GuiEditorTabBookLibraryPopup" ) )
		new PopupMenu( GuiEditorTabBookLibraryPopup )
	{
		superClass = "MenuBuilder";
		isPopup = true;
		item[ 0 ] = "Alphabetical View" TAB "" TAB "GuiEditorToolbox.setViewType( \"Alphabetical\" );";
		item[ 1 ] = "Categorized View" TAB "" TAB "GuiEditorToolbox.setViewType( \"Categorized\" );";
	};
}

//---------------------------------------------------------------------------------------------

function GuiEditorTabBook::onTabSelected( %this, %text, %index )
{
	%sidebar = GuiEditorSidebar;

	if (!isObject(%sidebar))
		return;

	%name = %this.getObject( %index ).getInternalName();

	switch$( %name )
	{
		case "guiPage":
			%sidebar-->button1.setVisible( false );
			%sidebar-->button2.setVisible( false );
			%sidebar-->button3.setVisible( true );
			%sidebar-->button4.setVisible( true );
			%sidebar-->button4.setBitmap( "tlab/Art/buttons/set01/default/delete" );
			%sidebar-->button4.command = "GuiEditor.deleteSelection();";
			%sidebar-->button4.tooltip = "Delete Selected Control(s)";
			%sidebar-->button3.setBitmap( "tlab/Art/buttons/set01/default/visible" );
			%sidebar-->button3.command = "GuiEditor.toggleHideSelection();";
			%sidebar-->button3.tooltip = "Hide Selected Control(s)";

		case "profilesPage":
			%sidebar-->button1.setVisible( true );
			%sidebar-->button2.setVisible( true );
			%sidebar-->button3.setVisible( true );
			%sidebar-->button4.setVisible( true );
			%sidebar-->button4.setBitmap( "tlab/Art/buttons/set01/default/delete" );
			%sidebar-->button4.command = "GuiEditor.showDeleteProfileDialog( GuiEditorProfilesTree.getSelectedProfile() );";
			%sidebar-->button4.tooltip = "Delete Selected Profile";
			%sidebar-->button3.setBitmap( "tlab/Art/buttons/set01/default/new" );
			%sidebar-->button3.command = "GuiEditor.createNewProfile( \"Unnamed\" );";
			%sidebar-->button3.tooltip = "Create New Profile with Default Values";
			%sidebar-->button2.setBitmap( "tlab/Art/buttons/set01/default/copy-btn" );
			%sidebar-->button2.command = "GuiEditor.createNewProfile( GuiEditorProfilesTree.getSelectedProfile().getName(), GuiEditorProfilesTree.getSelectedProfile() );";
			%sidebar-->button2.tooltip = "Create New Profile by Copying the Selected Profile";
			%sidebar-->button1.setBitmap( "tlab/Art/buttons/set01/default/reset-icon" );
			%sidebar-->button1.command = "GuiEditor.revertProfile( GuiEditorProfilesTree.getSelectedProfile() );";
			%sidebar-->button1.tooltip = "Revert Changes to the Selected Profile";

		case "toolboxPage":
			//TODO
			%sidebar-->button1.setVisible( false );
			%sidebar-->button2.setVisible( false );
			%sidebar-->button3.setVisible( false );
			%sidebar-->button4.setVisible( false );
	}
}

//---------------------------------------------------------------------------------------------

function GuiEditorTabBook::onTabRightClick( %this, %text, %index )
{
	%name = %this.getObject( %index ).getInternalName();

	switch$( %name )
	{
		case "toolboxPage":
			// Open toolbox popup.
			%popup = GuiEditorTabBookLibraryPopup;
			%currentViewType = GuiEditorToolbox.getViewType();

			switch$( %currentViewType )
			{
				case "Alphabetical":
					%popup.checkRadioItem( 0, 1, 0 );

				case "Categorized":
					%popup.checkRadioItem( 0, 1, 1 );
			}

			%popup.showPopup( Canvas );
	}
}
//------------------------------------------------------------------------------
