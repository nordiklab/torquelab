//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function GuiEditorContentList::init( %this )
{
	%this.clear();
	%this.scanGroup( GuiGroup );
}
//------------------------------------------------------------------------------
//==============================================================================
function GuiEditorContentList::scanGroup( %this, %group )
{
	foreach( %obj in %group )
	{
		if ( %obj.isMemberOfClass( "GuiControl" ) )
		{
			if (%obj.getClassName() $= "GuiCanvas")
			{
				%this.scanGroup( %obj );
			}
			else
			{
				if (%obj.getName() $= "" && !$pref::GuiEditor::ShowUnnamed)
					%skip = true;
				else if (%obj.getName() $= "")
					%name = "(unnamed) - " @ %obj;
				else
					%name = %obj.getName() @ " - " @ %obj;

				%skip = false;

				foreach$( %guiEntry in $GuiEditor::GuiFilterList )
					if ( %obj.getName() $= %guiEntry )
					{
						%skip = true;
						break;
					}

				if ( !%skip )
					%this.add( %name, %obj );
			}
		}
		else if ( %obj.isMemberOfClass( "SimGroup" )&&  ( %obj.internalName !$= "EditorGuiGroup"  || GuiEditor.showEditorGuis)  )     // except if explicitly requested.
		{
			// Scan nested SimGroups for GuiControls.
			%this.scanGroup( %obj );
		}
		else if ( %obj.isMemberOfClass( "SimGroup" ) && %obj.internalName $= "EditorGuiGroup" && $pref::GuiEditor::ShowEditorsGui )     // except if explicitly requested.
		{
			// Scan nested SimGroups for GuiControls.
			%this.scanGroup( %obj );
		}
	}
}
//------------------------------------------------------------------------------

//=============================================================================================
//    Event Handlers.
//=============================================================================================

//==============================================================================
function GuiEditorContentList::onSelect( %this, %ctrl )
{
	GuiEditor.openForEditing( %ctrl );
}
//------------------------------------------------------------------------------
