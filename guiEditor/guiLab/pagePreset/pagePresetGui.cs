//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$GLab_ProfileBook_ActivePageId = 0;

//==============================================================================
function GLab::initPresetPage(%this)
{
	GLab.initColorManager();
	GLab.updateFontTypeList();
}
//------------------------------------------------------------------------------

