//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
delObj(GuiColor_Group);
new SimGroup(GuiColor_Group)
{
	canSave = "1";
	canSaveDynamicFields = "1";

	new SimGroup(GuiColorFont_Group)
	{
		internalName = "colorFont";
		canSave = "1";
		canSaveDynamicFields = "1";

		new ScriptObject()
		{
			internalName = "BaseA";
			canSave = "1";
			canSaveDynamicFields = "1";
			fontColors0 = "254 254 254 255";
			fontColors1 = "255 160 0 255";
			fontColors2 = "3 206 254 255";
			fontColors3 = "254 3 62 255";
			fontColors4 = "238 255 0 255";
			fontColors5 = "3 254 148 255";
			fontColors6 = "3 21 254 255";
			fontColors7 = "254 236 3 255";
			fontColors8 = "254 3 43 255";
			fontColors9 = "3 48 248 255";
		};
		new ScriptObject()
		{
			internalName = "BaseB";
			canSave = "1";
			canSaveDynamicFields = "1";
			fontColors0 = "254 185 5 255";
			fontColors1 = "206 196 41 255";
			fontColors2 = "32 203 14 255";
			fontColors3 = "0 30 255 255";
			fontColors4 = "143 95 14 255";
			fontColors5 = "3 254 148 255";
			fontColors6 = "3 21 254 255";
			fontColors7 = "254 236 3 255";
			fontColors8 = "254 3 43 255";
			fontColors9 = "3 48 248 255";
		};
		new ScriptObject()
		{
			internalName = "AltA";
			canSave = "1";
			canSaveDynamicFields = "1";
			fontColors0 = "12 14 34 255";
			fontColors1 = "189 180 180 255";
			fontColors2 = "0 255 50 255";
			fontColors3 = "254 3 62 255";
			fontColors4 = "101 85 12 255";
			fontColors5 = "37 78 60 255";
			fontColors6 = "3 21 254 255";
			fontColors7 = "150 87 15 255";
			fontColors8 = "254 3 43 255";
			fontColors9 = "3 48 248 255";
		};
		new ScriptObject()
		{
			internalName = "AltB";
			canSave = "1";
			canSaveDynamicFields = "1";
			fontColors0 = "12 27 15 255";
			fontColors1 = "99 83 83 255";
			fontColors2 = "62 255 0 255";
			fontColors3 = "254 3 62 255";
			fontColors4 = "67 64 12 255";
			fontColors5 = "9 71 43 255";
			fontColors6 = "3 21 254 255";
			fontColors7 = "97 67 9 255";
			fontColors8 = "254 3 43 255";
			fontColors9 = "3 48 248 255";
		};
	};
	new SimGroup(GuiColorFill_Group)
	{
		internalName = "colorFill";
		canSave = "1";
		canSaveDynamicFields = "1";

		new ScriptObject()
		{
			internalName = "BGAltA";
			canSave = "1";
			canSaveDynamicFields = "1";
			bevelColorLL = "252 252 252 77";
			borderColor = "67 83 83 255";
			borderColorHL = "50 53 53 255";
			borderColorNA = "39 255 0 203";
			fillColor = "252 254 252 255";
			fillColorERR = "254 3 62 255";
			fillColorHL = "252 189 81 255";
			fillColorNA = "254 227 83 255";
			fillColorSEL = "238 255 0 255";
		};
		new ScriptObject()
		{
			internalName = "BGBaseA";
			canSave = "1";
			canSaveDynamicFields = "1";
			bevelColorLL = "252 252 252 255";
			borderColor = "67 83 83 255";
			borderColorHL = "50 53 252 255";
			borderColorNA = "39 255 0 203";
			fillColor = "252 254 252 255";
			fillColorERR = "254 3 62 255";
			fillColorHL = "39 189 81 255";
			fillColorNA = "254 227 39 255";
			fillColorSEL = "252 255 0 255";
		};
	};
	new ScriptObject()
	{
		internalName = "colorFill";
		canSave = "1";
		canSaveDynamicFields = "1";
		colorA = "43 43 43 255";
		ColorB = "25 25 25 255";
		ColorC = "15 0 255 255";
		DarkA = "255 0 9 187";
		DarkB = "21 21 21 189";
		DarkC = "37 36 36 194";
		LightA = "241 241 241 189";
		LightB = "34 255 0 255";
		LightC = "153 153 153 196";
	};
};
//--- OBJECT WRITE END ---
