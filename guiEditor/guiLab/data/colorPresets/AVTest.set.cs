//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
delObj(GuiColor_Group);
new SimGroup(GuiColor_Group)
{
	canSave = "1";
	canSaveDynamicFields = "1";

	new SimGroup(GuiColorFont_Group)
	{
		internalName = "colorFont";
		canSave = "1";
		canSaveDynamicFields = "1";
		colorRole["Common_0"] = "Client";
		colorRole["Common_1"] = "Server";
		colorRole["Common_2"] = "pf Client";
		colorRole["Common_3"] = "pf Game";
		colorRole["Common_4"] = "";
		colorRole["Common_5"] = "pf Loca";
		colorRole["Common_6"] = "";
		colorRole["Common_7"] = "";
		colorRole["Common_8"] = "";
		colorRole["Common_9"] = "";
		new ScriptObject()
		{
			internalName = "Common";
			canSave = "1";
			canSaveDynamicFields = "1";
			fontColors0 = "232 245 255 255 255";
			fontColors1 = "194 245 254 255";
			fontColors2 = "203 203 203 255";
			fontColors3 = "250 227 176 255";
			fontColors4 = "202 203 203 255 255";
			fontColors5 = "210 217 200 255 255";
			fontColors6 = "255 115 2 255 255";
			fontColors7 = "233 245 255 255";
			fontColors8 = "33 190 232 255 255";
			fontColors9 = "255 229 204 255 255";

		};

		new ScriptObject()
		{
			internalName = "List";
			canSave = "1";
			canSaveDynamicFields = "1";
			fontColors0 = "12 14 34 255";
			fontColors1 = "189 180 180 255";
			fontColors2 = "3 73 254 255";
			fontColors3 = "254 3 62 255";
			fontColors4 = "101 85 12 255";
			fontColors5 = "37 78 60 255";
			fontColors6 = "3 21 254 255";
			fontColors7 = "150 87 15 255";
			fontColors8 = "254 3 43 255";
			fontColors9 = "3 48 248 255";
		};

	};
	new SimGroup(GuiColorFill_Group)
	{
		internalName = "colorFill";
		canSave = "1";
		canSaveDynamicFields = "1";

		new ScriptObject()
		{
			internalName = "BGCommon";
			canSave = "1";
			canSaveDynamicFields = "1";
			bevelColorLL = "219 187 11 255";
			borderColor = "234 0 255 157";
			borderColorHL = "50 53 53 255";
			borderColorNA = "39 255 0 203";
			fillColor = "69 255 0 255";
			fillColorERR = "254 3 62 255";
			fillColorHL = "255 0 9 255";
			fillColorNA = "254 227 83 255";
			fillColorSEL = "238 255 0 255";
		};
		new ScriptObject()
		{
			internalName = "BGAltA";
			canSave = "1";
			canSaveDynamicFields = "1";
			bevelColorLL = "173 136 16 255";
			borderColor = "21 255 0 95";
			borderColorHL = "50 53 252 255";
			borderColorNA = "39 255 0 203";
			fillColor = "215 3 254 255";
			fillColorERR = "254 3 62 255";
			fillColorHL = "0 9 255 255";
			fillColorNA = "254 227 39 255";
			fillColorSEL = "252 255 0 255";
		};
	};
	new ScriptObject()
	{
		internalName = "colorFill";
		canSave = "1";
		canSaveDynamicFields = "1";
		colorA = "153 113 9 255";
		ColorB = "25 25 25 255";
		ColorC = "39 60 81 255";
		DarkA = "50 255 0 255";
		DarkB = "21 20 20 255";
		DarkC = "37 36 36 255";
		LightA = "229 229 229 255";
		LightB = "192 194 194 255";
		LightC = "153 153 153 255";
	};
};
//--- OBJECT WRITE END ---

