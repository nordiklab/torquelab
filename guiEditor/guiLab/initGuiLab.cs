//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$ProfileUpdateSkipCategories = "Tools Core Editor Lab";
$GLProfileColorTypes = "colorFill colorFont";

$GLab_DataRoot = "tlab/guiEditor/guiLab/data/";

$ProfileAutoSave = true;

$GLab_JustifyOptions = "Left Center Right Top Bottom";
$GLab_ProfileCategories = "GameContainer UIButton GameText GameElement GameList GameCore";

$GLab_FontList = "Davidan\tBoostSSK\tBrela\tCarval\tCrimson Text\tLuthier Regular\tRoboto Slab Regular\tWP DOMINO novel";

$GLab::TabId = 0;
$GLab_SelectedObject = "ToolsDefaultProfile";

$GLab_Caption_NoFieldSelected = "Click field name to select";
$GLab_Text_FieldStarMean = "*  \c1= \c2Field value is set in a parent profile";

$GLab_ShowGameProfile = true;

$GLab_UpdateColorsOnSetChanged = true;
$GLab_RescanProfilesOnSave = true;

$GLab_EmbedColorSetInProfile = false;
//==============================================================================
// Load the GuiManager scripts and guis if specified
function initGuiLab()
{
	newScriptObject("GLab");
	newScriptObject("LGTools");
	$arProfData = newArrayObject("arProfData");

	if (isFile("tlab/guiEditor/guiLab/prefs.cs"))
		exec("tlab/guiEditor/guiLab/prefs.cs");

	//GLab.initColorManager();
	initGuiSystem();
}
//------------------------------------------------------------------------------
function postGuiLab()
{
	GLab.initColorManager(true);
	scanAllProfileFile();
}
function execglab()
{
	execPattern("tlab/guiEditor/guilab/*.cs");
	GLab.initColorManager(true);
}

//==============================================================================
// Build the Game related Profiles data list
function initGameProfilesData()
{
	//doGuiGroupAction("GLab_GameProfileMenu","clear()");
	eLink.doEval("GLab_GameProfileMenu","clear()");
	newSimSet("GameProfileGroup");

	foreach(%obj in GuiDataGroup)
	{
		if (!%obj.isMemberOfClass("GuiControlProfile"))
			continue;

		%startCat = getSubStr(%obj.category,0,3);

		if (%startCat !$= "Gam")
			$GLab_IsGameProfile[%obj.getName()] = true;

		if (strFind(%obj.getName(),"Tools") || strFind(%obj.getName(),"Lab") || %obj.category $= "Tools" || strFind(%obj.getName(),"Inspector"))
			$GLab_IsToolProfile[%obj.getName()] = true;

		//  else
		//   continue;
		// }
		$ProfileDefault["fontSize"] = %obj.fontSize;
		GameProfileGroup.add(%obj);
		eLink.doEval("GLab_GameProfileMenu","add(\""@%obj.getName()@"\",\""@%obj.getId()@"\")");
		//doGuiGroupAction("GLab_GameProfileMenu","add(\""@%obj.getName()@"\",\""@%obj.getId()@"\")");
	}
}

//------------------------------------------------------------------------------

function initGuiSystem(%check)
{
	if (%check && $GuiSystemLoaded)
		return;

	if (!isObject(GameProfileGroup))
	{
		initGameProfilesData();
	}

	if (!$ProfileScanDone)
	{
		//scanAllProfileFile();
	}

	//GLab.updateColors();
	$CanvasSize = Canvas.getExtent();
	$GuiSystemLoaded = true;
}

