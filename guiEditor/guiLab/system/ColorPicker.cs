//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function GuiColorDefaultPicker::pickColorF(%this, %updateFunc,%arg1,%arg2,%arg3)
{
	%ctrl.updateCommand = %updateFunc@"(%this.internalName,%color,\""@%arg1@"\",\""@%arg2@"\",\""@%arg3@"\");";
	%currentColor =   %this.baseColor;
	%callBack = %this@".ColorPicked";
	%updateCallback = %this@".ColorUpdated";
	GetColorF(%currentColor, %callback, %this.getRoot(), %updateCallback, %cancelCallback);
}
function GuiColorDefaultPicker::pickColorI(%this, %updateFunc,%arg1,%arg2,%arg3)
{
	%ctrl.updateCommand = %updateFunc@"(%this.internalName,%color,\""@%arg1@"\",\""@%arg2@"\",\""@%arg3@"\");";
	%currentColor =   ColorFloatToInt(%this.baseColor);
	%callBack = %this@".ColorPicked";
	%updateCallback = %this@".ColorUpdated";
	GetColorI(%currentColor, %callBack, %this.getRoot(),%updateCallback);
}
//==============================================================================
// Color Picker callbacks for Gui Manager color selection
//==============================================================================

//==============================================================================
// Empty Editor Gui
function GuiColorDefaultPicker::ColorRefresh(%this)
{
	%srcObj = %this.sourceObject;
	%srcField = %this.sourceField;
	%alpha = mCeil(getWord(%color,3));
	%color = setWord(%color,3,%alpha);
	%this.baseColor = ColorIntToFloat(%color);
	%srcObj.setFieldValue(%srcField,%color);
}
//------------------------------------------------------------------------------
//==============================================================================
// Empty Editor Gui
function GuiColorDefaultPicker::ColorPicked(%this,%color)
{
	%srcObj = %this.sourceObject;
	%srcField = %this.sourceField;
	%alpha = mCeil(getWord(%color,3));
	%color = setWord(%color,3,%alpha);
	%this.baseColor = ColorIntToFloat(%color);
	%srcObj.setFieldValue(%srcField,%color);

	if (isObject(%this.alphaSlider))
	{
		%this.alphaSlider.setValue(%alpha);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Empty Editor Gui
function GuiColorDefaultPicker::ColorUpdated(%this,%color)
{
	%alpha = mCeil(getWord(%color,3));
	%this.baseColor = ColorIntToFloat(%color);
	// %this.sourceArray.setValue(%this.internalName,%color);
	%this.updateColor();

	if (isObject(%this.alphaSlider))
	{
		%this.alphaSlider.setValue(%alpha);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Empty Editor Gui
function GLabSliderAlpha::update(%this)
{

	if (!isObject(%this.colorPicker))
	{
		warnLog("Invalid color picker referenced!");
		return;
	}

	%this.colorPicker.baseColor.a = %this.getValue();
	%current = $GLab_SelectedObject.getFieldValue(%this.fieldSource);
	%currentAlpha = %current.a;
	%intAlpha = mCeil(%this.getValue() * 255);
	%new = setWord(%current,3,%intAlpha);
	%this.colorPicker.evalUpdate();
}
//------------------------------------------------------------------------------
//==============================================================================
// Empty Editor Gui
function GLabSliderAlpha::altUpdate(%this)
{

	if (!isObject(%this.colorPicker))
	{
		warnLog("Invalid color picker referenced!");
		return;
	}

	%this.colorPicker.baseColor.a = %this.getValue();
}
//------------------------------------------------------------------------------
