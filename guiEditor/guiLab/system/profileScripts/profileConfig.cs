//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function cfgProfileDump(%fields,%file)
{
   if (%file $= "")
   {
      cfgProfileConsoleDump(%fields);
      return;  
   }
	%fileObj = getFileWriteObj(%file);
	info("-----------------SavingFile",%file);

	//%j = 1;
	foreach(%profile in ThemeProfileGroup)
	{
	   %list = strAddWord(%list,%profile.getName(),1);
	  %fileObj.writeLine(%profile.getName()); 
	}
	%fileObj.writeLine("===================================="); 
	%fileObj.writeLine("Complete list of profile names:"); 
	%fileObj.writeLine(%list); 
	closeFileObj(%fileObj);
	
}
//------------------------------------------------------------------------------
function cfgProfileConsoleDump(%fields,%asList)
{
   
	info("-----------------cfgProfileConsoleDump",%fields);

	foreach(%profile in ThemeProfileGroup)
	{
	   if (%asList)
	      %list = strAddWord(%list,%profile.getName(),1);
      else
	      info(%profile.getName());
	}
	if (%asList)
	{
	   info("List of all Theme Profiles Names:");
	   info("","","",%list);
	}
	
}
