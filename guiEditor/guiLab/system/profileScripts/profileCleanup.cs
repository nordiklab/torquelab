//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//$GLProfileSetFields_["colorFontIds"] = "fontColors[0] fontColors[1] fontColors[2] fontColors[3] fontColors[4] fontColors[5] fontColors[6] fontColors[7] fontColors[8] fontColors[9]";
//$GLProfileSetFields_["colorFont"] = "fontColor fontColorHL fontColorNA fontColorSEL fontColorLink fontColorLinkHL";

//==============================================================================
// Remove all reference for a color type (used to force the colorSet)
function GLab::ClearProfileColorType(%this,%type,%profile)
{
	if (%profile $= "")
		%profile = $GLab_SelectedProfile;

	%fieldList =  $GLProfileSetFields_[%type] ;

	if (%fieldList $= "" || !isObject(%profile))
		return false;

	info("Removing fields:",%fieldList,"From Profile:",%profile);
	removeProfileField(%profile,%fieldList);
}
//------------------------------------------------------------------------------
