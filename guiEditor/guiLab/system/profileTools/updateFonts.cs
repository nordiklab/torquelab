//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Profile Fonts Update from font source
//==============================================================================

//==============================================================================
// Update all the Color Sets assigned profiles
function GLab::updateProfilesFonts(%this)
{
	foreach$(%profile in $GLProfileList_["fontSource"])
	{
		%fontSrc = %profile.getFieldValue("fontSource");
		%fontType = $GuiFont[%fontSrc];
		GLab.updateProfileField(%profile,"fontType",%fontType);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Update a single profile field and set profile dirty if changed
function GLab::updateFontSize(%this,%ratio,%all)
{
	if (!$GLab_FontResizeEnabled)
	{
		//warnLog("Font Resizing mode is disabled, exiting updateFontSize function.");
		return;
	}

	if (%ratio $="")
	{
		%ratio = %this.getGuiRatio();
	}

	%list = $GLProfilesWithField_["fontSize"];

	if (!%all)
		%list = $ResizableProfiles;

	foreach$(%profileName in %list)
	{
		%value = $GLProfileDefaultField_[%profileName,"fontSize"];
		%newValue = %value * %ratio;
		%newValue = mCeil(%newValue);
		%this.updateProfileField(%profileName,"fontSize",%newValue,true);

		foreach$(%child in $GLProfileChilds_[%profileName])
		{
			%childValue = $GLProfileDefaultField_[%profileName,"fontSize"];

			if (%childValue $= "")
				continue;

			%childValue = $GLProfileDefaultField_[%child,"fontSize"];
			%newChildValue = %childValue * %ratio;
			%newChildValue = mCeil(%newChildValue);
			%this.updateProfileField(%child,"fontSize",%newChildValue,true);
		}
	}
}

//==============================================================================
// Update a single profile field and set profile dirty if changed
function GLab::restoreFontSize(%this,%ratio,%all)
{
	%list = $GLProfilesWithField_["fontSize"];

	if (!%all)
		%list = $ResizableProfiles;

	foreach$(%profileName in %list)
	{
		%value = $GLProfileDefaultField_[%profileName,"fontSize"];
		//%newValue = %value * %ratio;
		//%newValue = mCeil(%newValue);
		%this.updateProfileField(%profileName,"fontSize",%value,true);
	}
}
