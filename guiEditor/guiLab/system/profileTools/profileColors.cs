//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function GLab::updateProfilesSetType(%this,%set,%type)
{
	foreach$(%profile in $GLab::ColorSetProfiles_[%type,%set])
	{
		%this.setProfileColorFromSet(%set,%type);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Profile Fonts Update from font source
//==============================================================================

//==============================================================================
// Update all the Color Sets assigned profiles
function GLab::setProfileColorFromSet(%this,%set,%type)
{
	%profile = $GLab_SelectedObject;
	$GLab::ColorSetProfiles_[%type,%set] = strAddWord($GLab::ColorSetProfiles_[%type,%set],%profile.getName(), true);
	GLab.updateProfileColorsSet(%profile,%set,%type);

	switch$(%type)
	{
		case "colorFont":
			GLab.updateSingleProfileColors(%profile,%set,%type);

		case "colorFill":
			GLab.updateSingleProfileColors(%profile,%set);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Update all the Color Sets assigned profiles
function GLab::updateProfileColorsSet(%this,%profile,%set,%type)
{
	if (!isObject(GuiColor_Group))
		return;

	switch$(%type)
	{
		case "colorFont":
			info("Updating profile font colors",%profile.getName(),"Set",%set);
			%srcGroup = GuiColor_Group.findObjectByInternalName(%type,true);
			%srcColor = %srcGroup.findObjectByInternalName(%set,true);
			%count = %srcColor.getDynamicFieldCount();

			for(%i = 0; %i < %count; %i++)
			{
				%id = ""; //Reset the ID so it's blank for fieal with no id
				%fieldFull = %srcColor.getDynamicField(%i);
				%field = getField(%fieldFull,0);
				%value = getField(%fieldFull,1);
				//If last char is numeric, we have to set it as field <=> id
				%strlen = strLen(%field);
				%lastChar = getSubStr(%field,%strlen-1);

				if (strIsNumeric(%lastChar))
				{
					%field = getSubStr(%field,0,%strlen-1);
					%id = %lastChar;
				}

				%current = %profile.getFieldValue(%field,%id);
				%isDirty = false;

				if (%current !$= %value)
				{
					%isDirty = true;
					warnLog("-> Should be set to dirty",%field,%value,%current);
				}

				GLab.updateProfileField(%profile,%field,%value,!%isDirty);
			}

		case "colorFill":
			%srcColor = GuiColor_Group.findObjectByInternalName(%type,true);
			%value = %srcColor.getFieldValue(%type);
			//GLab.updateProfileField(%profile,"fillColor",%value);
	}
}
//------------------------------------------------------------------------------
