//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$GLab_ProfileBook_ActivePageId = 0;

//==============================================================================
function GLab::initProfilePage(%this)
{
	GLab.initProfileParams();
	GLab_ProfileBook.selectPage($GLab_ProfileBook_ActivePageId);
	GLab_ProfilesTree.init();
	GLab_ActiveFieldRollout.expanded = false;
	GLab_ActiveFieldRollout.caption = $GLab_Caption_NoFieldSelected;
	GLab.setSelectedProfile($GLab_SelectedObject);
	//GLab_ProfilesTree.open( GameProfileGroup );
	//GLab_ProfilesTree.buildVisibleTree(true);
}
//------------------------------------------------------------------------------

//==============================================================================
function GLab_ProfileBook::onTabSelected(%this,%text,%id)
{
	$GLab_ProfileBook_ActivePageId = %id;
	$GLab_ProfileBook_ActivePageText = %text;
}
//------------------------------------------------------------------------------
