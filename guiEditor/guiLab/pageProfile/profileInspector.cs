//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function GLab_ProfileInspector::onFieldSelected(%this, %fieldName, %fieldTypeStr, %fieldDoc)
{
	GuiEditorProfileFieldInfo.setText("<font:ArialBold:14>" @ %fieldName @ "<font:ArialItalic:14> (" @ %fieldTypeStr @ ") " NL "<font:Arial:14>" @ %fieldDoc);
}

//---------------------------------------------------------------------------------------------

function GLab_ProfileInspector::onFieldAdded(%this, %object, %fieldName)
{
	GLab.setProfileDirty(%object, true);
	// GuiEditor.setProfileDirty( %object, true );
}

//---------------------------------------------------------------------------------------------

function GLab_ProfileInspector::onFieldRemoved(%this, %object, %fieldName)
{
	GLab.setProfileDirty(%object, true);
	//GuiEditor.setProfileDirty( %object, true );
}

//---------------------------------------------------------------------------------------------

function GLab_ProfileInspector::onFieldRenamed(%this, %object, %oldFieldName, %newFieldName)
{
	GLab.setProfileDirty(%object, true);
	//GuiEditor.setProfileDirty( %object, true );
}

function GLab_ProfileInspector::onInspectorFieldModified(%this, %object, %fieldName, %arrayIndex, %oldValue, %newValue)
{
	GLab.setProfileDirty(%object, true);

	if (strFind(%arrayIndex,"null"))
		%arrayIndex = "";

	GLab.updateProfileChildsField(%object,%fieldName SPC %arrayIndex,%newValue);
	//GuiEditor.setProfileDirty( %object, true );
}

//---------------------------------------------------------------------------------------------

function GLab_ProfileInspector::onInspectorPreFieldModification(%this, %fieldName, %arrayIndex)
{
	GLab.setProfileDirty($GLab_SelectedObject, true);
}

//---------------------------------------------------------------------------------------------

function GLab_ProfileInspector::onInspectorPostFieldModification(%this)
{
}

//---------------------------------------------------------------------------------------------

function GLab_ProfileInspector::onInspectorDiscardFieldModification(%this)
{
}
function GuiInspectorTypeColorI::apply(%this,%text)
{
	%newtext = %text;

	if (getWordCount(%text) != 3 && getWordCount(%text) != 4)
		%newtext = strToColorI(%text);

	//Parent::apply(%newtext,);

}

