//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$GLToolColorSet_Folder = "tlab/EditorLab/tools/guiLab/data/ColorSet/";
//==============================================================================
function GLab::initToolColorSet(%this)
{

}
//------------------------------------------------------------------------------

//==============================================================================
function GLabToolColorSetApply::onClick(%this)
{
	%type = %this.internalName;

	%editGlobal = $GLab_ColorSet[%type];

	if (%editGlobal $= "" || !isObject($GLab_SelectedObject))
		return;

	colorizeProfile($GLab_SelectedObject,%editGlobal,%type);

}
//------------------------------------------------------------------------------

//==============================================================================
function GLabToolColorSetGroup::onClick(%this)
{
	%typeData = %this.internalName;
	%type = firstWord(%typeData);
	%param = restWords(%typeData);
	%editGlobal = $GLab_ColorSetGroup;

	switch$(%type)
	{
		case "Add":
			$GLToolColorSet_Group_[%editGlobal,%param] = strAddWord($GLToolColorSet_Group_[%editGlobal,%param],$GLab_SelectedObject.getName(),true);

		case "Remove":
			$GLToolColorSet_Group_[%editGlobal,%param] = strRemoveWord($GLToolColorSet_Group_[%editGlobal,%param],$GLab_SelectedObject.getName());

	}

	export("$GLToolColorSet_Group_*",$GLToolColorSet_Folder@"groupsets.cfg.cs");
	//export("$GLToolColorSet_Group_"@%editGlobal@"*",$GLToolColorSet_Folder@"groupset_"@%editGlobal@".cfg.cs");

}
//------------------------------------------------------------------------------

//==============================================================================
// Define color sets
//------------------------------------------------------------------------------

//==============================================================================
// AV Color Set 1
%c=0;
%set=1;
$LabColorSet[%set,%c++] = convertColorToRGB("FFFFFF") SPC "255";
$LabColorSet[%set,%c++] = convertColorToRGB("E8F5FF") SPC "255";
$LabColorSet[%set,%c++] = convertColorToRGB("CFF7DE") SPC "255";
$LabColorSet[%set,%c++] = convertColorToRGB("FFE5CC") SPC "255";
$LabColorSet[%set,%c++] = convertColorToRGB("CFF7DE") SPC "255";
$LabColorSet[%set,%c++] = convertColorToRGB("5BBBFF") SPC "255";
$LabColorSet[%set,%c++] = convertColorToRGB("006CB6") SPC "255";
$LabColorSet[%set,%c++] = convertColorToRGB("7ADB9F") SPC "255";
//==============================================================================
// AV Color Set 2
%c=0;
$LabColorSet[%set,%c++] = convertColorToRGB("B2B3B2") SPC "255";
$LabColorSet[%set,%c++] = convertColorToRGB("9AA6FF") SPC "255";
$LabColorSet[%set,%c++] = convertColorToRGB("FFF664") SPC "255";
$LabColorSet[%set,%c++] = convertColorToRGB("49E7FF") SPC "255";
$LabColorSet[%set,%c++] = convertColorToRGB("96FF91") SPC "255";
$LabColorSet[%set,%c++] = convertColorToRGB("FCDA62") SPC "255";
$LabColorSet[%set,%c++] = convertColorToRGB("7EBFE9") SPC "255";
$LabColorSet[%set,%c++] = convertColorToRGB("EEF4F8") SPC "255";
//------------------------------------------------------------------------------

function getColorTest(%set)
{
	%text = "\c00-Color \c11-Color \c22-Color \c33-Color \c44-Color \c55-Color \c66-Color \c77-Color \c88-Color \c99-Color";
	return %text;
}
//==============================================================================
function colorizeProfile(%profile,%set,%type)
{
	%field["fill"] = "fillColor";
	%field["font"] = "fontColors";

	for(%i=0; %i<10; %i++)
	{
		%color = $LabColorSet[%set,%i];
		if (getWordCount(%color) < 3)
			continue;
		%profile.setFieldValue(%field[%type],$LabColorSet[%set,%i],%i);
	}

}
//------------------------------------------------------------------------------
