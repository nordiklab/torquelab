//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function GLMulti::addMultiInspectorTo(%this,%group)
{
	%newGui = new GuiVariableInspector(GLMultiVarInspector)
	{
		dividerPos = "0.65";
		dividerMargin = "5";
		showCustomFields = "1";
		stackingType = "Vertical";
		horizStacking = "Left to Right";
		vertStacking = "Top to Bottom";
		padding = "1";
		dynamicSize = "1";
		dynamicNonStackExtent = "0";
		dynamicPos = "0";
		changeChildSizeToFit = "1";
		changeChildPosition = "1";
		position = "1 1";
		extent = "309 1227";
		minExtent = "16 16";
		horizSizing = "right";
		vertSizing = "bottom";
		profile = "ToolsDefaultProfile";
		visible = "1";
		active = "1";
		tooltipProfile = "ToolsToolTipProfile";
		hovertime = "1000";
		isContainer = "1";
		canSave = "1";
		canSaveDynamicFields = "0";
	};

	if (isObject(%group))
		%group.add(%newGui);

	return %newGui;
}
//------------------------------------------------------------------------------
//==============================================================================
function GLMulti::getSelectedProfiles(%this)
{
	%items = GLMultiList.getSelectedItems();

	foreach$(%item in %items)
	{
		%profiles = strAddWord(%profiles, GLMultiList.getItemObject(%item),true);
	}

	%this.selProfiles = %profiles;
	%this.selItems = %items;
	$GLMulti_SelCount = GLMultiList.getSelCount();
	return %profiles;
}
//------------------------------------------------------------------------------
//==============================================================================
function GLMultiApplyButton::onClick(%this)
{
	%typeData = %this.internalName;
	%type = firstWord(%typeData);
	%param = restWords(%typeData);
	%editGlobal = $GLab_ColorSetGroup;

	switch$(%type)
	{
		case "ApplyFont":
			%source = GuiColorFont_Group.findObjectByInternalName(%param);
			GLMulti.applyFontColorSetToSel(%source);

	}

}
//------------------------------------------------------------------------------

//==============================================================================
function GLMulti::applyFontColorSetToSel(%this,%source)
{

	%set = %source.internalName;
	%profiles = %this.getSelectedProfiles();

	foreach$(%prof in %profiles)
	{
		GLab.setProfileDirty(%prof,true);
		%count = %source.getDynamicFieldCount();

		for(%i=0 ; %i<%count; %i++)
		{
			%fieldFull = %source.getDynamicField(%i);
			%field = getWord(%fieldFull,0);
			%color = removeWord(%fieldFull,0);
			%index = getSubStr(%field,strlen(%field)-1,1);
			%fieldOnly = getSubStr(%field,0,strlen(%field)-1);
			%prof.setFieldValue(%fieldOnly,%color,%index);
		}

		$GLMultiProfileColorSet_Font_[%prof.getName()] = %set;
	}

	export("$GLMultiProfileColorSet_*",$GLab_DataRoot@"multiSet/profiles.cfg.cs");
}
//------------------------------------------------------------------------------
