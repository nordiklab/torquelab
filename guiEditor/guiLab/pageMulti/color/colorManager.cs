//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$GLab::DefaultColorsFile = "tlab/EditorLab/tools/guiLab/data/colorPresets/default.cs";

$GLab_ColorGroups = "colorFont colorFill";
//==============================================================================
// Initialized the Color Manager by loading default colors and updating the colors
function GLab::initColorManager(%this,%force)
{
	if ($GLab::ColorManager::Loaded && !%force)
		return;

	exec($GLab::DefaultColorsFile);
	//Scan and initialize the loaded color groups
	%this.updateColors();
	//Scan the presets and update the menu
	GLab.initColorPresetMenu();
	%this.updateProfilesColors();
	$GLab::ColorManager::Loaded = true;
}
//------------------------------------------------------------------------------

//==============================================================================
function GLab::saveColors(%this,%saveDefault)
{
	%file = $GLab::DefaultColorsFile;

	if (!%saveDefault)
	{
		%name = $GLab_PresetName;

		if (%name $= "default" || %name $= "[default]")
		{
			msgBoxOk("Can't name a preset \"Default\"","You are trying to save a preset named default, this name is reserved for the default color set. Please change the name and try again","");
			return;
		}

		%file = strreplace(%file,"default",%name@".set");
		%id = DIG_ColorPresetMenu.lastId;

		if (!isFile(%file))
		{
			GLab.addColorPresetFile(%file);
			GLab_ColorPresetMenu.setSelected(GLab_ColorPresetMenu.lastId,false);
		}
	}

	GuiColor_Group.save(%file,false,"delObj(GuiColor_Group); \n");
}
//------------------------------------------------------------------------------

//==============================================================================
// Manage color presets menu
//==============================================================================
//==============================================================================
function GLab::initColorPresetMenu(%this)
{
	GLab_ColorPresetMenu.clear();
	%filePathScript = "tlab/EditorLab/tools/guiLab/data/colorPresets/*.set.cs";
	%pid = 0;
	GLab_ColorPresetMenu.lastId = -1;
	GLab.addColorPresetFile($GLab::DefaultColorsFile);
	//GLab_ColorPresetMenu.add("Default",%pid);

	for(%file = findFirstFile(%filePathScript); %file !$= ""; %file = findNextFile(%filePathScript))
	{
		GLab.addColorPresetFile(%file);
	}

	GLab_ColorPresetMenu.setSelected(0);
}
//------------------------------------------------------------------------------
//==============================================================================
function GLab::addColorPresetFile(%this,%file)
{
	%fileName = strreplace(fileBase(%file),".","\t");
	%fileName = getField(%fileName,0);
	GLab_ColorPresetMenu.add(%fileName, GLab_ColorPresetMenu.lastId++);
}
//------------------------------------------------------------------------------
//==============================================================================
function GLab_ColorPresetMenu::onSelectPreset(%this)
{
	%name = %this.getText();
	%file = $GLab::DefaultColorsFile;

	if (%name !$= "Default")
	{
		%file = strreplace(%file,"default",%name@".set");
	}

	if (%name $= "default")
		%name = "[default]";

	GLab_ColorPresetName.setText(%name);
	exec(%file);
	GLab.updateColors();
	GLab.updateProfilesColors();
	GLab.updateColors(GLMulti_ColorSetStack);
}
//------------------------------------------------------------------------------
/*
            fontColors0 = convertColorToRGB("E8F5FF") SPC "255";
            fontColors1 = convertColorToRGB("D2D9C8") SPC "255";
            fontColors2 = convertColorToRGB("5BBBFF") SPC "255";
            fontColors3 = convertColorToRGB("F2BE3E") SPC "255";
            fontColors4 = convertColorToRGB("CACBCB") SPC "255";
            fontColors5 = convertColorToRGB("D2D9C8") SPC "255";
            fontColors6 = convertColorToRGB("FF7302") SPC "255";
            fontColors7 = convertColorToRGB("C2D9E8") SPC "255";
            fontColors8 = convertColorToRGB("21BEE8") SPC "255";
            fontColors9 = convertColorToRGB("FFE5CC") SPC "255";
            */
