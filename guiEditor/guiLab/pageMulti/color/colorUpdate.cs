//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Update the Colors preset preview from current presets
//==============================================================================
//==============================================================================
// Update all the color sets
//GLab.updateColors( GLMulti_ColorSetStack);
function GLab::updateColors(%this,%stack)
{
	foreach(%obj in GLMulti_ColorSetStack)
	{
		if (%obj.noremove)
			%obj.visible = 0;
		else
			%obj.schedule(0,delete);
	}

	GLMulti_ColorSetRolloutSrc.visible = 0;
	GLMulti_ColorSetStack.visible = 1;

	//First reset the color list for each color sets type
	foreach$(%colorGroup in $GLab_ColorGroups)
		$GLab_ColorGroupSetList[%colorGroup] = "None";

	//Check each object in Color group and update the associated colorPickerCtrl
	//Colors are store in 2 ways: SimGroup and ScriptObject
	if (!isObject(GuiColor_Group))
		return;

	foreach(%obj in GuiColor_Group)
	{
		if (%obj.getClassName() $= "SimGroup" && %obj.internalName $= "colorFont")
			%this.updateColorFontGroup(%obj,%stack);

		if (%obj.getClassName() $= "SimGroup" && %obj.internalName $= "colorFill")
			%this.updateColorFillGroup(%obj,%stack);
		else if (%obj.getClassName() $= "ScriptObject")
			%this.updateColorFillObj(%obj,%stack);
	}
}
//------------------------------------------------------------------------------
$GLab_FontRole[0] = "Normal";
$GLab_FontRole[1] = "Hover";
$GLab_FontRole[2] = "N/A";
$GLab_FontRole[3] = "Sel.";
//==============================================================================
// Update a color preset using SimGroup
function GLab::updateColorFontGroup(%this,%group,%stack)
{
	%colorGroup = "colorFont";

	if (!isObject(%stack))
		%stack = GLMulti_ColorSetStack;

	foreach(%obj in %group)
	{
		%colorSet = %obj.internalName;
		//Make sure the color is in the color sets list
		$GLab_ColorGroupSetList[%colorGroup] = strAddWord($GLab_ColorGroupSetList[%colorGroup],%colorSet,true);
		%colorsGroup = %stack.findObjectByInternalName("rollout" SPC %colorSet,true);

		if (!isObject(%colorsGroup))
		{
			%colorsGroup = cloneGui(GLMulti_ColorSetRolloutSrc,GLMulti_ColorSetStack);
			%colorsGroup.caption = %colorSet @ "Font Colors";
			%colApply = %colorsGroup-->ApplyFont;
			%colApply.internalName = "ApplyFont" SPC %colorSet;
		}
		else
			%colorsGroup.visible = 1;

		//%groupHeader = %stack.findObjectByInternalName(%colorSet@"Text",true);
		//%groupHeader.text = %colorSet SPC "Colors:" @ "\c0 c0\c1 c1\c2 c2\c3 c3\c4 c4\c5 c5\c6 c6\c7 c7\c8 c8\c9 c9";
		%count = %obj.getDynamicFieldCount();

		for(%i=0 ; %i<%count; %i++)
		{
			%fieldFull = %obj.getDynamicField(%i);
			%field = getWord(%fieldFull,0);
			%color = removeWord(%fieldFull,0);
			%len = %field;
			%colorId = strLastChars(%field,1);
			%textCtrl = %colorsGroup.findObjectByInternalName("c"@%colorId,true);

			%role = %group.colorRole[%colorSet,%colorId];

			if (%role !$= "")
				%taggedText = %role;
			else if ($GLab_FontRole[%colorId] !$= "")
				%taggedText = $GLab_FontRole[%colorId];
			else
				%taggedText = "Font["@%colorId@"]";

			//%taggedText = '\c'@%colorId@'Me';
			//eval("%taggedText = \\\"c"@%colorId@%text@";");
			if (!isObject(%textCtrl))
			{
				warnLog("Can't find the text ctrl for color:",%field,"ID:",%colorId,"GroupID:",%colorsGroup.getId());
			}
			else
				%textCtrl.setText(%taggedText);

			%colorPick = %colorsGroup.findObjectByInternalName(%field,true);
			%this.updateColorPickerCtrl(%colorPick,%field,%color,%obj);
		}
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Update a color preset using SimGroup
function GLab::updateColorFillGroup(%this,%group,%stack)
{
	%colorGroup = "colorFill";

	if (!isObject(%stack))
		%stack = GLMulti_ColorSetStack;

	foreach(%obj in %group)
	{
		%colorSet = %obj.internalName;
		//Make sure the color is in the color sets list
		$GLab_ColorGroupSetList[%colorGroup] = strAddWord($GLab_ColorGroupSetList[%colorGroup],%colorSet,true);
		%colorsGroup = %stack.findObjectByInternalName(%colorSet,true);
		//%groupHeader = %stack.findObjectByInternalName(%colorSet@"Text",true);
		//%groupHeader.text = %colorSet SPC "Colors:" @ "\c0 c0\c1 c1\c2 c2\c3 c3\c4 c4\c5 c5\c6 c6\c7 c7\c8 c8\c9 c9";
		%count = %obj.getDynamicFieldCount();

		for(%i=0 ; %i<%count; %i++)
		{
			%fieldFull = %obj.getDynamicField(%i);
			%field = getWord(%fieldFull,0);
			%color = removeWord(%fieldFull,0);
			//%len = %field;
			//%colorId = strLastChars(%field,1);

			%colorPick = %colorsGroup.findObjectByInternalName(%field,true);
			%this.updateColorPickerCtrl(%colorPick,%field,%color,%obj);
		}
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Update a color preset using ScriptObject
function GLab::updateColorFillObj(%this,%obj,%stack)
{
	%colorGroup = "colorFill";
	%count = %obj.getDynamicFieldCount();

	if (!isObject(%stack))
		%stack = GLab_ColorSetStack;

	for(%i=0 ; %i<%count; %i++)
	{
		%fieldFull = %obj.getDynamicField(%i);
		%field = getWord(%fieldFull,0);
		%color = removeWord(%fieldFull,0);
		//Make sure the color is in the color sets list
		$GLab_ColorGroupSetList[%colorGroup] = strAddWord($GLab_ColorGroupSetList[%colorGroup],%field,true);
		%fieldlen = strlen(%field);
		%fillType = getSubStr(%field,0,%fieldlen-1);
		%fillId = getSubStr(%field,%fieldlen-1);
		%typeStack  = %stack.findObjectByInternalName(%fillType,true);
		%idCtrl = %typeStack.findObjectByInternalName(%fillId,true);
		%colorPick = %idCtrl->picker;
		%this.updateColorPickerCtrl(%colorPick,%field,%color,%obj);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Update a color preset using ScriptObject
function GLab::updateColorPickerCtrl(%this,%colorPick,%field,%color,%srcObj)
{
	%colorPick.canSaveDynamicFields = "1";
	%colorPick.baseColor = ColorIntToFloat(%color);
	%colorPick.pickColor = ColorIntToFloat(%color);
	%colorPick.superClass = "GuiColorDefaultPicker";
	%colorPick.sourceObject = %srcObj;
	%colorPick.sourceField = %field;
	%colorPick.command = "$ThisControl.pickColorI();";
}
//------------------------------------------------------------------------------
