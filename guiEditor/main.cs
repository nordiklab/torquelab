//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Fix for moving item in tree instead of nudging selected controls is:
//    GuiEditorTreeView.clearFirstResponder();
//==============================================================================

$InGuiEditor = false;
$MLAAFxGuiEditorTemp = false;
$GuiEdGui_BottomTabPage = 0;

//GuiEditorProfilesTree.init
//==============================================================================
function initializeGuiEditor()
{

	if ($TLab_GuiEditorLoaded)
		return;

	if ($Cfg_GuiEditor_GuiEditor_previewResolution $= "")
		exec("tlab/config.cfg.cs");

	if (!isObject(GuiLab))
		$GuiLab = new scriptObject("GuiLab");

	if (!isObject(GuiEd))
		$GuiEd = new scriptObject("GuiEd");

	newSimGroup(GuiEditDumpGroup,GuiGroup);
	delObj(GuiEdMap);
	newActionMap(GuiEdMap);

	GuiEdMap.bind(keyboard,"delete",EditorGlobalDelete);
	GuiEdMap.bind(keyboard,"up",guiEdNudgeUp);
	GuiEdMap.bind(keyboard,"down",guiEdNudgeDown);
	GuiEdMap.bind(keyboard,"left",guiEdNudgeLeft);
	GuiEdMap.bind(keyboard,"right",guiEdNudgeRight);
	GuiEdMap.bindCmd(keyboard,"delete","GuiEditorDeleteSelection(1);","");
	//GlobalActionMap.unbind(keyboard,"delete");
	// GUIs.
	execGuiEdit(true);
	GuiEditorGui.initSettings();
	$TLab_GuiEditorLoaded = true;

	$GuiEditor::GuiFilterList =
	   "GuiEditorGui" TAB
	   "AL_ShadowVizOverlayCtrl" TAB
	   "ToolsMsgBoxOKDlg" TAB
	   "ToolsMsgBoxOKCancelDlg" TAB
	   "ToolsMsgBoxOKCancelDetailsDlg" TAB
	   "ToolsMsgBoxYesNoDlg" TAB
	   "ToolsMsgBoxYesNoCancelDlg" TAB
	   "MessagePopupDlg";
      
      //If not loaded, the menu popups are not working (FIXME)
	   if (!isObject(EditorGui))	
		   execGui("tlab/ui/gui/EditorGui.gui");	
}
//------------------------------------------------------------------------------

//==============================================================================

//==============================================================================
function execGuiEdit(%execGui,%execMainGui)
{
	if (%execGui)
	{
		%execMainGui = true;
		exec( "./gui/guiEditorNewGuiDialog.gui" );
		exec( "./gui/guiEditorPrefsDlg.gui" );
		exec( "./gui/EditorChooseGUI.gui" );

		exec( "tlab/guiEditor/gui/GuiEditFieldDuplicator.gui" );

	}

	if (%execMainGui)
	{
		exec( "tlab/guiEditor/gui/guiEditor.gui" );
		execGuiDir( "tlab/guiEditor/gui/dlgs",1);

		//exec( "tlab/guiEditor/gui/CloneEditorGui.gui" );
	}

	if (!isObject(GuiEditor))
	{
		warnLog("GuiEditor COntrol is missing, fix this before using Gui Editor");
		return;
		addGuiEditorCtrl();
	}

	// Scripts.
	exec( "tlab/guiEditor/guiEditorState.cs" );
	exec( $TLab_GuiEd_ConfigDefault );

	execPattern( "tlab/guiEditor/scripts/*.cs" );
	//exec( "tlab/guiEditor/scripts/functionControls.cs" );
	execPattern( "tlab/guiEditor/lab/*.cs","templateManager" );
	execPattern( "tlab/guiEditor/system/*.cs" );
	//execPattern( "tlab/guiEditor/helpers/*.cs" );
   execPattern( "tlab/guiEditor/guiLab/*.cs","" );
   	execGuiDir( "tlab/guiEditor/guiLab/",1);
	execPattern( "tlab/guiEditor/gui/*.cs","backup" );
   Lab.initElinkUtil();
   initGuiLab();
	//Now let's build the GuiEditor menu
	Lab.buildGuiMenus();
	GuiEd.SetInitialState();
}
//------------------------------------------------------------------------------
//==============================================================================
function destroyGuiEditor()
{
}
//------------------------------------------------------------------------------

function guiEdMapPush(%push)
{
	if (%push && !guiEd.mapPushed)
	{
		guiEd.mapPushed = true;
		guiEdMap.push();
	}
	else if (!%push && guiEd.mapPushed)
	{
		guiEd.mapPushed = false;
		guiEdMap.pop();
	}
}

function GuiEditorDeleteSelection(%val)
{

	if (!%val)
		return;

	GuiEditor.deleteSelection();

}
