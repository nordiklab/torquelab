//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$TLab_ConfigDefault = "tlab/system/settings/defaults.cfg.cs";
$TLab_ConfigUser = "tlab/config.cfg.cs";
$TLab_ConfigCustom_Folder = "tlab/system/settings/custom/";
$TLab_GuiEd_ConfigDefault = "tlab/guiEditor/guiEd_Config.cs";

$TLab_ParamsDlg_Style = "StyleA";

//==============================================================================
// Initialize the config system based on default.cfg.cs if no file specified
function Lab::initConfigSystem(%this,%cfgFile)
{
   if (!isFile(%cfgFile))
      %cfgFile = $TLab_ConfigUser;
      
   %this.initConfigArraySystem();
	//Start by building all the ConfigArray Params
	newSimGroup("LabConfigArrayGroup");
	exec("tlab/system/core/commonSettings.cs");

	LabParamsGroup.clear();
	%this.initCommonParams();

   //Start by loading default which would be overiden (Prevent missing parameters)
	exec($TLab_ConfigDefault);

	//Overwrite the GuiEditor Globals	
	if (isFile($TLab_GuiEd_ConfigDefault))
	   exec($TLab_GuiEd_ConfigDefault);

	if (isFile(%cfgFile))
		exec(%cfgFile);
	else
		exec("tlab/config.cfg.cs");

	//This will update the array with the loaded $Cfg values
	Lab.readAllConfigArray(true,true);		
}
//------------------------------------------------------------------------------
function Lab::saveConfig(%this)
{
	
	export("$Cfg_*", $TLab_ConfigUser, false);
	
	//Save all the modules changes
	Lab.saveModules();
}
//==============================================================================
//------------------------------------------------------------------------------
function Lab::loadDefaultConfig(%this)
{
	exec($TLab_ConfigDefault);
}
//==============================================================================

//==============================================================================
// Set config array values from related $cfg_ globals
//==============================================================================

//------------------------------------------------------------------------------
function Lab::readAllConfigArray(%this,%skipPluginArray,%setEmptyToDefault)
{
	foreach(%array in LabParamsGroup)
		%this.readConfigArray(%array,%skipPluginArray,%setEmptyToDefault);
}

//------------------------------------------------------------------------------

function Lab::readConfigArray(%this,%array,%skipPluginArray,%setEmptyToDefault)
{

	if (!isObject(%array))
	{
		warnLog("Trying to read a config array for an invalid ArrayObject",	%array);
		return;
	}

	if (isObject(%array.pluginObj))
	{
		if (!%array.pluginObj.initialized || %skipPluginArray)
			return;
	}

	%i = 0;

	for(%i = 0; %i < %array.count() ; %i++)
	{
		%field = %array.getKey(%i);

		%value = $Cfg_[%array.cfgData,%field];

		if (%value !$= "")
			setParamFieldValue(%array,%field,%value);
	}

	%array.firstReadDone = true;

}
//------------------------------------------------------------------------------
