//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Config System With Globals
//==============================================================================

//==============================================================================
// Add default setting (Must set beginGroup and endGroup from caller)
function Cfg::get(%this,%field)
{
	if (!isObject(%this.paramArray))
		return "INVALID";

	return %this.paramArray.getCfg(%field);
}
//------------------------------------------------------------------------------

//==============================================================================
// Add default setting (Must set beginGroup and endGroup from caller)
function Cfg::set(%this,%field,%value)
{
	if (!isObject(%this.paramArray))
		return false;

	return %this.paramArray.setCfg(%field,%value);
}
//------------------------------------------------------------------------------
//==============================================================================
// Add default setting (Must set beginGroup and endGroup from caller)
function ParamArray::getCfg(%this,%field)
{
	%value = $Cfg_[%this.cfgData,%field];
	return %value;
}
//------------------------------------------------------------------------------
//==============================================================================
// Add default setting (Must set beginGroup and endGroup from caller)
function ParamArray::setCfg(%this,%field,%value)
{
	$Cfg_[%this.cfgData @"_"@%field] = %value;
	return true;
}
//------------------------------------------------------------------------------

//==============================================================================
// Add default setting (Must set beginGroup and endGroup from caller)
function EditorPlugin::getCfg(%this,%field)
{
	if (!isObject(%this.paramArray))
		return "INVALID";

	return %this.paramArray.getCfg(%field);
}
//------------------------------------------------------------------------------

//==============================================================================
// Add default setting (Must set beginGroup and endGroup from caller)
function EditorPlugin::setCfg(%this,%field,%value)
{
	if (!isObject(%this.paramArray))
		return false;

	return %this.paramArray.setCfg(%field,%value);
}
//------------------------------------------------------------------------------

