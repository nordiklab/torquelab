//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// The ConfigArray object is used to stored differents settings groups inside
// an array. It allow to keep track of all the configs in a universal way.
//==============================================================================

//==============================================================================
// 
function Lab::initConfigArraySystem(%this)
{
   //Create the Group into all ConfigArray will be dropped
   newSimGroup("CfgGroup",LabGroup,"ConfigArrayGroup","ConfigArrayGroup");
   
}
//------------------------------------------------------------------------------

//==============================================================================
// 
function Lab::createConfigArrayObject(%this,%name,%type,%owner)
{
   %arrayName = getUniqueName("cfg_"@%type@"_"@%name);
   //Create the Group into all ConfigArray will be dropped
  %cfgArray = newArrayObject(%arrayName,CfgGroup,"ConfigArray",%name@"Config");
   %cfgArray.owner = %owner;
   %cfgArray.cfgType = %type;//$Cfg_TYPE_name_field;
    %cfgArray.cfgName = %name;//$Cfg_type_NAME_field;
    
   %owner.cfgArray = %cfgArray;
   
   if (%owner.isMethod("setConfigDefault"))
		%owner.setConfigDefault(%cfgArray);
   return %cfgArray;
   
}
//------------------------------------------------------------------------------

function ConfigArray::set(%this, %key,%value,%oldBehavior)
{

	//Check for index, if not, we will add the new value
	%index = %this.getIndexFromKey(%key);

	if (%index $="-1" )	
		%this.add(%key,%value);	
	else	
		%this.setValue(%value,%index);
	
}
function ConfigArray::extra(%this, %key,%value)
{
   %this.extra[%key] = %value;	
}

function CfgGroup::dumpData(%this)
{

   foreach(%cfgArray in %this)
   {
      //Go through all stats and do some update
      for(%i=0; %i<%cfgArray.count(); %i++)
      {         
         %field = %cfgArray.getKey(%i);
         %value = %cfgArray.getValue(%i);
         %default = getField(%value,0);
         %display = getField(%value,1);
         %dataType = getField(%value,2);
         %linkObject = getField(%value,3);
         %updCallback = getField(%value,4);
         %groupId = getField(%value,5);
         %custom = getField(%value,6);
         %group = %cfgArray.group[%groupId];
         info("Variable:",%field,"Default",%default,"Display",%display,"Group",%group,"DataType",%dataType);
         
      }
   }
	
}
