$Cfg_3DView_SelectByGroup = "1";
$Cfg_BrushTool_isEnabled = "1";
$Cfg_BrushTool_pluginOrder = "5";
$Cfg_BrushTool_pluginOrderDefault = "3";
$Cfg_BrushTool_renderMesh = "0";
$Cfg_Common_Camera_cameraDisplayMode = "Standard";
$Cfg_Common_Camera_cameraDisplayType = "6";
$Cfg_Common_Camera_cameraSpeed = "71.2575";
$Cfg_Common_Camera_CamViewEnabled = "0";
$Cfg_Common_Camera_DefaultControlMode = "Player";
$Cfg_Common_Camera_invertXAxis = "0";
$Cfg_Common_Camera_invertYAxis = "0";
$Cfg_Common_Camera_LaunchDefaultAlways = "0";
$Cfg_Common_Camera_LaunchFreeviewAlways = "1";
$Cfg_Common_Camera_LaunchInFreeview = "0";
$Cfg_Common_Camera_MouseMoveMultiplier = "0.31";
$Cfg_Common_Camera_MouseScrollMultiplier = "0.57";
$Cfg_Common_Camera_movementSpeed = "40";
$Cfg_Common_Camera_orthoFOV = "0.681498";
$Cfg_Common_Camera_renderOrthoGrid = "0";
$Cfg_Common_Color_dragRectColor = "220 254 3 255";
$Cfg_Common_Color_faceSelectColor = "213 192 192 255";
$Cfg_Common_Color_gridColor = "85 3 0 203";
$Cfg_Common_Color_gridMinorTickColor = "36 67 15 255";
$Cfg_Common_Color_gridOriginColor = "183 183 183 255";
$Cfg_Common_Color_objectTextColor = "248 254 3 255";
$Cfg_Common_Color_objMouseOverColor = "254 3 9 255";
$Cfg_Common_Color_objMouseOverSelectColor = "197 199 46 254";
$Cfg_Common_Color_objSelectColor = "254 155 3 255";
$Cfg_Common_Color_popupBackgroundColor = "85 3 169 185";
$Cfg_Common_Color_popupTextColor = "84 1 169 255";
$Cfg_Common_Color_raceSelectColor = "243 254 3 255";
$Cfg_Common_Color_selectGridColor = "248 164 76 65";
$Cfg_Common_Color_selectionBoxColor = "18 238 64 255";
$Cfg_Common_Color_uvEditorHandleColor = "254 3 9 255";
$Cfg_Common_General_levelsDirectory = "art/Worlds/TorqueLab";
$Cfg_Common_General_TorsionPath = "C:\\Program Files (x86)\\Torsion\\Torsion.exe";
$Cfg_Common_General_undoLimit = "40";
$Cfg_Common_Gizmo_ = "0.380392 0.996078 0.333333 1";
$Cfg_Common_Gizmo_allowSnapRotations = "1";
$Cfg_Common_Gizmo_allowSnapScale = "1";
$Cfg_Common_Gizmo_alwaysRotationSnap = "1";
$Cfg_Common_Gizmo_gridColor = "243 254 3 248";
$Cfg_Common_Gizmo_gridSize = "10";
$Cfg_Common_Gizmo_planeDim = "40";
$Cfg_Common_Gizmo_renderInfoText = "1";
$Cfg_Common_Gizmo_renderMoveGrid = "1";
$Cfg_Common_Gizmo_renderPlane = "0";
$Cfg_Common_Gizmo_renderPlaneHashes = "1";
$Cfg_Common_Gizmo_renderSolid = "0";
$Cfg_Common_Gizmo_renderWhenUsed = "1";
$Cfg_Common_Gizmo_rotateScalar = "0.8";
$Cfg_Common_Gizmo_rotationSnap = "7.25";
$Cfg_Common_Gizmo_scaleScalar = "0.7";
$Cfg_Common_Gizmo_scaleSnap = "0.4";
$Cfg_Common_Gizmo_screenLength = "89";
$Cfg_Common_Gizmo_snapToGrid = "0";
$Cfg_Common_Grid_forceToGrid = "0";
$Cfg_Common_Grid_forceToGridNoZ = "1";
$Cfg_Common_Grid_gridSize = "1.5";
$Cfg_Common_Grid_gridStep = "1";
$Cfg_Common_Grid_planeDim = "200";
$Cfg_Common_Grid_renderPlane = "0";
$Cfg_Common_Grid_renderPlaneHashes = "1";
$Cfg_Common_Objects_boundingBoxCollision = "1";
$Cfg_Common_Objects_dropAtScreenCenterMax = "100.0";
$Cfg_Common_Objects_dropAtScreenCenterScalar = "0";
$Cfg_Common_Objects_dropType = "toTerrain";
$Cfg_Common_Objects_fadeIcons = "1";
$Cfg_Common_Objects_fadeIconsDist = "12";
$Cfg_Common_Objects_forceLoadDAE = "0";
$Cfg_Common_Objects_gridSnap = "0";
$Cfg_Common_Objects_IgnoreDropSelRotation = "0";
$Cfg_Common_Objects_renderObjHandle = "1";
$Cfg_Common_Objects_renderObjText = "1";
$Cfg_Common_Objects_renderPopupBackground = "1";
$Cfg_Common_Objects_renderSelectionBox = "1";
$Cfg_Common_Objects_showMousePopupInfo = "1";
$Cfg_Common_Objects_snapGround = "0";
$Cfg_Common_Objects_snapSoft = "0";
$Cfg_Common_Objects_snapSoftSize = "2.0";
$Cfg_convexEditor_isEnabled = 1;
$Cfg_convexEditor_pluginOrder = "15";
$Cfg_ConvexEditor_pluginOrderDefault = "16";
$Cfg_DatablockEditor_excludeClientOnlyDatablocks = "1";
$Cfg_datablockEditor_isEnabled = 1;
$Cfg_datablockEditor_pluginOrder = "6";
$Cfg_DatablockEditor_pluginOrderDefault = "12";
$Cfg_DecalEditor_DefaultScale = "1";
$Cfg_decalEditor_isEnabled = 1;
$Cfg_decalEditor_pluginOrder = "5";
$Cfg_DecalEditor_pluginOrderDefault = "8";
$Cfg_Dev_Console_DevLogLevel = "1";
$Cfg_Dev_Console_MouseDragLog = "1";
$Cfg_Dev_Console_MouseDragLogDelay = "0.5";
$Cfg_Dev_Console_MouseLog = "1";
$Cfg_Dev_Console_MouseMoveLog = "1";
$Cfg_Dev_Console_MouseMoveLogDelay = "0.5";
$Cfg_Dev_Console_ShowInfos = "1";
$Cfg_Dev_Console_ShowNotes = "1";
$Cfg_Dev_Console_ShowParamLog = "0";
$Cfg_Dev_Console_TraceLogLevel = "1";
$Cfg_Development_Console_DevLogLevel = "0";
$Cfg_Development_Console_MouseDragLog = "0";
$Cfg_Development_Console_MouseDragLogDelay = "0";
$Cfg_Development_Console_MouseLog = "0";
$Cfg_Development_Console_MouseMoveLog = "0";
$Cfg_Development_Console_MouseMoveLogDelay = "0";
$Cfg_Development_Console_ShowInfos = "1";
$Cfg_Development_Console_ShowNotes = "1";
$Cfg_Development_Console_ShowParamLog = "0";
$Cfg_Development_Console_TraceLogLevel = "0";
$Cfg_ForestEditor_BrushHardness = "2";
$Cfg_ForestEditor_BrushPressure = "2";
$Cfg_ForestEditor_BrushSize = "5";
$Cfg_ForestEditor_DefaultBrush = "BaseBrush";
$Cfg_ForestEditor_DefaultBrushHardness = "50";
$Cfg_ForestEditor_DefaultBrushPressure = "20";
$Cfg_ForestEditor_DefaultBrushSize = "5";
$Cfg_ForestEditor_DefaultGlobalScale = "1";
$Cfg_ForestEditor_GlobalScale = "1";
$Cfg_forestEditor_isEnabled = 1;
$Cfg_forestEditor_pluginOrder = "9";
$Cfg_ForestEditor_pluginOrderDefault = "4";
$Cfg_GameLab_ActionMap = "moveMap";
$Cfg_General_Console_DevLogLevel = "1";
$Cfg_General_Console_ShowInfos = "1";
$Cfg_General_Console_ShowNotes = "1";
$Cfg_General_Console_ShowParamLog = "0";
$Cfg_General_Console_TraceLogLevel = "0";
$Cfg_General_Misc_undoLimit = "40";
$Cfg_GuiEditor__ = "";
$Cfg_GuiEditor_drawBorderLines_fullBox = "1";
$Cfg_GuiEditor_drawGuides_fullBox = "1";
$Cfg_GuiEditor_Editor_lastPath = "G:/GameProjects/AlterVerse/LOCAL/Game/tlab/plugins/materialEditor/gui";
$Cfg_GuiEditor_Editor_previewResolution = "1440 900";
$Cfg_GuiEditor_EngineDevelopment_showEditorGuis = "";
$Cfg_GuiEditor_EngineDevelopment_showEditorProfiles = "";
$Cfg_GuiEditor_EngineDevelopment_toggleIntoEditor = "";
$Cfg_GuiEditor_GuiEditor_lastPath = "F:/Game Workplace/AlterVerse/GameGIT/tlab/ui/guiSystem/SideBar";
$Cfg_GuiEditor_GuiEditor_previewResolution = "1440 900";
$Cfg_GuiEditor_Help_documentationLocal = "";
$Cfg_GuiEditor_Help_documentationReference = "";
$Cfg_GuiEditor_Help_documentationURL = "";
$Cfg_GuiEditor_Library_viewType = "Categorized";
$Cfg_GuiEditor_Rendering_drawBorderLines = "1";
$Cfg_GuiEditor_Rendering_drawGuides = "1";
$Cfg_GuiEditor_Selection_fullBox = "1";
$Cfg_GuiEditor_Snapping_sensitivity = "2";
$Cfg_GuiEditor_Snapping_snap2Grid = "";
$Cfg_GuiEditor_Snapping_snap2GridSize = "";
$Cfg_GuiEditor_Snapping_snapToCanvas = "1";
$Cfg_GuiEditor_Snapping_snapToCenters = "0";
$Cfg_GuiEditor_Snapping_snapToControls = "0";
$Cfg_GuiEditor_Snapping_snapToEdges = "0";
$Cfg_GuiEditor_Snapping_snapToGuides = "0";
$Cfg_Interface_Editor_ToolFrameLocked = "0";
$Cfg_Interface_Editor_ToolFrameSize = "Normal";
$Cfg_ipsEditor_isEnabled = 1;
$Cfg_ipsEditor_pluginOrder = "4";
$Cfg_IpsEditor_pluginOrderDefault = "6";
$Cfg_LockToolbar = 0;
$Cfg_MaterialEditor_AOSuffix = "_s";
$Cfg_MaterialEditor_AutoAddAO = "1";
$Cfg_MaterialEditor_AutoAddComposite = "1";
$Cfg_MaterialEditor_AutoAddMetalness = "1";
$Cfg_MaterialEditor_AutoAddNormal = "1";
$Cfg_MaterialEditor_AutoAddSmoothness = "1";
$Cfg_MaterialEditor_AutoAddSpecular = "1";
$Cfg_MaterialEditor_CompositeSuffix = "_s";
$Cfg_MaterialEditor_DefaultMaterialFile = "10";
$Cfg_MaterialEditor_DiffuseSuffix = "_d";
$Cfg_materialEditor_isEnabled = 1;
$Cfg_MaterialEditor_MapModePBR = "1";
$Cfg_MaterialEditor_MetalnessSuffix = "_s";
$Cfg_MaterialEditor_NormalSuffix = "_n";
$Cfg_MaterialEditor_PBRenabled = "1";
$Cfg_materialEditor_pluginOrder = "2";
$Cfg_MaterialEditor_pluginOrderDefault = "5";
$Cfg_MaterialEditor_PropShowGroup_Advanced = "0";
$Cfg_MaterialEditor_PropShowGroup_Animation = "0";
$Cfg_MaterialEditor_PropShowGroup_Lighting = "1";
$Cfg_MaterialEditor_PropShowGroup_PBR = "0";
$Cfg_MaterialEditor_PropShowGroup_Rendering = "1";
$Cfg_MaterialEditor_PropShowGroup_TextureMaps = 1;
$Cfg_MaterialEditor_PropShowMap_Detail = "0";
$Cfg_MaterialEditor_PropShowMap_detailNormal = "0";
$Cfg_MaterialEditor_PropShowMap_Diffuse = 1;
$Cfg_MaterialEditor_PropShowMap_environment = "0";
$Cfg_MaterialEditor_PropShowMap_Light = "0";
$Cfg_MaterialEditor_PropShowMap_Normal = "1";
$Cfg_MaterialEditor_PropShowMap_Overlay = "0";
$Cfg_MaterialEditor_PropShowMap_specular = "0";
$Cfg_MaterialEditor_PropShowMap_tone = "0";
$Cfg_MaterialEditor_SmoothnessSuffix = "_s";
$Cfg_MaterialEditor_SpecularSuffix = "_s";
$Cfg_MaterialEditor_ThumbnailCountIndex = "3";
$Cfg_meshRoadEditor_isEnabled = 1;
$Cfg_meshRoadEditor_pluginOrder = "10";
$Cfg_MeshRoadEditor_pluginOrderDefault = "9";
$Cfg_NavEditor_backgroundBuild = "1";
$Cfg_navEditor_isEnabled = 1;
$Cfg_NavEditor_playSoundWhenDone = "1";
$Cfg_navEditor_pluginOrder = "7";
$Cfg_NavEditor_pluginOrderDefault = "15";
$Cfg_NavEditor_renderBVTree = "0";
$Cfg_NavEditor_renderMesh = "0";
$Cfg_NavEditor_renderPortals = "0";
$Cfg_NavEditor_saveIntermediates = "1";
$Cfg_NavEditor_spawnClass = "AIPlayer";
$Cfg_NavEditor_spawnDatablock = "DemoPlayerData";
$Cfg_particleEditor_isEnabled = 1;
$Cfg_particleEditor_pluginOrder = "8";
$Cfg_ParticleEditor_pluginOrderDefault = "14";
$Cfg_Plugins_ShapeLab_UseSimplifiedSystem = 1;
$Cfg_RiverEditor_DefaultDepth = "5";
$Cfg_RiverEditor_DefaultNormal = "0 0 1";
$Cfg_RiverEditor_DefaultWidth = "10";
$Cfg_RiverEditor_HoverNodeColor = "255 255 255 255";
$Cfg_RiverEditor_HoverSplineColor = "255 0 0 255";
$Cfg_riverEditor_isEnabled = 1;
$Cfg_riverEditor_pluginOrder = "11";
$Cfg_RiverEditor_pluginOrderDefault = "11";
$Cfg_RiverEditor_SelectedSplineColor = "255 0 255 255";
$Cfg_RoadEditor_borderMovePixelSize = "20";
$Cfg_RoadEditor_borderMoveSpeed = "0.1";
$Cfg_RoadEditor_consoleCircleSegments = "32";
$Cfg_RoadEditor_consoleFillColor = "0 0 0 0";
$Cfg_RoadEditor_consoleFrameColor = "255 0 0 255";
$Cfg_RoadEditor_consoleLineWidth = "1";
$Cfg_RoadEditor_consoleSphereLevel = "1";
$Cfg_RoadEditor_DefaultWidth = "10";
$Cfg_RoadEditor_HoverNodeColor = "255 255 255 255";
$Cfg_RoadEditor_HoverSplineColor = "255 0 0 255";
$Cfg_roadEditor_isEnabled = 1;
$Cfg_RoadEditor_MaterialName = "DefaultDecalRoadMaterial";
$Cfg_roadEditor_pluginOrder = "12";
$Cfg_RoadEditor_pluginOrderDefault = "10";
$Cfg_RoadEditor_SelectedSplineColor = "0 255 0 255";
$Cfg_SceneEditor_AutoCreatePrefab = "1";
$Cfg_SceneEditor_AutoLight_ShowLights = "0";
$Cfg_SceneEditor_AutoLight_ShowShapes = "0";
$Cfg_SceneEditor_AutoPrefabFolder = "art/models/prefabs/";
$Cfg_SceneEditor_AutoPrefabMode = "1";
$Cfg_SceneEditor_CoreGroup = "mgCore";
$Cfg_SceneEditor_CoverPointGroup = "CoverPoint";
$Cfg_SceneEditor_DropLocation = "10";
$Cfg_SceneEditor_EnvironmentGroup = "mgEnvironment";
$Cfg_SceneEditor_GroundCoverDefaultMaterial = "grass1";
$Cfg_SceneEditor_IconWidth = "120";
$Cfg_SceneEditor_isEnabled = 1;
$Cfg_SceneEditor_LightsGroup = "mgLights";
$Cfg_SceneEditor_MiscObjectGroup = "mgMiscObject";
$Cfg_SceneEditor_NavAIGroup = "NavAI";
$Cfg_SceneEditor_NavMeshGroup = "NavMesh";
$Cfg_SceneEditor_NavPathGroup = "NavPath";
$Cfg_SceneEditor_Occluders = "mgOccluders";
$Cfg_SceneEditor_pluginOrder = "1";
$Cfg_SceneEditor_pluginOrderDefault = "1";
$Cfg_SceneEditor_renameInternal = "0";
$Cfg_SceneEditor_SceneObjectsGroup = "mgSceneObjects";
$Cfg_SceneEditor_ShapeGroup = "mgShapeGroup";
$Cfg_SceneEditor_showClassNames = "0";
$Cfg_SceneEditor_showInternalNames = "0";
$Cfg_SceneEditor_showObjectIds = "0";
$Cfg_SceneEditor_showObjectNames = "1";
$Cfg_SceneEditor_SpawnGroup = "PlayerDropPoints";
$Cfg_SceneEditor_TSStaticGroup = "mgMapModels";
$Cfg_SceneEditor_Vehicle = "Vehicle";
$Cfg_ShapeEditor_AdvancedWindowVisible = "1";
$Cfg_ShapeEditor_AnimationBarVisible = "1";
$Cfg_ShapeEditor_BackgroundColor = "0 0 0 0.9";
$Cfg_ShapeEditor_GridDimension = "40 40";
$Cfg_ShapeEditor_GridSize = "0.1";
$Cfg_ShapeEditor_HighlightMaterial = "1";
$Cfg_ShapeEditor_isEnabled = "1";
$Cfg_ShapeEditor_pluginOrder = "13";
$Cfg_ShapeEditor_pluginOrderDefault = "13";
$Cfg_ShapeEditor_PreviewColorBG = "0 0 0 0.9";
$Cfg_ShapeEditor_RenderCollision = "0";
$Cfg_ShapeEditor_RenderMounts = "1";
$Cfg_ShapeEditor_ShowBounds = "0";
$Cfg_ShapeEditor_ShowGrid = "1";
$Cfg_ShapeEditor_ShowNodes = "1";
$Cfg_ShapeEditor_ShowObjBox = "1";
$Cfg_ShapeEditor_SunAmbientColor = "180 180 180 255";
$Cfg_ShapeEditor_SunAngleX = "45";
$Cfg_ShapeEditor_SunAngleZ = "135";
$Cfg_ShapeEditor_SunDiffuseColor = "255 255 255 255";
$Cfg_ShapeLab_AdvancedWindowVisible = "1";
$Cfg_ShapeLab_AnimationBarVisible = "1";
$Cfg_ShapeLab_BackgroundColor = "0.12549 0.00784314 0.996078 1";
$Cfg_ShapeLab_GridDimension = "40 30";
$Cfg_ShapeLab_GridSize = "0.1";
$Cfg_ShapeLab_HighlightMaterial = "1";
$Cfg_ShapeLab_isEnabled = 1;
$Cfg_ShapeLab_pluginOrder = "3";
$Cfg_ShapeLab_pluginOrderDefault = "7";
$Cfg_ShapeLab_PreviewColorBG = "0 0 0 0.9";
$Cfg_ShapeLab_RenderCollision = "0";
$Cfg_ShapeLab_RenderMounts = "1";
$Cfg_ShapeLab_ShowBounds = "0";
$Cfg_ShapeLab_ShowGrid = "1";
$Cfg_ShapeLab_ShowNodes = "1";
$Cfg_ShapeLab_ShowObjBox = "1";
$Cfg_ShapeLab_SunAmbientColor = "180";
$Cfg_ShapeLab_SunAngleX = "45";
$Cfg_ShapeLab_SunAngleZ = "135";
$Cfg_ShapeLab_SunDiffuseColor = "255";
$Cfg_TerrainEditor_adjustHeightVal = "10";
$Cfg_TerrainEditor_BrushPressure = "1";
$Cfg_TerrainEditor_BrushSetHeight = "1";
$Cfg_TerrainEditor_BrushSetHeightRange = "0 100";
$Cfg_TerrainEditor_BrushSize = "2";
$Cfg_TerrainEditor_BrushSoftness = "1";
$Cfg_TerrainEditor_BrushType = "box";
$Cfg_TerrainEditor_DefaultBrushPressure = "43";
$Cfg_TerrainEditor_DefaultBrushSetHeight = "100";
$Cfg_TerrainEditor_DefaultBrushSize = "8";
$Cfg_TerrainEditor_DefaultBrushSoftness = "43";
$Cfg_TerrainEditor_DefaultBrushType = "box";
$Cfg_TerrainEditor_isEnabled = 1;
$Cfg_TerrainEditor_maxBrushSize = "40 40";
$Cfg_TerrainEditor_noiseFactor = "1";
$Cfg_TerrainEditor_pluginOrder = "13";
$Cfg_TerrainEditor_pluginOrderDefault = "2";
$Cfg_TerrainEditor_scaleVal = "1";
$Cfg_TerrainEditor_setHeightVal = "100";
$Cfg_TerrainEditor_slopeMaxAngle = "90";
$Cfg_TerrainEditor_slopeMinAngle = "0";
$Cfg_TerrainEditor_smoothFactor = "0.1";
$Cfg_TerrainEditor_softSelectDefaultFilter = "1.000000 0.833333 0.666667 0.500000 0.333333 0.166667 0.000000";
$Cfg_TerrainEditor_softSelectFilter = "1.000000 0.833333 0.666667 0.500000 0.333333 0.166667 0.000000";
$Cfg_TerrainEditor_softSelectRadius = "50";
$Cfg_TerrainPainter_adjustHeightVal = "10";
$Cfg_TerrainPainter_DefaultBrushPressure = "50";
$Cfg_TerrainPainter_DefaultBrushSize = "2";
$Cfg_TerrainPainter_DefaultBrushSlopeMax = "90";
$Cfg_TerrainPainter_DefaultBrushSlopeMin = "0";
$Cfg_TerrainPainter_DefaultBrushSoftness = "50";
$Cfg_TerrainPainter_DefaultBrushType = "box";
$Cfg_TerrainPainter_isEnabled = 1;
$Cfg_TerrainPainter_noiseFactor = "1";
$Cfg_TerrainPainter_pluginOrder = "14";
$Cfg_TerrainPainter_pluginOrderDefault = "3";
$Cfg_TerrainPainter_scaleVal = "1";
$Cfg_TerrainPainter_setHeightVal = "100";
$Cfg_TerrainPainter_slopeMaxAngle = "90";
$Cfg_TerrainPainter_slopeMinAngle = "0";
$Cfg_TerrainPainter_smoothFactor = "0.1";
$Cfg_TerrainPainter_softSelectDefaultFilter = "1.000000 0.833333 0.666667 0.500000 0.333333 0.166667 0.000000";
$Cfg_TerrainPainter_softSelectFilter = "1.000000 0.833333 0.666667 0.500000 0.333333 0.166667 0.000000";
$Cfg_TerrainPainter_softSelectRadius = "50";
$Cfg_Common_Saving_AutoSaveDelay = "5";
$Cfg_TLab_Class_Inspector_LogLevel = 0;
$Cfg_TLab_defaultGui = "GuiGameMenu";
$Cfg_TLab_GuiEditorLoaded = 1;
$Cfg_TLab_LeftFrameMin = "123";
$Cfg_TLab_Object_DropTypes = "atOrigin atCamera atCameraRot belowCamera screenCenter atCentroid toTerrain belowSelection";
$Cfg_TLab_PrefabAutoMode = "Level Object Folder";
$Cfg_TLab_RightFrameMin = "123";
$Cfg_TLab_Theme = "Laborean";
$Cfg_TLab_ThemePath = "tlab/themes/Laborean/";
$Cfg_TorsionPath = "40";
$Cfg_UI_Editor_SideFrameWidth = "220";
$Cfg_UI_Editor_ToolFrameWidth = "280";
$Cfg_UI_Frame_SideFrameWidth = "220";
$Cfg_UI_Frame_ToolFrameWidth = "280";
$Cfg_UI_Menu_UseNativeMenu = 0;
$Cfg_WorldEditor_Misc_defaultHandle = "tlab/art/icons/set01/default/DefaultHandle";
$Cfg_WorldEditor_Misc_documentationLocal = "../../../Documentation/Official";
$Cfg_WorldEditor_Misc_documentationReference = "../../../Documentation/Torque";
$Cfg_WorldEditor_Misc_documentationURL = "http://www.garagegames.com/products/torque-3d/documentation/user";
$Cfg_WorldEditor_Misc_forumURL = "http://www.garagegames.com/products/torque-3d/forums";
$Cfg_WorldEditor_Misc_lockedHandle = "tlab/art/icons/set01/default/LockedHandle";
$Cfg_WorldEditor_Misc_planeDim = "500";
$Cfg_WorldEditor_Misc_renderPlane = "0";
$Cfg_WorldEditor_Misc_renderPlaneHashes = "0";
$Cfg_WorldEditor_Misc_selectHandle = "tlab/art/icons/set01/default/SelectHandle";
