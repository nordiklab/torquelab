//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function LabCfg::loadSelectedCfg(%this)
{
	%fileText = LPD_ConfigNameEdit.getText();
	%filename = fileBase(%fileText)@".cfg.cs";
	%cfgFile = $TLab_ConfigCustom_Folder@%filename;

	if (fileBase(%fileText) $= "config")
		%cfgFile = "tlab/config.cfg.cs";

	if (!isFile(%cfgFile))
		return;

	Lab.initConfigSystem(%cfgFile);
}
//------------------------------------------------------------------------------
//==============================================================================
// Add default setting (Must set beginGroup and endGroup from caller)
function LabCfg::loadBaseCfg(%this)
{
	%cfg = "tlab/config.cfg.cs";
	%fileName = "config.cfg.cs";

	if (!isFile(%cfg))
	{
		%fileName = "default.cfg.cs";
		%cfg = $TLab_ConfigDefault;
	}

	if (!isFile(%cfg))
	{
		LPD_ConfigNameEdit.setText("defaults.cfg.cs");
		return;
	}

	LPD_ConfigNameEdit.setText(%fileName);	
	Lab.initConfigSystem();
}
//------------------------------------------------------------------------------

//==============================================================================
// Add default setting (Must set beginGroup and endGroup from caller)
function LabCfg::saveSelectedCfg(%this,%forced)
{
	%fileText = LPD_ConfigNameEdit.getText();

	if (%fileText $= "default" && !%forced)
	{
		LabMsgYesNo("Warning! Overwritting default config","You are about to overwrite the default config file which can cause problem if some settings are corrupted. We recommend to not" SPC
		            "overwrite the file and use a custom file name instead. Do you want to proceed with overwritting default config?","LabCfg.saveSelectedCfg(true);","");
		return;
	}

	%filename = fileBase(%fileText)@".cfg.cs";

	//If this is the main config, store it in root
	if (fileBase(%fileText) $= "config")
		%cfgFile = "tlab/"@%filename;
	else
		%cfgFile = $TLab_ConfigCustom_Folder@%filename;

	LabParamsDlg.currentCfgFile = fileBase(%fileText);
	export("$Cfg_*", %cfgFile, false);
	//export("$CfgDefault_*", "tlab/system/settings/defaults.cfg.cs", false);
	Lab.getAllConfigs();
}
//------------------------------------------------------------------------------
//==============================================================================
// Add default setting (Must set beginGroup and endGroup from caller)
function Lab::getAllConfigs(%this)
{
	LPD_ConfigNameMenu.clear();
	//Start with main config
	LPD_ConfigNameMenu.add("config",%cid++);

	for(%file = findFirstFile("tlab/system/core/settings/cfgs/*.cfg.cs"); %file !$= ""; %file = findNextFile("tlab/system/core/settings/cfgs/*.cfg.cs"))
	{
		%fileBase = fileBase(fileBase(%file));
		//if (%fileBase $= "default")
		//	continue;
		%fileName = %fileBase;
		LPD_ConfigNameMenu.add(%fileName,%cid++);
	}

	if (LabParamsDlg.currentCfgFile $= "")
		LabParamsDlg.currentCfgFile = "config";

	LPD_ConfigNameEdit.setText(LabParamsDlg.currentCfgFile);
}
//------------------------------------------------------------------------------

