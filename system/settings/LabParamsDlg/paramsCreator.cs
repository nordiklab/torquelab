//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Prepare categorized params for the LabParamsDlg
//==============================================================================

//==============================================================================
// New ParamsArray creation system (to replace newParamsArray over time)
function Lab::createParamsCategory(%this,%category,%categoryDisplay)
{
	if (%category !$= "")
		LabParams.categoryDisplay[%category] = (%categoryDisplay !$= "") ? %categoryDisplay : %category;

}
//==============================================================================
// New ParamsArray creation system (to replace newParamsArray over time)
function Lab::createParamsArray(%this,%category,%typeData,%container)
{   
	if (%category $= "" || %typeData $= "")
	{
		warnLog("You need to specify a category and group for each params array");
		return;
	}
 
    %categoryDisplay = LabParams.categoryDisplay[%category];

	%type = getField(%typeData,0);
	%typeDisplay =  getField(%typeData,1);

	if (%typeDisplay $= "")
		%typeDisplay = %type @ " settings";

	%fullname = %category@"_"@%type;

	if (!isObject(%container))
		%container = %fullname@"ParamStack";

	%arrayName = getUniqueName("ar_"@%fullname);

	%array = newArrayObject(%arrayName,LabParamsGroup,"ParamArray");
	%array.internalName = %fullname;
	%array.displayName = %typeDisplay;
	%array.container = %container;
	%array.paramCallback = "Lab.onParamBuild";

	%array.group = %category;
	%array.category = %category;
	%array.type = %type;
	%array.set = %fullname;
	%array.useNewSystem = true;

//==============================================================================
//TO BE CHECKED
	%array.fields = "Default Title Type Options syncObjs";
	%array.syncObjsField = 4;

//==============================================================================
	//If no cfgObject supplied, simply use the new array as object

	%array.cfgObject = %array;
	%array.groupLink = %fullname;
	%array.cfgData = %fullname;

	%array.prefBase = "$Cfg_"@%fullname@"_";

	%prefGroup = %fullname;

	%array.prefGroup = %prefGroup;
	%array.updateFunc = "LabParams.updateParamArrayCtrl";

	return %array;
}
//------------------------------------------------------------------------------
//==============================================================================
// Basic Params using helpers system called here to make sure widgets are loaded
function Lab::createBaseParamsArray(%this,%paramName,%container)
{
	%paramWords = strReplace(%paramName,"_"," ");
	%cat =  getWord(%paramWords,0);
	%type =  getWord(%paramWords,1);

	if (%type $= "")
	{
		%cat =  "base";
		%type =  getWord(%paramWords,0);
	}

	%arCfg = Lab.createParamsArray(%cat,%type,%container);

	if (!isObject(wParams_StyleA))
		exec("tlab/ui/gui/LabWidgetsGui.gui");

	%arCfg.style = "StyleA";
	return %arCfg;
}

//==============================================================================
//Initialize plugin data
function Lab::onParamBuild(%this,%array,%field,%paramData)
{
}
//------------------------------------------------------------------------------
//==============================================================================
//Initialize plugin data
function Lab::onParamPluginBuild(%this,%array,%field,%paramData)
{
	%plugin = %array.pluginObj;
	%cfgValue = %plugin.getCfg(%field);

	if (%cfgValue $= "")
		%plugin.setCfg(%field,%paramData.Default);
}
//------------------------------------------------------------------------------
