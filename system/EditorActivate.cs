//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function Lab::resetEditor(%this)
{
	%this.setEditor(%this.currentEditor);
	Lab.onEditorOpen("World");
}

//==============================================================================
function Lab::setEditor(%this, %newEditor)
{
	if (!%newEditor.isEnabled)
		return;

	//First make sure the new editor is valid
	if (%newEditor $= "" || !isObject(%newEditor))
	{
		%newEditor = Lab.defaultPlugin;

		if (!isObject(%newEditor))
		{
			warnLog("Set editor called for invalid editor plugin:",%newEditor);
			return;
		}
	}

	if (!%newEditor.initialized)
	{
		Lab.loadPlugin(%newEditor);
		%this.schedule(0,"setEditor",%newEditor);
		return;
	}

	// If we have a special set editor function, run that instead
	if (%newEditor.isMethod("setEditorFunction"))
	{
		%pluginActivated= %newEditor.setEditorFunction();

		if (!%pluginActivated)
		{
			warnLog("Plugin failed to activate, keeping the current plugin active");
			return;
		}
	}

	//Make sure currentEditor exist for the next checks
	if (isObject(%this.currentEditor))
	{
		//Desactivated current editor if activated and not same as new
		if (%this.currentEditor.getId() != %newEditor.getId() && %this.currentEditor.isActivated)
			%this.currentEditor.onDeactivated(%newEditor);

		//Store current OrthoFOV view to set same in new editor
		if (isObject(%this.currentEditor.editorGui))
			%this.orthoFOV = %this.currentEditor.editorGui.getOrthoFOV();
	}

	%this.syncEditor(%newEditor);
	%this.currentEditor = %newEditor;

	%this.currentEditor.onActivated();
	Lab.activePluginName = %this.currentEditor.displayName;

}
//------------------------------------------------------------------------------
//==============================================================================
// Synchronize tht various UI relative to current Editor
function Lab::syncEditor(%this,%newEditor)
{
	if (%newEditor $= "")
		%newEditor = %this.currentEditor;

	if (!isObject(%newEditor))
	{
		warnLog("Trying to sync Toolbar for invalid editor:",%newEditor);
		return;
	}

	// Sync with menu bar
	%menu = WEMenuEditors;
	%count = %menu.getItemCount();

	for(%i = 0; %i < %count; %i++)
	{
		%pluginObj = getField(%menu.item[%i], 2);

		if (%pluginObj $= %newEditor)
		{
			%menu.checkRadioItem(0, %count, %i);
			break;
		}
	}

	%icon = LabPluginArray.findObjectByInternalName(%newEditor.plugin);

	if (isObject(%icon))
		%icon.setStateOn(1); //Grouped radio button => Other will be turned off automatically

	%paletteName = strreplace(%newEditor, "Plugin", "Palette");

	if (%newEditor.customPalette !$= "")
	{
		%paletteName = %newEditor.customPalette;
	}

	Lab.resizeLayoutModules();
	Lab.togglePluginPalette(%paletteName,%newEditor);

}
//------------------------------------------------------------------------------
