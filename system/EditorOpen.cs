//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function Editor::open(%this,%ignorePreviousGui)
{

	$LabEditor_RestoreCursor = Canvas.isCursorOn();

	// prevent the mission editor from opening while the GuiEditor is open.
	if (isObject(GuiEditorGui))
	{
		if (Canvas.getContent() == GuiEditorGui.getId())
			return;

	}

	//Store current content and set EditorGui as content
	if (!%ignorePreviousGui)
		EditorGui.previousGui = Canvas.getContent();

	Canvas.setContent(EditorGui);
}
//------------------------------------------------------------------------------
//==============================================================================
// Call after the onWake method to complete the launch
function Editor::postWakeInit(%this,%ignorePreviousGui)
{
	Lab.onWorldEditorOpen();
	$EClient = LocalClientConnection;

	if (!LabEditor.isInitialized)
		Lab.InitialGuiSetup();

	//EditManager call to set the Editor Enabled
	%this.editorEnabled();

	if (Lab.currentEditor $= "" || !isObject(Lab.currentEditor))
		Lab.currentEditor = Lab.defaultPlugin;

	Lab.setEditor(Lab.currentEditor, true);

	//The default menu seem to be create somewhere between setCOntent and here
	if (!$Cfg_UI_Menu_UseNativeMenu && isObject(Lab.menuBar))
	{
		flog("Editor::open removeMenu");
		Lab.menuBar.removeFromCanvas();
	}

	SceneBrowserTree.currentSet = "";
	Lab.SetAutoSave();

}
//------------------------------------------------------------------------------
//==============================================================================
// EditorGui OnWake -> When the EditorGui is rendered
function EditorGui::onWake(%this)
{

	Lab.onWorldEditorWake();
	activatePackage(TorqueLabPackage);
	Lab.setInitialCamera();
	//EHWorldEditor.setStateOn( 1 );
	startFileChangeNotifications();

	%slashPos = 0;

	while(strpos($Server::MissionFile, "/", %slashPos) != -1)
		%slashPos = strpos($Server::MissionFile, "/", %slashPos) + 1;

	%levelName = getSubStr($Server::MissionFile, %slashPos, 99);

	if (%levelName !$= Lab.levelName)
		%this.onNewLevelLoaded(%levelName);

	Scene.setDropType();
	$TLabWorldReady = true;
	//if ($Cfg_EditorLab_ShowSideBarOnWake)
	Lab.schedule(0,"launchGuiSetup","true");

	skipPostFx(false);

	Editor.postWakeInit();
}
//------------------------------------------------------------------------------
//==============================================================================
// Called when we have been set as the content and onWake has been called
function EditorGui::onSetContent(%this, %oldContent)
{
	//Lab.attachMenus();
	//Lab.schedule(500,"openSidebar","true");
}
//------------------------------------------------------------------------------
