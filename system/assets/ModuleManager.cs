//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function Editor::open(%this,%ignorePreviousGui)
{

ModuleDatabase.setModuleExtension("module");
ModuleDatabase.scanModules( "data", false );
ModuleDatabase.LoadGroup( "Game" );
}
//------------------------------------------------------------------------------
//==============================================================================
/*

 addProtectedField("ModuleId", TypeString, Offset(mModuleId, ModuleDefinition), &defaultProtectedSetFn, &defaultProtectedGetFn, "");

    /// Module configuration.
    addProtectedField( "ModuleId", TypeString, Offset(mModuleId, ModuleDefinition), &setModuleId, &defaultProtectedGetFn, "A unique string Id for the module.  It can contain any characters except a comma or semi-colon (the asset scope character)." );
    addProtectedField( "VersionId", TypeS32, Offset(mVersionId, ModuleDefinition), &setVersionId, &defaultProtectedGetFn, "The version Id.  Breaking changes to a module should use a higher version Id." );
    addProtectedField( "BuildId", TypeS32, Offset(mBuildId, ModuleDefinition), &setBuildId, &defaultProtectedGetFn, &writeBuildId, "The build Id.  Non-breaking changes to a module should use a higher build Id.  Optional: If not specified then the build Id will be zero." );
    addProtectedField( "enabled", TypeBool, Offset(mEnabled, ModuleDefinition), &setEnabled, &defaultProtectedGetFn, &writeEnabled, "Whether the module is enabled or not.  When disabled, it is effectively ignored.  Optional: If not specified then the module is enabled." );
    addProtectedField( "Synchronized", TypeBool, Offset(mSynchronized, ModuleDefinition), &setSynchronized, &defaultProtectedGetFn, &writeSynchronized, "Whether the module should be synchronized or not.  Optional: If not specified then the module is not synchronized." );
    addProtectedField( "Deprecated", TypeBool, Offset(mDeprecated, ModuleDefinition), &setDeprecated, &defaultProtectedGetFn, &writeDeprecated, "Whether the module is deprecated or not.  Optional: If not specified then the module is not deprecated." );
    addProtectedField( "CriticalMerge", TypeBool, Offset(mCriticalMerge, ModuleDefinition), &setDeprecated, &defaultProtectedGetFn, &writeCriticalMerge, "Whether the merging of a module prior to a restart is critical or not.  Optional: If not specified then the module is not merge critical." );
    addProtectedField( "OverrideExistingObjects", TypeBool, Offset(mOverrideExistingObjects, ModuleDefinition), &setOverrideExistingObjects, &defaultProtectedGetFn, &writeOverrideExistingObjects, "Controls if when this module is loaded and the create function is executed, it will replace existing objects that share names or not.");
    addProtectedField( "Description", TypeString, Offset(mModuleDescription, ModuleDefinition), &setModuleDescription, &defaultProtectedGetFn, &writeModuleDescription, "The description typically used for debugging purposes but can be used for anything." );
    addProtectedField( "Author", TypeString, Offset(mAuthor, ModuleDefinition), &setAuthor, &defaultProtectedGetFn, &writeAuthor, "The author of the module." );
    addProtectedField( "Group", TypeString, Offset(mModuleGroup, ModuleDefinition), &setModuleGroup, &defaultProtectedGetFn, "The module group used typically when loading modules as a group." );
    addProtectedField( "Type", TypeString, Offset(mModuleType, ModuleDefinition), &setModuleType, &defaultProtectedGetFn, &writeModuleType, "The module type typically used to distinguish modules during module enumeration.  Optional: If not specified then the type is empty although this can still be used as a pseudo 'global' type for instance." );
    addProtectedField( "Dependencies", TypeString, Offset(mDependencies, ModuleDefinition), &setDependencies, &getDependencies, &writeDependencies, "A comma-separated list of module Ids/VersionIds (<ModuleId>=<VersionId>,<ModuleId>=<VersionId>,etc) which this module depends upon. Optional: If not specified then no dependencies are assumed." );
    addProtectedField( "ScriptFile", TypeString, Offset(mScriptFile, ModuleDefinition), &setScriptFile, &defaultProtectedGetFn, &writeScriptFile, "The name of the script file to compile when loading the module.  Optional." );
    addProtectedField( "CreateFunction", TypeString, Offset(mCreateFunction, ModuleDefinition), &setCreateFunction, &defaultProtectedGetFn, &writeCreateFunction, "The name of the function used to create the module.  Optional: If not specified then no create function is called." );
    addProtectedField( "DestroyFunction", TypeString, Offset(mDestroyFunction, ModuleDefinition), &setDestroyFunction, &defaultProtectedGetFn, &writeDestroyFunction, "The name of the function used to destroy the module.  Optional: If not specified then no destroy function is called." );
    addProtectedField( "AssetTagsManifest", TypeString, Offset(mAssetTagsManifest, ModuleDefinition), &setAssetTagsManifest, &defaultProtectedGetFn, &writeAssetTagsManifest, "The name of tags asset manifest file if this module contains asset tags.  Optional: If not specified then no asset tags will be found for this module.  Currently, only a single asset tag manifest should exist." );
    addProtectedField( "ScopeSet", TypeS32, Offset( mScopeSet, ModuleDefinition ), &defaultProtectedNotSetFn, &getScopeSet, &defaultProtectedNotWriteFn, "The scope set used to control the lifetime scope of objects that the module uses.  Objects added to this set are destroyed automatically when the module is unloaded." );

    /// Module location (Read-only).
    addProtectedField( "ModulePath", TypeString, Offset(mModulePath, ModuleDefinition), &defaultProtectedNotSetFn, &defaultProtectedGetFn, &defaultProtectedNotWriteFn, "The path of the module.  This is read-only and is available only after the module has been registered by a module manager." );
    addProtectedField( "ModuleFile", TypeString, Offset(mModuleFile, ModuleDefinition), &defaultProtectedNotSetFn, &defaultProtectedGetFn, &defaultProtectedNotWriteFn, "The file of the module.  This is read-only and is available only after the module has been registered by a module manager." );
    addProtectedField( "ModuleFilePath", TypeString, Offset(mModuleFilePath, ModuleDefinition), &defaultProtectedNotSetFn, &defaultProtectedGetFn, &defaultProtectedNotWriteFn, "The file-path of the module definition.  This is read-only and is available only after the module has been registered by a module manager." );
    addProtectedField( "ModuleScriptFilePath", TypeString, Offset(mModuleScriptFilePath, ModuleDefinition), &defaultProtectedNotSetFn, &defaultProtectedGetFn, &defaultProtectedNotWriteFn, "The file-path of the script-file referenced in the module definition.  This is read-only and is available only after the module has been registered by a module manager." );

    /// Misc.
    addProtectedField( "Signature", TypeString, 0, &defaultProtectedNotSetFn, &getSignature, &defaultProtectedNotWriteFn, "A unique signature of the module definition based upon its Id, version and build.  This is read-only and is available only after the module has been registered by a module manager." );
}

void ModuleManager::addListener  ( SimObject *  pListener  )   

Module notifications. 

bool ModuleManager::canMergeModules  ( const char *  pMergeSourcePath  )   

StringTableEntry ModuleManager::copyModule  ( ModuleDefinition *  pSourceModuleDefinition,  
  const char *  pTargetModuleId,  
  const char *  pTargetPath,  
  const bool  useVersionPathing   
 )    

Module synchronization. 

ModuleManager::DECLARE_CONOBJECT  ( ModuleManager    )   

Declare Console Object. 

ModuleDefinition* ModuleManager::findLoadedModule  ( const char *  pModuleId  )   

ModuleDefinition* ModuleManager::findModule  ( const char *  pModuleId,  
  const U32  versionId   
 )    

Module type enumeration. 

void ModuleManager::findModules  ( const bool  loadedOnly,  
  typeConstModuleDefinitionVector &  moduleDefinitions   
 )    

void ModuleManager::findModuleTypes  ( const char *  pModuleType,  
  const bool  loadedOnly,  
  typeConstModuleDefinitionVector &  moduleDefinitions   
 )    

static void ModuleManager::initPersistFields  (   )  [static] 

Register dynamic fields in a subclass of ConsoleObject. 

See also:
addField(), addFieldV(), addDeprecatedField(), addGroup(), endGroup() 
Reimplemented from SimObject.

bool ModuleManager::isModuleMergeAvailable  ( void    )  const [inline] 

Module updates. 

bool ModuleManager::loadModuleExplicit  ( const char *  pModuleId,  
  const U32  versionId = 0   
 )    

bool ModuleManager::loadModuleGroup  ( const char *  pModuleGroup  )   

Module (un)loading. 

bool ModuleManager::mergeModules  ( const char *  pMergeTargetPath,  
  const bool  removeMergeDefinition,  
  const bool  registerNewModules   
 )    

virtual bool ModuleManager::onAdd  (   )  [virtual] 

SimObject overrides. 

Reimplemented from SimObject.

virtual void ModuleManager::onDeleteNotify  ( SimObject *  object  )  [virtual] 

Called when a SimObject is deleted. 

When you are on the notification list for another object and it is deleted, this method is called. 

Reimplemented from SimObject.

virtual void ModuleManager::onRemove  (   )  [virtual] 

Called when the object is removed from the sim. 

Reimplemented from SimObject.

bool ModuleManager::registerModule  ( const char *  pModulePath,  
  const char *  pModuleFile   
 )    

Module unregister. 

void ModuleManager::removeListener  ( SimObject *  pListener  )   

bool ModuleManager::renameModule  ( ModuleDefinition *  pSourceModuleDefinition,  
  const char *  pNewModuleName   
 )    

Editing modules. 

bool ModuleManager::scanModules  ( const char *  pPath,  
  const bool  rootOnly = false   
 )    

Module discovery. 

void ModuleManager::setIgnoreLoadedGroups  ( bool  doIgnore  )  [inline] 

bool ModuleManager::setModuleExtension  ( const char *  pExtension  )   

Module definitions. 

bool ModuleManager::synchronizeDependencies  ( ModuleDefinition *  pRootModuleDefinition,  
  const char *  pTargetDependencyPath   
 )    

bool ModuleManager::unloadModuleExplicit  ( const char *  pModuleId  )   

bool ModuleManager::unloadModuleGroup  ( const char *  pModuleGroup  )   

bool ModuleManager::unregisterModule  ( const char *  pModuleId,  
  const U32  versionId   
 )    

*/
