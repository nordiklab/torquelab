//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Make sure all GUIs are fine once the editor is launched
function GuiFrameSetCtrl::togglePanel(%this,%panelIntName)
{
	%panel = %this.findObjectByInternalName(%panelIntName);

	if (!isObject(%panel))
	{
		return;
	}

	if (%panel.visible)
	{
		//Hide the panel
		%index = %this.getObjectIndex(%panel);
		%rows = %this.rows;
		%panel.myIndex = %index;
		%panel.myRow = %rows;
		%this.lastRows[getWordCount(%rows)] = %rows;
		%newRows = removeWord(%rows,getWordCount(%rows)-1);
		hide(%panel);
		%this.pushToBack(%panel);
		%this.rows = %newRows;
	}
	else
	{
		%index = %panel.myIndex;
		%newRows = %this.lastRows[getWordCount(%this.rows)+1];
		%this.rows = %newRows;
		show(%panel);
		%this.reorderChild(%panel,%this.getObject(%index));
	}

	%this.updateSizes();
}
//------------------------------------------------------------------------------
