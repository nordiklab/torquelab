//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Handle the escape bind
function Lab::pathCopy(%this,%srcFile,%tgtFile,%noOverwrite,%unique)
{
   return doPathCopy(%srcFile,%tgtFile,%noOverwrite,%unique);	
}
//------------------------------------------------------------------------------

//==============================================================================
// Handle the escape bind
function Lab::backupFileHome(%this,%file,%unique)
{
   return backupFileHome(%file,%unique,"TorqueLab/backup");
}
//------------------------------------------------------------------------------
//==============================================================================
// Handle the escape bind
function Lab::backupFileUser(%this,%file,%unique)
{
   return backupFileUser(%file,%unique,"TorqueLab/backup");
	
}
//------------------------------------------------------------------------------


