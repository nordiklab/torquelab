//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//------------------------------------------------------------------------------
// Generate or regenerate the Gui Editor Menu
//------------------------------------------------------------------------------
// Functions:
//   - Lab::buildGuiMenus();
//------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------

/// Create the Gui Editor menu bar.
function Lab::buildGuiMenus(%this)
{
   if (!isObject(GuiEdMenuArea))
      return;
   if (isObject(GuiEdMenu))
	   GuiEdMenu.destroy();
	
   Lab.GuiEdMap = newActionMap(GuiEdMenuMap,true);
   %actionMap = GuiEdMenuMap;
	new GuiMenuBar(GuiEdMenu)
	{
		dynamicItemInsertPos = 3;
		extent = "500 22";
		minExtent = "500 22";
		horizSizing = "width";
		profile = "ToolsMenuBarProfile";
		superClass = "EditorMenuBar";
		actionMap = %actionMap;
      
	};
	
	//GuiEdMenuArea.add(GuiEdMenu);
    GuiEdMenuArea.margin = "0 0 0 0";
    GuiEdMenuArea.visible = 1;

	%this.MenuBar["Gui"] = GuiEdMenu;

   delObj(GuiEdMenu_File);
	new PopupMenu(GuiEdMenu_File)
	{
		superClass = "EditorMenuPopup";
		barTitle = "File";
		internalName = "FileMenu";
      actionMap = %actionMap;
      
		item[0] = "New Gui..." TAB $KeyCtrl SPC "N" TAB GuiEditCanvas @ ".create();";
		item[1] = "Open..." TAB $KeyCtrl SPC "O" TAB GuiEditCanvas @ ".open();";
		item[2] = "Save" TAB $KeyCtrl SPC "S" TAB GuiEditCanvas @ ".save( false, true );";
		item[3] = "Save As..." TAB $KeyCtrl @ "-Shift S" TAB GuiEditCanvas @ ".save( false );";
		item[4] = "Save Selected As..." TAB $KeyCtrl @ "-Alt S" TAB GuiEditCanvas @ ".save( true );";
		item[5] = "-";
		item[6] = "Revert Gui" TAB "" TAB GuiEditCanvas @ ".revert();";
		item[7] = "Add Gui From File..." TAB "" TAB GuiEditCanvas @ ".append();";
		item[8] = "-";
		item[9] = "Open Gui File in Torsion" TAB "" TAB GuiEditCanvas @".openInTorsion();";
		item[10] = "-";
		item[11] = "Close Editor" TAB "" TAB "toggleGuiEdit(true);";//GuiEditCanvas @ ".quit();";
		item[12] = "Quit" TAB $KeyCtrl SPC "Q" TAB "quit();";
	};
	GuiEdMenu.insert(GuiEdMenu_File,0);
	delObj(GuiEdMenu_Edit);
	new PopupMenu(GuiEdMenu_Edit)
	{
		superClass = "EditorMenuPopup";
		barTitle = "Edit";
		internalName = "EditMenu";
      actionMap = %actionMap;
      
		item[0] = "Undo" TAB $KeyCtrl SPC "Z" TAB "GuiEditor.undo();";
		item[1] = "Redo" TAB $KeyRedo TAB "GuiEditor.redo();";
		item[2] = "-";
		item[3] = "Cut" TAB $KeyCtrl SPC "X" TAB "GuiEditor.saveSelection(); GuiEditor.deleteSelection();";
		item[4] = "Copy" TAB $KeyCtrl SPC "C" TAB "GuiEditor.saveSelection();";
		item[5] = "Paste" TAB $KeyCtrl SPC "V" TAB "GuiEditor.loadSelection();";
		item[6] = "-";
		item[7] = "Select All" TAB $KeyCtrl SPC "A" TAB "GuiEditor.selectAll();";
		item[8] = "Deselect All" TAB $KeyCtrl SPC "D" TAB "GuiEditor.clearSelection();";
		item[9] = "Select Parent(s)" TAB $KeyCtrl @ "-Alt Up" TAB "GuiEditor.selectParents();";
		item[10] = "Select Children" TAB $KeyCtrl @ "-Alt Down" TAB "GuiEditor.selectChildren();";
		item[11] = "Add Parent(s) to Selection" TAB $KeyCtrl @ "-Alt-Shift Up" TAB "GuiEditor.selectParents( true );";
		item[12] = "Add Children to Selection" TAB $KeyCtrl @ "-Alt-Shift Down" TAB "GuiEditor.selectChildren( true );";
		item[13] = "Select..." TAB "" TAB "GuiEditorSelectDlg.toggleVisibility();";
		item[14] = "-";
		item[15] = "Lock/Unlock Selection" TAB $KeyCtrl SPC "L" TAB "GuiEditor.toggleLockSelection();";
		item[16] = "Hide/Unhide Selection" TAB $KeyCtrl SPC "H" TAB "GuiEditor.toggleHideSelection();";
		item[17] = "-";
		item[18] = "Group Selection" TAB $KeyCtrl SPC "G" TAB "GuiEditor.groupSelected();";
		item[19] = "Ungroup Selection" TAB $KeyCtrl @ "-Shift G" TAB "GuiEditor.ungroupSelected();";
		item[20] = "-";
		item[21] = "Full Box Selection" TAB "" TAB "GuiEditor.toggleFullBoxSelection();";
		item[22] = "-";
		item[23] = "Grid Size" TAB $KeyCtrl SPC "," TAB "GuiEditor.showPrefsDialog();";
	};
	GuiEdMenu.insert(GuiEdMenu_Edit);
delObj(GuiEdMenu_Layout);
	new PopupMenu(GuiEdMenu_Layout)
	{
		superClass = "EditorMenuPopup";
		barTitle = "Layout";
		internalName = "LayoutMenu";
      actionMap = %actionMap;
      
		item[%itemId = 0] = "Align Left" TAB $KeyCtrl SPC "Left" TAB "GuiEditor.Justify(0);";
		item[%itemId++] = "Center Horizontally" TAB "" TAB "GuiEditor.Justify(1);";
		item[%itemId++] = "Align Right" TAB $KeyCtrl SPC "Right" TAB "GuiEditor.Justify(2);";
		item[%itemId++] = "-";
		item[%itemId++] = "Align Top" TAB $KeyCtrl SPC "Up" TAB "GuiEditor.Justify(3);";
		item[%itemId++] = "Center Vertically" TAB "" TAB "GuiEditor.Justify(7);";
		item[%itemId++] = "Align Bottom" TAB $KeyCtrl SPC "Down" TAB "GuiEditor.Justify(4);";
		item[%itemId++] = "-";
		item[%itemId++] = "Space Vertically" TAB "" TAB "GuiEditor.Justify(5);";
		item[%itemId++] = "Space Horizontally" TAB "" TAB "GuiEditor.Justify(6);";
		item[%itemId++] = "-";
		item[%itemId++] = "Fit into Parent(s)" TAB "" TAB "GuiEditor.fitIntoParents();";
		item[%itemId++] = "Fit Width to Parent(s)" TAB "" TAB "GuiEditor.fitIntoParents( true, false );";
		item[%itemId++] = "Fit Height to Parent(s)" TAB "" TAB "GuiEditor.fitIntoParents( false, true );";
		item[%itemId++] = "Force into Parent" TAB "Alt g" TAB "Lab.forceCtrlInsideParent( );";
		item[%itemId++] = "-";
		item[%itemId++] = "Bring to Front" TAB "" TAB "GuiEditor.BringToFront();";
		item[%itemId++] = "Send to Back" TAB "" TAB "GuiEditor.PushToBack();";
	};
	GuiEdMenu.insert(GuiEdMenu_Layout);
	
	delObj(GuiEdMenu_LabLayout);
	new PopupMenu(GuiEdMenu_LabLayout)
	{
		superClass = "EditorMenuPopup";
		barTitle = "LabLayout";
		internalName = "LabLayoutMenu";
      actionMap = %actionMap;
      
		item[%itemId = 0]= "Align Left" TAB $KeyCtrl SPC "Left" TAB "GuiEd.AlignCtrlToParent(\"left\");";
		item[%itemId++] = "Align Right" TAB $KeyCtrl SPC "Left" TAB "GuiEd.AlignCtrlToParent(\"right\");";
		item[%itemId++] = "Align Top" TAB $KeyCtrl SPC "Left" TAB "GuiEd.AlignCtrlToParent(\"top\");";
		item[%itemId++] = "Align Bottom" TAB $KeyCtrl SPC "Left" TAB "GuiEd.AlignCtrlToParent(\"bottom\");";
		item[%itemId++] = "-";
		item[%itemId++] = "Force into Parent-" TAB "Alt g" TAB "Lab.forceCtrlInsideParent( );";
		item[%itemId++] = "-";
		item[%itemId++] = "Set control as reference-" TAB "Alt r" TAB "Lab.setSelectedControlAsReference( );";
		item[%itemId++] = "Set profile from reference-" TAB "Shift 1" TAB "Lab.setControlReferenceField(\" Profile \");";
		item[%itemId++] = "Set position from reference-" TAB "Shift 2" TAB "Lab.setControlReferenceField(\" position \");";
		item[%itemId++] = "Set extent from reference-" TAB "Shift 3" TAB "Lab.setControlReferenceField(\" extent \");";
		item[%itemId++] = "Set empty name tp selection-" TAB "Shift n" TAB "Lab.setControlReferenceField(\" name \");";
		item[%itemId++] = "-";
		item[%itemId++] = "Toggle auto load last GUI" TAB "" TAB "Lab.toggleAutoLoadLastGui();" TAB "$pref::Editor::AutoLoadLastGui;";
	};
	GuiEdMenu.insert(GuiEdMenu_LabLayout);
	delObj(GuiEdMenu_Move);
	new PopupMenu(GuiEdMenu_Move)
	{
		superClass = "EditorMenuPopup";
		barTitle = "Move";
		internalName = "MoveMenu";
      actionMap = %actionMap;
      
		item[%itemId = 0] = "Nudge Left" TAB "Left" TAB "GuiEditor.move( -1, 0);";
		item[%itemId++] = "Nudge Right" TAB "Right" TAB "GuiEditor.move( 1, 0);";
		item[%itemId++] = "Nudge Up" TAB "Up" TAB "GuiEditor.move( 0, -1);";
		item[%itemId++] = "Nudge Down" TAB "Down" TAB "GuiEditor.move( 0, 1 );";
		item[%itemId++] = "-";
		item[%itemId++] = "Big Nudge Left" TAB "Shift Left" TAB "GuiEditor.move( - $Cfg_GuiEditor_NudgeBigStep, 0 );";
		item[%itemId++] = "Big Nudge Right" TAB "Shift Right" TAB "GuiEditor.move( $Cfg_GuiEditor_NudgeBigStep, 0 );";
		item[%itemId++] = "Big Nudge Up" TAB "Shift Up" TAB "GuiEditor.move( 0, - $Cfg_GuiEditor_NudgeBigStep );";
		item[%itemId++] = "Big Nudge Down" TAB "Shift Down" TAB "GuiEditor.move( 0, $Cfg_GuiEditor_NudgeBigStep );";
	};
	GuiEdMenu.insert(GuiEdMenu_Move);
	delObj(GuiEdMenu_Snap);
	
	new PopupMenu(GuiEdMenu_Snap)
	
	{
		superClass = "EditorMenuPopup";
		barTitle = "Snap";
		internalName = "SnapMenu";
      actionMap = %actionMap;
      
		item[%itemId = 0] = "Snap Edges" TAB "Alt-Shift E" TAB "GuiEditor.toggleEdgeSnap();" TAB "GuiEditor.snapToEdges;";
		item[%itemId++] = "Snap Centers" TAB "Alt-Shift C" TAB "GuiEditor.toggleCenterSnap();" TAB "GuiEditor.snapToCenters;";
		item[%itemId++] = "-";
		item[%itemId++] = "Snap to Guides" TAB "Alt-Shift G" TAB "GuiEditor.toggleGuideSnap();" TAB "GuiEditor.snapToGuides;";
		item[%itemId++] = "Snap to Controls" TAB "Alt-Shift T" TAB "GuiEditor.toggleControlSnap();" TAB "GuiEditor.snapToControls;";
		item[%itemId++] = "Snap to Canvas" TAB "" TAB "GuiEditor.toggleCanvasSnap();" TAB "GuiEditor.snapToCanvas;";
		item[%itemId++] = "Snap to Grid" TAB "" TAB "GuiEditor.toggleGridSnap();" TAB "GuiEditor.snap2Grid;";
		item[%itemId++] = "-";
		item[%itemId++] = "Show Guides" TAB "" TAB "GuiEditor.toggleDrawGuides();" TAB "GuiEditor.drawGuides;";
		item[%itemId++] = "Full box selection" TAB "" TAB "GuiEditor.toggleFullBoxSelection();" TAB "GuiEditor.fullBoxSelection;";
		item[%itemId++] = "Clear Guides" TAB "" TAB "GuiEditor.clearGuides();";
	};
	GuiEdMenu.insert(GuiEdMenu_Snap);
	delObj(GuiEdMenu_Lab);
	new PopupMenu(GuiEdMenu_Lab)
	{
		superClass = "EditorMenuPopup";
		internalName = "LabMenu";
      actionMap = %actionMap;
      
		barTitle = "Lab";
		item[%itemId = 0] = "-> Automated Saving <-";
		item[%itemId++] = "Save all layout GUIs" TAB "" TAB "Lab.layoutSaveAll();";
		item[%itemId++] = "-> General Editors GUIs <-";
		item[%itemId++] = "Toggle Editors GUI listing" TAB "" TAB "Lab.toggleEditorGuiListing();";
		item[%itemId++] = "Detach the Editor GUIs" TAB "" TAB "Lab.detachAllGuis();";
		item[%itemId++] = "Attach the Editor GUIs" TAB "" TAB "Lab.attachAllGuis();";
		item[%itemId++] = "Toggle Lab Editor Settings" TAB "" TAB "toggleDlg(LabEditorSettings);";
		item[%itemId++] = "-> Special Editors GUIs <-";
		item[%itemId++] = "Toggle Field Duplicator" TAB  "Alt-Shift D" TAB "Lab.toggleDuplicator();";
		item[%itemId++] = "-> Special TorqueLab functions <-";
		item[%itemId++] = "Save all toolbars" TAB  "" TAB "Lab.saveToolbar();";
		item[%itemId++] = "-> Editing the GuiEditor GUI <-";
		item[%itemId++] = "Clone GuiEditorGui" TAB  "" TAB "Lab.cloneGuiEditor();";
		item[%itemId++] = "Apply GuiEditorGui Clone" TAB  "" TAB "Lab.convertClonedGuiEditor();";
		item[%itemId++] = "Dump GUI TextIds" TAB  "" TAB "getGuiTextIds(GuiEd.lastGuiLoaded);";

	};
	GuiEdMenu.insert(GuiEdMenu_Lab);
	delObj(GuiEdMenu_Help);
	new PopupMenu(GuiEdMenu_Help)
	{
		superClass = "EditorMenuPopup";
		internalName = "HelpMenu";
      actionMap = %actionMap;
      
		barTitle = "Help";
		item[%itemId = 0] = "Online Documentation..." TAB "Alt F1" TAB "gotoWebPage( GuiEditor.documentationURL );";
		item[%itemId++] = "Offline User Guid..." TAB "" TAB "gotoWebPage( GuiEditor.documentationLocal );";
		item[%itemId++] = "Offline Reference Guide..." TAB "" TAB "shellExecute( GuiEditor.documentationReference );";
		item[%itemId++] = "-";
		item[%itemId++] = "Torque 3D Public Forums..." TAB "" TAB "gotoWebPage( \"http://www.garagegames.com/community/forums/73\" );";
		item[%itemId++] = "Torque 3D Private Forums..." TAB "" TAB "gotoWebPage( \"http://www.garagegames.com/community/forums/63\" );";

	};
	GuiEdMenu.insert(GuiEdMenu_Help);
	
	//GuiEdMenu.activate();
    GuiEdMenuArea.add(GuiEdMenu);
   //GuiEdMenu.buildAccel();
   
}

$Cfg_GuiEditor__idx_snap_edge = 0;
$Cfg_GuiEditor__idx_snap_center = 1;
$Cfg_GuiEditor__idx_snap_guides = 3;
$Cfg_GuiEditor__idx_snap_control = 4;
$Cfg_GuiEditor__idx_snap_canvas = 5;
$Cfg_GuiEditor__idx_snap_grid = 6;
$Cfg_GuiEditor__idx_draw_guides = 8;
$Cfg_GuiEditor__idx_select_fullbox = 21;
