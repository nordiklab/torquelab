//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function Lab::destroyMenus(%this)
{
   warnLog("Lab::destroyMenus","DEPRECIATED");
   return;
	if(isObject(WorldEdMenu))
	{
		warnLog("The Gui Editor MenuBar is already built. It will be deleted and regenerated");
		WorldEdMenu.destroy();
	}
if(isObject(GuiEdMenu))
	{
		warnLog("The Gui Editor MenuBar is already built. It will be deleted and regenerated");
		GuiEdMenu.destroy();
	}
}

function EditorMenuBar::destroy(%this)
{
 
   // Destroy menus      
   while( %this.getCount() != 0 )
   {
      %this.getObject( 0 ).delete();
   }
   delObj(%this.actionMap);

   %this.delete();
}

function EditorMenuBar::activate(%this)
{
   
  // Canvas.setMenuBar(%this);
  // %this.attachToCanvas( Canvas, 0 );   
   //Lab.menuBar = %this;
     %this.fitIntoParents("width");
   
     //Canvas.setMenuBar(%this);
     //Lab.menuBar = %this;
   
}
//------------------------------------------------------------------------------
// A menu item have been selected
function EditorMenuBar::findMenuTitle(%this,%menuTitle)
{

	for (%i=0 ; %i<%this.getMenuCount(); %i++)
	{
		%menu = %this.getMenu(%i);

		if (%menu.barTitle $= %menuTitle)
			return %menu;
	}

	return "";
}

//------------------------------------------------------------------------------
// A menu item have been selected
function EditorMenuBar::findSubMenu(%this,%menuTitle,%subMenuTitle)
{
	%menuObj = %this.findMenuTitle(%menuTitle);

	if (!isObject(%menuObj))
		return -1;

	%subMenu = %menuObj.findMenuTitle(%subMenuTitle);
	return %subMenu;
}
//- Method called to setup default state for all menus in menubar. 
function EditorMenuBar::setupDefaultState(%this)
{
	for(%i = 0; %i < %this.menuBar.getCount(); %i++)
	{
		%menu = %this.menuBar.getObject(%i);
		%menu.setupDefaultState();
	}
}

//------------------------------------------------------------------------------
// A menu item have been selected
function EditorMenuBar::initMenu(%this)
{

	for (%i=0 ; %i<%this.getMenuCount(); %i++)
	{
		%menu = %this.getMenu(%i);
      %menu.actionMap = %this.actionMap;
	   %menu.menuObj = %this;
	}	
}

//-----------------------------------------------------------------------------
// Editors Menu Helper Class
//-----------------------------------------------------------------------------
/// @class EditorMenuBar
/// @brief Create Dynamic Context and MenuBar Menus
///
///
/// Summary : The EditorMenuBar script class exists merely as a helper for creating
///           popup menu's for use in torque editors.  It is setup as a single
///           object with dynamic fields starting with item[0]..[n] that describe
///           how to create the menu in question.  An example is below.
///
/// isPopup : isPopup is a persistent field on PopupMenu console class which
///           when specified to true will allow you to perform .showPopup(x,y)
///           commands which allow popupmenu's to be used/reused as menubar menus
///           as well as context menus.
///
/// barPosition : barPosition indicates which index on the menu bar (0 = leftmost)
///           to place this menu if it is attached.  Use the attachToMenuBar() command
///           to attach a menu.
///
/// barName : barName specifies the visible name of a menu item that is attached
///           to the global menubar.
///
/// canvas  : The GuiCanvas object the menu should be attached to. This defaults to
///           the global Canvas object if unspecified.
///
/// Remarks : If you wish to use a menu as a context popup menu, isPopup must be
///           specified as true at the creation time of the menu.
///
///
/// @li @b item[n] (String) TAB (String) TAB (String) : <c>A Menu Item Definition.</c>
/// @code item[0] = "Open File..." TAB "Ctrl O" TAB "Something::OpenFile"; @endcode
///
/// @li @b isPopup (bool) : <c>If Specified the menu will be considered a popup menu and should be used via .showPopup()</c>
/// @code isPopup = true; @endcode
///
///
/// Example : Creating a @b MenuBar Menu
/// @code
/// %%editMenu = new PopupMenu()
/// {
///    barPosition = 3;
///    barName     = "View";
///    superClass = "EditorMenuBar";
///    item[0] = "Undo" TAB "Ctrl Z" TAB "levelBuilderUndo(1);";
///    item[1] = "Redo" TAB "Ctrl Y" TAB "levelBuilderRedo(1);";
///    item[2] = "-";
/// };
///
/// %%editMenu.attachToMenuBar( 1, "Edit" );
///
/// @endcode
///
///
/// Example : Creating a @b Context (Popup) Menu
/// @code
/// %%contextMenu = new PopupMenu()
/// {
///    superClass = EditorMenuBar;
///    isPopup    = true;
///    item[0] = "My Super Cool Item" TAB "Ctrl 2" TAB "echo(\"Clicked Super Cool Item\");";
///    item[1] = "-";
/// };
///
/// %%contextMenu.showPopup();
/// @endcode
///
///
/// Example : Modifying a Menu
/// @code
/// %%editMenu = new PopupMenu()
/// {
///    item[0] = "Foo" TAB "Ctrl F" TAB "echo(\"clicked Foo\")";
///    item[1] = "-";
/// };
/// %%editMenu.addItem( 2, "Bar" TAB "Ctrl B" TAB "echo(\"clicked Bar\")" );
/// %%editMenu.removeItem( 0 );
/// %%editMenu.addItem( 0, "Modified Foo" TAB "Ctrl F" TAB "echo(\"clicked modified Foo\")" );
/// @endcode
///
///
/// @see PopupMenu
///
//-----------------------------------------------------------------------------
//=============================================================================================
//    EditorMenuBar Callbacks.
//=============================================================================================
// DECLARE_CALLBACK( void, onMouseInMenu, ( bool hasLeftMenu ));
// DECLARE_CALLBACK( void, onMenuSelect, ( S32 menuId, const char* menuText ));
// DECLARE_CALLBACK( void, onMenuItemSelect, ( S32 menuId, const char* menuText, S32 menuItemId, const char* menuItemText  ));
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// The mouse is over a editor menu bar
function EditorMenuBar::onMouseInMenu(%this,%hasLeftMenu)
{

}

//------------------------------------------------------------------------------
// A menu have been selected
function EditorMenuBar::onMenuSelect(%this,%menuId,%menuText)
{

}

//------------------------------------------------------------------------------
// A menu item have been selected
function EditorMenuBar::onMenuItemSelect(%this,%menuId,%menuText,%menuItemId,%menuItemText)
{

}

//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Editors Menu Popup Helper Class
//-----------------------------------------------------------------------------
/// @class EditorMenuPopup
/// @brief Create Dynamic Popup
///
///
/// Summary : The EditorMenuPopup script class exist.

//------------------------------------------------------------------------------
// Adds one item to the menu.
// if %item is skipped or "", we will use %item[#], which was set when the menu was created.
// if %item is provided, then we update %item[#].
function EditorMenuPopup::addItem(%this, %pos, %item)
{
	if (%item $= "")
		%item = %this.item[%pos];

	if (%item !$= %this.item[%pos])
		%this.item[%pos] = %item;

	%name = getField(%item, 0);
	%accel = getField(%item, 1);
	%cmd = getField(%item, 2);
	// We replace the [this] token with our object ID
	%cmd = strreplace(%cmd, "[this]", %this);
	%this.item[%pos] = setField(%item, 2, %cmd);
	%listName = %name;

	//PosOffset is used when a custom item have been added
	%pos += %this.posOffset;

	if (isObject(%accel) )
	{
		//Mud-H: Can create error if accel key is use a an object name
		// If %accel is an object, we want to add a sub menu
		if (%accel.isMemberOfClass("PopupMenu"))
		{
			%this.insertSubmenu(%pos, %name, %accel);
			return %pos;

		}

		%accel = "";
	}

	if (getSubStr(%name,0,2) $= "- ")
	{
		%this.createItem(%pos, "", "");

		if (restWords(%name) !$= "")
		{
			%this.createItem(%pos+1, restWords(%name), %accel,%cmd);
			%this.posOffset++;
		}
	}
	else
	{
		%this.createItem(%pos, %name !$= "-" ? %name : "", %accel,%cmd);
	}

	return %pos;
}

// Helpers to do some validation before insertItem sent
function EditorMenuPopup::createItem(%this, %pos,%name,%accel,%cmd)
{
   //Check if the menu use an ActionMap instead of Accelerators
   if (isObject(%this.actionMap))
   {
      
      if (%accel !$= "" && %cmd !$="")
      {
         %actionMap = %this.actionMap;
         %actionMap.bindCmd(keyboard,%accel,%cmd,"");
   
         //%accel = ""; We still want to see the bind
         %cmd = "";
      }
   }
   %this.insertItem(%pos, %name, %accel,%cmd);
}
//------------------------------------------------------------------------------

function EditorMenuPopup::appendItem(%this, %item)
{
	%this.addItem(%this.getItemCount(), %item);
}

//------------------------------------------------------------------------------

function EditorMenuPopup::onAdd(%this)
{
	%this.curPos = 0;
	%this.posOffset = 0;

	if (! isObject(%this.canvas))
		%this.canvas = Canvas;

	for(%i = 0; %this.item[%i] !$= ""; %i++)
	{
		%this.addItem(%i);
	}
}

//------------------------------------------------------------------------------

function EditorMenuPopup::onRemove(%this)
{
	if (!$Cfg_UI_Menu_UseNativeMenu && !$InGuiEditor)
		return;

	if (!isObject(%this))
	{
		warnLog("Trying to remove a menu which is not a menu:",%this);
		return;
	}

}

//------------------------------------------------------------------------------

function EditorMenuPopup::onSelectItem(%this, %id, %text)
{
	%cmd = getField(%this.item[%id], 2);

	if (%cmd !$= "")
	{
		eval(%cmd);
		return true;
	}

	return false;
}

//- Sets a new name on an existing menu item.
function EditorMenuPopup::setItemName(%this, %id, %name)
{
	%item = %this.item[%id];
	%accel = getField(%item, 1);
	%this.setItem(%id, %name, %accel);
}

//- Sets a new command on an existing menu item.
function EditorMenuPopup::setItemCommand(%this, %id, %command)
{
	%this.item[%id] = setField(%this.item[%id], 2, %command);
}

//-///-///-///-///-///-///-///-///-///-///-///-///-///-///-///-///-///-///

//- Method called to setup default state for the menu. Expected to be overriden
//- on an individual menu basis. See the mission editor for an example.
function EditorMenuPopup::setupDefaultState(%this)
{
	for(%i = 0; %this.item[%i] !$= ""; %i++)
	{
		%name = getField(%this.item[%i], 0);
		%accel = getField(%this.item[%i], 1);
		%cmd = getField(%this.item[%i], 2);

		// Pass on to sub menus
		if (isObject(%accel))
			%accel.setupDefaultState();
	}
}

//- Method called to easily enable or disable all items in a menu.
function EditorMenuPopup::enableAllItems(%this, %enable)
{
	for(%i = 0; %this.item[%i] !$= ""; %i++)
	{
		%this.enableItem(%i, %enable);
	}
}

//------------------------------------------------------------------------------
//- Method called to easily enable or disable all items in a menu.
function EditorMenuPopup::clearAllItems(%this, %enable)
{
	%count = %this.getItemCount();

	for(%i = %count-1; %i >=0; %i--)
	{
		//%this.removeItem(%i);
%name = getField(%this.item[%i], 0);
		%accel = getField(%this.item[%i], 1);
		%cmd = getField(%this.item[%i], 2);
		info("Menu item data = ",%name,%accel,%cmd);

	}
}

//- Method called to easily enable or disable all items in a menu.
function EditorMenuPopup::findMenu(%this, %title)
{
	for(%i = 0; %this.item[%i] !$= ""; %i++)
	{
		%name = getField(%this.item[%i], 0);
		%accel = getField(%this.item[%i], 1);

		if (%name $= %title)
		{
			if (isObject(%accel))
				return %accel;
			else
				return %i;
		}
	}

	return -1;
}

//- Method called to easily enable or disable all items in a menu.
function EditorMenuPopup::checkMenuItem(%this, %firstPos,%lastPos,%checkPos)
{
	%this.checkRadioItem(%firstPos,%lastPos,%checkPos);
}

/*
DefineConsoleMethod(PopupMenu, enableItem, void, (S32 pos, bool enabled), , "(pos, enabled)")
{
   object->enableItem(pos, enabled);
}

DefineConsoleMethod(PopupMenu, checkItem, void, (S32 pos, bool checked), , "(pos, checked)")
{
   object->checkItem(pos, checked);
}

DefineConsoleMethod(PopupMenu, checkRadioItem, void, (S32 firstPos, S32 lastPos, S32 checkPos), , "(firstPos, lastPos, checkPos)")
{
   object->checkRadioItem(firstPos, lastPos, checkPos);
}

DefineConsoleMethod(PopupMenu, isItemChecked, bool, (S32 pos), , "(pos)")
{
   return object->isItemChecked(pos);
}
*/
