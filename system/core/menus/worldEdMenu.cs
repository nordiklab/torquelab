//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
//GuiEditCanvas.createLabMenu();
function Lab::buildWorldMenus(%this)
{
	 if (isObject(WorldEdMenu))
	   WorldEdMenu.destroy();
	
	Lab.worldEdMap = newActionMap(WorldEdMap,true);
	%actionMap = WorldEdMap;
	new GuiMenuBar(WorldEdMenu)
	{
		dynamicItemInsertPos = 3;
		extent = "500 22";
		minExtent = "500 22";
		horizSizing = "width";
		profile = "ToolsMenuBarProfile";
		superClass = "EditorMenuBar";
		actionMap = %actionMap;
		
	};
	
WorldEdMenuArea.visible = 1;

	%menu = new PopupMenu()
	{
		superClass = "EditorMenuPopup";
		barTitle = "File";
		internalName = "FileMenu";
      actionMap = %actionMap;
      
		item[%itemId = 0]  = "Save Level" TAB "Ctrl S" TAB "Lab.SaveCurrentMission();";
		item[%itemId++]  = "Save Level As..." TAB "" TAB "Lab.SaveCurrentMission(true);";
		item[%itemId++]  = "-";
		item[%itemId++]  =  "Open Project in Torsion" TAB "" TAB "EditorOpenTorsionProject();";
		item[%itemId++]  =  "Open Level File in Torsion" TAB "" TAB "EditorOpenFileInTorsion();";
		item[%itemId++]  =  "-";
		item[%itemId++]  = "Create Blank Terrain" TAB "" TAB "Canvas.pushDialog( CreateNewTerrainGui );";
		item[%itemId++]  = "Import Terrain Heightmap" TAB "" TAB "Canvas.pushDialog( TerrainImportGui );";
		item[%itemId++]  = "Export Terrain Heightmap" TAB "" TAB "Canvas.pushDialog( TerrainExportGui );";
		item[%itemId++]  = "-";
		item[%itemId++]  = "Export To COLLADA..." TAB "" TAB "EditorExportToCollada();";
		item[%itemId++]  =  "-";
		item[%itemId++]  =  "Add FMOD Designer Audio..." TAB "" TAB "AddFMODProjectDlg.show();";
		item[%itemId++]  = "-";
		item[%itemId++]  = "Play Level" TAB "" TAB "Editor.close($HudCtrl);";
		item[%itemId++]  = "Exit Level" TAB "" TAB "Lab.ExitMission();";
		item[%itemId++]  = "Quit" TAB $KeyQuit TAB "Lab.QuitGame();";

	};
	WorldEdMenu.insert(%menu);
	%menu = new PopupMenu()
	{
		superClass = "EditorMenuPopup";
		barTitle = "Edit";
		internalName = "EditMenu";
      actionMap = %actionMap;
      
		item[%itemId = 0] = "Undo" TAB $KeyCtrl SPC "Z" TAB "Editor.getUndoManager().undo();";
		item[%itemId++] = "Redo" TAB $KeyRedo TAB "Editor.getUndoManager().redo();";
		item[%itemId++] = "-";
		item[%itemId++] = "Cut" TAB $KeyCtrl SPC "X" TAB "EditorMenuEditCut();";
		item[%itemId++] = "Copy" TAB $KeyCtrl SPC "C" TAB "EditorMenuEditCopy();";
		item[%itemId++] = "Paste" TAB $KeyCtrl SPC "V" TAB "EditorMenuEditPaste();";
		item[%itemId++] = "Delete" TAB "Delete" TAB "EditorMenuEditDelete();";
		item[%itemId++] = "-";
		item[%itemId++] = "Deselect" TAB "X" TAB "EditorMenuEditDeselect();";
		item[%itemId++] = "Select..." TAB "" TAB "ESelectObjects.toggleVisibility();";
		item[%itemId++] = "-";
		item[%itemId++] = "Audio Parameters..." TAB "" TAB "EManageSFXParameters.ToggleVisibility();";
		item[%itemId++] = "LabEditor Settings..." TAB "" TAB "toggleDlg(LabSettingsDlg);";
		item[%itemId++] = "Snap Options..." TAB "" TAB "ESnapOptions.ToggleVisibility();";
		item[%itemId++] = "-";
		item[%itemId++] = "Game Options..." TAB "" TAB "Canvas.pushDialog(DlgOptions);";
		item[%itemId++] = "PostEffect Manager" TAB "" TAB "Canvas.pushDialog(PostFXManager);";
		item[%itemId++] = "Lab PostFX Manager" TAB "" TAB "EPostFxManager.toggleState();";
		item[%itemId++] = "Copy Tool" TAB "" TAB "toggleDlg(ToolObjectCopyDlg);";
		item[%itemId++] = "Toggle transform box" TAB "" TAB "LabTransformBox.toggleBox();";
	};
	WorldEdMenu.insert(%menu);
	%menu = new PopupMenu()
	{
		superClass = "EditorMenuPopup";
		barTitle = "Object";
		internalName = "ObjectMenu";
      actionMap = %actionMap;
      
		item[%itemId = 0] = "Lock Selection" TAB $KeyCtrl @ " L" TAB "EWorldEditor.lockSelection(true); EWorldEditor.syncGui();";
		item[%itemId++] = "Unlock Selection" TAB $KeyCtrl @ "-Shift L" TAB "EWorldEditor.lockSelection(false); EWorldEditor.syncGui();";
		item[%itemId++] = "-";
		item[%itemId++] = "Hide Selection" TAB $KeyCtrl @ " H" TAB "EWorldEditor.hideSelection(true); EWorldEditor.syncGui();";
		item[%itemId++] = "Show Selection" TAB $KeyCtrl @ "-Shift H" TAB "EWorldEditor.hideSelection(false); EWorldEditor.syncGui();";
		item[%itemId++] = "-";
		item[%itemId++] = "Group Selection" TAB $KeyCtrl @ " G" TAB "Lab.addSelToNewContainer();";
		item[%itemId++] = "Group Selection" TAB $KeyCtrl @ "-Shift G" TAB "Lab.removeSelFromContainer();";
		item[%itemId++] = "-";
		item[%itemId++] = "Align Bounds";
		item[%itemId++] = "Align Center";
		item[%itemId++] = "-";
		item[%itemId++] = "Reset Transforms" TAB "Ctrl R" TAB "EWorldEditor.resetTransforms();";
		item[%itemId++] = "Reset Selected Rotation" TAB "" TAB "EWorldEditor.resetSelectedRotation();";
		item[%itemId++] = "Reset Selected Scale" TAB "" TAB "EWorldEditor.resetSelectedScale();";
		item[%itemId++] = "Transform Selection..." TAB "Ctrl T" TAB "ETransTool.ToggleVisibility();";
		item[%itemId++] = "-";
		//item[13] = "Drop Camera to Selection" TAB "Ctrl Q" TAB "Lab.dropCameraToSelection();";
		//item[14] = "Add Selection to Instant Group" TAB "" TAB "EWorldEditor.addSelectionToAddGroup();";
		item[%itemId++] = "Drop Selection" TAB "Ctrl D" TAB "EWorldEditor.dropSelection();";
		//item[15] = "-";
		item[%itemId++] = "Drop Location";

		item[%itemId++] = "-";
		item[%itemId++] = "Make Selection Prefab" TAB "" TAB "Lab.CreatePrefab();";
		item[%itemId++] = "Explode Selected Prefab" TAB "" TAB "Lab.ExplodePrefab();";
		item[%itemId++] = "-";
		item[%itemId++] = "Mount Selection A to B" TAB "" TAB "Lab.mountObjAToB();";
		item[%itemId++] = "Unmount Selected Object" TAB "" TAB "Lab.unmountObjA();";
		item[%itemId++] = "-";
		item[%itemId++] = "Objects Manager" TAB  $KeyCtrl @ " M" TAB "ESelectObjects.toggleVisibility();";
	};
	WorldEdMenu.insert(%menu);

	%menu = new PopupMenu()
	{
		superClass = "EditorMenuPopup";
		barTitle = "Utility";
		internalName = "UtilityMenu";
      actionMap = %actionMap;
      		item[%itemId = 0] = "Full Relight" TAB "Alt L" TAB "Editor.lightScene(\"\", forceAlways);";
		item[%itemId++] = "Toggle ShadowViz" TAB "" TAB "toggleShadowViz();";
		item[%itemId++] = "-------------";
		item[%itemId++] = "Open disabled plugins bin" TAB "" TAB "Lab.openDisabledPluginsBin();";
		item[%itemId++] = "Customize Interface" TAB "" TAB "ETools.toggleTool(\"GuiCustomizer\");";
		item[%itemId++] = "----------------------";
		item[%itemId++] = "Auto arrange MissionGroup Root" TAB "" TAB "SEP_ScenePage.organizeMissionGroup();";
		item[%itemId++] = "Auto arrange MissionGroup" TAB "" TAB "SEP_ScenePage.organizeMissionGroup(5);";
		item[%itemId++] = "----------------------";
		item[%itemId++] = "Capture current view as level preview" TAB "" TAB "Lab.setCurrentViewAsPreview();";
		item[%itemId++] = "Set next screenshot as preview" TAB "" TAB "Lab.setNextScreenShotPreview();";
	};
	WorldEdMenu.insert(%menu);

	if (!isObject(WEMenuViewMode))
	{
		new PopupMenu(WEMenuViewMode)
		{
			superClass = "EditorMenuPopup";
			barTitle = "ViewModes";
			internalName = "ViewModesMenu";
			item[%itemId = 0] = "Top" TAB "Alt 2" TAB "Lab.setCameraView(\"Top\");";
			item[%itemId++] = "Bottom" TAB "Alt 5" TAB "Lab.setCameraView(\"Bottom\");";
			item[%itemId++] = "Front" TAB "Alt 3" TAB "Lab.setCameraView(\"Front\");";
			item[%itemId++] = "Back" TAB "Alt 6" TAB "Lab.setCameraView(\"Back\");";
			item[%itemId++] = "Left" TAB "Alt 4" TAB "Lab.setCameraView(\"Left\");";
			item[%itemId++]= "Right" TAB "Alt 7" TAB "Lab.setCameraView(\"Right\");";
			item[%itemId++] = "Perspective" TAB "Alt 1" TAB "Lab.setCameraView(\"Standard\");";
			item[%itemId++] = "Isometric" TAB "Alt 8" TAB "Lab.setCameraView(\"Isometric\");";
		};
	}

	%menu = new PopupMenu()
	{
		superClass = "EditorMenuPopup";
		barTitle = "Camera";
		internalName = "CameraMenu";
      actionMap = %actionMap;
      		item[%itemId = 0] = "World Camera";
		item[%itemId++] = "Player Camera";
		item[%itemId++] = "-";
		item[%itemId++] = "Toggle Control Object" TAB $KeyAltCtrl SPC "C" TAB "Lab.toggleControlObject();";
	
		item[%itemId++] = "Place Camera at Selection" TAB "Ctrl Q" TAB "Lab.dropCameraToSelection();";
		item[%itemId++] = "Place Camera at Player" TAB "Alt Q" TAB "Lab.dropCameraAtPlayer();";
		item[%itemId++] = "Place Player at Camera" TAB "Alt W" TAB "Lab.DropPlayerAtCamera();";
		item[%itemId++] = "-";
	
		item[%itemId++] = "Fit View to Selection" TAB "F" TAB "Lab.fitCameraToSelection();";
		item[%itemId++] = "Fit View To Selection and Orbit" TAB "Alt F" TAB "Lab.fitCameraToSelection(true);";
		item[%itemId++] = "-";
		item[%itemId++] = "Views" TAB WEMenuViewMode;

		item[%itemId++] = "-";
		item[%itemId++] = "Speed";

		item[%itemId++] = "-";
		item[%itemId++] = "Add Bookmark..." TAB "Ctrl B" TAB "EManageBookmarks.addCameraBookmarkByGui();";
		item[%itemId++] = "Manage Bookmarks..." TAB "Ctrl-Shift B" TAB "EManageBookmarks.ToggleVisibility();";
		item[%itemId++] = "Jump to Bookmark";
	};
	WorldEdMenu.insert(%menu);
	%menu = new PopupMenu()
	{
		superClass = "EditorMenuPopup";
		barTitle = "Mission";
		internalName = "MissionMenu";
      actionMap = %actionMap;
      		item[%itemId = 0] = "Mission settings" TAB "" TAB "toggleDlg(LabMissionSettingsDlg);";
		item[%itemId++] = "Capture current view as level preview" TAB "" TAB "Lab.setCurrentViewAsPreview();";
		item[%itemId++] = "Set next screenshot as preview" TAB "" TAB "Lab.setNextScreenShotPreview();";
		item[%itemId++] = "Capture screen without EditorGui" TAB $KeyCtrl @ "-Shift p" TAB "Lab.takeScreenShot(1);";
		item[%itemId++] = "Capture EditorGui screen" TAB $KeyCtrl SPC "p" TAB "Lab.takeScreenShot();";
	};
	WorldEdMenu.insert(%menu);

	if (!isObject(WEMenuEditors))
	{
		new PopupMenu(WEMenuEditors)
		{
			superClass = "EditorMenuPopup";
			barTitle = "ToolEditors";
			internalName = "ToolEditorsMenu";
      actionMap = %actionMap;
      			item[%itemId = 0] = "SceneEditor";
		};
	}

	%menu = new PopupMenu()
	{
		superClass = "EditorMenuPopup";
		barTitle = "Tool";
		internalName = "ToolMenu";
      actionMap = %actionMap;
      
		item[%itemId = 0]  = "Editors" TAB WEMenuEditors;
		item[%itemId++] = "Toggle GroundCover Manager" TAB "" TAB "SceneEditorDialogs.toggleDlg(\"GroundCover\");";
		item[%itemId++] = "Toggle Ambient Manager" TAB "" TAB "SceneEditorDialogs.toggleDlg(\"AmbientManager\");";
		item[%itemId++] = "Toggle Vehicle Manager" TAB "" TAB "SceneEditorDialogs.toggleDlg(\"VehicleManager\");";
		item[%itemId++] = "Toggle PostFx Manager" TAB "" TAB "EPostFxManager.toggleState();" TAB "EPostFxManager.visible;";
		item[%itemId++] = "-";
		item[%itemId++] = "Open terrain material manager" TAB "" TAB "TerrainMaterialDlg.show();" TAB "TerrainMaterialDlg.visible;";
		item[%itemId++] = "-";
		item[%itemId++] = "Open HelpersLab Dialog" TAB "" TAB "pushDlg(LabHelpersDlg);" TAB "LabHelpersDlg.visible;";

	};
	WorldEdMenu.insert(%menu);
	%menu = new PopupMenu()
	{
		superClass = "EditorMenuPopup";
		barTitle = "View";
		internalName = "ViewMenu";
      actionMap = %actionMap;
      
		//item[%itemId++] = "Visibility Layers" TAB "Alt V" TAB "EVisibilityLayers.toggleVisibility();";
		item[%itemId = 0] = "Show Grid in Ortho Views" TAB $KeyCtrl @ "-Shift-Alt G" TAB "EWorldEditor.renderOrthoGrid = !EWorldEditor.renderOrthoGrid;";
		item[%itemId++] = "-";
		item[%itemId++] = "Set default plugins order" TAB "" TAB "Lab.sortPluginsBar(true);";
		item[%itemId++] = "Store plugins order as default" TAB "" TAB "Lab.updatePluginIconOrder(true);";
		item[%itemId++] = "-";
		item[%itemId++] = "Lock Toolbar Drag n Drop" TAB "" TAB "" TAB "$Cfg_LockToolbar = !$Cfg_LockToolbar;";
		item[%itemId++] = "-";
	};
	WorldEdMenu.insert(%menu);
	%menu = new PopupMenu()
	{
		superClass = "EditorMenuPopup";
		barTitle = "Help";
		internalName = "HelpMenu";
      actionMap = %actionMap;
      
		item[%itemId = 0] = "Online Documentation..." TAB "Alt F1" TAB "gotoWebPage(EWorldEditor.documentationURL);";
		item[%itemId++] = "Offline User Guide..." TAB "" TAB "gotoWebPage(EWorldEditor.documentationLocal);";
		item[%itemId++] = "Offline Reference Guide..." TAB "" TAB "shellexecute(EWorldEditor.documentationReference);";
		item[%itemId++] = "Torque 3D Forums..." TAB "" TAB "gotoWebPage(EWorldEditor.forumURL);";
	};
	WorldEdMenu.insert(%menu);

   //WorldEdMenu.activate();
    WorldEdMenuArea.add(WorldEdMenu);
   //WorldEdMenu.buildAccel();
}

function WEMenuEditors::addPlugin(%this,%pluginObj)
{

	%title = %pluginObj.displayName;
	%cmd = "Lab.setEditor("@%pluginObj.getName()@");";
	%item = %title TAB "" TAB %cmd TAB %pluginObj;

	%count = %this.getItemCount();
	%itemId = -1;

	//Make sure it's not already there, if so just leave
	for(%i = 0; %i < %count; %i++)
	{
		%data = %this.item[%i];
		%checktitle = getField(%data,0);

		if(%checktitle $= %title)				
			%itemId = %i;
		
	}

	if (%itemId == -1)
		%itemId = %this.addItem(%this.getItemCount(),%item);

	if (!%pluginObj.isEnabled)
		%this.enableItem(%itemId,0);

	%this.pluginItemId[%pluginObj.plugin] = %itemId ;
	return %itemId;
}
function WEMenuEditors::removePlugin(%this,%pluginObj)
{
	%title = %pluginObj.plugin;
	%count = %this.getItemCount();

	//Make sure it's not already there, if so just leave
	for(%i = 0; %i < %count; %i++)
	{
		%data = %this.item[%i];
		%checktitle = getField(%data,0);

		if(%checktitle $= %title)
		{
			info("Removing Item",%data,"Match Title: ",%title);
			%this.removeItem(%i);
			return;

		}
	}

}
function WEMenuEditors::onSelectItem(%this, %id, %text)
{
	%cmd = getField(%this.item[%id], 2);
	%plugin = getField(%this.item[%id], 1);

	if (isObject(%plugin))
	{
		Lab.setEditor(%plugin);

		return true;
	}

	if (%cmd !$= "")
	{
		eval(%cmd);
		return true;
	}

	return false;
}
function WEMenuEditors::disablePlugin(%this, %plugin)
{
	%count = %this.getItemCount();

	for(%i = 0; %i < %count; %i++)
	{
		%data = %this.item[%i];
		%checktitle = getField(%data,0);

		if(%checktitle $= %plugin.displayName)
		{
			info("Plugin diabled in menu already exist",%plugin.plugin);

			WEMenuEditors.checkItem(%i,false);
			WEMenuEditors.enableItem(%i,false);

			return true;
		}
	}

	return false;
}
function WEMenuEditors::enablePlugin(%this, %plugin)
{
	%count = %this.getItemCount();

	for(%i = 0; %i < %count; %i++)
	{
		%data = %this.item[%i];
		%checktitle = getField(%data,0);	

		if(%checktitle $= %plugin.displayName)
		{		
			WEMenuEditors.enableItem(%i,true);
			WEMenuEditors.checkItem(%i,true);
			return true;
		}
	}

	return false;
}
function WEMenuEditors::updatePlugin(%this, %plugin)
{
	if (%plugin.isEnabled)
		%this.enablePlugin(%plugin);
	else
		%this.disablePlugin(%plugin);

}
