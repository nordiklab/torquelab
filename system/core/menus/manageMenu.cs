//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Functions to manage various Menu States
//==============================================================================
//------------------------------------------------------------------------------

//==============================================================================
// Build both editors Main Menus. If force true, it will rebuilt if exist
// Should be use only manually as they should be built individually after
// their GUI is created
function Lab::buildEditorMenus(%this,%force)
{
	if(!isObject(WorldEdMenu) || %force)
		Lab.buildWorldMenus();

	if(!isObject(GuiEdMenu)|| %force)
		Lab.buildGuiMenus();
}

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//==============================================================================
//Editor Initialization callbacks
//==============================================================================

function Lab::updateUndoMenu(%this,%undoManager)
{

	%editMenu =  WorldEdMenu.findMenuTitle("Edit");
	%undoName = %undoManager.getNextUndoName();
	%redoName = %undoManager.getNextRedoName();
	%editMenu.setItemName(0, "Undo " @ %undoName);
	%editMenu.setItemName(1, "Redo " @ %redoName);
	%editMenu.enableItem(0, %undoName !$= "");
	%editMenu.enableItem(1, %redoName !$= "");

}

function Lab::insertWorldMenu(%this,%menu,%pos)
{

	WorldEdMenu.insert(%menu);

}
function Lab::removeWorldMenu(%this,%menu)
{
   LabGroup.add(%menu);
   return;
   //Can't find a built-in way to remove menu, simply rebuild the menu
   
   Lab.buildWorldMenus();

}
function Lab::insertGuiMenu(%this,%menu,%pos)
{

	WorldEdMenu.insert(%menu);

}
