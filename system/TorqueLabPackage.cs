//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// The TorqueLab Package is activated when Editor is open and deactivated when closed.
// You can add any function you want to behave differently while in the editor
//==============================================================================

package TorqueLabPackage
{
	function AIPlayer::npcThink(%this, %obj)
	{
		if(!$TLPack_npcThinkEchoed)
			info("AIPlayer::npcThink Stopped by TorqueLab. To disable overwrite, comment npcThink function in tlab/system/TorqueLabPackage.cs");

		$TLPack_npcThinkEchoed = true;
		Lab.npcThinkList = strAddWord(Lab.npcThinkList,%obj.getId(), true);
	}
	function AIPlayer::Think(%this, %obj)
	{
		if(!$TLPack_ThinkEchoed)
			info("AIPlayer::Think Stopped by TorqueLab. To disable overwrite, comment Think function in tlab/system/TorqueLabPackage.cs");

		$TLPack_ThinkEchoed = true;

		Lab.ThinkList = strAddWord(Lab.ThinkList,%obj.getId(), true);
	}
	function AIPlayer::openFire(%this, %obj, %tgt, %rtt)
	{
		if(LocalClientConnection.player !$= %tgt)
			Parent::openFire(%obj, %tgt, %rtt);
	}

	function PlayerData::damage(%this, %obj, %sourceObject, %position, %damage, %damageType)
	{
		if(isObject(LocalClientConnection.player))
			if(%obj.getId() $= LocalClientConnection.player.getId())
				return;

		Parent::damage(%obj, %sourceObject, %position, %damage, %damageType);
	}

	//==============================================================================
	//Commonly use from game scripts so added to call new sync function
	function serverCmdTogglePathCamera(%client, %val)
	{
		Lab.TogglePathCamera(%client);
	}
	function serverCmdToggleCamera(%client)
	{
		Lab.toggleControlObject(%client);
	}

	function serverCmdSetEditorCameraPlayer(%client)
	{
		Lab.setCameraView("FirstPerson");

	}

	function serverCmdSetEditorCameraPlayerThird(%client)
	{
		Lab.setCameraView("ThirdPerson");
	}

	function serverCmdDropPlayerAtCamera(%client)
	{
		Lab.DropPlayerAtCamera();
	}

	function serverCmdDropCameraAtPlayer(%client)
	{
		Lab.DropCameraAtPlayer();
	}

	function serverCmdCycleCameraFlyType(%client)
	{
		Lab.CycleCameraFlyType(%client);

	}

	function serverCmdSetEditorCameraStandard(%client)
	{
		Lab.setCameraView("Standard");
	}

	function serverCmdSetEditorCameraNewton(%client)
	{
		Lab.setCameraView("Newton");
	}

	function serverCmdSetEditorCameraNewtonDamped(%client)
	{
		Lab.setCameraView("NewtonDamped");
	}

	function serverCmdSetEditorOrbitCamera(%client)
	{
		Lab.setCameraView("Orbit");
	}

	function serverCmdSetEditorFlyCamera(%client)
	{
		Lab.setCameraView("Standard");
	}

	function serverCmdEditorOrbitCameraSelectChange(%client, %size, %center)
	{
		Lab.EditorOrbitCameraChange(%client, %size, %center);
	}

	function serverCmdEditorCameraAutoFit(%client, %radius)
	{
		Lab.setCameraAutoFit(%radius);
	}

	function EditorDoExitMission(%saveFirst)
	{
		Lab.DoExitMission(%saveFirst);
	}

	function EditorGui::syncCameraGui(%this)
	{
		Lab.syncCameraGui();
	}
	function loadMission(%missionName, %isFirstMission )
	{
		//Clear the enabled editor level so the newLevel launch is called all time
		Lab.levelName = "";
		Parent::loadMission(%missionName,%isFirstMission);
	}

};

package EditorDisconnectOverride
{
	function disconnect()
	{
		if(isObject(Editor) && Editor.isEditorEnabled())
		{
			if(isObject($Cfg_TLab_defaultGui))
				Editor.close($Cfg_TLab_defaultGui);
		}

		Parent::disconnect();
	}
};
activatePackage(EditorDisconnectOverride);

package TorqueLabCoreOveride
{
	function EditorGui::syncCameraGui(%this)
	{
		Lab.syncCameraGui();
	}
};
activatePackage(TorqueLabCoreOveride);
