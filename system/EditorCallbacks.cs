//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function Lab::onEditorOpen(%this,%type)
{
	freeDeviceAction(keyboard,lalt,"editorMap");
	freeDeviceAction(keyboard,"alt c","editorMap");

	//Restore possible conflicted binds
	editorMap.bindCmd(keyboard,"alt c","Lab.toggleControlObject();","");

	globalActionMap.unbind(mouse, button1);

	$LabEditorActiveType = %type;
	$LabEditorActive = true;
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::onEditorClose(%this,%type)
{
	restoreDeviceAction(keyboard,lalt,"editorMap");
	restoreDeviceAction(keyboard,"alt c","editorMap");

	//globalActionMap.bind(mouse, button1, toggleCursor);
	$LabEditorActiveType = "";
	$LabEditorActive = false;

	if($LabEditor_RestoreCursor)
		showCursor();
	else
		hideCursor();

	hideCursor();

	Canvas.cursorOff();
}
//------------------------------------------------------------------------------

//==============================================================================
// Editor onWake/OnSleep Callbacks
//==============================================================================
//==============================================================================
function Lab::onEditorWake(%this,%type)
{
	$LabEditorAwake = true;
	$LabEditorAwakeType = %type;	
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::onEditorSleep(%this,%type)
{
	$LabEditorAwake = false;
   $LabEditorAwakeType = "";	
}
//------------------------------------------------------------------------------
//==============================================================================
//==============================================================================
function Lab::onWorldEditorOpen(%this)
{
	Lab.onEditorOpen("World");	

	//Check for methods in each plugin objects
	foreach(%pluginObj in EditorPluginSet)
	{
		if (%pluginObj.isMethod("onEditorOpen"))
			%pluginObj.onEditorOpen();

		//Lab.addMenuItemLast(WEMenuEditors,%pluginObj.displayName,"","");

		WEMenuEditors.addPlugin(%pluginObj);
	}

}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::onWorldEditorWake(%this)
{  
	Lab.onEditorWake("World");
	Lab.buildWorldMenus();
	 EditorMap.push();
	 WorldEdMap.push();
   //Canvas.setMenuBar(WorldEdMenu);
	//Check for methods in each plugin objects
	foreach(%pluginObj in EditorPluginSet)
	{
		if (%pluginObj.isMethod("onEditorWake"))
			%pluginObj.onEditorWake();
	}

	if (LocalClientConnection.camera.editorTransform !$= "")
		LocalClientConnection.camera.setTransform(LocalClientConnection.camera.editorTransform);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::onWorldEditorClose(%this)
{
	Lab.onEditorClose("World");

	if (isObject(HudChatEdit))
		HudChatEdit.close();

	LocalClientConnection.camera.editorTransform = LocalClientConnection.camera.getTransform();

	//Restore the Client COntrolling Object
	if (isObject(Lab.clientWasControlling))
		LocalClientConnection.setControlObject(Lab.clientWasControlling);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::onWorldEditorSleep(%this)
{
	Lab.onEditorSleep("World");

	// Remove the editor's ActionMaps.
	EditorMap.pop();
	MoveMap.pop();

	// Notify the editor plugins that the editor will be closing.
	foreach(%plugin in EditorPluginSet)
		%plugin.onEditorSleep();
		
   if (isObject(WorldEdMenu))   
      WorldEdMenu.destroy(); 
     
}
//------------------------------------------------------------------------------

//==============================================================================
// GUI EDITOR CALLBACKS
//==============================================================================

//==============================================================================
function Lab::onGuiEditorOpen(%this)
{
	Lab.onEditorOpen("Gui");
	//GuiEdMap.schedule(100,push);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::onGuiEditorWake(%this)
{
 
	Lab.onEditorWake("Gui");
 Lab.buildGuiMenus();
   GuiEdMap.push();
 GuiEdMenuMap.push();
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::onGuiEditorClose(%this)
{
	Lab.onEditorClose("Gui");
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::onGuiEditorSleep(%this)
{
    GuiEdMap.pop();
 GuiEdMenuMap.pop();
   
	Lab.onEditorSleep("Gui");
	 if (isObject(GuiEdMenu))   
      GuiEdMenu.destroy(); 
}
//------------------------------------------------------------------------------

