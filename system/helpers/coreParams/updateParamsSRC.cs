//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function autoUpdateParam(%field,%value,%paramObj,%callback)
{
	if (%callback !$="")
		eval(%callback);

	%syncObjs = %paramObj.syncObjs[%field];

	//If no object to sync, there might be a prefGroup to use
	if (%syncObjs $= "" && %paramObj.prefGroup !$= "")
	{
		eval(%paramObj.prefGroup@%field@" = %value;");
	}

	for(; %i< getFieldCount(%syncObjs); %i++)
	{
		%data = getField(%syncObjs,%i);

		if (isObject(%data))
		{
			%data.setFieldValue(%field,%value);
		}
		else if (strstr(%data,");") !$= "-1")
		{
			//Seem to be a function so replace ** with the value
			%function = strreplace(%data,"**",%value);
			eval(%function);
		}
		else if (getSubStr(%data,0,1) $= "$")
		{
			//Seem to be a function so replace ** with the value
			%data = strreplace(%data,"\"","");
			eval(%data@" = %value;");
		}
		// Check for special object field name EX: EWorldEditor.stickToGround
		else if (strstr(%data,".") !$= "-1")
		{
			//Seem to be a function so replace ** with the value
			eval(%data@" = %value;");
		}
	}
}

//==============================================================================
function syncParamObj(%paramObj)
{
	foreach$(%field in %paramObj.fieldList)
	{
		if (%paramObj.skipField[%field])
			continue;

		%value = "";
		%syncObjs = %paramObj.syncObjs[%field];
		//If no object to sync, there might be a prefGroup to use
		%firstObj = getWord(%syncObjs,0);

		if (%syncObjs $= "" && %paramObj.prefGroup !$= "")
		{
			eval("%value = "@%paramObj.prefGroup@%field@";");
		}
		else if (isObject(%firstObj))
		{
			%value = %firstObj.getFieldValue(%field);
		}
		else if (getSubStr(%firstObj,0,1) $= "$")
		{
			//Seem to be a function so replace ** with the value
			%fix = strreplace(%firstObj,"\"","");
			eval("%value = "@%fix@";");
		}
		// Check for special object field name EX: EWorldEditor.stickToGround
		else if (strstr(%firstObj,".") !$= "-1")
		{
			//Seem to be a function so replace ** with the value
			eval("%value = "@%firstObj@";");
		}

		if (%value $= "") continue;

		%ctrl = %paramObj.baseGuiControl.findObjectByInternalName(%field,true);
		%ctrl.setTypeValue(%value);
	}
}
//------------------------------------------------------------------------------

