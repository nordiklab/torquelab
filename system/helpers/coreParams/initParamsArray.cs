//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$Params::EnableLogs = 0;

//==============================================================================
function paramLog(%log1,%log2,%log3,%log4,%log5,%log6,%log7,%log8,%log9)
{
	if (!$pref::Console::ShowParamLog)
		return;

	info("ParamLog->",%log1,%log2,%log3,%log4,%log5,%log6,%log7,%log8,%log9);
}
//------------------------------------------------------------------------------
