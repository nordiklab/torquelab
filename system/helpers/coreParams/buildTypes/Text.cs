//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function buildParamText(%pData)
{
	%pData.pill-->field.text = %pData.Title;
	//TextEdit ctrl update
	%textValue = %pData.pill-->value;

	%textValue.text = %pData.Default;

	%textValue.internalName = %pData.InternalName;
}
//------------------------------------------------------------------------------
