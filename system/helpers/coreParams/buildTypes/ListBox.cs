//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function buildParamListBox(%pData)
{
	%pData.pill-->field.text = %pData.Title;

	if (%pData.Variable !$= "")
		eval("%value = "@%pData.Variable@";");

	//Dropdown ctrl updare
	%list = %pData.pill-->list;

	if (!isObject(%list))
	{
		warnLog("Invalid ListBox param source for", %pData.Title);
		return;
	}

	%list.command = %pData.Command;
	%list.altCommand = %pData.AltCommand;
	%list.internalName = %pData.InternalName;
	%listId = 0;

	if (%pData.Option[%pData.Setting,"itemList"] !$= "")
	{
		%dataList = %pData.Option[%pData.Setting,"itemList"];
		eval("%items = "@%dataList@";");

		foreach$(%item in %items)
		{
			%list.insertItem(%item,%list.getItemCount());
			%listId++;
		}
	}
	else if (%pData.Option[%pData.Setting,"fieldList"] !$= "")
	{
		%dataList = %pData.Option[%pData.Setting,"fieldList"];
		eval("%items = "@%dataList@";");

		for(%i=0; %i<getFieldCount(%items); %i++)
		{
			%name = getField(%items,%i);
			%list.insertItem(%name,%list.getItemCount());
			%listId++;
		}
	}
	else if (%pData.Option[%pData.Setting,"objList"] !$= "")
	{
		%dataList = %pData.Option[%pData.Setting,"objList"];

		foreach$(%obj in %dataList)
		{
			%list.insertItem(%obj.getName(),%list.getItemCount());
			%listId++;
		}
	}
	else if (%pData.Option[%pData.Setting,"itemGroup"] !$= "")
	{
		%itemGroup = %pData.Option[%pData.Setting,"itemGroup"];

		if (!isObject(%itemGroup))
		{
			warnLog("Invalid menu items group supplied:",%itemGroup,"No items have been added!");
			return;
		}

		foreach(%obj in %itemGroup)
		{
			%list.insertItem(%obj.getName(),%list.getItemCount());
			%listId++;
		}
	}

	return;
}
//------------------------------------------------------------------------------
