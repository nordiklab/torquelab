//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function buildParamCheckbox(%pData)
{
	//Checkbox ctrl update
	%checkbox = %pData.pill-->checkbox;
	%checkbox.text = "";
	%checkbox.command = %pData.Command;
	%checkbox.altCommand = %pData.AltCommand;
	%checkbox.internalName = %pData.InternalName;
	%checkbox.variable = %pData.Variable;

	//If no field ctrl, set the title on checkbox
	if (isObject(%pData.pill-->field))
		%pData.pill-->field.text = %pData.Title;
	else
		%checkbox.text = %pData.Title;
}
//------------------------------------------------------------------------------
