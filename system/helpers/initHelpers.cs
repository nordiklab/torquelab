//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$HLab_ScreenshotsFolder = "core/dump/screenshots/";
//-----------------------------------------------------------------------------
// Load up our main GUI which lets us see the game.

//==============================================================================
// Start by calling the old tools so if overide exist, new will be used
//exec("tlab/system/core/helpers/initHelpers.cs");
//============================================================================
//Taking screenshot
function loadToolsHelpers(%onlyEditorHelpers)
{
	//loadOldToolsHelpers();

	%pattern = "./*.cs";

	for(%file = findFirstFile(%pattern); %file !$= ""; %file = findNextFile(%pattern))
	{
		if (fileBase(%file) $= "initHelpers" || strpos(%file,"_src/") !$= "-1")
			continue;

      //Skip files in which external/ is in the path (Local dev)
      if (%onlyEditorHelpers && strpos(%file,"external/") !$= "-1")
      {
            //info("Helpers needed for TorqueLab only are loaded. External skipped:",fileBase(%file));
            continue;
      }
		exec(%file);
	}

	%pattern = "./*.cs.dso";

	for(%file = findFirstFile(%pattern); %file !$= ""; %file = findNextFile(%pattern))
	{
		if (fileBase(%file) $= "initHelpers") continue;

		exec(%file);
	}

	//if (!isObject(LabHelpersDlg))
	//exec("tlab/system/helpers/LabHelpersDlg.gui");
	$HelperLabLoaded = true;
}
//----------------------------------------------------------------------------
//Load the helpers on execution
loadToolsHelpers(1);

function helpersDlg()
{
	if (!isObject(LabHelpersDlg))
		exec("tlab/system/helpers/LabHelpersDlg.gui");

	toggleDlg(LabHelpersDlg);
}

