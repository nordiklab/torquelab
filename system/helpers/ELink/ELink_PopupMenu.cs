//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function ELink::menuAdd(%this,%linkName,%text,%id)
{
	%linkObjs = %this.getVal(%linkName);

	if(%linkObjs $= "")
		return;

	foreach$(%obj in %linkObjs)
	{
	   if (%obj.findText(%text) !$= "-1")
			continue;
			
		if (!%this.isValidObject(%linkName,%obj,"GuiPopUpMenuCtrl GuiPopUpMenuCtrlEx"))
			continue;

		%obj.add(%text,%id);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function ELink::menuClear(%this,%linkName)
{
	%linkObjs = %this.getVal(%linkName);

	if(%linkObjs $= "")
		return;

	foreach$(%obj in %linkObjs)
	{
		if (!%this.isValidObject(%linkName,%obj,"GuiPopUpMenuCtrl GuiPopUpMenuCtrlEx"))
			continue;

		%obj.clear();
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Set the item selected on all menus (if %doCallback true would only be set for
// first menu since they would all call the same onSelect function)
function ELink::menuSelect(%this,%linkName,%id,%doCallback)
{
	if (%doCallback $= "")
		%doCallback = true;

	%linkObjs = %this.getVal(%linkName);

	if(%linkObjs $= "")
		return;

	foreach$(%obj in %linkObjs)
	{
		if (!%this.isValidObject(%linkName,%obj,"GuiPopUpMenuCtrl GuiPopUpMenuCtrlEx"))
			continue;

		if (%obj.getSelected() == %id)
			continue;

		%obj.setSelected(%id,%doCallback);
		%doCallback = false;
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Search first menu for text and return the id if found or -1
function ELink::menuFindText(%this,%linkName,%text)
{
	if (%doCallback $= "")
		%doCallback = true;

	%linkObjs = %this.getVal(%linkName);

	if(%linkObjs $= "")
		return;

	foreach$(%obj in %linkObjs)
	{
		if (!%this.isValidObject(%linkName,%obj,"GuiPopUpMenuCtrl GuiPopUpMenuCtrlEx"))
			continue;

		return %obj.findText(%text);
	}
}
//------------------------------------------------------------------------------
