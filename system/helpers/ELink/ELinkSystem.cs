//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Callback from code when a GuiControl is added with a eLink value. By using this
// callback, it avoid the issues where onAdd is used for a eLinked control
//------------------------------------------------------------------------------
function GuiControl::onAddElink(%this)
{
	%linkName = %this.eLink;

	if (%linkName $= "")
		return;

	ELink.link(%this,%linkName);
}
//------------------------------------------------------------------------------

//==============================================================================
// Prebuilt functions for ToolIcon icons
//==============================================================================
if (!isObject(ELink))
	new ArrayObject(ELink)
{
	uniqueKeyWords = true;
	logging = false;
};

//==============================================================================
//Add the object ID to the linkName object list
function ELink::link(%this,%obj,%linkName)
{
	//If no linkName supplied, try to use the obj eLink field
	if (%linkName $= "")
		%linkName = %obj.eLink;

	if (%linkName $= "" || !isObject(%obj))
		return;

	if (%this.exist(%linkName,%obj.getId()))
		return;

	//Use special ArrayObject addWord to add a unique word to the list
	%this.addWord(%linkName,%obj.getId());
	%this.log("Elink updated:",%linkName,"Added:",%obj.getId());

}

//------------------------------------------------------------------------------

//==============================================================================
//Remove the object ID from the linkName object list
function ELink::unlink(%this,%obj,%linkName)
{
	if (%linkName $= "" || !isObject(%obj))
		return;

	//Use special ArrayObject removeWord to Remove a word from the list
	%this.removeWord(%linkName,%obj.getId());
}
//------------------------------------------------------------------------------

//==============================================================================
//Remove the object ID from the linkName object list
function ELink::list(%this,%linkName)
{
	return  %this.getVal(%linkName);
}
//------------------------------------------------------------------------------

//==============================================================================
//Remove the object ID from the linkName object list
function ELinked(%linkName)
{
	return ELink.list(%linkName);
}
//------------------------------------------------------------------------------

//==============================================================================
// Call a method with supplied arguments on all linked objects
function ELink::doCall(%this, %linkName,%method, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9)
{
	%linkObjs = %this.getVal(%linkName);

	if (%linkObjs $= "")
		return;

	//Need to use eval since some method can't have more arguments even if empty
	%evalStr = "."@%method@"(";
	%i=1;

	while(%arg[%i] !$="")
	{
		%evalStr = %evalStr@%comma@"\""@%arg[%i]@"\"";
		%comma = ", ";
		%i++;
	}

	%evalStr =  %evalStr @");";

	foreach$(%obj in %linkObjs)
	{
		if (!isObject(%obj))
		{
			%this.removeWord(%linkName,%obj);
			continue;
		}

		if (!%obj.isMethod(%method))
			continue;

		eval("%obj"@%evalStr);
	}
}
//==============================================================================
// Eval each linked objects with supplied eval. $ThisObject or $This will be
// replaced with object ID and it will be evaluated as it on all objects
// If no $ThisObject (or $This) found, it will be assument that the supplied eval is a method
// and a dot (.) will be added if not found at start of method
function ELink::doEval(%this, %linkName,%eval)
{
	%linkObjs = %this.getVal(%linkName);

	if (%linkObjs $= "")
		return;

	%replaceObj = %this.getObjReplacement(%eval);

	//If not replace Obj mode, make sure it start with a dot
	if (%replaceObj $= "")
	{
		//Validate the method
		%startChar = getSubStr(%eval,0,1);

		if ( %startChar !$= ".")
			%eval = "."@%eval;
	}

	%lastChar = getSubStr(%eval,strlen(%eval)-1,1);

	if (%lastChar !$= ";")
		%eval = %eval@";";

	foreach$(%obj in %linkObjs)
	{
		if (!isObject(%obj))
		{
			%this.removeWord(%linkName,%obj);
			continue;
		}

		if (%replaceObj !$= "")
			eval(strreplace(%eval,%replaceObj,%obj));
		else
			eval("%obj"@%eval);

	}
}
//==============================================================================
// Common Function to deal with ToolIcon and ToolbarIcon callbacks
//==============================================================================
//==============================================================================
// Eval each linked objects with supplied eval. $ThisObject or $This will be
// replaced with object ID and it will be evaluated as it on all objects
// If no $ThisObject (or $This) found, it will be assument that the supplied eval is a method
// and a dot (.) will be added if not found at start of method
function ELink::getObjReplacement(%this, %checkString)
{
	//Got some issue with case during console test as it was always keeping previous case
	//This check various possibility of $ThisObject and $This in a string
	if (strstr(%checkString,"$thisObject") !$= "-1")
		%replaceObj = "$thisObject";
	else if (strstr(%checkString,"$ThisObject") !$= "-1")
		%replaceObj = "$ThisObject";
	else if (strstr(%checkString,"$thisobject") !$= "-1")
		%replaceObj = "$thisobject";
	else if (strstr(%checkString,"$this") !$= "-1")
		%replaceObj = "$this";

	else if (strstr(%checkString,"$This") !$= "-1")
		%replaceObj = "$This";

	return %replaceObj;
}

//==============================================================================
function ELink::isValidObject(%this,%linkName,%obj,%classes)
{
	if(!isObject(%obj))
	{
		%this.removeWord(%linkName,%obj);
		return false;
	}

	if (%classes $= "")
		return true;

	foreach$(%class in %classes)
	{
		if(%obj.isMemberOfClass(%class) )
			return true;
	}

	//Obj not matching supplied classes
	return false;
}
//------------------------------------------------------------------------------

//==============================================================================
//Remove the object ID from the linkName object list
function ELink::exist(%this,%linkName,%objId)
{
	%list = %this.getVal(%linkName);

	return strFindWords(%list,%objId);
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function ELink::dumpLink(%this,%linkName,%objId)
{
	%id = %this.moveFirst();

	while(true )
	{
		%outval = "";
		%key = %this.getKey(%id);
		%values = %this.getValue(%id);

		foreach$(%value in %values)
			%outval = strAddWord(%outval,%value.getName());

		info(%key,"-->",%outval);
		%id = %this.moveNext();

		if (%id <= 0)
			break;

	}

}
//------------------------------------------------------------------------------
//==============================================================================
// Add a word to the key value, if unique only add if not found. Works similar
// to the strAddWord function. If no key exist, it will be created
function ELink::addWord(%this,%key, %word,%isUnique)
{
	loge("ELink::addWord(%this,%key,%inc)",%this,%key,%inc);

	if (%this.uniqueKeyWords)
		%isUnique = true;

	%index = %this.getIndexFromKey(%key);

	if (%index $= "-1")
	{
		//The key is new so simply  set a new key/value with the word alone
		%this.add(%key,%word);
		return %word;
	}

	//Get the existing value
	%value = %this.getValue(%index);
	%value = strAddWord(%value,%word,%isUnique);
	%this.setValue(%value,%index);

	return %value;
}
//------------------------------------------------------------------------------
//==============================================================================
// Add a word to the key value, if unique only add if not found. Works similar
// to the strAddWord function. If no key exist, it will be created
function ELink::removeWord(%this,%key, %word)
{
	loge("ELink::removeWord(%this,%key,%inc)",%this,%key,%inc);

	%index = %this.getIndexFromKey(%key);

	if (%index $= "-1")
	{
		return "";
	}

	//Get the existing value
	%value = %this.getValue(%index);
	%value = strRemoveWord(%value,%word);
	%this.setValue(%value,%index);

	return %value;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function ELink::fixLink(%this,%linkName,%objId)
{
	%id = %this.moveFirst();

	while(true )
	{
		%key = %this.getKey(%id);
		%values = %this.getValue(%id);

		foreach$(%value in %values)
			%allVal[%key] = strAddWord(%allVal[%key],%value.getName());

		if (%exist[%key])
		{
			info("Duplicate key will be removed",%key);
			%this.erase(%id);
			continue;

		}

		%exist[%key] = true;
		%this.setVal(%key,%allVal[%key]);
		%id = %this.moveNext();

		if (%id <= 0)
			break;

	}

}
//------------------------------------------------------------------------------
//==============================================================================
//Add the object ID to the linkName object list
function ELink::setLogMode(%this,%logMode)
{
	if (%logMode $= "" || !%logMode)
		%logMode = false;
	else
		%logMode = true;

	%this.logging = %logMode;
}
//------------------------------------------------------------------------------
//==============================================================================
// Toggle a Dialog GUI
function ELink::log(%this,%text,%a1, %a2, %a3, %a4,%a5, %a6, %a7, %a8,%a9, %a10, %a11)
{
	if (%this.logging)
		info("ELink",%text,%a1, %a2, %a3, %a4,%a5, %a6, %a7, %a8,%a9, %a10, %a11);
}
//------------------------------------------------------------------------------
