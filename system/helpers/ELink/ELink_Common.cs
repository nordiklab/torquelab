//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function ELink::setText(%this,%linkName,%text)
{
	%linkObjs = %this.getVal(%linkName);

	if(%linkObjs $= "")
		return;

	foreach$(%obj in %linkObjs)
	{
		if (!%this.isValidObject(%linkName,%obj))
			continue;

		if (%obj.isMethod("setText"))
			%obj.setText(%text);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function ELink::getText(%this,%linkName)
{
	%linkObjs = %this.getVal(%linkName);

	if(%linkObjs $= "")
		return;

	foreach$(%obj in %linkObjs)
	{
		if (!%this.isValidObject(%linkName,%obj))
			continue;

		return %obj.getText();
	}
}
//------------------------------------------------------------------------------
function ELink::setTypeValue(%this,%linkName,%value)
{
	%linkObjs = %this.getVal(%linkName);

	if(%linkObjs $= "")
		return;

	foreach$(%obj in %linkObjs)
	{
		if (!%this.isValidObject(%linkName,%obj))
			continue;
      //Special method that set the proper value for object class
      if (%obj.isMethod("setTypeValue"))		
			%obj.setTypeValue(%value);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// TorqueLab Plugin Tools Container (Side settings area)
//==============================================================================

//==============================================================================
//Show all linked objects
function Elink::show(%this,%linkName)
{
	%linkObjs = %this.getVal(%linkName);

	if(%linkObjs $= "")
		return;

	foreach$(%obj in %linkObjs)
	{
		if(!isObject(%obj))
		{
			%this.removeWord(%linkName,%obj);
			continue;
		}

		show(%obj);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
//Show all linked objects
function Elink::hide(%this,%linkName)
{
	%linkObjs = %this.getVal(%linkName);

	if(%linkObjs $= "")
		return;

	foreach$(%obj in %linkObjs)
	{
		if(!isObject(%obj))
		{
			%this.removeWord(%linkName,%obj);
			continue;
		}

		hide(%obj);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
//Show all linked objects
function Elink::toggleVisible(%this,%linkName)
{
	%linkObjs = %this.getVal(%linkName);

	if(%linkObjs $= "")
		return;

	foreach$(%obj in %linkObjs)
	{
		if(!isObject(%obj))
		{
			%this.removeWord(%linkName,%obj);
			continue;
		}

		//SceneObject use hidden instead of visible
		if(%obj.isField("hidden"))
			%visible = !%obj.hidden;
		else if(%obj.isField("visible"))
			%visible = !%obj.visible;

		if(%visible)
			hide(%obj);
		else
			show(%obj);

	}
}
//------------------------------------------------------------------------------

//==============================================================================
//Show all linked objects
function Elink::fitParents(%this,%linkName)
{
	%linkObjs = %this.getVal(%linkName);

	if(%linkObjs $= "")
		return;

	foreach$(%obj in %linkObjs)
	{
		if(!isObject(%obj))
		{
			%this.removeWord(%linkName,%obj);
			continue;
		}

		%obj.fitIntoParent();

	}
}
//------------------------------------------------------------------------------

//==============================================================================
//Show all linked objects
function ELinkControlSC::onAdd(%this)
{
	%linkName = %this.superClass;

	if (%linkName $= "ELinkControl")
	{
		warnLog("ELinkControlSC::OnAdd","Have ELinkControl as superClass which is wrong, should be set as class. Add individually if can't use class","Problematic ID:",%this);
		return;
	}

	ELink.link(%this,%linkName);
}
//------------------------------------------------------------------------------

//==============================================================================
//Show all linked objects
function ELinkControl::onAdd(%this)
{
	%linkName = %this.internalName;

	if (%linkName $= "")
	{
		warnLog("ELinkControl::OnAdd","Have no internalName which is used as linkName so no link can be created","Problematic ID:",%this);
		return;
	}

	ELink.link(%this,%linkName);
}
//------------------------------------------------------------------------------

