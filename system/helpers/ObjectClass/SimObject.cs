//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// SimObject - Deletion Helpers
//==============================================================================

//------------------------------------------------------------------------------

//==============================================================================
// ScriptObject Manipulation
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// SetVal -> Set a field value for a type
function SimObject::setVal(%this, %field,%value,%type)
{
	if (%type $="")
		%type = "Default";

	%this.setFieldType(%field, %type);
	%this.setFieldValue(%field, %value);
	return true;
}

//------------------------------------------------------------------------------
/// GetVal -> Return the value of field
function SimObject::getVal(%this, %field)
{
	%return = %this.getFieldValue(%field);
	return %return;
}

//------------------------------------------------------------------------------
/// GetType -> Return field type of obj field
function SimObject::getType(%this, %field)
{
	if (%type $="")
		%type = "Default";

	%return = %this.getFieldType(%field, %type);
	return %return;
}
//------------------------------------------------------------------------------
//==============================================================================
// SimObject - Debugging and console report
//==============================================================================

//==============================================================================
// Default onDefineIcons functions (Make sure to set valid path to images)
function SimObject::getDisplayText( %this,%method )
{
	if (%method $= "")
	{
		%text = %this.getName();
		if (%text $= "")
		{
			%text = %this.internalName;
			if (%text $= "")
				%text = %this.getId();
		}
		return %text;
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Default onDefineIcons functions (Make sure to set valid path to images)
function SimObject::getDisplayName( %this,%noID )
{
	%dispName = (%this.getName() !$= "") ? %this.getName() : %this.internalName;
	if (%dispName $= "" && %noID)
	   return "";
	   
   if (%dispName $= "") 
		%dispName = %this;
	return %dispName;
}
//------------------------------------------------------------------------------

//==============================================================================
// Basic Task Schedule for any SimObject Derived class (Based on AIPlayer Tasks)
//==============================================================================
//==============================================================================
function SimObject::pushTask(%this,%method)
{
	if (%this.taskIndex $= "")
	{
		%this.taskIndex = 0;
		%this.taskCurrent = -1;
	}
	%this.task[%this.taskIndex] = %method;
	%this.taskIndex++;
	if (%this.taskCurrent == -1)
		%this.executeTask(%this.taskIndex - 1);
}
//------------------------------------------------------------------------------
//==============================================================================
function SimObject::clearTasks(%this)
{
	%this.taskIndex = 0;
	%this.taskCurrent = -1;
}
//------------------------------------------------------------------------------
//==============================================================================
function SimObject::nextTask(%this)
{
	if (%this.taskCurrent != -1)
		if (%this.taskCurrent < %this.taskIndex - 1)
			%this.executeTask(%this.taskCurrent++);
		else
			%this.taskCurrent = -1;
}
//------------------------------------------------------------------------------
//==============================================================================
function SimObject::executeTask(%this,%index)
{
	%this.taskCurrent = %index;
	eval(%this.getId() @"."@ %this.task[%index] @";");
}
//------------------------------------------------------------------------------
//==============================================================================
function SimObject::wait(%this, %time)
{
	%this.schedule(%time * 1000, "nextTask");
}
//------------------------------------------------------------------------------
//==============================================================================
function SimObject::done(%this,%time)
{
	%this.schedule(0, "delete");
}
//------------------------------------------------------------------------------

//==============================================================================
/// Delete an object by making sure it exist
/// %obj - Object to be deleted
function SimObject::getGroupDepth(%this,%group)
{
   return;
	if (!isObject(%group))
		return "-1";
	%depth = 0;
	%obj = %this;
	while(isObject(%obj.parentGroup))
	{
		%depth++;
		if ( %obj.parentGroup.getId() $= %group.getId())
			return %depth;
		%obj = %obj.parentGroup;
	}
	return "-1";
}
//------------------------------------------------------------------------------
//==============================================================================
function SimObject::setFirst(%this)
{
	if (isObject(%this.parentGroup))
	   %this.parentGroup.bringToFront(%this);
}
//------------------------------------------------------------------------------
//==============================================================================
function SimObject::setLast(%this)
{
	if (isObject(%this.parentGroup))
	   %this.parentGroup.pushToBack(%this);
}
//------------------------------------------------------------------------------
//==============================================================================
function SimObject::setAfter(%this,%objBefore)
{
	if (!isObject(%this.parentGroup) || !isObject(%objBefore))
	   return  false;
   if (!%this.parentGroup.isMember(%objBefore))
      return false;
   
   %this.parentGroup.reorderChild(%objBefore, %this);
	 
}
//------------------------------------------------------------------------------
//==============================================================================
function SimObject::setBefore(%this,%objAfter)
{
	if (!isObject(%this.parentGroup) || !isObject(%objAfter))
	   return  false;
   if (!%this.parentGroup.isMember(%objAfter))
      return false;
   
   %this.parentGroup.reorderChild(%this, %objAfter);
	 
}
//------------------------------------------------------------------------------
