//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//------------------------------------------------------------------------------
/// SimSet Creator function
function SimSet::collapseAll(%this )
{
	foreach(%item in %this)
	{
	   if (%item.isMemberOfClass(GuiRolloutCtrl))
		   %item.expanded = 0;
	}
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
/// SimSet Creator function
function SimSet::expandAll(%this )
{
	foreach(%item in %this)
	{
	   if (%item.isMemberOfClass(GuiRolloutCtrl))
		%item.expanded = 1;
	}
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// SimSet Creator function
function SimSet::toggleExpand(%this )
{
	foreach(%item in %this)
	{
	   if (%item.isMemberOfClass(GuiRolloutCtrl))
		%item.expanded = !%item.expanded;
	}
}
//------------------------------------------------------------------------------
