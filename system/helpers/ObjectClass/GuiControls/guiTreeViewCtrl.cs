//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Default onDefineIcons functions (Make sure to set valid path to images)
function GuiTreeViewCtrl::onDefineIcons( %this )
{
	%icons = $Cfg_CoreImages_Folder @ "/tables/TreeViewBase/default:" @
	         $Cfg_CoreImages_Folder @ "/tables/TreeViewBase/simgroup:" @
	         $Cfg_CoreImages_Folder @ "/tables/TreeViewBase/simgroup_closed:" @
	         $Cfg_CoreImages_Folder @ "/tables/TreeViewBase/simgroup_selected:" @
	         $Cfg_CoreImages_Folder @ "/tables/TreeViewBase/simgroup_selected_closed:" @
	         $Cfg_CoreImages_Folder @ "/tables/TreeViewBase/hidden:" @
	         $Cfg_CoreImages_Folder @ "/tables/TreeViewBase/shll_icon_passworded_hi:" @
	         $Cfg_CoreImages_Folder @ "/tables/TreeViewBase/shll_icon_passworded:" @
	         $Cfg_CoreImages_Folder @ "/tables/TreeViewBase/default";
	%this.buildIconTable(%icons);
}
//------------------------------------------------------------------------------

function GuiTreeViewCtrl::handleRenameObject( %this, %name, %obj )
{
	%inspector = GuiInspector::findByObject( %obj );

	if( isObject( %inspector ) )
	{
		%field = ( %this.renameInternal ) ? "internalName" : "name";
		%inspector.setObjectField( %field, %name );
		return true;
	}

	return false;
}

function GuiTreeViewCtrl::deselectObject( %this, %obj )
{
	%item	= SceneEditorTree.findItemByObjectId(%obj.getId());
	if (%item $= "-1")
	   return;
   %this.selectItem(%item,false);
	
}

function GuiTreeViewCtrl::selectObject( %this, %obj )
{
	%item	= SceneEditorTree.findItemByObjectId(%obj.getId());
	if (%item $= "-1")
	   return;
   %this.selectItem(%item,true);
	
}

/*
void 	addSelection (int id, bool isLastSelection=true)
 	Add an item/object to the current selection.
bool 	buildIconTable (builds an icon table)
void 	clear ()
 	empty tree
void 	clearFilterText ()
 	Clear the current item filtering pattern.
void 	clearSelection ()
 	Unselect all currently selected items.
void 	deleteSelection ()
 	Delete all items/objects in the current selection.
bool 	editItem (TreeItemId item, string newText, string newValue)
bool 	expandItem (TreeItemId item, bool expand=true)
int 	findChildItemByName (int parentId, string childName)
 	Get the child item of the given parent item whose text matches childName.
int 	findItemByName (string text)
 	Get the ID of the item whose text matches the given text.
int 	findItemByObjectId (find item by object id and returns the mId)
int 	findItemByValue (string value)
 	Get the ID of the item whose value matches value.
int 	getChild (TreeItemId item)
string 	getFilterText ()
 	Get the current filter expression. Only tree items whose text matches this expression are displayed. By default, the expression is empty and all items are shown.
string 	getItemText (TreeItemId item)
string 	getItemValue (TreeItemId item)
int 	getNextSibling (TreeItemId item)
int 	getParent (TreeItemId item)
int 	getPrevSibling (TreeItemId item)
int 	getSelectedItem (int index=0)
 	Return the selected item at the given index.
int 	getSelectedObject (int index=0)
 	Return the currently selected SimObject at the given index in inspector mode or -1.
string 	getTextToRoot (TreeItemId item, Delimiter=none)
 	gets the text from the current node to the root, concatenating at each branch upward, with a specified delimiter optionally
void 	hideSelection (bool state=true)
 	Call SimObject::setHidden( state ) on all objects in the current selection.
int 	insertItem (int parentId, string text, string value="", string icon="", int normalImage=0, int expandedImage=0)
 	Add a new item to the tree.
bool 	isItemSelected (int id)
 	Check whether the given item is currently selected in the tree.
bool 	isParentItem (int id)
 	Returns true if the given item contains child items.
void 	lockSelection (bool lock=true)
 	Set whether the current selection can be changed by the user or not.
bool 	markItem (TreeItemId item, bool mark=true)
void 	moveItemDown (TreeItemId item)
void 	moveItemUp (TreeItemId item)
void 	open (SimSet obj, bool okToEdit=true)
 	Set the root of the tree view to the specified object, or to the root set.
bool 	removeItem (TreeItemId item)
void 	removeSelection (deselects an item)
void 	scrollVisible (TreeItemId item)
int 	scrollVisibleByObjectId (show item by object id.returns true if sucessful.)
bool 	selectItem (TreeItemId item, bool select=true)
void 	setDebug (bool value=true)
 	Enable/disable debug output.
void 	setFilterText (string pattern)
 	Set the pattern by which to filter items in the tree. Only items in the tree whose text matches this pattern are displayed.
void 	setItemImages (int id, int normalImage, int expandedImage)
 	Sets the normal and expanded images to show for the given item.
void 	setItemTooltip (int id, string text)
 	Set the tooltip to show for the given item.
void 	showItemRenameCtrl (TreeItemId id)
 	Show the rename text field for the given item (only one at a time).
void 	sort (int parent, bool traverseHierarchy=false, bool parentsFirst=false, bool caseSensitive=true)
 	Sorts all items of the given parent (or root). With 'hierarchy', traverses hierarchy.
void 	toggleHideSelection ()
 	Toggle the hidden state of all objects in the current selection.
void 	toggleLockSelection ()
 	Toggle the locked state of all objects in the current selection.
Callbacks

bool 	canRenameObject (SimObject object)
bool 	handleRenameObject (string newName, SimObject object)
bool 	isValidDragTarget (int id, string value)
void 	onAddGroupSelected (SimGroup group)
void 	onAddMultipleSelectionBegin ()
void 	onAddMultipleSelectionEnd ()
void 	onAddSelection (int itemOrObjectId, bool isLastSelection)
void 	onBeginReparenting ()
void 	onClearSelection ()
void 	onDefineIcons ()
bool 	onDeleteObject (SimObject object)
void 	onDeleteSelection ()
void 	onDragDropped ()
void 	onEndReparenting ()
void 	onInspect (int itemOrObjectId)
void 	onKeyDown (int modifier, int keyCode)
void 	onMouseDragged ()
void 	onMouseUp (int hitItemId, int mouseClickCount)
void 	onObjectDeleteCompleted ()
void 	onRemoveSelection (int itemOrObjectId)
void 	onReparent (int itemOrObjectId, int oldParentItemOrObjectId, int newParentItemOrObjectId)
void 	onRightMouseDown (int itemId, Point2I mousePos, SimObject object)
void 	onRightMouseUp (int itemId, Point2I mousePos, SimObject object)
void 	onSelect (int itemOrObjectId)
void 	onUnselect (int itemOrObjectId)
Public Attributes

void 	addChildSelectionByValue
 	addChildSelectionByValue(TreeItemId parent, value)
void 	buildVisibleTree
 	Build the visible tree.
void 	cancelRename
 	For internal use.
int 	getFirstRootItem
 	Get id for root item.
int 	getItemCount
string 	getSelectedItemList
 	returns a space seperated list of mulitple item ids
int 	getSelectedItemsCount
string 	getSelectedObjectList
 	Returns a space sperated list of all selected object ids.
void 	onRenameValidate
 	For internal use.
void 	removeAllChildren
 	removeAllChildren(TreeItemId parent)
void 	removeChildSelectionByValue
 	removeChildSelectionByValue(TreeItemId parent, value)
Inspector Trees

bool 	canRenameObjects
 	If true clicking on a selected item ( that is an object and not the root ) will allow you to rename it.
bool 	compareToObjectID
bool 	renameInternal
 	If true then object renaming operates on the internalName rather than the object name.
bool 	showClassNameForUnnamedObjects
 	If true, class names will be used as object names for unnamed objects.
bool 	showClassNames
 	If true, item text labels for objects will include class names.
bool 	showInternalNames
 	If true, item text labels for obje ts will include internal names.
bool 	showObjectIds
 	If true, item text labels for objects will include object IDs.
bool 	showObjectNames
 	If true, item text labels for objects will include object names.
TreeView

bool 	clearAllOnSingleSelection
bool 	deleteObjectAllowed
bool 	destroyTreeOnSleep
 	If true, the entire tree item hierarchy is deleted when the control goes to sleep.
bool 	dragToItemAllowed
bool 	fullRowSelect
int 	itemHeight
bool 	mouseDragging
bool 	multipleSelections
 	If true, multiple items can be selected concurrently.
bool 	showRoot
 	If true, the root item is shown in the tree.
int 	tabSize
int 	textOffset
bool 	tooltipOnWidthOnly
bool 	useInspectorTooltips
*/
