//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Called each time a GuiControl is loaded
/* Now builtin in code and use a specific onAddElink callback
function GuiControl::onAdd	(%this)
{		
    if (%this.eLink !$= "" )
		ELink.link(%this,%this.eLink);
 
}
*/
//------------------------------------------------------------------------------
//==============================================================================
// Called each time a GuiControl is loaded
function GuiControl::onRemove	(%this)
{
	//Check if want to assign to a guiGroup
	foreach$(%gui in %this.myLabGuis)
	{
		if (!isObject(%gui))
			continue;

		if(!%this.isMember(%gui))
			%this.add(%gui);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Called each time a GuiControl is loaded
function GuiControl::findHitInRect	(%this,%rect)
{
   %x = getWord(%rect,0); 
	%y = getWord(%rect,1);
	%width = getWord(%rect,2); 
	%height = getWord(%rect,3); 
	return %this.findHitControls(%x,%y,%width,%height);
}
//------------------------------------------------------------------------------
