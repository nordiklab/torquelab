//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Default onDefineIcons functions (Make sure to set valid path to images)
function SimSet::findChild( %this,%internalName,%notrecursive )
{
	return %this.findObjectByInternalName(%internalName,!%notrecursive);
}
//------------------------------------------------------------------------------

//==============================================================================
// Call a method with supplied arguments on all linked objects
function SimSet::doCall(%this, %method, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9)
{	
   foreach(%obj in %this)
      if (%obj.isMethod(%method))
         %obj.call(%method, %arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9);
   //Updated to support method validation for custom methods
	//%this.callOnChildrenNoRecurse(%method,%arg1, %arg2, %arg3, %arg4, %arg5, %arg6, %arg7, %arg8, %arg9);
}
//------------------------------------------------------------------------------

//==============================================================================
// SimSet/SimGroup - Children update
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// SimSet Creator function
function hideChilds(%dlg, %all)
{

	if (%all)
		eval(%dlg@".callOnChildren(\"setVisible\",false);");
	else
		eval(%dlg@".callOnChildrenNoRecurse(\"setVisible\",false);");
}
//------------------------------------------------------------------------------
/// SimSet Creator function
function SimSet::setChildField(%this, %field,%value )
{
	foreach(%item in %this)
	{
		%item.setFieldValue(%field,%value);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function SimSet::disableSave(%this)
{
	foreach(%obj in %this)
	{
		%this.baseCanSave[%obj.getId()] = %obj.canSave;
		%obj.canSave = "0";
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function SimSet::restoreSave(%this)
{
	foreach(%obj in %this)
	{
		if (%this.baseCanSave[%obj.getId()] !$= "")
			%obj.canSave = %this.baseCanSave[%obj.getId()];
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function SimSet::enableSave(%this)
{
	foreach(%obj in %this)
	{
		%obj.canSave = "1";
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// SimSet/SimGroup - Debugging , reporting and dumping
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// SimSet Creator function
function SimSet::dumpData(%this )
{
	foreach(%item in %this)
	{
		%index = %this.getObjectIndex(%item);
		%name = (%item.getName() !$= "") ? %item.getName() : %item.getInternalName();
		info("Simset Item", %item,"Name", %name, "Index = ",%index);
	}
}

//------------------------------------------------------------------------------
/// SimSet Creator function
function SimSet::exportToFile(%this,%file,%saveAs,%preAppend)
{
	%simSet = %this;

	if (%saveAs !$= "")
	{
		%simSet = %this.deepClone();
		%tempName = getUniqueName(%saveAs);
		%simSet.setName(%tempName);
		%deleteMe = true;
	}

	%simSet.save(%file,false,%preAppend);

	if (%deleteMe)
		delObj(%simSet);
}

//==============================================================================
/// Get the hierarchy depth of a SimGroup vs another SimGroup
/// %group - Group parent we are looking for
/// return- -1 if parent not found or the depth of the group vs the parent
function SimGroup::getParentDepth(%this,%group)
{  
	if (!isObject(%group))
		return "-1";
	%depth = 0;
	%obj = %this;
	while(isObject(%obj.parentGroup))
	{
		%depth++;
		if ( %obj.parentGroup.getId() $= %group.getId())
			return %depth;
		%obj = %obj.parentGroup;
	}
	return "-1";
}
//------------------------------------------------------------------------------
