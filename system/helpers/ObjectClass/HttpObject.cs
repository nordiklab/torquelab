//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function HTTPObject::onDNSFailed(%this)
{	
	// Store this state
	%this.lastState = "DNSFailed";
}

function HTTPObject::onConnectFailed(%this)
{	
	// Store this state
	%this.lastState = "ConnectFailed";
}

function HTTPObject::onDNSResolved(%this)
{
	// Store this state
	%this.lastState = "DNSResolved";
}

function HTTPObject::onConnected(%this)
{
	// Store this state
	%this.lastState = "Connected";
}

function HTTPObject::onDisconnect(%this)
{
	// Store this state
	%this.lastState = "Disconnected";		
}
function PhabWeb::onLine(%this,%line)
{
   Parent::online(%this,%line);
      
}
function HTTPObject::onLine(%this,%line)
{
   if (%this.echo)
      info(%line);
	// Store this state
	%this.lastState = "onLine" TAB %line;	
	
	// When HTTPObject have an onDataLine method
	// the line received are checked for field=value format
   // if a field and value is found, they will be sent to object
	if (%this.isMethod("onDataLine"))
	{
      // Print the line to the console
      %line = rtrim(%line);
      %line = strreplace(%line,"=","\t");
      
      %field = getField(%line,0);
      %value = getField(%line,1);
      if (%field !$= "")
         %this.onDataLine(%field,%value);
	}
	
}
function HTTPObject::onPacket(%this,%data)
{
	// Store this state
	%this.lastState = "onPacket" TAB %data;	
}
