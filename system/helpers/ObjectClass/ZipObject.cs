//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function ZipObject::addPath( %this, %path, %pathInZip )
{
	%beginPath = expandFilename( %path );
	%path = pathConcat(%path, "*");
	%file = findFirstFile( %path );

	while(%file !$= "")
	{
		%zipRel = makeRelativePath( %file, %beginPath );
		%finalZip = pathConcat(%pathInZip, %zipRel);
		%this.addFile( %file, %finalZip );
		%file = findNextFile(%path);
	}
}
