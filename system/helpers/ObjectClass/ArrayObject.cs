//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// ArrayObject Manipulation
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// Get directly the value of an array if found entry matching the key
/// %key = Key you want to get the value from
function ArrayObject::getVal(%this, %key)
{
	//Get the index
	%index = %this.getIndexFromKey(%key);
	%value =  %this.getValue(%index);
	return %value;
}
//------------------------------------------------------------------------------
/// Get the array key that match a value
/// %value = Value to find the assigned key
function ArrayObject::getKeyFromValue(%this, %value)
{
	//Get the index
	%index = %this.getIndexFromValue(%value);
	%key =  %this.getKey(%index);
	return %key;
}

//------------------------------------------------------------------------------
/// Add the array Key/Value pair (If key exist it will be replaced)
/// %key = Key to be added/replaced
/// %value = Value to be added/replaced
function ArrayObject::setVal(%this, %key,%value,%oldBehavior)
{

	//Check for index, if not, we will add the new value
	%index = %this.getIndexFromKey(%key);

	if (%index $="-1" )
	{
		%this.add(%key,%value);
	}
	else
	{
		%this.setValue(%value,%index);
	}
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
/// Add the array Key/Value pair (If key exist it will be replaced)
/// %key = Key to be added/replaced
/// %value = Value to be added/replaced
function ArrayObject::eraseKey(%this, %key)
{

	//Check for index, if not, we will add the new value

	while(true)
	{
		%index = %this.getIndexFromKey(%key);
		if (%index !$="-1" )
			%this.erase(%index);
		else
			break;
	}

}
//------------------------------------------------------------------------------

//==============================================================================
// ArrayObject Debugging
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// Dump the key/values informations of an ArrayObject to the console
function ArrayObject::dumpData(%this)
{
	%count = %this.count();

	for(%i = 0; %i< %count; %i++)
	{
		%key = %this.getKey(%i);
		%value = %this.getValue(%i);
		if (%key $= "")
			%key = "NULL";
		info("Array dump: Index:",%i,"Key=",%key,"Value=",%value);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Increment the value of a key (making sure it's numerical)
function ArrayObject::Inc(%this,%key, %inc)
{
	loge("ArrayObject::Inc(%this,%key,%inc)",%this,%key,%inc);

	//Make sure we have a numerical value
	if (!strIsNumeric(%inc))
	{
		error("Trying to increment a non-numeric value for a DB object");
		return;
	}

	%curVal = %this.getVal(%key);
	%newVal = %curVal + %inc;
	%this.setVal(%key,%newVal);
	return %this.getVal(%key);
}
//------------------------------------------------------------------------------

//==============================================================================
// ArrayObject Debugging
//==============================================================================

//==============================================================================
// Dump Array Content
function ArrayObject::logMove(%this)
{
	%id = %this.moveFirst();

	while(%id !$= "-1")
	{
		%key = %this.getKey(%id);
		%value = %this.getValue(%id);
		info("Array dump: Index:",%id,"Key=",%key,"Value=",%value);
		%id = %this.moveNext();
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Called when a user is disconnecting from server or between server sessions
function ArrayObject::cleanupKeys(%this,%keepEmptyKey,%allowDuplicatedKeys)
{
	//Go through all stats and do some update
	for(%i=0; %i<%this.count(); %i++)
	{
		%eraseMe = false;
		%field = %this.getKey(%i);
		%value = %this.getValue(%i);

		if (%field $= "" && !%keepEmptyKey)
			%eraseMe = true;
		else if (%checked[%field] && !%allowDuplicatedKeys)
			%eraseMe = true;

		if ( %eraseMe)
		{
			%this.erase(%i);
			%i--;
		}
		%checked[%field] = true;
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Write the array content to a file using the writeArray helper
function ArrayObject::write(%this,%file,%append,%prefixDeclare)
{
	writeArray(%this,%file,%append,%prefixDeclare);
}
//------------------------------------------------------------------------------

//==============================================================================
// Write the key/values of an array to a file with a create call at start.
// By execing the output file, the array will be regenerated like it was.
function ArrayObject::dumpToFile(%array,%file,%replaceList )
{

	if (%file $= "")
		%file = "ArrayDump.txt";

	// Create a file object for writing
	%fileWrite = getFileWriteObj(%file,%append);

	//===========================================================================
	// Write the Header and Array creator
	%id = %array.moveFirst();
	while(%id >=0 )
	{
		%key = %array.getKey(%id);
		%value = %array.getValue(%id);
		if (%replaceList !$= "")
			%value = strReplaceList(%value,%replaceList);

		%fileWrite.writeLine(%key SPC "->" SPC %value);
		%id = %array.moveNext();

	}
	closeFileObj(%fileWrite);
}
//------------------------------------------------------------------------------
