//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function scanZones(%this, %type)
{

	%zoneList = getMissionObjectClassList("Zone Portal OcculsionVolume");

	foreach$(%zone in %zoneList)
	{
		if (!isObject(%zone))
			continue;

		%class = %zone.getClassName();
		%zoneClassList = strAddWord(%zoneClassList,%class,1);
		%zoneList[%class] = strAddWord(%zoneList[%class],%zone,1);
	}

	foreach$(%class in %zoneClassList)
	{

	}
}
//------------------------------------------------------------------------------//==============================================================================
function getZoneDetail(%object)
{
	%detail = %object.getId() @ "\t\c1 "@  %object.getClassName() @ "\t\c2 "@ %object.hidden @ "\t\c3 "@ %object.position @ "\t\c4 "@ %object.scale;
	return %detail;

}
//------------------------------------------------------------------------------
