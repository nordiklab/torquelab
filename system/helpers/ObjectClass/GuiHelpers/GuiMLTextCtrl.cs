//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function WebTextML::onUrl(%this, %text)
{
	goToWebPage(%text);
}
//------------------------------------------------------------------------------
//==============================================================================
// Add a line of text to a GuiMLTextCtrl. It will add automatically the <br>
// and also make sure everything fit correctly.
function GuiMLTextCtrl::addLine(%this,%text)
{
	%text = strreplace(%text,"<br>","");
	%this.addText(%text@"<br>",1);
	%this.forceReflow();
}
//------------------------------------------------------------------------------
//==============================================================================
// Get the amount of ticks needed for a range and a tick step size desired
function GuiMLTextCtrl::onResize(%this,%width,%maxY)
{
}
//------------------------------------------------------------------------------
