//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Clear the editors menu (for dev purpose only as now)

function GuiControl::fitIntoParent( %this,%mode,%offsetTBLR )
{
  
  	%marginTop = (getWord(%offsetTBLR,0) > 0) ? getWord(%offsetTBLR,0) : 0;
	%marginBottom = (getWord(%offsetTBLR,1) > 0) ? getWord(%offsetTBLR,1) : 0;
	%marginLeft = (getWord(%offsetTBLR,2) > 0) ? getWord(%offsetTBLR,2) : 0;
	%marginRight = (getWord(%offsetTBLR,3) > 0) ? getWord(%offsetTBLR,3) : 0;
	
	%parent = %this.parentGroup;
	%pos = "0 0";
	%extent = %parent.extent;

	if ( %mode $= "width")
	{
	    %marginLeft = %marginTop;
	   %marginRight = %marginBottom;
	}

	
	%extent.x = %extent.x - (%marginLeft + %marginRight);
	%extent.y = %extent.y - (%marginTop + %marginBottom);
	
	
	if (%mode $= "" )
	{
	
		%this.position =  %marginLeft SPC %marginTop;		
		%this.extent = %extent;

	}
	if ( %mode $= "width")
	{	
		%this.position.x = %marginLeft;
		%this.extent.x = %extent.x;
	}

	if ( %mode $= "height")
	{	  
		%this.position.y = %marginTop;
		%this.extent.y = %extent.y;
	}
}
//------------------------------------------------------------------------------
//Depreciated - There's never more than one parent...
function GuiControl::fitIntoParents( %this,%mode )
{
   %this.fitIntoParent(%mode);
}
//------------------------------------------------------------------------------
function GuiControl::fillParent( %this,%mode )
{
	%pos = "0 0";
	%extent = %this.parentGroup.extent.x SPC %this.parentGroup.extent.y;
	if (%mode $= "width")
	{
		%pos.y = %this.extent.y;
		%extent.y = %this.extent.y;
	}
	if (%mode $= "height")
	{
		%pos.x = %this.extent.x;
		%extent.x = %this.extent.x;
	}
	%this.resize(%pos.x,%pos.y,%extent.x,%extent.y);
}

//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function GuiControl::fitParentOffset( %this,%offsetTBLR )
{
	%parent = %this.parentGroup;
	%pos = "0 0";
	%extent = %parent.extent;
	%marginTop = (getWord(%offsetTBLR,0) > 0) ? getWord(%offsetTBLR,0) : 0;
	%marginBottom = (getWord(%offsetTBLR,1) > 0) ? getWord(%offsetTBLR,1) : 0;
	%marginLeft = (getWord(%offsetTBLR,2) > 0) ? getWord(%offsetTBLR,2) : 0;
	%marginRight = (getWord(%offsetTBLR,3) > 0) ? getWord(%offsetTBLR,3) : 0;

	%pos.x += %marginLeft;
	%pos.y += %marginTop;
	%extent.x = %extent.x - %marginLeft - %marginRight;
	%extent.y = %extent.y - %marginTop - %marginBottom;

	%this.position = %pos;
	%this.extent = %extent;

}
//------------------------------------------------------------------------------
//==============================================================================
/// Set a GuiControl to be aligned with parent top, bottom, left or right
/// %direction : Direction to allign with (top,bottom,left,right)
/// %margin(opt.): Set a margin to be added to direction.
/// Usage: GuiControl.AlignCtrlToParent("top",10); //The Y position will be set to 10
function GuiControl::AlignCtrlToParent(%this,%direction,%offset)
{
	%parent = %this.parentGroup;

	if (!isObject(%parent))
	{
		info(%this.getName()," have no parent to be forced inside");
		return;
	}

	%posX = %this.getPosition().x;
	%posY = %this.getPosition().y;
	%offsetX = getWord(%offset,0);
	%offsetY = getWord(%offset,1);

	if (%offsetX $= "")
		%offsetX = 0;
	if (%offsetY $= "")
		%offsetY = %offsetX;
	switch$(%direction)
	{
		case "right": //Set max right of parent
			%posX = %parent.getExtent().x - %this.getExtent().x - %offsetX;

		case "left": //Set max left of parent
			%posX= %offsetX;

		case "top": //Set max left of parent
			%posY = %offsetY;

		case "bottomright": //Set max left of parent

			%posX = %parent.getExtent().x - %this.getExtent().x - %offsetX;
			%posY = %parent.getExtent().y - %this.getExtent().y -%offsetY;
		case "bottomleft": //Set max left of parent
			%posX= %offsetX;
			%posY = %parent.getExtent().y - %this.getExtent().y -%offsetY;
		case "topleft":
			%posX= %offsetX;
			%posY = %offsetY;
		case "topright":
			%posX = %parent.getExtent().x - %this.getExtent().x - %offsetX;
			%posY = %offsetY;
		case "bottom": //Set max right of parent
			%posY = %parent.getExtent().y - %this.getExtent().y -%offsetY;

		case "centerX": //Set max right of parent
			%diff = %parent.getExtent().x - %this.getExtent().x;
			%posX = (%parent.getExtent().x - %this.getExtent().x)/2;
			%posX = %posX + %offsetX;
		case "centerY": //Set max right of parent
			%diff = %parent.getExtent().y - %this.getExtent().y;
			%posY = (%parent.getExtent().y - %this.getExtent().y)/2;
			%posY = %posY + %offsetY;
		case "center": //Set max right of parent
			%diff = %parent.getExtent().x - %this.getExtent().x;
			%posX = (%parent.getExtent().x - %this.getExtent().x)/2;
			%diff = %parent.getExtent().y - %this.getExtent().y;
			%posY = (%parent.getExtent().y - %this.getExtent().y)/2;
			%posX = %posX + %offsetX;
			%posY = %posY + %offsetY;

	}

	%this.setPosition(%posX,%posY);
}
//------------------------------------------------------------------------------
function GuiControl::onVisible( %this )
{
	if (%this.alignTo !$= "")
		%this.AlignCtrlToParent(firstWord(%this.alignTo),restWords(%this.alignTo));
}

//==============================================================================
/// Get the real position of a control inside the canvas.
function GuiControl::getRealPosition(%this)
{
	%parent = %this.parentGroup;
	%pos = %this.position;

	while(isObject(%parent))
	{
		%pos.x += %parent.position.x;
		%pos.y += %parent.position.y;
		%parent = %parent.parentGroup;
	}

	return %pos;
}
//----------------------------------------------------------------------------
//==============================================================================
/// Force a control extent to be inside the extent of another ctrl. (Generally a parent)
function GuiControl::forceInsideCtrl(%this,%ctrl)
{
	%maxX = %ctrl.extent.x - %this.extent.x;
	%maxY = %ctrl.extent.y - %this.extent.y;

	if (%maxX < 0 || %maxY < 0)
	{
		warnLog("The control doesn't fit into the target because it's bigger! Source:",%this,%this.extent,"Target:",%ctrl,%ctrl.extent);
		return;
	}

	%this.position.x = mClamp(%this.position.x,0,%maxX);
	%this.position.y = mClamp(%this.position.y,0,%maxY);
}
//----------------------------------------------------------------------------
//==============================================================================
/// Force a control extent to be inside the extent of another ctrl. (Generally a parent)
function GuiControl::forceInsideParent(%this)
{
	%this.forceInsideCtrl(%this.parentGroup);
}
//----------------------------------------------------------------------------
//==============================================================================
/// Force a control extent to be inside the extent of another ctrl. (Generally a parent)
function GuiControl::getCent(%this,%ctrl)
{
	%maxX = %ctrl.extent.x - %this.extent.x;
	%maxY = %ctrl.extent.y - %this.extent.y;

	if (%maxX < 0 || %maxY < 0)
	{
		warnLog("The control doesn't fit into the target because it's bigger! Source:",%this,%this.extent,"Target:",%ctrl,%ctrl.extent);
		return;
	}

	%this.position.x = mClamp(%this.position.x,0,%maxX);
	%this.position.y = mClamp(%this.position.y,0,%maxY);
}
//----------------------------------------------------------------------------
//==============================================================================
/// Force a control extent to be inside the extent of another ctrl. (Generally a parent)
function GuiControl::getMarginToParent(%this)
{
	%parent = %this.parentGroup;
	if (!isObject(%parent))
		return "0 0 0 0";

	%max = vectorAdd2D(%this.position,%this.extent);
	%marginMax = vectorSub2D(%parent.extent,%max);
	return %this.position SPC %marginMax;

}
//----------------------------------------------------------------------------
