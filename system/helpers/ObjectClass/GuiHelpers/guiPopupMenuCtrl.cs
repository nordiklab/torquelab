//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//------------------------------------------------------------------------------
// Update a global without the start char $ (simplier than using eval)
//------------------------------------------------------------------------------

//==============================================================================
// Default onDefineIcons functions (Make sure to set valid path to images)
function GuiPopUpMenuCtrl::cloneSimSet( %this,%simSet,%textMethod,%zeroData )
{
	%this.clear();

	if (%zeroData !$= "")
		%this.add(%zeroData,0);
	foreach(%obj in %simSet)
	{
		%text = %obj.getDisplayText(%textMethod);
		%this.add(%text,%obj.getId());
	}
}
//------------------------------------------------------------------------------
