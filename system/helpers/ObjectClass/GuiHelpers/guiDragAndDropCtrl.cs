//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function startBasicDnD( %sourceCtrl,%defaultCallback)
{
	%xOffset = getWord( %sourceCtrl.extent, 0 ) / 2;
	%yOffset = getWord( %sourceCtrl.extent, 1 ) / 2;
	//if (%defaultCallback $= "")
	// %defaultCallback = %sourceCtrl.getId()@".droppedOnCtrl";
	startDragCtrl( %sourceCtrl,"",%defaultCallback,%failCallback,%addTo, %xOffset SPC %yOffset);
}

//==============================================================================
// startDragAndDropCtrl - Simplified version of dragAndDropCtrl();
//------------------------------------------------------------------------------
/// Start dragging a specified control with a defaultCallback in case specific
/// onControlDropped haven't been called. If not set, the default onControlDropped
/// will be used.
/// @sourceCtrl The control use as drag source
/// @defaultCallback Function to call when default onControlDropped is called
/// @dropType Variable used to validate the dropped ON control
function startDragAndDropCtrl( %sourceCtrl,%dropType,%defaultCallback,%failCallback,%addTo )
{
	%xOffset = getWord( %sourceCtrl.extent, 0 ) / 2;
	%yOffset = getWord( %sourceCtrl.extent, 1 ) / 2;
	startDragCtrl( %sourceCtrl,%dropType,%defaultCallback,%failCallback,%addTo, %xOffset SPC %yOffset);
}
//==============================================================================
// Dragging start function used by TLab
//------------------------------------------------------------------------------
/// Drag a ctrl and drop it in container with field set to dropType only
/// @dropType The control will only be dropped in ctrl with dropType field
/// @failCallback Function to call when drop succeed (default to DragFailed)
function dragAndDropCtrl( %sourceCtrl,%dropType,%failCallback )
{
	%xOffset = getWord( %sourceCtrl.extent, 0 ) / 2;
	%yOffset = getWord( %sourceCtrl.extent, 1 ) / 2;
	startDragCtrl( %sourceCtrl,%dropType,"",%failCallback,"", %xOffset SPC %yOffset);
}
//------------------------------------------------------------------------------
//==============================================================================
// Default DragFail function
//==============================================================================
function DragFailed(%payload)
{
	if (isObject(%payload.dragSourceControl))
		show(%payload.dragSourceControl);
}
//------------------------------------------------------------------------------
//==============================================================================
// Main Drag And Drop Start Function
//==============================================================================
function startDragCtrl( %sourceCtrl,%dropType,%defaultCallback,%failCallback,%addTo,%cursorOffset,%addDraggingCtrl )
{
	if (!isObject(%sourceCtrl))
	{
		warnLog("Trying to drop something that is not an object:",%sourceCtrl);
		return;
	}

	if (%failCallback $= "")
		%failCallback = "DragFailed";

	// First we construct a new temporary swatch button that becomes the payload for our
	// drag operation and give it the properties of the swatch button we want to copy.
	%payload = %sourceCtrl.deepClone();
	if (isObject(%addDraggingCtrl))
	{
		%payload.add(%addDraggingCtrl);
		%payload.bringToFront(%addDraggingCtrl);
	}
	%payload.position = "0 0";
	%payload.setName("");
	%payload.dragSourceControl = %sourceCtrl; // Remember where the drag originated from so that we don't copy a color swatch onto itself.
	%payload.dropType = %dropType;
	%payload.failOnDefault = true;
	%payload.defaultCallback = %defaultCallback;
	%payload.failCallback = %failCallback;
	// Calculate the offset of the GuiDragAndDropControl from the mouse cursor.  Here we center
	// it on the cursor.
	if (%cursorOffset !$= "")
	{
		%xOffset = %cursorOffset.x;
		%yOffset = %cursorOffset.y;
	}
	else
	{
		%xOffset = Canvas.getCursorPos().x - %sourceCtrl.position.x;
		%yOffset = Canvas.getCursorPos().y - %sourceCtrl.position.y;
	}
	// Compute the initial position of the GuiDragAndDrop control on the cavas based on the current
	// mouse cursor position.
	%cursorPos = Canvas.getCursorPos();

	%xPos = getWord( %cursorpos, 0 ) - %xOffset;
	%yPos = getWord( %cursorpos, 1 ) - %yOffset;

	%sourceCtrl.dropOffset = %xOffset SPC %yOffset;
	%payload.dropOffset = %xOffset SPC %yOffset;

	// Create the drag control.
	%dragCtrl = new GuiDragAndDropControl()
	{
		canSaveDynamicFields    = "0";
		Profile                 = "ToolsSolidDefaultProfile";
		HorizSizing             = "right";
		VertSizing              = "bottom";
		Position                = %xPos SPC %yPos;
		extent                  = %payload.extent;
		MinExtent               = "4 4";
		canSave                 = "1";
		Visible                 = "1";
		hovertime               = "1000";
		forceInParent           = true; // Force the control to stay entirely within it's parent container
		// Let the GuiDragAndDropControl delete itself on mouse-up.  When the drag is aborted,
		// this not only deletes the drag control but also our payload.
		deleteOnMouseUp         = true;
		// To differentiate drags, use the namespace hierarchy to classify them.
		// This will allow a color swatch drag to tell itself apart from a file drag, for example.
		class                   = "DropControlClass_"@%dropType;
		superClass                   = "DragCtrl_"@%dropType;
		dropType = %dropType;
	};
	// Add the temporary color swatch to the drag control as the payload.
	%dragCtrl.add( %payload );
	// Start drag by adding the drag control to the canvas and then calling startDragging().
	%addToCtrl = Canvas.getContent();

	if (isObject(%addTo))
		%addToCtrl = %addto;

	%addToCtrl.add( %dragCtrl );
	%dragCtrl.startDragging( %xOffset, %yOffset );
	return %payload;
}
//------------------------------------------------------------------------------

//==============================================================================
// Empty Methods to overwrite
//==============================================================================

//==============================================================================
/// Called when a drag&drop operation through GuiDragAndDropControl has entered the control.
/// This is only called for topmost visible controls as the GuiDragAndDropControl moves over them.
/// @param control The payload of the drag operation.
/// @param dropPoint The point at which the payload would be dropped if it were released now.  Relative to the canvas.
function GuiControl::onControlDragEnter(%this, %control,%dropPoint)
{
}
//------------------------------------------------------------------------------
//==============================================================================
/// Called when a drag&drop operation through GuiDragAndDropControl has exited the control and moved over a different control.
/// This is only called for topmost visible controls as the GuiDragAndDropControl moves off of them.
/// @param control The payload of the drag operation.
/// @param dropPoint The point at which the payload would be dropped if it were released now.  Relative to the canvas.
function GuiControl::onControlDragExit(%this, %control, %dropPoint)
{
}
//------------------------------------------------------------------------------
//==============================================================================
/// Called when a drag&drop operation through GuiDragAndDropControl is moving across the control after it has entered it.
/// This is only called for topmost visible controls as the GuiDragAndDropControl moves across them.
/// @param control The payload of the drag operation.
/// @param dropPoint The point at which the payload would be dropped if it were released now.  Relative to the canvas.
function GuiControl::onControlDragged(%this, %control, %dropPoint)
{
	//%x = Canvas.getCursorPos().x;
	//%y = Canvas.getCursorPos().y;
	//if (%x >= 600)
	//Canvas.setCursorPos("612" SPC %y);
	//%control.onDragged(%this,%dropPoint);

}
//------------------------------------------------------------------------------
//==============================================================================
/// Called when a drag&drop operation through GuiDragAndDropControl has completed and is dropping its payload onto the control.
/// This is only called for topmost visible controls as the GuiDragAndDropControl drops its payload on them.
/// @param control The control that is being dropped onto this control.
/// @param dropPoint The point at which the control is being dropped.  Relative to the canvas.
function GuiControl::onControlDropped(%this, %control, %dropPoint)
{
	if (%control.isDragging)
		%control.isDragging = false;

	//Nothing to do with invalid dropped control
	%srcCtrl = %control.dragSourceControl;
	if (%control.defaultCallback !$= "")
	{
		if (isFunction(%control.defaultCallback) || strFind(%control.defaultCallback,"."))
		{
			warnLog("Default onControlDropped called but switched to specified callback:",%control.defaultCallback,%this,%control,%dropPoint,%srcCtrl);
			eval(%control.defaultCallback@"(%this,%control,%dropPoint);");
			return;
		}
		/*%fields = strreplace(%control.defaultCallback,".","\t");
		if (getFieldCount(%fields) == 2)
		{
			eval(%control.defaultCallback@"(%this,%control,%dropPoint);");
		}*/

	}
	if (%control.dropMethod !$= "" && %control.isMethod(%control.dropMethod))
	{
		eval("%control."@%control.dropMethod@"(%this,%dropPoint);");
		return;
	}

	if (!isObject(%control))
		return;

	if (%control.dropType $= "")
	{
		//No droptype, just call the success callback
		if (%srcCtrl.isMethod("DragSuccess"))
			eval(%srcCtrl@".DragSuccess(%this,%dropPoint);");
		else
			warnLog("Dragged control dropped in valid container but no Success callback found. Nothing has been done");

		return;
	}

	if (%this.dropType $= %control.dropType)
	{

		if (%control.isMethod("DragSuccess"))
			eval(%control@".DragSuccess(%this,%dropPoint);");
		else
			warnLog("Dragged control dropped in valid container but no Success callback found. Nothing has been done");

		return;
	}

	if (%control.isMethod(%control.failCallback))
		eval(%control@"."@%control.failCallback@"(%this,%dropPoint);");
	else
		eval(%control.failCallback);

	//Make sure the source is visible in case it was hiden during dragging
	show(%control.dragSourceControl);
}
//------------------------------------------------------------------------------
//==============================================================================
// OLD DRAG AND DROPPED TO BE REMOVED ONCE CONFIRMED
//==============================================================================
/// Drag a ctrl and drop it in container with field set to dropType only
/// @dropType The control will only be dropped in ctrl with dropType field
/// @failCallback Function to call when drop succeed (default to DragFailed)
/*function dragAndDropCtrl( %sourceCtrl,%dropType,%failCallback )
{
	if (!isObject(%sourceCtrl))
	{
		warnLog("Trying to drop something that is not an object:",%sourceCtrl);
		return;
	}

	if (%failCallback $= "")
		%failCallback = "DragFailed";

	// First we construct a new temporary swatch button that becomes the payload for our
	// drag operation and give it the properties of the swatch button we want to copy.
	%payload = %sourceCtrl.deepClone();
	%payload.position = "0 0";
	%payload.setName("");
	%payload.dragSourceControl = %sourceCtrl; // Remember where the drag originated from so that we don't copy a color swatch onto itself.
	%payload.failCallback = %failCallback;
	// Calculate the offset of the GuiDragAndDropControl from the mouse cursor.  Here we center
	// it on the cursor.
	%xOffset = getWord( %payload.extent, 0 ) / 2;
	%yOffset = getWord( %payload.extent, 1 ) / 2;
	// Compute the initial position of the GuiDragAndDrop control on the cavas based on the current
	// mouse cursor position.
	%cursorPos = Canvas.getCursorPos();
	%xPos = getWord( %cursorpos, 0 ) - %xOffset;
	%yPos = getWord( %cursorpos, 1 ) - %yOffset;

	// Create the drag control.
	%dragCtrl = new GuiDragAndDropControl()
	{
		canSaveDynamicFields    = "0";
		Profile                 = "ToolsSolidDefaultProfile";
		HorizSizing             = "right";
		VertSizing              = "bottom";
		Position                = %xPos SPC %yPos;
		extent                  = %payload.extent;
		MinExtent               = "4 4";
		canSave                 = "1";
		Visible                 = "1";
		hovertime               = "1000";
		offset = %xOffset SPC %yOffset;
		// Let the GuiDragAndDropControl delete itself on mouse-up.  When the drag is aborted,
		// this not only deletes the drag control but also our payload.
		deleteOnMouseUp         = true;
		// To differentiate drags, use the namespace hierarchy to classify them.
		// This will allow a color swatch drag to tell itself apart from a file drag, for example.
		class                   = "DropControlClass_"@%dropType;
		superClass                   = "DragCtrl_"@%dropType;
		dropType = %dropType;
	};
	// Add the temporary color swatch to the drag control as the payload.
	%dragCtrl.add( %payload );
	// Start drag by adding the drag control to the canvas and then calling startDragging().
	Canvas.getContent().add( %dragCtrl );
	Canvas.getContent().pushToBack(%dragCtrl);
	%dragCtrl.startDragging( %xOffset, %yOffset );
}
*/
