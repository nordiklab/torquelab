//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//------------------------------------------------------------------------------
//==============================================================================
// Info Log - Show official game informations to the console
function getListObjName(%listCtrl,%listObj)
{
	%name = %listObj.internalName;

	if (%name $= "")
		%name = %listObj.getName();

	if (%name $= "")
		%name = %listObj.getId();

	return %name;
}
//------------------------------------------------------------------------------

