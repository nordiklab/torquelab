//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// GuiControls Values Updates By Type
// Ex: TextCtrl value = getText() while ChecboxCtrl value = isStateOn();
//==============================================================================
//==============================================================================
// Get the type of a GuiControl depending of it class
function GuiControl::getType( %this,%requestType )
{
	if (!isObject(%this))
	{
		return "";
	}

	%class = %this.getClassName();

	if (%class $= "GuiTextEditSliderBitmapCtrl" || %class $= "GuiTextEditSliderCtrl"
	              || %class $= "GuiSliderCtrl")
		%type = "Value";

	if (%class $= "GuiCheckBoxCtrl")
		%type = "Checkbox";

	if (%class $= "GuiColorPickerCtrl")
		%type = "Color";

	if (%class $= "GuiPopUpMenuCtrl")
	{
		%type = "Text";

		if (%this.syncId && %requestType $= "get")
			%type = "MenuId";
	}

	if ( %class $= "GuiTextEditCtrl" || %class $= "GuiTextCtrl")
		%type = "Text";

	return %type;
}
//------------------------------------------------------------------------------

//==============================================================================
/// Set the value for a GuiControl depending of it's type
/// %value : Value to assign to control
/// %updateFriends : Check for aggregated ctrl friends
function GuiControl::setTypeValue( %this,%value,%updateFriends )
{
	%type = %this.getType("set");

	switch$(%type)
	{
		case "Value":
			%this.setValue(%value);

		case "Checkbox":
			%this.setStateOn(%value);

		case "Color":
			if (%this.isIntColor)
			{
				if (getWord(%value,3) $="")
					%value = setWord(%value,3,"255");

				%this.BaseColor = ColorIntToFloat(%value);
			}
			else
			{
				if (getWord(%value,3) $="")
					%value = setWord(%value,3,"1");

				%this.BaseColor = %value;
			}

			%this.updateColor();

		case "Text":
			%this.setText(%value);

		default:
			%this.setValue(%value);
	}

	if (%updateFriends)
		%this.updateFriends();
}
//------------------------------------------------------------------------------

//==============================================================================
/// Get the value of a GuiControl depending of it type
/// return : The value assigned to the control
function GuiControl::getTypeValue( %this )
{
	%type = %this.getType("get");

	switch$(%type)
	{
		case "Value":
			%value = %this.getValue();

		case "Text":
			%value = %this.getText();

		case "Checkbox":
			%value = %this.isStateOn();

		case "Color":
			%value = %this.BaseColor ;

			if (getWordCount(%value) <= 3)
				%value = %value SPC 1;

			if (%this.isIntColor)
			{
				%value = ColorFloatToInt(%value);
			}

		case "MenuId":
			%value = %this.getSelected();
	}

	return %value;
}
//------------------------------------------------------------------------------
//==============================================================================
// Get the type of a GuiControl depending of it class
function GuiControl::checkVariableSupport( %this )
{
	if (!isObject(%this))
		return "";

	%class = %this.getClassName();

	if ( %class $= "GuiTextEditCtrl")
		return false;

	return true;
}
//------------------------------------------------------------------------------
//==============================================================================
// Get the type of a GuiControl depending of it class
//buildWidget("test","Text","DefaultValue","MyInternal");
function GuiControl::setGlobalValue( %this,%global )
{
	eval("%value = $"@%global@";");
	%class = %this.getClassName();
	%this.variable = "$"@%global;
	if ( %class $= "GuiMLTextCtrl")
	{
		%this.setTypeValue( %value );
		return;

	}

}
//------------------------------------------------------------------------------

//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function GuiControl::clearChildResponder( %this )
{
	%responder = %this.getFirstResponder();

	if (isObject(%responder))
		%responder.clearFirstResponder();
}
//------------------------------------------------------------------------------

//==============================================================================
// Set Value Safe - From Torque3D (Not sure if needed anymore)
//==============================================================================

//==============================================================================
/// Safe way to set a value without affecting the commands (From Torque3D default)
function GuiControl::setValueSafe(%this, %val)
{
	%cmd = %this.command;
	%alt = %this.altCommand;
	%this.command = "";
	%this.altCommand = "";
	%this.setValue(%val);
	%this.command = %cmd;
	%this.altCommand = %alt;
}
//----------------------------------------------------------------------------
//==============================================================================
function GuiControl::shareValueSafe(%this, %dest)
{
	%dest.setValueSafe(%this.getValue());
}
//----------------------------------------------------------------------------
//==============================================================================
function GuiControl::shareValueSafeDelay(%this, %dest, %delayMs)
{
	%this.schedule(%delayMs, "shareValueSafe", %dest);
}
//----------------------------------------------------------------------------

function saveGuiToFile(  %currentObject, %filename )
{
	if (!isObject(%currentObject))
		return;

	if (%filename $= "")
		%filename = %currentObject.getFileName();

	if( isWriteableFileName( %filename ) )
	{
		if (%currentObject.isMethod("onPreEditorSave"))
			%currentObject.onPreEditorSave();

		//
		// Extract any existent TorqueScript before writing out to disk
		//
		%fileObject = new FileObject();
		%fileObject.openForRead( %filename );
		%skipLines = true;
		%beforeObject = true;
		// %var++ does not post-increment %var, in torquescript, it pre-increments it,
		// because ++%var is illegal.
		%lines = -1;
		%beforeLines = -1;
		%skipLines = false;

		while( !%fileObject.isEOF() )
		{
			%line = %fileObject.readLine();

			if( %line $= "//--- OBJECT WRITE BEGIN ---" )
				%skipLines = true;
			else if( %line $= "//--- OBJECT WRITE END ---" )
			{
				%skipLines = false;
				%beforeObject = false;
			}
			else if( %skipLines == false )
			{
				if(%beforeObject)
					%beforeNewFileLines[ %beforeLines++ ] = %line;
				else
					%newFileLines[ %lines++ ] = %line;
			}
		}

		%fileObject.close();
		%fileObject.delete();
		%fo = new FileObject();
		%fo.openForWrite(%filename);

		// Write out the captured TorqueScript that was before the object before the object
		for( %i = 0; %i <= %beforeLines; %i++)
			%fo.writeLine( %beforeNewFileLines[ %i ] );

		%fo.writeLine("//--- OBJECT WRITE BEGIN ---");
		%fo.writeObject(%currentObject, "%guiContent = ");
		%fo.writeLine("//--- OBJECT WRITE END ---");

		// Write out captured TorqueScript below Gui object
		for( %i = 0; %i <= %lines; %i++ )
			%fo.writeLine( %newFileLines[ %i ] );

		%fo.close();
		%fo.delete();
		%currentObject.setFileName( makeRelativePath( %filename, getMainDotCsDir() ) );
		info( "Saved file '" @ %currentObject.getFileName() @ "'" );

		if (%currentObject.isMethod("onPostEditorSave"))
			%currentObject.onPostEditorSave();
	}
	else
		warnLog( "Error writing to file", "There was an error writing to file '" @ %currentFile @ "'. The file may be read-only." );

}
//---------------------------------------------------------------------------------------------
