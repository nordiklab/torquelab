//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
//! getRealCursorPos() > Get the Cursor position inside the engine window
//+ Canvas.getCursorPos() return the cursor position on the user screen, with this function
//+ you will get the cursor position inside Torque3D window.
//+ Usage: %realPos = getRealCursorPos();
function getRealCursorPos()
{
	%cursorPos = Canvas.getCursorPos();
	%windowPos = Canvas.getWindowPosition();
	%realPos = getWords(VectorSub(%cursorPos,%windowPos),0,1);
	return %realPos;
}
//------------------------------------------------------------------------------

//==============================================================================
// Push, Pop and Toggle Dialogs
//==============================================================================

//==============================================================================
//Push a dialog to the canvas
function pushDlg(%dlg,%layer,%center)
{
	if ($InGuiEditor)
		return;
	Canvas.pushDialog(%dlg,%layer,%center);
}
//------------------------------------------------------------------------------
//==============================================================================
//Pop a dialog from the canvas
function popDlg(%dlg)
{
	Canvas.popDialog(%dlg);
}
//------------------------------------------------------------------------------
//==============================================================================
// Toggle a Dialog GUI
function toggleDlg(%dlg)
{
	if (%dlg.isAwake())
		popDlg(%dlg);
	else
		pushDlg(%dlg);
}
//------------------------------------------------------------------------------

//==============================================================================
// Content GUI helpers (Canvas.setContent() simplified)
//------------------------------------------------------------------------------
$HLab_DefaultGui = $pref::UI::defaultGui; //DefaultGui if invalid GUI called
//==============================================================================

//==============================================================================
// Set canvas content and store previous GUI (Can be specified optionally)
function setHud(%hudCtrl)
{
   if (isObject(%hudCtrl))
      $PlayGui = %hudCtrl;
      
   if (!isObject($PlayGui))
   {      
      warnLog("No valid HUD defined", "-"@ $PlayGui,"Trying default PlayGui",isObject(PlayGui));
      if (!isObject(PlayGui))
         return;
         
      $PlayGui = PlayGui;      
   }  
   setGui($PlayGui);   
}
//------------------------------------------------------------------------------

//==============================================================================
// Set canvas content and store previous GUI (Can be specified optionally)
function setGui(%gui,%previousGui)
{
	$PreviousGui = Canvas.getContent();
	if ($PreviousGui == %gui)
	   return;   
   
	Canvas.setContent(%gui);

	if (isObject(%previousGui))
		$PreviousGui = %previousGui;
}
//------------------------------------------------------------------------------
//==============================================================================
// Set the Previous canvas content that was loaded before the current content
function setPrevGui(%defaultGui)
{
	if (%defaultGui $= "")
		%defaultGui = $HLab_DefaultGui;

	%setGui = $PreviousGui;

	if (!isObject($PreviousGui) && isObject(%defaultGui))
	{
		warnLog("Couldn't find a previous GUI, calling the Default GUI:",%defaultGui);
		%setGui = %defaultGui;
	}

	if (!isObject(%setGui))
	{
		warnLog("There's no previous GUI Stored! Nothing done...");
		return;
	}

	Canvas.setContent(%setGui);
}
//------------------------------------------------------------------------------

//==============================================================================
// Toggle between Specified GUI and the PreviousGui setted
function toggleGui(%gui)
{
	if (!isObject(%gui))return;

	if (Canvas.getContent().getId() $= %gui.getId())
		setGui($PreviousGui);
	else
		setGui(%gui);
}
//------------------------------------------------------------------------------
//==============================================================================
// Set canvas content and store previous GUI (Can be specified optionally)
function getGui(%id)
{
	%gui = Canvas.getContent();
	if (!%id)
		%gui = (%gui.name $= "") ? %gui.getId() : %gui.name;
	return %gui;
}
//------------------------------------------------------------------------------
