//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Action Map Constructors (newActionMap is use when saved with customHeader = true)
//==============================================================================

//==============================================================================
function AddActionMap(%map)
{
	return newActionMap(%map);
}
//------------------------------------------------------------------------------
//==============================================================================
function MakeLabActionMaps()
{
	foreach(%map in ActionMapGroup)
	   %map.superClass = "LabActionMap";
}
//------------------------------------------------------------------------------

//==============================================================================
function newActionMap(%map,%deleteExist)
{
	if (!isObject(ActionMapSet))
		new SimSet("ActionMapSet");

	if (!isObject(ActionMapPushed))
		new SimSet("ActionMapPushed");

	if (%map $= "")
		%map = "auto"@ActionMapSet.count();

	if (endsWith(%map,"Map"))
		%actionMap = %map;
	else
		%actionMap = %map@"Map";
		
	if (isObject( %actionMap ) && %deleteExist)
	    %actionMap.delete();

	//Create the actionMap if not existing
	if ( !isObject( %actionMap )  )		  
		new ActionMap(%actionMap);	
	else
		info("ActionMap",%actionMap,"Already existed");

	%actionMap.superClass = "LabActionMap";
	ActionMapSet.add(%actionMap);
	return %actionMap;
}
//------------------------------------------------------------------------------

//==============================================================================
function LabActionMap::bind(%this,%device,%action,%command)
{
	%this.addBindData("bind",%device,%action,%command);
	BindArrayGroup.cmdBinds[%command] = strAddField(BindArrayGroup.cmdBinds[%command],%device SPC %action,true);
	Parent::bind(%this,%device,%action,%command);
}
//------------------------------------------------------------------------------
//==============================================================================
function LabActionMap::bindCmd(%this,%device,%action,%onCommand,%offCommand)
{
	%this.addBindData("bindCmd",%device,%action,%onCommand,%offCommand);
	Parent::bindCmd(%this,%device,%action,%onCommand,%offCommand);
}
//------------------------------------------------------------------------------
//==============================================================================
function LabActionMap::addBindData(%this,%type,%device,%action,%onCommand,%offCommand)
{
	%array = %this.getName() @"Array";
	if (!isObject(BindArrayGroup))
		new SimGroup(BindArrayGroup);

	if (!isObject(%array))
	{
		new ArrayObject(%array);
		BindArrayGroup.add(%array);
		%array.internalName = %this.getName();

	}
	%array.setVal(%device SPC %action,%type TAB %onCommand TAB %offCommand);

}
//------------------------------------------------------------------------------

//==============================================================================
function ActionMap::onAdd(%this)
{
	info("ActionMap::onAdd(%this)",%this.getName());
	
}
//------------------------------------------------------------------------------
//==============================================================================
function LabActionMap::pop(%this)
{
	if (ActionMapPushed.getObjectIndex(%this) != -1)
		ActionMapPushed.remove(%this);
	%this.pushed = false;
	Parent::pop(%this);
}
//------------------------------------------------------------------------------
//==============================================================================
function LabActionMap::push(%this)
{
	if (%this.pushBlocked)
	{
		warnLog(%this.getName(),"\c2ActionMap push is blocked, use \c4enableMap(\c5"@%this.getName()@"\c4); \c2to make it pushable again");
		return;
	}
	ActionMapPushed.add(%this);
	%this.pushed = true;
	%this.pushIndex = $LabActionMapPushIndex++;
	Parent::push(%this);
}
//------------------------------------------------------------------------------

//==============================================================================
function sortByPushIndex(%objA,%objB)
{
	%a = %objA.pushIndex;
	%b = %objB.pushIndex;

	if (%a < %b)
		return -1;
	return 1;

}
//------------------------------------------------------------------------------

function dumpPushedBind()
{
	info("List of pushed ActionMaps");
	ActionMapPushed.dumpData();
	ActionMapPushed.sort("sortByPushIndex");
	foreach(%obj in ActionMapPushed)
	{
		%intName =  %obj.internalName;
		if (%obj.getName() !$= "")
			%name = %obj.getName();
		else
			%name = %obj;

		if (%intName !$= "")
			%name = %name@"\c1[\c2"@%intName@"\c1]";

		info("=>",%name,"-","PushedIndex",%obj.pushIndex);
	}
}

function dumpBindData(%pop)
{
	foreach(%array in BindArrayGroup)
	{
		if (%intName.pushed)
			%pushed = "\c1Yes";
		else
			%pushed = "\c2No";
		%intName =  %array.internalName;
		for(%i=0; %i<%array.count(); %i++)
		{
			%key = %array.getKey(%i);
			%value = %array.getValue(%i);
			%device = getWord(%key,0);
			%action = restWords(%key);
			%type = getField(%value,0);
			%cmd = getField(%value,1);
			%offCmd = getField(%value,2);
			if (%offCmd $= "")
				info(%type,%device,%action,"==>","OnPress",%cmd);
			else
				info(%type,%device,%action,"==>","OnPress",%cmd,"OnRelease",%offCmd);
		}

	}
}

function popAllBind(%pop)
{
	foreach(%array in BindArrayGroup)
	{
	   %intName =  %array.internalName;
		if (%intName.pushed)
			%pushed = "\c1Yes";
		else
			%pushed = "\c2No";
		
		%intName.pop();
		for(%i=0; %i<%array.count(); %i++)
		{
			%key = %array.getKey(%i);
			%value = %array.getValue(%i);
			%device = getWord(%key,0);
			%action = restWords(%key);
			%type = getField(%value,0);
			%cmd = getField(%value,1);
			%offCmd = getField(%value,2);
			if (%offCmd $= "")
				info(%type,%device,%action,"==>","OnPress",%cmd);
			else
				info(%type,%device,%action,"==>","OnPress",%cmd,"OnRelease",%offCmd);
		}

	}
}

//============================================================================
//Taking screenshot
function getActionMapList(%filter,%dump)
{
	foreach(%map in ActionMapGroup)
	{
		if(%filter !$="" && !strFind(%map.getName(),%filter))
			continue;

		%mapList = strAddWord(%mapList,%map.getName());
		if(%dump)
			%map.save( "core/dump/allBinds.cs",%append );

		%append = true;
	}

	return %mapList;
}
//----------------------------------------------------------------------------

//==============================================================================
// Unbind a device action(EX: device=keyboard and action=Enter will unbind Enter key
function freeDeviceAction(%device, %action,%exceptMap)
{	
	%list = getAllCommandData(%device,%action);
	
	//If nothing found for that device and action, we are done.
	if (%list $= "")		
	   return; 
	for(%i = 0;%i<getFieldCount(%list);%i++)
	{
	   %map = firstWord(getField(%list,%i));
	   if (strFindWords(%exceptMap,%map))
	      continue;
      %saveList = strAddField(%saveList,getField(%list,%i));
	   %cmd = restWords(getField(%list,%i));
	   eval(%map@".unbind(%device,%action);");	   
	}
	
	$LabActionMapLastFree[%device,%action] = %saveList;
	return %list;
}
//==============================================================================
// Restore a previously unbinded device action
function restoreDeviceAction(%device, %action,%exceptMap)
{
	
	%list = $LabActionMapLastFree[%device,%action];

   //If nothing previously freed, we are done.
	if (%list $= "")		
	   return;  
	
	for(%i = 0;%i<getFieldCount(%list);%i++)
	{
	   %map = firstWord(getField(%list,%i));
	   if (strFindWords(%exceptMap,%map))
	      continue;
	   %cmd = restWords(getField(%list,%i));
	   eval(%map@".bind(%device,%action,%cmd);");
	}
	$LabActionMapLastFree[%device,%action] = "";
	return %list;
}
//==============================================================================
// Get the Screen coords for a 3D position
function getAllCommandData(%device, %action)
{
	foreach(%map in ActionMapGroup)
	{
		%data = %map.getCommand(%device,%action);
		if (%data $= "")
			continue;
			
		%list = strAddField(%list,%map.getName() SPC %data);
	}
	return %list;
}
//==============================================================================
// getAllBindingData(doCrouch)
function getAllBindingData(%command)
{
	
	foreach(%map in ActionMapGroup)
	{
		%data = %map.getBinding(%command);
		if (%data $= "")
			continue;

	}
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
function dumpAllActionMap()
{

	foreach(%map in ActionMapGroup)
	{
		%map.save( "core/dump/allBinds.cs",%append );
		%append = true;
	}

	extractBindsFromFile("core/dump/allBinds.cs");
}
//==============================================================================
// REFERENCE
//==============================================================================
/*

Public Member Functions

bool 	bind (string device, string action, string command)
 	Associates a function to an input event.
bool 	bind (string device, string action, string flag, string deadZone, string scale, string command)
 	Associates a function and input parameters to an input event.
bool 	bindCmd (string device, string action, string makeCmd, string breakCmd="")
 	Associates a make command and optional break command to a specified input device action.
bool 	bindObj (string device, string action, string command, SimObjectID object)
 	Associates a function to an input event for a specified class or object.
bool 	bindObj (string device, string action, string flag, string deadZone, string scale, string command, SimObjectID object)
 	Associates a function to an input event for a specified class or object.
string 	getBinding (string command)
 	Gets the ActionMap binding for the specified command.
string 	getCommand (string device, string action)
 	Gets ActionMap command for the device and action.
string 	getDeadZone (string device, string action)
 	Gets the Dead zone for the specified device and action.
float 	getScale (string device, string action)
 	Get any scaling on the specified device and action.
bool 	isInverted (string device, string action)
 	Determines if the specified device and action is inverted.
void 	pop ()
 	Pop the ActionMap off the ActionMap stack.
void 	push ()
 	Push the ActionMap onto the ActionMap stack.
void 	save (string fileName=NULL, bool append=false)
 	Saves the ActionMap to a file or dumps it to the console.
bool 	unbind (string device, string action)
 	Removes the binding on an input device and action.
bool 	unbindObj (string device, string action, string obj)
 	Remove any object-binding on an input device and action.
*/
