//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$ActionMapSet["Player"] = "move player hud";//hud move weapon player combat
//$ActionMapSet["Player"] = "weapon player combat hud move";
//$ActionMapSet["Player"] = "weapon player combat move";

$ActionMapSet["GuiEditor"] = "GuiEdMap";
$ActionMapSet["WorldEditor"] = "EditorMap";

$ActionMapDefault = "guiMap";
$ActionMapCantSave["menuMap"] = true;
$ActionMapInGameOnly["menuMap"] = false;
$ActionMapFirst["gui"] = true;
$ActionMapFirst["dev"] = true;
$ActionMapFirst["superDev"] = true;
$ActionMapLast["move"] = false;
$ActionMapLast["hud"] = false;
$ActionMapLastSet = "Player";

$pref::Dev::ValidateBinds = false;

//==============================================================================
// ActionMap Management Functions/Helpers
//==============================================================================
//==============================================================================
// Push ActionMap
function checkMap(%map)
{
	//Check if it's a full map name or simply the type
	if (!isObject(%map))
	{
	   %map = %map@"Map";
		//if (getSubStr(%map,strlen(%map)-3) !$= "Map")
			//%map = %map@"Map";

		if (!isObject(%map))
			return "";
	}
	if (!%map.isMemberOfClass(ActionMap))
		return "";
	return %map;
}
//------------------------------------------------------------------------------
//==============================================================================
// Allow a map to be pushed - only affect map that have been disabled (disableMap)
function enableMap(%map,%noPush)
{
	%map = checkMap(%map);
	if (%map $= "")
		return;

    %map.pushBlocked = false;
	if (!%noPush)
		%map.push();
}
//------------------------------------------------------------------------------
//==============================================================================
// Prevent a map from being pushed (Still pushable with default actionMap.push() )
function disableMap(%map,%stayPushed)
{
	%map = checkMap(%map);
	if (%map $= "")
		return;

	if (!%stayPushed)
		%map.pop();//Use the direct pop function to make sure it's disabled
   %map.pushBlocked = true;

}
//------------------------------------------------------------------------------
