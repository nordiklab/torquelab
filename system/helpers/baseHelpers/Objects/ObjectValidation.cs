//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function Lab::validateObjectName(%name, %mustHaveName)
{
	if (%mustHaveName && %name $= "")
	{
		LabMsgOK("Missing Object Name", "No name given for object.  Please enter a valid object name.");
		return false;
	}

	if (!isValidObjectName(%name))
	{
		LabMsgOK("Invalid Object Name", "'" @ %name @ "' is not a valid object name." NL
		         "" NL
		         "Please choose a name that begins with a letter or underscore and is otherwise comprised " @
		         "exclusively of letters, digits, and/or underscores."
		        );
		return false;
	}

	if (isObject(%name))
	{
		%filename = %name.getFilename();

		if (%filename $= "")
			%filename = "an unknown file";

		LabMsgOK("Invalid Object Name", "Object names must be unique, and there is an " @
		         "existing " @ %name.getClassName() @ " object with the name '" @ %name @ "' (defined " @
		         "in " @ %filename @ ").  Please choose another name.");
		return false;
	}

	if (isClass(%name))
	{
		LabMsgOK("Invalid Object Name", "'" @ %name @ "' is the name of an existing TorqueScript " @
		         "class.  Please choose another name.");
		return false;
	}

	return true;
}
//==============================================================================
// Validate a string of specific needs
//==============================================================================

//==============================================================================
// Validate a string used for a filename
function validateObjectName(%name)
{   
	return isValidObjectName( %name );
}
//------------------------------------------------------------------------------

//==============================================================================
// Validate a string used for a filename
function Editor::validateObjectName( %name, %mustHaveName )
{
   if( %mustHaveName && %name $= "" )
   {
      MessageBoxOK( "Missing Object Name", "No name given for object.  Please enter a valid object name." );
      return false;
   }
   if( !isValidObjectName( %name ) )
   {
      MessageBoxOK( "Invalid Object Name", "'" @ %name @ "' is not a valid object name." NL
         "" NL
         "Please choose a name that begins with a letter or underscore and is otherwise comprised " @
         "exclusively of letters, digits, and/or underscores."
      );
      return false;
   }
   if( isObject( %name ) )
   {
      %filename = %name.getFilename();
      if ( %filename $= "" )
         %filename = "an unknown file";

      MessageBoxOK( "Invalid Object Name", "Object names must be unique, and there is an " @
         "existing " @ %name.getClassName() @ " object with the name '" @ %name @ "' (defined " @
         "in " @ %filename @ ").  Please choose another name." );
      return false;
   }
   if( isClass( %name ) )
   {
      MessageBoxOK( "Invalid Object Name", "'" @ %name @ "' is the name of an existing TorqueScript " @
         "class.  Please choose another name." );
      return false;
   }

   return true;
}
//------------------------------------------------------------------------------
