//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// SimObject - Deletion Helpers
//==============================================================================

//==============================================================================
/// Delete an object by making sure it exist
/// %obj - Object to be deleted
function delObj(%obj)
{
	if (isObject(%obj))
		if (%obj.isMethod(delete))
			%obj.delete();
}
//------------------------------------------------------------------------------

//==============================================================================
/// Delete an object by making sure it exist
/// %obj - Object to be deleted
function delObjList(%objList,%condition)
{
	foreach$(%obj in %objList)
	{
		if (!isObject(  %obj))
			continue;
		if (%condition !$= "")
		{
			eval("%result = %obj."@%condition);
			if (!%result)
				continue;
		}
		delObj(%obj);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// SimObject - Cloning Helpers
//==============================================================================

//==============================================================================
/// Clone an object and make source invisible
/// %source = Object used as source for the cloning
/// %name = Name of the cloned object (if exist, would be deleted)
/// %internalName = InternalName for the cloned object
/// %parent = Parent group in which to add the new object
function cloneObject( %source,%name,%internalName,%parent,%cantSave )
{
	if (%name !$= "")
		delObj(%name);

	%clone = %source.deepClone();
	%clone.setVisible(true);

	if (%internalName !$= "")
		%clone.internalName = %internalName;

	%clone.setName(%name);

	if (isObject(%parent))
		%parent.add(%clone);

	if (%cantSave)
		%clone.canSave = false;

	return %clone;
}
//------------------------------------------------------------------------------

//==============================================================================
/// Clone a GuiControl and make source invisible
/// %source = Object used as source for the cloning
/// %name = Name of the cloned object (if exist, would be deleted)
/// %internalName = InternalName for the cloned object
/// %parent = Parent group in which to add the new object
function cloneGui( %source,%parent,%internalName,%canSave,%name)
{

	%gui = cloneObject(%source,%name,%internalName,%parent,!%canSave);

	return %gui;
}
//------------------------------------------------------------------------------

//==============================================================================
// SimObject Show and Hide Helpers
//==============================================================================

//==============================================================================
/// Hide an object and call onHide method if found for the object
/// %obj = Object to hide (unless child is provided)
/// %child(opt) = Children of the object to be hidden
function hide(%obj,%child)
{
	//If %child specified, hide the %obj child
	%hideMe = %obj;

	if (%child !$="")
	{
		%hideMe = %obj.findObjectByInternalName(%child,true);
	}

	if (!isObject(%hideMe))
		return false;

	if (%hideMe.isMethod("setVisible"))
		%hideMe.setVisible(false);
	else if (%hideMe.isField("hidden"))
		%hideMe.hidden = 1;

	if (%hideMe.isMethod("onHide"))
		%hideMe.onHide();
}
//------------------------------------------------------------------------------

//==============================================================================
/// Show an object and call onShow method if found for the object
/// %obj = Object to show (unless child is provided)
/// %child(opt) = Children of the object to be shown
function show(%obj,%child)
{
	//If %child specified, hide the %obj child
	%showMe = %obj;

	if (%child !$="")
	{
		%showMe = %obj.findObjectByInternalName(%child,true);
	}

	if (!isObject(%showMe))
		return false;

	if (%showMe.isMethod("setVisible"))
		%showMe.setVisible(true);
	else if (%showMe.isField("hidden"))
		%showMe.hidden = 0;

	if (%showMe.isMethod("onShow"))
		%showMe.onShow();
}
//------------------------------------------------------------------------------

//==============================================================================
/// Toggle the visibility setting of an obj or it's child if specified
/// %obj = Object to toggle visibility (unless child is provided)
/// %child(opt) = Children of the object to be toggled
function toggleVisible(%obj,%child)
{
	//If %child specified, hide the %obj child
	%toggleMe = %obj;

	if (%child !$="")
	{
		%toggleMe = %obj.findObjectByInternalName(%child,true);
	}

	if (!isObject(%toggleMe))
		return false;

	if (%toggleMe.visible)
		hide(%toggleMe);
	else
		show(%toggleMe);
}
//------------------------------------------------------------------------------

//==============================================================================
// SimObject - Debugging and console report
//==============================================================================

//==============================================================================
/// Dump the array data of object methods
/// %object = Object to dump the data from
function dumpMethods(%object)
{
	%array = %object.dumpMethods();
	%array.dumpData();
}
//------------------------------------------------------------------------------

//==============================================================================
// Default onDefineIcons functions (Make sure to set valid path to images)
function objName( %obj )
{
	return %obj.getDisplayName();
}
//------------------------------------------------------------------------------
