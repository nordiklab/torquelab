//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// ScriptObject - Create, Delete and Clone
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// ScripObject Creator
//------------------------------------------------------------------------------
/// %name = Name of the array object
/// %target = SimSet to add the arrayObject into
/// %superClass = SuperClass to be assigned to the arrayObject
/// %class = class to be assigned to the arrayObject
function newScriptObject( %name, %target,%superClass )
{	
	if (!validateObjectName(%name))
	   return;
	   
   delObj(%name);
   
	%obj = new ScriptObject(%name);
	%obj.internalName = %name;
	%obj.superClass = %superClass;
	
	if (%name !$= "")
		eval("$"@%name@" = %obj;");

	if (isObject(%target))
		%target.add(%obj);

	return %obj;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// ArrayObject Creator
//------------------------------------------------------------------------------
/// Create a new array object which would delete object with same name if exist
/// %name = Name of the array object
/// %target = SimSet to add the arrayObject into
/// %superClass = SuperClass to be assigned to the arrayObject
function newArrayObject( %name, %target,%superClass,%internalName )
{
	delObj(%name);
	%obj = new ArrayObject(%name);
	%obj.internalName = (%internalName $= "") ? %name : %internalName;
	%obj.superClass = %superClass;
	eval("$"@%name@" = %obj;");

	if (isObject(%target))
		%target.add(%obj);

	return %obj;
}
//------------------------------------------------------------------------------
//==============================================================================
// SimSet/SimGroup - Create, Delete and Clone
//==============================================================================

//==============================================================================
/// SimSet Creator function
function newSimSet( %name, %target,%internalName,%superClass )
{
	delObj(%name);
	%obj = new SimSet(%name);
	%obj.internalName = %internalName;
	if (%superClass !$= "")
		%obj.superClass = %superClass;

	eval("$"@%name@" = %obj;");

	if (isObject(%target))
		%target.add(%obj);

	return %obj;
}

//==============================================================================
/// Simgroup Creator function
function newSimGroup( %name, %target,%internalName,%superClass )
{
	delObj(%name);
	%obj = new SimGroup(%name);
	%obj.internalName = %internalName;
	if (%superClass !$= "")
		%obj.superClass = %superClass;
	eval("$"@%name@" = %obj;");

	if (isObject(%target))
		%target.add(%obj);

	return %obj;
}
