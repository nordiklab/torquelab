//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function validateDatablockName(%name)
{
	// remove whitespaces at beginning and end
	%name = trim( %name );
	// remove numbers at the beginning
	%numbers = "0123456789";

	while( strlen(%name) > 0 )
	{
		// the first character
		%firstChar = getSubStr( %name, 0, 1 );

		// if the character is a number remove it
		if( strpos( %numbers, %firstChar ) != -1 )
		{
			%name = getSubStr( %name, 1, strlen(%name) -1 );
			%name = ltrim( %name );
		}
		else
			break;
	}

	// replace whitespaces with underscores
	%name = strreplace( %name, " ", "_" );
	// remove any other invalid characters
	%invalidCharacters = "-+*/%$&�=()[].?\"#,;!~<>|�^{}";
	%name = stripChars( %name, %invalidCharacters );

	if( %name $= "" )
		%name = "Unnamed";

	return %name;
}

//==============================================================================
// Schedule global on-off - Used to limit output of fast logs
//==============================================================================

//==============================================================================
// UNUSED
function checkDatablockField(%field )
{
	//Need to run that for invalid array without bracket in code
	%strlen = strlen(%field);
	%l = getSubStr(%field,%strlen-1,1);

	if (%l $= "0" || %l $= "1" || %l $= "2" || %l $= "3" || %l $= "4" || %l $= "5" || %l $= "6" || %l $= "7" ||  %l $= "8" || %l $= "9"  )
	{
		%startStr = getSubStr(%field,0,%strlen-1);
		%final = %startStr @ "[" @ %l @ "]";
		%field = %final;
	}

	return %field;
}
//------------------------------------------------------------------------------

//==============================================================================
function getDatablockClassList( %class )
{
	foreach( %obj in DatablockSet )
	{
		if( %obj.getClassName(  ) $= %class )
		{
			%list = strAddWord(%list,%obj.getName());
		}
	}

	return %list;
}
//------------------------------------------------------------------------------

//==============================================================================
// Used by AMbient Manager to find CubemapData
function getRootClassList( %class )
{
	for( %i = 0; %i < RootGroup.getCount(); %i++ )
	{
		%obj = RootGroup.getObject(%i);

		if( %obj.getClassName() $= %class )
			%list = strAddWord(%list,%obj.getName());
	}

	return %list;
}
//------------------------------------------------------------------------------
function doListDatablockClass(%class)
{
	%list = "";

	foreach(%data in DataBlockGroup)
	{
		if (%data.isMemberOfClass( %class) )
		{
			%list = trim(%list SPC %data.getName());
		}
	}

	return %list;
}
