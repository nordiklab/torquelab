//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//============================================================================
/// Undefine all global variables matching the given name pattern
/// %patternWords - Space seperated Global pattern list - $ will be prepend and * will be append
function delGlobals(%patternWords)
{
	foreach$(%pattern in %patternWords)
		deleteVariables("$"@stripChars(%pattern,"*$")@"*");
}
//----------------------------------------------------------------------------

//==============================================================================

/*==============================================================================
DefineEngineFunction( deleteVariables, void, ( const char* pattern ),,
   "Undefine all global variables matching the given name @a pattern.\n"
   "@param pattern A global variable name pattern.  Must begin with '$'.\n"
   "@tsexample\n"
      "// Define a global variable in the \"My\" namespace.\n"
      "$My::Variable = \"value\";\n\n"
      "// Undefine all variable in the \"My\" namespace.\n"
      "deleteVariables( \"$My::*\" );\n"
   "@endtsexample\n\n"
   "@see strIsMatchExpr\n"
   "@ingroup Scripting" )
//----------------------------------------------------------------------------*/
/*==============================================================================
DefineEngineFunction( debugv, void, ( const char* variableName ),,
   "@brief Logs the value of the given variable to the console.\n\n"
   "Prints a string of the form \"<variableName> = <variable value>\" to the console.\n\n"
   "@param variableName Name of the local or global variable to print.\n\n"
   "@tsexample\n"
      "%var = 1;\n"
      "debugv( \"%var\" ); // Prints \"%var = 1\"\n"
   "@endtsexample\n\n"
   "@ingroup Debugging" )
//----------------------------------------------------------------------------*/
/*==============================================================================
DefineConsoleFunction( export, void, ( const char* pattern, const char* filename, bool append ), ( "", false ),
   "Write out the definitions of all global variables matching the given name @a pattern.\n"
   "If @a fileName is not \"\", the variable definitions are written to the specified file.  Otherwise the "
   "definitions will be printed to the console.\n\n"
   "The output are valid TorqueScript statements that can be executed to restore the global variable "
   "values.\n\n"
   "@param pattern A global variable name pattern.  Must begin with '$'.\n"
   "@param filename %Path of the file to which to write the definitions or \"\" to write the definitions "
      "to the console.\n"
   "@param append If true and @a fileName is not \"\", then the definitions are appended to the specified file. "
      "Otherwise existing contents of the file (if any) will be overwritten.\n\n"
   "@tsexample\n"
      "// Write out all preference variables to a prefs.cs file.\n"
      "export( \"$prefs::*\", \"prefs.cs\" );\n"
   "@endtsexample\n\n"
   "@ingroup Scripting" )
//----------------------------------------------------------------------------*/
/*==============================================================================
//----------------------------------------------------------------------------*/
/*==============================================================================
//----------------------------------------------------------------------------*/
/*==============================================================================
//----------------------------------------------------------------------------*/
//==============================================================================

//==============================================================================
// Validate a string of specific needs
//==============================================================================

//==============================================================================
// Characters Manipulations
//==============================================================================
