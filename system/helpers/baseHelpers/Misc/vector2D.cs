//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//============================================================================
//Taking screenshot
function VectorAdd2D(%vectA,%vectB)
{
	return %vectA.x + %vectB.x SPC %vectA.y + %vectB.y;
}
function VectorSub2D(%vectA,%vectB)
{
	return %vectA.x - %vectB.x SPC %vectA.y - %vectB.y;
}
function VectorDiv2D(%vectA,%vectB,%precision)
{
	if (%precision !$= "")
		return mFloatLength(%vectA.x / %vectB.x,%precision) SPC mFloatLength(%vectA.y / %vectB.y,%precision);
	else
		return %vectA.x / %vectB.x SPC %vectA.y / %vectB.y;
}
function VectorMul2D(%vectA,%vectB,%precision)
{
	if (%precision !$= "")
		return mFloatLength(%vectA.x * %vectB.x,%precision) SPC mFloatLength(%vectA.y * %vectB.y,%precision);
	else
		return %vectA.x * %vectB.x SPC %vectA.y * %vectB.y;
}

function VectorScale2D(%vectA,%scale)
{
	return %vectA.x * %scale SPC %vectA.y * %scale;
}

function VectorClamp2D(%vectA,%min,%max)
{
   if(getWordCount(%min) == 1)
      %min = %min SPC %min;
   if(getWordCount(%min) == 1)
      %max = %max SPC %max;  
   
   %x = getWord( %vectA,0);
   %y = getWord( %vectA,1);
   if (%y $= "")
       %y = %x;
   if (!strIsNumeric(%x)||!strIsNumeric(%y ))
      return;
   %x = mClamp( %x,getWord(%min,0),getWord(%max,0));
    %y = mClamp(%y,getWord(%min,1),getWord(%max,1));  
    
	return %x SPC %y;
}

//Will return a valid Vector by setting emptyDefault to empty x/y
function getVector2D(%vectA,%emptyDefault)
{
   if (%emptyDefault $= "")
      %emptyDefault = 0;
   if (!strIsNumeric(getWord(%vectA,0)))
      %vectA = setWord(%vectA,0,%emptyDefault);
    if (!strIsNumeric(getWord(%vectA,1)))
      %vectA = setWord(%vectA,1,%emptyDefault);
	return %vectA;
}
function VectorIClamp2D(%vectA,%min,%max)
{
   %result = VectorClamp2D(%vectA,%min,%max);
   return VectorCeil2D(%result);
  
}
function VectorCeil2D(%vectA)
{
 %x =  mCeil(getWord(%vectA,0));
%y =  mCeil(getWord(%vectA,1));	
return %x SPC %y;
}
