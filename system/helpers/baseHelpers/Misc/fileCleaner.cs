//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$HLab_FileCleaner_FixHeader = true;
$HLab_FileCleaner_FixEmptyLines = false;


$HLab_FileCleaner_MaxConsecutiveEmptyLine = "1"; 

$HLab_FileCleaner_Header[1] = "//==============================================================================";
$HLab_FileCleaner_Header[2] = "// Copyright (c) 2012-2018 GarageGames, LLC";
$HLab_FileCleaner_Header[3] = "// This software may be modified and distributed under the terms";
$HLab_FileCleaner_Header[4] = "// of the MIT license.  See tlab/LICENSE.md for details.";
$HLab_FileCleaner_Header[5] = "//==============================================================================";
//==============================================================================
// Only for fun... for now...
//==============================================================================
//fixPatternFile("tlab/system/helpers/external/*.cs");
function fixPatternFile(%pattern,%exclude,%noBackup )
{
   for(%file = findFirstFile(%pattern); %file !$= ""; %file = findNextFile(%pattern))
	{
	   %found = false;
			foreach$(%excludeStr in %excludeFileMatch)
			{
				//If already found, don't check it again
				
					%found = strFind(%file,%excludeStr);
					if (%found)
					   continue;
			}
			if (%found)
				continue;
		fixFile(%file);		
         
	}
}

function fixFile(%file,%noBackup )
{
	if (%file $= "")
		%file = "fixme.cs";

	%fileId = 0;
	%lineId = 0;
	// Create a file object for writing
	%fileObj = getFileReadObj(%file);

   %isHeader = $HLab_FileCleaner_FixHeader;
	while( !%fileObj.isEOF() )
	{
		%skipMe = false;
		%line = %fileObj.readLine();
		%trimmed = trim(%line);

		if (%trimmed $= "")
		{
			if (%removeNextEmpty && $HLab_FileCleaner_FixEmptyLines)
				%skipMe = true;

			%removeNextEmpty = true;
			
			//The header stop at first empty line or first non-commented line
			%isHeader = false;
		}
		else
		{
			%removeNextEmpty = false;
			if (%isHeader)
			{
            if (getSubStr(%trimmed,0,2) $= "//")
            {
               %skipMe = true;
               %licenseRemoved = true;
            }
            else 
            {
               %isHeader = false;
            }
            
			}
			
		}

		if (!%skipMe)
			%fileLine[%newlineId++] = %line;

		%lineId++;
	}
   %removeCount = %lineId - %newlineId;
	
	closeFileObj(%fileObj);
	
	if (%removeCount <= 0)
	   return;
	 info("Removed:",%removeCount,"lines in file:",%file);  
   if (!%noBackup)
	   backupFileExt(%file,"fixed");
	
   $Cfg_FileCleaner_RemovedLines +=%removeCount;
	%line = 0;
	%fileWrite = getFileWriteObj(%file,false);
	
	if (%licenseRemoved)
	{
	   %lid = 1;
	   while($HLab_FileCleaner_Header[%lid]!$= "")
	   {
	       %fileWrite.writeLine($HLab_FileCleaner_Header[%lid]);
	       %lid++;
	   }
	 // %fileWrite.writeLine("");
	}

	for (%i=1; %i <= %newlineId; %i++)
	{
		%fileWrite.writeLine(%fileLine[%i]);
	}

	closeFileObj(%fileWrite);

}
