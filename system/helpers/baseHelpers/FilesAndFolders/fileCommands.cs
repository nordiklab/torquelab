//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function getLocalFolder(%path)
{
	return strReplace(getUserHomeDirectory()@"/"@$appName@"/"@%path,"//","/");
}
//------------------------------------------------------------------------------
//==============================================================================
// Copy file to User Home helpers
//==============================================================================

//==============================================================================
// Handle the escape bind
function fileCopyToHome(%file,%folder,%newfile)
{
	if (%newfile $= "")
		%newfile = %file;

	%newPath = (%folder $= "") ? %newfile : "/"@%folder@"/"@%newfile;

	%newPath = strReplace(  %newPath,"//","/");
	%homeFile = getUserHomeDirectory()@"/"@$appName@"/"@%newPath;
	createPath(%homeFile);
	%result = pathCopy(%file,%homeFile,false);
	return %result;
}
//------------------------------------------------------------------------------
//==============================================================================
// Handle the escape bind
function fileBackup(%file,%suffix,%ext)
{
	if (%suffix $= "")
		%suffix = "_bak";

	%bakfile = strreplace(%file,fileExt(%file),%suffix@fileExt(%file));
	%result = pathCopy(%file,%bakfile,false);
	return %result;
}
//------------------------------------------------------------------------------

//==============================================================================
/// delFilePattern - delete files matching a pattern
function delFilePattern(%pattern,%backup,%forceDelete)
{
	for(%file = findFirstFile(%pattern); %file !$= ""; %file = findNextFile(%pattern))
	{
		if (%backup)
		{
			%result = fileCopyToHome(%file,"recycleBin");
			if (!%result)
			{
				if (!%forceDelete)
				{
					warnLog("Backup failed for file:", %file,"file deletion aborted! To force the deletion set forceDeletion to true");
					continue;
				}
				warnLog("Backup failed for file:", %file,"file deletion aborted! Proceeding to deletion as force delete is true.");
			}
		}
		fileDelete(%file);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
/// copyFilePattern - copy files matching a pattern to HOMEDIR path
function copyFilePattern(%pattern,%homeTargetPath)
{
	for(%file = findFirstFile(%pattern); %file !$= ""; %file = findNextFile(%pattern))
	{
		fileCopyToHome(%file,%homeTargetPath);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
/// execFile - Simple function to call exec on file if exist
function getUniqueFilename(%file)
{
	%filePath = filePath(%file);
	%fileName = fileBase(%file);
	%fileExt = fileExt(%file);
	while(isFile(%file))
	{
		%file = %filePath@"/"@%fileName@"_"@%id++@%fileExt;
	}
	return %file;
}
//------------------------------------------------------------------------------
//==============================================================================
/// execFile - Simple function to call exec on file if exist
function timestampFile(%file)
{
	%ext = fileExt(%file);
	%timestampedEnd = "_"@localTime("timestamp")@%ext;
	%file = strReplace(%file,%ext,%timestampedEnd);

	return %file;
}
//------------------------------------------------------------------------------

//==============================================================================
/// execFile - Simple function to call exec on file if exist
function renameFile(%file,%prefixTABsuffix,%extprefixTABsuffix)
{
	%filePrefix = getField(%prefixTABsuffix,0);
	%fileSuffix = getField(%prefixTABsuffix,1);
	%extPrefix = getField(%extprefixTABsuffix,0);
	%extSuffix = getField(%extprefixTABsuffix,1);

	%filePath = filePath(%file);
	%fileName = fileBase(%file);
	%fileExt = getSubStr(fileExt(%file),1);

	if (%filePath !$= "")
		%filePath = %filePath@"/";
	%newfile = %filePath@trim(%filePrefix)@%fileName@trim(%fileSuffix)@"."@trim(%extPrefix)@%fileExt@trim(%extSuffix);

	%newfile = strReplace(  %newfile,"//","/");

	return %newfile;
}
//------------------------------------------------------------------------------
