//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// still needs to be optimized further
function searchForTexture(%material, %texture)
{
	if (%texture !$= "")
	{
		// set the find signal as false to start out with
		%isFile= false;
		// sete the formats we're going to be looping through if need be
		%formats = ".png .jpg .dds .bmp .gif .jng .tga";

		// if the texture contains the correct filepath and name right off the bat, lets use it
		if (isFile(%texture))
			%isFile = true;
		else
		{
			for(%i = 0; %i < getWordCount(%formats); %i++)
			{
				%testFileName = %texture @ getWord(%formats, %i);

				if (isFile(%testFileName))
				{
					%isFile = true;
					break;
				}
			}
		}

		// if we didn't grab a proper name, lets use a string logarithm
		if (!%isFile)
		{
			%materialDiffuse = %texture;
			%materialDiffuse2 = %texture;
			%materialPath = %material.getFilename();

			if (strchr(%materialDiffuse, "/") $= "")
			{
				%k = 0;

				while(strpos(%materialPath, "/", %k) != -1)
				{
					%count = strpos(%materialPath, "/", %k);
					%k = %count + 1;
				}

				%materialsCs = getSubStr(%materialPath, %k, 99);
				%texture =  strreplace(%materialPath, %materialsCs, %texture);
			}
			else
				%texture =  strreplace(%materialPath, %materialPath, %texture);

			// lets test the pathing we came up with
			if (isFile(%texture))
				%isFile = true;
			else
			{
				for(%i = 0; %i < getWordCount(%formats); %i++)
				{
					%testFileName = %texture @ getWord(%formats, %i);

					if (isFile(%testFileName))
					{
						%isFile = true;
						break;
					}
				}
			}

			// as a last resort to find the proper name
			// we have to resolve using find first file functions very very slow
			if (!%isFile)
			{
				%k = 0;

				while(strpos(%materialDiffuse2, "/", %k) != -1)
				{
					%count = strpos(%materialDiffuse2, "/", %k);
					%k = %count + 1;
				}

				%texture =  getSubStr(%materialDiffuse2, %k, 99);

				for(%i = 0; %i < getWordCount(%formats); %i++)
				{
					%searchString = "*" @ %texture @ getWord(%formats, %i);
					%testFileName = findFirstFile(%searchString);

					if (isFile(%testFileName))
					{
						%texture = %testFileName;
						%isFile = true;
						break;
					}
				}
			}

			return %texture;
		}
		else
			return %texture; //Texture exists and can be found - just return the input argument.
	}

	return ""; //No texture associated with this property.
}
//------------------------------------------------------------------------------

//==============================================================================
// FileObject Helpers
//==============================================================================

//==============================================================================
$TLab_ImageExtensions = "png dds jpg jpeg bmp tga";
function isImageFile(%file)
{
	%file = strreplace(%file,"\"","");

	if (isFile(%file))
		return true;

	%extId = 0;
	%extension = getWord($TLab_ImageExtensions,%extId);

	while(%extension !$= "")
	{
		%testFile = %file @"."@%extension;

		if (isFile(%testFile))
			return true;

		%extId++;
		%extension = getWord($TLab_ImageExtensions,%extId);
	}

	return false;
}
//------------------------------------------------------------------------------
//==============================================================================

function getImageFileFull(%file)
{
	%file = strreplace(%file,"\"","");

	if (isFile(%file))
		return %file;

	%extId = 0;
	%extension = getWord($TLab_ImageExtensions,%extId);

	while(%extension !$= "")
	{
		%testFile = %file @"."@%extension;

		if (isFile(%testFile))
			return %testFile;

		%extId++;
		%extension = getWord($TLab_ImageExtensions,%extId);
	}

	return "";
}
//------------------------------------------------------------------------------

//--------------------------------------------------------------------------
// returns the text in a file with "\n" at the end of each line
//--------------------------------------------------------------------------

function loadFileText( %file)
{
	%fo = new FileObject();
	%fo.openForRead(%file);
	%text = "";

	while(!%fo.isEOF())
	{
		%text = %text @ %fo.readLine();

		if (!%fo.isEOF()) %text = %text @ "\n";
	}

	%fo.delete();
	return %text;
}

//==============================================================================
// Get the World coords from a screen pos (X Y Depth)
function getFullPath(%path,%dir)
{
	if (%path $="")
	{
		error("Invalid path");
		return;
	}

	if (%dir $="")
	{
		%return = makeFullPath( %path );
	}
	else
		%return = makeFullPath( %path,%dir );

	return %return;
}
//------------------------------------------------------------------------------

//==============================================================================
// Get the World coords from a screen pos (X Y Depth)
function getMultiExtensionFileList(%path,%extensions,%norecurse)
{
 
	foreach$(%ext in %extensions)
	{
	   if (%path !$= "")
	      %path = %path@"/";
	      
	  %path = validatePath(%path,true);   	    
      %pattern = %path@"*."@%ext;
     
      %pattern = strreplace(%pattern,"//","/");
		for(%file = findFirstFile(%pattern,!%norecurse); %file !$= ""; %file = findNextFile(%pattern))
		{
			%relFile = strreplace(%file,%path,"");
			%fileList = strAddRecord(%fileList,%relFile);
		}
	}

	return %fileList;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Check if a script file exists, compiled or not.
function isScriptFile(%path)
{
	if( isFile(%path @ ".dso") || isFile(%path) )
		return true;

	return false;
}

//==============================================================================
function addFilenameToPath(%path, %filename, %extension)
{
	%fullName = strreplace(%filename,"/","");

	if (%extension !$= "")
		%fullName = %fullName @"."@%extension;

	%fullPath = strreplace(%path @"/"@%fullName,"//","/");
	%relPath = makeRelativePath(%fullPath);
	return %relPath;
}
//------------------------------------------------------------------------------

//==============================================================================
function validatePath(%path, %makeRelative)
{
	%path = strreplace(%path,"//","/");

	if (%makeRelative)
		%path = makeRelativePath(%path);

	return %path;
}
//------------------------------------------------------------------------------
//==============================================================================
// Validate a string used for a filename
function checkFilenameString(%filename)
{
	%filename = strreplace(%filename," ","_");
	%filename = stripChars(%filename,"/?<>\\@:*|\"");
	return %filename;
}
//------------------------------------------------------------------------------

//==============================================================================
// Handle the escape bind
function doPathCopy(%srcFile,%tgtFile,%noOverwrite,%unique)
{
	%tgtFile = getUniqueFilename(%tgtFile);
	createPath(%tgtFile);

	%result = pathCopy(%srcFile,%tgtFile,%noOverwrite);
	return %result;
}
//------------------------------------------------------------------------------
//==============================================================================
// Handle the escape bind
function backupFileExt(%file,%extension)
{
	%file = makeRelativePath(%file);
	%newFile = %file@"."@%extension;
	%success = doPathCopy(%file,%newFile,false,true);

	if (%success)
		return %newFile;

	return;
}
//------------------------------------------------------------------------------
//==============================================================================
// Handle the escape bind
function backupFileHome(%this,%file,%unique,%homeSubDir)
{
	%ext = fileExt(%file);
	%fileName = fileBase(%file);
	%relPath = makeRelativePath(%file);
	%filePath = filePath(%relPath);
	%newFile = %filePath@"/"@%fileName@"_"@localTime("timestamp")@%ext@".bak";
	%newFile = strreplace(%newFile,"//","/");

	//Store it into User Home APP/
	%newFile = getLocalFolder(%homeSubDir)@"/"@%newFile;
	%success = doPathCopy(%file,%newFile,false);

	if (%success)
		return %newFile;

	return;
}
//------------------------------------------------------------------------------
//==============================================================================
// Handle the escape bind
function backupFileUser(%this,%file,%unique,%homeSubDir)
{
	%file = makeRelativePath(%file);
	%newFile = getLocalFolder(%homeSubDir)@"/"@%file;
	%success = doPathCopy(%file,%newFile,false,%unique);

	if (%success)
		return %newFile;

	return;
}
//------------------------------------------------------------------------------
