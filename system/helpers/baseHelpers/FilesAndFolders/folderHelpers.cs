//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function getFolderName( %filter, %callback,%startFolder,%title,%extraArg1,%extraArg2 )
{
	if (%title $= "")
		%title = "Select Export Folder";

	%dlg = new OpenFolderDialog()
	{
		Title = %title;
		Filters = %filter;
		DefaultFile = %startFolder;
		ChangePath = false;
		MustExist = true;
		MultipleFiles = false;
	};

	if(%dlg.Execute())
	{
		%relativePath = makeRelativePath(%dlg.FileName);
		%cb = %callback@"(\""@%relativePath@"/\", \""@%extraArg1@"\" ,  \""@%extraArg2@"\" );";
		//if (%addArgs !$= "")
		//%cb = %callback@"(\""@%relativePath@"/\""@%addArgs@");";
		//%cb = %callback@"(\""@%args@"/\");";
		eval(%cb);
	}

	%dlg.delete();
}
//------------------------------------------------------------------------------
// Update a global without the start char $ (simplier than using eval)
//------------------------------------------------------------------------------
//==============================================================================
// Get the parent folder of a file or path (Set depth for deeper folder)
function getParentFolder(%file,%depth)
{
	if (%depth $= "")
		%depth = 0;

	%folder = filePath(%file);
	%folder = trim(strReplace(%folder,"/"," "));
	%folderId = getWordCount(%folder) - 1 - %depth;
	%lastFolder = getWord(%folder,%folderId);
	return %lastFolder;
}
//------------------------------------------------------------------------------
function loadDirectory(%path, %type, %dsoType)
{
	if( %type $= "" )
		%type = "ed.cs";

	if( %dsoType $= "" )
		%dsoType = "edso";

	%cspath = %path @ "/*." @ %type;

	// Because in a shipping version there will be no .cs files, we can't just
	// find all the cs files and exec them.

	// First we find all the scripts and compile them if there are any
	// In the shipping version, this wont find anything.
	if( !$Scripts::ignoreDSOs )
	{
		%dsoReloc = compileDirectory(%cspath);

		// Finally we find all the dsos and exec them instead

		// If the DSOs are relocated by the engine (which will be the case when
		// running the tools) then we need to look for the scripts again.

		if(! %dsoReloc)
			%dsopath = %path @ "/*." @ %type @ "." @ %dsoType;
		else
			%dsopath = %cspath;
	}
	else
		%dsopath = %cspath;

	//error("Execing Directory " @ %dsopath @ " ...");
	%file = findFirstFile(%dsopath);

	while(%file !$= "")
	{
		//error("  Found File: " @ %file);
		// As we cant exec() a .dso directly, we need to strip that part from the filename
		%pos = strstr(%file, "." @ %dsoType);

		if(%pos != -1)
			%csfile = getSubStr(%file, 0, %pos);
		else
			%csfile = %file;

		exec(%csfile);
		%file = findNextFile(%dsopath);
	}
}

function compileDirectory(%path, %dsoPath)
{
	%saveDSOPath = $Scripts::OverrideDSOPath;
	$Scripts::OverrideDSOPath = %dsoPath;
	%dsoReloc = false;
	%file = findFirstFile(%path);

	//error("Compiling Directory " @ %path @ " ...");
	while(%file !$= "")
	{
		//error("  Found File: " @ %file @ " (" @ getDSOPath(%file) @ ")");
		if(filePath(%file) !$= filePath(getDSOPath(%file)))
			%dsoReloc = true;

		compile(%file);
		%file = findNextFile(%path);
	}

	$Scripts::OverrideDSOPath = %saveDSOPath;
	return %dsoReloc;
}

function listDirectory(%path)
{
	%file = findFirstFile(%path);
	echo("Listing Directory " @ %path @ " ...");

	while(%file !$= "")
	{
		echo("  " @ %file);
		%file = findNextFile(%path);
	}
}
//==============================================================================
/// Validate a folder/file path and return the fixed path
/// %path - Path to validate
/// %returnIfExist - If true, the path will be only returned if exist
function validateFolder(%path,%returnIfExist)
{
	//If the path have an extension, it's a file and we need to get the path
	if (fileExt(%path)!$= "")
		%path = filePath(%path);
	//Sometime double slash can be added when building a path
	%path = strreplace(%path,"//","/");
	%path = strreplace(%path,"\\","/");//replace \ with /

	//Make sure it's ending with /
	if (getSubStr(%path,strlen(%path)-1) !$= "/")
		%path = %path @ "/";

	if (%returnIfExist)
		%path = (isDirectory(%path)) ? %path : "";

	return %path;

}
