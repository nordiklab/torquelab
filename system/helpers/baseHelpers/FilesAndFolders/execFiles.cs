//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$HelpersExecPatternExcludeAlways = "_src";
//==============================================================================
// ExecPattern no double extension warning excluded list
$ExecPatterNoWarn["scripts/game/modes/*.cs"] = true;
$ExecPatterNoWarn["scripts/game/system/*.cs"] = true;
$ExecPatterNoWarn["scripts/game/world/*.cs"] = true;
$ExecPatterNoWarn["scripts/game/activity/*.cs"] = true;
$ExecPatterNoWarn["scripts/game/triggers/*.cs"] = true;
$ExecPatterNoWarn["scripts/server/database/*.cs"] = true;
$ExecPatterNoWarn["scripts/server/ai/AIPlayer/*.cs"] = true;
$ExecPatterNoWarn["tlab/tools/*.cs"] = true;
$ExecPatterNoWarn["tlab/guiEditor/system/*.cs"] = true;

//$ExecPatterNoWarn[] = true;
//$ExecPatterNoWarn[] = true;
//------------------------------------------------------------------------------

//==============================================================================
// Define the client-only file extension for use with execClientDir
$HelpersClientFileExt = "cl.cs";
//------------------------------------------------------------------------------

//==============================================================================
// ExecPattern functions - Exec all files inside folder matching a pattern
//==============================================================================

//==============================================================================
/// execPattern -- Execute all files found that match a specified pattern
/// %pattern - Pattern for the files to be execed
/// %excludeFileMatch - If set, the file path containing the exclude string will be ignored
/// %midDotAllowed - if true, no check will be made for double extensions (*.cs will exec file.ed.cs)
function execPattern(%pattern,%excludeFileMatch,%midDotAllowed,%checkDso)
{
	%patExplode = strReplace(%pattern,"*","\t");
	%scanExt = getField(%patExplode,1);
	if ($HelpersExecPatternExcludeAlways !$= "")
	   %excludeFileMatch = $HelpersExecPatternExcludeAlways SPC %excludeFileMatch;
	//call exec() on all files found matching the pattern
	for(%file = findFirstFile(%pattern); %file !$= ""; %file = findNextFile(%pattern))
	{
		if (%excludeFileMatch !$= "")
		{
			%found = false;
			foreach$(%excludeStr in %excludeFileMatch)
			{
				//If already found, don't check it again
				if (!%found)
					%found = strFind(%file,%excludeStr);

			}
			if (%found)
				continue;
			//info("ExecPattern NOT  excluded a file",%file,"Match:",%excludeFileMatch);
		}
		if (!%midDotAllowed)
		{
			//Can't use filebase because it won't care about double ext pattern
			%nudeFile = strreplace(%file,%scanExt,"");
			//Skip if  there's a dot as double ext ("file.ed.cs") are not allowed
			if (strFind(%nudeFile,"."))
				continue;

		}

		//Check for .gui file and call special function which won't reload existing GUIs unless specified
		if (fileExt(%file) $= ".gui")
		{
			execGui(%file,false);
			continue;
		}
		//Check for .gui file and call special function which won't reload existing GUIs unless specified
		if (fileExt(%file) $= ".dso")
		{
			%file = strreplace(%file,".dso","");
			//warnLog("Dso file found, converted to .cs for exec",%file);

			//If checkDso true, we skip .dso that have an existing .cs version
			if (%checkDso && isFile(%file))
			{
				// warnLog("dso skipped because there's a .cs version",%file);
				continue;
			}
		}

		exec( %file );
	}
}
//------------------------------------------------------------------------------

///=============================================================================
/// execPath -- Variation in which excluded match are set as first field pf pattern
/// - %patternAndExclude => Scan files pattern and optional exclude match as fields
///      EX: execPath("scripts/*.cs" TAB "skips words if match"
/// %midDotAllowed - if true, no check will be made for double extensions (*.cs will exec file.ed.cs)
function execPath(%patternAndExclude,%checkDso)
{
	%pattern = getField(%patternAndExclude,0);
	%excludeFileMatch = getField(%patternAndExclude,1);
	execPattern(%pattern,%excludeFileMatch,%midDotAllowed,%checkDso);
}
//------------------------------------------------------------------------------

///=============================================================================
/// execPath -- Variation in which excluded match are set as first field pf pattern
/// - %patternAndExclude => Scan files pattern and optional exclude match as fields
///      EX: execPath("scripts/*.cs" TAB "skips words if match"
/// %exts - extensions to search divided with space for multiple (can be double extension like ed.cs)
/// %midDotAllowed - if true, no check will be made for double extensions (*.cs will exec file.ed.cs)
function execDsoPath(%dirAndExclude,%exts,%midDotAllowed)
{

	execDir(%dirAndExclude,%exts,%midDotAllowed,true);

}
//------------------------------------------------------------------------------

//==============================================================================
// ExecDir functions - Exec all files inside folder matching an extension
//==============================================================================

//==============================================================================
/// execDir -- Execute all files found in a directory with specified extension
//             Unlike execPattern, it don't check for double extension by default
/// %dir - Directory root to start the search (Pattern allowed if no %ext provided
/// %ext - extension to serach with no dot at start (can be double extension like ed.cs)
function execDir(%dirAndExclude,%exts,%midDotAllowed,%checkDso)
{
	%dir = getField(%dirAndExclude,0);
	%excludeFileMatch = getField(%dirAndExclude,1);
	foreach$(%ext in %exts)
	{
		//Remove firstchar if dot (Will be included in pattern builder)
		if (getSubStr(%ext,0,1) $= ".")
			%ext = getSubStr(%ext,1);

		%pattern = %dir@"/*."@%ext;
		if (%checkDso)
		{
			%dsopattern = %pattern@".dso";
			execPattern( %dsopattern,%excludeFileMatch,%midDotAllowed,true);
		}

		execPattern( %pattern,%excludeFileMatch,%midDotAllowed,%checkDso);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Exec only the defined client-only files found in a directory
function execClientDir(%dir)
{
	execDir(%dir,$HelpersClientFileExt);
}
//------------------------------------------------------------------------------
//==============================================================================
// Execing GUI (.gui) files helpers
//==============================================================================

//==============================================================================
/// execGuiDir -- Execute all .gui files found that match a specified pattern
///      It will automatically skip files which have an object matching the name
/// %folderAndExclude - Scan files pattern and optional exclude match as fields
///         EX: execGuiDir("scripts/gui" TAB "skips words if match");
/// %checkFileBaseObj - Skip file if an object match the file base name (without extension)
/// %overideGui - Delete and reload existing Gui
function execGuiDir(%folderAndExclude,%checkFileBaseObj,%overideGui)
{
	%folderRoot = getField(%folderAndExclude,0);
	%excludeFileMatch = getField(%folderAndExclude,1);
	//in case a .gui have been submitted, replace it with nothing and re-add .gui at end
	//If no extension sent the replace will do nothing and it would be like adding ext only
	%folderRoot = %folderRoot@"/";
	%folderRoot = strreplace(%folderRoot,"//","/");
	%pattern = strreplace(%folderRoot,"*.gui","")@"*.gui";
	//execPattern(%pattern,%excludeFileMatch,"",%replace);
	for(%file = findFirstFile(%pattern); %file !$= ""; %file = findNextFile(%pattern))
	{
		if (%excludeFileMatch !$= "")
		{
			%found = false;
			foreach$(%excludeStr in %excludeFileMatch)
			{
				//If already found, don't check it again
				if (!%found)
					%found = strFind(%file,%excludeStr);

			}
			if (%found)
				continue;
			//info("ExecPattern NOT  excluded a file",%file,"Match:",%excludeFileMatch);
		}

		execGui(%file,%checkFileBaseObj,%overideGui);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
/// execGui -- Exec a single .gui file if pre-exec validation succeed
/// %file - File to be check and execed
/// %checkFileBaseObj - Skip file if an object match the file base name (without extension)
/// %overideGui - Delete and reload existing Gui
function execGui(%file,%checkFileBaseObj,%overideGui)
{
	if (%checkFileBaseObj)
	{
		if(isObject(fileBase(%file)))
		{
			if (%overideGui)
            delObj(fileBase(%file));
         else
            return;
		}
	}

	if (isObject($ExecedGuiFile[%file]))
	{
		if (%overideGui)
			delObj($ExecedGuiFile[%file]);
		else
			return;
	}

	%guiContent = "";
	exec( %file );

	if (isObject(%guiContent))
	{
		$ExecedGuis = trim($ExecedGuis SPC %guiContent.getId());
		$ExecedGuiFile[%file] = %guiContent;
	}
}
//------------------------------------------------------------------------------

//==============================================================================
/// clearGuiDir -- Delete all Gui found in a dir
/// %folderAndExclude - Scan files pattern and optional exclude match as fields
///         EX: execGuiDir("scripts/gui" TAB "skips words if match");
function clearGuiDir(%folderAndExclude)
{
	%folderRoot = getField(%folderAndExclude,0);
	%excludeFileMatch = getField(%folderAndExclude,1);
	//in case a .gui have been submitted, replace it with nothing and re-add .gui at end
	//If no extension sent the replace will do nothing and it would be like adding ext only
	%folderRoot = %folderRoot@"/";
	%folderRoot = strreplace(%folderRoot,"//","/");
	%pattern = strreplace(%folderRoot,"*.gui","")@"*.gui";

	for(%file = findFirstFile(%pattern); %file !$= ""; %file = findNextFile(%pattern))
	{
		if (%excludeFileMatch !$= "")
		{
			%found = false;
			foreach$(%excludeStr in %excludeFileMatch)
			{
				//If already found, don't check it again
				if (!%found)
					%found = strFind(%file,%excludeStr);

			}
			if (%found)
				continue;
			//info("ExecPattern NOT  excluded a file",%file,"Match:",%excludeFileMatch);
		}

		if (isObject($ExecedGuiFile[%file]))
		{
			%deleteIds = strAddWord(%deleteIds,$ExecedGuiFile[%file].getId(),true);
			$ExecedGuiFile[%file] = "";
		}
		if(isObject(fileBase(%file)))
		{
			%deleteIds = strAddWord(%deleteIds,fileBase(%file).getId(),true);
		}

	}
	delObjList(%deleteIds);
}
//------------------------------------------------------------------------------
//==============================================================================
/// clearGuiDir -- Delete all Gui found in a dir
/// %folderAndExclude - Scan files pattern and optional exclude match as fields
///         EX: execGuiDir("scripts/gui" TAB "skips words if match");
function getGuiDir(%folderAndExclude)
{
	%folderRoot = getField(%folderAndExclude,0);
	%excludeFileMatch = getField(%folderAndExclude,1);
	//in case a .gui have been submitted, replace it with nothing and re-add .gui at end
	//If no extension sent the replace will do nothing and it would be like adding ext only
	%folderRoot = %folderRoot@"/";
	%folderRoot = strreplace(%folderRoot,"//","/");
	%pattern = strreplace(%folderRoot,"*.gui","")@"*.gui";

	for(%file = findFirstFile(%pattern); %file !$= ""; %file = findNextFile(%pattern))
	{
		if (%excludeFileMatch !$= "")
		{
			%found = false;
			foreach$(%excludeStr in %excludeFileMatch)
			{
				//If already found, don't check it again
				if (!%found)
					%found = strFind(%file,%excludeStr);

			}
			if (%found)
				continue;
			//info("ExecPattern NOT  excluded a file",%file,"Match:",%excludeFileMatch);
		}

		if (isObject($ExecedGuiFile[%file]))
		{
			%deleteIds = strAddWord(%deleteIds,$ExecedGuiFile[%file].getId(),true);
			$ExecedGuiFile[%file] = "";
		}
		if(isObject(fileBase(%file)))
		{
			%deleteIds = strAddWord(%deleteIds,fileBase(%file).getId(),true);
		}

	}
	return %deleteIds;

}
//------------------------------------------------------------------------------

//==============================================================================
/// execFile - Simple function to call exec on file if exist
function execFile(%file)
{
	if (isFile(%file))
		exec(%file);
}
//------------------------------------------------------------------------------

