//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// SimObject - Create, Delete and Clone
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
/// Create a FileObject and open it for reading
/// %file - File to be readed
function getFileReadObj(%file)
{
	// Create a file object for reading
	%fileRead = new FileObject();
	// Open a text file, if it exists
	%fileRead.OpenForRead(%file);
	return %fileRead;
}
//------------------------------------------------------------------------------
/// Create a FileObject and open it for write or append
/// %file - File to be written
/// %append - If true, the writting will be append at end of file
function getFileWriteObj(%file,%append)
{
	// Create a file object for reading
	%fileObj = new FileObject();

	// Open a text file, if it exists
	if (%append)
		%fileObj.OpenForAppend(%file);
	else
		%fileObj.OpenForWrite(%file);

	return %fileObj;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
/// Close a FileObject and delete it
/// %fileObj - FileObject to be deleted
function closeFileObj(%fileObj)
{
	// Close the text file
	%fileObj.close();
	// Cleanup
	%fileObj.delete();
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Special helpers for quick write an object data to a file
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
/// Write object properties to a file (to append use appendObjectToFile)
function writeObjectToFile(%obj,%file,%prepend)
{
	%fileWrite = new FileObject();
	%result = %fileWrite.OpenForWrite(%file);

	if (%result && isObject(%obj))
	{
		if (%prepend !$="")
			%fileWrite.writeObject(%obj,%prepend);
		else
			%fileWrite.writeObject(%obj);
	}

	%fileWrite.close();
	%fileWrite.delete();
	return %result;
}

//------------------------------------------------------------------------------
/// Append object properties to a file (to overwrite file use writeObjectToFile)
function appendObjectToFile(%obj,%file,%prepend)
{
	%fileWrite = new FileObject();
	%result = %fileWrite.OpenForAppend(%file);

	if (%result && isObject(%obj))
	{
		if (%prepend !$="")
			%fileWrite.writeObject(%obj,%prepend);
		else
			%fileWrite.writeObject(%obj);
	}

	%fileWrite.close();
	%fileWrite.delete();
	return %result;
}
//------------------------------------------------------------------------------

function FileObject::emptyLine(%this)
{
	%this.writeLine("");
}
//------------------------------------------------------------------------------
