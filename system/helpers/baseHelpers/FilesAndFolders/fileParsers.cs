//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Remove all line that have a match with one of search string records
//==============================================================================
function fileLineRemover(%file,%removeLineMatches,%logRemoval)
{
	%fileObj = getFileReadObj(%filename);

	while(!%fileObj.isEOF())
	{
	   %line = %fileObj.readLine();
	   if (%skip > 0)
		{
		   %skip--;
		   
		    if (%logRemoval)
		      info("Line skipped requested from previous match:", %match,"Left to skip:",%skip);
			
			continue;
		}
		
		for (%i = 0;%i < getRecordCount(%removeLineMatches);%i++)
		{
		   
		   %matchData = getRecord(%removeLineMatches,%i);
		   %match = getField(%matchData,0);
		   %matchSkips = getField(%matchData,1); //If set, the next lines will be removed too
		   if (strpos(%line,%match) !$= "-1")
		   {
		      if (%logRemoval)
		      {
		         info("Line marked as removed:",%line,"--> Match -->",%match);
		         if (%matchSkips > 0)
		            info("The following",%matchSkips,"lines will be skipped and removed");
               
		      }			  
			   %skip = %matchSkips;
			   continue;
		   }
		}
		
		%output[%co++] = %line;
	}
	closeFileObj(%fileObj);
	%fileWrite = getFileWriteObj(%filename);
	for (%i = 1; %i <= %co; %i++)
		%fileWrite.writeLine(%output[%i]);

	closeFileObj(%fileWrite);
}
//------------------------------------------------------------------------------
