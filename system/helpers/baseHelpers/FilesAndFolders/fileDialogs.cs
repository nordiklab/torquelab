//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

/*
Derived from FileDialog, this class is responsible for opening a file browser with the intention of opening a file. 

The core usage of this dialog is to locate a file in the OS and return the path and name. This does not handle the actual file parsing or data manipulation. That functionality is left up to the FileObject class.

Example:
 // Create a dialog dedicated to opening files
 %openFileDlg = new OpenFileDialog()
 {
    // Look for jpg image files
    // First part is the descriptor|second part is the extension
    Filters = "Jepg Files|*.jpg";
    // Allow browsing through other folders
    ChangePath = true;

    // Only allow opening of one file at a time
    MultipleFiles = false;
 };

 // Launch the open file dialog
 %result = %openFileDlg.Execute();

 // Obtain the chosen file name and path
 if ( %result )
 {
    %seletedFile = %openFileDlg.file;
 }
 else
 {
    %selectedFile = "";
 }

 // Cleanup
 %openFileDlg.delete();

*/
//==============================================================================
// Universal function to get an existing file (Works for save but more complicated)
function getFile( %filters,%defaultFile,%defaultPath,%allowChangePath, %mustExist)
{
   
   if (%filters $= "") %filters = "All Files | *.*";
   if (%allowChangePath $= "") %allowChangePath = true;
   if (%mustExist $= "") %mustExist = true;

   if (!isFile(%defaultFile))
        %defaultPath = filePath(%defaultFile);   
     
	%dlg = new OpenFileDialog()
	{
		Filters        = %filters;
		DefaultPath    = %defaultPath;
		DefaultFile    = %defaultFile;
		ChangePath     = %allowChangePath;
		MustExist      = %mustExist;
	};
	%ret = %dlg.Execute();

	if ( %ret )
		%file = %dlg.FileName;
   
	%dlg.delete();

	if ( !%ret )
		return;
	
	return makeRelativePath(%file);
}
//------------------------------------------------------------------------------
// Simple Variation to get a texture/image
function getLoadFile( %filters,%defaultPath)
{
   return getFile(%filters,%defaultPath);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Simple Variation to get a texture/image
function getTextureFile( %defaultFile,%defaultPath)
{
   %filters = "Image Files (*.png, *.jpg, *.dds, *.bmp, *.gif, *.jng. *.tga)|*.png;*.jpg;*.dds;*.bmp;*.gif;*.jng;*.tga|All Files (*.*)|*.*|";
   return getFile(%filters,%defaultFile,%defaultPath);
}
//------------------------------------------------------------------------------

//==============================================================================
// Function specialy made to choose a save file which exist or not
function getSaveFile( %filters,%defaultFile)
{
   info("File",%defaultFile);
   if (%filters $= "") %filters = "All Files | *.*";

   %defaultPath = filePath(%defaultFile);   
  
	%dlg = new SaveFileDialog()
	{
		Filters        = %filters;	
		//DefaultPath    = %defaultPath;
		DefaultFile    = %defaultFile;		
		ChangePath     = false;
		MustExist      = false;
	};
	if( filePath( %defaultFile ) !$= "" )
		%dlg.DefaultPath = filePath( %defaultFile );
	else
		%dlg.DefaultPath = getMainDotCSDir();
		
	%ret = %dlg.Execute();
   %file = %dlg.FileName;   
	%dlg.delete();
	
	if ( %ret )
		return makeRelativePath(%file);
   
	return false;
	
}
//------------------------------------------------------------------------------

//==============================================================================
// Torque3D Stock FileDialog Functions
//==============================================================================

//==============================================================================
// Stock Torque3D getSaveFilename - Kept Intact for maximum compatibility
function getSaveFilename( %filespec, %callback, %currentFile, %overwrite )
{
	if( %overwrite $= "" )
		%overwrite = true;

	%dlg = new SaveFileDialog()
	{
		Filters = %filespec;
		DefaultFile = %currentFile;
		ChangePath = false;
		OverwritePrompt = %overwrite;
	};

	if( filePath( %currentFile ) !$= "" )
		%dlg.DefaultPath = filePath( %currentFile );
	else
		%dlg.DefaultPath = getMainDotCSDir();

	if( %dlg.Execute() )
	{
		%filename = %dlg.FileName;
		eval( %callback @ "(\"" @ %filename @ "\");" );
	}

	%dlg.delete();
}
//------------------------------------------------------------------------------
//==============================================================================
function getLoadFilename(%filespec, %callback, %currentFile, %getRelative, %defaultPath)
{
   //If no default path passed in then try to get one from the file
   if(%defaultPath $= "")
   {
      if ( filePath( %currentFile ) !$= "" )
         %defaultPath = filePath(%currentFile);
   }
   
   %dlg = new OpenFileDialog()
   {
      Filters = %filespec;
      DefaultFile = %currentFile;
      DefaultPath = %defaultPath;
      ChangePath = false;
      MustExist = true;
      MultipleFiles = false;
   };
   
   %ok = %dlg.Execute();
   if ( %ok )
   {
      %file = %dlg.FileName;
      if(%getRelative)
         %file = strreplace(%file,getWorkingDirectory() @ "/", "");
      eval(%callback @ "(\"" @ %file @ "\");");
      $Tools::FileDialogs::LastFilePath = filePath( %dlg.FileName );
   }
   
   %dlg.delete();
   
   return %ok;
}
//==============================================================================
// Opens a choose file dialog with format filters already loaded
// in. This avoids the issue of passing a massive list of format 
// filters into a function as an arguement.
function getLoadFormatFilename(%callback, %currentFile)
{   
   %dlg = new OpenFileDialog()
   {
      Filters = getFormatFilters() @ "(All Files (*.*)|*.*|";
      DefaultFile = %currentFile;
      ChangePath = false;
      MustExist = true;
      MultipleFiles = false;
   };
   
   if ( filePath( %currentFile ) !$= "" )
      %dlg.DefaultPath = filePath(%currentFile);  
      
   if ( %dlg.Execute() )
   {
      eval(%callback @ "(\"" @ %dlg.FileName @ "\");");
      $Tools::FileDialogs::LastFilePath = filePath( %dlg.FileName );
   }
   
   %dlg.delete();
}
