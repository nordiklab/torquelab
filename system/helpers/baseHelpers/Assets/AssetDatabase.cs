//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Simple replacement of removed isWebDemo code check

//------------------------------------------------------------------------------
function getAllAsset()
{
	%query = new AssetQuery();
	AssetDatabase.findAllAssets( %query );

	for( %i = 0; %i < %query.getCount(); %i++ )
	{
		%assetId = %query.getAsset( %i );
		echo( AssetDatabase.getAssetName(%assetId) );
		echo( AssetDatabase.getAssetDescription(%assetId) );
		echo( AssetDatabase.getAssetType(%assetId) );
	}

	%query.delete();
}
