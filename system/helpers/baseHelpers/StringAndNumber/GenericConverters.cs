//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$HLab_ScreenshotsFolder = "core/dump/screenshots/";
//-----------------------------------------------------------------------------
// Load up our main GUI which lets us see the game.

//============================================================================
//Taking screenshot

function dec2hex(%val)
{
	// Converts a decimal number into a 2 digit HEX number
	%digits ="0123456789ABCDEF";    //HEX digit table

	//(13 * 16) + (14 * 1)
	// To get the first number we divide by 16 and then round down, using
	// that number as a lookup into our HEX table.
	%firstDigit = getSubStr(%digits,mFloor(%val/16),1);

	// To get the second number we do a MOD 16 and using that number as a
	// lookup into our HEX table.
	%secDigit = getSubStr(%digits,%val % 16,1);

	// return our two digit HEX number
	return %firstDigit @ %secDigit;
}
function hex2dec(%val)
{
	// Converts a decimal number into a 2 digit HEX number
	%digits ="0 1 2 3 4 5 6 7 8 9 A B C D E F";    //HEX digit table

	//(13 * 16) + (14 * 1)
	// To get the first number we divide by 16 and then round down, using
	// that number as a lookup into our HEX table.
	%hex[0] = getSubStr(%val,0,1);
	%hex[1]= getSubStr(%val,1,1);

	%hexNum[0] = getWordIndex(%digits,%hex[0]);
	%hexNum[1] = getWordIndex(%digits,%hex[1]);
	%firstDigit = %hexNum[0] * 16;
	%secDigit = %hexNum[1] * 1;

	// return our two digit HEX number
	return %firstDigit + %secDigit;
}
