//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Validate a string of specific needs
//==============================================================================

//==============================================================================
// Characters Manipulations
//==============================================================================
//==============================================================================
/// Get the characters at end of string for the specified count
/// Optional: %count -> Will return that amount of characters. (default = 1)
function strLastChars(%str,%count)
{
	%len = strlen(%str);
	%chars = getSubStr(%str,%len-%count);
	return %chars;
}
//------------------------------------------------------------------------------
//==============================================================================
/// Get the characters at end of string for the specified count
/// Optional: %count -> Will return that amount of characters. (default = 1)
function strStartChars(%str,%count)
{
	return getSubStr(%str,0,%count);
}
//------------------------------------------------------------------------------
//==============================================================================
/// Capitalise the first letter of the input string
function strCap( %str )
{
	%len = strlen( %str );
	return strupr( getSubStr( %str,0,1 ) ) @ getSubStr( %str,1,%len-1 );
}
//------------------------------------------------------------------------------
//==============================================================================
// Words Manipulations
//==============================================================================
//==============================================================================
//
function strAddWord(%str,%word,%unique,%prepend)
{
	if (%unique)
	{
		foreach$(%w in %str)
		{
			if (%w $= %word)
			{
				return %str;
			}
		}
	}

	if ((strlen(%str) + strlen(%word)) > 5000)
	{
		%str = "Junk";
		echo("Error! strAddWord() - String dropped to prevent overflow...");
	}

	if (%prepend)
		%newStr = trim(%word SPC %str);
	else
		%newStr = trim(%str SPC %word);

	return %newStr;
}
//------------------------------------------------------------------------------

//==============================================================================
//
function strRemoveWord(%str,%word)
{
	%id = 0;

	foreach$(%w in %str)
	{
		if (%w $= %word)
		{
			//Reverse order to remove words from last to not affect index ordering
			%removeIds = strAddWord(%removeIds,%id,true,true);
		}

		%id++;
	}

	foreach$(%id in %removeIds)
	{
		%preStr = %str;
		%str = removeWord(%str,%id);
	}

	return %str;
}
//------------------------------------------------------------------------------
//==============================================================================
/// Gey the last word of a string or the word index from last
/// Optional: %index -> Will return the word at index counting from end
/// return the word found or empty string if not found
function getLastWord(%str,%index)
{
	%lastWord = getWord(%str,getWordCount(%str)-1-%index);
	return %lastWord;
}
//------------------------------------------------------------------------------
//==============================================================================
/// Get the last word of a string (if index specified, will count from end)
/// Optional: %index -> Will return the word at index counting from end
/// return the word found or empty string if not found
function lastWord(%str,%index)
{
	return getLastWord(%str,%index);
}
//------------------------------------------------------------------------------
//==============================================================================
/// Get the index of a word in a string
/// %str -> String to search the word in
/// %findWord -> Word that we want the index
/// return -> Index of the word, if word not found, return -1
function getWordIndex(%str,%findWord)
{
	%index = 0;
	foreach$(%word in %str)
	{
		if (%word $= %findWord)
			return %index;

		%index++;
	}
	return "-1";
}
//------------------------------------------------------------------------------
//==============================================================================
// Check if the word contain any of the supplied words. Return true if found.
// Optional: %all -> If %all is true, must find all words to return true;
function strFindWords(%str,%words,%all)
{
   %str = strlwr(%str);
	foreach$(%word in %words)
	{
		%result = strstr(%str,strlwr(%word));

		//If not found and all is required, return false
		if (%result $= "-1" && %all)
			return false;

		//If word is found, return true;
		if (%result !$= "-1")
			return true;
	}

	//If %all is true and still here, it mean all words have been found
	if (%all)
		return true;

	//If still here, it mean we found not word
	return false;
}
//------------------------------------------------------------------------------

//==============================================================================
/// Check if the word contain any of the supplied words. Return true if found.
/// Optional: %all -> If %all is true, must find all words to return true;
function strFind(%str,%find,%allowEmpty)
{
	if (%allowEmpty && %find $= "")
		return true;

	//Add a space before string so if found, won't be at position 0
	%result = strstr(" "@%str,%find);

	if (%result $= "-1" || %result $= "" || !%result )
		return false;

	return true;
}
//------------------------------------------------------------------------------
//==============================================================================
/// Check if the word contain any of the supplied words. Return true if found.
/// Optional: %all -> If %all is true, must find all words to return true;
function strFindList(%str,%wordList,%findAll)
{
	%str = strlwr(%str);
	foreach$(%word in %wordList)
	{
		%result = strstr(%str,strlwr(%word));

		//If not found and all is required, return false
		if (%result $= "-1" && %findAll)
			return false;

		//If word is found, return true;
		if (%result !$= "-1" && !%findAll)
			return true;
	}
	if (%findAll)
		return true;
	return false;
}
//------------------------------------------------------------------------------

//==============================================================================
/// Check if the word contain any of the supplied words. Return true if found.
/// Optional: %all -> If %all is true, must find all words to return true;
function recordFind(%records,%find,%partof)
{
	for( %i = 0; %i < getRecordCount( %records ); %i++ )
	{
		%record = getRecord( %records, %i );

		if (%record $= %find)
			return %i;

		if (!%partof)
			continue;

		if (strFind(%record,%find))
			return %i;
	}

	return "-1";
}
//------------------------------------------------------------------------------

//==============================================================================
//
function strAddField(%str,%field,%unique,%addEmpty)
{
	if (%unique)
	{
		for(%i =0; %i < getFieldCount(%str); %i++)
		{
			%check = getField(%str,%i);

			if (%check $= %field)
			{
				return %str;
			}
		}
	}

	if (%addEmpty && %field $= "")
		%field = " ";

	if (%str !$= "" )
		%str = %str @"\t"@ %field;
	else
		%str = %field;

	if (!%addEmpty)
		%str = trim(%str);

	return %str;
}
//------------------------------------------------------------------------------
//==============================================================================
function strAddRecord(%str,%line)
{
	if (%str !$= "")
		%str = %str NL %line;
	else
		%str = %line;

	%str = trim(%str);
	return %str;
	
}
//------------------------------------------------------------------------------
//==============================================================================
function strAddCommaWord(%str,%line,%unique)
{
	if (%unique)
	{
		if (getCommaWordIndex(%str,%line) !$= "-1")
			return %str;
	}
	
	if (%str !$= "")
		%str = %str @","@ %line;
	else
		%str = %line;

	%str = trim(%str);
	return %str;
}
//------------------------------------------------------------------------------
//==============================================================================
//
function strRemoveCommaWord(%str,%word)
{
	%newStr = "";
	%word = trim(%word);
	for(%i =0; %i < getCommaWordCount(%str); %i++)
	{
		%check = getCommaWord(%str,%i);
		if (trim(%check) $= %word)
			continue;
		%newStr = strAddCommaWord(%newStr,%check);

	}

	return %newStr;
}
//------------------------------------------------------------------------------
//==============================================================================
function getCommaWordIndex(%str,%word)
{
	%word = trim(%word);
	for(%i =0; %i < getCommaWordCount(%str); %i++)
	{
		%check = getCommaWord(%str,%i);
		if (trim(%check) $= %word)
			return %i;

	}
	return "-1";
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Convert an HTML string into a game text
function strBeforeChar(%str,%search)
{
	%searchPos = strpos(%str,%search);
	%newStr = getSubStr(%str,0,%searchPos);
	return %newStr;
}

function strBreakChar(%str,%charToBreak,%last)
{
	%searchPos = strpos(%str,%charToBreak);
	%startStr = getSubStr(%str,0,%searchPos);
	%endStr = getSubStr(%str,%searchPos+1);
	return trim(%startStr) TAB trim(%endStr);
}

//==============================================================================
// Advanced string replacements
//==============================================================================
function strReplaceList(%str,%replaces)
{
	%count = getRecordCount(%replaces);

	for(%i=0; %i<%count; %i++)
	{
		%record = getRecord(%replaces,%i);
		%find = getField(%record,0);
		%replace = getField(%record,1);
		%str = strreplace(%str,%find,%replace);
	}

	return %str;
}

//--------------------------------------------------------------------------
// Finds location of %word in %text, starting at %start.  Works just like strPos
//--------------------------------------------------------------------------

function wordPos(%text, %word, %start)
{
	if (%start $= "") %start = 0;

	if (strpos(%text, %word, 0) == -1) return -1;

	%count = getWordCount(%text);

	if (%start >= %count) return -1;

	for (%i = %start; %i < %count; %i++)
	{
		if (getWord( %text, %i) $= %word) return %i;
	}

	return -1;
}

//--------------------------------------------------------------------------
// Finds location of %field in %text, starting at %start.  Works just like strPos
//--------------------------------------------------------------------------

function fieldPos(%text, %field, %start)
{
	if (%start $= "") %start = 0;

	if (strpos(%text, %field, 0) == -1) return -1;

	%count = getFieldCount(%text);

	if (%start >= %count) return -1;

	for (%i = %start; %i < %count; %i++)
	{
		if (getField( %text, %i) $= %field) return %i;
	}

	return -1;
}

