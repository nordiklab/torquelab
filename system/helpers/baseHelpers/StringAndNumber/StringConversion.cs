//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$ColTag[0] = "\c0";
$ColTag[1] = "\c1";
$ColTag[2] = "\c2";
$ColTag[3] = "\c3";
$ColTag[4] = "\c4";
$ColTag[5] = "\c5";
$ColTag[6] = "\c6";
$ColTag[7] = "\c7";
$ColTag[8] = "\c8";
$ColTag[9] = "\c9";

//==============================================================================
// String Manipulation Helpers
//==============================================================================

function addColorToFields(%fields,%colIds)
{
	for(%i=0; %i<getFieldCount(%fields); %i++)
	{
		%field = getField(%fields,%i);

		%col = getWord(%colIds,%i);
		%output = strAddField(%output,$ColTag[%col]@%field);
	}
	return %output;
}
function getStringFromObjectFields(%obj,%string,%dividers)
{
	foreach$(%word in %string)
	{
		%value = %obj.getFieldValue(%word);
		%output = %output@%div@%value;
		%div = %dividers;
	}
	return %output;
}
function getStringFromFieldValues(%fields,%string,%dividers)
{
	for(%i=0; %i<getFieldCount(%fields); %i++)
	{
		%field = getField(%fields,%i);
		%key = firstWord(%field);
		%val = restWords(%field);
		%fieldVal[%key] = %val;
		%exist[%key] = true;
	}
	foreach$(%word in %string)
	{
		if (!%exist[%key])
			%value = "MissingKey";
		else
			%value = %fieldVal[%word];
		%output = %output@%div@%value;
		%div = %dividers;
	}
	return %output;
}

//==============================================================================
// String Manipulation Helpers
//==============================================================================

//<a:www.reallyreallygoodthings.com>aa</a>
//------------------------------------------------------------------------------
// Convert an HTML string into a game text
function convertHtmlToGame(%html)
{
	//Replace all </p> with new line
	%newText = strreplace(%html, "</p>", "\n" );
	%newText = strreplace(%newText, "<p>", "" );
	return %newText;
}

//------------------------------------------------------------------------------
// Convert the extra fields of K2
function convertExtraFields(%fields)
{
	//"[{\"id\":\"1\",\"value\":\"1\"},{\"id\":\"2\",\"value\":\"1\"}]";
	//Replace all </p> with new line
	%newText = strreplace(%fields, "</p>", "\n" );
	%newText = strreplace(%newText, "<p>", "" );
	return %newText;
}
