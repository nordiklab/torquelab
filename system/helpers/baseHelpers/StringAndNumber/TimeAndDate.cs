//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Empty Editor Gui
function localTime(%format,%noAMPM,%offsetH)
{
	if (%format $= "")
		%format = "HH:SS";
	%t= getLocalTime();
	%month = getWord (%t, 1);
	%day = getWord (%t, 2);
	%year = getWord (%t, 3);
	%hour = getWord (%t, 4) + %offsetH;
	%min = getWord (%t, 5);
	%sec = getWord (%t, 6);

	%part = "AM";
	if (%hour > 24)
	{
		%hour -= 24;
		%day++;
	}
	if (%hour < 0)
	{
		%hour += 24;
		%day--;
	}
	%hour12 = %hour;
	if (%hour12 > 12)
	{
		%part = "PM";
		%hour12 -= 12;
	}
	switch$(%format)
	{
		case "HH:SS":
			if(%noAMPM)
				%time = %hour@":"@%min;
			else
				%time = %hour12@":"@%min SPC %part;
			return %time;
		case "timestamp":
			return %year@"_"@%month@"_"@%day@"_"@%hour@"_"@%min@"_"@%sec;

	}

}
//==============================================================================
// Empty Editor Gui
function getTimeCST()
{
	if ($DBuser_cstdiff $= "")
		$DBuser_cstdiff = 0;
	%time = localTime("",0,$DBuser_cstdiff);
	return %time;

}
//==============================================================================
// Empty Editor Gui
function getSQLTimeFromSeconds(%seconds)
{
	//No milliseconds, remove any decimal values
	%seconds = mRound(%seconds);

	%minutes = 0;
	%hours = 0;
	if (%seconds > 3600)
	{
		%hours = mFloor(%seconds / 3600);
		%seconds = %seconds - (%hours * 3600);
	}
	if (%seconds > 60)
	{
		%minutes = mFloor(%seconds / 60);
		%seconds = %seconds - (%minutes * 60);
	}

	%sqlTime = %hours @":"@%minutes@":"@%seconds;

	return %sqlTime;

}
//------------------------------------------------------------------------------
//==============================================================================
// Empty Editor Gui
function getSecondsFromSQLTime(%sqlTime)
{
	%sqlTime = strReplace(%sqlTime,":"," ");
	%hours = getWord(%sqlTime,0);
	%minutes = getWord(%sqlTime,1);
	%seconds = getWord(%sqlTime,2);

	%fullSeconds = %seconds + (%hours * 3600) + (%minutes * 60);

	return %fullSeconds;
}
//------------------------------------------------------------------------------
//==============================================================================
// Empty Editor Gui
function getDateTime()
{
	//Get Local Time in format =>weekday month monthday year hour min sec
	%t = getLocalTime();
	//Convert into SQL DATETIME =>YYYY-MM-DD HH:MM:SS
	%dateStamp = getWord (%t, 3)@"-"@getWord (%t, 1)@"-"@getWord (%t, 2)@" "@getWord (%t, 4)@"-"@getWord (%t, 5)@"-"@getWord (%t, 6);
	return %dateStamp;
}
function getMilliSecondsDisplay(%milliseconds,%style)
{
	%milliSecondOnly = %milliseconds % 1000;
	%seconds = getSecondsDisplay(mFloor(%milliseconds / 1000),%style);
	if (%style $= "1")
	{
		%fullTime = strReplace(%seconds,"s ","."@ %milliSecondOnly @"s");
	}
	else if (%style $= "2"|| %style $= "3"  )
	{
		%fullTime = strReplace(%seconds,"sec","."@ %milliSecondOnly @"sec");
	}
	else
		%fullTime = %seconds @ "."@ %milliSecondOnly;
	return %fullTime;
}
function getSecondsDisplay(%seconds,%style)
{
	%fields = getTimeFields(%seconds);
	%seconds = getField(%fields,0);
	%minutes = getField(%fields,1);
	%hours = getField(%fields,2);
	%days = getField(%fields,3);

	if (%style $= "1")
	{
		%time = %seconds @"s ";
		if (%minutes > 0)
			%time =%minutes @"m "@%time;

		if (%hours > 0)
			%time = %hours @"h "@%time;

		if (%days > 0)
			%time = %days @"d "@%time;

		return %time;
	}
	if (%style $= "2" || %style $= "3" )
	{
		%time = (%seconds == 1) ? %seconds @"sec ": %seconds @"secs ";
		if (%minutes > 0)
			%time = (%minutes == 1) ? %minutes @"min "@%time : %minutes @"mins "@%time ;

		if (%hours > 0)
			%time = (%hours == 1) ? %hours @"hour "@%time : %hours @"hours "@%time ;

		if (%days > 0)
			%time = (%days == 1) ? %days @"day "@%time : %days @"days "@%time ;

		if (%style $= "3")
		{
			%time = strReplaceList(%time,"min" TAB "minute" NL "sec" TAB "second");

		}
		return %time;
	}
	%time = %seconds;
	if (%minutes > 0)
		%time = %minutes @":"@%time;

	if (%hours > 0)
		%time = %hours @":"@%time;

	if (%days > 0)
		%time = (%days == 1) ? %days @"day "@%time : %days @"days "@%time ;

	return %time;
}
//==============================================================================
// Empty Editor Gui
function getTimeFields(%seconds,%useTextForTime)
{
	//No milliseconds, remove any decimal values
	%seconds = mRound(%seconds);
	%minutes = 0;
	%hours = 0;
	if (%seconds > 86400)
	{
		%days = mFloor(%seconds / 86400);
		%seconds = %seconds - (%days * 86400);
	}
	if (%seconds > 3600)
	{
		%hours = mFloor(%seconds / 3600);
		%seconds = %seconds - (%hours * 3600);
	}
	if (%seconds > 60)
	{
		%minutes = mFloor(%seconds / 60);
		%seconds = %seconds - (%minutes * 60);
	}
	if ( strlen(%seconds) == 1)
		%seconds = "0"@%seconds;

	if (strlen(%minutes) == 1)
		%minutes = "0"@%minutes;
	%time = %seconds;

	%timeFields = %seconds TAB %minutes TAB %hours TAB %days;
	return %timeFields;
	if (%minutes > 0)
		%time = %minutes@":"@%seconds;
	if (%hours > 0)
		%time = %hours @":"@%time;
	if (%days > 0)
		%time = %days@":"@%time;
	return %time;
}
//==============================================================================
// Empty Editor Gui
function convertSecondsToTime(%seconds,%useTextForTime)
{
	//No milliseconds, remove any decimal values
	%seconds = mRound(%seconds);

	%minutes = 0;
	%hours = 0;
	if (%seconds > 86400)
	{
		%days = mFloor(%seconds / 86400);
		%seconds = %seconds - (%days * 3600);
	}
	if (%seconds > 3600)
	{
		%hours = mFloor(%seconds / 3600);
		%seconds = %seconds - (%hours * 3600);
	}
	if (%seconds > 60)
	{
		%minutes = mFloor(%seconds / 60);
		%seconds = %seconds - (%minutes * 60);
	}

	if ( strlen(%seconds) == 1)
		%seconds = "0"@%seconds;

	if (strlen(%minutes) == 1)
		%minutes = "0"@%minutes;
	if (%useTextForTime $= "1")
	{
		%time = %seconds@"s";
		if (%minutes > 0)
			%time = %minutes@"m "@%time;
		if (%hours > 0)
			%time = %hours @"h "@%time;
		return %time;
	}

	if (%useTextForTime $= "2")
	{
		%time = %seconds@"sec";
		if (%minutes > 0)
			%time = %minutes@"min "@%time;
		if (%hours > 0)
			%time = %hours @"hour "@%time;
		return %time;
	}
	if (%useTextForTime $= "3")
	{
		if (%seconds > 1)
			%time = %seconds@" seconds";
		else  if (%seconds == 1)
			%time = %seconds@" second";

		if (%minutes > 1)
			%time = %minutes@" minutes "@%time;
		else if (%minutes == 1)
			%time = %minutes@" minute "@%time;

		if (%hours == 1)
			%time = %hours @" hour "@%time;
		else if (%hours > 1)
			%time = %hours @" hours "@%time;
		return %time;
	}
	if ( strlen(%seconds) == 1)
		%seconds = "0"@%seconds;

	if (strlen(%minutes) == 1)
		%minutes = "0"@%minutes;

	%time = %seconds;

	if (%minutes > 0)
		%time = %minutes@":"@%seconds;
	if (%hours > 0)
		%time = %hours @":"@%time;

	return %time;

}
//------------------------------------------------------------------------------

function millisecondsToMinutesSeconds(%time,%style)
{
	%te = %time / 1000;
	%te = %te / 60;
	%decimal = %te - mFloor(%te);
	%seconds = %decimal * 60;
	%minutes = %te - %decimal;
	%minutes = mCeil(%minutes);
	%seconds = mCeil(%seconds);

	if ( strlen(%seconds) == 1)
		%seconds = "0"@%seconds;

	//if (strlen(%minutes) == 1)
	//%minutes = "0"@%minutes;

	if (%style $= "1")
	{
		if (%minutes <= 0)
			return %seconds @ "s";

		return %minutes @ "m " @ %seconds @ "s";
	}
	if (%style $= "2")
	{
		if (%minutes <= 0)
			return %seconds @ "sec";

		return %minutes @ "min " @ %seconds @ "sec";
	}
	if (%style $= "3")
	{
		if (%minutes <= 0)
			return %seconds @ " seconds";
		if (%minutes == 1)
			return %minutes @ " minute " @ %seconds @ " seconds";

		return %minutes @ " minutes " @ %seconds @ " seconds";
	}

	return %minutes @ " minutes and " @ %seconds @ " seconds";
}
