//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function setAmbientGroupId( %id )
{

	if (!isObject(mgAmbientGroup))
	{
		return;
	}

	if (%id $= "")
		%id = 0;

	foreach(%group in mgAmbientGroup)
	{
		foreach(%obj in %group)
			%obj.hidden = "1";
	}

	%curGroup = mgAmbientGroup.getObject(%id);

	if (!isObject(%curGroup))
	{
		%curGroup = mgAmbientGroup.getObject(0);
		%id = "0";
	}

	foreach(%obj in %curGroup)
		%obj.hidden = "0";

	SEP_AmbientManager.curAmbientId = %id;
	%name = %curGroup.internalName;

	if (%name $= "")
		%name = "Unnamed ID " @ %id;

	$Mission_AmbientGroup = %name;
	$Mission_AmbientGroupId = %id;
}
//------------------------------------------------------------------------------
//==============================================================================
// Load the game datablocks
function checkMissionAmbientGroup()
{
	%ambientFile = strReplace($Server::MissionFile,".mis",".amb.cs");

	if (!isFile(%ambientFile))
	{
		warnLog("No custom ambient stored for:",fileBase($Server::MissionFile));
		return;
	}

	delObj(MissionAmbientGroup);
	exec(%ambientFile);
	$MissionAmbientGroups = "";
	$MissionAmbientGroupId = "";

	foreach(%ambGroup in MissionAmbientGroup)
	{
		%ambient = %ambGroup.internalName;
		$MissionAmbientGroups = strAddWord($MissionAmbientGroups,%ambGroup.getId(),true);
		$MissionAmbientGroupObjs[%ambient] = "";

		foreach(%obj in %ambGroup)
		{
			$MissionAmbientGroupObjs[%ambient] = strAddWord($MissionAmbientGroupObjs[%ambient],%obj.getId(),true);
			%type = %obj.internalName;
		}
	}

	foreach$(%ambGroup in $MissionAmbientGroups)
	{
		%ambient = %ambGroup.internalName;

		foreach$(%obj in $MissionAmbientGroupObjs[%ambient])
		{
		}
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Load the game datablocks
function setNextAmbientGroup()
{
	if ($MissionAmbientGroupId $= "")
		$MissionAmbientGroupId = 0;
	else
		$MissionAmbientGroupId++;

	setAmbientId($MissionAmbientGroupId);
}
//------------------------------------------------------------------------------
//==============================================================================
// Load the game datablocks
function setAmbientId(%id)
{
	$MissionAmbientGroupId = %id;

	if ($MissionAmbientGroupId >= getWordCount($MissionAmbientGroups) || $MissionAmbientGroupId $= "")
		$MissionAmbientGroupId = 0;

	%ambientGroup = getWord($MissionAmbientGroups,$MissionAmbientGroupId);

	foreach(%obj in %ambientGroup)
	{
		//foreach$(%obj in %subGroup){
		%type = %obj.internalName;
		%updated = false;

		foreach(%envObj in mgEnvironment)
		{
			if (%envObj.getClassName() $= %type && !%updated )
			{
				for(%i=0; %i<%obj.getDynamicFieldCount(); %i++)
				{
					%fieldData = %obj.getDynamicField(%i);
					%field = getField(%fieldData,0);
					%value = getField(%fieldData,1);
					%envObj.setFieldValue(%field,%value);
				}

				if (%type $= "ScatterSky")
					%envObj.applyChanges();

				%updated = true;
			}
		}

		//}
	}
}
