//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function getMissionObjects( %group)
{
	if (%group $= "")
		%group = MissionObjectsGroup;
	if (!isObject(%group))
		%group = new SimGroup(%group);
	else
		%group.clear();

	%i = 0;
	for(%file = findFirstFile($Server::MissionFileSpec); %file !$= ""; %file = findNextFile($Server::MissionFileSpec))
	{
		// Skip our new level/mission if we arent choosing a level
		// to launch in the editor.
		if ( !%this.launchInEditor )
		{
			if (strstr(%file, "newMission.mis") > -1)
				continue;
			if (strstr(%file, "newLevel.mis") > -1)
				continue;
		}
		if (!$stockLevels && strFind(%file,"levels/Stock/"))
			continue;
		%levelName = fileBase(%file);
		%levelDesc = "A Torque level";

		%misObj = getMissionInfo(%file);

		if (%misObj != 0)
		{
			if(%misObj.levelName !$= "")
				%levelName = %misObj.levelName;
			else if(%misObj.name !$= "")
				%levelName = %misObj.name;

			if (%misObj.desc0 !$= "")
				%levelDesc = %misObj.desc0;

			//%LevelInfoObject.delete();
		}
		//%misObj = new ScriptObject();
		%misObj.levelName = %levelName;
		%misObj.levelDesc = %levelDesc;
		%misObj.file = %file;
		%misObj.preview = filePath(%file) @ "/" @ fileBase(%file) @ "_preview";

		%group.add(%misObj);
	}
}
function getMissionInfo( %missionFile )
{
	%file = new FileObject();

	%LevelInfoObject = "";

	if ( %file.openForRead( %missionFile ) )
	{
		%inInfoBlock = false;

		while ( !%file.isEOF() )
		{
			%line = %file.readLine();
			%line = trim( %line );

			if( %line $= "new LevelInfo(theLevelInfo) {" )
			{
				%line = "new ScriptObject() {";
				%inInfoBlock = true;
			}

			else if( %inInfoBlock && %line $= "};" )
			{
				%inInfoBlock = false;
				%LevelInfoObject = %LevelInfoObject @ %line;
				break;
			}

			if( %inInfoBlock )
				%LevelInfoObject = %LevelInfoObject @ %line @ " ";
		}

		%file.close();
	}
	%file.delete();

	if( %LevelInfoObject !$= "" )
	{
		//%LevelInfoObject = "%LevelInfoObject = " @ %LevelInfoObject;
		%evalObj = %LevelInfoObject;
		eval("%misObj = "@ %evalObj );

		return %misObj;
	}

	// Didn't find our LevelInfo
	return 0;
}
