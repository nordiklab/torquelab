//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Update the GuiControl data field depending of the Class
//==============================================================================
//==============================================================================
// getObjectClassListFromGroup(RootGroup,"ActionMap").getMissionGroundCover();
function getObjectClassListFromGroup( %group,%class )
{
   
	%list = checkGroupForClass(%group,%class);
	return %id;
}
//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
function checkGroupForClass(%group,%classes,%firstonly )
{
	foreach(%obj in %group)
	{
		if (strFind(%classes,%obj.getClassname()))
		{			
         %list = strAddWord(%list,%obj.getId());
         if (%firstonly)
            return %obj.getId();
			
		}
		else if (%obj.getClassName()$= "SimGroup")//%obj.getClassname() $= "SimGroup")
		{
			%listAdd = checkGroupForClass(%obj,%classes,%firstonly);
			%list = strAddWord(%list,%listAdd);
		}
	}

	return %list;
}
//------------------------------------------------------------------------------
function getFirstMissionObjectClass( %class )
{
	%id = findFirstObjectOfClassInGroup(MissionGroup,%class);
	return %id;
}
//------------------------------------------------------------------------------
function findFirstObjectOfClassInGroup( %searchGroup, %className )
{
	foreach(%obj in %searchGroup)
	{
		if( %obj.getClassName() $= %className )
			return %obj;

		if( %obj.getClassName() $= "SimGroup" )
		{
			%result = findFirstObjectOfClassInGroup(%obj,%className);
			if(isObject(%result))
				return %result;
		}

	}

	return "";
}
//==============================================================================
// SEP_GroundCover.getMissionGroundCover();
function getMissionObjectClassList( %classes,%condition )
{
	%list = checkMissionSimGroupForClass(MissionGroup,%classes,%condition);
	return %list;
}
//------------------------------------------------------------------------------
//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
function checkMissionSimGroupForClass(%group,%classes,%condition,%firstonly )
{
	foreach(%obj in %group)
	{
		if (strFind(%classes,%obj.getClassname()))
		{
			if (%condition !$="")
			{
				%rejected = "";
				%condField = getWord(%condition,0);
				%condValue = trim(removeWord(%condition,0));
				%objValue = %obj.getFieldValue(%condField);

				if (%condValue $="" && %objValue !$= "")
					%rejected = "Obj value is not empty";
				else if (%condValue $= "NotEmpty" && %objValue $= "")
					%rejected = "Obj Value is empty";
				else if (%condValue $="isObject" && !isObject(%objValue ))
					%rejected = "ObjValue is not an object";
				else if (%condValue !$=%objValue)
					%rejected = "ObjValue don't match the condition";
			}

			if (%rejected $= "")
			{
				%list = strAddWord(%list,%obj.getId());
				if (%firstonly)
					return %obj.getId();
			}
		}
		else if (%obj.getCount() > 0 )//%obj.getClassname() $= "SimGroup")
		{
			%listAdd = checkMissionSimGroupForClass(%obj,%classes,%condition,%firstonly);
			%list = strAddWord(%list,%listAdd);
		}
	}

	return %list;
}
//------------------------------------------------------------------------------
//==============================================================================
// SEP_GroundCover.getMissionGroundCover();
function getAllMissionObjectClass( %classes,%condition )
{
	%list = getAllMissionObjectClassFromGroup(MissionGroup,%classes,%condition);
	return %list;
}
//------------------------------------------------------------------------------
function getAllMissionObjectClassFromGroup(%group,%classes,%condition )
{
	foreach(%obj in %group)
	{
		if (%obj.getClassname() $= "SimGroup")
		{
			%listAdd = checkMissionSimGroupForClass(%obj,%classes,%condition);
			%list = strAddWord(%list,%listAdd);
			continue;
		}
		if(!%obj.isMemberOfClass(%classes))
			continue;
		eval("%good = %obj."@%condition@";");
		if (%good)
			%list = strAddWord(%list,%obj.getId());

	}
	return %list;
}
//==============================================================================
// Update the GuiControl data field depending of the Class
//==============================================================================
//----------------------------------------------------------------------------
// A function used in order to easily parse the MissionGroup for classes . I'm pretty
// sure at this point the function can be easily modified to search the any group as well.
function parseMissionGroup( %className, %childGroup )
{
	if( getWordCount( %childGroup ) == 0)
		%currentGroup = "MissionGroup";
	else
		%currentGroup = %childGroup;

	for(%i = 0; %i < (%currentGroup).getCount(); %i++)
	{
		if( (%currentGroup).getObject(%i).getClassName() $= %className )
			return true;

		if( (%currentGroup).getObject(%i).getClassName() $= "SimGroup" )
		{
			if( parseMissionGroup( %className, (%currentGroup).getObject(%i).getId() ) )
				return true;
		}
	}
}

// A variation of the above used to grab ids from the mission group based on classnames
function parseMissionGroupForIds( %className, %childGroup,%firstOnly )
{
	if( getWordCount( %childGroup ) == 0)
		%currentGroup = "MissionGroup";
	else
		%currentGroup = %childGroup;

	for(%i = 0; %i < (%currentGroup).getCount(); %i++)
	{
		if( (%currentGroup).getObject(%i).getClassName() $= %className )
			%classIds = %classIds @ (%currentGroup).getObject(%i).getId() @ " ";

		if( (%currentGroup).getObject(%i).getClassName() $= "SimGroup" )
			%classIds = %classIds @ parseMissionGroupForIds( %className, (%currentGroup).getObject(%i).getId());
	}

	return %classIds;
}
//==============================================================================
// Random helpers to help with fixing corrupted MissionGroup
//==============================================================================
function clearMissionClass( %className )
{
	$List = getMissionObjectClassList("Px3Shape");
	foreach$(%obj in $List)
		delObj(%obj);
}
function moveMissionClass( %className,%tmpGroup )
{
	if (%tmpGroup $= "")
		%tmpGroup = newSimGroup(MissionGroupTmp);

	$List = getMissionObjectClassList("Px3Shape");
	foreach$(%obj in $List)
		%tmpGroup.add(%obj);

	MissionGroupTmp.save("tmpGroup.mis");
}
function addFileToMission( %file,%subGroup )
{
	exec(%file);
	MissionGroup.add(%newGroup);
}
function addRootRange( %range,%target )
{
	%tmpGroup = newSimGroup(MissionGroupTmp);
	for(%i=%range.y; %i>=%range.x; %i--)
	{
		%obj = RootGroup.getObject(%i);
		%tmpGroup.add(%obj);
	}

}
