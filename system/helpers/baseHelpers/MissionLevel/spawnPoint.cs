//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Update the GuiControl data field depending of the Class
//==============================================================================
//==============================================================================
// Last resort spawnpoint picking - Should be removed for released games
function pickPlayerSpawnPoint(%client)
{
	if( isObject(%client.spawnPoint) )
	{
		%playerSpawnPoint = %client.spawnPoint;
		%client.spawnPoint = "";
		return %playerSpawnPoint;
	}

	if( isObject(spawnsphere001) )
		return spawnsphere001;
		
   if (%client.spawnGroup $= "")
      %spawnGroup = PlayerDropPoints;
   else
      %spawnGroup = %client.spawnGroup;
      
   %spawnPoint =  pickSpawnPointInGroup(%spawnGroup );
   if (isObject(%spawnPoint))     
      return %spawnPoint;   
         
   info("Failed to pick a spawn for group:",%client.spawnGroup ,"Returning any spawn");
  
	//This should never happen, find a random SpawnSphere for mission list
	%spawnSpheres = getMissionObjectClassList("SpawnSphere");
	%random = getRandom(0,getWordCount(%spawnSpheres)-1);
	%spawnPoint = getWord(%spawnSpheres,%random);

	if (isObject(%spawnPoint))
	{
		warnLog("All spawnpoint picking method failed, picked a random spawn from the mission. This should NEVER happen on an official game. Please fix this ASAP");
		return %spawnPoint;
	}

	%msg = "There's no SpawnSphere in mission, can't spawn a player for this mission. Make sure the spawnpoints configuration is correct to be able to spawn in the mission";
	MessageBoxOk("No spawnpoint found",%msg);
	return "";
}
function pickSpawnPoint(%group)
{
   if (%group !$= "")
   {
     %spawnPoint =  pickSpawnPointInGroup(%group);
     if (isObject(%spawnPoint))     
      return %spawnPoint;
      
      info("Failed to pick a spawn for group:",%group,"Returning any spawn");
   }
     
   //If bo valid spawnpoint at this stage, get any spawn in level
   %spawnSpheres = getMissionObjectClassList("SpawnSphere");
   %random = getRandom(0,getWordCount(%spawnSpheres)-1);
   %spawnPoint = getWord(%spawnSpheres,%random);
  
   return %spawnPoint;
}
//------------------------------------------------------------------------------
function pickSpawnPointInGroup(%spawnGroups,%createIfMissing)
{
   
	// Walk through the groups until we find a valid object
	for (%i = 0; %i < getWordCount(%spawnGroups); %i++)
	{
		%group = getWord(%spawnGroups, %i);

      if (!isObject(%group))
         %group = MissionGroup.findObjectByInternalName(%group,false);
         
		if (isObject(%group))
		{
		    %spawnPoint = %group.getRandom();
		   if (isObject(%spawnPoint))
			   return %spawnPoint;
		}
		   	   
	}

   if (!%createIfMissing)
      return false;
	// Didn't find a spawn point by looking for the groups
	// so let's return the "default" SpawnSphere
	// First create it if it doesn't already exist
	if (!isObject(DefaultPlayerSpawnSphere))
	{
		%spawn = new SpawnSphere(DefaultPlayerSpawnSphere)
		{
			dataBlock      = "SpawnSphereMarker";
			spawnClass     = $Game::DefaultPlayerClass;
			spawnDatablock = $Game::DefaultPlayerDataBlock;
		};

		// Add it to the MissionCleanup group so that it
		// doesn't get saved to the Mission (and gets cleaned
		// up of course)
		MissionCleanup.add(%spawn);
	}

	return DefaultPlayerSpawnSphere;
}
//==============================================================================
// Last resort spawnpoint picking - Should be removed for released games
function getClientSpawnPosition(%client,%rebuild)
{
	$GameSpawnGroups = getMissionObjectClassList("SimGroup","superClass SpawnGroup");

	foreach$(%group in $GameSpawnGroups)
	{
		%spawnObj = %group.getRandom();
		if (isObject(%spawnObj))
			return %spawnObj;
	}

	if (isObject(PlayerDropPoints))
		%spawnObj = PlayerDropPoints.getRandom();
	if (isObject(%spawnObj))
		return %spawnObj;

	%spawn = new SpawnSphere(DefaultPlayerSpawnSphere)
	{
		dataBlock      = "SpawnSphereMarker";
		spawnClass     = $Game::DefaultPlayerClass;
		spawnDatablock = $Game::DefaultPlayerDataBlock;
	};

	// Add it to the MissionCleanup group so that it
	// doesn't get saved to the Mission (and gets cleaned
	// up of course)
	MissionCleanup.add(%spawn);

	return DefaultPlayerSpawnSphere;
}
//------------------------------------------------------------------------------

//==============================================================================
// Pick a random point inside the SpawnSPhere
function pickPointInSpawnSphere(%objectToSpawn, %spawnSphere)
{
	%SpawnLocationFound = false;
	%attemptsToSpawn = 0;
	while(!%SpawnLocationFound && (%attemptsToSpawn < 5))
	{
		%sphereLocation = %spawnSphere.getTransform();
		// Attempt to spawn the player within the bounds of the spawnsphere.
		%angleY = mDegToRad(getRandom(0, 100) * m2Pi());
		%angleXZ = mDegToRad(getRandom(0, 100) * m2Pi());
		%sphereLocation = setWord( %sphereLocation, 0, getWord(%sphereLocation, 0) + (mCos(%angleY) * mSin(%angleXZ) * getRandom(-%spawnSphere.radius, %spawnSphere.radius)));

		//Z Should not be changed unless maybe a dynamicField used like zFactor
		if (%spawnSphere.zFactor !$= "")
		{
			%offsetZ = (mCos(%angleXZ) * getRandom(-%spawnSphere.radius, %spawnSphere.radius)) * %spawnSphere.zFactor;
			%sphereLocation = setWord( %sphereLocation, 1, getWord(%sphereLocation, 1) + %offsetZ);

		}
		%SpawnLocationFound = true;
		// Now have to check that another object doesn't already exist at this spot.
		// Use the bounding box of the object to check if where we are about to spawn in is
		// clear.
		%boundingBoxSize = %objectToSpawn.getDatablock().boundingBox;
		%searchRadius = getWord(%boundingBoxSize, 0);
		%boxSizeY = getWord(%boundingBoxSize, 1);
		// Use the larger dimention as the radius to search
		if (%boxSizeY > %searchRadius)
			%searchRadius = %boxSizeY;
		// Search a radius about the area we're about to spawn for players.
		initContainerRadiusSearch( %sphereLocation, %searchRadius, $TypeMasks::PlayerObjectType );
		while ( (%objectNearExit = containerSearchNext()) != 0 )
		{
			// If any player is found within this radius, mark that we need to look
			// for another spot.
			%SpawnLocationFound = false;
			break;
		}
		// If the attempt at finding a clear spawn location failed
		// try no more than 5 times.
		%attemptsToSpawn++;
	}
	// If we couldn't find a spawn location after 5 tries, spawn the object
	// At the center of the sphere and give a warning.
	if (!%SpawnLocationFound)
	{
		%sphereLocation = %spawnSphere.getTransform();
		warn("WARNING: Could not spawn player after" SPC %attemptsToSpawn
		     SPC "tries in spawnsphere" SPC %spawnSphere SPC "without overlapping another player. Attempting spawn in center of sphere.");
	}
	return %sphereLocation;
}
//------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Torque3D Default Pick SPawnPoint Function
//-----------------------------------------------------------------------------
// pickPlayerSpawnPoint() is responsible for finding a valid spawn point for a
// player.
//-----------------------------------------------------------------------------

