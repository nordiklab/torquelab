//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
/*
 LabMenu.addMenu("MyFirstMenu",0);
   LabMenu.addMenuItem(0,"MyFirstItem",0,"Ctrl numpad0",-1);
   LabMenu.addSubmenuItem("MyFirstMenu","MyFirstItem","MyFirstSubItem",0,"",-1);
   */

//==============================================================================
function Lab::resetPlayGui(%this)
{

	Editor.open(true);
	LocalClientConnection.camera.setTransform(LocalClientConnection.camera.editorTransform);

}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::setHudlessPlayGui(%this)
{
	%controlObject = LocalClientConnection.getControlObject();
	LocalClientConnection.camera.editorTransform = LocalClientConnection.camera.getTransform();
	Canvas.setContent(HudlessPlayGui);
	LocalClientConnection.setControlObject(%controlObject);

}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::setCurrentViewAsPreview(%this)
{

	%this.setHudlessPlayGui();
	%name = expandFileName(filePath(MissionGroup.getFilename())@"/"@fileBase(MissionGroup.getFilename())@ "_preview");
	takeScreenShot(%name);
	%this.schedule(500,"resetPlayGui");
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::setNextScreenShotPreview(%this)
{
	$LabNextScreenshotIsPreview = true;
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::takeScreenShot(%this,%hideEditor)
{

	if ($LabNextScreenshotIsPreview || (%hideEditor && !isImageFile(strReplace(MissionGroup.getFilename(),".mis","_preview"))))
	{
		Lab.setCurrentViewAsPreview();
		$LabNextScreenshotIsPreview = false;
		return;
	}

	if (%hideEditor)
	{
		%this.setHudlessPlayGui();
		takeScreenShot();
		%this.schedule(500,"resetPlayGui",true);
		return;
	}

	takeScreenShot();
}
//------------------------------------------------------------------------------

//==============================================================================
// SEP_GroundCover.getMissionGroundCover();
function Lab::getMissionObjectClassList(%this,%class)
{
	if (!isObject(MissionGroup))
		return "-1";

	%this.missionObjectClassList = "";
	%this.checkMissionSimGroupForClass(MissionGroup,%class);
	%list = %this.missionObjectClassList;
	%this.missionObjectClassList = "";
	return %list;
}
//------------------------------------------------------------------------------
//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
function Lab::checkMissionSimGroupForClass(%this,%group,%class)
{
	foreach(%obj in %group)
	{
		if (%obj.getClassname() $= %class)
		{
			%this.missionObjectClassList = strAddWord(%this.missionObjectClassList,%obj.getId());
		}
		else if (%obj.getClassname() $= "SimGroup")
		{
			%this.checkMissionSimGroupForClass(%obj,%class);
		}
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// SEP_GroundCover.getMissionGroundCover();
function Lab::getDatablockClassList(%this,%class,%fieldValidators)
{
	%dataList = "";

	foreach(%obj in DataBlockSet)
	{
		if (%obj.getClassname() !$= %class)
			continue;

		if (%fieldValidators !$= "")
		{
			%skipThis = false;
			%records = strReplace(%fieldValidators,";;","\n");
			%count = getRecordCount(%records);

			for(%i = 0; %i<%count; %i++)
			{
				%fieldsData = getRecord(%fieldValidators,%i);
				%fields = strReplace(%fieldsData,">>","\t");
				%field = getField(%fields,0);
				%value = getField(%fields,1);
				%thisValue = %obj.getFieldValue(%field);

				if (%thisValue !$= %value)
				{
					%skipThis = true;
					continue;
				}
			}
		}

		if (%skipThis)
			continue;

		%dataList = strAddWord(%dataList,%obj.getId());
	}

	return %dataList;
}
//------------------------------------------------------------------------------
//==============================================================================
// SEP_GroundCover.getMissionGroundCover();
function getScatterSkies()
{
// Check for scattersky object already in the level.  If there is one,
	// warn the user.
	initContainerTypeSearch($TypeMasks::EnvironmentObjectType);

	while(1)
	{
		%object = containerSearchNext();

		if (!%object)
			break;

		if (%object.isMemberOfClass("ScatterSky"))
		{
			%ids = strAddWord(%ids,%object.getId());
		}
	}

	return %ids;
}
