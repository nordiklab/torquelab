//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function EditorMenuEditDelete()
{

	if ($InGuiEditor)
	{
		GuiEditorDeleteSelection(1);
	}
	else if (isObject(Lab.currentEditor))
	{
		if (Lab.currentEditor.isMethod(handleDelete))
			Lab.currentEditor.handleDelete();
		else
			warnLog("EditorMenuEditDelete missing in current editor",Lab.currentEditor.plugin);
	}
	else
		warnLog("EditorMenuEditDelete no active editor",Lab.currentEditor);
}

function EditorMenuEditDeselect()
{
	if (isObject(Lab.currentEditor))
	{
		if (Lab.currentEditor.isMethod(handleDeselect))
			Lab.currentEditor.handleDeselect();
		else
			warnLog("EditorMenuEditDeselect missing in current editor",Lab.currentEditor.plugin);
	}
	else
		warnLog("EditorMenuEditDeselect no active editor",Lab.currentEditor);
}

function EditorMenuEditCut()
{

	if (isObject(Lab.currentEditor))
	{
		if (Lab.currentEditor.isMethod(handleCut))
			Lab.currentEditor.handleCut();
		else
			warnLog("EditorMenuEditCut missing in current editor",Lab.currentEditor.plugin);
	}
	else
		warnLog("EditorMenuEditCut no active editor",Lab.currentEditor);
}

function EditorMenuEditCopy()
{
  warnLog("EditorMenuEditCopy  current editor",Lab.currentEditor.plugin);
	if (isObject(Lab.currentEditor))
	{
		if (Lab.currentEditor.isMethod(handleCopy))
			Lab.currentEditor.handleCopy();
		else
			warnLog("EditorMenuEditCopy missing in current editor",Lab.currentEditor.plugin);
	}
	else
		warnLog("EditorMenuEditCopy no active editor",Lab.currentEditor);
}

function EditorMenuEditPaste()
{
	if (isObject(Lab.currentEditor))
	{
		if (Lab.currentEditor.isMethod(handlePaste))
			Lab.currentEditor.handlePaste();
		else
			warnLog("EditorMenuEditPaste missing in current editor",Lab.currentEditor.plugin);
	}
	else
		warnLog("EditorMenuEditPaste no active editor",Lab.currentEditor);

}
