//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$Cfg_Phabricator_Domain = "http://TorqueLab.test";
$Cfg_Phabricator_Path = "/conduit/";

$Cfg_Phabricator_QueryKey["Tasks"] = "_HkF7smCLLDh";
$Cfg_Phabricator_QueryKey["Features"] = "hJ0B0SCmeTuI";
$Cfg_Phabricator_QueryKey["Issues"] = "5Kkf38WEw7yz";

$Cfg_Phabricator_QueryKey["Commits"] = "1oAlDLZQo0XF";

//==============================================================================
// Handle the main ConduitAPI querying
//==============================================================================

//==============================================================================
// QueryKey - Queries saved by NordikLab admin for simple request
// WqpawILvSBIA -> All tasks in TorqueLab of TASK type only
function Phabricator::queryManiphest(%this,%itemType)
{
   if (%itemType $= "")
      return;
   export("$Phabricator_"@%itemType@"*","tlab/system/helpers/Phabricator/archives/"@%itemType@"_"@getSimTime()@".txt");
   deleteVariables("$Phabricator_"@%itemType@"*");    

   %queryKey = $Cfg_Phabricator_QueryKey[%itemType];
   if (%queryKey !$= "")
      %postData = "queryKey="@%queryKey;    
   
   PhabWeb.itemType = %itemType;
   PhabWeb.queryType = "Maniphest";
   
	PhabWeb.post($Cfg_Phabricator_Domain, $Cfg_Phabricator_Path@"queryManiphest.php", "", %postData);
   return;
}

function Phabricator::onQueryCompleted(%this,%print)
{   
      foreach$(%type in Phabricator.tasksTypes)
         Phabricator.tasksOfType[%type] = ""; 
      
   for(%i = 1; %i <= $Phabricator_[PhabWeb.itemType,"count"];%i++)
   {
        %id =  $Phabricator_[PhabWeb.itemType,%i,"id"];
        %type =  $Phabricator_[PhabWeb.itemType,%i,"type"];        
        %title =  $Phabricator_[PhabWeb.itemType,%i,"name"];
        %description =  $Phabricator_[PhabWeb.itemType,%i,"description"];
        %status =  $Phabricator_[PhabWeb.itemType,%i,"status"];
        Phabricator.tasksTypes = strAddWord(Phabricator.tasksTypes ,%type,1); 
        Phabricator.tasksOfType[%type] = strAddWord(Phabricator.tasksOfType[%type],%id,1); 
        if (!PhabWeb.printInfo)
         continue;
        info("----------------------------------------");
        info("Task Title:",%title,"Status",%status);
        info("Description",%description);      
   }
}
function PhabWeb::onDataLine(%this, %field,%value)
{   
   if (%field $= "id")			      
		$Phabricator_[PhabWeb.itemType,"count"]++;		

	$Phabricator_[PhabWeb.itemType,$Phabricator_[PhabWeb.itemType,"count"],%field] = %value;	
}

function PhabWeb::onDisconnect(%this)
{   
   Phabricator.onQueryCompleted();
}
/*
 Builtin and Saved Queries
You can choose a builtin or saved query as a starting point for filtering results by selecting it with queryKey. If you don't specify a queryKey, the query will start with no constraints.

For example, many applications have builtin queries like "active" or "open" to find only active or enabled results. To use a queryKey, specify it like this:

Selecting a Builtin Query
{
  ...
  "queryKey": "active",
  ...
}
The table below shows the keys to use to select builtin queries and your saved queries, but you can also use any query you run via the web UI as a starting point. You can find the key for a query by examining the URI after running a normal search.

You can use these keys to select builtin queries and your configured saved queries:

Query Key	Name	Builtin
pmOnv3yp14Nm	TorqueLab	Custom
yOtQrU_etyW0	The Return Plan	Custom
assigned	Assigned	Builtin
authored	Authored	Builtin
subscribed	Subscribed	Builtin
open	Open Tasks	Builtin
all	All Tasks

Custom Query Constraints
You can apply custom constraints by passing a dictionary in constraints. This will let you search for specific sets of results (for example, you may want show only results with a certain state, status, or owner).

If you specify both a queryKey and constraints, the builtin or saved query will be applied first as a starting point, then any additional values in constraints will be applied, overwriting the defaults from the original query.

Specify constraints like this:

Example Custom Constraints
{
  ...
  "constraints": {
    "authors": ["PHID-USER-1111", "PHID-USER-2222"],
    "statuses": ["open", "closed"],
    ...
  },
  ...
}

This API endpoint supports these constraints:

Key	Label	Type	Description
ids	IDs	list<int>	Search for objects with specific IDs.
phids	PHIDs	list<phid>	Search for objects with specific PHIDs.
assigned	Assigned To	list<user>	Search for tasks owned by a user from a list.
authorPHIDs	Authors	list<user>	Search for tasks with given authors.
statuses	Statuses	list<string>	Search for tasks with given statuses.
priorities	Priorities	list<int>	Search for tasks with given priorities.
subtypes	Subtypes	list<string>	Search for tasks with given subtypes.
hasParents	Open Parents	bool
hasSubtasks	Open Subtasks	bool
parentIDs	Parent IDs	list<int>
subtaskIDs	Subtask IDs	list<int>
group	Group By		Not supported.
createdStart	Created After	epoch
createdEnd	Created Before	epoch
modifiedStart	Updated After	epoch
modifiedEnd	Updated Before	epoch
closedStart	Closed After	epoch
closedEnd	Closed Before	epoch
closerPHIDs	Closed By	list<user>	Search for tasks closed by certain users.
query	Query	string	Fulltext search.
subscribers	Subscribers	list<user>	Search for objects with certain subscribers.
projects	Tags	list<project>	Search for objects tagged with given projects.

Choosing a Result Order
{
  ...
  "order": "newest",
  ...
}
These builtin orders are available:

Key	Description	Columns
priority	Priority	priority, subpriority, id
updated	Date Updated (Latest First)	updated, id
outdated	Date Updated (Oldest First)	-updated, -id
newest	Creation (Newest First)	id
oldest	Creation (Oldest First)	-id
closed	Date Closed (Latest First)	closed, id
title	Title	title, id
relevance	Relevance	rank, fulltext-modified, id

You can choose a low-level column order instead. To do this, provide a list of columns instead of a single key. This is an advanced feature.

In a custom column order:

each column may only be specified once;
each column may be prefixed with - to invert the order;
the last column must be a unique column, usually id; and
no column other than the last may be unique.
To use a low-level order, choose a sequence of columns and specify them like this:

Using a Custom Order
{
  ...
  "order": ["color", "-name", "id"],
  ...
}
These low-level columns are available:

Key	Unique
id	Yes
rank	No
fulltext-created	No
fulltext-modified	No
priority	No
owner	No
status	No
project	No
title	No
subpriority	No
updated	No
closed	No

These are the fields available on this object type:

Key	Type	Description
title	string	The title of the task.
description	remarkup	The task description.
authorPHID	phid	Original task author.
ownerPHID	phid?	Current task owner, if task is assigned.
status	map<string, wild>	Information about task status.
priority	map<string, wild>	Information about task priority.
points	points	Point value of the task.
subtype	string	Subtype of the task.
closerPHID	phid?	User who closed the task, if the task is closed.
dateClosed	int?	Epoch timestamp when the task was closed.
spacePHID	phid?	PHID of the policy space this object is part of.
dateCreated	int	Epoch timestamp when the object was created.
dateModified	int	Epoch timestamp when the object was last updated.
policy	map<string, wild>	Map of capabilities to current policies.
*/
