//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$Cfg_Phabricator_Domain = "http://TorqueLab.test";
$Cfg_Phabricator_Path = "/conduit/";
$Cfg_Phabricator_PrintInfo = false;
$Cfg_Phabricator_EchoLines = false;

//-----------------------------------------------------------------------------
function initPhabricatorAPI()
{
	Lab.phabricator = new SimSet(Phabricator);

	// Create the TCPObject
	new HTTPObject(PhabWeb);
	PhabWeb.doDataline = true;
	PhabWeb.printInfo = $Cfg_Phabricator_PrintInfo;
	PhabWeb.echo = $Cfg_Phabricator_EchoLines;

}
function Phabricator::getTasks(%this)
{
	%this.queryManiphest( "Tasks");
}
function Phabricator::getIssues(%this)
{
	%this.queryManiphest( "Issues");
}
function Phabricator::getFeatures(%this)
{
	%this.queryManiphest( "Features");
}

function Phabricator::dumpAllData(%this,%print)
{

	foreach$( %type in "Tasks Issues Features")
	{
		for(%i = 1; %i <= $Phabricator_[%type,"count"]; %i++)
		{
			%id =  $Phabricator_[%type,%i,"id"];
			%type =  $Phabricator_[%type,%i,"type"];
			%title =  $Phabricator_[%type,%i,"name"];
			%description =  $Phabricator_[%type,%i,"description"];
			%status =  $Phabricator_[%type,%i,"status"];

			info("----------------------------------------");
			info(%i,"-",%type,"Title:",%title,"Status",%status);
			info("Description",%description);
		}
	}
}
