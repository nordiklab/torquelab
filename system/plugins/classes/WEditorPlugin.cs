//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
new ScriptObject(WEditorPlugin)
{
	superClass = "EditorPlugin"; //Default to EditorPlugin class
	editorGui = EWorldEditor; //Default to EWorldEditor
	isHidden = true;
};

function WEditorPlugin::onActivated(%this)
{
	EditorGui.bringToFront(EWorldEditor);
	EWorldEditor.setVisible(true);

	EWorldEditor.makeFirstResponder(true);
	SceneEditorTree.open(MissionGroup,true);
	EWorldEditor.syncGui();
	EditorGuiStatusBar.setSelectionObjectsByCount(EWorldEditor.getSelectionSize());

	// Should the Transform Selection window open?
	if (EWorldEditor.ETransToolDisplayed)
	{
		ETransTool.setVisible(true);
	}

	Parent::onActivated(%this);
}

function WEditorPlugin::onDeactivated(%this)
{
	// Hide the Transform Selection window from other editors
	//ETransTool.setVisible(false);
	EWorldEditor.setVisible(false);

	//Not what we want here, should be deactivate plugin but activating one will deactivate other menu editors
	//WEMenuEditors.disablePlugin(%this);
	Parent::onDeactivated(%this);
}

