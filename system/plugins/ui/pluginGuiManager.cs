//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$LabPlugin::ToolbarPos = "306 0";

//==============================================================================
// Activate the interface for a plugin
//==============================================================================

//==============================================================================
//Reinitialize all plugin data
function Lab::togglePluginParamUI(%this)
{
	if (LabParamsDlg.isAwake())
	{
		popDlg(LabParamsDlg);
		return;
	}

	pushDlg(LabParamsDlg);
	%currentPlugin = Lab.currentEditor.plugin;
	%id = LabParamsTree.findItemByValue("Plugins_"@%currentPlugin);

	if (%id <= 0)
		return;

	LabParamsTree.clearSelection();
	LabParamsTree.selectItem(%id);
}
//------------------------------------------------------------------------------

//==============================================================================
// Manage the GUI for the plugins
//==============================================================================
function Lab::addGuiToPluginSet(%this,%plugin,%gui,%type)
{
	%pluginSimSet = %plugin@"_GuiSet";

	if (!isObject(%pluginSimSet))
		%pluginSimSet = newSimSet(%pluginSimSet,LabGuiTypesGroup);

	%this.addGui(%gui,%type);
	%gui.plugin = %plugin;
	%pluginSimSet.add(%gui);
	LabPluginGuiSet.add(%gui);
}
function Lab::addPluginEditor(%this,%plugin,%gui,%notFullscreen)
{
	%pluginObj = %plugin@"Plugin";
	%pluginObj.editorGui = %gui;

	%type = (%notFullscreen) ? "ExtraGui" : "EditorGui";
	%this.addGuiToPluginSet(%plugin,%gui,%type);
}
//------------------------------------------------------------------------------
//==============================================================================
// Plugin Tools (Right Column UI)
function Lab::addPluginGui(%this,%plugin,%gui)
{
	%this.addGuiToPluginSet(%plugin,%gui,"Tool");
	%pluginObj = %plugin@"Plugin";
	%pluginObj.useTools = true;
	%pluginObj.toolsGui = %gui;
	%gui.superClass = "FrameSetPluginTools";

}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::addPluginToolbar(%this,%plugin,%gui)
{
	%gui.plugin = %plugin;
	%this.addGuiToPluginSet(%plugin,%gui,"Toolbar");

}
function Lab::addPluginDlg(%this,%plugin,%gui)
{
	%pluginObj = %plugin@"Plugin";
	%pluginObj.dialogs = %gui;
	%this.addGuiToPluginSet(%plugin,%gui,"Dialog");
	%gui.pluginObj = %pluginObj;
	%gui.superClass = "PluginDlg";

	%gui.isDlg = true;
}
//==============================================================================
// Plugin Tools (Right Column UI)

//------------------------------------------------------------------------------
function Lab::addPluginPalette(%this,%plugin,%gui)
{
	if (!isObject(%gui))
	{
		warnLog("Sent an invalid Palette gui for add:",""@%gui,"Plugin:",%plugin);
		return;
	}

	if (%gui.getClassName() !$= "GuiStackControl")
	{
	   if (%gui.getCount() <= 0)
	      return;
		if (%gui.getObject(0).getClassName() $= "GuiStackControl")
		{
			%name = %gui.getName()@"Stack";
			%gui =  %gui.getObject(0);
			%gui.setName(%name);
		}
	}

	%gui.internalName = %plugin;
	Lab.pluginHavePalette[%plugin] = true;
	%this.addGuiToPluginSet(%plugin,%gui,"Palette");

}

//==============================================================================
// Editor Main menu functions
//==============================================================================

//------------------------------------------------------------------------------

//==============================================================================
// Manage the GUI for the plugins
//==============================================================================

function EditorPlugin::addEditor(%this,%gui,%notFullscreen)
{
	Lab.addPluginEditor(%this.moduleId,%gui,%notFullscreen);
}
//------------------------------------------------------------------------------
//==============================================================================
// Plugin Tools (Right Column UI)
function EditorPlugin::addGui(%this,%gui)
{
	Lab.addPluginGui(%this.moduleId,%gui);
}
//------------------------------------------------------------------------------

//==============================================================================
function EditorPlugin::addToolbar(%this,%gui)
{
Lab.addPluginToolbar(%this.moduleId,%gui);

}
function EditorPlugin::addDlg(%this,%gui)
{
	Lab.addPluginDlg(%this.moduleId,%gui);
}
//==============================================================================
// Plugin Tools (Right Column UI)

//------------------------------------------------------------------------------
function EditorPlugin::addPalette(%this,%gui)
{
	Lab.addPluginPalette(%this.moduleId,%gui);

}

//==============================================================================
function Lab::updatePluginsTools(%this)
{
	foreach(%tools in LabToolGuiSet)
	{
		EditorGui-->ToolsContainer.add(%tools);
	}
}
//------------------------------------------------------------------------------
