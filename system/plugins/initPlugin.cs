//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$TLab::DefaultPlugins = "SceneEditor";

//==============================================================================
// Create the Plugin object with initial data
function Lab::createPlugin(%this,%moduleID,%displayName,%module)
{
   
   %modObj = TLModules.getModuleObj(%moduleID);
   if (!isObject(%modObj))
	{
      warnLog("Can't create a plugin without a modules anymore, please update your plugin script for:",%moduleId);
      return;
	}
	
   %plugName = %moduleId@"Plugin";
   if (%modObj.PluginName !$= "")
      %plugName = %modObj.PluginName;
   
	if (isObject(%plugName))
	{
		warnLog("Plugin already created:",%moduleId);
		return %plugName;
	}		
		
	//Create the ScriptObject for the Plugin
	%pluginObj = new ScriptObject(%plugName)
	{
		superClass = "EditorPlugin"; //Default to EditorPlugin class
		editorGui = EWorldEditor; //Default to EWorldEditor
		editorMode = "World";
		displayName = (%displayName !$= "") ? %displayName : %moduleId ;	
	   plugin = %moduleId;
		isEnabled =  $Cfg_Modules_[%moduleID,"activated"];
		useTools = false;
		moduleID = %modObj.moduleId;
		module = %modObj;
	};
	
	%pluginObj.plugin = %moduleId;
	%pluginObj.pluginOrder = $Cfg_[%moduleID,"pluginOrder"];
	
	LabPluginGroup.add(%pluginObj);

	if (%pluginObj.isMethod("onPluginCreated"))
		%pluginObj.onPluginCreated();
	
	return %pluginObj;
}
//------------------------------------------------------------------------------
//==============================================================================
// Load the Plugin scripts - if no execPLUGINNAME, the scripts were loaded using old system
function Lab::loadPlugin(%this,%pluginObj)
{
   if (%pluginObj.moduleId !$= "")
        TLModules.loadModule(%pluginObj.moduleId);
    
 		if (!isObject(%pluginObj.cfgArray))
		%cfgArray = Lab.createConfigArrayObject(%pluginObj.plugin, "Plugin",%pluginObj);  
 	
 	if (!isObject(%pluginObj.paramArray))
		Lab.initPluginConfig(%pluginObj);  
		
   if (!%pluginObj.paramArray.firstReadDone)
		%this.readConfigArray(%pluginObj.paramArray);

	if (%pluginObj.isMethod("onPluginLoaded"))
		%pluginObj.onPluginLoaded();

	%pluginObj.initialized = true;
	
}

//==============================================================================
// Plugins Initialization Scripts
//==============================================================================

//==============================================================================
// Initialize the PluginObj configs
function Lab::initPluginConfig(%this,%pluginObj)
{
	
	%moduleId = %pluginObj.plugin;

	if (isFile("tlab/"@%moduleId@"/defaults.cs"))
		exec("tlab/"@%moduleId@"/defaults.cs");

	//Moving toward new params array system
	%newArray = Lab.createParamsArray("Plugins",%moduleId,%pluginObj);
	%newArray.displayName = %pluginObj.displayName;
	%pluginObj.paramArray = %newArray;
	%newArray.pluginObj = %pluginObj;
	%newArray.paramCallback = "Lab.onParamPluginBuild";
	//Default plugin settings
	%newArray.setVal("pluginOrder",      "99" TAB "pluginOrder" TAB "" TAB "" TAB %pluginObj.getName());
	%newArray.setVal("isEnabled",      "1" TAB "isEnabled" TAB "" TAB "" TAB %pluginObj.getName());
	%newArray.cfgData = %moduleId;

	if (%pluginObj.isMethod("initParamsArray"))
		%pluginObj.initParamsArray(%newArray);
}
//------------------------------------------------------------------------------

//==============================================================================
// Make the Active Plugins Enabled - Called from Editor::open
//==============================================================================

//------------------------------------------------------------------------------
function Lab::enablePlugin(%this,%pluginObj,%enabled)
{
	if (!isObject(%pluginObj))
		return;
	
	%pluginObj.setPluginEnable(true);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
function Lab::disablePlugin(%this,%pluginObj)
{
	if (!isObject(%pluginObj))
		return;
	
	%pluginObj.setPluginEnable(false);
}
//------------------------------------------------------------------------------
//==============================================================================
function EditorPlugin::setPluginEnable(%this,%enabled,%noMenuUpdate)
{
   if (!isObject(%this.module))
	   return;
	
	//The module system will deal with activation/enabled   
   TLModules.setEnabled(%this.module,%enabled);   		
	%this.isEnabled = %enabled; //TODO: Should remove and stick with module
	
	Lab.checkPluginIcon(%this);

	//Simply rebuild the editorMenus since the GuiMenuBar is primitive...
	if (!%noMenuUpdate)
		WEMenuEditors.updatePlugin(%this);

}
//------------------------------------------------------------------------------
function Lab::addPluginMap(%this,%plugin)
{

	%pluginObj = %plugin@"Plugin";

	if (!$Cfg_[%plugin,"noMap"])
		%map = newActionMap(%plugin);

	%pluginObj.map = %map;
	return %map;
	// Simset Holding Editor Guis for the plugin
}
//------------------------------------------------------------------------------
