xcopy *.bak G:\GameProjects\BotHeroes\Archive\backup  /sy
xcopy *.guibak G:\GameProjects\BotHeroes\Archive\backup  /sy
xcopy *.bakgui G:\GameProjects\BotHeroes\Archive\backup  /sy

forfiles /S /M *.bak /C "cmd /c del @file"
forfiles /S /M *.guibak /C "cmd /c del @file"
forfiles /S /M *.bakgui /C "cmd /c del @file"
forfiles /S /M *.guiorig /C "cmd /c del @file"