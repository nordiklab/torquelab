REM Get the User Documents folders
FOR /F "tokens=3 delims= " %%G IN ('REG QUERY "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" /v "Personal"') DO (SET docsdir=%%G)
echo mydocuments="%docsdir%"

REM Echo all files that are older than 10 days under Documents/TorqueLab/Backup
FORFILES /P "%docsdir%\TorqueLab\backup" /s /m *.* /D -10 /c "cmd /c echo @path is more than 10 days old and will be deleted"

REM Delete the 10 days older files
FORFILES /P "%docsdir%\TorqueLab\backup" /s /m *.* /D -10 /c "cmd /c del /Q @path"

REM 1 Minute Timeout to allow the user to see deleted files (Will close on key press)
timeout /t 60