//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//---------------------------------------------------------------------------------------------
// TCP Debugger
// To use the debugger, first call "dbgSetParameters(port, password);" from one instance of
// your game. Then, in another instance (either on the same system, or a different one) call
// "startDebugger();". Then use the gui to connect to the first instance with the port and
// password you first passed to dbgSetParameters.
//---------------------------------------------------------------------------------------------

function initializeDebugger()
{
	info("Plugin","->","Initializing","Debugger");
	// Load the scripts.
	exec("./Scripts/debugger.cs");
	// And the guis.
	exec("./Gui/breakConditionDlg.gui");
	exec("./Gui/connectDlg.gui");
	exec("./Gui/editWatchDlg.gui");
	exec("./Gui/findDlg.gui");
	exec("./Gui/debugger.gui");
	exec("./Gui/watchDlg.gui");
}

function destroyDebugger()
{
	if (isObject(TCPDebugger))
		TCPDebugger.delete();
}
function startDebugger()
{
	// Clean up first.
	destroyDebugger();
	// Create a TCP object named TCPDebugger.
	new TCPObject(TCPDebugger);
	// Used to get unique IDs for breakpoints and watch expressions.
	$DbgBreakId = 0;
	$DbgWatchSeq = 1;
	// Set up the GUI.
	DebuggerConsoleView.setActive(false);
	Canvas.pushDialog(DebuggerGui);
}

