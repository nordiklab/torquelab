//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

singleton CubemapData( MipCubemap )
{
	cubeFace[0] = "./TEST";
	cubeFace[1] = "./TEST";
	cubeFace[2] = "./TEST";
	cubeFace[3] = "./TEST";
	cubeFace[4] = "./TEST";
	cubeFace[5] = "./TEST";
};

