//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//------------------------------------------------------------------------------

function ModelEditor::create( %this )
{
  info("Tool","->","Initializing","Model Editor");
      execGuiDir("tlab/tools/ModelEditor/",true);
		execPattern("tlab/tools/ModelEditor/*.cs");
}

//------------------------------------------------------------------------------

function ModelEditor::destroy( %this )
{
  
}

//------------------------------------------------------------------------------
