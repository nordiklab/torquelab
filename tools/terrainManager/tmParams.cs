//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$TEManager_TerrainTextureFormats = "JPG DDS PNG";
//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
//SEP_ScatterSkyManager.buildParams();
function TEManager::buildTerrainInfoParams( %this )
{
	%arCfg = Lab.createBaseParamsArray("TEManager_Terrain",TEManager_TerrainArrayStack);
	%arCfg.updateFunc = "TEManager.updateTerrainParam";
	%arCfg.style = "StyleA";
	%arCfg.useNewSystem = true;
	%arCfg.group[%gid++] = "TerrainBlock Parameters";
	%arCfg.setVal("terrainFile",       "" TAB "terrainFile" TAB "FileSelect" TAB "callback>>TEManager.getTerrainFile();" TAB "TEManager.activeTerrain" TAB %gid);
	%arCfg.setVal("castShadows",        "" TAB "castShadows" TAB "Checkbox" TAB "" TAB "TEManager.activeTerrain" TAB %gid);
	%arCfg.setVal("squareSize",   "" TAB "squareSize" TAB "SliderEdit" TAB "range>>0 10;;tickAt>>0.1" TAB "TEManager.activeTerrain" TAB %gid);
	%arCfg.setVal("baseTexSize",        "" TAB "baseTexSize" TAB "TextEdit" TAB "" TAB "TEManager.activeTerrain" TAB %gid);
	%arCfg.setVal("baseTexFormat",        "" TAB "baseTexFormat" TAB "DropDown" TAB "itemList>>$TEManager_TerrainTextureFormats" TAB "TEManager.activeTerrain" TAB %gid);
	%arCfg.setVal("lightMapSize",        "" TAB "lightMapSize" TAB "TextEdit" TAB "" TAB "TEManager.activeTerrain" TAB %gid);
	%arCfg.setVal("screenError",        "" TAB "screenError" TAB "TextEdit" TAB "" TAB "TEManager.activeTerrain" TAB %gid);
	buildParamsArray(%arCfg,isObject(TEManager.activeTerrain));
	%this.terrainArray = %arCfg;
}
//------------------------------------------------------------------------------
//syncParamArray(TEManager.terrainArray);
//==============================================================================
function TEManager::updateTerrainParam(%this,%field,%value,%ctrl,%array)
{
}
//------------------------------------------------------------------------------
