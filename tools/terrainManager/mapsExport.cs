//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$TEManager_ExportLayerFormat = "png";
//==============================================================================
function TEManager::initExporter( %this)
{
}
//------------------------------------------------------------------------------

//==============================================================================
function TEManager::exportEverything( %this,%target,%noOverwrite)
{
	%terObj = %this.activeTerrain;

	if (!isObject(%terObj))
	{
		warnlog("Trying to export layers and heightmap for invalid terrain:",%terObj);
		return;
	}

	if (%terObj.isNew)
	{
		warnLog("You must import an heightmap on a new terrain before exporting.");
		return;
	}

	if (%target $= "")
	{
		getFolderName("","TEManager.exportEverything",TEManager.dataFolder,"Select destination folder");
		return;
	}

	%this.exportHeightMap(%target,%noOverwrite);
	%this.exportTerrainLayersToPath(%target,"png");
}
//------------------------------------------------------------------------------

//==============================================================================
function TEManager::exportTerrainData( %this,%target)
{
	if (%target $= "")
	{
		getFolderName("","TEManager.exportTerrainData","Select folder to export terrain data");
		return;
	}

	if (%target $= "Source")
		%folder = TEManager.sourceFolder;
	else if (%target $= "Target")
		%folder = TEManager.targetFolder;
	else if (%target $= "Terrain")
		%folder = TEManager.terrainFolder;
	else
		%folder = %target;

	if (!isDirectory(%folder))
	{
		warnLog("Invalid folder specified for export:",%folder);
		return;
	}

	%exportHeightmap = TEManager_ExportTypeRadios-->exportHeightmap.isStateOn();
	%exportTexturemap = TEManager_ExportTypeRadios-->exportTexturemap.isStateOn();
	%exportAll = TEManager_ExportTypeRadios-->exportAll.isStateOn();

	if (%exportHeightmap || %exportAll)
		%this.exportHeightMap(%folder,%noOverwrite);

	if (%exportTexturemap || %exportAll)
		%this.exportTerrainLayersToPath(%folder,"png");
}
//------------------------------------------------------------------------------

//==============================================================================
// Export Functions
//==============================================================================
function TEManager::exportHeightMapToFile( %this)
{
	getSaveFilename("Png Files|*.png","TEManager.exportHeightMapFile",TEManager.dataFolder,true);
}
//==============================================================================
function TEManager::exportHeightMap( %this,%folder,%noOverwrite)
{
	%terObj = %this.activeTerrain;

	if (!isObject(%terObj))
	{
		warnlog("Trying to export layers for invalid terrain:",%terObj);
		return;
	}

	if ($TEManager_ExportLayerFormat $= "")
		$TEManager_ExportLayerFormat = "png";

	if (%folder $= "")
		%folder = %terObj.dataFolder;

	if (%folder $= "")
	{
		%baseFile = MissionGroup.getFilename();
		%folder = filePath( MissionGroup.getFilename());
	}

	if ($TEManager_ExportFolderName $= "")
		$TEManager_ExportFolderName = "Default";

	%exportPath = %folder@"/"@$TEManager_ExportFolderName;
	%hmName = TEManager.getTerrainHeightmapName();
	%filePrefix = %hmName@".png";
	%exportFile = %folder @ "/" @ %filePrefix;
	%result = %this.exportHeightMapFile(%exportFile,"png",%noOverwrite);
	//%ret = %terObj.exportHeightMap( %folder @ "/" @ %filePrefix,"png" );

	if (%result)
		return %folder @ "/" @ %filePrefix;

	return "";
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager::exportHeightMapFile( %this,%file,%format,%noOverwrite)
{
	%terObj = %this.activeTerrain;

	if (!isObject(%terObj))
	{
		warnlog("Trying to export layers for invalid terrain:",%terObj);
		return;
	}

	if (%format $= "")
		%format = "png";

	if (%noOverwrite)

	if (isFile(%file))

	if (isFile(%file) && %noOverwrite)
	{
		%newFile = filePath(%file)@"/"@fileBase(%file)@"_"@%inc++@fileExt(%file);

		while (isFile(%newFile))
		{
			%newFile = filePath(%file)@"/"@fileBase(%file)@"_"@%inc++@fileExt(%file);

			if (%inc > 20)
			{
				warnLog("Couldn't find a unique file name after 20 attempts. Export aborted!");
				return false;
			}
		}

		%file = %newFile;
	}

	%ret = %terObj.exportHeightMap( %file,%format );

	if (%ret)
		info("Heightmap exported to:",%file);
	else
		info("Heightmap failed to export to:",%file,"See console for report");

	return %ret;
}
//------------------------------------------------------------------------------

//==============================================================================
function TEManager::exportTerrainLayersToPath( %this,%exportPath,%format,%layerId)
{
	%terObj = %this.activeTerrain;

	if (!isObject(%terObj))
	{
		warnlog("Trying to export layers for invalid terrain:",%terObj);
		return;
	}

	if (%terObj.isNew)
	{
		warnLog("You must import an heightmap on a new terrain before exporting.");
		return;
	}

	if (%format $= "")
		%format = "png";

	if (%exportPath $= "")
	{
		getFolderName("","TEManager.exportTerrainLayersToPath",TEManager.dataFolder,"Select destination folder",%format, %layerId);
		return;
	}

	if (%exportPath $= "Default")
	{
		%exportPath = TEManager.terrainFolder;
	}

	%filePrefix = %terObj.getName() @ "_layerMap";

	if (%layerId !$= "")
	{
		%ret = %terObj.exportSingleLayerMap( %layerId, %exportPath @ "/" @ %filePrefix, %format );
	}
	else
	{
		%ret = %terObj.exportLayerMaps( %exportPath @ "/" @ %filePrefix, "png" );
	}
}
//------------------------------------------------------------------------------
