//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function TEManager::getActiveFolders(%this)
{
	%dataFolder = TEManager.activeTerrain.dataFolder;

	if (%dataFolder $= "")
		%dataFolder = MissionGroup.dataFolder;

	if (%dataFolder $= "")
		%dataFolder = filePath(TEManager.activeTerrain.terrainFile);

	TEManager.setFolder("data",validatePath(%dataFolder));
	%sourceFolder = TEManager.activeTerrain.sourceFolder;

	if (%sourceFolder $= "")
		%sourceFolder = MissionGroup.sourceFolder;

	if (%sourceFolder $= "")
		%sourceFolder = %dataFolder@"/Source";

	TEManager.setFolder("source",validatePath(%sourceFolder));
	%targetFolder = TEManager.activeTerrain.targetFolder;

	if (%targetFolder $= "")
		%targetFolder = MissionGroup.targetFolder;

	if (%targetFolder $= "")
		%targetFolder = %dataFolder@"/Target";

	TEManager.setFolder("target",validatePath(%targetFolder));
	%terrainFolder = validatePath(TEManager.dataFolder@"/terData/"@TEManager.activeTerrain.getName(),true);
	TEManager.terrainFolder = %terrainFolder;
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager::setFolder(%this,%type,%folder,%relativeToData,%onlyTEManager)
{
	if (%folder $= "")
		return;

	if (%relativeToData)
	{
		%subFolder = %folder;
		%folder = TEManager.dataFolder@"/"@%subFolder;
		%folder = strreplace(%folder,"//","/");
		%subField = %type@"SubFolder";
		eval("TEManager."@%subField@" = %subFolder;");
	}

	%folder = validatePath(%folder,true);
	%field = %type@"Folder";
	eval("%currentFolder = TEManager."@%field@";");

	if (%currentFolder !$= %folder)
		%folderChanged = true;

	eval("TEManager."@%field@" = %folder;");

	if (%type !$= "data")
	{
		%subText = %folder;

		if (strFind(%folder,TEManager.dataFolder))
		{
			%relativeFolder = strreplace(%folder,TEManager.dataFolder,"");

			if (getSubStr(%relativeFolder,0,1) $= "/")
				%relativeFolder = getSubStr(%relativeFolder,1);

			%subText = %relativeFolder;
		}

		eval("%editCtrl = TEManagerGui-->"@%type@"_sideFolder;");
		%editCtrl.setText(%subText);
		eval("%subEdit = TEManagerGui-->"@%type@"_FolderEdit;");
		%subEdit.setText(%subText);
		eval("%genEdit = TEManager_GeneralFolderSetup-->"@%type@"_FolderEdit;");
		%genEdit.setText(%subText);
	}
	else
	{
		TEManagerGui-->dataFolder.setText(TEManager.dataFolder);
		TEManagerGui-->sideDataFolderText.text = TEManager.dataFolder;
		TEManager_GeneralFolderSetup-->dataFolder.setText(TEManager.dataFolder);
		%this.setFolder("source",TEManager.sourceSubFolder,true);
		%this.setFolder("target",TEManager.targetSubFolder,true);
	}

	if (!%onlyTEManager)
	{
		MissionGroup.setFieldValue(%field,%folder);
	}

	if (isObject(TEManager.activeTerrain))
	{
		if (TEManager.activeTerrain.storeFolders)
			TEManager.activeTerrain.setFieldValue(%field,%folder);
	}

	//Update map layers data if source changed
	if (%type $= "Source" && %folderChanged)
		TEManager.updateAllMaterialLayersMenu();
}
//------------------------------------------------------------------------------
//==============================================================================
// Select TEManager Data folder
//==============================================================================
//==============================================================================
function TEManager::selectDataFolder( %this,%type)
{
	if (%type $= "")
		%type = "data";

	eval("%currentFolder = TEManager."@%type@"Folder;");

	if (!isDirectory(%currentFolder))
		%currentFolder = MissionGroup.getFilename();

	getFolderName("*.*","TEManager.setDataFolder",%currentFolder,"Select Export Folder",%type);
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager::setDataFolder( %this, %path,%type )
{
	%terObj = %this.activeTerrain;

	if (!isObject(%terObj))
	{
		warnlog("Not active terrain detected. Please select one before setting data folder:",%terObj);
		return;
	}

	%path =  makeRelativePath( %path, getMainDotCsDir() );
	//TEManagerGui-->dataFolder.setText(%path);
	%this.setFolder(%type,%path);
}
//------------------------------------------------------------------------------

//==============================================================================
// Data/Source/Target TextEdit validations
//==============================================================================

//==============================================================================
function RelativeFolderEdit::onValidate(%this)
{
	%data = strreplace(%this.internalName,"_"," ");
	%type = getWord(%data,0);
	%path = TEManager.fixEditPath(%this.getText());
	TEManager.setFolder(%type,%path,true);
}
//------------------------------------------------------------------------------

//==============================================================================
function TEManager_RelativeSourceFolderEdit::onValidate( %this )
{
	%path = TEManager.fixEditPath(%this.getText());
	TEManager.setFolder("source",%path,true);
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager_RelativeTargetFolderEdit::onValidate( %this )
{
	%path = TEManager.fixEditPath(%this.getText());
	TEManager.setFolder("target",%path,true);
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager::fixEditPath( %this,%path )
{
	%fixPath = %path;
	return %fixPath;
}
//------------------------------------------------------------------------------
