//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function TEManager::setActiveTerrain(%this,%terrainId)
{
	/*	if (isObject(TEManager.activeTerrain) && isObject(TEManager.activeTerrain)){
			if (TEManager.activeTerrain.getId() $= %terrainId.getId()){
				warnlog(TEManager.activeTerrain.getName(),"Terrain is already active");
				return;
			}
		}
	*/
	TEManager.activeDataUpdated = false;

	if (!isObject(%terrainId))
	{
		TEManager.activeTerrain = "";
		TEManager.activeHeightInfo = "";
	}

	TEManager.activeTerrain = %terrainId;
	TEManager.activeTerrainId = %terrainId.getId();
	TEManager_ActiveTerrainMenu.setText(TEManager.activeTerrain.getName());
	%this.getActiveFolders();
	//===========================================================================
	// GENERAL TAB INFORMATIONS
	TEManager_ActiveTerrainNameEdit.setText(TEManager.activeTerrain.getName());
	TEManager_ActiveTerrainNameApply.active = 0;
	TEManager_ActiveTerrainFileEdit.setText(TEManager.activeTerrain.terrainFile);
	TEManager_ActiveTerrainFileApply.active = 0;
	//---------------------------------------------------------------------------
	TEManager_PageMaterialLayers-->heightmapModeStack-->Current.visible = !%terrainId.isNew;

	//TEManager_MaterialLayersNewTerrain.visible = %terrainId.isNew;
	if (%terrainId.isNew)
	{
		if (TEManager.heightmapMode $= "Current")
			TEManager.changeHeightmapMode("","Source");

		%terrainName = getUniqueName("theTerrain");
	}
	else
	{
		TEManager.changeHeightmapMode("","Current");
		%terrainName = %terrainId.getName();
		ETerrainEditor.attachTerrain(%terrainId);
		TEManager.activeHeightInfo = ETerrainEditor.getHeightRange();
		TEManager.activeHeightRangeInfo = TEManager.activeHeightInfo.x SPC "\c1(\c2"@TEManager.activeHeightInfo.y@"\c1/\c2"@TEManager.activeHeightInfo.z@"\c1)";
		TEManager.activeHeightRange = "\c2"@TEManager.activeHeightInfo.x;
		TEManager.activeHeightMin = "\c3"@TEManager.activeHeightInfo.y;
		TEManager.activeHeightMax = "\c4"@TEManager.activeHeightInfo.z;
		TEManager_HeightmapOptions-->heightScale.setText(TEManager.activeHeightInfo.x);
		TEManager_HeightmapOptions-->squareSize.setText(%terrainId.squareSize);
		TEManager.activeTexturesCount = ETerrainEditor.getNumTextures();
		TEManager.infoTexturesCount = "All terrains textures used:\c2" SPC TEManager.activeTexturesCount;
		TEManager.infoTexturesActive = "Active terrain textures used:\c2" SPC getRecordCount(ETerrainEditor.getMaterials());
		TEManager.infoBlockCount = "Total terrain blocks:\c2" SPC ETerrainEditor.getTerrainBlockCount();
		TEManager.infoBlockMatCount = "Total block materials:\c2" SPC getRecordCount(ETerrainEditor.getTerrainBlocksMaterialList());
		TEManager.infoBlockList = "Terrain blocks list:\c2" SPC ETerrainEditor.getTerrainBlocksMaterialList();
		TEManager_PageGeneral-->storeFolders.setStateOn(%terrainId.storeFolders);
		TEManager.infoActions1 = "Totals actions:\c2" SPC ETerrainEditor.getActionName(1);
		syncParamArray(TEManager.terrainArray);
		TEManager_PageMaterialLayers-->terrainX.setText(%terrainId.position.x);
		TEManager_PageMaterialLayers-->terrainY.setText(%terrainId.position.y);
		TEManager_PageMaterialLayers-->terrainZ.setText(%terrainId.position.z);
	}

	%this.validateImportTerrainName(%terrainName);
	%this.getTerrainHeightmapName();
	%this.updateTerrainLayers();

	if (ETerrainEditor.getMaterials() $= "")
		TEManager.schedule(1000,"updateMaterialLayers");
	else
		%this.updateMaterialLayers();

	if (TEManager.AutoGenerateHeightmap)
		%this.prepareHeightmap();

	TEManager.activeDataUpdated = true;
}
//------------------------------------------------------------------------------

//==============================================================================
function TEManager::updateTerrainList(%this,%selectCurrent)
{
	TEManager_ActiveTerrainMenu.clear();
	%list = getMissionObjectClassList("TerrainBlock");

	if (getWordCount(%list) < 1)
	{
		TEManager_ActiveTerrainMenu.add("No terrain found",0);
		TEManager_ActiveTerrainMenu.setText("No terrain found");
		return;
	}

	foreach$(%terrain in %list)
	{
		TEManager_ActiveTerrainMenu.add(%terrain.getName(),%terrain.getId());
	}

	//TEManager_ActiveTerrainMenu.add("New terrain",0);

	if (!%selectCurrent)
		return;

	if (!isObject(TEManager.activeTerrain))
		TEManager.setActiveTerrain(getWord(%list,0));
	else if (%selectCurrent)
		TEManager.setActiveTerrain(TEManager.activeTerrain);

	//TEManager_ActiveTerrainMenu.setSelected(TEManager.activeTerrain.getId());
}
//------------------------------------------------------------------------------

//==============================================================================
function TEManager_ActiveTerrainMenu::onSelect(%this,%id,%text)
{
	if (%id $= "0")
	{
		TEManager.activeTerrain = newScriptObject("TEManager_NewTerrain");
		TEManager_NewTerrain.isNew = true;
		TEManager_NewTerrain.dataFolder =
		   %id = TEManager.activeTerrain.getId();
	}

	if (!isObject(%id))
		return;

	TEManager.setActiveTerrain(%id);
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager::storeFolderToTerrain(%this,%storeToTerrain)
{
	if (!isObject(TEManager.ActiveTerrain))
		return;

	if (!%storeToTerrain && TEManager.ActiveTerrain.getFieldValue("storeFolders") $= "")
		return;

	TEManager.ActiveTerrain.setFieldValue("storeFolders",%storeToTerrain);
}
//------------------------------------------------------------------------------
//==============================================================================
// Export Single Layer Map
//==============================================================================

//==============================================================================
function TEManager::getTerrainHeightmapName(%this)
{
	TEManager.activeHeightmapName = "";

	if (!isObject(TEManager.ActiveTerrain))
		return;

	%heightMapName = TEManager.ActiveTerrain.getName()@"_hm_"@TEManager.ActiveTerrain.squareSize;

	if (TEManager.activeHeightRange !$="")
		%heightMapName = %heightMapName @"_"@mCeil(TEManager.activeHeightInfo.x);

	TEManager.activeHeightmapName = %heightMapName;
	return %heightMapName;
}
//------------------------------------------------------------------------------
