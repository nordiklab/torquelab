//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$pref::Directories::Terrain = "art/Levels/FarmDemo/";

$TEManager_DefaultHeightmap_Invalid = "tlab/terrainEditor/gui/images/heightmapInvalid.png";
$TEManager_DefaultHeightmap_NotGenerated = "tlab/terrainEditor/gui/images/heightmapN.png";
$TEManager_DefaultHeightmap_Invalid = "tlab/terrainEditor/gui/images/heightmapInvalid.png";
//==============================================================================
// Prepare and import the terrain HeightMap (Manager Import Terrain Button)
//==============================================================================
//==============================================================================
function TEManager::updateImportHeightmapBmp(%this,%doImport)
{
	if (TEManager.heightmapMode $= "Source")
	{
		%hmMenu = TEManager_PageMaterialLayers-->heightMapMenu;
		%heightmapSrc = TEManager_SourceHeightMapMenu.getText();
		%heightmapFile = TEManager.SourceFolder@"/"@%heightmapSrc;

		if (!isFile(%heightmapFile))
			%bmpFile = "tlab/terrainEditor/gui/images/heightmapNotSelected.png";
		else
			%bmpFile = %heightmapFile;
	}
	else if (TEManager.heightmapMode $= "Current")
	{
		%heightmapFile = TEManager.terrainHeightMap;

		if (!isFile(%heightmapFile))
		{
			TEManager_GenerateHeightmapButton.visible = 1;
			%bmpFile = "tlab/terrainEditor/gui/images/heightmapNotGenerated.png";
		}
		else
		{
			TEManager_GenerateHeightmapButton.visible = 0;
			%bmpFile = %heightmapFile;
		}
	}
	else if (TEManager.heightmapMode $= "Browse")
	{
		%heightmapFile = TEManager.browseHeightMap;

		if (!isFile(%heightmapFile))
			%bmpFile = "tlab/terrainEditor/gui/images/heightmapNotSelected.png";
		else
			%bmpFile = %heightmapFile;
	}

	//%heightmapFile = TEManager.currentHeightMap;
	%heightmapFile = validatePath(%heightmapFile,true);

	if (!isFile(%heightmapFile))
	{
		warnLog("Invalid file or terrainObj file:",	%heightmapFile);
		%heightmapFile = "";
		%bmpFile = "tlab/terrainEditor/gui/images/heightmapInvalid.png";
	}

	TEManager_ImportHeightMapBmp.setBitmap(%bmpFile);
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager::prepareAllLayers(%this,%doImport)
{
	%folder = TEManager.DataFolder@"/TerData/"@TEManager.activeTerrain.getName();
	%this.prepareOpacityMaps(%folder);

	foreach(%pill in TEManager_MaterialLayersStack)
	{

		if (%pill.useDefaultLayer && !isFile(%pill.file))
		{
			%pill.file = %folder@"/"@%pill.defaultLayerFile@".png";
		}

		if (!isFile(%pill.file))
		{
			%pill.file = %folder@"/"@%pill-->mapMenu.file[0]@".png";
			warnLog(%pill.layerId," Layer file created from menu text:",%pill.file);
		}

		%pill.activeChannels = "";
		%stack = %pill-->channelStack;

		foreach$(%chan in %pill.channelRadios)
		{
			%radio = %stack.findObjectByInternalName(%chan);

			if (%radio.isStateOn())
				%pill.activeChannels = strAddWord(%pill.activeChannels,%chan);
		}
	}

	%this.prepareHeightmap(%folder);

	if (%doImport)
		%this.importTerrain();

	%this.updateImportHeightmapBmp();
}
//------------------------------------------------------------------------------

//==============================================================================
function TEManager::prepareHeightmap(%this,%folder)
{
	if (%folder $= "")
		%folder = TEManager.DataFolder@"/TerData/"@TEManager.activeTerrain.getName();

	TEManager.terrainHeightMap = %this.exportHeightMap(%folder);
	%this.updateImportHeightmapBmp();
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager::prepareOpacityMaps(%this,%folder)
{
	if (%folder $= "")
		%folder = TEManager.DataFolder@"/TerData/"@TEManager.activeTerrain.getName();

	%this.exportTerrainLayersToPath(%folder,"png");
}
//------------------------------------------------------------------------------
//==============================================================================
// Import Terrain Heightmap + Layers
//==============================================================================
//==============================================================================
function TEManager::importTerrain(%this)
{
	%terObj = %this.activeTerrain;
	%folder = TEManager_PageMaterialLayers-->textureSourceFolder.text;
	%hmMenu = TEManager_PageMaterialLayers-->heightMapMenu;

	if (%hmMenu.getSelected() !$= "0")
	{
		%file = %folder @"/"@%hmMenu.getText();
		%file = strreplace(%file,"//","/");
		TEManager.currentHeightMap = %file;
	}

	TEManager.currentHeightMap = strreplace(TEManager.currentHeightMap,"//","/");
	%heightmapFile = TEManager.currentHeightMap;

	if (TEManager.heightmapMode $= "Current")
	{
		%heightmapFile = TEManager.terrainHeightMap;
	}
	else if (TEManager.heightmapMode $= "Source")
	{
		%heightmapSrc = %hmMenu.getText();
		%heightmapFile = TEManager.SourceFolder@"/"@%heightmapSrc;
	}

	if (!isFile(%heightmapFile))
	{
		warnLog("Invalid file or terrainObj terrain:",	%terObj);
		warnLog("Invalid file or terrainObj file:",	TEManager.currentHeightMap);
		return;
	}

	%metersPerPixel = TEManager_HeightmapOptions-->squareSize.getText();
	%heightScale = TEManager_HeightmapOptions-->heightScale.getText();
	%flipYAxis = TEManager_HeightmapOptions-->flipAxisCheck.isStateOn();

	foreach(%pill in TEManager_MaterialLayersStack)
	{
		%fixFile = %pill.file;

		if (!isFile(%fixFile))
			%preImportFailed = true;

		%opacityNames = strAddRecord(%opacityNames,%fixFile TAB %pill.activeChannels);
		%materialNames = strAddRecord(%materialNames,%pill.matInternalName);
	}

	if (%preImportFailed)
	{
		return;
	}

	%name = TEManager_PageMaterialLayers-->importTerrainName.getText();
	%bmpInfo = getBitmapinfo(%heightmapFile );
	%size = getWord(%bmpInfo,0);
	%worldSize = (%size * %metersPerPixel);
	%position = TEManager_PageMaterialLayers-->terrainX.getText() SPC TEManager_PageMaterialLayers-->terrainY.getText() SPC TEManager_PageMaterialLayers-->terrainZ.getText();

	if (TEManager.centerImportTerrain)
	{
		%position.x =  %worldSize/-2;
		%position.y =  %worldSize/-2;
	}

	//%saveToFile = filePath(MissionGroup.getFileName());
	//if (isObject(%name))
	//	%saveToFile = %name.getFileName();
	//%defaultTerrainDir = $pref::Directories::Terrain;
	//$pref::Directories::Terrain = %saveToFile;
	delObj(%name);
	%updated = nameToID( %name );
	%obj = TerrainBlock::import(  %name,
	                              %heightmapFile,
	                              %metersPerPixel,
	                              %heightScale,
	                              %opacityNames,
	                              %materialNames,
	                              %flipYAxis );
	%obj.terrainHeight = %heightScale;
	%obj.dataFolder = TEManager.dataFolder;
	%obj.sourceFolder = TEManager.sourceFolder;
	%obj.targetFolder = TEManager.targetFolder;
	%obj.position = %position;
	//%obj.setFilename(%saveToFile);

	if ( isObject( %obj ) )
	{
		if ( %obj != %updated )
		{
			// created a new TerrainBlock
			// Submit an undo action.
			MECreateUndoAction::submit(%obj);
		}

		%obj.schedule(500,"setPosition",%position);
		assert( isObject( EWorldEditor ),
		        "ObjectBuilderGui::processNewObject - EWorldEditor is missing!" );
		// Select it in the editor.
		EWorldEditor.clearSelection();
		EWorldEditor.selectObject(%obj);
		// When we drop the selection don't store undo
		// state for it... the creation deals with it.
		EWorldEditor.dropSelection( true );
		ETerrainEditor.isDirty = true;
		EPainter.updateLayers();
		TEManager.activeTerrain = %obj;
		TEManager.updateTerrainList(true);
		//TEManager.saveTerrain();
		//ETerrainEditor.saveTerrainToFile(%obj);
		//TEManager.setActiveTerrain(%obj);
	}
	else
	{
		warnLog("Something bad happen and heightmap import failed!");
	}

	//$pref::Directories::Terrain = %defaultTerrainDir;
}

//------------------------------------------------------------------------------
//==============================================================================
// Export Single Layer Map
//==============================================================================

//==============================================================================
function TEManager::selectSingleTextureMapFolder( %this, %layerId)
{
	%folder = TEManagerGui-->dataFolder.getText();
	getFolderName(%filter,"TEManager.setSingleTextureMapFolder",%folder,"Select Export Folder",%layerId);
}
//------------------------------------------------------------------------------
function TEManager::setSingleTextureMapFolder( %this, %path,%layerId)
{
	%this.exportTerrainLayersToPath(%path,"png",%layerId);
}
//------------------------------------------------------------------------------

//==============================================================================
function TEManager_ImportTerrainNameEdit::onValidate(%this)
{
	%name = %this.getText();
	TEManager.validateImportTerrainName(%name);
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager::validateImportTerrainName(%this,%name)
{
	if (isObject(%name))
	{
		%info = "\c2Terrain exist and will be overridden with heightmap";
	}
	else
	{
		%info = "\c1New terrain will be created from heightmap";
	}

	TEManager_PageMaterialLayers-->importTerrainNameStatus.setText(%info);
	TEManager_PageMaterialLayers-->importTerrainName.setText(%name);
}
//------------------------------------------------------------------------------
//==============================================================================
// Heightmap for re-importing
//==============================================================================
//==============================================================================
function TEManager::changeHeightmapMode( %this, %ctrl, %mode)
{
	TEManager.heightmapMode = %mode;

	if (TEManager.heightmapMode $= "")
		TEManager.heightmapMode = %ctrl.internalName;

	TEManager_HeightmapOptions-->flipAxisCheck.active = 1;
	TEManager_PageMaterialLayers-->currentHeightmap.visible = 0;
	TEManager_PageMaterialLayers-->browseHeightmap.visible = 0;
	TEManager_PageMaterialLayers-->sourceHeightmap.visible = 0;
	eval("TEManager_PageMaterialLayers-->"@TEManager.heightmapMode@"Heightmap.visible = 1;");
	eval("TEManager_PageMaterialLayers-->heightmapModeStack-->"@TEManager.heightmapMode@".setStateOn(true);");

	if (%mode $= "Current")
	{
		TEManager_HeightmapOptions-->flipAxisCheck.setStateOn(true);
		//TEManager_HeightmapOptions-->flipAxisCheck.active = 1;
	}

	TEManager.updateImportHeightmapBmp();
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager_SourceHeightMapMenu::onSelect( %this, %id, %text)
{
	TEManager.updateImportHeightmapBmp();
}
//------------------------------------------------------------------------------
function TEManager::selectHeightMapImage( %this)
{
	getLoadFilename("Png Files|*.png","TEManager.setHeightMapImage",TEManager.terrainHeightMap);
}
//==============================================================================
function TEManager::setHeightMapImage( %this,%file)
{
	%file = validatePath(%file,true);
	TEManager.browseHeightMap = %file;
	TEManager_PageMaterialLayers-->heightmapFile.setText(%file);
	TEManager.updateImportHeightmapBmp();
}
function TEManager_FlipAxisCheck::onClick( %this)
{
	if (TEManager.heightmapMode $= "Current" && !%this.isStateOn())
	{
		LabMsgOkCancel("Terrain will be inverted","Torque3D export the terrain heightmap with flipped Y axis. If you choose to not flip the Y axis while importing current terrain heightmap" SPC
		               "exported from Torque3D, your terrain will be inverted. If you also use exported layer maps, those won't fit with terrain shape. If you want to toggle Y Axis Flipping off, click OK.",
		               "","TEManager_HeightmapOptions-->flipAxisCheck.setStateOn(true);");
	}
}
function TEManager_CenterTerrainCheckbox::onClick( %this)
{
	TEManager.setCenteredTerrain(%this.isStateOn());
}
function TEManager::setCenteredTerrain( %this,%isCentered)
{
	if (%isCentered)
	{
		TEManager_ImportOptions-->TerrainX.active = 0;
		TEManager_ImportOptions-->TerrainY.active = 0;
		return;
	}

	TEManager_ImportOptions-->TerrainX.active = 1;
	TEManager_ImportOptions-->TerrainY.active = 1;
}
