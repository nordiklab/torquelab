//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function TEManager_AutoExportLayerRadio::onClick( %this)
{
	%mode = %this.internalName;
	TEManager.setAutoExportMode(%mode);
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager::setAutoExportMode( %this,%mode)
{
	%radio = TEManager_AutoExportLayerRadios.findObjectByInternalName(%mode);
	%radio.setStateOn(true);
	TEManager.autoExportLayerMode = %mode;
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager::getLayersMapFolder( %this)
{
	if (TEManager.autoExportLayerMode $= "Target")
		return TEManager.targetFolder;
	else if (TEManager.autoExportLayerMode $= "Terrain")
	{
		return TEManager.terrainFolder;
	}

	return TEManager.sourceFolder;
}
//------------------------------------------------------------------------------

//==============================================================================
function TEManager_ShowMapPreviewCheck::onClick( %this)
{
	TEManager.ShowMapPreview = %this.isStateOn();

	foreach(%pill in TEManager_MaterialLayersStack)
	{
		%previewContainer = %pill-->imageButton.parentGroup.parentGroup;
		%previewContainer.visible = TEManager.ShowMapPreview;
	}
}
//------------------------------------------------------------------------------
