//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$TEManager_Init = false;
$TEManager_CurrentPage = 0;
$TEManager_LayersImportExportPage = 0;
$TEManager_LayerOptionsPage = 0;
//==============================================================================
//TEManagerGui onWake and onSleep callbacks
//==============================================================================
//==============================================================================
function TEManagerGui::onWake(%this)
{
	TEManager_LayerOptionsBook.selectPage($TEManager_LayerOptionsPage);
	TEManager_LayersImportExportBook.selectPage($TEManager_LayersImportExportPage);
	TEManager_MainBook.selectPage($TEManager_CurrentPage);
	TEManager.setCenteredTerrain(TEManager.centerImportTerrain);

	if ($InGuiEditor)
		return;

	if (!TEManager.initialized)
		TEManager.init();

	TEManager.refreshData();
	Lab.hidePluginTools();
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManagerGui::onSleep(%this)
{
	if (isObject(TEManager_GroundCoverClone-->MainContainer))
		SEP_GroundCover.add(TEManager_GroundCoverClone-->MainContainer);

	Lab.showPluginTools();
}
//------------------------------------------------------------------------------

//==============================================================================
// Called before GUI is saved in GuiEditor
function TEManagerGui::onPreEditorSave(%this)
{
	if (isObject(TEManager_GroundCoverClone-->MainContainer))
		SEP_GroundCover.add(TEManager_GroundCoverClone-->MainContainer);
}
//------------------------------------------------------------------------------
//==============================================================================
// Called after GUI is saved in GuiEditor
function TEManagerGui::onPostEditorSave(%this)
{
}
//------------------------------------------------------------------------------

//==============================================================================
// Common TEManager functions
//==============================================================================

//==============================================================================
function TEManager::init(%this)
{
	TEManager.ShowMapPreview = true;
	TEManager.setAutoExportMode("Never");
	TEManagerGui-->dataFolder.setText("");
	TEManager.initialized = true;
}
//------------------------------------------------------------------------------

//==============================================================================
function TEManager::refreshData(%this)
{
	hide(SEP_GroundCover);
	TEManager_GroundCoverClone.add(SEP_GroundCover-->MainContainer);
	TEManager.updateTerrainList(true);
	%this.refreshMaterialLayersPage();
	%this.buildTerrainInfoParams();
}
//------------------------------------------------------------------------------

//==============================================================================
function TEManager::toggleTools(%this,%button)
{
	%visible = Lab.togglePluginTools();
	%button.setStateOn(%visible);
}
//------------------------------------------------------------------------------

//==============================================================================
function TEManager_MainBook::onTabSelected(%this,%text,%id)
{
	$TEManager_CurrentPage = %id;
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager_LayersImportExportBook::onTabSelected(%this,%text,%id)
{
	$TEManager_LayersImportExportPage = %id;
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager_LayerOptionsBook::onTabSelected(%this,%text,%id)
{
	$TEManager_LayerOptionsPage = %id;
}
//------------------------------------------------------------------------------
