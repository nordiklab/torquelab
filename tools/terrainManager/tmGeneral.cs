//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// TerrainObject Functions
//==============================================================================

//==============================================================================
// Manage the terrain name
//==============================================================================
//==============================================================================
// Sync the current profile values into the params objects
function TEManager::applyTerrainName( %this )
{
	%newName = TEManager_ActiveTerrainNameEdit.getText();

	if (%newName $= TEManager.activeTerrain.getName())
		return;

	if (isObject(%newName))
	{
		warnLog("There's already an object using that name:",%newName);
		return;
	}

	TEManager.activeTerrain.setName(%newName);
	TEManager.setActiveTerrain(TEManager.activeTerrain);
}
//------------------------------------------------------------------------------
//==============================================================================
// Sync the current profile values into the params objects
function TEManager_ActiveTerrainNameEdit::onValidate( %this )
{
	%newName = TEManager_ActiveTerrainNameEdit.getText();

	if (%newName $= TEManager.activeTerrain.getName())
	{
		TEManager_ActiveTerrainNameApply.active = 0;
		return;
	}

	TEManager_ActiveTerrainNameApply.active = 1;
}
//------------------------------------------------------------------------------
//==============================================================================
// Manage the terrain file
//==============================================================================

//==============================================================================
// Sync the current profile values into the params objects
function TEManager::relocateTerrainFile( %this )
{
	%newFile = TEManager_ActiveTerrainFileEdit.getText();

	if (%newFile $= TEManager.activeTerrain.terrainFile)
	{
		TEManager_ActiveTerrainFileApply.active = 0;
		return;
	}

	%fileBase = fileBase(%newFile);
	%filePath = filePath(%newFile);
	%file = %filePath@"/"@%fileBase@".ter";
	TEManager.saveTerrain(TEManager.activeTerrain,%file);
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager::saveTerrain(%this,%obj,%file)
{
	if (%obj $= "")
		%obj = TEManager.activeTerrain;

	if (!isObject(%obj))
		return;

	if (%file $= "")
		%file = addFilenameToPath(filePath(MissionGroup.getFileName()),%obj.getName(),"ter");

	%obj.save(%file);
	TEManager.schedule(500,"updateTerrainFile",%obj,%file);
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager::updateTerrainFile(%this,%obj,%file)
{
	if (!isObject(%obj))
		return;

	%obj.setFieldValue("terrainFile",%file);
}
//------------------------------------------------------------------------------
//==============================================================================
// Sync the current profile values into the params objects
function TEManager::getTerrainFile( %this )
{
	%currentFile = TEManager.activeTerrain.terrainFile;
	//Canvas.cursorOff();
	getLoadFilename("*.*|*.*", "TEManager.setTerrainFile", %currentFile);
}
//------------------------------------------------------------------------------
//==============================================================================
// Sync the current profile values into the params objects
function TEManager::setTerrainFile( %this,%file )
{
	%fileBase = fileBase(%file);
	%filePath = filePath(%file);
	%newFile = %filePath@"/"@%fileBase@".ter";
	%filename = makeRelativePath( %newFile, getMainDotCsDir() );
	TEManager.saveTerrain(TEManager.activeTerrain, %filename);
	//%this.updateTerrainField("terrainFile",%filename);
}
//------------------------------------------------------------------------------

//==============================================================================
function TEManager::updateTerrainField(%this,%field,%value)
{
	%terrain = TEManager.activeTerrain;
	%terrain.setFieldValue(%field,%value);
}
//------------------------------------------------------------------------------

//==============================================================================
// Terrain Layers Functions
//==============================================================================
//==============================================================================
function TEManager::updateTerrainLayers(%this,%clearOnly)
{
	TEManager_TerrainLayerStack.clear();
	hide(TEManager_TerrainLayerPill);
	show(TEManager_TerrainLayerStack);

	if (%clearOnly)
		return;

	%mats = ETerrainEditor.getMaterials();

	for( %i = 0; %i < getRecordCount( %mats ); %i++ )
	{
		%matInternalName = getRecord( %mats, %i );
		%mat = TerrainMaterialSet.findObjectByInternalName( %matInternalName );
		%pill = cloneObject(TEManager_TerrainLayerPill);
		%pill.matObj = %mat;
		%pill.layerId = %i;
		%pill.internalName = "Layer_"@%i;
		%pill-->materialName.text = "Material:\c1" SPC %mat.internalName;
		%pill-->materialMouse.pill = %pill;
		%pill-->materialMouse.superClass = "TEManager_GeneralMaterialMouse";
		%pill-->materialMouse.callback = "TEManager_GeneralMaterialChangeCallback";
		TEManager_TerrainLayerStack.add(%pill);
	}
}
//------------------------------------------------------------------------------
function TEManager_GeneralMaterialMouse::onMouseDown(%this,%modifier,%mousePoint,%mouseClickCount)
{
	if (%mouseClickCount > 1)
	{
		TEManager.showGeneralMaterialDlg(%this.pill);
	}
}
//==============================================================================
function TEManager::showGeneralMaterialDlg( %this,%pill )
{
	if (!isObject(%pill))
	{
		warnLog("Invalid layer to change material");
		return;
	}

	if (%callback $= "")
		%callback = "TEManager_LayerMaterialChangeCallback";

	%mat = %pill.matObj;
	TEManager.changeMaterialPill = %pill;
	TEManager.changeMaterialLive = %directUpdate;
	TerrainMaterialDlg.show( %pill.layerId, %mat,TEManager_GeneralMaterialChangeCallback );
}
//------------------------------------------------------------------------------
//==============================================================================
// Callback from TerrainMaterialDlg returning selected material info
function TEManager_GeneralMaterialChangeCallback( %mat, %matIndex, %activeIdx )
{
	TEManager_LayerMaterialChangeCallback(%mat, %matIndex, %activeIdx );
	EPainter_TerrainMaterialUpdateCallback(%mat, %matIndex);
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager::importLayerTextureMap(%this,%matInternalName)
{
}
//------------------------------------------------------------------------------
//==============================================================================
function TEManager::exportLayerTextureMap(%this,%matInternalName)
{
}
//------------------------------------------------------------------------------

//==============================================================================
// TEManager.getTerrainHeightRange
function TEManager::getTerrainHeightRange( %this )
{
	%heightRange = ETerrainEditor.getHeightRange();
	return %heightRange;
}
//------------------------------------------------------------------------------
//==============================================================================
/*
new TerrainBlock(TerrainTile_x0y0) {
         terrainFile = "art/Levels/Demo/MiniTerrain/MiniTerrainDemo.ter";
         castShadows = "1";
         squareSize = "2";
         baseTexSize = "256";
         baseTexFormat = "JPG";
         lightMapSize = "256";
         screenError = "16";
         position = "-256 -256 0";
         rotation = "1 0 0 0";
         canSave = "1";
         canSaveDynamicFields = "1";
            scale = "1 1 1";
            tile = "0";
      };
*/
