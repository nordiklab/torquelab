//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//------------------------------------------------------------------------------

function TerrainManager::create( %this )
{
    info("Tool","->","Initializing","Terrain Manager");
}

//------------------------------------------------------------------------------

function TerrainManager::destroy( %this )
{
   
}

//------------------------------------------------------------------------------
