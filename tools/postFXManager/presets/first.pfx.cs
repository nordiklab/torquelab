$PostFXManager::Settings::CA::colorDistortionFactor = 2.40732;
$PostFXManager::Settings::CA::cubeDistortionFactor = 3.18537;
$PostFXManager::Settings::CA::distCoeffecient = 4.25854;
$PostFXManager::Settings::CA::enable = "0";
$PostFXManager::Settings::ColorCorrectionRamp = "core/scripts/client/postFx/null_color_ramp.png";
$PostFXManager::Settings::DOF::BlurCurveFar = "0.712737";
$PostFXManager::Settings::DOF::BlurCurveNear = "-0.823848";
$PostFXManager::Settings::DOF::BlurMax = "0.799458";
$PostFXManager::Settings::DOF::BlurMin = "0.471545";
$PostFXManager::Settings::DOF::EnableAutoFocus = "0";
$PostFXManager::Settings::DOF::EnableDOF = "0";
$PostFXManager::Settings::DOF::FocusRangeMax = "9.21409";
$PostFXManager::Settings::DOF::FocusRangeMin = "3.52304";
$PostFXManager::Settings::EnableDOF = "0";
$PostFXManager::Settings::EnabledSSAO = "0";
$PostFXManager::Settings::EnableHDR = "1";
$PostFXManager::Settings::EnableLightRays = "1";
$PostFXManager::Settings::EnablePostFX = 1;
$PostFXManager::Settings::EnableVignette = "0";
$PostFXManager::Settings::HDR::adaptRate = "4.84737";
$PostFXManager::Settings::HDR::blueShiftColor = "0.00392157 0.415686 0.968628 1";
$PostFXManager::Settings::HDR::brightPassThreshold = "3.62857";
$PostFXManager::Settings::HDR::enableBloom = "1";
$PostFXManager::Settings::HDR::enableBlueShift = "0.0793651";
$PostFXManager::Settings::HDR::enableToneMapping = "0.333333";
$PostFXManager::Settings::HDR::gaussMean = "0.811429";
$PostFXManager::Settings::HDR::gaussMultiplier = "2.54286";
$PostFXManager::Settings::HDR::gaussStdDev = "1.44";
$PostFXManager::Settings::HDR::keyValue = "0.245614";
$PostFXManager::Settings::HDR::minLuminace = "0.631579";
$PostFXManager::Settings::HDR::whiteCutoff = "0.134503";
$PostFXManager::Settings::LightRays::brightScalar = "0.75";
$PostFXManager::Settings::LightRays::decay = "1.0";
$PostFXManager::Settings::LightRays::decay_hi = 0.9;
$PostFXManager::Settings::LightRays::density = "0.94";
$PostFXManager::Settings::LightRays::enableLightRays = "1";
$PostFXManager::Settings::LightRays::exposure = 0.01;
$PostFXManager::Settings::LightRays::numSamples = "40";
$PostFXManager::Settings::LightRays::resolutionScale = 0.1;
$PostFXManager::Settings::LightRays::weight = "5.65";
$PostFXManager::Settings::SSAO::blurDepthTol = "0.001";
$PostFXManager::Settings::SSAO::blurNormalTol = "0.95";
$PostFXManager::Settings::SSAO::enableSSAO = "0";
$PostFXManager::Settings::SSAO::lDepthMax = "2";
$PostFXManager::Settings::SSAO::lDepthMin = "0.2";
$PostFXManager::Settings::SSAO::lDepthPow = "0.2";
$PostFXManager::Settings::SSAO::lNormalPow = "2";
$PostFXManager::Settings::SSAO::lNormalTol = "-0.5";
$PostFXManager::Settings::SSAO::lRadius = "1";
$PostFXManager::Settings::SSAO::lStrength = "10";
$PostFXManager::Settings::SSAO::overallStrength = "11.2466";
$PostFXManager::Settings::SSAO::quality = "2";
$PostFXManager::Settings::SSAO::sDepthMax = "1";
$PostFXManager::Settings::SSAO::sDepthMin = "0.1";
$PostFXManager::Settings::SSAO::sDepthPow = "1";
$PostFXManager::Settings::SSAO::sNormalPow = "1";
$PostFXManager::Settings::SSAO::sNormalTol = "0";
$PostFXManager::Settings::SSAO::sRadius = "0.1";
$PostFXManager::Settings::SSAO::sStrength = "6";
$PostFXManager::Settings::SSAO::targetScale = 0;
$PostFXManager::Settings::Vignette::enableVignette = "0";
$PostFXManager::Settings::Vignette::VMax = "0.6";
$PostFXManager::Settings::Vignette::VMin = 0.1;
$PostFXManager::Settings::VolFogGlow::enable = "0";
$PostFXManager::Settings::VolFogGlow::glowStrength = "5.35854";
