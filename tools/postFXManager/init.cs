//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function PostFXTool::create(%this)
{
   info("Tool","->","Initializing","PostFX Tool");
   execPattern("tlab/tools/PostFXManager/*.cs");
     execGui("tlab/tools/PostFXManager/EPostFxManager.gui");
 
}
function PostFXTool::destroy(%this)
{
 
}
