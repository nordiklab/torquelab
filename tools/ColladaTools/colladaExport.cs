//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function EditorExportToCollada()
{
	if (!$Pref::disableSaving )
	{
		%dlg = new SaveFileDialog()
		{
			Filters        = "COLLADA Files (*.dae)|*.dae|";
			DefaultPath    = $Pref::WorldEditor::LastPath;
			DefaultFile    = "";
			ChangePath     = false;
			OverwritePrompt   = true;
		};
		%ret = %dlg.Execute();

		if (%ret)
		{
			$Pref::WorldEditor::LastPath = filePath(%dlg.FileName);
			%exportFile = %dlg.FileName;
		}

		if (fileExt(%exportFile) !$= ".dae")
			%exportFile = %exportFile @ ".dae";

		%dlg.delete();

		if (!%ret)
			return;

		if (Lab.currentEditor.getId() == ShapeLabPlugin.getId())
			ShapeLabEditor.exportToCollada(%exportFile);
		else
			EWorldEditor.colladaExportSelection(%exportFile);
	}
}
//------------------------------------------------------------------------------
//==============================================================================

function Lab::CreateColladaClone(%this)
{
	//Get first object in selection which have a file

	%objId = 0;

	for(%i=EWorldEditor.getSelectionSize()-1; %i>=0; %i--)
	{
		%obj = EWorldEditor.getSelectedObject(%i);

		if (%obj.shapeName $= "")
			continue;

		%baseFile = %obj.shapeName;
		break;
	}

	if (%baseFile $= "")
	{
		%dlg = new SaveFileDialog()
		{
			Filters        = "COLLADA Clone (*.cc.dae)|*.cc.dae|";
			DefaultPath    = $Pref::WorldEditor::LastPath;
			DefaultFile    = "";
			ChangePath     = false;
			OverwritePrompt   = true;
		};
		%ret = %dlg.Execute();

		if (%ret)
		{
			$Pref::WorldEditor::LastPath = filePath(%dlg.FileName);
			%baseFile = %dlg.FileName;
		}

		%dlg.delete();

		if (!%ret)
			return;
	}

	%basePath = filePath(%baseFile);
	%baseName = fileBase(fileBase(%baseFile));
	%baseFile = %basePath@"/"@%baseName@".cc.DAE";

	%fixFile = getUniqueFilename(%baseFile);
	EWorldEditor.colladaExportSelection(%fixFile);

}

//------------------------------------------------------------------------------
