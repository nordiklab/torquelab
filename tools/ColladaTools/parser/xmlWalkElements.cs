//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function execDAEParser()
{
	execPattern("tlab/tools/ColladaTools/parser/*.cs");
}
//==============================================================================
// Walk thorugh all element and check for attrib to call an eval on
function DAEParser::walkElementAttribs(%xml,%elements)
{
	%id = -1;

	while(%xml.pushChildElement(%id++))
	{
		%element = %xml.elementValue();

		if (%element !$= "" && strFindWords(%element,%elements))
		{
			%attrib = %xml.firstAttribute();

			while( %attrib !$= "")
			{
				if (%xml.evalAtt[%attrib] !$= "")
					eval("%xml."@%xml.evalAtt[%attrib]);

				%attrib = %xml.nextAttribute();
			}
		}

		%xml.walkElementAttribs(%elements);
		%xml.popElement();
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// OLD Experiments
//==============================================================================

//==============================================================================
// Walk thorugh all element and validate specified data
function walkdae(%xml)
{
	%id = -1;

	while(%xml.pushChildElement(%id++))
	{
		%element = %xml.elementValue();

		if (%element !$= "")
		{
			%next = %xml.firstAttribute();

			while( %next !$= "")
			{
				%value = %xml.attribute(%next);

				if ( %xml.replaceAtt[%element,%next] !$= "")
				{
					%search = getField(%xml.replaceAtt[%element,%next],0);
					%replace = getField(%xml.replaceAtt[%element,%next],1);
					%newvalue = strreplace(%value,%search,%replace);

					if (%value !$= %newvalue)
					{
						info(%element,%next,"Fixed from",%value,"To",%newvalue);
						%xml.setAttribute(%next,%newvalue);
					}

					if (%next $= "name" && %xml.nodeData[%value] !$= "")
					{
						%newElement = firstWord(%xml.nodeData[%value]);
						%data = restWords(%xml.nodeData[%value]);
						%xml.pushNewElement(%newElement);
						%xml.addData(%data);
						%xml.popElement();

					}
				}

				%next = %xml.nextAttribute();

			}

		}

		walkdae(%xml);
		%xml.popElement();
	}

}
//------------------------------------------------------------------------------

//==============================================================================
// daePatternParser("art/models/buildings/blocks/misc/","fixdaename");
function daePatternParser(%folder,%func)
{
	%pattern = strreplace(%folder@"/*.dae","//","/");

	if (!isFunction(%func))
		return;

	for(%file = findFirstFile(%pattern); %file !$= ""; %file = findNextFile(%pattern))
		call(%func,%file);
}
//------------------------------------------------------------------------------
