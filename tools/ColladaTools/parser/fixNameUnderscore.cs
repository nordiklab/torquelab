//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// By default underscore are interpreted as - causing a collision type detailSize.
// The reason is because Maya seem to use underscore instead of - (unconfirmed but
// the code suggest that behavior, this script will replace all underscore in node
// names with empty space so the trailingNumber is interpreted correctly
//==============================================================================

//==============================================================================
function fixdaename(%filename,%reloadImporter)
{
	if (%reloadImporter && !isFile(%filename))
		%filename = ColladaImportDlg.path;

	if ($cfg_ColladaImport_lodType $= "SingleSize")
		return;

	//Backup the file in user documents backup
	Lab.backupFileUser(%filename);

	%xml = new SimXMLDocument() {};
	%xml.superClass = "DAEParser";
	%xml.loadFile(%filename);

	%xml.replaceAtt["node","name"] = "_" TAB "";

	%xml.evalAtt[name] = "fixTrailingUnderscore(node,name);";

	%xml.pushFirstChildElement("COLLADA");
	%xml.pushFirstChildElement("library_visual_scenes");
	%xml.pushFirstChildElement("visual_scene");
	%xml.walkElementAttribs("node");
	%xml.popElement();
	%xml.popElement();
	%xml.popElement();

	%xml.saveFile(%filename);

	if (%reloadImporter)
	{
		ColladaImportDlg.reload();
	}

}
//------------------------------------------------------------------------------

//==============================================================================
function DAEParser::fixTrailingUnderscore(%xml,%node,%name)
{
	%value = %xml.attribute("name");
	%newvalue = strreplace(%value,"_","");
	%lastUnderPos = strposr(%value,"_");

	if (%lastUnderPos $= "-1")
		return;

	%afterUnderStr = getSubStr(%value, %lastUnderPos + 1);

	if (!strIsNumeric(%afterUnderStr))
		return;

	%newvalue = getSubStr(%value, 0,%lastUnderPos) @ %afterUnderStr;

	if (%value !$= %newvalue)
	{
		info(%xml.elementValue(),"name","Fixed from",%value,"To",%newvalue);
		%xml.setAttribute("name",%newvalue);
	}
}

//==============================================================================
function updateDaeNodes(%filename)
{
	//Backup the file in user documents backup
	Lab.backupFileUser(%filename);

	%xml = new SimXMLDocument() {};
	%xml.loadFile(%filename);

	%xml.replaceAtt["node","name"] = "_" TAB "";

	%xml.nodeData["start01"] = "translate 0.06454682 0.2214611 0.04716256";
	%xml.nodeData["misc"] = "rotate -0.07858581 -0.9938054 0.07858186 -90.35607";
	%xml.pushFirstChildElement("COLLADA");
	%xml.pushFirstChildElement("library_visual_scenes");
	%xml.pushFirstChildElement("visual_scene");

	walkdae(%xml);
	%xml.popElement();
	%xml.popElement();
	%xml.popElement();

	%xml.saveFile(%filename);

}
//------------------------------------------------------------------------------
//==============================================================================
// updateDaeMaterials("art/models/chronos_dev/station/floors/osFloor01_4x2AA.DAE")
function updateDaeMaterials(%filename)
{
	//Backup the file in user documents backup
	Lab.backupFileUser(%filename);

	%xml = new SimXMLDocument() {};
	%xml.loadFile(%filename);

	%xml.replaceAtt["node","name"] = "_" TAB "";

	%xml.nodeData["start01"] = "translate 0.06454682 0.2214611 0.04716256";
	%xml.nodeData["misc"] = "rotate -0.07858581 -0.9938054 0.07858186 -90.35607";
	%xml.pushFirstChildElement("COLLADA");
	%xml.pushFirstChildElement("library_materials");
	%id = -1;

	while(%xml.pushChildElement(%id++))
	{
		%element = %xml.elementValue();

		if (%element !$= "")
		{
			%value = %xml.attribute("name");

			if (startsWith(%value,"ColorEffect"))
			{
				%xml.removeText();
				%xml.setAttribute("name","");
				%xml.setAttribute("id","");
			}

		}

		%xml.popElement();
	}

	%xml.popElement();
	%xml.popElement();

	%xml.saveFile(%filename);

}
//------------------------------------------------------------------------------
// fixDaeMaterials("art/models/chronos_dev/station/floors/osFloor01_4x2AA.DAE")
function fixDaeMaterials(%filename)
{
	
	fileLineRemover(%filename,"material id=\"ColorEffect" TAB "3");
	
}
//------------------------------------------------------------------------------
