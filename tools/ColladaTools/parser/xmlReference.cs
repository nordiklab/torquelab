//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// DAE Dumping Experiments - For learning as a FileObject System would work better
//==============================================================================
//==============================================================================
function xmlStat(%xml,%pad)
{
	%xml.pad = %pad;

	if (%xml.elementValue() !$= "")
		info(%xml.pad,"<>",%xml.elementValue());

	if (%xml.getText() !$= "")
		info(%xml.pad @ "  ",%xml.getText());

	xmlListAtt(%xml,%pad);

}
//------------------------------------------------------------------------------
//==============================================================================
function xmlListAtt(%xml,%pad)
{
	%xml.pad = %pad @ "   :";
	%next = %xml.firstAttribute();

	while( %next !$= "")
	{
		info(%xml.pad,%next,"=",%xml.attribute(%next));
		%xml.setAttribute(%next,"NEW"@%xml.attribute(%next));
		%next = %xml.nextAttribute();

	}

}
//------------------------------------------------------------------------------
//==============================================================================
function dumpxml(%file,%save)
{
	%xml = new SimXMLDocument() {};
	%xml.pad = "";
	%xml.loadFile(%file);
	%xml.pushFirstChildElement("COLLADA");
	%xml.pushFirstChildElement("asset");
	dumpxmlnode(%xml);

	if(%save)
		%xml.saveFile(renameFile(%file,"tmp_"));
}
//------------------------------------------------------------------------------
//==============================================================================
function dumpxmlnode(%xml,%tab)
{
	%id = -1;

	while(%xml.pushChildElement(%id++))
	{
		xmlStat(%xml,%tab);
		dumpxmlnode(%xml,%tab @ "  ");

		%xml.popElement();
	}

	//%xml.pad = getSubStr(%xml.pad,0,strlen(%xml.pad)-2);
}
//------------------------------------------------------------------------------

