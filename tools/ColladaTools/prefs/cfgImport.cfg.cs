$cfg_ColladaImport_adjustCenter = "0";
$cfg_ColladaImport_adjustFloor = "1";
$cfg_ColladaImport_alwaysImport = "an";
$cfg_ColladaImport_alwaysImportMesh = "am";
$cfg_ColladaImport_forceUpdateMaterials = "1";
$cfg_ColladaImport_ignoreNodeScale = "1";
$cfg_ColladaImport_loadLights = "0";
$cfg_ColladaImport_lodType = "SingleSize";
$cfg_ColladaImport_materialPrefix = "mat!";
$cfg_ColladaImport_neverImport = "in";
$cfg_ColladaImport_neverImportMesh = "im";
$cfg_ColladaImport_overrideScale = "1";
$cfg_ColladaImport_overrideUpAxis = "1";
$cfg_ColladaImport_scale = "1";
$cfg_ColladaImport_singleDetailSize = "2";
$cfg_ColladaImport_upAxis = "Z_AXIS";
$cfg_ColladaImport_allowPlayerStep = "1";

