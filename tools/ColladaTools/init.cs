//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function ColladaTools::create( %this )
{
   info("Tool","->","Initializing","Collada Tools");
       execGuiDir("tlab/tools/ColladaTools/",true);
		execPattern("tlab/tools/ColladaTools/*.cs");
}
function ColladaTools::destroy( %this )
{
   
}
