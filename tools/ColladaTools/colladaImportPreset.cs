//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function ColladaImportDlg::readDtsConfig(%this)
{
	%filename = filePath(%this.path) @ "/" @ fileBase(%this.path) @ ".cfg.cs";

	if (!isFile(%filename))
	{
		warnLog("No import config for this shape");
		return;
	}

	if (!isFile(%filename))
		return;

	exec(%filename);
	%this.assignCfg();
	$ColladaImporterPreset = "Shape Config";
	ColladaImport_PresetName.setText($ColladaImporterPreset);
}
function ColladaImportDlg::writeDtsConfig(%this)
{
	%filename = filePath(%this.path) @ "/" @ fileBase(%this.path) @ ".cfg.cs";
	ColladaImportDlg.writeConfig(%filename);
	%this-->shapeConfigButton.active = true;
}
function ColladaImportDlg::defaultConfig(%this)
{

	%file = strreplace($ColladaImportConfigDefault,"default",$ColladaImporterPreset);

	if (!isFile(%file))
		return;

	exec(%file);
	%this.assignCfg();

}
function ColladaImportDlg::readConfig(%this,%filename)
{
	if (%filename $= "")
	{
		if ($ColladaImporterPreset $= "Shape Config")
			return;

		%presetName = ColladaImport_PresetName.getText();

		%filename = strreplace($ColladaImportConfigDefault,"default",%presetName);
	}

	if (!isFile(%filename))
		return;

	exec(%filename);
	%this.assignCfg();

	%activePreset = strreplace(fileBase(%filename),".cfg","");
	$ColladaImporterPreset = %activePreset;
	ColladaImport_PresetName.setText(%activePreset);
	ColladaImport_PresetMenu.setText("");

}
function ColladaImportDlg::writeConfig(%this,%filename)
{
	if (%filename $= "")
	{
		if ($ColladaImporterPreset $= "Shape Config")
			return;

		%presetName = ColladaImport_PresetName.getText();
		%filename = strreplace($ColladaImportConfigDefault,"default",%presetName);

	}
	else
		%presetName = "Shape Config";

	%this.updateCfg();

	if (strFind(%fileName,"Shape Config"))
		return;

	$ColladaImporterPreset = %presetName;
	ColladaImport_PresetName.setText(%presetName);
	export("$cfg_ColladaImport_*", %filename, false);
	%this.getPresets();
}
function ColladaImportDlg::assignCfg(%this)
{
	foreach$(%field in $ColladaImportFields)
	{
		%ctrl = %this.findObjectByInternalName(%field,1);

		if (!isObject(%ctrl))
			continue;

		%value =   $cfg_ColladaImport_[%field];
		%ctrl.setTypeValue(%value);
	}
}
function ColladaImportDlg::updateCfg(%this)
{
	foreach$(%field in $ColladaImportFields)
	{
		%ctrl = %this.findObjectByInternalName(%field,1);

		if (!isObject(%ctrl))
			continue;

		%value =  %ctrl.getTypeValue();
		$cfg_ColladaImport_[%field] = %value;
	}
}

function ColladaImportDlg::updateLastCfg(%this)
{
	%this.updateCfg();
	%filename = strreplace($ColladaImportConfigDefault,"default","last_used");
	export("$cfg_ColladaImport_*", %filename, false);
}

//==============================================================================
//SideBarVIS.getPresets
function ColladaImportDlg::getPresets(%this)
{

	ColladaImport_PresetMenu.clear();
	ColladaImport_PresetMenu.add("Select a preset",0);
	%pid = 0;
	%selected ="Select a preset";

	if ($ColladaImporterPreset $= "Shape Config")
		%selected = $ColladaImporterPreset;

	%searchFolder = filePath($ColladaImportConfigDefault)@"/*.cfg.cs";

	for(%presetFile = findFirstFile(%searchFolder); %presetFile !$= ""; %presetFile = findNextFile(%searchFolder))
	{
		%presetName = strreplace(fileBase(%presetFile),".cfg","");

		if ($ColladaImporterPreset $= %presetName)
			%selected = %presetName;

		ColladaImport_PresetMenu.add(%presetName,%pid++);

	}

	%filename = filePath(%this.path) @ "/" @ fileBase(%this.path) @ ".cfg.cs";

	if (isFile(%filename))
		ColladaImport_PresetMenu.add("Shape Config",%pid++);

	ColladaImport_PresetName.setText(%selected);
}
//------------------------------------------------------------------------------

//==============================================================================
function ColladaImport_PresetMenu::onSelect(%this,%id,%text)
{
	if (%id $= "0")
		return;

	if (%text $= "Shape Config")
	{
		ColladaImportDlg.readDtsConfig();
		return;
	}

	%filename = strreplace($ColladaImportConfigDefault,"default",%text);

	ColladaImportDlg.readConfig(%filename);
}
//------------------------------------------------------------------------------
