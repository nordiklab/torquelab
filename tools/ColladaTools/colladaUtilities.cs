//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
//(Unused)Convert all COLLADA models that match the given pattern (defaults to *) to DTS
function convertColladaModels(%pattern)
{
	// Force loading the COLLADA file (to ensure cached DTS is updated)
	$collada::forceLoadDAE = true;
	%fullPath = findFirstFile("*.dae");

	while(%fullPath !$= "")
	{
		// Check if this file is inside the given path
		%fullPath = makeRelativePath(%fullPath, getMainDotCSDir());

		if ((%pattern $= "") || strIsMatchMultipleExpr(%pattern, %fullPath))
		{
			// Load the model by creating a temporary TSStatic
			echo("Converting " @ %fullPath @ " to DTS...");
			%temp = new TSStatic()
			{
				shapeName = %fullPath;
				collisionType = "None";
			};
			%temp.delete();
		}

		%fullPath = findNextFile("*.dae");
	}

	$collada::forceLoadDAE = false;
}
//------------------------------------------------------------------------------
