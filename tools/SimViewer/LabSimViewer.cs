//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function LabSimViewer::onWake(%this,%group)
{
	LabToolbarStack-->SimViewer.setStateOn(true);

}
//------------------------------------------------------------------------------
//==============================================================================
function LabSimViewer::OnSleep(%this,%group)
{
	LabToolbarStack-->SimViewer.setStateOn(false);
}
//------------------------------------------------------------------------------
//==============================================================================
function LabSimViewer::toggle(%this,%group)
{
	if (LabSimViewer.isAwake())
		popDlg(LabSimViewer);
	else
		LabSimViewer.open();

}
//------------------------------------------------------------------------------

//==============================================================================
function LabSimViewer::open(%this,%group)
{
	if (!isObject(SView))
		new ScriptObject(SView);

	Canvas.popDialog("LabSimViewer");
	Canvas.pushDialog("LabSimViewer", 20);
	//if (!%this.isAwake())
	//popDlg(%this);

	if (isObject(%group))
		%viewGroup = %group;
	else
		%viewGroup = RootGroup;

	SView.initialGroup = %viewGroup;

	SView_TreeInspector.open(%viewGroup);

}
//------------------------------------------------------------------------------

//==============================================================================
function SView::loadParentTree(%this)
{
	if (!isObject(SView_InspectObjectName.refObj))
		return;

	%parent = SView_InspectObjectName.refObj.parentGroup;

	if (!isObject(%parent))
		return;

	SView.tree(%parent);

}
//------------------------------------------------------------------------------

//==============================================================================
function SView::Inspect(%this,%obj)
{
	// Don't inspect the root group.
	if (%obj == -1)
		return;

	SimViewInspectButton.visible = 0;

	if (%obj.isMemberOfClass("SimSet"))
		SimViewInspectButton.visible = 1;

	LabInspect.inspect(%obj);

	// Update selected object properties
	SView_InspectObjectName.setValue(%obj.getName());
	SView_InspectObjectInternal.setValue(%obj.getInternalName());
	SView_InspectObjectID.setValue(%obj.getId());

	// Store Object Reference
	SView_InspectObjectName.refObj = %obj;

}
//------------------------------------------------------------------------------
//==============================================================================
function SView::OpenSelGroup(%this)
{
	%group = SView_InspectObjectName.refObj;

	if (!%group.isMemberOfClass("SimSet"))
	{
		SimViewInspectButton.visible = 0;
		return;
	}

	LabSimViewer.open(%group);

}
//------------------------------------------------------------------------------
//==============================================================================
function SView::InspectApply(%this)
{
	%obj = SView_InspectObjectName.refObj;

	if (!isObject(%obj))
		return;

	// Update name and internal name
	%obj.setName(SView_InspectObjectName.getValue());
	%obj.setInternalName(SView_InspectObjectInternal.getValue());

	// Update inspected object information.
	LabInspect.inspect(%obj);
}
//------------------------------------------------------------------------------
//==============================================================================
function SView::InspectDelete(%this)
{
	%obj = SView_InspectObjectName.refObj;

	if (!isObject(%obj))
		return;

	%obj.delete();

	// Update inspected object information.
	LabInspect.inspect(0);

	// Update selected object properties
	SView_InspectObjectName.setValue("");
	SView_InspectObjectInternal.setValue("");
	SView_InspectObjectID.setValue(0);

}

//------------------------------------------------------------------------------
//==============================================================================
function SView_TreeInspector::onSelect(%this, %obj)
{
	SView.Inspect(%obj);
}
//------------------------------------------------------------------------------
//==============================================================================
function SView::Tree(%this,%obj)
{
	LabSimViewer.open(%obj);
	return;
	Canvas.popDialog("LabSimViewer");
	Canvas.pushDialog("LabSimViewer", 20);
	SView_TreeInspector.open(%obj);
}
//------------------------------------------------------------------------------
//==============================================================================
// MM: Added Dynamic group toggle support.
function GuiInspector::toggleDynamicGroupScript(%this, %obj)
{
	%this.toggleDynamicGroupExpand();
	%this.inspect(%obj);
}
// MM: Added group toggle support.
function GuiInspector::toggleGroupScript(%this, %obj, %fieldName)
{
	%this.toggleGroupExpand(%obj, %fieldName);
	%this.inspect(%obj);
}

// MM: Set All Group State support.
function GuiInspector::setAllGroupStateScript(%this, %obj, %groupState)
{
	%this.setAllGroupState(%groupState);
	%this.inspect(%obj);
}

function GuiInspector::ignoreGroups(%this, %groups)
{
	foreach$(%group in %groups)
	{
		%this.ignoredGroups = strAddWord(%this.ignoredGroups,%group,1);
		%this.listedGroups = strRemoveWord(%this.listedGroups,%group);
	}

}
function GuiInspector::listGroups(%this, %groups)
{

	foreach$(%group in %groups)
	{
		%this.listedGroups = strAddWord(%this.listedGroups,%group,1);
		%this.ignoredGroups = strRemoveWord(%this.ignoredGroups,%group);
	}
}

function GuiInspector::filterGroups(%this)
{
	foreach$(%group in  %this.listedGroups)
		%filterStr = strAddWord(%filterStr,"+" @ %group,1);

	foreach$(%group in  %this.ignoredGroups)
		%filterStr = strAddWord(%filterStr,"-" @ %group,1);

	%this.groupFilters = %filterStr;
	info(%this.getName(),"inspector group filters set to",%this.groupFilters);
}
