//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//------------------------------------------------------------------------------

function SimViewer::create( %this )
{
   info("Tool","->","Initializing","Sim Viewer");
}

//------------------------------------------------------------------------------

function SimViewer::destroy( %this )
{

}

//------------------------------------------------------------------------------
