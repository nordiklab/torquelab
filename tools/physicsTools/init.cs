//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//------------------------------------------------------------------------------

function PhysicsTools::create( %this )
{
   info("Tool","->","Initializing","Physics Tools");
        execGuiDir("tlab/tools/PhysicsTools/",true);
		execPattern("tlab/tools/PhysicsTools/*.cs");
		
		initPhysicsTools();
}

//------------------------------------------------------------------------------

function PhysicsTools::destroy( %this )
{
  
}

//------------------------------------------------------------------------------
