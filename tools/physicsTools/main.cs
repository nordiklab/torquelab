//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$TLab_PluginName_["PhysicsTools"] = "Physics Tools";
$TLab_PluginType_["PhysicsTools"] = "Module";
function initPhysicsTools()
{
	if ( !physicsPluginPresent() )
	{
	   if ($LabPhysicDisabled)
	      return;
		$LabPhysicDisabled = true;
		info( "PhysicsTools","->","Physics Tools disabled as no physic plugins detected" );
		LabToolbarStack-->TogglePhysicsTool.visible = 0;
		EPhysicsTools.isDisabled = true;
		return;
	}

	info("Plugin","->","Initializing","Physics Tools" );
	new ScriptObject(PhysicsToolsModule);
	LabToolbarStack-->TogglePhysicsTool.visible = 1;
	EPhysicsTools.isDisabled = false;
	execPhysTools(true);
	$PT = newScriptObject("PT");
	//Lab.createModule("PhysicsTools","Physics Tools");
	Lab.addPluginToolbar("PhysicsTools",PhysicsToolsToolbar);
	EditorMap.bindCmd( keyboard, "alt t", "PT.physicsToggleSimulation();", "" );
	EditorMap.bindCmd( keyboard, "alt r", "PT.physicsRestoreState();", "" );
}
function execPhysTools(%loadGui)
{
	if (!isObject(PhysicsToolsToolbar))
	{
		exec("tlab/tools/physicsTools/gui/PhysicsToolsToolbar.gui");
	}

	exec("tlab/tools/physicsTools/PhysicsToolsPlugin.cs");
	exec("tlab/tools/physicsTools/physicsTools.cs");
}
function destroyPhysicsTools()
{
}

