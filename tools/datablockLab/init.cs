//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//------------------------------------------------------------------------------

function DatablockLab::create( %this )
{
    info("Tool","->","Initializing","Datablock Lab");
  
        execGuiDir("tlab/tools/DatablockLab/",true);
		execPattern("tlab/tools/DatablockLab/*.cs");
		initToolDatablockLab();
}

//------------------------------------------------------------------------------

function DatablockLab::destroy( %this )
{

}

//------------------------------------------------------------------------------
