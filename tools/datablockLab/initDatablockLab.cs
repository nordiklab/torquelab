//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$DBLab_PMLog = 1;
//------------------------------------------------------------------------------
//==============================================================================
// Create the MatLab Object manager and exec the files
function initToolDatablockLab()
{
	
	Lab.newObjLab("DBLab");
	DBLab.logPM = $DBLab_PMLog;

}
//------------------------------------------------------------------------------

//==============================================================================
// Add materials that shouldn't be listed here
//------------------------------------------------------------------------------
if (!isObject(UnlistedMaterials))
{
	new ArrayObject(UnlistedMaterials);
	UnlistedMaterials.add("unlistedMaterials", WarningMaterial);
	UnlistedMaterials.add("unlistedMaterials", materialEd_previewMaterial);
	UnlistedMaterials.add("unlistedMaterials", notDirtyMaterial);
	UnlistedMaterials.add("unlistedMaterials", materialEd_cubemapEd_cubeMapPreview);
	UnlistedMaterials.add("unlistedMaterials", matEdCubeMapPreviewMat);
	UnlistedMaterials.add("unlistedMaterials", materialEd_justAlphaMaterial);
	UnlistedMaterials.add("unlistedMaterials", materialEd_justAlphaShader);
}

//------------------------------------------------------------------------------

