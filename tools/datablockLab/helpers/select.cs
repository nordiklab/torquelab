//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function DBLab::selectDatablock(%this,%db )
{
	%this.selectedDatablock = %db;
}
//------------------------------------------------------------------------------
