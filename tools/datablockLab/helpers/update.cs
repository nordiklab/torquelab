//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function DBLab::changeFile(%this)
{
	%db = DBLab.selectedDatablock;

	if (!isObject(%db))
		return;

	%file = getSaveFile("TorqueScript (*.cs)|*.cs|All Files|*.*",DBLab.selectedDatablock.getFileName());

	if (!%file)
		return;

	%this.setDirty(%db,%file);
}
//------------------------------------------------------------------------------
