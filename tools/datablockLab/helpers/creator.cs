//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function DBLab::createDatablock(%this,%name, %copySource )
{
	if (!isObject(%copySource))
		return false;

	%class = %copySource.getClassName();

	if ( %copySource $= "" )
		%dbType = "singleton ";
	else
		%dbType = "datablock ";

	%eval = %dbType @ %class @ "(" @ %name @ ") { canSaveDynamicFields = \"1\"; };";
	%res = eval( %eval );

	//Using deepclone create a singleton so we simply assign fields from source
	%name.assignFieldsFrom(%copySource);
	%name.setFileName(%copySource.getFileName());

	%this.setDirty(%name,true);
	return %name;
}
//------------------------------------------------------------------------------
//==============================================================================
function DBLab::cloneDatablock(%this )
{
	%copySource = SceneEd.selectedDatablock;

	if (!isObject(%copySource))
		return false;

	%name = getUniqueName(%copySource.getName());
	%newDB = %this.createDatablock(%name,%copySource);
	return %newDB;
}
//------------------------------------------------------------------------------
