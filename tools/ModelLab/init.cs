//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//------------------------------------------------------------------------------

function ModelLab::create( %this )
{
  info("Tool","->","Initializing","Model Lab");
     execGuiDir("tlab/tools/ModelLab/",true);
		execPattern("tlab/tools/ModelLab/*.cs");
		
		initToolModelLab();
}

//------------------------------------------------------------------------------

function ModelLab::destroy( %this )
{
  
}

//------------------------------------------------------------------------------
