//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$TSConstructorDefault_AllowPlayerStep = true;
function ModelLab::initActions( %this )
{
	Lab.setEvent("MLSequenceAdded");
	Lab.setEvent("MLSequenceRemoved");
}

//==============================================================================

function ModelLab::createAction( %this, %class, %desc )
{
	pushInstantGroup();
	%action = new UndoScriptAction()
	{
		class = %class;
		superClass = BaseShapeLabAction;
		actionName = %desc;
		done = 0;
	};
	popInstantGroup();

	//Create an event

	return %action;
}

function ModelLab::doAction( %this, %action )
{
	//Execute the action and return true if success
	if ( %action.doit() )
	{
		ModelLab.setDirty( true );
		%action.addToManager( ShapeLabUndoManager );
	}
	else
	{
		LabMsgOK( "Error", %action.actionName SPC "failed. Check the console for error messages.", "" );
	}
}

function BaseModelLabAction::redo( %this )
{
	// Default redo action is the same as the doit action
	if ( %this.doit() )
	{
		ModelLab.setDirty( true );
	}
	else
	{
		LabMsgOK( "Error", "Redo" SPC %this.actionName SPC "failed. Check the console for error messages.", "" );
	}
}

function BaseModelLabAction::undo( %this )
{
	ModelLab.setDirty( true );
}
//------------------------------------------------------------------------------
