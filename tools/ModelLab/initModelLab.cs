//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$ModelLab_PMLog = 1;
//------------------------------------------------------------------------------
//==============================================================================
// Create the MatLab Object manager and exec the files
function initToolModelLab()
{
	
	Lab.newObjLab("ModelLab");
	ModelLab.logPM = $ModelLab_PMLog;

	ModelLab.initActions();
}
//------------------------------------------------------------------------------

//==============================================================================
// Create the MatLab Object manager and exec the files
function ModelLab::setActiveShape(%this,%shape)
{
	%this.shape = %shape;
	info("ModelLab shape set to:",%shape);

}
//------------------------------------------------------------------------------

//==============================================================================
function ModelLab::getUniqueName( %this, %type, %name )
{
	%uniqueName = %name;

	for ( %idx = 1; %idx < 100; %idx++ )
	{
		if ( !%this.nameExists( %type, %uniqueName ) )
			break;

		%uniqueName = %name @ %idx;
	}

	return %uniqueName;
}
//------------------------------------------------------------------------------
//==============================================================================
// Check if the given name already exists
function ModelLab::nameExists( %this, %type, %name )
{
	if ( %this.shape == -1 )
		return false;

	if ( %type $= "node" )
		return ( %this.shape.getNodeIndex( %name ) >= 0 );
	else if ( %type $= "sequence" )
		return ( %this.shape.getSequenceIndex( %name ) >= 0 );
	else if ( %type $= "object" )
		return ( %this.shape.getObjectIndex( %name ) >= 0 );
}
//------------------------------------------------------------------------------
