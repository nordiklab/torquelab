//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Get the constructor for player object (if empty, LocalClient player will be used)
function getPlayerConstructor( %player )
{
	if (!isObject(%player))
		%player = $cl.player;

	return getShapeConstructor(%player);
}
//------------------------------------------------------------------------------
//==============================================================================
// Get the constructor for player object (if empty, LocalClient player will be used)
function TSShapeConstructor::camOffset( %this,%offset )
{
	%this.nodeOffset("cam",%offset);
}
//------------------------------------------------------------------------------

//==============================================================================
// Get the constructor for player object (if empty, LocalClient player will be used)
function TSShapeConstructor::incCamZ( %this,%incZ )
{
	%this.incCam("0 0" SPC %incZ);
}
//------------------------------------------------------------------------------
//==============================================================================
// Get the constructor for player object (if empty, LocalClient player will be used)
function TSShapeConstructor::incCam( %this,%incXYZ )
{
	%offset = VectorAdd( %this.offset,%incXYZ);
	%this.nodeOffset("cam",%offset);
}
//------------------------------------------------------------------------------

//==============================================================================
// Get the constructor for player object (if empty, LocalClient player will be used)
function listPlayerAnims( %this,%incXYZ )
{
	// This array is indexed using the enum values defined in player.h

	info("","");
	// Root is the default animation
	info("root");       // RootAnim,

	// These are selected in the move state based on velocity
	info("run",          "0.0f, 1.0f, 0.0f ");       // RunForwardAnim,
	info("back",         "0.0f,-1.0f, 0.0f ");       // BackBackwardAnim
	info("side",         "-1.0f, 0.0f, 0.0f ");       // SideLeftAnim,
	info("side_right",   "1.0f, 0.0f, 0.0f ");       // SideRightAnim,

	info("sprint_root");
	info("sprint_forward",  "0.0f, 1.0f, 0.0f ");
	info("sprint_backward", "0.0f,-1.0f, 0.0f ");
	info("sprint_side",     "1.0f, 0.0f, 0.0f ");
	info("sprint_right",    "1.0f, 0.0f, 0.0f ");

	info("crouch_root");
	info("crouch_forward",  "0.0f, 1.0f, 0.0f ");
	info("crouch_backward", "0.0f,-1.0f, 0.0f ");
	info("crouch_side",     "1.0f, 0.0f, 0.0f ");
	info("crouch_right",    "1.0f, 0.0f, 0.0f ");

	info("prone_root");
	info("prone_forward",   "0.0f, 1.0f, 0.0f ");
	info("prone_backward",  "0.0f,-1.0f, 0.0f ");

	info("swim_root");
	info("swim_forward",    "0.0f, 1.0f, 0.0f ");
	info("swim_backward",   "0.0f,-1.0f, 0.0f ");
	info("swim_left",       "1.0f, 0.0f, 0.0f ");
	info("swim_right",      "1.0f, 0.0f, 0.0f ");

	// These are set explicitly based on player actions
	info("fall");       // FallAnim
	info("jump");       // JumpAnim
	info("standjump");  // StandJumpAnim
	info("land");       // LandAnim
	info("jet");        // JetAnim
}
//------------------------------------------------------------------------------
