//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$TSConstructorDefault_AllowPlayerStep = true;

//==============================================================================
function getShapeConstructor( %obj )
{
	// Get a TSShapeConstructor for this object (use the ShapeLab
	// utility functions to create one if it does not already exist).
	%shapePath = getObjectShapeFile( %obj );
	%shape = getShapePathConstructor(%shapePath);

	if ( !isObject( %shape ) )
		return;

	%shape.setName(getUniqueName(fileBase(%shapePath)));
	%obj.construct = %shape;
	return %shape;
}
//------------------------------------------------------------------------------
//==============================================================================
function getShapePathConstructor( %shapePath )
{
	// Get a TSShapeConstructor for this object (use the ShapeLab
	// utility functions to create one if it does not already exist).

	%shape = findConstructor( %shapePath );

	if ( !isObject( %shape ) )
		%shape = createConstructor( %shapePath );

	if ( !isObject( %shape ) )
	{
		echo( "Failed to create TSShapeConstructor for " @ %obj.getId() );
		return;
	}

	return %shape;
}
//------------------------------------------------------------------------------
//==============================================================================
// Load up our main GUI which lets us see the game.
function getObjectShapeFile(  %obj )
{
	// Get the path to the shape file used by the given object (not perfect, but
	// works for the vast majority of object types)
	%path = "";

	if ( %obj.isMemberOfClass( "TSStatic" ) )
		%path = %obj.shapeName;
	else if ( %obj.isMemberOfClass( "PhysicsShape" ) )
		%path = %obj.getDataBlock().shapeName;
	else if ( %obj.isMemberOfClass( "GameBase" ) )
		%path = %obj.getDataBlock().shapeFile;

	return %path;
}
//------------------------------------------------------------------------------
//==============================================================================
function findConstructor(  %path )
{
	%count = TSShapeConstructorGroup.getCount();

	for ( %i = 0; %i < %count; %i++ )
	{
		%obj = TSShapeConstructorGroup.getObject( %i );

		if ( %obj.baseShape $= %path )
			return %obj;
	}

	return -1;
}
//------------------------------------------------------------------------------
//==============================================================================
function createConstructor( %path )
{
	%name = strupr( fileBase( %path ) ) @ strupr( getSubStr( fileExt( %path ), 1, 3 ) );
	%name = strreplace( %name, "-", "_" );
	%name = strreplace( %name, ".", "_" );

	if (strIsNumeric(getSubStr(%name,0,1)))
		%name = "TS"@%name;

	%name = getUniqueName( %name );

	return new TSShapeConstructor( %name )
	{
		baseShape = %path;
	};
}
//------------------------------------------------------------------------------
//==============================================================================
function saveConstructor( %constructor, %path )
{
	%savepath = filePath( %constructor.baseShape ) @ "/" @ fileBase( %constructor.baseShape ) @ ".cs";

	if (!isObject(Lab_PM))
		new PersistenceManager( Lab_PM );

	Lab_PM.setDirty( %constructor, %savepath );
	Lab_PM.saveDirtyObject( %constructor );
}
//------------------------------------------------------------------------------
//==============================================================================
// Shape Constructor Functions
//==============================================================================

//==============================================================================
function TSStatic::onAdd( %this,%that )
{
}
//------------------------------------------------------------------------------
//==============================================================================
function checkAutoSkin( %group )
{
	if (%group $= "")
		%group = mgMapModels;

	if (!isObject(%group))
		return;

	foreach(%obj in %group)
	{
		if (%obj.isMemberOfClass("SimSet"))
		{
			checkAutoSkin(%obj);
			continue;
		}

		if (%obj.isMemberOfClass("TSStatic"))
		{
			%skin = AutoSkin(%obj);
		}
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function AutoSkin( %obj )
{
	// Get a TSShapeConstructor for this object (use the ShapeLab
	// utility functions to create one if it does not already exist).
	%shapePath = getObjectShapeFile( %obj );
	%shape = findConstructor( %shapePath );

	if ( !isObject( %shape ) )
		%shape = createConstructor( %shapePath );

	if ( !isObject( %shape ) )
	{
		echo( "Failed to create TSShapeConstructor for " @ %obj.getId() );
		return;
	}

	if (%shape.defaultSkin $="")
		return "";

	%obj.skin = %shape.defaultSkin;
	return %obj.skin;
}
//------------------------------------------------------------------------------
//==============================================================================
function getGroupObjectData( %group )
{
	foreach(%obj in %group)
		getObjectData(%obj);
}
//------------------------------------------------------------------------------
//==============================================================================
function getObjectData( %obj )
{
	if (%obj.refLight !$= "" && !isObject(%obj.refLight))
		%obj.refLight = "";

	%shape = getShapeConstructor(%obj);

	if ( !isObject( %shape ) )
	{
		echo( "Failed to create TSShapeConstructor for " @ %obj.getId() );
		return;
	}

	%dataGroup = "Data";
	%obj.allowPlayerStep = $TSConstructorDefault_AllowPlayerStep;

	if ( %shape.getNodeIndex( %dataGroup ) $= "-1" )
		return;

	if ( %shape.getNodeIndex( "NoWalk" ) !$= "-1" )
	{
		%obj.allowPlayerStep = false;
	}

	%shapeName = fileBase(%obj.shapeName);
	%count = %shape.getNodeChildCount( %dataGroup );

	for ( %i = 0; %i < %count; %i++ )
	{
		// get node transform in object space, then transform to world space
		%child = %shape.getNodeChildName( %dataGroup, %i );
		%txfmA = %shape.getNodeTransform( %child, true );
		%txfm = MatrixMultiply( %objTransform, %txfmA );
		%fields = strreplace(%child,"_","\t");

		if (getFieldCount(%fields) <= 1)
			continue;

		%setting = getField(%fields,0);
		%value = getField(%fields,1);

		if (%setting $= "Skin")
		{
			%obj.skin = %value;
		}
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function addObjectAtShapeNodeChildPos( %addObjFile,%srcObj,%nodeGroup )
{
	%shape = getShapeConstructor(%srcObj);

	if ( !isObject( %shape ) )
	{
		echo( "Failed to create TSShapeConstructor for " @ %obj.getId() );
		return;
	}

	%shapeName = fileBase(%obj.shapeName);
	%count = %shape.getNodeChildCount( %nodeGroup );

	for ( %i = 0; %i < %count; %i++ )
	{
		// get node transform in object space, then transform to world space
		%child = %shape.getNodeChildName( %nodeGroup, %i );
		%txfmA = %shape.getNodeTransform( %child, true );
		%txfmB = %shape.getNodeTransform( %child, false );
		%txfm = MatrixMultiply( %srcObj.getTransForm(), %txfmB );
		%position = getWords(%txfmA,0,2);
		%rotation = getWords(%txfmA,3,6);
		%realPos = VectorAdd(%srcObj.position,%position);
		%transform = %realPos SPC %rotation;

		if (isObject(%addObjFile))
		{
			%newObj = %addObjFile.deepClone();
			%newObj.setTransform(%transform);
			Scene.addObjectToScene(%newObj);
			continue;
		}

		Scene.createStatic(%addObjFile,%transform);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
function checkShapeNodeCfg( %TSStatic )
{
	foreach$(%field in $Cfg_Creator_TSStatic_Fields)
	{
		%value = $Cfg_Creator_TSStatic_[%field];
		%TSStatic.setFieldValue(%field,%value);
		info("New TSStatic got default setting:",%field,"set to",%value);
	}

	%shapePath = getObjectShapeFile(%TSStatic);
	%shape = getShapePathConstructor(%shapePath);

	if (%shape.getNodeIndex("nodecfg") !$= "-1")
	{
		%count = %shape.getNodeChildCount( "nodecfg" );

		for ( %i = 0; %i < %count; %i++ )
		{
			%child = %shape.getNodeChildName( "nodecfg", %i );
			%settingWords = strreplace(%child,"_"," ");
			%setting = firstWord(%settingWords);
			%value = restWords(%settingWords);
			info("Found a setting under nodecfg:",%child,"Field",%setting,"Value",%value);
			%TSStatic.setFieldValue(%setting,%value);

		}
	}

	foreach$(%nodeName in $ColladaImportSpecialNodes)
	{
		if (%shape.getNodeIndex("nodecfg") !$= "-1")
		{
			%settingWords = $ColladaImportSpecialNode[%nodeName];
			%field = firstWord(%settingWords);
			%value = restWords(%settingWords);
			info("Found a special node setting:",%nodeName,"Field",%field,"Value",%value);
			%TSStatic.setFieldValue(%field,%value);
		}
	}

	delObj(%shape);
	return %TSStatic;
}
