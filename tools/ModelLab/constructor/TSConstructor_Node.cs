//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function TSShapeConstructor::nodeOffset( %this,%node,%offset )
{
	if (%this.defaultNodeTransform[%node] $= "")
		%this.defaultNodeTransform[%node] = %this.getNodeTransform(%node);

	%defaultPos = getWords(%this.defaultNodeTransform[%node],0,2);
	%defaultTrans = getWords(%this.defaultNodeTransform[%node],3,6);
	%newPos = VectorAdd(%defaultPos,%offset);
	%this.offset = %offset;
	%this.setNodeTransform(%node,%newPos @ %defaultTrans );

}
//------------------------------------------------------------------------------
//==============================================================================
function TSShapeConstructor::defaultNodeTrans( %this,%node )
{
	if (%this.defaultNodeTransform[%node] $= "")
		%this.defaultNodeTransform[%node] = %this.getNodeTransform(%node);

	%this.offset = "0 0 0";
	%this.setNodeTransform(%node, %this.defaultNodeTransform[%node] );
}
//------------------------------------------------------------------------------
