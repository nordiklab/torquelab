//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$MatLab_SkipTerrainMaterials = true;
//==============================================================================
function MatLab::buildMaterialSet(%this,%reset)
{
	//If reset specified, delete all the filtering controls
	if (%reset)
		MatLab.clearFilterArray();

	MatLab.staticFilterObjCount = MatLabFiltersDynamicArray.getCount();// Remove the header box
	//%staticFilterContainer.delete();
	// Create our category array used in the selector, this code should be taken out
	// in order to make the material selector agnostic
	%mats = "";
	%count = 0;

	newArrayObject(MatLabTagFilterArray);//Contain all tags active state (TAG,1 or 0)
	newArrayObject(MatLabTagMaterialArray);//Contain all tags/Material keyValues
	MatLabTagFilterArray.setVal("Untagged",1);
	newSimSet(MatLabFullSet);
	newSimSet(MatLabMappedSet);
	newSimSet(MatLabUnmappedSet);
	newSimSet(MatLabCubemapSet);

	MatLab.currentStaticSet = MatLabMappedSet;

	//If in terrainMaterials mode, get terrain Materials list, else get materials
	if (MatLab.terrainMaterials)
	{
		//Used by GroundCover to select terrainMat
		%this.buildTerrainStaticFilters();
		return;
	}

	%count = materialSet.getCount();

	for (%i = 0; %i < %count; %i++)
	{
		// Process regular materials here
		%material = materialSet.getObject(%i);
		%this.processMaterialData(%material);
	}

	%this.matSetBuilt = true;
	MatLab.activeMatSet = MatLabFullSet;
	MatLab.setBaseFiltersCount();

	//Update the Tag Checkbox listing with fresh data
	MatLab.updateTagCheckboxes();
	matLog("Mat sets built! Full:",MatLabFullSet.getCount(),"Mapped:",MatLabMappedSet.getCount(),"UnMapped",MatLabUnmappedSet.getCount());
}
//------------------------------------------------------------------------------
function MatLab::processMaterialData(%this,%material,%buildPreview)
{
	for (%k = 0; %k < UnlistedMaterials.count(); %k++)
	{
		if (UnlistedMaterials.getValue(%k) $= %material.name)
			return false;
	}

	//------------------------------------------------------------------------
	// Add it to appropriate group is mapped or not
	MatLab.matBaseFilters(%material,1,1);
	MatLab.extractMatTags(%material);

	if (%buildPreview)
	{
		%container = MatLab.buildPreviewArray(%material);
		%this.addToCurrentPage(%container);
		%this.sort();
	}

	if (isObject(%material.cubemap))
	{
		info("Cubemap found from material:",%material.cubemap,"ID",%material.cubemap.getId());
		MatLabCubemapSet.add(%material.cubemap);

	}

	info("MatLab found",MatLabCubemapSet.getCount(),"cubemaps");
	return true;
}

//------------------------------------------------------------------------------

//oldmatSelector.buildStaticFilters();
function MatLab::buildTerrainMaterialSet(%this)
{
	%mats = ETerrainEditor.getTerrainBlocksMaterialList();
	%count = getRecordCount(%mats);

	for (%i = 0; %i < %count; %i++)
	{
		// Process terrain materials

		%matInternalName = getRecord(%mats, %i);
		%material = TerrainMaterialSet.findObjectByInternalName(%matInternalName);

		// Is there no material info for this slot?
		if (!isObject(%material))
			continue;

		// Add to the appropriate filters
		MatLab.matBaseFilters(%material,1);

		if (%material.terrainMaterials && $MatLab_SkipTerrainMaterials)
			continue;
	}
}
//------------------------------------------------------------------------------

function MatLab::buildTerrainStaticFilters(%this)
{
	MatLab.buildTerrainMaterialSet();
}
//------------------------------------------------------------------------------

function MatLab::fixMaterialsPBR(%this)
{

	foreach(%mat in materialSet)
	{

		if (%mat.metalness[0] > 1 || %mat.metalness[0] <0)
		{
			//%mat.setFieldValue("metalness","0.9",0);
			//Using the PersistenceManager saveDirty was causing crash when many dirty objects
			// By saving it right away, it was taking longer but no crash at least.
			%this.setAndSave(%mat,"metalness 0","0.9");
		}
	}
}
//------------------------------------------------------------------------------
