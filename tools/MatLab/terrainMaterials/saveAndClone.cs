//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
//TerrainMaterial have a specific save function to make sure it's not saved with
//splatmap as it could be assign to the diffuse based on current terrain
function MatLab::saveTerrainMaterial(%this,%material)
{
	if (%layer $= "")
		%layer = 0;

	if (!isObject(%material) || !isImageFile(%texture)|| %map $= "" || %layer < 0 || %layer > 4)
		return false;

	//The function support map with the Map suffix or not, just remove it if exist
	%map = strReplace(%map,"Map","");
	%map = %map@"Map";

	if (!%material.isField(%map))
	{
		warnLog("Trying to set invalid Material Map:",%map,"for material", %material.getName());
		return false;
	}

	%this.set(%material,%map SPC %layer,%texture);

	return true;
}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::cloneTerrainMaterial( %this,%sourceId,%internalName,%isMissionSpecific)
{
	if (%isMissionSpecific)
		%internalName = "Ter_"@%internalName;

	%matName = getUniqueInternalName( %internalName, TerrainMaterialSet, true );

	%fileName = "art/terrains/materials.cs";
	%fileName = TerrainMaterialDlg-->cloneFileName.getText();
	// Create the new material.
	%newMat = new TerrainMaterial()
	{
		internalName = %matName;
		parentGroup = TerrainMaterialDlgNewGroup;
	};

	if (isObject(%sourceId) && %sourceId.isMemberOfClass("TerrainMaterial"))
	{
		%newMat.assignFieldsFrom(%sourceId);
		%fileName = %sourceId.getFilename();
	}

	if (%isMissionSpecific)
		%fileName = strreplace($Server::MissionFile,".mis",".mats.cs");

	%newMat.setInternalName( %matName );
	%newMat.setFileName( %fileName );

	// Mark it as dirty and to be saved in the default location.
	%this.setDirty( %newMat, %fileName );
	return %newMat;
}
