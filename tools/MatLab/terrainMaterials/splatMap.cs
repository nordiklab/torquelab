//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

/*

//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
function SceneEd::toggleSplatMapMode( %this )
{
	toggleSplatMapMode();
}
//------------------------------------------------------------------------------

//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
function SceneEd::selectSplatMapImage( %this )
{
	selectSplatMapImage();
}
//------------------------------------------------------------------------------
function EPainter::toggleSplatMap( %this,%checkBox )
{
	toggleSplatMapMode();
	%checkBox.setStateOn($SplatMapModeActivated);
}
*/
