//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$MatLab_PreviewOld = 0;
$MatLab_PreviewDefaultDistance = 10;
$MatLab_ShapePreviewDefaultDistance = 10;
$MatLab_MaterialPreviewTypes = "Cube Sphere Pyramid Cylinder Torus Knot";
$MatLab_PreviewShapesPath = "tlab/art/assets/shapes/tlabPreview_";
$MatLab_PreviewStockShapesPath = "tlab/art/assets/stockdts/Preview_";

$MatLab_MaterialPreviewGui = "matEd_previewObjectViewLab";
//==============================================================================
function MatLab::initMaterialPreview(%this,%reset)
{
	if (MatLab_PreviewObjectMenu.size() >0 && !%reset)
		return;

	MatLab_ShapePreviewObjectMenu.clear();
	MatLab_PreviewObjectMenu.clear();
	%id = -1;

	foreach$(%type in $MatLab_MaterialPreviewTypes)
	{
		%id++;
		MatLab_ShapePreviewObjectMenu.add(%type,%id);
		MatLab_PreviewObjectMenu.add(%type,%id);
	}

	MatLab_PreviewObjectMenu.setSelected( 0, false );
	MatLab_PreviewObjectMenu.selected = MatLab_PreviewObjectMenu.getText();
	MatLab_ShapePreviewObjectMenu.setSelected( 0, false );
	MatLab_ShapePreviewObjectMenu.selected = MatLab_ShapePreviewObjectMenu.getText();

}

function MatLab_PreviewObjectMenu::onSelect(%this,%id,%text)
{
	MatLab.updateMaterialPreviewType(%text);
}
//==============================================================================

function MatLab::updateMaterialPreviewType(%this,%type,%distance)
{
	if (%distance $= "")
		%distance = $MatLab_PreviewDefaultDistance;

	if (!isFile($MatLab_PreviewShapesPath@%type@".DAE"))
		return warnLog("Wrong preview shape called",  $MatLab_PreviewShapesPath@%type@".DAE");

	$MatLab_MaterialPreviewGui.setModel($MatLab_PreviewShapesPath@%type@".DAE");
	$MatLab_MaterialPreviewGui.setOrbitDistance($MatLab_PreviewDefaultDistance);

	MatLab_PreviewObjectMenu.setText(%type);
	MatLab_PreviewObjectMenu.selected = %type;
}
//------------------------------------------------------------------------------

function MatLab::previewMaterial(%this,%material)
{
	%material = MatLab.selectedMaterial;

	if (!isObject(%material))
		return;

	%this.copyMaterials( %material, MatLab_previewMaterial );
	MatLab_previewMaterial.flush();
	MatLab_previewMaterial.reload();

	schedule(500,0,toggleDlg,COlorPickerDlg);
	schedule(500,0,toggleDlg,COlorPickerDlg);
	//%this.updateMaterialPreviewType(MatLab_PreviewObjectMenu.selected);
}

//==============================================================================
function MatLab::disablePreview(%this,%preview)
{

	MatLab.copyMaterials( MatLab_notDirtyMaterial, MatLab_previewMaterial );
	MatLab.copyMaterials( MatLab_notDirtyMaterial, MatLab.selectedMaterial );

	//Flush and reload is required to update the preview material on shapes
	MatLab_previewMaterial.flush();
	MatLab_previewMaterial.reload();

	//First delete the model so that it releases material instances that use the preview materials.
	MatLab_GuiMaterialPreview.deleteModel();

	//Now we can delete the preview materials and shaders knowing that there are no matinstances using them.
	MatLab_previewMaterial.delete();
}
//------------------------------------------------------------------------------

function fixme()
{
	MatLab.refreshPreviewColor();
}

//==============================================================================
function MatLab::refreshPreviewColor(%this)
{
	MatLab.updatePreviewBackground(MatLab_BackgroundColorPicker.color);
	matEd_previewObjectViewLab.setAmbientLightColor(MatLab_ambientColorPicker.color);
	matEd_previewObjectViewLab.setLightColor(MatLab_lightColorPicker.color);
	matEd_previewBackgroundLab.color = MatLab_BackgroundColorPicker.color;
	MatLab_previewMaterial.flush();
	MatLab_previewMaterial.reload();
}
//------------------------------------------------------------------------------

//==============================================================================
// Material Preview Colors Edit
//==============================================================================
//==============================================================================
//These two functions are focused on environment specific functionality
function MatLab::updateLightColor(%this, %color)
{
	matEd_previewObjectViewLab.setLightColor(%color);
	MatLab_lightColorPicker.color = %color;
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::updatePreviewBackground(%this,%color)
{
	matEd_previewBackgroundLab.color = %color;
	MatLab_BackgroundColorPicker.color = %color;
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::updateAmbientColor(%this,%color)
{
	matEd_previewObjectViewLab.setAmbientLightColor(%color);
	MatLab_ambientColorPicker.color = %color;
}
//------------------------------------------------------------------------------

//==============================================================================
// ShapeEd Preview Mode
//==============================================================================
function MatLab_ShapePreviewObjectMenu::onSelect(%this,%id,%text)
{
	MatLab.updateShapePreviewType(%text);
}
//==============================================================================

function MatLab::updateShapePreviewType(%this,%type,%distance)
{
	if (%type $= "")
		%type = "cube";

	if (%distance $= "")
		%distance = $MatLab_ShapePreviewDefaultDistance;

	if (!isFile($MatLab_PreviewShapesPath@%type@".DAE"))
		return warnLog("Wrong preview shape called",  $MatLab_PreviewShapesPath@%type@".DAE");

	%this.shapePath = $MatLab_PreviewShapesPath@%type@".DAE";

	if ( !MatLabModelEdView.setModel( %this.shapePath ) )
	{
		LabMsgOK( "Error", "Failed to load '" @ %this.shapePath @ "'. Check the console for error messages." );
		return;
	}

	MatLabModelEdView.orbitDist = %distance;
	MatLab_PreviewObjectMenu.setText(%type);
	MatLab_PreviewObjectMenu.selected = %type;
}
//------------------------------------------------------------------------------

function matShape(%type,%dist)
{
	MatLab.updateShapePreviewType(%type,%dist);
}
//==============================================================================
function MatLab::previewShape(%this,%type)
{
	if (%type $= "")
		%type = "cube";

	if (!isFile($MatLab_PreviewShapesPath@%type@".DAE"))
		return warnLog("Wrong preview shape called",  $MatLab_PreviewShapesPath@%type@".DAE");

	%this.shapePath = $MatLab_PreviewShapesPath@%type@".DAE";

	if ( !MatLabModelEdView.setModel( %this.shapePath ) )
	{
		LabMsgOK( "Error", "Failed to load '" @ %this.shapePath @ "'. Check the console for error messages." );
		return;
	}

	MatLabModelEdView.fitToShape();
}
//------------------------------------------------------------------------------

//==============================================================================
//These two functions are focused on environment specific functionality
function MatLab::updateShapeLightColor(%this, %color)
{
	%colorI = ColorFloatToInt(%color);
	MatLabModelEdView.sunDiffuse = %colorI;
	MatLab_lightShapeColorPicker.color = %color;
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::updateShapePreviewBackground(%this,%color)
{

	MatLabModelEdSwatch.color = %color;
	MatLab_BackgroundShapeColorPicker.color = %color;
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::updateShapeAmbientColor(%this,%color)
{
	%colorI = ColorFloatToInt(%color);
	MatLabModelEdView.sunAmbient = %colorI;
	MatLab_ambientShapeColorPicker.color = %color;
}
//------------------------------------------------------------------------------
