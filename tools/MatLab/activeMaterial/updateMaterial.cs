//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function MatLab::setActive(%this,%field,%value,%save )
{
	if (%save)
		%this.setAndSave(MatLab.selectedMaterial,%field,%value);
	else
		%this.set(MatLab.selectedMaterial,%field,%value);
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::saveActiveMaterial(%this )
{
	%this.saveMaterial(MatLab.selectedMaterial,true);
}
//------------------------------------------------------------------------------

//==============================================================================

//==============================================================================
function MatLab_ActiveMatEdit::onValidate(%this)
{
	%mat = MatLab.selectedMaterial;

	if (!isObject(%mat))
		return;

	%field = %this.internalName;
	%text = %this.getText();

	switch$(%field)
	{
		case "name":
			if (%text $= %mat.getName())
				return;

			%success = MatLab.setMaterialName(%mat,%text);

			if (%success)
				MatLab_ActiveMatName.setText(%text);

		case "fileName":
			if (%text $= %mat.getFileName())
				return;

			%success = MatLab.setMaterialFile(%mat,%text);

			if (%success)
				MatLab_ActivePropertyPage-->filename.setText(%mat.getFileName());

		default:
			MatLab.set(%mat,%this.internalName,%this.getText());
	}

}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab_ActiveMatCheckbox::onClick(%this)
{
	%mat = MatLab.selectedMaterial;

	if (!isObject(%mat))
		return;

	MatLab.set(%mat,%this.internalName,%this.isStateOn());
}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::unlistMaterial(%this, %materialName)
{
	%success = %this.setMaterialUnlisted(%materialName);

	if (!%success)
		return;

	%previewBox = %this.getMatName(%materialName)@"_preview";

	if (isObject(%previewBox))
		delObj(%previewBox);
}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::updateActiveMatPreview(%this)
{
	if (!isObject(MatLab.selectedMaterial))
		return;

	%newPreview = MatLab.getMatPreview(MatLab.selectedMaterial);

	if (%newPreview $= MatLab.selectedPreviewImagePath)
		return;

	%this.setActiveMatField("preview",%newPreview);

	%box = MatLab_MaterialsPages.findObjectByInternalName(MatLab.selectedMaterial.getId(),1);

	if (!isObject(%box))
		return;

	%box-->bitmapButton.setBitmap( %newPreview );

}
//------------------------------------------------------------------------------

//==============================================================================
//needs to be deleted with the persistence manager and needs to be blanked out of the matmanager
//also need to update instances... i guess which is the tricky part....
function MatLab::confirmDeleteDialog(%this)
{
	%material = MatLab.selectedMaterial;
	%secondFilter = "MaterialFilterMappedArray";
	%secondFilterName = "Mapped";

	for (%i = 0; %i < MaterialFilterUnmappedArray.count(); %i++)
	{
		if (MaterialFilterUnmappedArray.getValue(%i) $= %material)
		{
			%secondFilter = "MaterialFilterUnmappedArray";
			%secondFilterName = "Unmapped";
			break;
		}
	}

	if (isObject(%material))
		MatLab.deleteMaterial(%material,true);

}
//------------------------------------------------------------------------------
