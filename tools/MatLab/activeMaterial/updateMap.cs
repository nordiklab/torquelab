//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function MatLab::updateMatMap(%this,%field,%texture)
{
	%preview = %texture;

	%success = %this.setMaterialTextureMap(MatLab.selectedMaterial,%field,%texture,MatLab.currentLayer);

	if (!%success)
		return;

	%rollout = MatLab_ActiveMaps.findObjectByInternalName(%field);

	if (!isObject(%rollout))
		return;

	if (!isImageFile(%preview))
		%preview = $MatLab_NoTextureBmp;

	%fileName = fileBase(%texture);

	%rollout-->file.text = %texture;
	%rollout-->fileName.text = %fileName;
	%rollout-->previewBitmap.bitmap = %texture;

	if (firstWord(%field) $= "diffuseMap")
	{
		MatLab.updateActiveMatPreview();
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Generic map type texture update
function MatLab::selectTextureMap( %this, %type )
{
	%texture = MatLab.selectMapFile(MatLab.selectedMaterial,%type,MatLab.currentLayer);

	if ( isImageFile( %texture) )
		%this.updateMatMap(%type@"Map" SPC MatLab.currentLayer,%texture);

}
//------------------------------------------------------------------------------

