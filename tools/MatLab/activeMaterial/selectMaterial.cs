//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function MatLabMaterialPreviewBtn::onClick(%this,%material)
{
	%container = %this.parentGroup;
	%material = %container.material;
	%previewBmp = %container.preview;

	if (!isObject(%material))
		return;

	if (!isFile(%previewBmp))
		%previewBmp  = MatLab.getMatPreview(%material);

	MatLab.updateSelection(%material,%previewBmp);

}
//==============================================================================
function MatLab::setSelectedMaterial(%this,%material)
{
	if (!isObject(%material))
		return;

	%matName = %this.getMatName(%material);
	%preview = %matName@"_Preview";

	if (isObject(%preview))
	{
		MatLab.updateSelection(%material,%preview.preview);
	}
}

function MatLab::updateSelection(%this, %material, %previewImagePath)
{

	//====================================
	// MATERIAL SELECTION EFFECT HERE
	// the material selector will visually update per material information
	// after we move away from the material. eg: if we remove a field from the material,
	// the empty checkbox will still be there until you move fro and to the material again
	//====================================
	%this.showMaterialCreator(false);
	%name = %this.getMatName(%material);
	MatLab.selectedMaterial = %material;
	MatLab.currentLayer = 0;
	MatLab.curMaterial = %material;
	MatLab.currentMaterial = %material;

	if ($MatLab_PreviewAuto)
	{
		MatLab.previewMaterial();
	}

	%this.setActiveMatField("preview",%previewImagePath);
	%this.setActiveMatField("name",%name);
	%this.setActiveMatField("file",%material.getFileName());

	foreach$(%field in $MatLab_ActiveMatProperties)
	{
		%this.setActiveMatField(%field,%material.getFieldValue(%field));
	}

	//If the material have a cubemap assigned show the button
	MatLabCubemapEditBox.visible = (isObject(%material.cubemap))?true:false;

	MatLab.getActiveMatTags(%material);

	if (MatLab.initTags[%material] $="")
		MatLab.initTags[%material] = %material.tags;

	MatLab.setActiveMapRollouts();

	$prevSelectedMaterialHL = %material;
}
function MatLab::setActiveMatField(%this, %field,%value)
{
	switch$(%field)
	{
		case "name":
			MatLab.curMaterialName = %value;
			MatLab_ActiveMatName.setText(%value);
			MatLab.selectedMaterialName = %value ;

		case "file":
			MatLab.MaterialPath = %value;
			MatLab_ActivePropertyPage-->filename.setText(%value);

		case "preview":
			MaterialSelector-->previewSelection.setBitmap(%value);
			MatLab.selectedPreviewImagePath = %value;
	}

	%ctrl = MatLab_ActivePropertyPage.findObjectByInternalName(%field,1);

	if (isObject(%ctrl))
		%ctrl.setTypeValue(%value);

}

//==============================================================================
function MatLab::getActiveMatTags(%this, %material)
{
	if (!isObject(%material))
		return;

	foreach (%ctrl in MatLabMaterialCategories)
		$MatLab_ActiveTag_[%ctrl.internalName] = 0;

	%this.fixMatTags(%material);
	%tags = %material.tags;

	for (%i =0; %i < getCommaWordCount(%tags); %i++)
	{
		%tag = trim(getCommaWord(%tags,%i));
		$MatLab_ActiveTag_[%tag] = 1;
	}

}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::setAsActive(%this,%name)
{
	%mat = MatLab.selectedMaterial;

	if (!isObject(%mat))
		return;

	MaterialEditorTools.setActiveMaterial(%mat);
}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::setActiveMapRollouts(%this)
{
	hide(MatLab_MapRolloutSrc);
	MatLab_ActiveMaps.clear();

	foreach$(%map in $MatLab_MaterialMaps)
		%this.setActiveMapRolloutType(%map);

	MatLab_ActiveMaps.findObjectByInternalName("diffuseMap 0").expanded = 1;
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::setActiveMapRolloutType(%this,%type,%material)
{
	if (%material $= "")
		%material = MatLab.selectedMaterial;

	if (!isObject(%material))
		return;

	%preview = %material.getFieldValue(%type@"Map",0);

	if (!isImageFile(%preview))
		%preview = $MatLab_NoTextureBmp;

	%fileName = fileBase(%preview);

	%rollout = cloneGui(MatLab_MapRolloutSrc,MatLab_ActiveMaps);
	%rollout.expanded = 0;
	%rollout.caption = %type SPC "Map";
	%rollout-->selectButton.command = "MatLab.selectTextureMap(\""@%type@"\");";
	%rollout-->selectMap.command = "MatLab.selectTextureMap(\""@%type@"\");";
	%rollout-->clearMap.command = "MatLab.updateMatMap(\""@%type@"Map 0\",\"\");";
	%rollout-->file.text = %preview;
	%rollout-->fileName.text = %fileName;
	%rollout-->previewBitmap.bitmap = %preview;
	%rollout.internalName = %type@"Map" SPC 0;
	hide(%rollout-->colorSwatch);

	if (%type $= "Diffuse")
	{
		%mapColor = %material.getFieldValue(%type@"Color",0);
		show(%rollout-->colorSwatch);
		%rollout-->colorSwatch.command = "getColorF(\""@%mapColor@"\", \"MatLab.updateMat"@%type@"Color\");";
	}
	else
	{
		hide(%rollout-->colorSwatch);
		%rollout-->colorSwatch.command = "warnLog(\"There's no color settings for this map:\","@%type@");";
	}

}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::updateMatDiffuseColor(%this,%color)
{
}
//------------------------------------------------------------------------------
