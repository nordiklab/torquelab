//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function MatLab::uiSetup(%this)
{
	if (MaterialSelector.visible) MaterialSelector.visible = true;

	//Hide Samples and Creator Box
	if (MatSelector_FilterSamples.visible) MatSelector_FilterSamples.visible = false;

	if (MatSelector_FilterSamples.visible) MatSelector_FilterSamples.visible = false;

	if (MatSelector_PageTextSample.visible) MatSelector_PageTextSample.visible = false;

	if (MatSelector_PageButtonSample.visible) MatSelector_PageButtonSample.visible = false;

	if (MatSelector_MaterialPreviewSample.visible) MatSelector_MaterialPreviewSample.visible = false;

	if (MatLab_Creator.visible) MatLab_Creator.visible = false;

	if (MatLabProcessingOverlay.visible) MatLabProcessingOverlay.visible = false;

	MatLab.initMaterialPreview();

	MatLab.sortMode = $MatLab_SortMode;
	MatLab.sortDown = $MatLab_SortDown;

	if ($Cfg_MatLab_ThumbPerPage $= "" || !strIsNumeric($Cfg_MatLab_ThumbPerPage))
		$Cfg_MatLab_ThumbPerPage = $MatLab_ThumbPerPage;

	if ($Cfg_MatLab_ThumbSize $= "" || !strIsNumeric($Cfg_MatLab_ThumbSize))
		$Cfg_MatLab_ThumbSize = $MatLab_ThumbSize;

	if (MatLab_ThumbPerPageMenu.size() == 0)
		MatLab.buildThumbPerPageMenu();

	if (MatLab_ThumbSizeMenu.size() == 0)
		MatLab.buildThumbSizeMenu();

	if (MatLab_SortByMenu.size() == 0)
		MatLab.initSortMenu();
}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::buildThumbPerPageMenu(%this)
{
	%i = -1;
	MatLab_ThumbPerPageMenu.clear();

	foreach$(%num in $MatLab_ThumbCountPerPages)
		MatLab_ThumbPerPageMenu.add(%num,%i++);

	//Simply set the text, pages will be built after previews
	MatLab_ThumbPerPageMenu.setText($Cfg_MatLab_ThumbPerPage);
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab_ThumbPerPageMenu::onSelect(%this,%id,%text )
{
	if ($Cfg_MatLab_ThumbPerPage $= %text)
		return;

	$Cfg_MatLab_ThumbPerPage = %text;
	MatLab.buildPages();
	//Not sure why, I would rather rebuild the pages

}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::buildThumbSizeMenu(%this)
{
	%i = -1;
	MatLab_ThumbSizeMenu.clear();

	foreach$(%size in $MatLab_ThumbSizes)
		MatLab_ThumbSizeMenu.add(%size,%i++);

	//Simply set the text, pages will be built after previews
	MatLab_ThumbSizeMenu.setText($Cfg_MatLab_ThumbSize);
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab_ThumbSizeMenu::onSelect(%this,%id,%text )
{
	if ($Cfg_MatLab_ThumbSize $= %text)
		return;

	$Cfg_MatLab_ThumbSize = %text;
	MatLab.resizeThumbnails();
	//Not sure why, I would rather rebuild the pages

}
//------------------------------------------------------------------------------

$MatLabResizeActivePageOnly = 0;
//==============================================================================
// Size slider dragged - Update the preview images sizes
function MatLab::thumbnailSizeUpdate(%this,%ctrl)
{
	if (!isObject(%ctrl))
		return;

	%index = %ctrl.getValue();
	%size = getWord($MatLab_ThumbSizes,%index);

	%this.resizeThumbnails(%size);
}
//------------------------------------------------------------------------------
//==============================================================================
// Size slider dragged - Update the preview images sizes
function MatLab::resizeThumbnails(%this,%size)
{
	if (%size $= "")
		%size = $Cfg_MatLab_ThumbSize;

	if (!strIsNumeric(%size) || %size < 16)
		return;

	$Cfg_MatLab_ThumbSize = %size;

	if ($MatLabResizeActivePageOnly)
	{
		%stack = MatLab.pageContainer[MatLab.page];
		%stack.colSize = %size + 12; //Add outbounds
		%stack.rowSize = %size + 21; //Add outbounds
		%stack.refresh();
		return;
	}

	foreach (%stack in MatLab_MaterialsPages)
	{
		%stack.colSize = %size + 12; //Add outbounds
		%stack.rowSize = %size + 21; //Add outbounds
		%stack.refresh();
	}
}
//------------------------------------------------------------------------------

function MatLab::setColorBG(%this,%color)
{
	if (%color $= "")
		%color = $MatLab_ColorBG;

	$Cfg_MatLab_ColorBG = %color;

	//Use LabObj updater to save the new profile
	LabObj.setAndSave(ToolsFillPreviewBG,"fillColor",%color);
}
//------------------------------------------------------------------------------
