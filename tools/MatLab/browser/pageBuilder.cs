//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function MatLab::buildPages(%this)
{
	if ($MatLab_MegaPageContainer)
		return %this.buildMegaPages(%id);

	MatLab_PageButtonsStack.clear();
	%perPage = $Cfg_MatLab_ThumbPerPage;
	%page = 1;
	%id = 1;
	MatLab.lastFilterText = MatLab.filterText;
	%set = MatLabPreviewSet;

	foreach (%obj in %set)
	{
		//Check if the ctrl not filtered
		if (!%this.getMatFilterState(%obj))
			continue;

		%obj.visible = 1;

		%page = mCeil(%id / %perPage);

		if (%page !$= %curPage)
			MatLab.getPageButton(%page);

		%curPage = %page;

		if (!isObject(MatLab.pageContainer[%page]))
			MatLab.pageContainer[%page] = MatLab.getPageContainer(%page);

		%addTo = MatLab.pageContainer[%page];

		if (%addTo.superClass !$= "MatLabPreviewPage")
		{
			warnLog("Invalid Container for:",%obj.getName(),"Container=",%addTo);
			%addTo = MatLab.getPageContainer("999");
		}

		%addTo.schedule(500,add,%obj);
		%id++;
	}

	MatLab.activePageCount = %page;
	%this.loadPage(mClamp(MatLab.page,1,MatLab.activePageCount));
	%this.updatePageButtons();
	%this.checkTextFilter();
	%this.SortPages();

}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::addToPages(%this, %container,%delay)
{
	//Add to properPage based on current count and thumbs per page
	%count = MatLabPreviewSet.getCount();
	%page = mCeil(%count / $MatLab_ThumbPerPage);

	//If already in a page, remove it from count as it will be moved
	if (%container.parentGroup.superClass $= "MatLabPreviewPage")
		%count--;

	//If there's no container for that page, get a fresh one
	if (!isObject(MatLab.pageContainer[%page]))
		MatLab.pageContainer[%page] = %this.getPageContainer(%page);

	if (%delay > 0)
		MatLab.pageContainer[%page].schedule(500,add,%container);
	else
		MatLab.pageContainer[%page].add(%container);
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::addToPages(%this, %container,%delay)
{
	//Add to properPage based on current count and thumbs per page
	%count = MatLabPreviewSet.getCount();
	%page = mCeil(%count / $MatLab_ThumbPerPage);

	//If already in a page, remove it from count as it will be moved
	if (%container.parentGroup.superClass $= "MatLabPreviewPage")
		%count--;

	//If there's no container for that page, get a fresh one
	if (!isObject(MatLab.pageContainer[%page]))
		MatLab.pageContainer[%page] = %this.getPageContainer(%page);

	if (%delay > 0)
		MatLab.pageContainer[%page].schedule(500,add,%container);
	else
		MatLab.pageContainer[%page].add(%container);
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::addToCurrentPage(%this, %container)
{
	if (!isObject(MatLab.pageCtrl))
	{
		if (!MatLabPreviewSet.isMember(%container))
			MatLabPreviewSet.add(%container);

		return false;
	}

	MatLab.pageCtrl.add(%container);
	return true;
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::getPageContainer(%this,%page)
{
	%name = "MatLab_MaterialsContainer_"@%page;
	delObj(%name);
	%control = %this.getPageButton(%page);
	%visible = (%page != MatLab.page) ? false : true;

	%container = new GuiDynamicCtrlArrayControl(%name)
	{
		colCount = "8";
		colSize = "92";
		rowCount = "10";
		rowSize = "101";
		rowSpacing = "2";
		colSpacing = "2";
		frozen = "0";
		autoCellSize = "0";
		fillRowFirst = "1";
		dynamicSize = "1";
		position = "3 4";
		extent = "768 1028";
		minExtent = "8 8";
		horizSizing = "width";
		vertSizing = "bottom";
		profile = "ToolsDefaultProfile";
		tooltipProfile = "ToolsToolTipProfile";
		isContainer = "1";
		internalName = %page;
		visible = %visible;
		canSave = "0";
		canSaveDynamicFields = "0";
		superClass = "MatLabPreviewPage";

	};
	MatLab_MaterialsPages.add(%container);
	MatLab.pageCount = MatLab_MaterialsPages.getCount();
	return %container;
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::getPageButton(%this,%page)
{
	if (isObject(MatLab_PageButtonsStack.findObjectByInternalName(%page)))
		return MatLab_PageButtonsStack.findObjectByInternalName(%page);

	%control = cloneObject(MatSelector_PageButtonSample);
	%control.text = %page;
	%control.internalname = %page;
	%control.command = "MatLab.loadPage("@%page@");";
	%control.active = (%page == MatLab.page) ? false : true;

	MatLab_PageButtonsStack.add(%control);

	return %control;
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::updatePageButtons(%this)
{
	MatLabPageButtonStack.AlignCtrlToParent("CenterX");
}
//------------------------------------------------------------------------------
