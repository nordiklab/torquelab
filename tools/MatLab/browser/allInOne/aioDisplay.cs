//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function MatLab::startRebuildingMegaPreviews(%this)
{
	MatLab_MaterialsSinglePage.visible = 1;
	MatLab_MaterialsPages.visible = 0;
	//MatLab_MaterialsPages.callOnChildrenNoRecurse("clear","");
	//MatEdPreviewArray.empty();

	foreach (%mat in materialSet)
	{
		MatLab.buildPreviewArray(%mat,%matName);
	}

	//LabProgessBox.end();
}
//------------------------------------------------------------------------------
//==============================================================================
// Build the preview control for the Material DynamicArray
function MatLab::buildMegaPreviewArray(%this, %material)
{
	// CustomMaterials are not available for selection
	if (!isObject(%material) || %material.isMemberOfClass("CustomMaterial"))
		return;

	MatLab.matBuilt++;
	%matName = MatLab.getMatName(%material);
	// %matName = %this.getMatName(%material);
	//%process = MatLab.matBuilt/MatLab.matCount;
	//MatLab.updProcess(%process,"Building:\c2 "@%matName@" \c0(\c3"@%process * 100@"\c0)");

	%previewImage = %this.getMatPreview(%material);

	%container = cloneGui(MatSelector_MaterialPreviewSample);

	%container.text = %matName;
	%container.name = getUniqueName(%matName@"_Preview");
	%container.material = %material;
	%previewBorder = %container-->button;
	%previewButton = %container-->bitmapButton;
	%previewButton.internalName = %matName;
	%previewBorder.tooltip = %matName;
	%previewBorder.sourceObj = %previewButton;
	//%previewBorder.Command = "MatLab.updateSelection( $ThisControl.getParent().getObject(1).internalName, $ThisControl.getParent().getObject(1).bitmap );";
	%previewBorder.Command = "MatLab.updateSelection( \""@ %matName@"\", $ThisControl.sourceObj.bitmap );";
	%previewBorder.internalName = %matName@"Border";
	%container.preview = %previewImage;
	MatLabPreviewSet.add(%container);

	if (isImageFile(%previewImage) )
		%previewButton.setBitmap(%previewImage);
	else
		warnLog("Invalid Image for material",%matName,"Image=",%previewImage);

	MatLab_MaterialsContainer.add(%container);

	if (MatLabPreviewSet.getCount() > 40)
		%container.visible = 0;

}

