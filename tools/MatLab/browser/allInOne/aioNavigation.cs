//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function MatLab::buildMegaPages(%this)
{
	MatLab_PageButtonsStack.clear();
	%perPage = $Cfg_MatLab_ThumbPerPage;
	%page = 1;
	%id = 1;

	foreach (%obj in MatLabPreviewSet)
	{
		%page = mCeil(%id / %perPage);

		if (%page $= %curPage)
			MatLab.getPageButton(%page);

		%curPage = %page;

		if (!isObject(MatLab.pageContainer[%page]))
			MatLab.pageContainer[%page] = MatLab.getPageContainer(%page);

		MatLab.pageContainer[%page].add(%obj);
		%id++;
	}

	MatLabPageButtonStack.AlignCtrlToParent("CenterX");
}
//------------------------------------------------------------------------------
//==============================================================================
function loadMegaPage(%id)
{
	MatLab_PageButtonsStack.callOnChildrenNoRecurse("setActive",1);
	%pageButton = MatLab_PageButtonsStack.findObjectByInternalName(%id);

	if (isObject(%pageButton))
		%pageButton.active = 0;

	%rangeStart = (%id-1) * 40;
	%rangeEnd = %rangeStart + 40;
	MatLab_MaterialsContainer.callOnChildrenNoRecurse("showRange",%rangeStart SPC %rangeEnd);
	MatLab.page = %id;

}
//------------------------------------------------------------------------------

//==============================================================================
function GuiControl::showRange(%this,%range)
{
	%index = MatLab_MaterialsContainer.getObjectIndex(%this);

	if (%index < %range.x || %index > %range.y)
		%visible = false;
	else
		%visible = true;

	%this.setVisible(%visible);

}
//------------------------------------------------------------------------------
