//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function MatLab::closePanel(%this,%panel)
{
	%this.setPanelState(%panel,0);

}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::setPanelState(%this,%panel,%state)
{

	%panelObj = MatLab_BrowserFrame.findObjectByInternalName(%panel);

	if (!isObject(%panelObj))
		return;

	%panelCloseObj = MatLab_BrowserFrame.findObjectByInternalName(%panel@"Closed");

	if (!isObject(%panelCloseObj))
		return;

	%panelObj.visible = %state;
	%panelCloseObj.visible = !%state;

}
//------------------------------------------------------------------------------//==============================================================================
function MatLab::openPanel(%this,%panel)
{
	%this.setPanelState(%panel,1);

}
//------------------------------------------------------------------------------
