//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function MatLab_FilterTextEdit::onValidate(%this)
{
	matLog("MatLab_FilterTextEdit::onValidate",%this.getText());
	MatLab.setListFilterText(%this.getText());
}
//------------------------------------------------------------------------------
//==============================================================================
// Called when enter is pressed
function MatLab_FilterTextEdit::onReturn(%this)
{
	matLog("MatLab_FilterTextEdit::onReturn",%this.getText());
	MatLab.setListFilterText(%this.getText(),true);
}
//------------------------------------------------------------------------------

//==============================================================================
// Call each time the text changed
function MatLab_FilterTextEdit::onCmd(%this)
{
	matLog("MatLab_FilterTextEdit::onCmd",%this.getText());
	MatLab.setListFilterText(%this.getText());
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::setListFilterText(%this,%text,%build)
{
	%filterText = %text;

	if (%text $= "" || strFind(%text,"Filter..."))
	{
		%text = "";
		//MatSel_ListFilterText.setText(%text);
		%filterText = "";
	}

	MatSel_ListFilterText.setText(%text);
	MatLab.filterText = %filterText;

	if (%build)
		%this.buildPages();
	else
		MatLab.checkTextFilter();
}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::applyTextFilter(%this)
{
	if (MatLab.lastFilterText !$= MatLab.filterText)
		%this.buildPages();
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::checkTextFilter(%this)
{
	%changed = false;

	if (MatLab.lastFilterText !$= MatLab.filterText)
		%changed = true;

	MatLab_ApplyFilterButton.active = %changed;
}
//------------------------------------------------------------------------------

//==============================================================================
// Sort the Materials based on specified type (Name or ID)
/*
function MatLab_FilterTextEdit::onAltCmd(%this)
{
   matLog("MatLab_FilterTextEdit::onAltCmd",%this.getText());
   MatLab.setListFilterText(%this.getText());
  MatLab.buildPages();
	//%this.updatePluginIconOrder(); //WIP Fast - Should only be updated when leaving
}*/
//------------------------------------------------------------------------------

//==============================================================================

//==============================================================================
// Check if a material should be shown or filtered out. All materials are shown
// if no tags checked. If one tag is checked, only those with the tag will be shown
// If there's a filter text, only those which have a text match in their name will
// be shown additionaly to the tag filter.
// Return true if not filtered and set as visible, if false it will be hidden
function MatLab::getMatFilterState(%this,%ctrl)
{

	//===========================================================================
	// Check all active tags and show items only if a text or tag match found
	//---------------------------------------------------------------------------
	%material = %ctrl.material;

	if (!isObject(%material))
		return;

	%state = true; //True by defaulr unless

	//First check if material is part of active static set (Full, Mapped or Unmapped)
	if (!MatLab.activeMatSet.isMember(%material))
	{
		%name = %this.getMatName(%material);
		%this.filterLog(%name,"Material hidden cause not in Active Set",MatLab.activeMatSet.getName());
		return %this.setMatFilterState(%ctrl,false);
	}

	if (MatLab.isField(filterText))
	{
		%name = %this.getMatName(%ctrl.material);
		%filterMatch = strfind(%name,MatLab.filterText);

		//Filter is not matching the filter text, hide it
		if (!%filterMatch )
		{
			%this.filterLog(%name,"Material hidden cause not match filter",MatLab.filterText);
			return %this.setMatFilterState(%ctrl,false);
		}
	}

	//========================================================================
	// If the filter text match the name, we can return true now
	//------------------------------------------------------------------------
	%tags = %ctrl.material.tags;

	//If materials as no tags and Filter Untagged is true, return true now
	if (%tags $= "" && $MatLab_FilterTag_Untagged)
		return %this.setMatFilterState(%ctrl,true);

	foreach (%tagCtrl in MatLabFiltersDynamicArray)
	{
		%tag = %tagCtrl.internalName;
		%tagState = $MatLab_FilterTag_[%tag];

		if (%tag $= "Untagged" || !%tagState)
			continue;

		%state = false; //If no tag match, should be false

		%matTageState = %material.isTag(%tag);
		%this.filterLog(%name,"Material check tag:",%tag,"State",%tagState,"Material have:",%tags,"State",%matTageState);

		if (%tagState && %matTageState)
		{
			%this.filterLog(%name,"Material match tag:",%tag,"Returning true");
			return %this.setMatFilterState(%ctrl,true);
		}
	}

	%this.filterLog(%name,"Material reached the filter check end with state:",%state);
	return %this.setMatFilterState(%ctrl,%state);
}
//------------------------------------------------------------------------------

//==============================================================================
// Used to return the material state and also set the appropriated visibility
function MatLab::setMatFilterState(%this,%ctrl,%state)
{
	%material = %ctrl.material;
	%this.filterLog(%this.getMatName(%material),"Material reached the filter check end with state:",%state);
	%ctrl.visible = %state;
	return %state;
}

//==============================================================================
// Old filtering with inverted system
//==============================================================================
/*
//==============================================================================
// Call when building pages to determine if a material preview should be shown
function MatLab::getMatFilterStateAlt(%this,%ctrl)
{
	//===========================================================================
	// Check all active tags and show items only if a text or tag match found
	//---------------------------------------------------------------------------

	if (!isObject(%ctrl.material))
	   return;

    if (!MatLab.activeMatSet.isMember(%ctrl.material))
    {
       %name = %this.getMatName(%ctrl.material);
         %this.filterLog(%name,"Material hidden cause not in Active Set",MatLab.activeMatSet.getName());
         %ctrl.visible = false;
	      return false;
      }
   %state = false;
   if (MatLab.isField(filterText))
   {
      %name = %this.getMatName(%ctrl.material);
      %filterMatch = strfind(%name,MatLab.filterText);

      if (!%filterMatch)
      {
         %this.filterLog(%name,"Material hidden cause not match filter",MatLab.filterText);
         %ctrl.visible = false;
	      return false;
      }
      else
      {
         //info(%name,"Material match filter",MatLab.filterText);
         //%ctrl.visible = true;
	      //return true;
      }
   }

   //========================================================================
   // If the filter text match the name, we can return true now
   //------------------------------------------------------------------------

	%tags = %ctrl.material.tags;
	foreach(%tagCtrl in MatLabFiltersDynamicArray)
	{
		if (!$MatLab_FilterTag_[%tagCtrl.internalName])
			continue;
      //========================================================================
	   // Show only material matching one of the selected tag
	   //------------------------------------------------------------------------
		if (getCommaWordIndex(%tags,%tagCtrl.internalName) !$= "-1")
			%state = true;

	   //========================================================================
	   // If True we can break now
	   if (%state)
	      break;
   }
   if (%tags $= "" && $MatLab_FilterTag_Untagged)
      %state = true;
	%ctrl.visible = %state;
	return %state;
}
//------------------------------------------------------------------------------
*/
//==============================================================================
// Methods to get the filtered items in an array or a SimSet
//==============================================================================

//==============================================================================
// Get all filtered items in an ArrayObject
function MatLab::getFilteredMatArray(%this,%ctrl)
{
	//===========================================================================
	// Show only material matching one of the selected tag
	//---------------------------------------------------------------------------
	newArrayObject(MatLabFilteredArray);

	foreach (%material in MatLab.currentStaticSet)
	{
		%tags = %material.tags;

		foreach (%tagCtrl in MatLabFiltersDynamicArray)
		{

			if (!$MatLab_FilterTag_[%tagCtrl.internalName])
				continue;

			if (getCommaWordIndex(%tags,%tagCtrl.internalName) !$= "-1")
			{
				MatLabFilteredArray.setVal(%material,%this.getMatName(%material));
				break;
			}
		}
	}

	return MatLabFilteredArray;
}
//------------------------------------------------------------------------------

//==============================================================================
// Get all filtered items in an SimSet
function MatLab::getFilteredMatSet(%this)
{
	//---------------------------------------------------------------------------
	// Create a fresh SimSet
	newSimSet(MatLabFilteredSet);

	foreach (%material in MatLab.currentStaticSet)
	{
		%tags = %material.tags;

		foreach (%tagCtrl in MatLabFiltersDynamicArray)
		{

			if (!$MatLab_FilterTag_[%tagCtrl.internalName])
				continue;

			if (getCommaWordIndex(%tags,%tagCtrl.internalName) !$= "-1")
			{
				MatLabFilteredSet.add(%material);
				break;
			}
		}
	}

	return MatLabFilteredSet;
}
//------------------------------------------------------------------------------

//==============================================================================
// Sort the Materials based on specified type (Name or ID)
function MatLab::filterLog(%this,%text,%a1, %a2, %a3, %a4,%a5, %a6, %a7, %a8,%a9, %a10, %a11, %a12)
{
	if ($MatLab_FilterLog)
		info(%text,%a1, %a2, %a3, %a4,%a5, %a6, %a7, %a8,%a9, %a10, %a11, %a12);

}
//------------------------------------------------------------------------------
