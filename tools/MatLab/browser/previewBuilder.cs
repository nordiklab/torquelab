//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Build All Material Previews Functions
//==============================================================================

//==============================================================================
function MatLab::buildPreviews(%this,%reset)
{
	if (%reset)
		%this.clearAll();

	//If static filters are not built, build them no
	if (!MatLab.matSetBuilt)
		%this.buildMaterialSet();

	if (!$MatLabProcessNoOverlay)
		%this.showBuildOverlay(MatLabProcessingOverlay);

	//MatLabFullSet.sort("matLabSortSetByName");
	MatLab.schedule(1000,startRebuildingPreviews);
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::startRebuildingPreviews(%this)
{
	MatLab.pageCount = 0;
	MatLab.page = 1;
	MatLab.matBuilt = 0;
	MatLab.matCount = materialSet.getCount();

	if (!isObject(MatLabPreviewSet))
		new SimSet(MatLabPreviewSet);
	else
		MatLabPreviewSet.deleteAllObjects();

	MatLab_MaterialsPages.deleteAllObjects();
	MatLab_PageButtonsStack.clear();
	hide(MatSelector_MaterialPreviewSample);

	//Alternate system using a single huge container
	if ($MatLab_MegaPageContainer)
		return %this.startRebuildingMegaPreviews();

	MatLab_MaterialsSinglePage.visible = 0;
	MatLab_MaterialsPages.visible = 1;

	foreach (%mat in MatLabFullSet)
	{
		MatLab.buildPreviewArray(%mat);
	}

	MatLab.sort();
	//LabProgessBox.end();
}
//------------------------------------------------------------------------------

//==============================================================================
// Build Single Material Previews Functions
//==============================================================================

//==============================================================================
// Build the preview control for the Material DynamicArray
function MatLab::buildPreviewArray(%this, %material,%buildPage)
{
	// CustomMaterials are not available for selection
	if (!isObject(%material) || %material.isMemberOfClass("CustomMaterial"))
		return;

	MatLab.matBuilt++;
	%matName = MatLab.getMatName(%material);
	%previewImage = %this.getMatPreview(%material);

	%container = cloneGui(MatSelector_MaterialPreviewSample);
	%container = %this.updateMatPreviewBox(%container,%material);

	MatLabPreviewSet.add(%container);
	return %container;
}
//------------------------------------------------------------------------------

//==============================================================================
// Update the Preview Box Material related data
function MatLab::updateMatPreviewBox(%this, %container,%material)
{
	// CustomMaterials are not available for selection
	if (!isObject(%material) || %material.isMemberOfClass("CustomMaterial"))
		return;

	%matName = MatLab.getMatName(%material);
	%previewImage = %this.getMatPreview(%material);

	%container.text = %matName;
	%container.name = getUniqueName(%matName@"_Preview");
	%container.material = %material;
	%container.internalName = %material;
	%selectButton = %container-->button;
	%previewButton = %container-->bitmapButton;
	%previewButton.internalName = "bitmapButton";

	%selectButton.tooltip = %matName;
	%selectButton.sourceObj = %previewButton;
	//%previewBorder.Command = "MatLab.updateSelection( $ThisControl.getParent().getObject(1).internalName, $ThisControl.getParent().getObject(1).bitmap );";
	//%selectButton.Command = "MatLab.updateSelection( \""@ %matName@"\", $ThisControl.sourceObj.bitmap );";
	%selectButton.internalName = %matName@"Border";
	%container.preview = %previewImage;

	if (isImageFile(%previewImage) )
		%previewButton.setBitmap(%previewImage);
	else
		warnLog("Invalid Image for material",%matName,"Image=",%previewImage);

	return %container;
}
//------------------------------------------------------------------------------

//==============================================================================
// Refresh the Material preview information in case it have changed
//==============================================================================
//==============================================================================
function MatLab::refreshMatBox(%this, %material )
{
	if (!isObject(%material))
		return;

	%matPreview = MatLabPreviewSet.findObjectByInternalName(%material.getId());
	%this.setMatPreview(%matPreview,%material);
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::refreshActiveMatBox(%this )
{
	%this.refreshMatBox(MatLab.selectedMaterial);
}
//------------------------------------------------------------------------------

//==============================================================================
// Material preview build processing overlay
//==============================================================================

//==============================================================================
function MatLab::showBuildOverlay(%this)
{
	MatLabProcessingOverlay.fitIntoParents();
	show(MatLabProcessingOverlay);
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::hideBuildOverlay(%this)
{
	hide(MatLabProcessingOverlay);
}
//-----------------------------------------------------------------------------
