//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$MatLab_SortMode[1] = "Name ASC" TAB "Name 0";
$MatLab_SortMode[2] = "Name DESC" TAB "Name 1";
$MatLab_SortMode[3] = "Id ASC" TAB "Id 0";
$MatLab_SortMode[4] = "Id DESC" TAB "Id 1";

//==============================================================================
// Call the default sorting
function MatLab::initSortMenu(%this)
{
	MatLab_SortByMenu.clear();

	while ($MatLab_SortMode[%i++] !$= "")
	{
		%text = getField($MatLab_SortMode[%i],0);
		MatLab_SortByMenu.add(%text,%i);
	}

	%act = getField($MatLab_SortMode[$MatLab_SortModeId],0);
	MatLab_SortByMenu.setText(%act);
}
//------------------------------------------------------------------------------
//==============================================================================
// Call the default sorting
function MatLab_SortByMenu::onSelect(%this,%id,%text)
{
	%sortData = getField($MatLab_SortMode[%id],1);
	MatLab.sortBy(getWord(%sortData,0),getWord(%sortData,1));
}
//------------------------------------------------------------------------------

//==============================================================================
// Call the default sorting
function MatLab::sort(%this,%skipBuildPages)
{
	MatLabPreviewSet.sort("matLabSortPreview");

	if (!%skipBuildPages)
		%this.buildPages();
}
//------------------------------------------------------------------------------

//==============================================================================
// Sort the Materials based on specified type (Name or ID)
function MatLab::sortBy(%this,%type,%desc,%skipBuildPages)
{
	MatLab.sortMode = %type;
	MatLab.sortDown = %desc;

	MatLabPreviewSet.sort("matLabSortPreview");
	%this.buildPages();
}
//------------------------------------------------------------------------------
//MatLab_MaterialsPages.callOnChildrenNoRecurse("sort","matLabSortPreview");
//==============================================================================
// Compare all material preview with each other to decide the order
function matLabSortPreview(%objA,%objB)
{
	if (MatLab.sortMode $= "Name")
	{
		%result = mClamp(stricmp(%objA.text, %objB.text),-1,1);

		if (MatLab.sortDown)
			%result *= -1;

		return %result;

	}

	if (MatLab.sortMode $= "ID")
	{

		%result = (%objA.material.getId() > %objB.material.getId());

		if (!%result)
			%result = -1;

		if (MatLab.sortDown)
			%result *= -1;

		return %result;
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Compare all material preview with each other to decide the order
function matLabSortSetByName(%objA,%objB)
{
	return mClamp(stricmp(MatLab.getMatName(%objA), MatLab.getMatName(%objB)),-1,1);

}
//------------------------------------------------------------------------------

function MatLab::SortPages(%this)
{
	foreach (%ctrl in MatLab_MaterialsPages)
	{
		%ctrl.sort("matLabSortPreview");
		%ctrl.schedule(200,refresh);
	}
}
