//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================

function MatLab::clearFilterArray(%this)
{
	MatLabFiltersDynamicArray.deleteAllObjects();
}
//------------------------------------------------------------------------------

//==============================================================================
// Preload filter: Need to examine what it actually do
//==============================================================================

//==============================================================================
// Reset all filters to default state
function MatLab::resetMaterialFilters(%this)
{
	%this.setAllTagFiltersState(1,true);
}
//------------------------------------------------------------------------------
//==============================================================================
// Reset all filters to default state
function MatLabFilterSelectBtn::onClick(%this)
{
	%type = %this.text;

	switch$(%type)
	{
		case "All":
			MatLab.setAllTagFiltersState(1,1);

		case "None":
			MatLab.setAllTagFiltersState(0,1);

		case "Invert":
			MatLab.invertAllTagFiltersState(1);

	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Set all Materials filters to specified state (active or not)
function MatLab::setAllTagFiltersState(%this,%state,%rebuildPages)
{
	foreach (%ctrl in MatLabFiltersDynamicArray)
		$MatLab_FilterTag_[%ctrl.internalName] = %state;

	if (%rebuildPages)
		%this.buildPages();
}
//------------------------------------------------------------------------------
//==============================================================================
// Set all Materials filters to specified state (active or not)
function MatLab::invertAllTagFiltersState(%this,%rebuildPages)
{
	foreach (%ctrl in MatLabFiltersDynamicArray)
		$MatLab_FilterTag_[%ctrl.internalName] = !$MatLab_FilterTag_[%ctrl.internalName];

	if (%rebuildPages)
		%this.buildPages();
}
//------------------------------------------------------------------------------
//==============================================================================
// Set a comma-seperated list of filters to a state
function MatLab::setTagFiltersState(%this,%filters,%state,%rebuildPages)
{
	for (%i=0; %i<getCommaWordCount(%filters); %i++)
	{
		%filter = getCommaWord(%filters,%i);
		$MatLab_FilterTag_[%filter] = %state;
	}

	if (%rebuildPages)
		%this.buildPages();
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// create category and update current material if there is one

//==============================================================================
// Create a new filter that can be assign to materials
function MatLab::createFilter(%this,%filter)
{

	if (%filter $= "")
		%filter = MatLab_AddFilterName.getText();

	if (%filter $= "")
		%error = "Can't create a blank filter";
	else if (%this.isFilter(%filter))
		%error = "Filter already exist";

	if (%error !$= "")
	{
		warnLog("Add filter error",%error);
		return;
	}

	%this.addTagFilter(%filter);

	// if selection exists, lets reselect it to refresh it
	if (isObject(MatLab.selectedMaterial))
		MatLab.updateSelection(MatLab.selectedMaterial, MatLab.selectedPreviewImagePath);

	// material category text field to blank
	MatLab_AddFilterName.setText("");
	hide(MatLab_AddFilterBox);
}
//------------------------------------------------------------------------------

