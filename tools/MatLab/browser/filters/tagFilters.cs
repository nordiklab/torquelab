//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function MatLabActiveTagCheckbox::onClick(%this)
{
	%filter = strreplace(%this.internalName," Check","");
	matLog("Mat Checkbox filter clicked",%filter);
	MatLab.updateMatTag(MatLab.curMaterial,%filter,%this.isStateOn());
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLabFilterTagCheckbox::onClick(%this)
{
	%filter = strreplace(%this.internalName," Check","");
	matLog("Global Checkbox filter clicked",%filter);
	MatLabTagFilterArray.setVal(%filter,%this.isStateOn());

	// Buildpages will check the filtered state with getMatFilterState under
	MatLab.buildPages();

}
//------------------------------------------------------------------------------
//==============================================================================
// Set the material tag filters checkbox state
function MatLab::updateTagCheckboxes(%this)
{

	MatLabMaterialCategories.clear(); //Active material tag checkboxes
	MatLabFiltersDynamicArray.clear();//All material tag checkboxes
	MatLabTagFilterArray.setVal("Untagged",1);
	//Clone the array to only have unique keys
	MatLabTagFilterArray.uniqueKey();
	// sort the the keys before we do anything
	MatLabTagFilterArray.sortkd();

	//eval(MatLab.currentStaticFilter @ "Checkbox.setStateOn(1);");
	// it may seem goofy why the checkbox can't be instanciated inside the container
	// reason being its because we need to store the checkbox ctrl in order to make changes
	// on it later in the function.
	%selectedFilter = "";

	for (%i = 0; %i < MatLabTagFilterArray.count(); %i++)
	{
		%filter = MatLabTagFilterArray.getKey(%i);

		if (%filter $= "")
			continue;

		$MatLab_FilterTag_[%filter] = true;
		%container = MatLabFiltersDynamicArray.findObjectByInternalName(%filter);

		if (!isObject(%container))
			%container = MatLab.getFilterPillCtrl(%filter);

		%checkbox = %container.getObject(0);

		%isFiltered = getWordIndex(MatLab.currentFilter,%filter);

		if (%isFiltered $= "-1")
			continue;

		%checkbox.setStateOn(1);
	}

}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::addTagFilter(%this, %filter)
{
	%container = MatLabFiltersDynamicArray.findObjectByInternalName(%filter);

	if (isObject(%container))
		return;

	return MatLab.getFilterPillCtrl(%filter);
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::getFilterPillCtrl(%this, %filter)
{
	%control = cloneGui(MatLab_FilterPillSrc,MatLabFiltersDynamicArray);
	%checkbox = %control->check;
	%checkbox.text = %filter @ " ( " @ MatLabTagMaterialArray.countKey(%filter) @ " )";
	%checkbox.filter = %filter;
	%checkbox.superClass = "MatLabFilterTagCheckbox";
	%checkbox.internalName = %filter SPC "Check";
	%checkbox.variable = "$MatLab_FilterTag_"@%filter;
	//%checkbox.setStateOn(1);
	%control.internalName = %filter;

	//Clone it and adapt for active material tags
	%activeTag = cloneGui(%control,MatLabMaterialCategories);
	%checkbox = %activeTag.getObject(0);
	%checkbox.setStateOn(0);
	%checkbox.text = %filter;
	%checkbox.superClass = "MatLabActiveTagCheckbox";
	%checkbox.variable = "$MatLab_ActiveTag_"@%filter;
	return %control;
}
//------------------------------------------------------------------------------

//==============================================================================

//==============================================================================
function MatLab::extractMatTags(%this, %material)
{
	if (!isObject(%material))
		return;

	%name = %this.getMatName(%material);
	%this.fixMatTags(%material);

	%tags = %material.tags;

	if (%tags $= "")
	{
		MatLabTagFilterArray.setVal("Untagged",1);
		MatLabTagMaterialArray.add("Untagged",%material);
	}

	for (%i =0; %i < getCommaWordCount(%tags); %i++)
	{
		%tag = trim(getCommaWord(%tags,%i));
		MatLabTagFilterArray.setVal(%tag,1);
		MatLabTagMaterialArray.add(%tag,%material);
		matLog(%this.getMatName(%material),"Found Tag:",%tag);

		//MaterialFilterAllArray.add(%tag, %material.name);
	}

	matLog(%name,"Tags",%material.tags,"MatLab All Filter count",MatLabTagFilterArray.count());
}
//------------------------------------------------------------------------------
function MatLab::getFilterList(%this)
{
	foreach (%obj in MatLabFiltersDynamicArray)
	{
		%filter = %obj.internalName;

		if ($MatLab_FilterTag_[%filter])
			%list = strAddCommaWord(%list,%filter,1);
	}

	MatLab.currentFilters = %list;
	return %list;
}

function MatLab::isFilter(%this,%filter)
{
	foreach (%obj in MatLabFiltersDynamicArray)
	{
		if (%filter $= %obj.internalName)
			return true;
	}

	return false;
}

function MatLab::isFilter(%this,%filter)
{
	foreach (%obj in MatLabFiltersDynamicArray)
	{
		if (%filter $= %obj.internalName)
			return true;
	}

	return false;
}
