//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function MatLab::matBaseFilters(%this,%material)
{
	MatLabFullSet.add(%material);
	FullMaterialSetCheckbox.setText("All ( " @ MatLabFullSet.getCount() + 1 @ " ) ");

	if (%material.mapTo $= "unmapped_mat" || %material.mapTo $= "")
	{
		MatLabUnmappedSet.add(%material);
		UnmappedMaterialSetCheckbox.setText("Unmapped ( " @ MatLabUnmappedSet.getCount() + 1 @ " ) ");
	}
	else
	{
		MatLabMappedSet.add(%material);
		MappedMaterialSetCheckbox.setText("Mapped ( " @ MatLabMappedSet.getCount() + 1 @ " ) ");
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::setBaseFiltersCount(%this)
{
	//Update the filter checkbox and add group count referemce
	FullMaterialSetCheckbox.setText("All ( " @ MatLabFullSet.getCount() @" ) ");
	MappedMaterialSetCheckbox.setText("Mapped ( " @ MatLabMappedSet.getCount() @" ) ");
	UnmappedMaterialSetCheckbox.setText("Unmapped ( " @ MatLabUnmappedSet.getCount() @" ) ");
}
//------------------------------------------------------------------------------

//==============================================================================
// Static CHeckbox Clicks
function MatLab::switchStaticFilters(%this,%staticFilter)
{
	switch$(%staticFilter)
	{
		case "Full":
			FullMaterialSetCheckbox.setStateOn(1);
			MappedMaterialSetCheckbox.setStateOn(0);
			UnmappedMaterialSetCheckbox.setStateOn(0);

		case "Mapped":
			MappedMaterialSetCheckbox.setStateOn(1);
			FullMaterialSetCheckbox.setStateOn(0);
			UnmappedMaterialSetCheckbox.setStateOn(0);

		case "Unmapped":
			UnmappedMaterialSetCheckbox.setStateOn(1);
			FullMaterialSetCheckbox.setStateOn(0);
			MappedMaterialSetCheckbox.setStateOn(0);
	}

	MatLab.activeMatSet = "MatLab"@%staticFilter@"Set";
	MatLab.currentStaticFilter = %staticFilter;
	MatLab.buildPages();
	matLog("Static Set Activated:",MatLab.activeMatSet.getName());
}
//==============================================================================
// Build materials filters data by checking all materials and adding it to filter groups
//MatLab.buildStaticFilters();

