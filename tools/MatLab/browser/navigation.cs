//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function MatLab::lastPage(%this)
{
	%this.loadPage( MatLab.pageCount);
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::nextPage(%this)
{
	%this.loadPage(MatLab.page + 1);
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::prevPage(%this)
{
	%this.loadPage(MatLab.page - 1);
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::nextPage(%this)
{
	%this.loadPage(MatLab.page + 1);
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::cyclePage(%this)
{
	%nextPage = MatLab.page + 1;

	if (%nextPage > MatLab.pageCount)
		%nextPage = 1;

	%this.loadPage(%nextPage);
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::loadPage(%this,%id)
{
	if ($MatLab_MegaPageContainer)
		return loadMegaPage(%id);

	%id = mClamp(%id,1,MatLab.pageCount);
	MatLab_PageButtonsStack.callOnChildrenNoRecurse("setActive",1);
	%pageButton = MatLab_PageButtonsStack.findObjectByInternalName(%id);

	if (isObject(%pageButton))
		%pageButton.active = 0;

	if (!isObject(MatLab.pageContainer[%id]))
		return;

	MatLab_MaterialsPages.callOnChildrenNoRecurse("setVisible",0);
	MatLab.pageContainer[%id].visible = 1;
	MatLab.page = %id;
	MatLab.pageCtrl = MatLab.pageContainer[%id];
}
//------------------------------------------------------------------------------

