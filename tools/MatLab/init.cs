//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// MatLab Universal Globals
//==============================================================================
$MatLab_MaterialPBRMode = true;
$MatLab_Properties = "mapTo doubleSided dynamicCubemap cubemap castShadows castDynamicShadows" SPC
                     "translucentBlendOp translucentZWrite translucent planarReflection alphaTest alphaRef" SPC
                     "showDust showFootprints customFootstepSound customImpactSound impactSoundId footstepSoundId";
$MatLab_TextureFormats =
   $MatLab_MaterialFileSpecs = "Torque Material Files (materials.cs)|materials.cs|All Files (*.*)|*.*|";
$MatLab_TextureFormats = "Image Files (*.png, *.jpg, *.dds, *.bmp, *.gif, *.jng. *.tga)|*.png;*.jpg;*.dds;*.bmp;*.gif;*.jng;*.tga|All Files (*.*)|*.*|";
//------------------------------------------------------------------------------
// Supported Texture Maps List (Non-PBR and PBR list - Can't simply append PBR as the order is important)
//------------------------------------------------------------------------------

//Keep the 2 lists accessible
$MatLab_MaterialMaps["Default"] = "diffuse normal specular detail detailNormal light overlay tone";
$MatLab_MaterialMaps["PBR"] = "diffuse normal specular rough ao metal detail detailNormal light overlay tone compositeDamage albedoDamage normalDamage";

//Official maps list based on PBR mode active or not
if ($MatLab_MaterialPBRMode)
	$MatLab_MaterialMaps = $MatLab_MaterialMaps["PBR"];
else
	$MatLab_MaterialMaps = $MatLab_MaterialMaps["Default"];

//------------------------------------------------------------------------------
// Material Browser Specific Globals
//------------------------------------------------------------------------------
$MatLab_ActiveMatProperties = "mapTo doubleSided dynamicCubemap cubemap castShadows castDynamicShadows" SPC
                              "translucentBlendOp translucentZWrite translucent planarReflection alphaTest alphaRef" SPC
                              "";//"showDust showFootprints customFootstepSound customImpactSound impactSoundId footstepSoundId";
$MatLab_ThumbPerPage[0] = "20";
$MatLab_ThumbPerPage[1] = "40";
$MatLab_ThumbPerPage[2] = "75";
$MatLab_ThumbPerPage[3] = "100";
$MatLab_ThumbPerPage[4] = "150";
$MatLab_ThumbPerPage[5] = "200";
$MatLab_ThumbPerPage[6] = "All";

$MatLab_ThumbSize[0] = "32";
$MatLab_ThumbSize[1] = "48";
$MatLab_ThumbSize[2] = "64";
$MatLab_ThumbSize[3] = "80";
$MatLab_ThumbSize[4] = "96";
$MatLab_ThumbSize[5] = "128";
$MatLab_ThumbSize[6] = "160";

$MatLab_ThumbSizes = "32 48 64 80 96 128 160";
$MatLab_ThumbCountPerPages = "20 40 75 100 150 200 All";

$MatLab_ThumbPerPage = "40";
$MatLab_ThumbSize = "64";

$MatLab_SortMode = "Name";
$MatLab_SortDown = "0";
$MatLab_SortModeId = "1";

$MatLab_ColorBG = "128 128 128 255";
//==============================================================================
// MatLab Default Globals
$Cfg_MatLab_DefaultMaterialFile = "art/textures/customMaterials.cs";

$MatLab_NoTextureBmp = "tlab/art/assets/no_texture.png";
$MatLab_MissingTextureBmp = "tlab/art/assets/missing_texture.png";
$MatLab_DefaultMaterialFile = "art/textures/customMaterials.cs";
$MatLab_CurrentStaticFilter = "MaterialFilterAllArray";
$MatLab_CurrentFilter = ""; //ALL

$MatLab_MapFilters = "Image Files (*.png, *.jpg, *.dds, *.bmp, *.gif, *.jng. *.tga)|*.png;*.jpg;*.dds;*.bmp;*.gif;*.jng;*.tga|All Files (*.*)|*.*|";
$MatLab_Log = 0;
$MatLab_FilterLog = 0;
$MatLab_AutoTags= 1;
$MatLab_LogPM = 1;
$MatLab_DialogMode = 0;

$MatLab_PreviewAuto = 1;

$MatLab_AdvancedImageSearch = 0;
$MatLab_MegaPageContainer = 0;
//------------------------------------------------------------------------------

function MatLab::create(%this)
{
   info("Tool","->","Initializing","Nav Editor");
   execGuiDir("tlab/tools/MatLab/",true);
		execPattern("tlab/tools/MatLab/*.cs");
		
   initMatLab();
}
function MatLab::destroy(%this)
{
   
}
//==============================================================================
// Create the MatLab Object manager and exec the files
function initMatLab()
{
	
	//Lab.newObjLab("MatLab");
	MatLab.logPM = $MatLab_PMLog;

	//Unused
	%path = "tlab/tools/MatLab/";
	//Exec all .gui and .cs files found
	//execGuiDir(%path TAB "_off",true);
	//execPattern("tlab/tools/MatLab/*.cs","_off");
	MaterialSelector.visible = 0;
	//Lab.addGui(MaterialSelector,"EditorBox")
	Lab.addGui(MaterialSelector,"FullBox");

	//Material used to preview other materials in the editor.
	if (!isObject(MatLab_previewMaterial))
		singleton Material(MatLab_previewMaterial)
	{
		mapTo = "MatLab_mappedMat";
		diffuseMap[0] = "tlab/art/assets/matEd_mappedMat";
	};

	if (!isObject(MatLab_notDirtyMaterial))
		singleton Material(MatLab_notDirtyMaterial)
	{
		mapTo = "MatLab_previewMat";
		diffuseMap[0] = "tlab/art/assets/matEd_dirtyMat";
	};

}
//------------------------------------------------------------------------------

//==============================================================================
// Add materials that shouldn't be listed here
//------------------------------------------------------------------------------
if (!isObject(UnlistedMaterials))
{
	new ArrayObject(UnlistedMaterials);
	UnlistedMaterials.add("unlistedMaterials", WarningMaterial);
	UnlistedMaterials.add("unlistedMaterials", materialEd_previewMaterial);
	UnlistedMaterials.add("unlistedMaterials", notDirtyMaterial);
	UnlistedMaterials.add("unlistedMaterials", materialEd_cubemapEd_cubeMapPreview);
	UnlistedMaterials.add("unlistedMaterials", matEdCubeMapPreviewMat);
	UnlistedMaterials.add("unlistedMaterials", materialEd_justAlphaMaterial);
	UnlistedMaterials.add("unlistedMaterials", materialEd_justAlphaShader);
}

//------------------------------------------------------------------------------

