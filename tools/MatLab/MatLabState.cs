//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function MatLab::activate(%this)
{
	MatLab.showMaterialCreator(0);
	hide(MatLab_AddFilterBox);
	MatLab_MainBook.selectPage(0);
	MatLab.updateMaterialPreviewType("sphere");

	//If no Preview Set existing, we need to build previews for first time
	if (!isObject(MatLabPreviewSet))
		MatLab.schedule(1000,buildPreviews);
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::deactivate(%this)
{
	$MatLab_CurrentStaticFilter = MatLab.currentStaticFilter;
	MatLab.getFilterList();
	$MatLab_CurrentFilters = MatLab.currentFilters;

	//MatLab.disablePreview();
	MatLab.selectCallback = "";
	MatLab.returnType = "";
	MatLab.terrainMaterials = "";
}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::closeDialog(%this,%doReturnCheck)
{
	//Select and Close - Return selected material if callback specified
	if (%doReturnCheck && isObject(%this.selectedMaterial))
		%this.returnMaterial(%this.selectedMaterial);

	if ($MatLab_DialogMode)
		Canvas.popDialog(LabMaterialBrowser);
	else
		MaterialSelector.visible = 0;
}
//------------------------------------------------------------------------------

//==============================================================================
// Show the Material browser with optional return selected callback by type  which
// would be called when "select and close" pressed. (if none only close will be on)
// %selectCallback = The callback function usedwhen selected without (%mat);
// %returnType = How to return the Material : Name, index or default: Mat Object
// %useTerrainMaterials = if true, terrainMaterial will be listed
function MatLab::showBrowser(%this, %selectCallback, %returnType, %useTerrainMaterials)
{
	MatLab.selectCallback = %selectCallback;
	MatLab.returnType = %returnType;
	MatLab.terrainMaterials = %useTerrainMaterials;

	MatLab.setListFilterText("");
	hide(MatSel_SetAsActiveContainer);

	//If MaterialEditor Plugin is active, show the Set As Active button
	if (isObject(MaterialEditorTools))
		if (MaterialEditorTools.isAwake())
			show(MatSel_SetAsActiveContainer);

	// Set the select callback
	MatLab.selectCallback = %selectCallback;
	MatLab.returnType = %returnType;
	MatLab.currentStaticFilter = $MatLab_CurrentStaticFilter;
	MatLab.currentFilter = $MatLab_CurrentFilter;
	MatLab.terrainMaterials = %useTerrainMaterials;
	MatLab.selectedMaterial = "";
	//MatLab-->materialPreviewCountPopup.clear();

	MatLab.uiSetup();

	if ($MatLab_DialogMode)
	{
		pushDlg(LabMaterialBrowser,0);
	}

	MaterialSelector.visible = 1;
	MaterialSelector.forceInsideParent();

}
//------------------------------------------------------------------------------
//==============================================================================
// Built-In callback used by inspector
function MaterialSelector::showDialog(%this, %selectCallback, %returnType, %useTerrainMaterials)
{
	MatLab.showBrowser(%selectCallback, %returnType, %useTerrainMaterials);
}

//==============================================================================
// MatLab - Material Selection for external components
//==============================================================================

//==============================================================================
// Material selection function using a clearer naming for outside use
function Lab::selectMaterial(%this, %selectCallback, %returnType,%useTerrainMaterials)
{
	if (%selectCallback $= "")
	{
		warnLog("You need to specify a callback function to return material to","Lab.selectMaterial(%selectCallback, %returnType,%useTerrainMaterials);");
		warnLog("%selectCallback","Function to which Material will be returned as first and only argument","EX:","Tools.setMaterial","Return","Tools.setMaterial(MATERIAL);");
		warnLog("%returnType","How the material should be returned. (ID by default)","Options:","Name (Material object name)","Index (Index in MaterialSet or ETerrainEditor material for terrain");
		warnLog("%useTerrainMaterials","Set true if a TerrainMaterial is wanted. Leave blank or false otherwise");
		return;
	}

	MatLab.showBrowser(%selectCallback, %returnType, %useTerrainMaterials);
}
//------------------------------------------------------------------------------

//==============================================================================
// Return selected material using returnCallback and returnType
//==============================================================================

//==============================================================================
function MatLab::returnMaterial(%this, %material)
{
	if (MatLab.terrainMaterials && !isObject(%material))
		%material = TerrainMaterialSet.findObjectByInternalName(%material);

	if (MatLab.selectCallback $= "" || !isObject(%material)  )
		return;

	switch$(MatLab.returnType)
	{
		case "index":
			%returnVal = %this.getMaterialIndex(%material);

		case "name":
			%returnVal = %this.getMatName(%material);

		default:
			%returnVal = %material.getId();
	}

	eval("" @ MatLab.selectCallback @ "(" @ %returnVal  @ ");");

	info(%material,"Returned","\c2(type:\c6"@MatLab.returnType@"\c2)\c0" SPC %returnVal,"to callback:","" @ MatLab.selectCallback @ "(" @ %returnVal  @ ");");

}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::getMaterialIndex(%this, %material)
{
	//get the name of material. For terrainMaterial the internalName is returned
	%name = %this.getMatName(%material);
	%index = -1;

	if (MatLab.terrainMaterials)
	{
		// Obtain the index into the terrain's material list
		%mats = ETerrainEditor.getMaterials();

		for (%i = 0; %i < getRecordCount(%mats); %i++)
		{
			%matInternalName = getRecord(%mats, %i);

			if (%matInternalName $= %name)
			{
				%index = %i;
				break;
			}
		}
	}
	else
	{
		%name = %this.getMatName(%material);

		// Obtain the index into the material set
		for (%i = 0; %i < materialSet.getCount(); %i++)
		{
			%obj = materialSet.getObject(%i);

			if (%obj.getName() $= %name)
			{
				%index = %i;
				break;
			}
		}
	}

	return %index;
}
//------------------------------------------------------------------------------

