//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function MatLab::setMaterialTextureMap(%this,%material,%map,%texture,%layer)
{
	if (%layer $= "")
		%layer = 0;

	if (!isObject(%material) || !isImageFile(%texture)|| %map $= "" || %layer < 0 || %layer > 4)
		return false;

	//The function support map with the Map suffix or not, just remove it if exist
	%map = strReplace(%map,"Map","");
	%map = %map@"Map";

	if (!%material.isField(%map))
	{
		warnLog("Trying to set invalid Material Map:",%map,"for material", %material.getName());
		return false;
	}

	%this.set(%material,%map SPC %layer,%texture);

	return true;
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::setMaterialName(%this,%mat,%name)
{
	if (!isObject(%mat))
		return false;

	if (isObject(%name))
	{
		labMsgOk("Object with same name exist","There's already an object using name:\c1" SPC	%name SPC "\c0. Please make sure to choose a unique name");
		return false;
	}

	%mat.setName(%name);
	%this.setDirty(%mat,1);

	return true;
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::setMaterialFile(%this, %material, %newFile)
{
	if (!isObject(%material))
		return false;

	%previousFile = %material.getFileName();
	%material.setFileName(%newFile);

	//Only remove from old file once it will be saved, else it would be gone if not saved
	%this.postSave[%material.getId()] = "MatLab.removeFromFile("@%material.getId()@",\""@%previousFile@"\");";

	//%this.removeFromFile(%material,%previousFile);
	%this.setDirty(%material,1);
	MatLab_ActivePropertyPage-->filename.setText(%material.getFileName());
	return true;
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::deleteMaterial(%this, %material,%mustConfirm)
{
	if (!isObject(%material))
		return false;

	if (%mustConfirm)
	{
		LabMsgYesNo("Delete Material?",
		            "Are you sure you want to delete<br><br>" @ %material.getName() @ "<br><br> Material deletion won't take affect until the engine is quit.",
		            "MatLab.deleteMaterial( " @ %material @" );",
		            "");
		return;
	}

	//TODO: Check tags to see if it was alone and remove it if soo
	%this.unlistMaterial(%materialName);

	%file = %materialName.getFilename();

	if (!isFile(%file))
		%error = "Can't remove material from it's file as it's not a valid file";

	if (getSubStr(%file,0,5) $= "tlab/")
		%error = "Can't remove material form a TorqueLab Material file";

	if (%error !$= "")
	{
		errorLog(%error,%file);
		return;
	}

	Lab_PM.removeObjectFromFile(%materialName);
	Lab_PM.saveDirty();

	info(%materialName,"Removed from it's file so won't be loaded next time","File:",%file);
	return true;
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::setMaterialUnlisted(%this, %material)
{
	if (!isObject(%material))
		return false;

	//The actual material instance is not deleted, only it's declaration in file
	// so we simply add it to unlisted material so it preview is no longer created
	UnlistedMaterials.add("unlistedMaterials", %material);
	return true;

}
//------------------------------------------------------------------------------
//==============================================================================
// Add/Remove a tag to/from a material
function MatLab::updateMatTag(%this, %material, %tag, %add)
{
	if (%tag $= "" || !isObject(%material))
		return;

	%tags = %material.tags;
	%stags = %tags;

	if (!isObject(MatLabTagsArray))
		new ArrayObject(MatLabTagsArray);

	// Add or remove a tag from material
	if (%add)
	{
		MatLabTagsArray.add(%material SPC %tag,%this.getMatName(%material));

		if (!%material.isTag(%tag))
			%tags = strAddCommaWord(%tags,%tag);
		else
			return;
	}
	else
	{
		MatLabTagsArray.eraseKey(%material SPC %tag);

		if (!%material.isTag(%tag))
			return;

		%tags = strRemoveCommaWord(%tags,%tag);

	}

	info(%this.getMatName(%material),"Tag:",%tag,"Action",%add,"Result",%material.tags);
	%this.set(%material,"tags",%tags);
}
//------------------------------------------------------------------------------
