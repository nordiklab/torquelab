//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Sort the Materials based on specified type (Name or ID)
function matLog(%text,%a1, %a2, %a3, %a4,%a5, %a6, %a7, %a8,%a9, %a10, %a11, %a12)
{
	if ($MatLab_Log)
		info(%text,%a1, %a2, %a3, %a4,%a5, %a6, %a7, %a8,%a9, %a10, %a11, %a12);

}
//------------------------------------------------------------------------------

//==============================================================================
// MatLab Callbacks - Use by the ObjLab class when completing an action
//==============================================================================

//==============================================================================
// Called from ManagerLab when DirtyState changed on an object
function MatLab::onObjectDirtyChanged(%this,%obj,%dirty)
{
	matLog("MatLab::onObjectDirtyChanged(%obj,%dirty)",%obj,%dirty);

	//The rest is only for Selected Material
	if (%obj != MatLab.selectedMaterial)
		return;

	MatLab_SaveSelectedBtn.active = %dirty;
}
//------------------------------------------------------------------------------

//==============================================================================
// Called from ManagerLab when DirtyState changed on an object
function MatLab::onObjectFieldChanged(%this,%obj,%field,%value,%initialValue)
{
	matLog("MatLab::onObjectFieldChanged(%obj,%field,%value,%initialValue)",%obj,%field,%value,%initialValue);

	//The rest is only for Selected Material
	if (%obj != MatLab.selectedMaterial)
		return;

	switch$(%field)
	{
		case "name":
			matLog("Name changed to:",%value,"Old name:", %initialValue);
	}

	MatLab_SaveSelectedBtn.active = %isDirty;
}
//------------------------------------------------------------------------------
//==============================================================================
// Called from ManagerLab when DirtyState changed on an object
function MatLab::onObjectSaved(%this,%obj)
{
	matLog("MatLab::onObjectSaved(%obj)",%obj);

	if (%this.postSave[%obj.getId()] !$= "")
	{
		info("PostSave Eval",%this.postSave[%obj.getId()]);
		eval(%this.postSave[%obj.getId()]);
	}

	%this.postSave[%obj.getId()] = "";
	//%this.onObjectSetDirty(%obj,0);
}
//------------------------------------------------------------------------------
//==============================================================================
// Called from ManagerLab when DirtyState changed on an object
function MatLab::onObjectFieldRemoved(%this,%obj,%field)
{
	matLog("MatLab::onObjectFieldRemoved(%obj,%field)",%obj,%field);
}
//------------------------------------------------------------------------------
//==============================================================================
// Called from ManagerLab when DirtyState changed on an object
function MatLab::onObjectDirtySaved(%this,%obj)
{
	matLog("MatLab::onObjectDirtySaved(%obj)",%obj);
	//%this.onObjectSetDirty(%obj,0);
}
//------------------------------------------------------------------------------
//==============================================================================
// Called from ManagerLab when DirtyState changed on an object
function MatLab::onObjectSetDirty(%this,%obj,%dirty)
{
	matLog("MatLab::onObjectSetDirty(%obj)",%obj,%dirty);

	if (%obj != MatLab.selectedMaterial)
		return;

	MatLab_SaveSelectedBtn.active = %dirty;
}
//------------------------------------------------------------------------------
