//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function MatLab::storePages(%this)
{
	foreach (%ctrl in MatLab_MaterialsPages)
	{
		%ctrl.canSave = 1;

		foreach (%child in %ctrl)
			%child.canSave = 1;

	}

	MatLab_MaterialsPages.save("tlab/tools/BuildMat.gui");

	foreach (%ctrl in MatLab_MaterialsPages)
	{
		%ctrl.canSave = 0;

		foreach (%child in %ctrl)
			%child.canSave = 0;
	}
}

function MatLab::restorePages(%this)
{
	MatLab_MaterialsPages.deleteAllObjects();
	exec("tlab/tools/BuildMat.gui");

	foreach (%ctrl in MatLab_MaterialsPages_Back)
	{
		%ids = strAddWord(%ids,%ctrl.getId());

	}

	foreach$(%id in %ids)
		MatLab_MaterialsPages.add(%id);

	MatLab_MaterialsPages_Back.delete();

}
//------------------------------------------------------------------------------
