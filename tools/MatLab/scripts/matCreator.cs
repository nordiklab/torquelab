//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function MatLab::showMaterialCreator(%this,%state)
{
	MatLab_Creator.visible = %state;
	MatLab_SelectPreview.visible = !%state;

	if (%state)
	{
		MatLab_CloneMatList.clear();

		foreach (%mat in MatLabFullSet)
		{
			%name = %this.getMatName(%mat);
			MatLab_CloneMatList.add(%name,%mat.getId());
		}

		MatLab_CloneMatList.setText(%this.getMatName(MatLab.selectedMaterial));
	}
}
function MatLab::blankCreatorMode(%this,%state)
{
	$MatLabCreator_BlankMat = %state;
	MatLab_CloneMatList.active = !%state;

	if (%state)
	{
		MatLab_CloneMatName.setText("NewMaterial");
		MatLab_CloneMatList.setText("Blank Material");
		MatLab_CloneMatFile.setText("art/materials.cs");
	}
	else if (isObject(MatLab.selectedMaterial))
	{
		%cloneName = %this.getMatName(MatLab.selectedMaterial);
		MatLab_CloneMatName.setText(%cloneName@"_copy");
		MatLab_CloneMatList.setText(%cloneName);
		MatLab_CloneMatFile.setText(MatLab.selectedMaterial.getFileName());

	}
	else
	{
		MatLab_CloneMatName.setText("NewMaterial");
		MatLab_CloneMatList.setText("[Choose source]");
		MatLab_CloneMatFile.setText("art/materials.cs");
	}

}

//------------------------------------------------------------------------------
// this should create a new material pretty nicely
function MatLab::createMaterial(%this,%cloneSelected)
{
	%this.showMaterialCreator(true);

	if (%cloneSelected && isObject(MatLab.selectedMaterial))
	{
		%this.blankCreatorMode(0);

	}
	else
		%this.blankCreatorMode(1);

}
//------------------------------------------------------------------------------
// this should create a new material pretty nicely
function MatLab::processNewMaterial(%this,%cloneSelected)
{
	$MatLab_AutoAddDisabled = true;

	if (!$MatLabCreator_BlankMat)
	{
		%source = MatLab_CloneMatList.getText();

		if (!isObject(%source))
			return;

		%newMat = %this.getMaterialClone(%source,MatLab_CloneMatName.getText(),MatLab_CloneMatFile.getText());
	}
	else
	{
		%newMat = new Material(%matName)
		{
			diffuseMap[0] = "core/art/warnMat";
			mapTo = "unmapped_mat";
			parentGroup = RootGroup;
		};
	}

	$MatLab_AutoAddDisabled = false;

	if (!isObject(%newMat))
		return;

	MatLab.setMatDirty(%newMat,true);
	//We need to call a refresh for preview since it was added before name changed
	%this.buildMat(%newMat);
	%this.showMaterialCreator(false);
	%this.schedule(200,setSelectedMaterial,%newMat);
}
//------------------------------------------------------------------------------
// this should create a new material pretty nicely
function MatLab::cancelNewMaterial(%this)
{
	%this.showMaterialCreator(false);
}
//==============================================================================
// Clone selected Material
function MatLab::getMaterialClone( %this,%sourceMaterial,%newName,%newFile )
{
	if (!isObject(%sourceMaterial))
		return;

	if (%newName $= "")
		%newName = %sourceMaterial.getName();

	if (%newFile $= "")
		%newFile = %sourceMaterial.getFilename();

	%newMat = %sourceMaterial.deepClone();
	%newName = getUniqueName(%newName);
	%newMat.setName(%newName);
	%newMat.setFilename(%newFile);
	return %newMat;

}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
function MatLabCreatorEdit::onValidate(%this)
{
	%text = %this.getText();
	%field = %this.internalName;

	if (%field $= "name")
	{
		%uniqueName = getUniqueName(%text);

		if (%text !$= %uniqueName)
		{
			warnLog("Name must be unique");
			%this.setText(%uniqueName);
		}
	}
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
function MatLab_CloneMatList::onSelect(%this,%id,%name)
{
	MatLab_CloneMatName.setText(%name@"_copy");
	MatLab_CloneMatFile.setText(%id.getFileName());
}
//------------------------------------------------------------------------------
/*
//==============================================================================
function MatLab_Creator::onWake(%this)
{
   LabMaterialBrowser.pushToBack(MatLab_Creator);
	%menu = MatLab_CloneMatList;
	%menu.clear();
	%menu.add("Blank Material",0);
	%count = materialSet.getCount();

	for(%i = 0; %i < %count; %i++)
	{
		// Process regular materials here
		%material = materialSet.getObject(%i);

		for(%k = 0; %k < UnlistedMaterials.count(); %k++)
		{
			%unlistedFound = 0;

			if (UnlistedMaterials.getValue(%k) $= %material.name)
			{
				%unlistedFound = 1;
				break;
			}
		}

		if (%unlistedFound)
			continue;

		%menu.add(%material.getName(),%material.getId());
	}

	%selected = 0;

	if (isObject(MatLab.selectedMaterial))
		%selected = MatLab.selectedMaterial.getId();

	%menu.setSelected(%selected);
}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab_CloneMatList::onSelect(%this,%id,%name)
{
	if (%id $= "0" || !isObject(%id))
	{
		%file = $Cfg_MatLab_DefaultMaterialFile;
		%name = "NewMaterial";
	}
	else
	{
		%file = %id.getFilename();

		if (!isFile(%file))
			%file = $Cfg_MatLab_DefaultMaterialFile;

		%name = %id.getName()@"_clone";
	}

	MatLab_CloneMatName.setText(%name);
	MatLab_CloneMatFile.setText(%file);
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::CreateNewMaterialDlg(%this)
{
	%src = MatLab_CloneMatList.getText();

	if (!isObject(%src))
		%createBlank = true;

	%name = MatLab_CloneMatName.getText();

	if (%name $= "")
		return;

	%matName = getUniqueName(%name);

	if (%createBlank)
	{
		new Material(%matName)
		{
			diffuseMap[0] = "core/art/warnMat";
			mapTo = "unmapped_mat";
			parentGroup = RootGroup;
		};
	}

	%file = MatLab_CloneMatFile.getText();
	%material.setFilename($Cfg_MatLab_DefaultMaterialFile);
	hide(MatLab_Creator);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// this should create a new material pretty nicely
function MatLab::createNewMaterial(%this)
{
	// look for a newMaterial name to grab
	%material = getUniqueName("newMaterial");
	new Material(%material)
	{
		diffuseMap[0] = "art/textures/core/warnMat";
		mapTo = "unmapped_mat";
		parentGroup = RootGroup;
	};
	// add one to All filter
	MatLab.matBaseFilters(%material);

	%material.setFilename($Cfg_MatLab_DefaultMaterialFile);
	// select me
	MatLab.updateSelection(%material, "art/textures/core/warnMat.png");
	MatLab.buildPreviewArray(%material);
}
//------------------------------------------------------------------------------
*/
