//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function MatLab::SaveAndCloseCubemap(%this)
{
	%cubemap = MatLab.currentCubemap;

	if (!isObject(%cubemap))
		return;

	if ( MatLab.isDirty(%cubemap) )
		MatLab.saveCubemap(%cubemap);

	MatLab.hideCubemapEditor();
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::selectCubemap(%this)
{
	%cubemap = MatLab.currentCubemap;

	if (!isObject(%cubemap))
		return;

	MatLab.updateActiveMaterial( "cubemap", %cubemap.name );
	MatLab.hideCubemapEditor();
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::cancelCubemap(%this)
{
	%cubemap = MatLab.currentCubemap;

	if (isObject(%cubemap))
	{
		if ( MatLab.isDirty(%cubemap) )
		{
			LabMsgYesNo("Unsaved changes on "@ %cubemap.getName(),
			            "Do you want to save changes to <br><br>" @ %cubemap.getName(),
			            "MatLab.saveCubemap(" @ %cubemap @ ");");

		}
	}

	%idx = MatLabCubemapEditorList.findItemText( %cubemap.getName() );
	MatLabCubemapEditorList.setItemText( %idx, MatLab_notDirtyCubemap.originalName );
	%cubemap.setName( MatLab_notDirtyCubemap.originalName );
	MatLab.copyCubemaps( MatLab_notDirtyCubemap, %cubemap );
	MatLab.copyCubemaps( MatLab_notDirtyCubemap, matEdCubeMapPreviewMat);
	%cubemap.updateFaces();
	matEdCubeMapPreviewMat.updateFaces();
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::showCubemapEditor(%this,%selectMode)
{
	pushDlg(MatLabCubemapEditor);

	MatLab.currentCubemap = "";
	LabCubemapEditor.setVisible(1);

	MatLabCubemapEdSaveCloseBtn.visible = !%selectMode;
	MatLabCubemapEdSelectBtn.visible = %selectMode;
	MatLab.setCubemapNotDirty();

	//Use the Cubemap list generated from initial MatLab material search
	foreach(%cubemap in MatLabCubemapSet)
	{
		MatLabCubemapEditorList.addItem( %cubemap.name );
	}

	singleton CubemapData(MatLab_notDirtyCubemap);

	// if there was no cubemap, pick the first, select, and bail, these are going to take
	// care of themselves in the selected function
	if ( !isObject( MatLab.currentMaterial.cubemap ) )
	{
		if ( MatLabCubemapEditorList.getItemCount() > 0 )
		{
			MatLabCubemapEditorList.setSelected(0, true);
			return;
		}
		else
		{
			// if there are no cubemaps, then create one, select, and bail
			%cubemap = MatLab.createNewCubemap();
			MatLabCubemapEditorList.addItem( %cubemap.name );
			MatLabCubemapEditorList.setSelected(0, true);
			return;
		}
	}

	// do not directly change activeMat!
	MatLab.currentCubemap = MatLab.currentMaterial.cubemap.getId();
	%cubemap = MatLab.currentCubemap;
	MatLab_notDirtyCubemap.originalName = %cubemap.getName();
	MatLab.copyCubemaps( %cubemap, MatLab_notDirtyCubemap);
	MatLab.copyCubemaps( %cubemap, matEdCubeMapPreviewMat);
	MatLab.syncCubemap( %cubemap );
}
//------------------------------------------------------------------------------
/*
	//Add the level specific World group to the check
	%checkGroups = "RootGroup";
	foreach$(%group in %checkGroups)
	{
		for( %i = 0; %i < %group.getCount(); %i++ )
		{
			if ( %group.getObject(%i).getClassName()!$= "CubemapData" )
				continue;

			for( %k = 0; %k < UnlistedCubemaps.count(); %k++ )
			{
				%unlistedFound = 0;

				if ( UnlistedCubemaps.getValue(%k) $= %group.getObject(%i).name )
				{
					%unlistedFound = 1;
					break;
				}
			}

			if ( %unlistedFound )
				continue;

			MatLabCubemapEditorList.addItem( %group.getObject(%i).name );
		}
	}*/
//==============================================================================
function MatLab::hideCubemapEditor(%this,%cancel)
{
	if (%cancel)
		MatLab.cancelCubemap();

	MatLabCubemapEditorList.clearItems();
	//MatLabCubemapEd_PerMan.delete();
	LabCubemapEditor.setVisible(0);
}
//------------------------------------------------------------------------------
//==============================================================================
// create category and update current material if there is one
function MatLab::addCubemap( %this,%cubemapName )
{
	if ( %cubemapName $= "" )
	{
		LabMsgOK( "Error", "Can not create a cubemap without a valid name.");
		return;
	}

	for(%i = 0; %i < RootGroup.getCount(); %i++)
	{
		if ( %cubemapName $= RootGroup.getObject(%i).getName() )
		{
			LabMsgOK( "Error", "There is already an object with the same name.");
			return;
		}
	}

	// Create and select a new cubemap
	%cubemap = MatLab.createNewCubemap( %cubemapName );
	%idx = MatLabCubemapEditorList.addItem( %cubemap.name );
	MatLabCubemapEditorList.setSelected( %idx, true );
	// material category text field to blank
	MatLabCubemapCreateWindow-->cubemapName.setText("");
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::createNewCubemap( %this, %cubemap )
{
	if ( %cubemap $= "" )
	{
		for(%i = 0; ; %i++)
		{
			%cubemap = "newCubemap_" @ %i;

			if ( !isObject(%cubemap) )
				break;
		}
	}

	new CubemapData(%cubemap)
	{
		cubeFace[0] = "tlab/plugins/materialEditor/assets/cube_xNeg";
		cubeFace[1] = "tlab/plugins/materialEditor/assets/cube_xPos";
		cubeFace[2] = "tlab/plugins/materialEditor/assets/cube_ZNeg";
		cubeFace[3] = "tlab/plugins/materialEditor/assets/cube_ZPos";
		cubeFace[4] = "tlab/plugins/materialEditor/assets/cube_YNeg";
		cubeFace[5] = "tlab/plugins/materialEditor/assets/cube_YPos";
		parentGroup = RootGroup;
	};
	MatLab.saveObj(%cubemap);
	//MatLabCubemapEd_PerMan.setDirty( %cubemap, "art/materials.cs" );
	//MatLabCubemapEd_PerMan.saveDirty();
	return %cubemap;
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::setCubemapDirty(%this,%onlyGuiUpdate)
{

	LabCubemapEditor.text = "Create Cubemap *";
	LabCubemapEditor.text = %propertyText;
	LabCubemapEditor.dirty = true;
	LabCubemapEditor-->saveCubemap.setActive(true);

	if (%onlyGuiUpdate)
		return;

	%cubemap = MatLab.currentCubemap;

	// materials created in the materail selector are given that as its filename, so we run another check
	//Simple, if no filename set or if the filename is under tlab/ we set the default file to store CubeMap
	if ( MatLab.isMatEditorMaterial( %cubemap ) )
		MatLab.setDirty(%cubemap, "art/skies/materials.cs");
	else
		MatLab.setDirty(%cubemap);
}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::setCubemapNotDirty(%this)
{
	LabCubemapEditor.text= strreplace(LabCubemapEditor.text, "*", "");

	LabCubemapEditor.dirty = false;
	LabCubemapEditor-->saveCubemap.setActive(false);
	%cubemap = MatLab.currentCubemap;
	//MatLabCubemapEd_PerMan.removeDirty(%cubemap);
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::showDeleteCubemapDialog(%this)
{
	%idx = MatLabCubemapEditorList.getSelectedItem();
	%cubemap = MatLabCubemapEditorList.getItemText( %idx );
	%cubemap = %cubemap.getId();

	if ( %cubemap == -1 || !isObject(%cubemap) )
		return;

	if ( isObject( %cubemap ) )
	{
		LabMsgYesNoCancel("Delete Cubemap?",
		                  "Are you sure you want to delete<br><br>" @ %cubemap.getName() @ "<br><br> Cubemap deletion won't take affect until the engine is quit.",
		                  "MatLab.deleteCubemap( " @ %cubemap @ ", " @ %idx @ " );",
		                  "",
		                  "" );
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::deleteCubemap( %this, %cubemap, %idx )
{
	MatLabCubemapEditorList.deleteItem( %idx );
	UnlistedCubemaps.add( "unlistedCubemaps", %cubemap.getName() );

	if ( !MatLab.isMatEditorMaterial( %cubemap ) )
	{
		%this.PM.removeDirty( %cubemap );
		%this.PM.removeObjectFromFile( %cubemap );
	}

	if ( MatLabCubemapEditorList.getItemCount() > 0 )
	{
		MatLabCubemapEditorList.setSelected(0, true);
	}
	else
	{
		// if there are no cubemaps, then create one, select, and bail
		%cubemap = MatLab.createNewCubemap();
		MatLabCubemapEditorList.addItem( %cubemap.getName() );
		MatLabCubemapEditorList.setSelected(0, true);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLabCubemapEditorList::onSelect( %this, %id, %cubemap )
{
	%cubemap = %cubemap.getId();

	if ( MatLab.currentCubemap $= %cubemap )
		return;

	if ( LabCubemapEditor.dirty )
	{
		%savedCubemap = MatLab.currentCubemap;
		LabMsgYesNoCancel("Save Existing Cubemap?",
		                  "Do you want to save changes to <br><br>" @ %savedCubemap.getName(),
		                  "MatLab.saveCubemap(" @ true @ ");",
		                  "MatLab.saveCubemapDialogDontSave(" @ %cubemap @ ");",
		                  "MatLab.saveCubemapDialogCancel();" );
	}
	else
		MatLab.changeCubemap( %cubemap );
}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::getCubemapSaveFile( %this )
{
	%cubemap = MatLab.currentCubemap;

	if (!isObject(%cubemap))
		return;

	%fileDefault = %cubemap.getFileName();

	if (!isFile(%fileDefault))
		%fileDefault = "art/skies/materials.cs";

	%file = getSaveFile( "TorqueScript Files(*.cs)|*.cs|",%fileDefault);

	if (%file $= "")
	{
		warnLog("Invalid file selected:",%file);
		return;
	}

	//Just make sure it's a .cs file, if not add extension
	if (fileExt(%file) !$= ".cs")
		%file = %file@ ".cs";

	if (%cubemap.getFileName() $= %file)
	{
		warnLog("File is the same as current:",%file,"Current:",%cubemap.getFileName());
		return;
	}

	MatLab.editCubemapFile(%file);
}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::saveCubemap( %this, %cubemap )
{
	if (!isObject(%cubemap))
		%cubemap =  MatLab.currentCubemap;

	if (!isObject(%cubemap))
		return;

	MatLab_notDirtyCubemap.originalName = %cubemap.getName();
	MatLab.copyCubemaps( %cubemap, MatLab_notDirtyCubemap );
	MatLab.copyCubemaps( %cubemap, matEdCubeMapPreviewMat);
	%this.saveObj( %cubemap);
	//MatLabCubemapEd_PerMan.saveDirty();
	MatLab.setCubemapNotDirty();
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::saveCubemapDialogDontSave( %this, %newCubemap)
{
	//deal with old cubemap first
	%oldCubemap = MatLab.currentCubemap;
	%idx = MatLabCubemapEditorList.findItemText( %oldCubemap.getName() );
	MatLabCubemapEditorList.setItemText( %idx, MatLab_notDirtyCubemap.originalName );
	%oldCubemap.setName( MatLab_notDirtyCubemap.originalName );
	MatLab.copyCubemaps( MatLab_notDirtyCubemap, %oldCubemap);
	MatLab.copyCubemaps( MatLab_notDirtyCubemap, matEdCubeMapPreviewMat);
	MatLab.syncCubemap( %oldCubemap );
	MatLab.changeCubemap( %newCubemap );
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::saveCubemapDialogCancel( %this )
{
	%cubemap = MatLab.currentCubemap;
	%idx = MatLabCubemapEditorList.findItemText( %cubemap.getName() );
	MatLabCubemapEditorList.clearSelection();
	MatLabCubemapEditorList.setSelected( %idx, true );
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::changeCubemap( %this, %cubemap )
{
	MatLab.setCubemapNotDirty();
	MatLab.currentCubemap = %cubemap;
	MatLab_notDirtyCubemap.originalName = %cubemap.getName();
	MatLab.copyCubemaps( %cubemap, MatLab_notDirtyCubemap);
	MatLab.copyCubemaps( %cubemap, matEdCubeMapPreviewMat);
	MatLab.syncCubemap( %cubemap );
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::editCubemapImage( %this, %face )
{
	MatLab.setCubemapDirty();
	%cubemap = MatLab.currentCubemap;
	%bitmap = getTextureFile(%this.lastCubemapFile);

	if ( %bitmap !$= "" && %bitmap !$= "tlab/plugins/materialEditor/assets/cubemapBtnBorder" )
	{
		%this.lastCubemapFile = %bitmap;
		%cubemap.cubeFace[%face] = %bitmap;
		MatLab.copyCubemaps( %cubemap, matEdCubeMapPreviewMat);
		MatLab.syncCubemap( %cubemap );
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::editCubemapName( %this, %newName )
{
	MatLab.setCubemapDirty();
	%cubemap = MatLab.currentCubemap;
	%idx = MatLabCubemapEditorList.findItemText( %cubemap.getName() );
	MatLabCubemapEditorList.setItemText( %idx, %newName );
	%cubemap.setName(%newName);
	MatLab.syncCubemap( %cubemap );
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::editCubemapFile( %this, %file )
{
	%cubemap = MatLab.currentCubemap;

	if (!isObject(%cubemap))
		return;

	MatLab.setFileName(%file);
	MatLab.setDirty(%cubemap,%file);
	MatLab.setCubemapDirty(true);

	MatLab.syncCubemap( %cubemap );
}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::syncCubemap( %this, %cubemap )
{
	%xpos = searchForTexture(%cubemap.getName(), %cubemap.cubeFace[0]);

	if ( %xpos !$= "" )
		MatLabCubemapEd_XPos.setBitmap( %xpos );

	%xneg = searchForTexture(%cubemap.getName(), %cubemap.cubeFace[1]);

	if ( %xneg !$= "" )
		MatLabCubemapEd_XNeg.setBitmap( %xneg );

	%yneg = searchForTexture(%cubemap.getName(), %cubemap.cubeFace[2]);

	if ( %yneg !$= "" )
		MatLabCubemapEd_YNeG.setBitmap( %yneg );

	%ypos = searchForTexture(%cubemap.getName(), %cubemap.cubeFace[3]);

	if ( %ypos !$= "" )
		MatLabCubemapEd_YPos.setBitmap( %ypos );

	%zpos = searchForTexture(%cubemap.getName(), %cubemap.cubeFace[4]);

	if ( %zpos !$= "" )
		MatLabCubemapEd_ZPos.setBitmap( %zpos );

	%zneg = searchForTexture(%cubemap.getName(), %cubemap.cubeFace[5]);

	if ( %zneg !$= "" )
		MatLabCubemapEd_ZNeg.setBitmap( %zneg );

	MatLabCubemapNameEdit.setText(%cubemap.getName());
	MatLabCubemapFileEdit.setText(%cubemap.getFileName());
	%cubemap.updateFaces();
	matEdCubeMapPreviewMat.updateFaces();
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::copyCubemaps( %this, %copyFrom, %copyTo)
{
	%copyTo.cubeFace[0] = %copyFrom.cubeFace[0];
	%copyTo.cubeFace[1] = %copyFrom.cubeFace[1];
	%copyTo.cubeFace[2] = %copyFrom.cubeFace[2];
	%copyTo.cubeFace[3] = %copyFrom.cubeFace[3];
	%copyTo.cubeFace[4] = %copyFrom.cubeFace[4];
	%copyTo.cubeFace[5] = %copyFrom.cubeFace[5];
}
//------------------------------------------------------------------------------

