//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function repage()
{
	MatLab.buildPages();
}
function mlSort(%color)
{
	MatLab.sortBy("Name");
}

function colBG(%color)
{
	MatLab.setColorBG(%color);
}

function doPrev()
{
	MatLab.previewMaterial();
}
//==============================================================================
function buildMat(%clearAll)
{
	if (%clearAll)
		MatLab.clearAll();

	// %proTitle = "Building Initial Material Preview Listing...";
	// LabProgessBox.start(%proTitle,"Can take some time depending of amount of materials","Initializing");
	if (!$MatLabProcessNoOverlay)
		show(MatLabProcessingOverlay);

	MatLab.schedule(1000,buildPreviews);
}
//------------------------------------------------------------------------------
function MatLab::clearAll(%this)
{
	MatLabFiltersDynamicArray.clear();
	MatLabMaterialCategories.clear();
	MatLabPreviewSet.deleteAllObjects();
	MatLab_MaterialsPages.deleteAllObjects();
	MatLab_PageButtonsStack.clear();
	MatLab.matSetBuilt = 0;
}

//------------------------------------------------------------------------------
function materialSet::onObjectAdded(%this, %material)
{
	if ($MatLab_AutoAddDisabled)
	{
		info(MatLab.getMatName(%material),"Added to MaterialSet but AutoAdd is disabled");
		return;
	}

	if (!isObject(MatLabPreviewSet))
		return;

	MatLab.processMaterialData(%material,true);

}

function MatLab::buildMat(%this, %material )
{
	MatLab.processMaterialData(%material,true);
}

