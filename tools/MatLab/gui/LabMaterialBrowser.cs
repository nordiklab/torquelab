//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function LabMaterialBrowser::onWake(%this)
{
	//LabMaterialBrowser.add(MaterialSelector);
	MaterialSelector.setVisible(1);
	//Moved to another function to work with integrated MaterialSelector dialog
	MatLab.activate();
}
//------------------------------------------------------------------------------
//==============================================================================
function LabMaterialBrowser::onSleep(%this)
{
	//Moved to another function to work with integrated MaterialSelector dialog
	MatLab.deactivate();
}
//------------------------------------------------------------------------------
