//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function MaterialSelector::onAdd(%this)
{
	MaterialSelector.visible = 0;
}
//------------------------------------------------------------------------------

//==============================================================================
function MaterialSelector::onWake(%this)
{
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialSelector::onSleep(%this)
{
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialSelector::onVisible(%this,%state)
{
	if (%state)
		MatLab.activate();
	else
		MatLab.deactivate();

}
//------------------------------------------------------------------------------

function MaterialSelector::onPreEditorSave(%this)
{
	%origin = MaterialSelector.parentGroup;
	MaterialSelector.canSave = 1;

	LabMaterialBrowser.add(MaterialSelector);
	Lab.saveGuiToFile(LabMaterialBrowser,"tlab/tools/MatLab/gui/LabMaterialBrowser.gui");
	%origin.schedule(1000,add,MaterialSelector);
	return "ABORT";

}
//------------------------------------------------------------------------------

//==============================================================================
function MaterialSelector::customGuiSave(%this)
{
	%origin = MaterialSelector.parentGroup;
	MaterialSelector.canSave = 1;
	LabMaterialBrowser.add(MaterialSelector);
	Lab.saveGuiToFile(LabMaterialBrowser,"tlab/tools/MatLab/gui/LabMaterialBrowser.gui");
	%origin.schedule(1000,add,MaterialSelector);
}
//------------------------------------------------------------------------------

//==============================================================================
function MaterialSelector::onAttached(%this)
{
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialSelector::onDetached(%this)
{
}
//------------------------------------------------------------------------------

