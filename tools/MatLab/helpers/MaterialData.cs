//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function MatLab::getMatPreview(%this, %material)
{
	if (%material.isMemberOfClass("TerrainMaterial"))
	{
		%matName = %material.getInternalName();

		if (%material.diffuseMap $= "")
			%previewImage = %material.detailMap;
		else
			%previewImage = %material.diffuseMap;
	}
	else
	{
		%matName = %material.name;

		if (%material.diffuseMap[0] !$= "")
			%previewImage = %material.diffuseMap[0];
		else if (%material.toneMap[0] !$= "")
			%previewImage = %material.toneMap[0];
		else if (%material.cubemap.cubeFace[0] !$= "")
			%previewImage = %material.cubemap.cubeFace[0];

		if ($MatLab_AdvancedImageSearch && %previewImage !$= "")
		{
			//%previewImage = searchForTexture( %material,  %previewImage );
			// were going to use a couple of string commands in order to properly
			// find out what the img src path is
			// **NEW** this needs to be updated with the above, but has some timing issues
			%materialDiffuse =  %previewImage;
			%materialPath = %material.getFilename();

			if (strchr(%materialDiffuse, "/") $= "")
			{
				%k = 0;

				while (strpos(%materialPath, "/", %k) != -1)
				{
					%foo = strpos(%materialPath, "/", %k);
					%k = %foo + 1;
				}

				%foobar = getSubStr(%materialPath, %k, 99);
				%previewImage =  strreplace(%materialPath, %foobar, %previewImage);
			}
			else
				%previewImage =  strreplace(%materialPath, %materialPath, %previewImage);
		}
	}

	if (%previewImage $= "" || !isImageFile(%previewImage))
		%previewImage = "tlab/plugins/materialEditor/assets/warnMat";

	if (!isImageFile(%previewImage))
		return "";

	MatLab.matPreview[%material.getId()] = %previewImage;
	return %previewImage;
}
//------------------------------------------------------------------------------
//==============================================================================
function MatLab::getMatName(%this, %material)
{
	if (%material.isMemberOfClass("TerrainMaterial"))
		return %material.getInternalName();

	return %material.name;

}
//------------------------------------------------------------------------------
//==============================================================================
// Material Tagging System
//==============================================================================

//==============================================================================
function Material::isTag(%this, %tag)
{
	if (getCommaWordIndex(%this.tags,%tag) $= "-1")
		return false;

	return true;
}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::convertStockTags(%this, %material,%saveChanges)
{
	for (%j = 0; %material.getFieldValue("materialTag" @ %j) !$= ""; %j++)
	{
		%isDirty = true;
		%dirtyList = strAddWord(%dirtyList,"materialTag" @ %j);
		%tag = %material.getFieldValue("materialTag" @ %j);
		%material.tags = strAddCommaWord(%material.tags,%tag,1);
		%this.clearField(%material,"materialTag" @ %j,0);
	}

	if (!%isDirty)
		return;

	info(%this.getMatName(%material),"Material converted to new tagging","Removed fields",%dirtyList);
	if (%saveChanges)
	   %this.saveObj(%material,1);
}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::fixMatTags(%this, %material)
{
	if (!isObject(%material))
		return;

	%this.convertStockTags(%material);
	%tags = %material.tags;

	for (%i =0; %i < getCommaWordCount(%tags); %i++)
	{
		%tag = trim(getCommaWord(%tags,%i));

		if (%isTag[%tag])
			continue;

		%isTag[%tag] = true;
		%fixTags = strAddCommaWord(%fixTags,%tag,1);
	}

	%material.tags = %fixTags;
}
//------------------------------------------------------------------------------
