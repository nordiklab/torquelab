//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function MatLab::copyMaterials( %this, %copyFrom, %copyTo)
{
	// Make sure we copy and restore the map to.
	if (!isObject(%copyFrom) || !isObject(%copyTo))
		return;

	%mapTo = %copyTo.mapTo;
	%copyTo.assignFieldsFrom( %copyFrom );
	%copyTo.mapTo = %mapTo;
}
//------------------------------------------------------------------------------

//==============================================================================
function MaterialEditorTools::copyMaterials( %this, %copyFrom, %copyTo)
{
	MatLab.copyMaterials(%copyFrom, %copyTo);
}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::isMatEditorMaterial(%this, %material)
{
	return ( %material.getFilename() $= "" || startsWith(%material.getFilename(),"tlab/") );
}
//------------------------------------------------------------------------------
//==============================================================================
function MaterialEditorTools::isMatEditorMaterial(%this, %material)
{
	return MatLab.isMatEditorMaterial(%material);
}
//------------------------------------------------------------------------------

//==============================================================================
function MatLab::convertTextureFields(%this,%material)
{
	//Not sure what it does yet
	// Find the absolute paths for the texture filenames so that
	// we can properly wire up the preview materials and controls.
	for(%i = 0; %i < 4; %i++)
	{
		foreach$(%map in %maps)
		{
			%mapFile = %material.getFieldValue(%map@"Map",%i);
			%mapFile = %this.searchForTexture(%material, %mapFile);
			%material.setFieldValue(%map@"Map",%mapFile,%i);

		}
	}
}

