//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function MatLab::getDlgTextureMapPath( %this, %material,%baseMap,%layer)
{
	if (%layer $= "")
		%layer = 0;

	%maps = $MatLab_MaterialMaps;

	if (%baseMap !$= "")
	{
		%maps = strRemoveWord(%maps,%baseMap);
		%maps = %baseMap SPC %maps;
	}

	//Find the first valid texture file using the maps list
	foreach$(%map in %maps)
	{
		%mapFile = %material.getFieldValue(%map@"Map",%layer);
		%path = filePath(%mapFile);

		if (isDirectory(%path) && %path !$= "")
			return %path;
	}

	if (!isDirectory(MatLab.lastTexturePath))
		MatLab.lastTexturePath = "art/";

	return MatLab.lastTexturePath;
}
//------------------------------------------------------------------------------

//==============================================================================
//Select a texture for a material map type
function MatLab::selectMapFile( %this, %material,%type,%layer )
{
	//Get the best path to open the file dialog at
	%defaultPath = MatLab.getDlgTextureMapPath(%material,%type,%layer);

	%dlg = new OpenFileDialog()
	{
		Filters        = $MatLab_TextureFormats;
		DefaultPath    = %defaultPath;
		ChangePath     = false;
		MustExist      = true;
	};
	%ret = %dlg.Execute();

	if (%ret)
	{
		%filename = makeRelativePath( %dlg.FileName, getMainDotCsDir() );
		MatLab.lastTextureFile = %filename;
		MatLab.lastTexturePath = filePath(MatLab.lastTextureFile);
	}

	%dlg.delete();
	return %filename;
}
//------------------------------------------------------------------------------

//==============================================================================
// Try to find a matching texture for a new material added from model
function MatLab::searchForTexture(%this,%material, %texture)
{
	if (%texture !$= "")
	{
		// set the find signal as false to start out with
		%isFile= false;
		// sete the formats we're going to be looping through if need be
		%formats = ".png .jpg .dds .bmp .gif .jng .tga";

		// if the texture contains the correct filepath and name right off the bat, lets use it
		if (isFile(%texture))
			%isFile = true;
		else
		{
			for(%i = 0; %i < getWordCount(%formats); %i++)
			{
				%testFileName = %texture @ getWord(%formats, %i);

				if (isFile(%testFileName))
				{
					%isFile = true;
					break;
				}
			}
		}

		// if we didn't grab a proper name, lets use a string logarithm
		if (!%isFile)
		{
			%materialDiffuse = %texture;
			%materialDiffuse2 = %texture;
			%materialPath = %material.getFilename();

			if (strchr(%materialDiffuse, "/") $= "")
			{
				%k = 0;

				while(strpos(%materialPath, "/", %k) != -1)
				{
					%count = strpos(%materialPath, "/", %k);
					%k = %count + 1;
				}

				%materialsCs = getSubStr(%materialPath, %k, 99);
				%texture =  strreplace(%materialPath, %materialsCs, %texture);
			}
			else
				%texture =  strreplace(%materialPath, %materialPath, %texture);

			// lets test the pathing we came up with
			if (isFile(%texture))
				%isFile = true;
			else
			{
				for(%i = 0; %i < getWordCount(%formats); %i++)
				{
					%testFileName = %texture @ getWord(%formats, %i);

					if (isFile(%testFileName))
					{
						%isFile = true;
						break;
					}
				}
			}

			// as a last resort to find the proper name
			// we have to resolve using find first file functions very very slow
			if (!%isFile)
			{
				%k = 0;

				while(strpos(%materialDiffuse2, "/", %k) != -1)
				{
					%count = strpos(%materialDiffuse2, "/", %k);
					%k = %count + 1;
				}

				%texture =  getSubStr(%materialDiffuse2, %k, 99);

				for(%i = 0; %i < getWordCount(%formats); %i++)
				{
					%searchString = "*" @ %texture @ getWord(%formats, %i);
					%testFileName = findFirstFile(%searchString);

					if (isFile(%testFileName))
					{
						%texture = %testFileName;
						%isFile = true;
						break;
					}
				}
			}

			return %texture;
		}
		else
			return %texture; //Texture exists and can be found - just return the input argument.
	}

	return ""; //No texture associated with this property.
}
//------------------------------------------------------------------------------
