//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Centralized function to set a module field, it will be marked as dirty and saved
function ModuleDefinition::set(%this,%field,%value)
{  
	if (!isObject(%this))
		return;

	%currentValue = %this.getFieldValue(%field);
	if (%currentValue $= %value)
		return;
  
	%this.setFieldValue(%field,%value);

	TLModules.isDirty[%this.moduleID] = true;
	Lab.postEvent("ModuleChanged",%this);
}
//------------------------------------------------------------------------------

//==============================================================================
//Remove the object ID from the linkName object list
//------------------------------------------------------------------------------

//==============================================================================
// All/Loaded Module listing - List ModuleIds if %listModuleIds = true
function TLModules::isModule(%this,%moduleID)
{
	
   %obj = TLModules.getModuleObj(%moduleID);
   if (isObject(%obj))
      return true;
	return false;

}
//------------------------------------------------------------------------------

//==============================================================================
// All/Loaded Module listing - List ModuleIds if %listModuleIds = true
function TLModules::list(%this,%loadedOnly,%listModuleIds)
{
	%modIds = TLModules.findModules(%loadedOnly);

	foreach$(%in in %modIds)
	{
		%id = (%listModuleIds) ? %in.ModuleId : %in;
		%list = strAddWord(%list,%id);
	}

	return %list;

}
//------------------------------------------------------------------------------

//==============================================================================
// Obtain the moduleID or object regardless of type submitted
function TLModules::getModuleId(%this,%module)
{
	if (%module.moduleId !$= "")
		return  %module.moduleId;

	return %module;

}

//------------------------------------------------------------------------------

function TLModules::getModuleObj(%this,%module)
{
	if (%module.moduleId !$= "")
		return %module;

	%modIds = TLModules.findModules(0);

	foreach$(%mod in %modIds)
	{
		if (%mod.moduleId $= %module)
			return %mod;

	}

	return -1;

}
//==============================================================================
// Obtain the moduleID or object regardless of type submitted
function TLModules::getDependenciesFields(%this,%module)
{
   %modObj = %this.getModuleObj(%module);

	%dependencies = %modObj.Dependencies;
	if (%dependencies $= "")
		return "";

	%fieldDepend = strreplace(%dependencies,",","\t");
	%fieldDepend= strreplace(%fieldDepend,"="," ");
	return %fieldDepend;

}

//==============================================================================
//Remove the object ID from the linkName object list
function TLModules::reloadModules(%this,%root)
{
	%modIds = %this.findModules();

	foreach$(%in in %modIds)
	{
		//TLModules.unloadExplicit(%in.ModuleId);
		%this.loadExplicit(%in.ModuleId);
	}

	%this.scanModules(%root);
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
//function TLModules::findModuleTypes(%type,%loadedOnly)

//------------------------------------------------------------------------------
//==============================================================================
// Functions handling the loading and unloading of modules
//==============================================================================

//==============================================================================
//Remove the object ID from the linkName object list
function TLModules::loadModule(%this,%module,%versionId)
{
	%modID = %this.getModuleId(%module);

	//Skip is module is already loaded
	if (%this.isLoaded(%modID))
		return;

	if (%versionId $= "")
		%this.loadExplicit(%modID);
	else
		%this.loadExplicit(%modID,%versionId);

}
//==============================================================================
//Remove the object ID from the linkName object list
function TLModules::unloadModule(%this,%module,%unloadDependOn)
{
	%modID = %this.getModuleId(%module);

	//Skip is module is already unloaded
	if (!%this.isLoaded(%modID))
		return;

	%dependOnMe = TLModules.dependOn[%modID];

	if (%dependOnMe !$= "")
	{
		foreach$(%dep in  %dependOnMe)
		{
			info(%modID,"have module depending on it",%dep,"Is loaded",%this.isLoaded(%dep));

			if (%unloadDependOn)
			{
				%this.unloadModule(%dep);
				info(%dep,"unloaded");
			}
		}
	}

	%this.unloadExplicit(%modID);
}
//------------------------------------------------------------------------------
function TLModules::isLoaded(%this,%module)
{
	//If ModuleId sent, convert to module object
	%modObj = %this.getModuleObj(%module);
	return isObject(%modObj.scopeSet);

}

//------------------------------------------------------------------------------
// Load/Unload modules of a type
function TLModules::loadModuleType( %this,%type,%force )
{
	%mods = %this.findModuleTypes(%type);

	foreach$(%mod in %mods)
	{
		//Make sure the module is enabled before loading it
		if ($Cfg_Modules_[%mod.moduleId,"Enable"])
		{
			info("Set",%mods,"Load Module:",%mod.moduleId,"ID",%mod);
			%mod.activated = true;
			%this.loadModule(%mod);
		}
		else
		{
			info("Set",%mods,"---","UNLoad Module:",%mod.moduleId,"ID",%mod);
			%mod.activated = false;
			%this.unloadModule(%mod);
		}
	}

}
function TLModules::unloadModuleType( %this,%type,%force )
{
	%mods = %this.findModuleTypes(%type);

	foreach$(%mod in %mods)
	{
		info("Set",%mods,"---","UNLoad Module:",%mod.moduleId,"ID",%mod);
		%this.unloadModule(%mod);
	}

}
function TLModules::isLoaded(%this,%module)
{
	foreach$(%in in TLModules.findModules())	
	   %in.activated = 1;
}

//------------------------------------------------------------------------------

//==============================================================================
// Overiding of load/unload group for Groups set to use custom function
// If no ModuleSet stored for the group, default methods will be called
//==============================================================================
function TLModules::loadGroup( %this,%group)
{
	%modSet = TLModules.moduleSet[%group];

	if (!isObject(%modSet))
	{
		//If not set exist containing that group, use built-in loadGroup
		info("TLModules::loadGroup","Before code call",%group);
		Parent::loadGroup(%this,%group);
		return;
	}

	foreach(%mod in %modSet)
	{
		if (!%mod.activated && %mod.activated !$= "")
		{
			info(%mod.moduleID,"Load skipped as it's marked as not activated","Need to unload?",%this.isLoaded(%mod));
			%this.unloadModule(%mod.moduleId); //Will only unload if loaded

			continue;
		}

		%this.loadModule(%mod.moduleID);
	}

}
function TLModules::unloadGroup( %this,%group )
{
	%modSet = TLModules.moduleSet[%group];

	if (!isObject(%modSet))
	{
		//If not set exist containing that group, use built-in loadGroup
		info("TLModules::unloadGroup","Before code call",%group);
		Parent::unloadGroup(%this,%group);
		return;
	}

	foreach(%mod in %modSet)
	{
		%this.unloadModule(%mod.moduleID);
	}
}

//==============================================================================
// Enable/Disable TorqueLab Modules - Not related to locked Module Enabled parameter
//==============================================================================
function TLModules::setEnabled(%this,%module,%enabled)
{
	%module = %this.getModuleObj(%module);

	if (!isObject(%module))
		return;

	if (%enabled $= "")
		%enabled = 1;

   //Enabled is reserved field for modules
	$Cfg_Modules_[%module.moduleId,"Activated"] = %enabled;
	%module.set("Activated",%enabled);

	if (!%this.isLoaded(%module) && %enabled)
		%this.loadExplicit(%module.moduleId);

	if (%this.isLoaded(%module) && !%enabled)
		%this.unloadExplicit(%module.moduleId);

}
function TLModules::enableModule(%this,%module,%autoload)
{
	%this.setEnabled(%module,1);
}

function TLModules::disableModule(%this,%module)
{
	%this.setEnabled(%module,0);
}
//------------------------------------------------------------------------------

//==============================================================================
// Modules data dumping for debugging and development purpose
//==============================================================================
//------------------------------------------------------------------------------
function TLModules::dumpSets( %this )
{
	foreach$(%group in "plugins tools themes")
	{
		%set =   %this.moduleSet[%group];

		if (!isObject(%set))
			continue;
      info("========================================");
      info(%group, "Modules list");
      info("----------------------------------------");
		foreach(%module in %set)
		{
			info("Set",%set.getName(),"Module:",%module.moduleId,"ID",%module,"Type",%module.type,"Activated",%module.activated);
		}
	}

}

//==============================================================================
//Remove the object ID from the linkName object list
function TLModules::dumpStatus(%this)
{
	foreach$(%in in TLModules.findModules(0))			
      info(%in,%in.ModuleId,"Group->",%in.group,"Type->",%in.type,"Loaded->",isObject(%in.scopeSet),"Enabled",%in.enabled,"Activated",%in.activated);	

	return;
}
//------------------------------------------------------------------------------
//==============================================================================
// ModuleManager Docs
//==============================================================================
/*

Public Member Functions

virtual void 	addListener ((string listenerObject=""))
virtual bool 	canMergeModules ((string mergeSourcePath=""))
virtual string 	copyModule ((string sourceModuleDefinition="", string pTargetModuleId="", string pTargetPath="", bool useVersionPathing=false))
virtual string 	findModule ((string pModuleId="", int pVersionId=0))
virtual string 	findModules ((bool loadedOnly=false))
virtual string 	findModuleTypes ((string pModuleType="", bool loadedOnly=false))
virtual bool 	isModuleMergeAvailable (())
virtual bool 	loadExplicit ((string pModuleId="", int pVersionId=-1))
virtual bool 	loadGroup ((string pModuleGroup=""))
virtual bool 	mergeModules ((string pMergeTargetPath="", bool removeMergeDefinition=false, bool registerNewModules=false))
virtual void 	removeListener ((string listenerObject=""))
virtual bool 	scanModules ((string pRootPath="", bool rootOnly=false))
virtual bool 	setModuleExtension ((string moduleExtension=""))
virtual bool 	synchronizeDependencies ((string rootModuleDefinition="", string pTargetDependencyFolder=""))
virtual bool 	unloadExplicit ((string pModuleId=""))
virtual bool 	unloadGroup ((string pModuleGroup=""))
virtual bool 	unregisterModule ((string pModuleId="", bool versionId=false))
Public Attributes

bool 	EchoInfo
bool 	EnforceDependencies
Member Function Documentation

virtual void ModuleManager::addListener	(	(string listenerObject="") 		 ) 	[virtual]
Registers the specified object as a listener for module notifications.

Parameters:
listenerObject 	The object to start receiving module notifications.
Returns:
No return value.
virtual bool ModuleManager::canMergeModules	(	(string mergeSourcePath="") 		 ) 	[virtual]
Checks whether a module merge using the modules in the source path can current happen or not.

Parameters:
mergeSourcePath 	The path where modules to be merged are located.
Returns:
Whether a module merge using the modules in the source path can current happen or not.
virtual string ModuleManager::copyModule	(	(string sourceModuleDefinition="", string pTargetModuleId="", string pTargetPath="", bool useVersionPathing=false) 		 ) 	[virtual]
Copy the module to a new location with a new module Id.

Parameters:
sourceModuleDefinition 	The module definition to copy.
targetModuleId 	The module Id to rename the copied module to including all references to the source module Id.It is valid to specifiy the source module Id to produce an identical copy.
targetPath 	The target path to copy the module to.Addition folders will be created depending on whether 'useVersionPathing' is used or not.
useVersionPathing 	Whether to add a '/targetModuleId/versionId' folder to the target path or not.This allows copying multiple versions of the same module Id.
Returns:
The new module definition file if copy was successful or NULL if not.

virtual string ModuleManager::findModule	(	(string pModuleId="", int pVersionId=0) 		 ) 	[virtual]
Find the specific module Id optionally at the specified version Id.

Parameters:
moduleId 	The module Id to find.
versionId 	The version Id to find.
Returns:
The module definition object or NULL if not found.

virtual string ModuleManager::findModules	(	(bool loadedOnly=false) 		 ) 	[virtual]
Find all the modules registered with the specified loaded state.

Parameters:
loadedOnly 	Whether to return only modules that are loaded or not.
Returns:
A list of space - separated module definition object Ids.
virtual string ModuleManager::findModuleTypes	(	(string pModuleType="", bool loadedOnly=false) 		 ) 	[virtual]
Find the modules registered with the specified module type.

Parameters:
moduleType 	The module type to search for.
loadedOnly 	Whether to return only modules that are loaded or not.
Returns:
A list of space - separated module definition object Ids.
virtual bool ModuleManager::isModuleMergeAvailable	(	() 		 ) 	[virtual]
Checks whether a module merge definition file is available or not.

Returns:
Whether a module merge definition file is available or not.
virtual bool ModuleManager::loadExplicit	(	(string pModuleId="", int pVersionId=-1) 		 ) 	[virtual]
Load the specified module explicitly.

Parameters:
moduleId 	The module Id to load.
versionId 	The version Id to load.Optional: Will load the latest version.
Returns:
Whether the module Id was loaded or not.

virtual bool ModuleManager::loadGroup	(	(string pModuleGroup="") 		 ) 	[virtual]
Load the specified module group.

Parameters:
moduleGroup 	The module group to load.
Returns:
Whether the module group was loaded or not.

virtual bool ModuleManager::mergeModules	(	(string pMergeTargetPath="", bool removeMergeDefinition=false, bool registerNewModules=false) 		 ) 	[virtual]
Performs a module merge into the selected target path.

Parameters:
mergeTargetPath 	The path where modules will be merged into.
removeMergeDefinition 	Whether to remove any merge definition found or not if merge is successful.
registerNewModules 	Whether new (not replaced or updated) modules should be registered or not.
Returns:
Whether the module merge was successful or not.Failure here could result in a corrupt module state.Reinstall is recommended or at least advised to the user is recommended.

virtual void ModuleManager::removeListener	(	(string listenerObject="") 		 ) 	[virtual]
Unregisters the specified object as a listener for module notifications.

Parameters:
listenerObject 	The object to stop receiving module notifications.
Returns:
No return value.

virtual bool ModuleManager::scanModules	(	(string pRootPath="", bool rootOnly=false) 		 ) 	[virtual]
Scans for modules which are sub-directories of the specified path.

Parameters:
moduleRootPath 	The root directory to scan for sub - directories containing modules.
rootOnly[Optional] 	- Specifies whether to only scan the root path or not when searching for modules.
Returns:
Whether the scan was successful or not.A successful scan can still find zero modules.

virtual bool ModuleManager::setModuleExtension	(	(string moduleExtension="") 		 ) 	[virtual]
Set the module extension used to scan for modules. The default is 'module'.

Parameters:
moduleExtension 	The module extension used to scan for modules.Do not use a period character.
Returns:
Whether setting the module extension was successful or not.
virtual bool ModuleManager::synchronizeDependencies	(	(string rootModuleDefinition="", string pTargetDependencyFolder="") 		 ) 	[virtual]
Synchronize the module dependencies of a module definition to a target dependency folder.

Parameters:
rootModuleDefinition 	The module definition used to determine dependencies.
targetDependencyPath 	The target dependency folder to copy dependencies to.
Returns:
Whether the module dependencies were synchronized correctly or not.

virtual bool ModuleManager::unloadExplicit	(	(string pModuleId="") 		 ) 	[virtual]
Unload the specified module explicitly.

Parameters:
moduleId 	The module Id to unload.
Returns:
Whether the module Id was unloaded or not.

virtual bool ModuleManager::unloadGroup	(	(string pModuleGroup="") 		 ) 	[virtual]
Unload the specified module group.

Parameters:
moduleGroup 	The module group to unload.
Returns:
Whether the module group was unloaded or not.

virtual bool ModuleManager::unregisterModule	(	(string pModuleId="", bool versionId=false) 		 ) 	[virtual]
Unregister the specified module.

Parameters:
moduleId 	The module Id to unregister.
versionId 	The version Id to unregister.
Returns:
Whether the module was unregister or not.
Member Data Documentation

bool ModuleManager::EchoInfo
Whether the module manager echos extra information to the console or not.

bool ModuleManager::EnforceDependencies
Whether the module manager enforces any dependencies on module definitions it discovers or not.

*/
