//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
//Remove the object ID from the linkName object list
function getAssetList(%ignoreInternal,%ignorePrivate)
{
    %assetQuery = new AssetQuery();
      %numAssetsFound = AssetDatabase.findAllAssets(%assetQuery,%ignoreInternal,%ignorePrivate);
      
      for( %i=0; %i < %numAssetsFound; %i++)
      {
          %assetId = %assetQuery.getAsset(%i);
          %list = strAddWord(%list,%assetId);
      }
   return %list;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function getAssetInCategory(%assetCategory,%assetQueryAsSource)
{
    %assetQuery = new AssetQuery();
      %numAssetsFound = AssetDatabase.findAssetCategory(%assetQuery,%assetCategory,%assetQueryAsSource);
      
      for( %i=0; %i < %numAssetsFound; %i++)
      {
          %assetId = %assetQuery.getAsset(%i);
          %list = strAddWord(%list,%assetId);
      }
   return %list;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function findAssetInternal(%assetInternal,%assetQueryAsSource)
{
    %assetQuery = new AssetQuery();
      %numAssetsFound = AssetDatabase.findAssetInternal(%assetQuery,%assetInternal,%assetQueryAsSource);
      
      for( %i=0; %i < %numAssetsFound; %i++)
      {
          %assetId = %assetQuery.getAsset(%i);
          %list = strAddWord(%list,%assetId);
      }
   return %list;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function isAssetLoaded(%assetId)
{
    %loaded = AssetDatabase.isAssetLoaded(%assetId);
   return %loaded;
}
//------------------------------------------------------------------------------

//==============================================================================
//Remove the object ID from the linkName object list
function isReferencedAsset(%assetId)
{
    %loaded = AssetDatabase.isReferencedAsset(%assetId);
   return %loaded;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function getAssetName(%assetId)
{
    %name = AssetDatabase.getAssetName(%assetId);
   return %name;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function findAssetsDependsOn(%assetId)
{   
    %assetQuery = new AssetQuery();
   %numAssetsFound = AssetDatabase.findAssetDependsOn(%assetQuery,%assetId);
      
      for( %i=0; %i < %numAssetsFound; %i++)
      {
          %assetId = %assetQuery.getAsset(%i);
          %list = strAddWord(%list,%assetId);
      }
   return %list;
   return %name;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function findAssetIsDependedOn(%assetId)
{   
    %assetQuery = new AssetQuery();
   %numAssetsFound = AssetDatabase.findAssetIsDependedOn(%assetQuery,%assetId);
      
      for( %i=0; %i < %numAssetsFound; %i++)
      {
          %assetId = %assetQuery.getAsset(%i);
          %list = strAddWord(%list,%assetId);
      }
   return %list;
   return %name;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function getAssetObj(%assetId)
{
    %asset = AssetDatabase.acquireAsset(%assetId);  
   return %asset;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function getAssetModule(%assetId)
{
    %module = AssetDatabase.getAssetModule(%assetId);  
   return %module;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function getAssetCategory(%assetId,%emptyValue,%tryTypeIfEmpty)
{
    %category = AssetDatabase.getAssetCategory(%assetId);  
    if (%tryTypeIfEmpty && %category $= "")
      %category = getAssetType(%assetId); 
   if (%category $= "")
      %category = %emptyValue;
      
   return %category;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function getAssetType(%assetId)
{
    %type = AssetDatabase.getAssetType(%assetId);  
   return %type;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function getAssetPath(%assetId)
{
    %type = AssetDatabase.getAssetPath(%assetId);  
   return %type;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function getAssetTags(%assetId)
{
    %type = AssetDatabase.getAssetPath(%assetId);  
   return %type;
}
//------------------------------------------------------------------------------

//==============================================================================
// Global Helpers
//==============================================================================
//==============================================================================
//Remove the object ID from the linkName object list
function purgeAssets()
{
    AssetDatabase.purgeAssets(); 
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function refreshAllAssets(%includeUnloaded)
{
   AssetDatabase.refreshAllAssets(%includeUnloaded);  
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function purgeAssets()
{
   AssetDatabase.purgeAssets();
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function saveAssetTags()
{
    %saved = AssetDatabase.saveAssetTags();  
   return %saved;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function restoreAssetTags()
{
    %restored = AssetDatabase.restoreAssetTags();  
   return %restored;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function getAssetCount()
{
    %count = AssetDatabase.getDeclaredAssetCount();  
   return %count;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function getExternalAssetCount()
{
    %count = AssetDatabase.getLoadedExternalAssetCount();  
   return %count;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function getInternalAssetCount()
{
    %count = AssetDatabase.getLoadedInternalAssetCount();  
   return %count;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function getReferencedAssetCount()
{
    %count = AssetDatabase.getReferencedAssetCount();  
   return %count;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function getMaxExternalAssetCount()
{
    %count = AssetDatabase.getMaxLoadedExternalAssetCount();  
   return %count;
}
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function getMaxInternalAssetCount()
{
    %count = AssetDatabase.getMaxLoadedInternalAssetCount();  
   return %count;
}
//------------------------------------------------------------------------------

//==============================================================================
//Remove the object ID from the linkName object list
function dumpAssets()
{
    %count = AssetDatabase.dumpDeclaredAssets();  
   return %count;
}
//------------------------------------------------------------------------------

/*
Member Function Documentation

virtual string AssetManager::acquireAsset	(	(string assetId="", bool asPrivate=false) 		 ) 	[virtual]
Acquire the specified asset Id. You must release the asset once you're finish with it using 'releaseAsset'.

Parameters:
assetId 	The selected asset Id.
asPrivate 	Whether to acquire the asset Id as a private asset.
Returns:
The acquired asset or NULL if not acquired.
virtual bool AssetManager::addDeclaredAsset	(	(string moduleDefinition="", string assetFilePath="") 		 ) 	[virtual]
Add the specified asset against the specified module definition.

Parameters:
moduleDefinition 	The module definition that may contain declared assets.
Returns:
Whether adding declared assets was successful or not.
virtual bool AssetManager::addModuleDeclaredAssets	(	(string moduleDefinition="") 		 ) 	[virtual]
Add any the declared assets specified by the module definition.

Parameters:
moduleDefinition 	The module definition specifies the asset manifest.
Returns:
Whether adding declared assets was successful or not.
virtual string AssetManager::addPrivateAsset	(	(string assetObject="") 		 ) 	[virtual]
Adds a private asset object.

Parameters:
assetObject 	The asset object to add as a private asset.
Returns:
The allocated private asset Id.
virtual bool AssetManager::compileReferencedAssets	(	(string moduleDefinition="") 		 ) 	[virtual]
Compile the referenced assets determined by the specified module definition.

Parameters:
moduleDefinition 	The module definition specifies the asset manifest.
Returns:
Whether the compilation was successful or not.
virtual bool AssetManager::deleteAsset	(	(string assetId="", bool deleteLooseFiles=false, bool deleteDependencies=false) 		 ) 	[virtual]
Deletes the specified asset Id and optionally its loose files and asset dependencies.

Parameters:
assetId 	The selected asset Id.
deleteLooseFiles 	Whether to delete an assets loose files or not.
deleteDependencies 	Whether to delete assets that depend on this asset or not.
Returns:
Whether the asset deletion was successful or not. A failure only indicates that the specified asset was not deleted but dependent assets and their loose files may have being deleted.
virtual void AssetManager::dumpDeclaredAssets	(	() 		 ) 	[virtual]
Dumps a breakdown of all declared assets.

Returns:
No return value.
virtual int AssetManager::findAllAssets	(	(string assetQuery="", bool ignoreInternal=true, bool ignorePrivate=true) 		 ) 	[virtual]
Performs an asset query searching for all assets optionally ignoring internal assets.

Parameters:
assetQuery 	The asset query object that will be populated with the results.
ignoreInternal 	Whether to ignore internal assets or not. Optional: Defaults to true.
ignorePrivate 	Whether to ignore private assets or not. Optional: Defaults to true.
Returns:
The number of asset Ids found or (-1) if an error occurred.
virtual int AssetManager::findAssetAutoUnload	(	(string assetQuery="", bool assetAutoUnload=false, bool assetQueryAsSource=false) 		 ) 	[virtual]
Performs an asset query searching for the specified asset auto-unload flag.

Parameters:
assetQuery 	The asset query object that will be populated with the results.
assetInternal 	The asset internal flag to search for.
assetQueryAsSource 	Whether to use the asset query as the data-source rather than the asset managers database or not. Doing this effectively filters the asset query. Optional: Defaults to false.
Returns:
The number of asset Ids found or (-1) if an error occurred.
virtual int AssetManager::findAssetCategory	(	(string assetQuery="", string assetCategory="", bool assetQueryAsSource=false) 		 ) 	[virtual]
Performs an asset query searching for the specified asset category.

Parameters:
assetQuery 	The asset query object that will be populated with the results.
assetCategory 	The asset category to search for.
assetQueryAsSource 	Whether to use the asset query as the data-source rather than the asset managers database or not. Doing this effectively filters the asset query. Optional: Defaults to false.
Returns:
The number of asset Ids found or (-1) if an error occurred.
virtual int AssetManager::findAssetDependsOn	(	(string assetQuery="", string assetId="") 		 ) 	[virtual]
Performs an asset query searching for asset Ids that the specified asset Id depends on.

Parameters:
assetQuery 	The asset query object that will be populated with the results.
assetId 	The asset Id to query for any asset Ids that it depends on.
Returns:
The number of asset Ids found or (-1) if an error occurred.
virtual int AssetManager::findAssetInternal	(	(string assetQuery="", bool assetInternal=false, bool assetQueryAsSource=false) 		 ) 	[virtual]
Performs an asset query searching for the specified asset internal flag.

Parameters:
assetQuery 	The asset query object that will be populated with the results.
assetInternal 	The asset internal flag to search for.
assetQueryAsSource 	Whether to use the asset query as the data-source rather than the asset managers database or not. Doing this effectively filters the asset query. Optional: Defaults to false.
Returns:
The number of asset Ids found or (-1) if an error occurred.
virtual int AssetManager::findAssetIsDependedOn	(	(string assetQuery="", string assetId="") 		 ) 	[virtual]
Performs an asset query searching for asset Ids that depend on the specified asset Id.

Parameters:
assetQuery 	The asset query object that will be populated with the results.
assetId 	The asset Id to query for any asset Ids that may depend on it.
Returns:
The number of asset Ids found or (-1) if an error occurred.
virtual int AssetManager::findAssetLooseFile	(	(string assetQuery="", string assetLooseFile="", bool assetQueryAsSource=false) 		 ) 	[virtual]
Performs an asset query searching for the specified loose file.

Parameters:
assetQuery 	The asset query object that will be populated with the results.
assetLooseFile 	The loose-file used by the asset to search for.
assetQueryAsSource 	Whether to use the asset query as the data-source rather than the asset managers database or not. Doing this effectively filters the asset query. Optional: Defaults to false.
Returns:
The number of asset Ids found or (-1) if an error occurred.
virtual int AssetManager::findAssetName	(	(string assetQuery="", string assetName="", bool partialName=false) 		 ) 	[virtual]
Performs an asset query searching for the specified asset name.

Parameters:
assetQuery 	The asset query object that will be populated with the results.
assetName 	The asset name to search for. This may be a partial name if 'partialName' is true.
partialName 	Whether the asset name is to be used as a partial name or not. Optional: Defaults to false.
Returns:
The number of asset Ids found or (-1) if an error occurred.
virtual int AssetManager::findAssetPrivate	(	(string assetQuery="", bool assetPrivate=false, bool assetQueryAsSource=false) 		 ) 	[virtual]
Performs an asset query searching for the specified asset private flag.

Parameters:
assetQuery 	The asset query object that will be populated with the results.
assetPrivate 	The asset private flag to search for.
assetQueryAsSource 	Whether to use the asset query as the data-source rather than the asset managers database or not. Doing this effectively filters the asset query. Optional: Defaults to false.
Returns:
The number of asset Ids found or (-1) if an error occurred.
virtual int AssetManager::findAssetType	(	(string assetQuery="", string assetType="", bool assetQueryAsSource=false) 		 ) 	[virtual]
Performs an asset query searching for the specified asset type.

Parameters:
assetQuery 	The asset query object that will be populated with the results.
assetType 	The asset type to search for.
assetQueryAsSource 	Whether to use the asset query as the data-source rather than the asset managers database or not. Doing this effectively filters the asset query. Optional: Defaults to false.
Returns:
The number of asset Ids found or (-1) if an error occurred.
virtual int AssetManager::findInvalidAssetReferences	(	(string assetQuery="") 		 ) 	[virtual]
Performs an asset query searching for invalid asset references.

Parameters:
assetQuery 	The asset query object that will be populated with the results.
Returns:
The number of asset Ids found that are invalid or (-1) if an error occurred.
virtual int AssetManager::findTaggedAssets	(	(string assetQuery="", string assetTagNames="", bool assetQueryAsSource=false) 		 ) 	[virtual]
Performs an asset query searching for the specified asset tag name(s).

Parameters:
assetQuery 	The asset query object that will be populated with the results.
assetTagNames 	The asset tag name or names to search for. Multiple names can be specified using comma, space, tab or newline separation. Tags use an OR operation i.e. only assets tagged with ANY of the specified tags will be returned.
assetQueryAsSource 	Whether to use the asset query as the data-source rather than the asset managers database or not. Doing this effectively filters the asset query. Optional: Defaults to false.
Returns:
The number of asset Ids found or (-1) if an error occurred.
virtual string AssetManager::getAssetCategory	(	(string assetId="") 		 ) 	[virtual]
Gets the asset category from the specified asset Id.

Parameters:
assetId 	The selected asset Id.
Returns:
The asset category from the specified asset Id.
virtual string AssetManager::getAssetDescription	(	(string assetId="") 		 ) 	[virtual]
Gets the asset description from the specified asset Id.

Parameters:
assetId 	The selected asset Id.
Returns:
The asset description from the specified asset Id.
virtual string AssetManager::getAssetFilePath	(	(string assetId="") 		 ) 	[virtual]
Gets the asset file-path from the specified asset Id.

Parameters:
assetId 	The selected asset Id.
Returns:
The asset file - path from the specified asset Id.
virtual string AssetManager::getAssetModule	(	(string assetId="") 		 ) 	[virtual]
Gets the module definition where the the specified asset Id is located.

Parameters:
assetId 	The selected asset Id.
Returns:
The module definition where the the specified asset Id is located.
virtual string AssetManager::getAssetName	(	(string assetId="") 		 ) 	[virtual]
Gets the asset name from the specified asset Id.

Parameters:
assetId 	The selected asset Id.
Returns:
The asset name from the specified asset Id.
virtual string AssetManager::getAssetPath	(	(string assetId="") 		 ) 	[virtual]
Gets the asset path (not including the asset file) from the specified asset Id.

Parameters:
assetId 	The selected asset Id.
Returns:
The asset path(not including the asset file) from the specified asset Id.
virtual int AssetManager::getAssetTags	(	() 		 ) 	[virtual]
Gets the currently loaded asset tags manifest.

Returns:
The currently loaded asset tags manifest or zero if not loaded.
virtual string AssetManager::getAssetType	(	(string assetId="") 		 ) 	[virtual]
Gets the asset type from the specified asset Id.

Parameters:
assetId 	The selected asset Id.
Returns:
The asset type from the specified asset Id.
virtual bool AssetManager::getDeclaredAssetCount	(	() 		 ) 	[virtual]
Gets the number of declared assets.

Returns:
Returns the number of declared assets.
virtual bool AssetManager::getLoadedExternalAssetCount	(	() 		 ) 	[virtual]
Gets the number of loaded external assets.

Returns:
Returns the number of loaded external assets.
virtual bool AssetManager::getLoadedInternalAssetCount	(	() 		 ) 	[virtual]
Gets the number of loaded internal assets.

Returns:
Returns the number of loaded internal assets.
virtual bool AssetManager::getMaxLoadedExternalAssetCount	(	() 		 ) 	[virtual]
Gets the maximum number of loaded external assets.

Returns:
Returns the maximum number of loaded external assets.
virtual bool AssetManager::getMaxLoadedInternalAssetCount	(	() 		 ) 	[virtual]
Gets the maximum number of loaded internal assets.

Returns:
Returns the maximum number of loaded internal assets.
virtual bool AssetManager::getReferencedAssetCount	(	() 		 ) 	[virtual]
Gets the number of asset referenced.

Returns:
Returns the number of asset references.
virtual bool AssetManager::isAssetAutoUnload	(	(string assetId="") 		 ) 	[virtual]
Check whether the specified asset Id is auto - unload or not.

Parameters:
assetId 	The selected asset Id.
Returns:
Whether the specified asset Id is auto-unload or not.
virtual bool AssetManager::isAssetInternal	(	(string assetId="") 		 ) 	[virtual]
Check whether the specified asset Id is internal or not.

Parameters:
assetId 	The selected asset Id.
Returns:
Whether the specified asset Id is internal or not.
virtual bool AssetManager::isAssetLoaded	(	(string assetId="") 		 ) 	[virtual]
Check whether the specified asset Id is loaded or not.

Parameters:
assetId 	The selected asset Id.
Returns:
Whether the specified asset Id is loaded or not.
virtual bool AssetManager::isAssetPrivate	(	(string assetId="") 		 ) 	[virtual]
Check whether the specified asset Id is private or not.

Parameters:
assetId 	The selected asset Id.
Returns:
Whether the specified asset Id is private or not.
virtual bool AssetManager::isDeclaredAsset	(	(string assetId="") 		 ) 	[virtual]
Check whether the specified asset Id is declared or not.

Parameters:
assetId 	The selected asset Id.
Returns:
Whether the specified asset Id is declared or not.
virtual bool AssetManager::isReferencedAsset	(	(string assetId="") 		 ) 	[virtual]
Check whether the specified asset Id is referenced or not.

Parameters:
assetId 	The selected asset Id.
Returns:
Whether the specified asset Id is referenced or not.
virtual void AssetManager::purgeAssets	(	() 		 ) 	[virtual]
Purge all assets that are not referenced even if they are set to not auto-unload. Assets can be in this state because they are either set to not auto-unload or the asset manager has/is disabling auto-unload.

Returns:
No return value.
virtual void AssetManager::refreshAllAssets	(	(bool includeUnloaded=false) 		 ) 	[virtual]
Refresh all declared assets.

Parameters:
Whether 	to include currently unloaded assets in the refresh or not. Optional: Defaults to false. Refreshing all assets can be an expensive (time-consuming) operation to perform.
Returns:
No return value.
virtual void AssetManager::refreshAsset	(	(string assetId="") 		 ) 	[virtual]
Refresh the specified asset Id.

Parameters:
assetId 	The selected asset Id.
Returns:
No return value.
virtual bool AssetManager::releaseAsset	(	(string assetId="") 		 ) 	[virtual]
Release the specified asset Id. The asset should have been acquired using 'acquireAsset'.

Parameters:
assetId 	The selected asset Id.
Returns:
Whether the asset was released or not.
virtual bool AssetManager::removeDeclaredAsset	(	(string assetId="") 		 ) 	[virtual]
Remove the specified declared asset Id.

Parameters:
assetId 	The selected asset Id.
Returns:
Whether removing the declared asset was successful or not.
virtual bool AssetManager::removeDeclaredAssets	(	(string moduleDefinition="") 		 ) 	[virtual]
Remove any the declared assets specified by the module definition.

Parameters:
moduleDefinition 	The module definition that may contain declared assets.
Returns:
Whether removing declared assets was successful or not.
virtual bool AssetManager::renameDeclaredAsset	(	(string assetIdFrom="", string assetIdTo="") 		 ) 	[virtual]
Rename declared asset Id.

Parameters:
assetIdFrom 	The selected asset Id to rename from.
assetIdFrom 	The selected asset Id to rename to.
Returns:
Whether the rename was successful or not.
virtual bool AssetManager::renameReferencedAsset	(	(string assetIdFrom="", string assetIdTo="") 		 ) 	[virtual]
Rename referenced asset Id.

Parameters:
assetIdFrom 	The selected asset Id to rename from.
assetIdFrom 	The selected asset Id to rename to.
Returns:
Whether the rename was successful or not.
virtual bool AssetManager::restoreAssetTags	(	() 		 ) 	[virtual]
Restore the currently loaded asset tags manifest from disk (replace anything in memory).

Returns:
Whether the restore was successful or not.
virtual bool AssetManager::saveAssetTags	(	() 		 ) 	[virtual]
Save the currently loaded asset tags manifest.

Returns:
Whether the save was successful or not.
Member Data Documentation

bool AssetManager::EchoInfo
Whether the asset manager echos extra information to the console or not.

bool AssetManager::IgnoreAutoUnload
Whether the asset manager should ignore unloading of auto-unload assets or not.

*/
//==============================================================================
// AssetQuery
//==============================================================================
/*
Member Function Documentation

virtual string AssetBase::getAssetId	(	() 		 ) 	[virtual]
Gets the assets' Asset Id. This is only available if the asset was acquired from the asset manager.

Returns:
The assets' Asset Id.
virtual void AssetBase::refreshAsset	(	() 		 ) 	[virtual]
Refresh the asset.

Returns:
No return value.
Member Data Documentation

bool AssetBase::AssetAutoUnload
Whether the asset is automatically unloaded when an asset is released and has no other acquisitions or not.

string AssetBase::AssetCategory
An arbitrary category that can be used to categorized assets.

string AssetBase::AssetDescription
The simple description of the asset contents.

bool AssetBase::AssetInternal
Whether the asset is used internally only or not.

string AssetBase::AssetName
The name of the asset. The is not a unique identification like an asset Id.

bool AssetBase::AssetPrivate
Whether the asset is private or not.
*/
//==============================================================================
// AssetQuery
//==============================================================================
/*
virtual void AssetQuery::clear	(	() 		 ) 	[virtual]
Clears all asset Id results.Clears all asset Id results.

Returns:
() No return value.
virtual string AssetQuery::getAsset	(	(int resultIndex=-1) 		 ) 	[virtual]
Gets the asset Id at the specified query result index.

Parameters:
resultIndex 	The query result index to use.
Returns:
(assetId)The asset Id at the specified index or NULL if not valid.
virtual int AssetQuery::getCount	(	() 		 ) 	[virtual]
Gets the count of asset Id results.

Returns:
(int)The count of asset Id results.
virtual bool AssetQuery::set	(	(int queryId) 		 ) 	[virtual]
Sets the asset query to a copy of the specified asset query.

Parameters:
assetQuery 	The asset query to copy.
Returns:
Whether the operation succeeded or not.
Member Data Documentation

int AssetQuery::count
Gets the number of results in the asset query.
*/
