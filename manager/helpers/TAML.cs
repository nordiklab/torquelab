//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// TamlWrite(simObject,filename, format, compressed); 
//------------------------------------------------------------------------------
// Writes an object to a file using Taml.\n"
//------------------------------------------------------------------------------
//@param object The object to write.\n"
//@param filename The filename to write to.\n"
//@param format The file format to use.  Optional: Defaults to 'xml'.  Can be set to 'binary'.\n"
//@param compressed Whether ZIP compression is used on binary formatting or not.  Optional: Defaults to 'true'.\n"
//@return Whether the write was successful or not.")
//------------------------------------------------------------------------------

//==============================================================================
//Remove the object ID from the linkName object list
function Lab::TamlWriteObj(%this,%obj,%file,%type)
{   
   if (%file $= "")
   {
      if (%obj.getFileName() $= "")
         return;      
         
      %file = %obj.getFileName();      
   } 
   //Remove the .taml extension if present, it will be appended to the file
   %file = strreplace(%file,".taml","");   
  
   // Create an instance of TAML.
   //%taml = new Taml();
   
  TamlWrite( %obj, %file@".xml","xml",false );
 
}
//------------------------------------------------------------------------------

//==============================================================================
//Remove the object ID from the linkName object list
function Lab::WriteObj(%this,%obj,%file,%type)
{   
   if (%file $= "")
   {
      if (%obj.getFileName() $= "")
         return;      
         
      %file = %obj.getFileName();      
   } 
   //Remove the .taml extension if present, it will be appended to the file
   %file = strreplace(%file,".taml","");   
  
   // Create an instance of TAML.
%taml = new Taml();   
%taml.setWriteDefaults(true);

   %taml.write( %obj, %file@".taml" );
 
}
//------------------------------------------------------------------------------

//==============================================================================
//Remove the object ID from the linkName object list
function Lab::WriteObjAsset(%this,%obj,%file,%type)
{   
   if (%file $= "")
   {
      if (%obj.getFileName() $= "")
         return;      
         
      %file = %obj.getFileName();      
   } 
   //Remove the .taml extension if present, it will be appended to the file
   %file = strreplace(%file,".taml","");   
  
  %newAsset = new Entity()
         {
            assetName = fileBase(%file);
            versionId = 1;
            fileName = filePath(%file);
            originalFilePath = filePath(%file);
         };
    %newAsset.add(%obj);
  
   %success = TAMLWrite( %obj, %file@ ".asset.taml" );
 
}
//------------------------------------------------------------------------------

//==============================================================================
//Taml.write(obj,filename);
//------------------------------------------------------------------------------
//Writes an object to a file using Taml.\n"
//------------------------------------------------------------------------------
//@param object The object to write.\n"
//@param filename The filename to write to.\n"
//@return Whether the write was successful or not.")
//------------------------------------------------------------------------------

//==============================================================================
// Taml Read Helpers
//==============================================================================

//==============================================================================
// TamlRead(filename,format);
//------------------------------------------------------------------------------
// Read an object from a file using Taml.\n"
//------------------------------------------------------------------------------
//@param filename The filename to read from.\n"
//@param format The file format to use.  Optional: Defaults to 'xml'.  Can be set to 'binary'.\n"
//@return (Object) The object read from the file or an empty string if read failed.")
//------------------------------------------------------------------------------
//==============================================================================
//Remove the object ID from the linkName object list
function Lab::TamlReadFile(%this,%file)
{   
   if (%file $= "")
         return;      
  //Remove the .taml extension if present, it will be appended to the file
   %file = strreplace(%file,".taml","");
    %file = %file@".taml"; 
   if (!isFile(%file))   
      return;       
  %readObj = TamlRead(%file );
  return %readObj;
 
}
//------------------------------------------------------------------------------

//==============================================================================
//Taml.read( filename);
//------------------------------------------------------------------------------
//Read an object from a file using Taml.\n"
//------------------------------------------------------------------------------
//@param filename The filename to read from.\n"
//@return (Object) The object read from the file or an empty string if read failed.")
//------------------------------------------------------------------------------

//==============================================================================
//Remove the object ID from the linkName object list
function Lab::ReadFile(%this,%file)
{   
   if (%file $= "")
         return;      
  //Remove the .taml extension if present, it will be appended to the file
   %file = strreplace(%file,".taml","");
    %file = %file@".taml"; 
   if (!isFile(%file))   
      return;       
  %readObj = %taml.read(%file );
  return %readObj;
 
}
//------------------------------------------------------------------------------

//==============================================================================
// Called prior to TAML writing the object.
function SimObject::onTamlPreWrite(%this)
{   
   info("SimObject::onTamlPreWrite",%this); 
}
//------------------------------------------------------------------------------
//==============================================================================
//  Called after TAML has finished writing the object.
function SimObject::onTamlPostWrite(%this)
{   
   info("SimObject::onTamlPostWrite",%this); 
}
//------------------------------------------------------------------------------
//==============================================================================
// Called prior to TAML reading the objects state.
function SimObject::onTamlPreRead(%this)
{   
   info("SimObject::onTamlPreRead",%this); 
}
//------------------------------------------------------------------------------
//==============================================================================
// Called after TAML has read the objects state.
function SimObject::onTamlPostRead(%this)
{   
   info("SimObject::onTamlPostRead",%this); 
}
//------------------------------------------------------------------------------
//==============================================================================
// Called after 'onTamlPostRead()' and has added the object to a parent object i.e. it is a child.
function SimObject::onTamlAddParent(%this)
{   
   info("SimObject::onTamlAddParent",%this); 
}
//------------------------------------------------------------------------------
//==============================================================================
// Called during the writing of the object to allow custom properties to be written.
function SimObject::onTamlCustomWrite(%this)
{   
   info("SimObject::onTamlCustomWrite",%this); 
}
//------------------------------------------------------------------------------
//==============================================================================
// Called during the reading of the object to allow custom properties to be read.
function SimObject::onTamlCustomRead(%this)
{   
   info("SimObject::onTamlCustomRead",%this); 
}
//------------------------------------------------------------------------------
/*
//==============================================================================
// From Taml Guide - https://github.com/GarageGames/Torque2D/wiki/Taml-Guide
//==============================================================================
https://github.com/GarageGames/Torque2D/wiki/Taml-Guide#xml-format

//==============================================================================
// Callback
//==============================================================================
onTamlPreWrite() - Called prior to TAML writing the object.
onTamlPostWrite() - Called after TAML has finished writing the object.
onTamlPreRead() - Called prior to TAML reading the objects state.
onTamlPostRead() - Called after TAML has read the objects state.
onTamlAddParent() - Called after 'onTamlPostRead()' and has added the object to a parent object i.e. it is a child.
onTamlCustomWrite() - Called during the writing of the object to allow custom properties to be written.
onTamlCustomRead() - Called during the reading of the object to allow custom properties to be read.

//==============================================================================
// Callback
//==============================================================================
// Create a sprite.
%obj = new Sprite();

// Create an instance of TAML.
%taml = new Taml();

// Set the XML Binary or JSON format.
%taml.Format = Xml;//Binary Json

// Write it out.
%taml.write( %obj, "stuff.taml" );

// Read it in using the same TAML instance.
%readObj = %taml.read( "stuff.taml" );

// Delete the instance of Taml.
%taml.delete();

//==============================================================================
// Auto Format System
//==============================================================================
//------------------------------------------------------------------------------
// Default Auto Formats
//------------------------------------------------------------------------------
// Write it out in XML.
%taml.write( %obj, "stuff.taml" );

// Write it out in JSON.
%taml.write( %obj, "stuff.json" );

// Write it out in Binary.
%taml.write( %obj, "stuff.baml" );

//------------------------------------------------------------------------------
// Modify the default Auto Formats
//------------------------------------------------------------------------------
%taml.AutoFormatXmlExtension = "xml";
%taml.AutoFormatBinaryExtension = "bin";

// Write it out in XML.
%taml.write( %obj, "stuff.xml" );

// Write it out in Binary.
%taml.write( %obj, "stuff.bin" );

// Read it in.
%readObj1 = %taml.read( "stuff.xml" );
%readObj2 = %taml.read( "stuff.bin" );

//------------------------------------------------------------------------------
// Binary Format
//------------------------------------------------------------------------------
When using the Binary format you can also specify whether binary compression is used or not. This is used by default and you should not turn it off unless you have a good reason to do so.

It is controlled with the following:

setBinaryCompression(true/false)
getBinaryCompression()
"BinaryCompression" field

//------------------------------------------------------------------------------
// JSON Format
//------------------------------------------------------------------------------
The JSON format has the option to write JSON that is strictly compatible with RFC4627 or not. The default is true.

This can be toggled with the following:

setJSONStrict(true/false)
getJSONStrict()
"JSONStrict" field

//------------------------------------------------------------------------------
// TamlRead and TamlWrite specify formats
//------------------------------------------------------------------------------
// Write it out in XML.
TamlWrite( %obj, "stuff.txt", xml );

// Write it out in Binary.
TamlWrite( %obj, "stuff.dat1", binary );

// Write it out in Binary (with compression off).
TamlWrite( %obj, "stuff.dat2", binary, false );

//------------------------------------------------------------------------------
// Writing Defaults
//------------------------------------------------------------------------------
When TAML determines that it needs to write out an object it checks each field to see if it should be written or not. 
It does this internally using a mechanism that allows a type to say whether it currently wants to write the field or not. 
In nearly all cases this decision is simply based upon whether the field is at its default or not.

You can control whether TAML asks this question or not or put another way, whether it simply 
writes out all fields or only the ones that are not at their defaults.

Writing out fields that are at their defaults results in larger files which are slower to read. 
Also, setting fields that are already at their default when reading is a waste of time. Some may prefer to have all the available fields for an object written out, sometimes used as a crude way of knowing what is available but this is not only an expensive way of doing things, it's also confusing to interpret because it's not easy to see how one object is configured differently than another as all fields are written.

If you do wish to write all fields that are at defaults, you can control it using the following:
setWriteDefaults(true/false)
getWriteDefaults()
"WriteDefaults" field.

//------------------------------------------------------------------------------
// Children Object Compilation
//------------------------------------------------------------------------------
TAML will query the type to see if it implements the type TamlChildren. If it does then 
it queries how many children the object contains. It then iterates each of those children 
perform a full compilation on each i.e. it will check for fields, for child objects and 
custom state as is being described here.

This is the key to how writing a single object produces multiple objects being written. 
This only occurs for types that have been specifically written to do this by implementing 
the "TamlChildren" type. Notable types that do this are SimSet which can contain as many 
other SimObject as required and Scene which contains SceneObject.

The SimSet type is particularly useful as it allows you to persist a generic list of objects like so:

%list = new SimSet();

// Add some objects.
%list.add( new ScriptObject() );
%list.add( new ScriptObject() );
%list.add( new ScriptObject() );

// Write out the list.
TamlWrite( %list, "list.taml" );
... which produces the following XML output:

<SimSet>
   <ScriptObject/>
   <ScriptObject/>
   <ScriptObject/>
</SimSet>

//------------------------------------------------------------------------------
// TAML Callbacks
//------------------------------------------------------------------------------
TAML provides a TamlCallbacks type which defines all the callbacks that TAML will perform 
on any object being written or read. This type is implemented by SimObject so is available
 to every object that TAML can process.

Each callback is virtual therefore allowing any derived type to hook into the callback but 
you must always ensure that you additionally call the parent type.

The callbacks and their meaning as as follows:

onTamlPreWrite() - Called prior to TAML writing the object.
onTamlPostWrite() - Called after TAML has finished writing the object.
onTamlPreRead() - Called prior to TAML reading the objects state.
onTamlPostRead() - Called after TAML has read the objects state.
onTamlAddParent() - Called after 'onTamlPostRead()' and has added the object to a parent object i.e. it is a child.
onTamlCustomWrite() - Called during the writing of the object to allow custom properties to be written.
onTamlCustomRead() - Called during the reading of the object to allow custom properties to be read.
Whilst these callbacks can be used for almost any purpose, they do have some typical uses.

The "onTamlPreWrite()" and "onTamlPostWrite()" can be used for preparation of an object prior 
to writing and post-write clean-up. This kind of work should be avoided so as to not cause 
side-effects during writing but it can be useful in certain circumstances.

The "onTamlPostRead()" and "onTamlAddParent()" can useful when some specific initialization 
is required after reading or adding to a parent. Again, this should be avoided as the need 
for some specific initialization actions after the state has been set isn't a good design 
configuration but can be useful in certain circumstances.
*/
