//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
exec( "tlab/manager/modules.cs");
exec( "tlab/manager/assets.cs");
exec( "tlab/manager/events.cs");

exec( "./helpers/AssetManager.cs");
exec( "./helpers/ModuleManager.cs");
exec( "./helpers/TAML.cs");

exec( "./scripts/listeners.cs");
execPattern( "tlab/manager/objects/*.cs");
//==============================================================================
// Called right after a server has been created
function initTLManager()
{
   
   // Create the event manager. 
	Lab.initEventManager();
	
	$LabObj = Lab.newObjLab("LabObj",true);
	
   // Create the module and asset managers. Force reset if already exist
	Lab.createModuleManager(true);
	Lab.createAssetManager(true);

}
//------------------------------------------------------------------------------

