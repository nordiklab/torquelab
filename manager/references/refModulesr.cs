//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Define some Globals shared between client and server build
//==============================================================================
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

//==============================================================================
// Called right after a server has been created

//------------------------------------------------------------------------------

//==============================================================================
// TAML file examples for each type
//==============================================================================
/*
module.taml
<ModuleDefinition
	ModuleId="Art"
	VersionId="1"
	Description="Default module for the Full template game assets."
	ScriptFile=""
	CreateFunction="create"
	DestroyFunction="destroy"
	Group="Game"
	Dependencies="">
    <DeclaredAssets
      Extension="asset.taml"
      Recurse="true"/>
</ModuleDefinition>

asset.taml
<ShapeAsset
    canSave="true"
    canSaveDynamicFields="true"
    AssetName="Red_Guy"
    fileName="modules/Characters/Shapes/Red_Guy.dae"
    VersionId="1" />
asset.taml   
<ShapeAsset
    canSave="true"
    canSaveDynamicFields="true"
    AssetName="Radar"
    fileName="modules/Placeholder/Shapes/Radar.dae"
    originalFilePath="art/radarMesh/Radar.dae"
    VersionId="1" />
    
asset.taml  
<ComponentAsset
    canSave="true"
    canSaveDynamicFields="true"
    AssetName="CandleItemComponent"
    componentName="CandleItemComponent"
    componentClass="Component"
    friendlyName="Candle Item"
    componentType="Interaction"
    description="Makes the entity interactable as a candle"
    scriptFile="modules/Items/components/CandleItemComponent.cs"
    VersionId="1" /> 
    
asset.taml  
<StateMachineAsset
    canSave="true"
    canSaveDynamicFields="true"
    AssetName="NewStateMachine3"
    stateMachineFile="modules/Gameplay/stateMachines/NewStateMachine3.xml"
    VersionId="1" />
    
asset.taml    
<GameObjectAsset
    canSave="true"
    canSaveDynamicFields="true"
    AssetName="FoxPlayerAsset"
    gameObjectName="FoxPlayer"
    scriptFilePath="modules/Characters/gameObjects/FoxPlayer.cs"
    TAMLFilePath="modules/Characters/gameObjects/FoxPlayer.taml" />
    
asset.taml    
<ImageAsset
    canSave="true"
    canSaveDynamicFields="true"
    AssetName="fire_extinguisher_D"
    imageFile="modules/TheFactory/Images/fire_extinguisher_D.dds"
    useMips="true"
    isHDRImage="false"
    originalFilePath="F:\GameDev Backup\art\T3DArt\warehousePacks_T3D_Oct2012\Modern_Warehouse_Art_Pack\game\art\shapes\Modern_warehouse\Textures\fire_extinguisher_D.dds"
    VersionId="1" />
*/
