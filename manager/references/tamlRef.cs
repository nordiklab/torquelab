//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

/*
Taml, setFormat, void, (const char* formatName), ,  "(format) - Sets the format that Taml should use to read/write.\n"
                                            "@param format The format to use: 'xml' or 'binary'.\n"
                                            "@return No return value.")

//-----------------------------------------------------------------------------

Taml, getFormat, const char*, (), ,  "() - Gets the format that Taml should use to read/write.\n"
                                                    "@return The format that Taml should use to read/write.")

//-----------------------------------------------------------------------------

Taml, setAutoFormat, void, (bool autoFormat), ,  "(autoFormat) Sets whether the format type is automatically determined by the filename extension or not.\n"
                                                "@param autoFormat Whether the format type is automatically determined by the filename extension or not.\n"
                                                "@return No return value." )

//-----------------------------------------------------------------------------

Taml, getAutoFormat, bool, (), ,  "() Gets whether the format type is automatically determined by the filename extension or not.\n"
                                                "@return Whether the format type is automatically determined by the filename extension or not." )

//-----------------------------------------------------------------------------

Taml, setWriteDefaults, void, (bool writeDefaults), ,   "(writeDefaults) Sets whether to write static fields that are at their default or not.\n"
                                                    "@param writeDefaults Whether to write static fields that are at their default or not.\n"
                                                    "@return No return value." )

//-----------------------------------------------------------------------------

Taml, getWriteDefaults, bool, (), ,   "() Gets whether to write static fields that are at their default or not.\n"
                                                    "@return Whether to write static fields that are at their default or not." )

//-----------------------------------------------------------------------------

Taml, setProgenitorUpdate, void, (bool progenitorUpdate), ,    "(progenitorUpdate) Sets whether to update each type instances file-progenitor or not.\n"
                                                        "If not updating then the progenitor stay as the script that executed the call to Taml.\n"
                                                        "@param progenitorUpdate Whether to update each type instances file-progenitor or not.\n"
                                                        "@return No return value." )

//-----------------------------------------------------------------------------

Taml, getProgenitorUpdate, bool, (), ,    "() Gets whether to update each type instances file-progenitor or not.\n"
                                                        "@return Whether to update each type instances file-progenitor or not." )

//-----------------------------------------------------------------------------

Taml, setAutoFormatXmlExtension, void, (const char* extension), ,  "(extension) Sets the extension (end of filename) used to detect the XML format.\n"
                                                            "@param extension The extension (end of filename) used to detect the XML format.\n"
                                                            "@return No return value." )

//-----------------------------------------------------------------------------

Taml, getAutoFormatXmlExtension, const char*, (), ,   "() Gets the extension (end of filename) used to detect the XML format.\n"
                                                                    "@return The extension (end of filename) used to detect the XML format." )

//-----------------------------------------------------------------------------

Taml, setAutoFormatBinaryExtension, void, (const char* extension), ,   "(extension) Sets the extension (end of filename) used to detect the Binary format.\n"
                                                                "@param extension The extension (end of filename) used to detect the Binary format.\n"
                                                                "@return No return value." )

//-----------------------------------------------------------------------------

Taml, getAutoFormatBinaryExtension, const char*, (), ,    "() Gets the extension (end of filename) used to detect the Binary format.\n"
                                                                        "@return The extension (end of filename) used to detect the Binary format." )

//-----------------------------------------------------------------------------

Taml, setBinaryCompression, void, (bool compressed), ,   "(compressed) - Sets whether ZIP compression is used on binary formatting or not.\n"
                                                        "@param compressed Whether compression is on or off.\n"
                                                        "@return No return value.")

//-----------------------------------------------------------------------------

Taml, getBinaryCompression, bool, (), ,  "() - Gets whether ZIP compression is used on binary formatting or not.\n"
                                                        "@return Whether ZIP compression is used on binary formatting or not.")

//-----------------------------------------------------------------------------

Taml, setJSONStrict, void, (bool strict), , "(jsonStrict) - Sets whether to write JSON that is strictly compatible with RFC4627 or not."
    "@param jsonStrict Whether to write JSON that is strictly compatible with RFC4627 or not."
    "@return No return value.")

//-----------------------------------------------------------------------------

Taml, getJSONStrict, bool, (), , "() - Gets whether to write JSON that is strictly compatible with RFC4627 or not."
    "@return whether to write JSON that is strictly compatible with RFC4627 or not.")

//-----------------------------------------------------------------------------

Taml, write, bool, (SimObject* obj, const char* filename), ,  "(object, filename) - Writes an object to a file using Taml.\n"
                                        "@param object The object to write.\n"
                                        "@param filename The filename to write to.\n"
                                        "@return Whether the write was successful or not.")

//-----------------------------------------------------------------------------

Taml, read, SimObject*, (const char* filename), ,    "(filename) - Read an object from a file using Taml.\n"
                                                "@param filename The filename to read from.\n"
                                                "@return (Object) The object read from the file or an empty string if read failed.")

//-----------------------------------------------------------------------------

TamlWrite, bool, (SimObject* simObject, const char* filename, const char* format, bool compressed), 
                                       ("xml", true),  
                                        "(object, filename, [format], [compressed]) - Writes an object to a file using Taml.\n"
                                        "@param object The object to write.\n"
                                        "@param filename The filename to write to.\n"
                                        "@param format The file format to use.  Optional: Defaults to 'xml'.  Can be set to 'binary'.\n"
                                        "@param compressed Whether ZIP compression is used on binary formatting or not.  Optional: Defaults to 'true'.\n"
                                        "@return Whether the write was successful or not.")

//-----------------------------------------------------------------------------

TamlRead, const char*, (const char* filename, const char* format), ("xml"),    "(filename, [format]) - Read an object from a file using Taml.\n"
                                                "@param filename The filename to read from.\n"
                                                "@param format The file format to use.  Optional: Defaults to 'xml'.  Can be set to 'binary'.\n"
                                                "@return (Object) The object read from the file or an empty string if read failed.")

//-----------------------------------------------------------------------------

GenerateTamlSchema, bool, (), , "() - Generate a TAML schema file of all engine types.\n"
                                                "The schema file is specified using the console variable '" TAML_SCHEMA_VARIABLE "'.\n"
                                                "@return Whether the schema file was writtent or not." )

*/
