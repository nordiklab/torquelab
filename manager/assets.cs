//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function Lab::createAssetManager( %this, %reset )
{
	if (%reset )
		delObj(TLAssets);

	if ( isObject(TLAssets))
		return;

	new AssetManager(TLAssets);
	return TLAssets;
}
