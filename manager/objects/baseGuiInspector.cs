//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function InspectorLab::doLog(%this,%a1, %a2, %a3, %a4,%a5, %a6, %a7, %a8,%a9, %a10, %a11, %a12)
{
	if (!isObject(%this.manager))
		return;

	if (%this.manager.logInspector)
		info(%this.manager.getName(),%a1, %a2, %a3, %a4,%a5, %a6, %a7, %a8,%a9, %a10, %a11, %a12);
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

function InspectorLab::onAdd(%this)
{
	Lab.baseInspectors = strAddWord(Lab.baseInspectors,%this.getId(),true);
}
//==============================================================================
function InspectorLab::inspect(%this, %obj)
{
	//echo( "inspecting: " @ %obj );
	%name = "";

	if (isObject(%obj))
		%name = %obj.getName();
	else
		SceneFieldInfoControl.setText("");

	//InspectorNameEdit.setValue( %name );
	Parent::inspect(%this, %obj);
}
//------------------------------------------------------------------------------
//==============================================================================
// Inspector Compound Callbacks
//==============================================================================
//==============================================================================
function InspectorLab::onFieldAdded(%this, %object, %fieldName)
{
	%this.doLog("InspectorLab::onFieldAdded( %this, %object, %fieldName )",%this, %object, %fieldName);
}
//------------------------------------------------------------------------------
//==============================================================================
function InspectorLab::onFieldRemoved(%this, %object, %fieldName)
{
	%this.doLog("InspectorLab::onFieldRemoved( %this, %object, %fieldName )",%this, %object, %fieldName);
}
//------------------------------------------------------------------------------
//==============================================================================
function InspectorLab::onFieldRenamed(%this, %object, %fieldName,%newName)
{
	%this.doLog("InspectorLab::onFieldRenamed( %this, %object, %fieldName,%newName )",%this, %object, %fieldName,%newName);
}
//------------------------------------------------------------------------------
//==============================================================================
//==============================================================================
function InspectorLab::onFieldSelected(%this, %fieldName, %fieldTypeStr, %fieldDoc)
{
	SceneFieldInfoControl.setText("<font:ArialBold:14>" @ %fieldName @ "<font:ArialItalic:14> (" @ %fieldTypeStr @ ") " NL "<font:Arial:14>" @ %fieldDoc);
}
//------------------------------------------------------------------------------
//==============================================================================
function InspectorLab::onFieldRightClick(%this, %object, %fieldName,%newName)
{
	%this.doLog("InspectorLab::onFieldRightClick( %this, %object, %fieldName,%newName )",%this, %object, %fieldName,%newName);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//==============================================================================
function InspectorLab::onInspectorFieldModified(%this, %object, %fieldName, %arrayIndex, %oldValue, %newValue)
{
	%this.doLog("InspectorLab::onInspectorFieldModified( %this,  %object, %fieldName, %arrayIndex, %oldValue, %newValue )",%this, %object, %fieldName, %arrayIndex, %oldValue, %newValue);

}
//------------------------------------------------------------------------------
//==============================================================================
function InspectorLab::onDatablockFieldModified(%this, %object, %fieldName, %arrayIndex, %oldValue, %newValue)
{
	%this.doLog("InspectorLab::onDatablockFieldModified( %this,  %object, %fieldName, %arrayIndex, %oldValue, %newValue )",%this, %object, %fieldName, %arrayIndex, %oldValue, %newValue);

}
//------------------------------------------------------------------------------
//==============================================================================
// The following three methods are for fields that edit field value live and thus cannot record
// undo information during edits.  For these fields, undo information is recorded in advance and
// then either queued or disarded when the field edit is finished.

function InspectorLab::onInspectorPreFieldModification(%this, %fieldName, %arrayIndex)
{
	%this.doLog("InspectorLab::onInspectorPreFieldModification( %this, %fieldName, %arrayIndex )",%this, %fieldName, %arrayIndex);
}
//------------------------------------------------------------------------------
//==============================================================================
function InspectorLab::onInspectorPostFieldModification(%this)
{
	%this.doLog("InspectorLab::onInspectorPostFieldModification( %this)",%this);
}
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
// Only used when using the Color Type field
function InspectorLab::onInspectorDiscardFieldModification(%this)
{
	%this.doLog("InspectorLab::onInspectorDiscardFieldModification( %this)",%this, %fieldName, %arrayIndex);
}
//------------------------------------------------------------------------------

//==============================================================================
// Inspector Compound Callbacks
//==============================================================================
//==============================================================================
function InspectorLab::onBeginCompoundEdit(%this)
{
	%this.doLog("InspectorLab::onBeginCompoundEdit( %this)",%this);
	Editor.getUndoManager().pushCompound("Multiple Field Edit");
}
//------------------------------------------------------------------------------
//==============================================================================
function InspectorLab::onEndCompoundEdit(%this)
{
	%this.doLog("InspectorLab::onEndCompoundEdit( %this)",%this);
	Editor.getUndoManager().popCompound();
}
//------------------------------------------------------------------------------
//==============================================================================
function InspectorLab::onCancelCompoundEdit(%this)
{
	%this.doLog("InspectorLab::onCancelCompoundEdit( %this)",%this);
	Editor.getUndoManager().popCompound(true);
}
//------------------------------------------------------------------------------
