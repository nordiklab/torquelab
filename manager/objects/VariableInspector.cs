//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function InspectTest(%this)
{
   InspectTest.startGroup("First Group");
	InspectTest.addCallbackField("snapSoftSize","snapSoftSize","float","Set the snap soft size","2.0","","setSnapSoftSize",EWorldEditor);
	InspectTest.addField("dropType","dropType","string","Set the droptype","screenCenter","",EWorldEditor);
	 InspectTest.endGroup("");
	 InspectTest.startGroup("Next Group");
	InspectTest.addCallbackField("snapSoftSize2","snapSoftSize","float","Set the snap soft size","2.0","","setSnapSoftSize",EWorldEditor);
	InspectTest.addField("dropType2","dropType","string","Set the droptype","screenCenter","",EWorldEditor);
	 InspectTest.endGroup();
	 InspectTest.startGroup("Last Group");
	InspectTest.addCallbackField("snapSoftSize4","snapSoftSize","float","Set the snap soft size","2.0","","setSnapSoftSize",EWorldEditor);
	InspectTest.addField("dropType4","dropType","string","Set the droptype","screenCenter","",EWorldEditor);
	 InspectTest.endGroup();
	 InspectTest.startGroup("Next Group");
	 InspectTest.addCallbackField("snapSoftSize5","snapSoftSize","float","Set the snap soft size","2.0","","setSnapSoftSize",EWorldEditor);
	InspectTest.addField("dropType5","dropType","string","Set the droptype","screenCenter","",EWorldEditor);
	InspectTest.endGroup();
	InspectTest.setFieldEnabled("dropType",0);
	 // ImportOptionsList.addField("AutogenCollisions", "Auto-gen Collisions", "bool", "", "0", "", EWorldEditor);
}
//------------------------------------------------------------------------------
