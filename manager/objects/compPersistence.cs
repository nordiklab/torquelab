//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function ObjLab::addPM(%this)
{
	%name = %this.getName()@"_PM";
	delObj(%this.PM);
	delObj(%name);
	%this.PM = new PersistenceManager();
	%this.PM.setName(%name);
	//%this.PM.superClass = "PMLab";
	return %this.PM;
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjLab::getPM(%this)
{
	if (isObject(%this.PM))
		return %this.PM;
	else
		return %this.addPM();
}
//------------------------------------------------------------------------------
function ObjLab::update(%this,%obj,%fieldAndIndex,%value)
{

	%fieldData = strReplaceList(%fieldAndIndex,"[" TAB " " NL "]" TAB "");
	%field = firstWord(%fieldData);
	%fieldId = getWord(%fieldData,1);

	if (%fieldId !$= "")
	{
		%initialValue = %obj.getFieldValue(%field,%fieldId);
		%obj.setFieldValue(%field,%value,%fieldId);
	}
	else
	{
		%initialValue = %obj.getFieldValue(%field);
		%obj.setFieldValue(%field,%value);
	}

	if (%initialValue !$= %value)
	{
		%this.setDirty(%obj,1);
		%this.PMLog(%obj,"Field",%field,"Changed from",%initialValue,"To",%value);

		if (%this.isMethod("onObjectFieldChanged"))
			%this.onObjectFieldChanged(%obj,%field,%value,%initialValue);

		return true;
	}

	%this.PMLog(%obj,"Field",%field,"Value remain same",%value);
	return false;
}
//==============================================================================
// LabObj.set is same as update but will also add the object to Lab_PM for future save
/// Set the value for a field/index (index is optional and sent after the field
/// seperated with a space EX: ObjLab.set(MyMat,"diffuseMap 1","art/mymap.png");
function ObjLab::set(%this,%obj,%fieldIndexWords,%value)
{
	if (%this.update(%obj,%fieldIndexWords,%value))
		return %this.setDirty(%obj,1);

	return false;
}
//------------------------------------------------------------------------------
//==============================================================================
// LabObj.set is same as update but will also add the object to Lab_PM for future save
function ObjLab::setAndSave(%this,%obj,%fieldIndexWords,%value)
{
	if (%this.set(%obj,%fieldIndexWords,%value))
		return %this.saveObj(%obj);

	return false;
}
//------------------------------------------------------------------------------

//==============================================================================
// The %dirty argument have ben replace with %file to behave like PM
function ObjLab::setDirty(%this,%obj,%file)
{
	//Check for the onObjectSetDirty callback even if already dirty
	// Only onObjectDirtyChanged will be skipped in that case
	if (%this.isMethod("onObjectSetDirty"))
		%this.onObjectSetDirty(%obj,true);

	%pm = %this.getPM();
	%isDirty = %pm.isDirty(%obj);

	//If not dirty, nothing to do here unless a file have been supplied
	if (%isDirty && %file $= "")
		return;

	%pm.setDirty(%obj,%file);

	if (%this.isMethod("onObjectDirtyChanged"))
		%this.onObjectDirtyChanged(%obj,true);

	%this.PMLog(%obj,"Dirty state changed from","false","To","true");

	return %dirty;
}
//------------------------------------------------------------------------------
//==============================================================================
// The %dirty argument have ben replace with %file to behave like PM
function ObjLab::setNotDirty(%this,%obj)
{
	//Check for the onObjectSetDirty callback even if already not dirty
	// Only onObjectDirtyChanged will be skipped in that case
	if (%this.isMethod("onObjectSetDirty"))
		MatLab.onObjectSetDirty(%obj,false);

	%pm = %this.getPM();
	%isDirty = %pm.isDirty(%obj);

	if (!%isDirty)
		return; //If not dirty, nothing to do here

	%pm.removeDirty(%obj);

	if (%this.isMethod("onObjectDirtyChanged"))
		%this.onObjectDirtyChanged(%obj,false);

	%this.PMLog(%obj,"Dirty state changed from","true","To","false");

	return true;
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjLab::sendDirty(%this,%obj,%dirty)
{
	if (%this.isMethod("onObjectSetDirty"))
		%this.onObjectSetDirty(%obj,%dirty);

	%this.PMLog(%obj,"Dirty state set to",%dirty);

}
//------------------------------------------------------------------------------
//==============================================================================
function ObjLab::saveObj(%this,%obj,%force)
{
	%pm = %this.getPM();
	%isDirty = %pm.isDirty(%obj);

	if (!%isDirty)
	{
		if(!%force)
			return false;

		%pm.setDirty(%obj);
	}

	%pm.saveDirtyObject(%obj);

	if (%this.isMethod("onObjectSaved"))
		%this.onObjectSaved(%obj);

	%this.sendDirty(%obj,0);

	%this.PMLog(%obj,"saved to file",%obj.getFileName());

	return true;
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjLab::saveAll(%this,%useList)
{
	%pm = %this.getPM();
	%list = %pm.listDirty();

	if (%useList)
	{
		%this.saveList(%list);
		return;
	}

	//%this.PMLog("All Dirty objects saved.","List",%list);
	%pm.saveDirty();

	if (%this.isMethod("onObjectDirtySaved"))
		%this.onObjectDirtySaved(%list);

}
//------------------------------------------------------------------------------

//==============================================================================
function ObjLab::saveList(%this,%objs)
{
	%pm = %this.getPM();
	%list = %objs;

	foreach$(%obj in %objs)
	{
		if (isObject(%obj))
			%pm.setDirty(%obj);
	}

	%pm.saveDirty();
	%this.PMLog("Custom object list saved.","List",%objs);
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjLab::forceSave(%this,%obj)
{
	%pm = %this.getPM();
	%pm.setDirty(%obj);
	%pm.schedule(100,saveDirtyObject,%obj);
	%this.PMLog(%obj,"Force saved to file:",%obj.getFileName());

	if (%this.isMethod("onObjectSaved"))
		%this.onObjectSaved(%obj);
}
//------------------------------------------------------------------------------
//==============================================================================
// UNREVIEWED
//==============================================================================

//==============================================================================
function ObjLab::listDirty(%this)
{
	%pm = %this.getPM();
	%pm.listDirty();
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjLab::isDirty(%this,%obj)
{
	%pm = %this.getPM();
	return %pm.isDirty(%obj);
}
//------------------------------------------------------------------------------

//==============================================================================
//DefineConsoleMethod( PersistenceManager, removeField, void, (const char * objName, const char * fieldName), , "(SimObject object, string fieldName)"
//            "Remove a specific field from an object declaration.")
function ObjLab::removeField(%this,%obj,%field,%saveNow)
{
	if (!isObject(%obj)||!%obj.isField(%field))
		return;

	%pm = %this.getPM();
	%pm.removeField(%obj,%field);
	%pm.setDirty(%obj);

	if (%saveNow)
	{
		%pm.saveDirtyObject(%obj);
		%this.PMLog(%obj,"Field removed:",%field,"Automatic saved to file:",%obj.getFileName());
	}
	else
		%this.PMLog(%obj,"Field removed:",%field);

	if (%this.isMethod("onObjectFieldRemoved"))
		%this.onObjectFieldRemoved(%obj,%field);
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjLab::clearField(%this,%obj,%field,%saveNow)
{
	%this.removeField(%obj,%field,%saveNow);
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjLab::removeFromFile(%this,%obj,%file)
{
	if (!isObject(%obj))
		return;

	%pm = %this.getPM();

	if (isFile(%file))
		%pm.removeObjectFromFile(%obj,%file);
	else
		%pm.removeObjectFromFile(%obj);
}
//------------------------------------------------------------------------------

/*

DefineConsoleMethod( PersistenceManager, deleteObjectsFromFile, void, ( const char * fileName ), , "( fileName )"
              "Delete all of the objects that are created from the given file." )
{
   // Delete Objects.
   object->deleteObjectsFromFile( fileName );
}

DefineConsoleMethod( PersistenceManager, setDirty, void,  ( const char * objName, const char * fileName ), (""), "(SimObject object, [filename])"
              "Mark an existing SimObject as dirty (will be written out when saveDirty() is called).")
{
   SimObject *dirtyObject = NULL;
   if (dStrcmp(objName,"") != 0)
   {
      if (!Sim::findObject(objName, dirtyObject))
      {
         Con::printf("PersistenceManager::setDirty(): Invalid SimObject: %s", objName);
         return;
      }
   }

   // Prevent ourselves from shooting us in the foot.
   if( dirtyObject == Sim::getRootGroup() )
   {
      Con::errorf( "PersistenceManager::setDirty(): Cannot save RootGroup" );
      return;
   }

   if (dirtyObject)
   {
      if (dStrcmp( fileName,"")!=0)
         object->setDirty(dirtyObject, fileName);
      else
         object->setDirty(dirtyObject);
   }
}

DefineConsoleMethod( PersistenceManager, removeDirty, void, ( const char * objName ), , "(SimObject object)"
              "Remove a SimObject from the dirty list.")
{
   SimObject *dirtyObject = NULL;
	if (dStrcmp(  objName,"")!=0)
   {
      if (!Sim::findObject(objName, dirtyObject))
      {
         Con::printf("PersistenceManager::removeDirty(): Invalid SimObject: %s", objName);
         return;
      }
   }

   if (dirtyObject)
      object->removeDirty(dirtyObject);
}

DefineConsoleMethod( PersistenceManager, isDirty, bool, ( const char * objName ), , "(SimObject object)"
              "Returns true if the SimObject is on the dirty list.")
{
   SimObject *dirtyObject = NULL;
   if (dStrcmp ( objName,"")!=0)
   {
      if (!Sim::findObject(objName, dirtyObject))
      {
         Con::printf("PersistenceManager::isDirty(): Invalid SimObject: %s", objName);
         return false;
      }
   }

   if (dirtyObject)
      return object->isDirty(dirtyObject);

   return false;
}

DefineConsoleMethod( PersistenceManager, hasDirty, bool, (), , "()"
              "Returns true if the manager has dirty objects to save." )
{
   return object->hasDirty();
}

DefineConsoleMethod( PersistenceManager, getDirtyObjectCount, S32, (), , "()"
              "Returns the number of dirty objects." )
{
   return object->getDirtyList().size();
}

DefineConsoleMethod( PersistenceManager, getDirtyObject, S32, (S32 index), , "( index )"
              "Returns the ith dirty object." )
{
   if ( index < 0 || index >= object->getDirtyList().size() )
   {
      Con::warnf( "PersistenceManager::getDirtyObject() - Index (%s) out of range.", index );
      return 0;
   }

   // Fetch Object.
   const PersistenceManager::DirtyObject& dirtyObject = object->getDirtyList()[index];

   // Return Id.
   return ( dirtyObject.getObject() ) ? dirtyObject.getObject()->getId() : 0;
}

DefineConsoleMethod( PersistenceManager, listDirty, void, (), , "()"
              "Prints the dirty list to the console.")
{
   const PersistenceManager::DirtyList dirtyList = object->getDirtyList();

   for(U32 i = 0; i < dirtyList.size(); i++)
   {
      const PersistenceManager::DirtyObject& dirtyObject = dirtyList[i];

      if (dirtyObject.isNull())
         continue;

      SimObject *obj = dirtyObject.getObject();
      bool isSet = dynamic_cast<SimSet *>(obj) != 0;
      const char *name = obj->getName();
      if (name)
      {
         Con::printf("   %d,\"%s\": %s %s %s", obj->getId(), name,
         obj->getClassName(), dirtyObject.fileName, isSet ? "(g)":"");
      }
      else
      {
         Con::printf("   %d: %s %s, %s", obj->getId(), obj->getClassName(),
         dirtyObject.fileName, isSet ? "(g)" : "");
      }
   }
}

DefineConsoleMethod( PersistenceManager, saveDirty, bool, (), , "()"
              "Saves all of the SimObject's on the dirty list to their respective files.")
{
   return object->saveDirty();
}

DefineConsoleMethod( PersistenceManager, saveDirtyObject, bool, (const char * objName), , "(SimObject object)"
              "Save a dirty SimObject to it's file.")
{
   SimObject *dirtyObject = NULL;
   if (dStrcmp (  objName, "")!=0)
   {
      if (!Sim::findObject(objName, dirtyObject))
      {
         Con::printf("%s(): Invalid SimObject: %s", object->getName(), objName);
         return false;
      }
   }

   if (dirtyObject)
      return object->saveDirtyObject(dirtyObject);
   return false;
}

DefineConsoleMethod( PersistenceManager, clearAll, void, (), , "()"
              "Clears all the tracked objects without saving them." )
{
   object->clearAll();
}

DefineConsoleMethod( PersistenceManager, removeObjectFromFile, void, (const char * objName, const char * filename),("") , "(SimObject object, [filename])"
              "Remove an existing SimObject from a file (can optionally specify a different file than \
               the one it was created in.")
{
   SimObject *dirtyObject = NULL;
   if (dStrcmp ( objName , "")!=0)
   {
      if (!Sim::findObject(objName, dirtyObject))
      {
         Con::printf("PersistenceManager::removeObjectFromFile(): Invalid SimObject: %s", objName);
         return;
      }
   }

   if (dirtyObject)
   {
      if (dStrcmp( filename,"")!=0)
         object->removeObjectFromFile(dirtyObject, filename);
      else
         object->removeObjectFromFile(dirtyObject);
   }
}

DefineConsoleMethod( PersistenceManager, removeField, void, (const char * objName, const char * fieldName), , "(SimObject object, string fieldName)"
              "Remove a specific field from an object declaration.")
{
   SimObject *dirtyObject = NULL;
   if (dStrcmp(objName,"")!=0)
   {
      if (!Sim::findObject(objName, dirtyObject))
      {
         Con::printf("PersistenceManager::removeField(): Invalid SimObject: %s", objName);
         return;
      }
   }

   if (dirtyObject)
   {
      if (dStrcmp(fieldName,"") != 0)
         object->addRemoveField(dirtyObject, fieldName);
   }
}

*/
