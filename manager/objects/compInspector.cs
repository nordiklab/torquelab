//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Get the GuiInspector assigned to this ObjLab, if none LabInspect will be used
function ObjLab::getInspector(%this)
{
	if (isObject(%this.inspector))
		%inspector = %this.inspector;
	else
		%inspector = LabInspect;

	%inspector.manager = %this;
	return %inspector;
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjLab::inspectObj(%this,%obj,%doApply)
{
	%inspector = %this.getInspector();
	%inspector.inspect(%obj);

	if (%doApply)
		%inspector.apply();
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjLab::updateList(%this,%objIds,%field,%value,%fieldId)
{
	%inspector = %this.getInspector();
	%inspector.inspect();

	%lastObj = lastWord(%objIds);

	foreach$(%id in %objIds)
		%inspector.addInspect(%id,(%lastObj $= %id ));

	%inspector.setObjectField(%field,%value);
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjLab::inspectField(%this,%obj,%field,%value,%fieldId)
{
	%inspector = %this.getInspector();
	%inspector.inspect(%obj);

	if (%obj.isMemberOfClass(ArrayObject))
	{
		%initialValue = %obj.getVal(%field);
		%obj.setVal(%field,%value);
	}
	else
	{
		%inspector = %this.getInspector();
		%inspector.inspect(%obj);

		%initialValue = %obj.getFieldValue(%field);
		%inspector.setObjectField(%field,%value);
		/*
		if (%field $= "name")
			%inspector.setObjectField(%field,%value);
		else if (%fieldId !$= "")
			%obj.setFieldValue(%field,%value,%fieldId);
		else
			%obj.setFieldValue(%field,%value);
		*/
		%inspector.apply();
	}

	if (%initialValue !$= %value)
		return true;

	return false;
}
//------------------------------------------------------------------------------
