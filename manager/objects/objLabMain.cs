//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// The ObjLab class is used by components manager to manage certain related
// objects. It include it own PersistenceManager to save dirty objects which is
// linked to predefined function to update those objects. To use it, the function
// ObjManager.set(%obj,%field,%value); must be used which would deal with everything
// and look for possible defined callbacks to return the results.
//==============================================================================
// Here's the list of callbacks
//------------------------------------------------------------------------------
// Called from ManagerLab when DirtyState changed on an object
//function ObjLab::onObjectDirtyChanged(%this,%obj,%dirty)
//------------------------------------------------------------------------------
// Called from ManagerLab when DirtyState changed on an object
//function ObjLab::onObjectFieldChanged(%this,%obj,%field,%value,%initialValue)
//------------------------------------------------------------------------------
// Called from ManagerLab when DirtyState changed on an object
//function ObjLab::onObjectSaved(%this,%obj)
//------------------------------------------------------------------------------
// Called from ManagerLab when DirtyState changed on an object
//function ObjLab::onObjectFieldRemoved(%this,%obj,%field)
//------------------------------------------------------------------------------
// Called from ManagerLab when DirtyState changed on an object
//function ObjLab::onObjectDirtySaved(%this,%obj)
//------------------------------------------------------------------------------
// Called from ManagerLab when DirtyState changed on an object
//function ObjLab::onObjectSetDirty(%this,%obj,%dirty)
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function Lab::newObjLab(%this,%name,%deleteExist)
{
	if (%deleteExist)
		delObj(%name);

	%objLab = %name;

	if (!isObject(%objLab))
		%objLab = newScriptObject(%name,LabGroup);

	ELink.link(%objLab,"ObjLab");
	%objLab.superClass = "ObjLab";
	//Assign a PersistenceManager to the Obj named the same with _PM suffix
	%objLab.getPM();
	return %objLab;
}
//------------------------------------------------------------------------------
//==============================================================================
// Loggin for PersistenceManager actions (ObjLab.logPM must be tru)
function ObjLab::PMLog(%this,%a1, %a2, %a3, %a4,%a5, %a6, %a7, %a8,%a9, %a10, %a11, %a12)
{
	if (%this.logPM)
		info(%this.getName(),%a1, %a2, %a3, %a4,%a5, %a6, %a7, %a8,%a9, %a10, %a11, %a12);
}
//------------------------------------------------------------------------------
