//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function Lab::createModuleManager( %this, %scanNow )
{
	delObj(TLModules);

	new ModuleManager(TLModules);
	TLModules.setModuleExtension("module");
	TLModules.EchoInfo = 0;
// Scan for modules using my module manager.

	TLModules.moduleSet["Tools"] = newSimSet("TLTools",LabGroup);
	TLModules.moduleSet["Plugins"] = newSimSet("TLPlugins",LabGroup);
	TLModules.moduleSet["Themes"] = newSimSet("TLThemes",LabGroup);

	TLModules.addListener(Lab);
	Lab.joinEvent("ModuleChanged",Lab);

	if (%scanNow)
		Lab.scanModules();

	return TLModules;
}
//==============================================================================
// Scan TorqueLab modules (under tlab/) with optional path relative to tlab/
function Lab::scanModules( %this, %tlabFolder )
{
	%folder = "tlab/"@%tlabFolder@"/";
	%folder = strreplace(%folder,"//","/");
	TLModules.scanModules(%folder);
   
}
//Delete all modules and rescan all tlab/ folder
function Lab::rescanModules( %this )
{
	%this.createModuleManager(1);
}

//==============================================================================
function Lab::saveModule( %this, %module,%noDirtyCheck )
{
	//Check if the moduleId or module object have been sent, we need the object
	%modObj = TLModules.getModuleObj(%module);

	if (!isObject(%modObj) )
		return;

	if (!TLModules.isDirty[%modObj.moduleID] && !%noDirtyCheck)
		return;

	%modObj.save();
	TLModules.isDirty[%modObj.moduleID] = false;

}
//==============================================================================
function Lab::saveModules( %this, %moduleIds,%noDirtyCheck)
{
	if (getWordCount(%moduleIds) <=0)
		%moduleIds = TLModules.findModules();

	foreach$(%in in %moduleIds)
	{
		if (%in.Activated $= "")
			%in.set("Activated",1);

		%this.saveModule(%in,%noDirtyCheck);
	}
}
//==============================================================================
// TorqueLab Manager Module Listener Callbacks
//==============================================================================
//==============================================================================
function Lab::onModuleRegister( %this, %module )
{
	if (%module.isMethod("onRegister"))
		%module.call("onRegister");

	%activated = $Cfg_Modules_[%module.moduleId,"Activated"];

	if (%activated $= "")
	{
		if (%module.Activated !$= "")
			%activated = %module.Activated;
		else
			%activated = "1";

		$Cfg_Modules_[%module.moduleId,"Activated"] = %activated;
	}

	%module.set("Activated",%activated);

   TLModules.module[%module.moduleId] = %module;
	if (isObject(TLModules.moduleSet[%module.group]))
		TLModules.moduleSet[%module.group].add(%module);

}

function Lab::onModulePreLoad( %this, %module )
{
	if (%module.isMethod("onPreLoad"))
		%module.call("onPreLoad");
		
   //Check if it's a theme, if so call the theme load function
   if (%module.group $= "Themes")
   {
      %module.activated = 1;    
      Lab.preloadTheme(%module.moduleId);
      
   }
}

function Lab::onModulePostLoad( %this, %module )
{

	if (%module.isMethod("onPostLoad"))
		%module.call("onPostLoad");

	%dependencies = %module.Dependencies;

	if (%dependencies $= "")
		return;

	%fieldDepend = strreplace(%dependencies,",","\t");

	foreach$(%depend in %fieldDepend)
	{
		%data = strreplace(%dependencies,"=","\t");
		%modId = getField(%data,0);
		%version = getField(%data,1);
		//info(%module.moduleId,"depends on",%modId,"Version",%version);
		TLModules.dependOn[%modId] = strAddWord(TLModules.dependOn[%module.moduleId],%module.moduleId, 1);
	}

}

function Lab::onModulePreUnload( %this, %module )
{

	if (%module.isMethod("onPreUnload"))
		%module.call("onPreUnload");

}

function Lab::onModulePostUnload( %this, %module )
{

	if (%module.isMethod("onPostUnload"))
		%module.call("onPostUnload");
   
   //Check if it's a theme, if so call the theme load function
   if (%module.group $= "Themes")
   {
      
      info("onModulePostUnload","Theme",%module.moduleId);
     
   }
}
//------------------------------------------------------------------------------

function Lab::onModuleChanged( %this, %module )
{
	//devLog("Lab::onModuleChanged",%module.moduleId,"Group",%module.group,"Type:",%module.type);

	if (%module.isMethod("onChanged"))
		%module.call("onChanged");
}
//------------------------------------------------------------------------------
