//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function LabModuleManager::onWake( %this )
{
 
  //LabModuleManager.updateData();
}

function LabModuleManager::updateData( %this )
{
   LMM_ModulePill.visible = 0;
   LMM_StackList_Plugins.clear();
   foreach(%mod in TLModules.moduleSet["Plugins"])
   {
      %pill = cloneGui(LMM_ModulePill,LMM_StackList_Plugins,%mod.moduleId,0);
      %pill-->moduleName.text = %mod.moduleId;
      %pill-->mouseEvent.moduleID = %mod.moduleId;
      %pill-->mouseEvent.module= %mod;
       %pill-->checkbox.text = "";
       %pill-->checkbox.setStateOn(%mod.activated);
   }
    foreach(%mod in TLModules.moduleSet["Tools"])
   {
      %pill = cloneGui(LMM_ModulePill,LMM_StackList_Plugins,%mod.moduleId,0);
      %pill-->moduleName.text = %mod.moduleId;
      %pill-->mouseEvent.moduleID = %mod.moduleId;
      %pill-->mouseEvent.module= %mod;
       %pill-->checkbox.text = "";
       %pill-->checkbox.setStateOn(%mod.activated);
   }
    foreach(%mod in TLModules.moduleSet["Themes"])
   {
      %pill = cloneGui(LMM_ModulePill,LMM_StackList_Plugins,%mod.moduleId,0);
      %pill-->moduleName.text = %mod.moduleId;
      %pill-->mouseEvent.moduleID = %mod.moduleId;
      %pill-->mouseEvent.module= %mod;
       %pill-->checkbox.text = "";
       %pill-->checkbox.setStateOn(%mod.activated);
   }
  
}

function LabModuleManager::selectModule( %this,%modObj)
{
   
   %group = %modObj.group;
   info("Group:",%group);
   switch$(%group){
      case "Plugins":
       LMM_Box_Plugins.text = %this.moduleId SPC "Manage options and actions";
      LMM_InfoBox_Plugins-->ModuleId.text = %modObj.ModuleId;
  LMM_InfoBox_Plugins-->VersionId.text = %modObj.VersionId;
  LMM_InfoBox_Plugins-->description.setText(%modObj.description);
   // case "Tools":
     //case "Themes":

   }
  
}

//==============================================================================
// ModuleManager Helpers
//==============================================================================
//==============================================================================
function LabModuleListMouse::onMouseUp( %this, %modifier, %mousePoint, %mouseClickCount )
{
  info("ModuleList selected",%this);

  LabModuleManager.selectModule(%this.module);
}
/*
void  onMouseDown (int modifier, Point2I mousePoint, int mouseClickCount) 
  Callback that occurs whenever the mouse is pressed down while in this control. 
 
void  onMouseDragged (int modifier, Point2I mousePoint, int mouseClickCount) 
  Callback that occurs whenever the mouse is dragged while in this control. 
 
void  onMouseEnter (int modifier, Point2I mousePoint, int mouseClickCount) 
  Callback that occurs whenever the mouse enters this control. 
 
void  onMouseLeave (int modifier, Point2I mousePoint, int mouseClickCount) 
  Callback that occurs whenever the mouse leaves this control. 
 
void  onMouseMove (int modifier, Point2I mousePoint, int mouseClickCount) 
  Callback that occurs whenever the mouse is moved (without dragging) while in this control. 
 
void  onMouseUp (int modifier, Point2I mousePoint, int mouseClickCount) 
  Callback that occurs whenever the mouse is released while in this control. 
 
void  onRightMouseDown (int modifier, Point2I mousePoint, int mouseClickCount) 
  Callback that occurs whenever the right mouse button is pressed while in this control. 
 
void  onRightMouseDragged (int modifier, Point2I mousePoint, int mouseClickCount) 
  Callback that occurs whenever the mouse is dragged in this control while the right mouse button is pressed. 
 
void  onRightMouseUp (int modifier, Point2I mousePoint, int mouseClickCount) 
  Callback that occurs whenever the right mouse button is released while in this control.  

*/
