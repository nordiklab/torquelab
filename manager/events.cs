//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Create the EventManager.
function Lab::initEventManager(%this)
{
	if (!isObject(TLEvents))
	{
		new EventManager(TLEvents)
		{
			queue = "TLEvents";
		};
	}

	//Temp, moved from useless function which called this before
	%this.setBaseEvents();
}
//------------------------------------------------------------------------------

//==============================================================================
// Set the default events listener can subscribe to
function Lab::setBaseEvents(%this)
{
	%this.setEvent("SceneChanged");

	%this.setEvent("SceneSelectionChanged");
	%this.setEvent("SceneObjectAddedChanged");
	%this.setEvent("MissionPreLoad");
	%this.setEvent("MissionPostLoad");
	%this.setEvent("GameCreated");
	%this.setEvent("GameLoaded");
	%this.setEvent("GameStart");
	%this.setEvent("GameEnd");
	%this.setEvent("LoadDatablocks");
	%this.setEvent("MissionSaved");

	%this.setEvent("ModuleChanged");
}
//------------------------------------------------------------------------------

//==============================================================================
// EventManager functions helpers
//==============================================================================

//==============================================================================
// Create the EventManager.
function Lab::setEvent(%this,%event)
{
	if (TLEvents.isRegisteredEvent(%event))
		return;

	// Trigger the event.
	TLEvents.registerEvent(%event);
}
//------------------------------------------------------------------------------
//==============================================================================
// Create the EventManager.
function Lab::postEvent(%this,%event,%data)
{
	if (!TLEvents.isRegisteredEvent(%event))
		TLEvents.registerEvent(%event);

	// Trigger the event.
	TLEvents.postEvent(%event, %data);
}
//------------------------------------------------------------------------------
//==============================================================================
// Create the EventManager.
function Lab::joinEvent(%this,%event,%listener,%forceEvent)
{
   //Make sure an event is sent
	if (%event $= "")
	{
		warnLog("EventManager","No event specified for JoinEvent");
		return;
	}
   
   //CHeck if event is registered, if not and %forceEvent is true, register the new event
	if (!TLEvents.isRegisteredEvent(%event))
	{
		if (!%forceEvent)
		{
			warnLog("EventManager","Trying to join an inexistant event:",%event,"For listener:",%listener);
			return;
		}

		%this.setEvent(%event);
	}

   //List of listeners for that event
   %list = TLEvents.subscriber[%event];
   //If the listener is not an object, create a ScriptMsgListener with the listener text as SuperClass
	if (!isObject(%listener))
	{
		%class = "TLListener";

		if (%listener !$= "")
			%class = %listener;

		%listener = new ScriptMsgListener()
		{
			superClass = %class;
		};
	}
	else
	{
		// Make sure it's not already a listener of that event
		if (getWordIndex(%list,%listener.getId()) !$= "-1")
		{
			info("EventManager",%list,"Already",%listener.getId());
			return;
		}
	}

	// Trigger the event.
	TLEvents.subscribe(%listener,  %event);

	//Used to prevent multiple subscribe for a listener to same event
	TLEvents.subscriber[%event] = strAddWord(%list,%listener.getId(),1);
	return %listener;
}
//------------------------------------------------------------------------------
//==============================================================================
// Create the EventManager.
function Lab::leaveEvent(%this,%event,%listener)
{
	// Trigger the event.
	TLEvents.subscriber[%event] = strRemoveWord(TLEvents.subscriber[%event],%listener.getId());
	return TLEvents.remove(%listener,  %event);
}
//------------------------------------------------------------------------------
