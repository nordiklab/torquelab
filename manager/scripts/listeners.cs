//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

/*
//==============================================================================
// Module Events and Listeners
//==============================================================================
When a module system performs an important action it raises a corresponding event. The following events are raised:

onModuleRegister - Called when a module is scanned, validated and finally registered.
onModulePreLoad - Called prior to a module loading
onModulePostLoad - Called after a module has been loaded
onModulePreUnload - Called prior to a module unloading
onModulePostUnload - Called after a module has been unloaded
The module system allows you to register your own objects to receive these notifications by using the following methods:

addListener()
removeListener()
When you perform these actions from TorqueScript, you can designate an object as a listener. When this happens, the module system will then perform the above callbacks on the object(s) you specify like so:

// Create a listener object.
new ScriptObject(MyModuleListener);

// Add my object as a listener.
TLModules.addListener(MyModuleListener);
If you were to do the above then you could receive notifications like so:

function MyModuleListener::onModuleRegister( %module )
{
}

function MyModuleListener::onModulePreLoad( %module )
{
}

function MyModuleListener::onModulePostLoad( %module )
{
}

function MyModuleListener::onModulePreUnload( %module )
{
}

function MyModuleListener::onModulePostUnload( %module )
{
}
  
If knowing when modules are registered, loading or unloaded is required then this functionality will provide that information.
If the object designated as a listener implements the interface class "ModuleCallbacks" then it will additionally receive direct callbacks in C++.     
//==============================================================================
//The Module Methods
//==============================================================================
TLModules.LoadExplicit( "RedModule", 2 );
TLModules.UnloadExplicit( "RedModule", 2 );

To load that group you then simply do:
ModuleManager.LoadGroup( "Colors" );

If you wanted to unload the group you simply do:
ModuleManager.UnloadGroup( "Colors" );
//==============================================================================
//The Module Definition
//==============================================================================
ModuleId - A unique string Id for the module. It can contain any characters except a comma or semi-colon (the asset scope character).
VersionId - The version Id. Breaking changes to a module should use a higher version Id.
Description - The description typically used for debugging purposes but can be used for anything.
Author - The author of the module.
Group - The module group used typically when loading modules as a group.
//------------------------------------------------------------------------------
// Optional Parameters
//------------------------------------------------------------------------------
BuildId - The build Id. Non-breaking changes to a module should use a higher build Id. Optional: If not specified then the build Id will be zero.
Enabled - Whether the module is enabled or not. When disabled, it is effectively ignored. Optional: If not specified then the module is enabled.
Deprecated - Whether the module is deprecated or not. Optional: If not specified then the module is not deprecated.

Type - The module type typically used to distinguish modules during module enumeration. 
   Optional: If not specified then the type is empty although this can still be used as a pseudo 'global' type for instance.
Dependencies - A comma-separated list of module Ids/VersionIds (=,=,etc) which this
      module depends upon. Optional: If not specified then no dependencies are assumed.
ScriptFile - The name of the script file to compile when loading the module. Optional.
CreateFunction - The name of the function used to create the module. 
      Optional: If not specified then no create function is called.
DestroyFunction - The name of the function used to destroy the module. 
      Optional: If not specified then no destroy function is called.
ScopeSet - The scope set used to control the lifetime scope of objects that the module uses. 
      Objects added to this set are destroyed automatically when the module is unloaded.
AssetTagsManifest - The name of tags asset manifest file if this module contains asset tags. 
      Optional: If not specified then no asset tags will be found for this module. Currently, only a single asset tag manifest should exist.
Synchronized - Whether the module should be synchronized or not. 
      Optional: If not specified then the module is not synchronized.
CriticalMerge - Whether the merging of a module prior to a restart is critical or not. 
      Optional: If not specified then the module is not merge critical.
 
//==============================================================================
// ModuleManager Docs
//==============================================================================

Public Member Functions

virtual void 	addListener ((string listenerObject=""))
virtual bool 	canMergeModules ((string mergeSourcePath=""))
virtual string 	copyModule ((string sourceModuleDefinition="", string pTargetModuleId="", string pTargetPath="", bool useVersionPathing=false))
virtual string 	findModule ((string pModuleId="", int pVersionId=0))
virtual string 	findModules ((bool loadedOnly=false))
virtual string 	findModuleTypes ((string pModuleType="", bool loadedOnly=false))
virtual bool 	isModuleMergeAvailable (())
virtual bool 	loadExplicit ((string pModuleId="", int pVersionId=-1))
virtual bool 	loadGroup ((string pModuleGroup=""))
virtual bool 	mergeModules ((string pMergeTargetPath="", bool removeMergeDefinition=false, bool registerNewModules=false))
virtual void 	removeListener ((string listenerObject=""))
virtual bool 	scanModules ((string pRootPath="", bool rootOnly=false))
virtual bool 	setModuleExtension ((string moduleExtension=""))
virtual bool 	synchronizeDependencies ((string rootModuleDefinition="", string pTargetDependencyFolder=""))
virtual bool 	unloadExplicit ((string pModuleId=""))
virtual bool 	unloadGroup ((string pModuleGroup=""))
virtual bool 	unregisterModule ((string pModuleId="", bool versionId=false))
Public Attributes

bool 	EchoInfo
bool 	EnforceDependencies
Member Function Documentation

virtual void ModuleManager::addListener	(	(string listenerObject="") 		 ) 	[virtual]
Registers the specified object as a listener for module notifications.

Parameters:
listenerObject 	The object to start receiving module notifications.
Returns:
No return value.
virtual bool ModuleManager::canMergeModules	(	(string mergeSourcePath="") 		 ) 	[virtual]
Checks whether a module merge using the modules in the source path can current happen or not.

Parameters:
mergeSourcePath 	The path where modules to be merged are located.
Returns:
Whether a module merge using the modules in the source path can current happen or not.
virtual string ModuleManager::copyModule	(	(string sourceModuleDefinition="", string pTargetModuleId="", string pTargetPath="", bool useVersionPathing=false) 		 ) 	[virtual]
Copy the module to a new location with a new module Id.

Parameters:
sourceModuleDefinition 	The module definition to copy.
targetModuleId 	The module Id to rename the copied module to including all references to the source module Id.It is valid to specifiy the source module Id to produce an identical copy.
targetPath 	The target path to copy the module to.Addition folders will be created depending on whether 'useVersionPathing' is used or not.
useVersionPathing 	Whether to add a '/targetModuleId/versionId' folder to the target path or not.This allows copying multiple versions of the same module Id.
Returns:
The new module definition file if copy was successful or NULL if not.
virtual string ModuleManager::findModule	(	(string pModuleId="", int pVersionId=0) 		 ) 	[virtual]
Find the specific module Id optionally at the specified version Id.

Parameters:
moduleId 	The module Id to find.
versionId 	The version Id to find.
Returns:
The module definition object or NULL if not found.
virtual string ModuleManager::findModules	(	(bool loadedOnly=false) 		 ) 	[virtual]
Find all the modules registered with the specified loaded state.

Parameters:
loadedOnly 	Whether to return only modules that are loaded or not.
Returns:
A list of space - separated module definition object Ids.
virtual string ModuleManager::findModuleTypes	(	(string pModuleType="", bool loadedOnly=false) 		 ) 	[virtual]
Find the modules registered with the specified module type.

Parameters:
moduleType 	The module type to search for.
loadedOnly 	Whether to return only modules that are loaded or not.
Returns:
A list of space - separated module definition object Ids.
virtual bool ModuleManager::isModuleMergeAvailable	(	() 		 ) 	[virtual]
Checks whether a module merge definition file is available or not.

Returns:
Whether a module merge definition file is available or not.
virtual bool ModuleManager::loadExplicit	(	(string pModuleId="", int pVersionId=-1) 		 ) 	[virtual]
Load the specified module explicitly.

Parameters:
moduleId 	The module Id to load.
versionId 	The version Id to load.Optional: Will load the latest version.
Returns:
Whether the module Id was loaded or not.
virtual bool ModuleManager::loadGroup	(	(string pModuleGroup="") 		 ) 	[virtual]
Load the specified module group.

Parameters:
moduleGroup 	The module group to load.
Returns:
Whether the module group was loaded or not.
virtual bool ModuleManager::mergeModules	(	(string pMergeTargetPath="", bool removeMergeDefinition=false, bool registerNewModules=false) 		 ) 	[virtual]
Performs a module merge into the selected target path.

Parameters:
mergeTargetPath 	The path where modules will be merged into.
removeMergeDefinition 	Whether to remove any merge definition found or not if merge is successful.
registerNewModules 	Whether new (not replaced or updated) modules should be registered or not.
Returns:
Whether the module merge was successful or not.Failure here could result in a corrupt module state.Reinstall is recommended or at least advised to the user is recommended.
virtual void ModuleManager::removeListener	(	(string listenerObject="") 		 ) 	[virtual]
Unregisters the specified object as a listener for module notifications.

Parameters:
listenerObject 	The object to stop receiving module notifications.
Returns:
No return value.
virtual bool ModuleManager::scanModules	(	(string pRootPath="", bool rootOnly=false) 		 ) 	[virtual]
Scans for modules which are sub-directories of the specified path.

Parameters:
moduleRootPath 	The root directory to scan for sub - directories containing modules.
rootOnly[Optional] 	- Specifies whether to only scan the root path or not when searching for modules.
Returns:
Whether the scan was successful or not.A successful scan can still find zero modules.
virtual bool ModuleManager::setModuleExtension	(	(string moduleExtension="") 		 ) 	[virtual]
Set the module extension used to scan for modules. The default is 'module'.

Parameters:
moduleExtension 	The module extension used to scan for modules.Do not use a period character.
Returns:
Whether setting the module extension was successful or not.
virtual bool ModuleManager::synchronizeDependencies	(	(string rootModuleDefinition="", string pTargetDependencyFolder="") 		 ) 	[virtual]
Synchronize the module dependencies of a module definition to a target dependency folder.

Parameters:
rootModuleDefinition 	The module definition used to determine dependencies.
targetDependencyPath 	The target dependency folder to copy dependencies to.
Returns:
Whether the module dependencies were synchronized correctly or not.
virtual bool ModuleManager::unloadExplicit	(	(string pModuleId="") 		 ) 	[virtual]
Unload the specified module explicitly.

Parameters:
moduleId 	The module Id to unload.
Returns:
Whether the module Id was unloaded or not.
virtual bool ModuleManager::unloadGroup	(	(string pModuleGroup="") 		 ) 	[virtual]
Unload the specified module group.

Parameters:
moduleGroup 	The module group to unload.
Returns:
Whether the module group was unloaded or not.
virtual bool ModuleManager::unregisterModule	(	(string pModuleId="", bool versionId=false) 		 ) 	[virtual]
Unregister the specified module.

Parameters:
moduleId 	The module Id to unregister.
versionId 	The version Id to unregister.
Returns:
Whether the module was unregister or not.
Member Data Documentation

bool ModuleManager::EchoInfo
Whether the module manager echos extra information to the console or not.

bool ModuleManager::EnforceDependencies
Whether the module manager enforces any dependencies on module definitions it discovers or not.

//==============================================================================
// ModuleManager References
//==============================================================================
/*

*/
