//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$HelpLab["SEP_InspectorMode","Title"] = "Scene Editor Inspector Settings";
$HelpLab["SEP_InspectorMode","Text"] = "The inspector is quite slow when multiple objects are selected so it's recommended to only use it when needed. To not inspect selected objects, you can simply switch to another tab like \"Builder\"" SPC
                                       "or uncheck \"auto\" options which will disable automatic inspecting of selected objects. When auto is off, you can still manually inspect selection by clicking the \"Do Inspect\" button.";

$HelpLab["SEP_EditorMode","Title"] = "Scene Editor Inspector vs Builder";
$HelpLab["SEP_EditorMode","Text"] = "The scene editor builder mode allow fast object manipulation as the selected object are not inspected which cause slow selection when many objects are selected. The builder mode" SPC
                                    "is always activated when the inspector tab is not open. By unchecking the \"auto\" checkbox in inspector tab, it will work like builder mode and you can inspect object manually by clicking \"inspect\" button";
