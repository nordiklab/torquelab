//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function WorldEditor::syncGui(%this)
{
	if (!EditorGui.isAwake())
		return;

	%this.syncToolPalette();

	Editor.getUndoManager().updateUndoMenu();
	EditorGuiStatusBar.setSelectionObjectsByCount(%this.getSelectionSize());
	//SceneTreeWindow-->LockSelection.setStateOn( %this.getSelectionLockCount() > 0 );
	SceneEditorToolbar-->boundingBoxCollision.setStateOn(EWorldEditor.boundingBoxCollision);

	if (EWorldEditor.objectsUseBoxCenter)
	{
		SceneEditorToolbar-->centerObject.iconBitmap ="tlab/art/icons/set01/oldtoolbar/SelObjectCenter";
	}
	else
	{
		SceneEditorToolbar-->centerObject.iconBitmap ="tlab/art/icons/set01/oldtoolbar/SelObjectBounds";
	}

	if (GlobalGizmoProfile.getFieldValue(alignment) $= "Object")
	{
		SceneEditorToolbar-->objectTransform.iconBitmap ="tlab/art/icons/set01/toolbar/transform/objecttransform.png";
	}
	else
	{
		SceneEditorToolbar-->objectTransform.iconBitmap ="tlab/art/icons/set01/toolbar/transform/worldtransform";
	}

	%gridSnap = %this.getGridSnap();
	%softSnap = %this.getSoftSnap();
	LabToolbarStack-->renderHandleBtn.setStateOn(EWorldEditor.renderObjHandle);
	LabToolbarStack-->renderTextBtn.setStateOn(EWorldEditor.renderObjText);
	LabToolbarStack-->ToggleSnapObject.setStateOn(%softSnap);
	LabToolbarStack-->ToggleSnapGrid.setStateOn(%gridSnap);

	//SceneEditorToolbar-->softSnapSizeTextEdit.setText( EWorldEditor.getSoftSnapSize() );
	if (isObject(ESnapOptions))
	{
		ESnapOptions-->SnapSize.setText(EWorldEditor.getSoftSnapSize());
		ESnapOptions-->GridSize.setText(EWorldEditor.getGridSize());
		ESnapOptions-->GridSnapButton.setStateOn(%gridSnap);
		ESnapOptions-->NoSnapButton.setStateOn(!%this.stickToGround && !%softSnap && !%gridSnap);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
function EWorldEditor::syncToolPalette(%this)
{
	switch$(GlobalGizmoProfile.mode)
	{
		case "None":
			EWorldEditorNoneModeBtn.performClick();

		case "Move":
			EWorldEditorMoveModeBtn.performClick();

		case "Rotate":
			EWorldEditorRotateModeBtn.performClick();

		case "Scale":
			EWorldEditorScaleModeBtn.performClick();
	}
}
//------------------------------------------------------------------------------

