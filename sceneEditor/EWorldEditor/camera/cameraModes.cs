//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$LabDefaultCameraView = "Standard Camera";

$LabCameraDisplayName[0] = "Top View";
$LabCameraDisplayName[1] = "Bottom View";
$LabCameraDisplayName[2] = "Front View";
$LabCameraDisplayName[3] = "Back View";
$LabCameraDisplayName[4] = "Left View";
$LabCameraDisplayName[5] = "Right View";
$LabCameraDisplayName[6] = "Standard Camera";
$LabCameraDisplayName[7] = "Isometric View";
$LabCameraDisplayName[8] = "1st Person Camera";
$LabCameraDisplayName[9] = "3rd Person Camera";
$LabCameraDisplayName[10] = "Orbit Camera";
$LabCameraDisplayName[11] = "Smooth Camera";
$LabCameraDisplayName[12] = "Smooth Rot Camera";

$LabCameraDisplayType["Top View"] = $EditTsCtrl::DisplayTypeTop;
$LabCameraDisplayType["Bottom View"] = $EditTsCtrl::DisplayTypeBottom;
$LabCameraDisplayType["Left View"] = $EditTsCtrl::DisplayTypeLeft;
$LabCameraDisplayType["Right View"] = $EditTsCtrl::DisplayTypeRight;
$LabCameraDisplayType["Front View"] = $EditTsCtrl::DisplayTypeFront;
$LabCameraDisplayType["Back View"] = $EditTsCtrl::DisplayTypeBack;
$LabCameraDisplayType["Isometric View"] = $EditTsCtrl::DisplayTypeIsometric;
$LabCameraDisplayType["Standard Camera"] = $EditTsCtrl::DisplayTypePerspective;
$LabCameraDisplayType["1st Person Camera"] = $EditTsCtrl::DisplayTypePerspective;
$LabCameraDisplayType["3rd Person Camera"] = $EditTsCtrl::DisplayTypePerspective;
$LabCameraDisplayType["Orbit Camera"] = $EditTsCtrl::DisplayTypePerspective;
$LabCameraDisplayType["Smooth Camera"] = $EditTsCtrl::DisplayTypePerspective;
$LabCameraDisplayType["Smooth Rot Camera"] = $EditTsCtrl::DisplayTypePerspective;

$LabCameraDisplayMode["Top View"] = "Standard";
$LabCameraDisplayMode["Bottom View"] ="Standard";
$LabCameraDisplayMode["Left View"] = "Standard";
$LabCameraDisplayMode["Right View"] = "Standard";
$LabCameraDisplayMode["Front View"] ="Standard";
$LabCameraDisplayMode["Back View"] = "Standard";
$LabCameraDisplayMode["Isometric View"] = "Standard";
$LabCameraDisplayMode["Standard Camera"] = "Standard";
$LabCameraDisplayMode["1st Person Camera"] = "Player";
$LabCameraDisplayMode["3rd Person Camera"] = "PlayerThird";
$LabCameraDisplayMode["Orbit Camera"] = "Orbit";
$LabCameraDisplayMode["Smooth Camera"] = "Newton";
$LabCameraDisplayMode["Smooth Rot Camera"] = "NewtonDamped";

$LabCameraViewMode["Top View"] = "Top";
$LabCameraViewMode["Bottom View"] ="Bottom";
$LabCameraViewMode["Left View"] = "Left";
$LabCameraViewMode["Right View"] = "Right";
$LabCameraViewMode["Front View"] ="Front";
$LabCameraViewMode["Back View"] = "Back";
$LabCameraViewMode["Isometric View"] = "Isometric";
$LabCameraViewMode["Standard Camera"] = "Standard";
$LabCameraViewMode["1st Person Camera"] = "FirstPerson";
$LabCameraViewMode["3rd Person Camera"] = "ThirdPerson";
$LabCameraViewMode["Orbit Camera"] = "Orbit";
$LabCameraViewMode["Smooth Camera"] = "Newton";
$LabCameraViewMode["Smooth Rot Camera"] = "NewtonDamped";

$LabCameraDisplayType["Top"] = $EditTsCtrl::DisplayTypeTop;
$LabCameraDisplayType["Bottom"] = $EditTsCtrl::DisplayTypeBottom;
$LabCameraDisplayType["Left"] = $EditTsCtrl::DisplayTypeLeft;
$LabCameraDisplayType["Right"] = $EditTsCtrl::DisplayTypeRight;
$LabCameraDisplayType["Front"] = $EditTsCtrl::DisplayTypeFront;
$LabCameraDisplayType["Back"] = $EditTsCtrl::DisplayTypeBack;
$LabCameraDisplayType["Isometric"] = $EditTsCtrl::DisplayTypeIsometric;
$LabCameraDisplayType["Standard"] = $EditTsCtrl::DisplayTypePerspective;
$LabCameraDisplayType["FirstPerson"] = $EditTsCtrl::DisplayTypePerspective;
$LabCameraDisplayType["ThirdPerson"] = $EditTsCtrl::DisplayTypePerspective;
$LabCameraDisplayType["Orbit"] = $EditTsCtrl::DisplayTypePerspective;
$LabCameraDisplayType["Newton"] = $EditTsCtrl::DisplayTypePerspective;
$LabCameraDisplayType["NewtonDamped"] = $EditTsCtrl::DisplayTypePerspective;

$LabCameraDisplayMode["Top"] = "Standard";
$LabCameraDisplayMode["Bottom"] ="Standard";
$LabCameraDisplayMode["Left"] = "Standard";
$LabCameraDisplayMode["Right"] = "Standard";
$LabCameraDisplayMode["Front"] ="Standard";
$LabCameraDisplayMode["Back"] = "Standard";
$LabCameraDisplayMode["Isometric"] = "Standard";
$LabCameraDisplayMode["Standard"] = "Standard";
$LabCameraDisplayMode["FirstPerson"] = "Player";
$LabCameraDisplayMode["ThirdPerson"] = "PlayerThird";
$LabCameraDisplayMode["Orbit"] = "Orbit";
$LabCameraDisplayMode["Newton"] = "Newton";
$LabCameraDisplayMode["NewtonDamped"] = "NewtonDamped";

$LabCameraDisplayName["Top"] = "Top View";
$LabCameraDisplayName["Bottom"] = "Bottom View";
$LabCameraDisplayName["Front"] = "Front View";
$LabCameraDisplayName["Back"] = "Back View";
$LabCameraDisplayName["Left"] = "Left View";
$LabCameraDisplayName["Right"] = "Right View";
$LabCameraDisplayName["Standard"] = "Standard Camera";
$LabCameraDisplayName["Isometric"] = "Isometric View";
$LabCameraDisplayName["FirstPerson"] = "1st Person Camera";
$LabCameraDisplayName["ThirdPerson"] = "3rd Person Camera";
$LabCameraDisplayName["Orbit"] = "Orbit Camera";
$LabCameraDisplayName["Newton"] = "Smooth Camera";
$LabCameraDisplayName["NewtonDamped"] = "Smooth Rot Camera";
//==============================================================================
// New set camera mode function based on short names
function Lab::setCameraView(%this, %mode)
{
	if( %mode $="")
		%mode = "Standard";

	Lab.SetEditorCameraView($LabCameraDisplayMode[%mode]);
	Lab.setEditorDisplayType($LabCameraDisplayType[%mode]);

	Lab.currentCameraView = %mode;
	Lab.currentCameraMode = $LabCameraDisplayMode[%mode];
	Lab.currentCameraType = $LabCameraDisplayType[%mode];
	Lab.currentCameraDisplay = $LabCameraDisplayName[%mode];

	Lab.syncCameraGui();
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::setCameraViewMode(%this, %mode)
{
	%shortMode = $LabCameraViewMode[%mode];
	Lab.setCameraView(%shortMode);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::SetEditorCameraView(%this,%type)
{
	%client = LocalClientConnection;

	switch$(%type)
	{
		case "Standard":
			// Switch to Fly Mode
			%client.camera.setFlyMode();
			%client.camera.newtonMode = "0";
			%client.camera.newtonRotation = "0";
			%client.camera.setVelocity("0 0 0");
			%client.setControlObject(%client.camera);

		case "Newton":
			// Switch to Newton Fly Mode without rotation damping
			%client.camera.setFlyMode();
			%client.camera.newtonMode = "1";
			%client.camera.newtonRotation = "0";
			%client.camera.setVelocity("0 0 0");
			%client.setControlObject(%client.camera);

		case "NewtonDamped":
			// Switch to Newton Fly Mode with damped rotation
			%client.camera.setFlyMode();
			%client.camera.newtonMode = "1";
			%client.camera.newtonRotation = "1";
			%client.camera.setAngularVelocity("0 0 0");
			%client.setControlObject(%client.camera);

		case "Orbit":
			LocalClientConnection.camera.setEditOrbitMode();
			%client.setControlObject(%client.camera);
			%noFly = true;

		case "Player":
			%client.player.setVelocity("0 0 0");
			LocalClientConnection.setControlObject(LocalClientConnection.player);
			ServerConnection.setFirstPerson(1);
			$isFirstPersonVar = 1;

		case "PlayerThird":
			%client.player.setVelocity("0 0 0");
			%client.setControlObject(%client.player);
			ServerConnection.setFirstPerson(0);
			$isFirstPersonVar = 0;
	}

	//Was wrong as %type is the mode name and $EditTSCtrl::DisplayTypePerspective = 6
	if(%type $= "Standard")//!= $EditTSCtrl::DisplayTypePerspective)
		Lab.setFlyCameraData();
}
//------------------------------------------------------------------------------

//==============================================================================
// Set the current camera type info in different editor areas
function Lab::setEditorDisplayType(%this, %type)
{
	%gui = %this.currentEditor.editorGui;

	if(!isObject(%gui))
		return;

	if($LabCameraDisplayType[%type] !$="")
		%type = $LabCameraDisplayType[%type];

	// Store the current camera rotation so we can restore it correctly when
	// switching back to perspective view
	if(%gui.getDisplayType() == $EditTSCtrl::DisplayTypePerspective)
		%this.lastPerspectiveCamRotation = LocalClientConnection.camera.getRotation();

	%gui.setDisplayType(%type);

	if(%gui.getDisplayType() == $EditTSCtrl::DisplayTypePerspective)
		LocalClientConnection.camera.setRotation(%this.lastPerspectiveCamRotation);

	%this.cameraDisplayType = %type;
}
//------------------------------------------------------------------------------
//==============================================================================
// Sync the camera information on the editor guis
function Lab::setFlyCameraData(%this,%forced)
{

	switch(%displayType)
	{
		case $EditTSCtrl::DisplayTypeTop:
			%name = "Top View";
			%camRot = "0 0 0";

		case $EditTSCtrl::DisplayTypeBottom:
			%name = "Bottom View";
			%camRot = "3.14159 0 0";

		case $EditTSCtrl::DisplayTypeLeft:
			%name = "Left View";
			%camRot = "-1.571 0 1.571";

		case $EditTSCtrl::DisplayTypeRight:
			%name = "Right View";
			%camRot = "-1.571 0 -1.571";

		case $EditTSCtrl::DisplayTypeFront:
			%name = "Front View";
			%camRot = "-1.571 0 3.14159";

		case $EditTSCtrl::DisplayTypeBack:
			%name = "Back View";
			%camRot = "-1.571 0 0";

		case $EditTSCtrl::DisplayTypeIsometric:
			%name = "Isometric View";
			%camRot = "0 0 0";
	}

	LocalClientConnection.camera.controlMode = "Fly";
	LocalClientConnection.camera.setRotation(%camRot);

	return;
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::OrbitCameraChange(%this, %size, %center,%client)
{
	if(%client $= "")
		%client = LocalClientConnection;

	%camera = %client.camera;

	if(!isObject(%camera))
	{
		warnLog("No Camera found for OrbitCameraChange. Function aborted!");
		return;
	}

	if(%size > 0)
	{
		%camera.setValidEditOrbitPoint(true);
		%camera.setEditOrbitPoint(%center);
	}
	else
	{
		%camera.setValidEditOrbitPoint(false);
	}
}

//------------------------------------------------------------------------------
function Lab::CycleCameraFlyType(%this,%client)
{
	if(%client $= "")
		%client = LocalClientConnection;

	if(%client.camera.getMode() $= "Fly")
	{
		if(%client.camera.newtonMode == false)    // Fly Camera
		{
			// Switch to Newton Fly Mode without rotation damping
			%client.camera.newtonMode = "1";
			%client.camera.newtonRotation = "0";
			%client.camera.setVelocity("0 0 0");
		}
		else if(%client.camera.newtonRotation == false)      // Newton Camera without rotation damping
		{
			// Switch to Newton Fly Mode with damped rotation
			%client.camera.newtonMode = "1";
			%client.camera.newtonRotation = "1";
			%client.camera.setAngularVelocity("0 0 0");
		}
		else     // Newton Camera with rotation damping
		{
			// Switch to Fly Mode
			%client.camera.newtonMode = "0";
			%client.camera.newtonRotation = "0";
		}

		%client.setControlObject(%client.camera);
		Lab.syncCameraGui();
	}
}
/*
TODO Missing the menu update from stock
      //quick way select menu bar options
		Lab.checkFindItem("Camera",0,1,1);
		Lab.checkMenuItem("EditorPlayerCameraTypeOptions",0, 2, %flyModeRadioItem);
		Lab.checkMenuItem("EditorFreeCameraTypeOptions",0, 4, -1);
*/
