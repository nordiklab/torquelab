//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function Lab::fitCameraToSelection(%this,%orbit)
{
	if(%orbit)
	{
		Lab.setCameraView("Orbit");
	}

	//GuiShapeLabPreview have it's own function
	if (Lab.currentEditor.isMethod("fitCameraToSelection"))
	{
		Lab.currentEditor.fitCameraToSelection(%orbit);
		return;
	}

	Lab.setCameraAutoFit(EWorldEditor.getSelectionRadius()+1);
}
//------------------------------------------------------------------------------

function Lab::setCameraAutoFit(%this,%radius)
{
	LocalClientConnection.camera.autoFitRadius(%radius);
	LocalClientConnection.setControlObject(LocalClientConnection.camera);
}
//==============================================================================

//==============================================================================
function Lab::DropPlayerAtCamera(%this)
{
	%client = LocalClientConnection;

	// If the player is mounted to something (like a vehicle) drop that at the
	// camera instead. The player will remain mounted.
	%obj = %client.player.getObjectMount();

	if(!isObject(%obj))
		%obj = %client.player;

	Lab.setControlPlayerMode(%client.camera.getTransform());
	//%obj.setTransform(%client.camera.getTransform());
	//%obj.setVelocity("0 0 0");
	%client.setControlObject(%client.player);
	Lab.syncCameraGui();
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::DropCameraAtPlayer(%this)
{
	%client = LocalClientConnection;

	if (%client.getControlObject() == %client.camera)
		return; //Already controlling a camera

	Lab.setControlCameraMode(%client.player.getEyeTransform());
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::dropCameraToSelection(%this)
{
	if(EWorldEditor.getSelectionSize() == 0)
		return;

	%pos = EWorldEditor.getSelectionCentroid();
	%cam = LocalClientConnection.camera.getTransform();
	// set the pnt
	%cam = setWord(%cam, 0, getWord(%pos, 0));
	%cam = setWord(%cam, 1, getWord(%pos, 1));
	%cam = setWord(%cam, 2, getWord(%pos, 2));
	LocalClientConnection.camera.setTransform(%cam);
}
//------------------------------------------------------------------------------

//==============================================================================
// Default T3D Game Editor Camera Server Commands
//==============================================================================

//==============================================================================
function Lab::TogglePathCamera(%this,%client, %val)
{
	if (!isObject( %client.PathCamera))
		return;

	if(%client.getControlObject() != %client.camera)
		%control = %client.camera;
	else
		%control = %client.PathCamera;

	%client.setControlObject(%control);
	clientCmdSyncEditorGui();
}
//------------------------------------------------------------------------------
