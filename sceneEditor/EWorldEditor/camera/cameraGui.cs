//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Sync the camera information on the editor guis
function Lab::syncCameraGui(%this,%forced)
{

	if(!EditorIsActive() /*|| !isObject(Lab.currentEditor.editorGui)*/)
		return;

	WEMenuViewMode.checkMenuItem(0,7,Lab.currentCameraType);

	%id = ELink.menuFindText("SceneCamModeMenu",Lab.currentCameraDisplay);

	if (%id != -1)
		ELink.menuSelect("SceneCamModeMenu",%id, false);


}
//------------------------------------------------------------------------------


//==============================================================================
function MenuCameraStatus::onWake(%this)
{
	%this.add("Standard Camera");
	%this.add("1st Person Camera");
	%this.add("3rd Person Camera");
	%this.add("Orbit Camera");
	%this.add("Top View");
	%this.add("Bottom View");
	%this.add("Left View");
	%this.add("Right View");
	%this.add("Front View");
	%this.add("Back View");
	%this.add("Isometric View");
	%this.add("Smooth Camera");
	%this.add("Smooth Rot Camera");
}
//------------------------------------------------------------------------------
//==============================================================================
function MenuCameraStatus::onSelect(%this, %id, %text)
{
	Lab.setCameraViewMode(%text);
}
//------------------------------------------------------------------------------
