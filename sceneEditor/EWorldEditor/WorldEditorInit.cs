//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function WorldEditor::init(%this)
{
	EWorldEditor.setDisplayType($EditTsCtrl::DisplayTypePerspective);

	%this.isDirty = false;
	// add objclasses which we do not want to collide with
	%this.addClassIgnore(Sky);
	// Ignore Replicated fxStatic Instances.
	%this.addClassIgnore("fxShapeReplicatedStatic");

	// Make sure we have an active selection set.
	if (!%this.getActiveSelection())
		%this.setActiveSelection(new WorldEditorSelection(EWorldEditorSelection));

	/*
		// context menu
		if (!isObject(WEContextPopupDlg))
			new GuiControl(WEContextPopupDlg, EditorGuiGroup)
		{
			profile = "ToolsDefaultProfile_NoModal";
			horizSizing = "width";
			vertSizing = "height";
			position = "0 0";
			extent = "640 480";
			minExtent = "8 8";
			visible = "1";
			setFirstResponder = "0";
			modal = "1";
			new GuiPopUpMenuCtrl(WEContextPopup)
			{
				profile = "ToolsScrollProfile";
				position = "0 0";
				extent = "0 0";
				minExtent = "0 0";
				maxPopupHeight = "200";
				command = "canvas.popDialog(WEContextPopupDlg);";
			};
		};
		WEContextPopup.setVisible(false);
	*/

}

//------------------------------------------------------------------------------

