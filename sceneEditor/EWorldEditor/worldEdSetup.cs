//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function WorldEditor::onAdd(%this)
{
	EWorldEditor.dropType = "atCentroid";
}
//------------------------------------------------------------------------------

//==============================================================================
function EWorldEditor::onGuiUpdate(%this, %text)
{
}
//------------------------------------------------------------------------------

//==============================================================================
function WorldEditor::onWorldEditorUndo(%this)
{
	Scene.doRefreshInspect();
}
//------------------------------------------------------------------------------
//==============================================================================
// Manage WorldEditor Ignored CLasses - Allow to add/remove classes ignored
//------------------------------------------------------------------------------
//ConsoleMethod( WorldEditor, ignoreObjClass, void, 3, 0, "(string class_name, ...)")
//DefineEngineMethod( WorldEditor, clearIgnoreList, void, () "Clear the ignore class list.\n")
//==============================================================================

//EWorldEditor.addClassIgnore(Sky);
//EWorldEditor.addClassIgnore("fxShapeReplicatedStatic");
//EWorldEditor.addClassIgnore("PointLight");
//EWorldEditor.removeClassIgnore("PointLight");
//==============================================================================
function WorldEditor::addClassIgnore(%this,%class)
{
	Lab.worldEdIgnoreClasses = strAddWord(Lab.worldEdIgnoreClasses,%class,1);
	%this.ignoreObjClass(%class);
}
//------------------------------------------------------------------------------

//==============================================================================
function WorldEditor::removeClassIgnore(%this,%class)
{
	%newList = strRemoveWord(Lab.worldEdIgnoreClasses,%class);

	if (%newList $= Lab.worldEdIgnoreClasses)
		return;

	Lab.worldEdIgnoreClasses = %newList;
	%this.clearIgnoreList();

	foreach$(%class in Lab.worldEdIgnoreClasses)
		%this.ignoreObjClass(%class);

	info("WorldEditor updated ignore list:",Lab.worldEdIgnoreClasses);
}
//------------------------------------------------------------------------------
