//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$SceneEd_TreePage = 0;
$SceneEd_TreeMode = "Scene";
function SceneEditorTreeTabBook::init( %this )
{
	%this.selectPage($SceneEd_TreePage);
}

//==============================================================================
function SceneEditorTreeTabBook::onTabSelected( %this,%text, %index )
{
	$SceneEd_TreePage = %index;
	SceneTreeWindow-->DeleteSelection.visible = false;
	SceneTreeWindow-->LockSelection.visible = false;
	SceneTreeWindow-->AddSimGroup.visible = false;
	%pageInt = %this.getObject(%index).internalName;
	$SceneEd_TreeMode = %pageInt;
	//SceneEditorUtilityBook-->SaveCurrentDatablock.visible = ($SceneEd_TreeMode $= "datablock") ? 1 : 0;
	//SceneEditorUtilityBook-->SaveCurrentDatablock.parentGroup.updateStack();

	if (%this.getObject(%index).isMethod("onPageSelected"))
		%this.getObject(%index).onPageSelected(%this,%text,%index);
}
//------------------------------------------------------------------------------

