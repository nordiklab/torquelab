//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function SceneEditorTree::onSceneChanged( %this,%data )
{
	%this.rebuild();
}
//------------------------------------------------------------------------------
//SceneEditorTree.showInternalNames = 0;
//SceneEditorTree.showObjectNames = 1;
//SceneEditorTree.showClassNames = 0;

//==============================================================================
function SceneEditorTree::rebuild( %this )
{
	%this.clear();
	%this.open(MissionGroup);
	%this.buildVisibleTree();
}
//------------------------------------------------------------------------------

//==============================================================================
function SceneEditorTree::toggleLock( %this )
{
	if (  SceneTreeWindow-->LockSelection.command $= "EWorldEditor.lockSelection(true); SceneEditorTree.toggleLock();" )
	{
		SceneTreeWindow-->LockSelection.command = "EWorldEditor.lockSelection(false); SceneEditorTree.toggleLock();";
		SceneTreeWindow-->DeleteSelection.command = "";
	}
	else
	{
		SceneTreeWindow-->LockSelection.command = "EWorldEditor.lockSelection(true); SceneEditorTree.toggleLock();";
		SceneTreeWindow-->DeleteSelection.command = "EditorMenuEditDelete();";
	}
}
//------------------------------------------------------------------------------

