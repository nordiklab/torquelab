//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$SceneEd_ModePage = 0;
$SceneEd_TreePage = 0;
function SceneEditorModeTab::onTabSelected( %this,%text,%index )
{
	$SceneEd_ModePage = %index;
}

function SceneEditorPlugin::toggleBuildMode( %this )
{
	%rowCount = getWordCount(SceneEditorTools.rows);

	if (%rowCount > 1)
	{
		SceneEditorTools.lastRows = SceneEditorTools.rows;
		SceneEditorTools.rows = "0";
		SceneEditorTools.updateSizes();
	}
	else
	{
		if (getWordCount(SceneEditorTools.lastRows) <= 1)
			SceneEditorTools.lastRows = "0 200";

		SceneEditorTools.rows = SceneEditorTools.lastRows;
		SceneEditorTools.updateSizes();
	}
}

