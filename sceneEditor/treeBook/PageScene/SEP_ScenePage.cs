//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function SEP_ScenePage::onPageSelected(%this,%book,%text,%id)
{
	SceneTreeWindow-->DeleteSelection.visible = true;
	SceneTreeWindow-->LockSelection.visible = true;
	SceneTreeWindow-->AddSimGroup.visible = true;

	%this.checkObject();
}
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function SEP_ScenePage::checkObject( %this,%object )
{
	if (%object $= "")
	{
		if (EWorldEditor.getSelectionSize() <= 0)
			return;

		%object = EWorldEditor.getSelectedObject(0);
	}

	if (isObject(%object))
		Scene.doInspect(%object);
}
