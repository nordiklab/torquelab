//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

/*
function SEP_GroupPage::onVisible( %this,%isVisible ) {
	if (!%isVisible)
		return;

	%this.updateContent();
}

function SEP_GroupPage::updateContent( %this ) {
	SceneObjectGroupSet.clear();
	%list = Lab.getMissionObjectClassList("Prefab");

	if (%list !$= "") {
		%prefabGroup = newSimSet("",SceneObjectGroupSet,"Prefabs");
	}

	foreach$(%obj in %list) {
		%obj.internalName = fileBase(%obj.filename);
		%prefabGroup.add(%obj);
	}

	if (LabSceneObjectGroups.getCount() > 0)
		%groupGroup = newSimSet("",SceneObjectGroupSet,"Group");

	foreach(%obj in LabSceneObjectGroups)
		%groupGroup.add(%obj);

	SEP_GroupTree.open(SceneObjectGroupSet);
}
*/
