//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
/*
$SEP_AutoGroupActiveSystem = true;
//==============================================================================
function SceneEd::setActiveSimGroup( %this, %group ) {
	return;

}
//------------------------------------------------------------------------------
//==============================================================================
function SceneEd::getActiveSimGroup( %this) {
	return;

	if (!isObject(%this.activeSimGroup))
		%this.setActiveSimGroup(MissionGroup);
	else if (!%this.activeSimGroup.isMemberOfClass(SimGroup))
		%this.setActiveSimGroup(MissionGroup);

	return %this.activeSimGroup;
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneEd::getActiveSimGroup( %this ) {
	return;
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneEd::setActiveSimGroup( %this, %group ) {
	return;

}
//------------------------------------------------------------------------------
*/
