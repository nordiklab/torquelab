//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function SEP_ScenePage::init( %this )
{
	SEP_SceneTreeRenameMenu.clear();
	SEP_SceneTreeRenameMenu.add("Object name",0);
	SEP_SceneTreeRenameMenu.add("Internal name",1);
	%selected = SceneEditorTree.renameInternal;

	if (%selected $= "")
		%selected = "0";

	SEP_SceneTreeRenameMenu.setSelected(%selected);
}
//------------------------------------------------------------------------------

//==============================================================================
function SEP_SceneTreeRenameMenu::onSelect( %this,%id,%text )
{
	SEP_ScenePage.setRenameMode(%id);
}
//------------------------------------------------------------------------------
//==============================================================================
function SEP_ScenePage::setRenameMode( %this,%modeId )
{
	if (%modeId $= "0")
		SceneEditorTree.renameInternal = false;
	else
		SceneEditorTree.renameInternal = true;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//==============================================================================
function SEP_ScenePage::updateContent( %this )
{
}
//------------------------------------------------------------------------------
