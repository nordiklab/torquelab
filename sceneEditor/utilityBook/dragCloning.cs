//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
//FONTS -> Change the font to all profile or only those specified in the list
function SceneEd::doClone( %this )
{
	%copyCount = SETools_CloneCount.getValue();

	if (%copyCount > 100)%copyCount = 100;

	if (%copyCount <= 0) return;

	Lab.copyDraggedSelection(Scene.lastDragOffset,%copyCount);
	//ETools.hideTool(CloneDrag);
	//EditorMap.push();
	//%this.setVisible(false);
}
//------------------------------------------------------------------------------
