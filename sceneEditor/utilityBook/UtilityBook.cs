//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$SceneEd_UtilityPage = 0;
function SceneEditorUtilityBook::init( %this )
{
	%this.selectPage($SceneEd_UtilityPage);
}

function SceneEditorUtilityBook::onTabSelected( %this,%text,%index )
{
	$SceneEd_UtilityPage = %index;
	%pageInt = %this.getObject(%index).internalName;
	$SceneEd_UtilityMode = %pageInt;
	$SceneInspectorActive = false;

	if (%pageInt $= "Inspector")
	{
		$SceneInspectorActive = true;

		//%currentInspect = SceneInspector.getInspectObject();
		//if (isObject(%currentInspect) || !$Cfg_Common_Objects_autoInspect)
		if (!$Cfg_Common_Objects_autoInspect)
		{
			return;
		}

		if ($SceneEd_TreeMode $= "Datablock")
		{
			%dataItem = SceneDatablockTree.getSelectedItemList();
			%item = getWord(%dataItem,0);
			%db =   SceneDatablockTree.getItemValue(%item);

			if (isObject(%db))
			{
				Scene.doInspect(%db);
				return;
			}

			SEP_DatablockPage.checkObjectDatablock();

		}
		else
		{
			if (EWorldEditor.getSelectionSize() <= 0)
				return;

			%object = EWorldEditor.getSelectedObject(0);
			Scene.doInspect(%object);
		}

	}
}

//==============================================================================
// Prefabs
//==============================================================================

