//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Hack to force LevelInfo update after Cubemap change...
//==============================================================================

//==============================================================================
function SceneEd::setInspectorDataMode( %this,%mode )
{
	SceneInspector.dataMode = %mode;
	SceneEdInspectHeader.callOnChildrenNoRecurse("setVisible","false");

	if(isObject(SceneEdInspectHeader.findChild(%mode,1)))
		SceneEdInspectHeader.findChild(%mode,1).visible = true;

	if (%mode $= "Datablock")
		%this.setInspectorDatablockMode();
	else
		%this.setInspectorObjectMode();
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneEd::setInspectorDatablockMode( %this,%mode )
{
	//SceneEditorUtilityBook-->InspectorIcons_Datablock.visible = true;
	SceneEditorUtilityBook-->InspectorIcons_Datablock.visible = true;
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneEd::setInspectorObjectMode( %this,%mode )
{
	SceneEditorUtilityBook-->InspectorIcons_Datablock.visible = false;
}
//------------------------------------------------------------------------------
