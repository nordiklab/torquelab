//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function SceneEditorTools::onPreEditorSave( %this )
{
	%this.removeToolClones();
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneEditorTools::onPostEditorSave( %this )
{
	%this.getToolClones();
}
//------------------------------------------------------------------------------
//==============================================================================

function SceneEditorTools::removeToolClones( %this )
{
	delObj(SEP_TransformRollout-->toolsStack);
	delObj(SceneEditorTools-->cloneTools);
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneEditorTools::getToolClones( %this )
{
	SceneEditorTools.removeToolClones();

	if (isObject(ETransformTool))
		ETransformTool.cloneToCtrl(SEP_TransformRollout);

	//ECloneTool.cloneToCtrl(SEP_CloneToolsStack);
	//foreach(%gui in SEP_TransformStack){
	//%gui.expanded = false;
	//}
}
//------------------------------------------------------------------------------
function SceneEditorTools::checkToolClones( %this )
{
	if (!isObject(SEP_TransformRollout-->toolsStack))
		%this.getToolClones();

}
//------------------------------------------------------------------------------
//==============================================================================
//==============================================================================
// SceneEditorTools Frame Set Scripts
//==============================================================================

function SceneEditorTools::setMeshRootFolder( %this,%folder )
{
	%this.meshRootFolder = %folder;
	%this-->meshRootFolder.setText(%folder);
}
//------------------------------------------------------------------------------
//==============================================================================
// SceneEditorTools.validateMeshRootFolder($ThisControl);
function SceneEditorTools::validateMeshRootFolder( %this,%ctrl )
{
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneEditorTools::getMeshRootFolder( %this )
{
	getFolderName("","SceneEditorTools.setMeshRootFolder","art/");
}
//------------------------------------------------------------------------------
function SceneEditorTools::onObjectAdded( %this,%obj )
{
}
function SceneEditorTools::onObjectRemoved( %this,%obj )
{
}
