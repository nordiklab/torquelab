//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function SceneEditorPlugin::initToolBar( %this )
{
	%menu = SceneEditorToolbar-->DropTypeMenu;
	%menu.clear();
	%selected = 0;

	foreach$(%type in $Cfg_TLab_Object_DropTypes)
	{
		%menu.add(%type,%id++);

		if (EWorldEditor.dropType $= %type)
			%selected = %id;
	}

	%menu.setSelected(%selected,false);
}

//==============================================================================
function Lab::toggleObjectCenter( %this,%updateOnly )
{
	if (!%updateOnly)
		EWorldEditor.objectsUseBoxCenter = !EWorldEditor.objectsUseBoxCenter;

	if ( EWorldEditor.objectsUseBoxCenter )
	{
		SceneEditorToolbar-->centerObject.setBitmap("tlab/art/icons/set01/oldtoolbar/SelObjectCenter.png");
	}
	else
	{
		SceneEditorToolbar-->centerObject.setBitmap("tlab/art/icons/set01/oldtoolbar/SelObjectBounds.png");
	}
}
//------------------------------------------------------------------------------

