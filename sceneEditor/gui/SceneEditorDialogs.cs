//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
function SceneEditorDialogs::toggleDlg( %this,%dlgInt )
{
	Parent::toggleDlg(%this,%dlgInt);

	%ctrl = SceneEditorDialogs.findObjectByInternalName(%dlgInt@"Toggle",true);

	if (!isObject(%ctrl))
		return;

	eval("%visible = "@%ctrl.variable@";");
	%ctrl.setStateOn(%visible);
}
//------------------------------------------------------------------------------

//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
function SceneEditorDialogs::hideDlg( %this,%dlgInt )
{
	Parent::hideDlg(%this,%dlgInt);
	%ctrl = SceneEditorDialogs.findObjectByInternalName(%dlgInt@"Toggle",true);

	if (!isObject(%ctrl))
		return;

	eval("%visible = "@%ctrl.variable@";");
	%ctrl.setStateOn(%visible);

}
//------------------------------------------------------------------------------
//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
function SceneEditorDialogs::showDlg( %this,%dlgInt )
{
	Parent::showDlg(%this,%dlgInt);

	%ctrl = SceneEditorDialogs.findObjectByInternalName(%dlgInt@"Toggle",true);

	if (!isObject(%ctrl))
		return;

	eval("%visible = "@%ctrl.variable@";");
	%ctrl.setStateOn(%visible);
}
//------------------------------------------------------------------------------
//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
function SceneEditorDialogs::onActivated( %this )
{
	if (SceneEditorDialogs.selectedPage $= "")
		SceneEditorDialogs.selectedPage = "0";

}
//------------------------------------------------------------------------------

//==============================================================================
// Hack to force LevelInfo update after Cubemap change...
//==============================================================================
//==============================================================================
function SceneEditorDialogs::onPreEditorSave( %this )
{
	//Parent::onPreEditorSave( %this );
	%this.callOnChildrenNoRecurse("setVisible",0);
	//%c1 = SEP_ScatterSky_Custom-->StackA SPC SEP_ScatterSky_Custom-->StackB;
	//%c1 = %c1 SPC SEP_LegacySkyProperties-->StackA SPC SEP_LegacySkyProperties-->StackB;
	//%c1 = %c1 SPC SEP_CloudLayer-->StackA SPC SEP_CloudLayer-->StackB;

	//foreach$(%ctrl in %c1)
	//%ctrl.clear();

	//if (isObject(SEP_PostFXManager_Clone-->MainContainer))
	//EPostFxManager.moveFromGui();

	//SEP_GroundCoverLayerArray.clear();
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneEditorDialogs::onPostEditorSave( %this )
{
	Parent::onPostEditorSave( %this );
	//EPostFxManager.moveToGui(SEP_PostFXManager_Clone);
}
//------------------------------------------------------------------------------
