//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Initialize the WorldEditor GUI options and settings
function Lab::initSceneEditor(%this)
{
	Lab.initSceneSettings();
	Lab.initSceneElinks();

	EWorldEditor.init();

	Lab.syncCameraGui();
}

//==============================================================================
// Initialize the WorldEditor GUI options and settings
function Lab::initSceneSettings(%this)
{
	//Most are in ElinkedSettings now
}

