//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Scene Editor Params - Used set default settings and build plugins options GUI
//==============================================================================
//==============================================================================
// Prepare the default config array for the Scene Editor Plugin
function SceneEditorPlugin::initParamsArray( %this,%array )
{
	$SceneEdCfg = newScriptObject("SceneEditorCfg");
	%array.group[%groupId++] = "Objects and Prefabs settings";
	%array.setVal("DropLocation",       "10" TAB "Drop object location" TAB "Dropdown"  TAB "itemList>>$Cfg_TLab_Object_DropTypes" TAB "SceneEditorCfg" TAB %groupId);
	%array.setVal("AutoCreatePrefab",       "1" TAB "Create prefab automatically" TAB "Checkbox"  TAB "" TAB "$SceneEd::AutoCreatePrefab" TAB %groupId);
	$Cfg_TLab_PrefabAutoMode = "Level Object Folder";
	%array.setVal("AutoPrefabMode",       "1" TAB "Automatic prefab mode" TAB "DropDown"  TAB "itemList>>$Cfg_TLab_PrefabAutoMode" TAB "$SceneEd::AutoPrefabMode" TAB %groupId);
	%array.setVal("AutoPrefabFolder",       "art/models/prefabs/" TAB "Folder for auto prefab mode" TAB "TextEdit"  TAB "" TAB "$SceneEd::AutoPrefabFolder" TAB %groupId);
	%array.group[%groupId++] = "MissionGroup Organizer";
	%array.setVal("CoreGroup",       "mgCore" TAB "Core Objects Group" TAB "TextEdit"  TAB "" TAB "SceneEditorCfg" TAB %groupId);
	%array.setVal("SceneObjectsGroup",       "mgSceneObjects" TAB "Ambient Group" TAB "TextEdit"  TAB "" TAB "SceneEditorCfg" TAB %groupId);
	%array.setVal("EnvironmentGroup",       "mgEnvironment" TAB "Environment Group" TAB "TextEdit"  TAB "" TAB "SceneEditorCfg" TAB %groupId);
	%array.setVal("TSStaticGroup",       "mgMapModels" TAB "TSStatic Group" TAB "TextEdit"  TAB "" TAB "SceneEditorCfg" TAB %groupId);
	%array.setVal("SpawnGroup",       "PlayerDropPoints" TAB "Spawn Group" TAB "TextEdit"  TAB "" TAB "SceneEditorCfg" TAB %groupId);
	%array.setVal("Vehicle",       "Vehicle" TAB "Vehicle Group" TAB "TextEdit"  TAB "" TAB "SceneEditorCfg" TAB %groupId);
	%array.setVal("MiscObjectGroup",       "mgMiscObject" TAB "Misc. Objects Group" TAB "TextEdit"  TAB "" TAB "SceneEditorCfg" TAB %groupId);
	%array.setVal("ShapeGroup",       "mgShapeGroup" TAB "Shape Group" TAB "TextEdit"  TAB "" TAB "SceneEditorCfg" TAB %groupId);
	%array.setVal("NavAIGroup",       "NavAI" TAB "NavAI Group" TAB "TextEdit"  TAB "" TAB "SceneEditorCfg" TAB %groupId);
	%array.setVal("NavMeshGroup",       "NavMesh" TAB "NavMesh Group" TAB "TextEdit"  TAB "" TAB "SceneEditorCfg" TAB %groupId);
	%array.setVal("NavPathGroup",       "NavPath" TAB "NavPath Group" TAB "TextEdit"  TAB "" TAB "SceneEditorCfg" TAB %groupId);
	%array.setVal("Occluders",       "mgOccluders" TAB "Occluders Group" TAB "TextEdit"  TAB "" TAB "SceneEditorCfg" TAB %groupId);
	%array.setVal("LightsGroup",       "mgLights" TAB "Lights Group" TAB "TextEdit"  TAB "" TAB "SceneEditorCfg" TAB %groupId);
	%array.setVal("CoverPointGroup",       "CoverPoint" TAB "AI Cover Group" TAB "TextEdit"  TAB "" TAB "SceneEditorCfg" TAB %groupId);
	%array.group[%groupId++] = "Scene tree list options";
	%array.setVal("renameInternal",       "0" TAB "Rename internal name" TAB "Checkbox"  TAB "" TAB "SceneEditorTree" TAB %groupId);
	%array.setVal("showObjectNames",       "0" TAB "Show object name" TAB "Checkbox"  TAB "" TAB "SceneEditorTree" TAB %groupId);
	%array.setVal("showInternalNames",       "0" TAB "Show object internalName" TAB "Checkbox"  TAB "" TAB "SceneEditorTree" TAB %groupId);
	%array.setVal("showObjectIds",       "0" TAB "Show object IDs" TAB "Checkbox"  TAB "" TAB "SceneEditorTree" TAB %groupId);
	%array.setVal("showClassNames",       "0" TAB "Show object Class" TAB "Checkbox"  TAB "" TAB "SceneEditorTree" TAB %groupId);
	%array.group[%groupId++] = "Objects Creator Options";
	%array.setVal("IconWidth",       "120" TAB "icon width" TAB "SliderEdit"  TAB "range>>30 300;;tickAt>>1" TAB "SceneEditorCfg" TAB %groupId);
	%array.group[%groupId++] = "Special tools and managers";
	%array.setVal("GroundCoverDefaultMaterial",       "grass1" TAB "Shape Group" TAB "TextEdit"  TAB "" TAB "$SEP_GroundCoverDefault_Material" TAB %groupId);
	%array.setVal("AutoLight_ShowShapes",       "0" TAB "Show shapes in AutoLight manager" TAB "Checkbox" TAB "" TAB "" TAB %gid);
	%array.setVal("AutoLight_ShowLights",       "0" TAB "Show lights in AutoLight manager" TAB "Checkbox" TAB "" TAB "" TAB %gid);
	%array.setVal("excludeClientOnlyDatablocks",       "1" TAB "excludeClientOnlyDatablocks" TAB "Checkbox"  TAB "" TAB "SceneEd" TAB %groupId);
}
//------------------------------------------------------------------------------

//==============================================================================
// Plugin Object Callbacks - Called from TLab plugin management scripts
//==============================================================================

//==============================================================================
// Called when TorqueLab is launched for first time
function SceneEditorPlugin::onPluginLoaded( %this )
{
	//%map = new ActionMap();
	%map = %this.map;
	%map.bindCmd( keyboard, "1", "EWorldEditorNoneModeBtn.performClick();", "" );  // Select
	%map.bindCmd( keyboard, "2", "EWorldEditorMoveModeBtn.performClick();", "" );  // Move
	%map.bindCmd( keyboard, "3", "EWorldEditorRotateModeBtn.performClick();", "" );  // Rotate
	%map.bindCmd( keyboard, "4", "EWorldEditorScaleModeBtn.performClick();", "" );  // Scale
	%map.bindCmd( keyboard, "f", "Lab.fitCameraToSelection();", "" );// Fit Camera to Selection
	%map.bindCmd( keyboard, "z", "EditorGuiStatusBar.setCamera(\"Standard Camera\");", "" );// Free camera
	%map.bindCmd( keyboard, "n", "ToggleNodeBar->renderHandleBtn.performClick();", "" );// Render Node
	%map.bindCmd( keyboard, "shift n", "ToggleNodeBar->renderTextBtn.performClick();", "" );// Render Node Text
	%map.bindCmd( keyboard, "g", "ESnapOptions-->GridSnapButton.performClick();" ); // Grid Snappping
	%map.bindCmd( keyboard, "t", "SnapToBar->terrainSnapBtn.performClick();", "" );// Terrain Snapping
	%map.bindCmd( keyboard, "b", "SnapToBar-->ToggleSnapObject.performClick();" ); // Soft Snappping
	%map.bindCmd( keyboard, "v", "SceneEditorToolbar->ToggleBoundSelect.performClick();", "" );// Bounds Selection
	%map.bindCmd( keyboard, "o", "EToolbarObjectCenterDropdown->objectBoxBtn.performClick(); objectCenterDropdown.toggle();", "" );// Object Center
	%map.bindCmd( keyboard, "p", "EToolbarObjectCenterDropdown->objectBoundsBtn.performClick(); objectCenterDropdown.toggle();", "" );// Bounds Center
	%map.bindCmd( keyboard, "k", "Lab.setGizmoAlignment(Object);", "" );// Object Transform
	%map.bindCmd( keyboard, "l", "Lab.setGizmoAlignment(World);", "" );//World Transform
	SceneEditorPlugin.map = %map;

	SceneEditorTree.myInspector = SceneInspector;
	SEP_GroundCover.buildLayerSettingGui();

	if (isObject(LabTransformBox))
		LabTransformBox.deactivate();

	SceneEditorTools.getToolClones();
	hide(SEP_ScenePageSettings);
	SEP_ScenePage.init();

	SceneEd.initToolsGui();
	SceneEditorTreeTabBook.selectPage(0);

	if (!isObject(UnlistedDatablocks))
		new SimSet( UnlistedDatablocks );

	SceneEd.initPageDatablock();
	SEP_BuilderPageToolsStack.callOnChildrenNoRecurse("collapse");
	SEP_PageUtilityStack.callOnChildrenNoRecurse("collapse");
}
//------------------------------------------------------------------------------
//==============================================================================
// Called when the Plugin is activated (Active TorqueLab plugin)
function SceneEditorPlugin::onActivated( %this )
{
	Parent::onActivated( %this );
	
	%this.initToolBar();
	SceneEditorUtilityBook.init();
	SceneEditorTreeTabBook.init();
	SceneEditorTreeFilter.extent.x = SceneEditorTreeTabBook.extent.x -  56;
	SceneEditorTreeTabBook.selectPage($SceneEd_TreePage);
	SceneEditorDialogs.visible = 1;
	//SceneEditorUtilityBook.selectPage($SceneEd_UtilityPage);
	//SEP_Creator.init();
	//hide(SEP_CreatorSettings);
	SceneEditorTools.checkToolClones();

	//SceneEd.initPrefabCreator();
	if (SceneEditorPlugin.getCfg("DropType") !$= "")
		EWorldEditor.dropType = %this.getCfg("DropType");

	//if (SceneEd.ambientGroups !$= $MissionAmbientGroups)
		//SEP_AmbientManager.updateAmbientGroups();

	Lab.joinEvent("SceneChanged",SceneEditorTree);
	SceneEditorTree.rebuild();

	SceneEditorPlugin.setMode("editor");
}
//------------------------------------------------------------------------------
//==============================================================================
// Called when the Plugin is deactivated (active to inactive transition)
function SceneEditorPlugin::onDeactivated( %this,%newPlugin )
{
	if (SideBarVIS.currentPresetFile $= "visBuilder" && 1 == 0)
	{
		SideBarVIS.loadPresetFile("default");
		%this.text = "Set TSStatic-Only Selectable";
	}

	Parent::onDeactivated( %this );
}
//------------------------------------------------------------------------------
//==============================================================================
// Called from TorqueLab after plugin is initialize to set needed settings
function SceneEditorPlugin::onPluginCreated( %this )
{
	EWorldEditor.dropType = SceneEditorPlugin.getCfg("DropType");
}
//------------------------------------------------------------------------------

//==============================================================================
// Called when the mission file has been saved
function SceneEditorPlugin::onSaveMission( %this, %file )
{
	SEP_GroundCover.setNotDirty();
}
//------------------------------------------------------------------------------
//==============================================================================
// Called when the mission file has been saved
function SceneEditorPlugin::onExitMission( %this )
{
}
//------------------------------------------------------------------------------
//==============================================================================
// Called when TorqueLab is closed
function SceneEditorPlugin::onEditorSleep( %this )
{
	%this.setCfg("renameInternal",SceneEditorTree.renameInternal);
	%this.setCfg("showObjectNames",SceneEditorTree.showObjectNames);
	%this.setCfg("showInternalNames",SceneEditorTree.showInternalNames);
	%this.setCfg("showObjectIds",  SceneEditorTree.showObjectIds);
	%this.setCfg("showClassNames",SceneEditorTree.showClassNames);
}
//------------------------------------------------------------------------------
//==============================================================================
//Called when editor is selected from menu
function SceneEditorPlugin::onEditMenuSelect( %this, %editMenu )
{
	%canCutCopy = EWorldEditor.getSelectionSize() > 0;
	%editMenu.enableItem( 3, %canCutCopy ); // Cut
	%editMenu.enableItem( 4, %canCutCopy ); // Copy
	%editMenu.enableItem( 5, EWorldEditor.canPasteSelection() ); // Paste
	%selSize = EWorldEditor.getSelectionSize();
	%lockCount = EWorldEditor.getSelectionLockCount();
	%hideCount = EWorldEditor.getSelectionHiddenCount();
	%editMenu.enableItem( 6, %selSize > 0 && %lockCount != %selSize ); // Delete Selection
	%editMenu.enableItem( 8, %canCutCopy ); // Deselect
}
//------------------------------------------------------------------------------

//==============================================================================
//Called when editor is selected from menu

//------------------------------------------------------------------------------

//==============================================================================
// Callbacks Handlers - Called on specific editor actions
//==============================================================================
//==============================================================================
//
function SceneEditorPlugin::handleDelete( %this )
{
	//Call the global scene object deletion function
	Scene.deleteSelection();
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneEditorPlugin::handleDeselect()
{
	EWorldEditor.clearSelection();
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneEditorPlugin::handleCut()
{
	EWorldEditor.cutSelection();
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneEditorPlugin::handleCopy()
{   
	EWorldEditor.copySelection();
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneEditorPlugin::handlePaste()
{   
	EWorldEditor.pasteSelection();
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneEditorPlugin::onToolsResized(%this)
{
	SceneEditorTree.buildVisibleTree();
	//SceneEditorTree.expandItem(0);
}
//------------------------------------------------------------------------------
//==============================================================================
$SceneEditorPluginToolModes = "Inspector Builder";

//==============================================================================
//Called when editor frames has been resized
function SceneEditorPlugin::onLayoutResized( %this )
{
	SceneEditorTree.buildVisibleTree();
	//Cause an invalid item all time
	//%rootItem = SceneEditorTree.getFirstRootItem();
	//SceneEditorTree.expandItem(%rootItem);
}
//------------------------------------------------------------------------------
//==============================================================================
//SceneEditorPlugin.setMode("Builder");
function SceneEditorPlugin::setMode( %this,%mode )
{
	return;

	foreach(%book in SceneEditorInspectorGui)
		%book.visible = (%mode $= %book.internalName);

	switch$(%mode)
	{
		case "builder":
			SceneEditorDialogs.showDlg("BuilderTools");
			SceneEd.buildMode = "builder";

		case "editor":
			SceneEditorDialogs.hideDlg("BuilderTools");
			%this.closeModeBuilder();
			SceneEd.buildMode = "editor";
	}

}
//------------------------------------------------------------------------------
