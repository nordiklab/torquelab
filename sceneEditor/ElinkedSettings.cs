//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Initialize the WorldEditor GUI options and settings
function Lab::initSceneElinks(%this)
{
	ELink.menuClear("SceneAlignMenu");
	ELink.menuAdd("SceneAlignMenu","World",0);
	ELink.menuAdd("SceneAlignMenu","Object",1);
	ELink.menuSelect("SceneAlignMenu",0);

	//EWorldEditorAlignPopup.clear();
	//EWorldEditorAlignPopup.add("World",0);
	//EWorldEditorAlignPopup.add("Object",1);
	//EWorldEditorAlignPopup.setSelected(0);

	//---------------------------------------------------------------------------
	Lab.initDropTypeMenu();
	//---------------------------------------------------------------------------
	//EWorldEditorStatusBarCamera.clear();
	ELink.menuClear("SceneCamModeMenu");
	%i=0;

	while($LabCameraDisplayName[%i] !$= "")
	{
		ELink.menuAdd("SceneCamModeMenu",$LabCameraDisplayName[%i],%i);
		//EWorldEditorStatusBarCamera.add($LabCameraDisplayName[%i],%i);
		%i++;
	}

	ELink.setText("SceneCamModeMenu",Lab.currentCameraDisplay);

}
