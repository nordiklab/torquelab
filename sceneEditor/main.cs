//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function SceneEditor::create( %this )
{
    info("Plugin","->","Initializing","Scene Editor plugin" );
	//Load the guis and scripts
	execSceneEd(true);
	//Create the TorqueLab Plugin instance
	//Lab.createPlugin("SceneEditor","Scene Editor");
	//Add the Plugin related GUIs to TorqueLab
	Lab.addPluginGui("SceneEditor",SceneEditorTools);
	Lab.addPluginToolbar("SceneEditor",SceneEditorToolbar);
	//Lab.addPluginPalette("SceneEditor",SceneEditorPalette);
	Lab.addPluginPalette("SceneEditor",SceneEditorPaletteStack);
	Lab.addPluginDlg("SceneEditor",SceneEditorDialogs);
	Lab.addPluginMap("SceneEditor");
	SceneEditorPlugin.superClass = "WEditorPlugin";
	$SceneEd = newScriptObject("SceneEd");
	$DbEd = newScriptObject("DbEd");
	$SEPtools = newScriptObject("SEPtools");
	$SceneObjectGroupSet = newSimSet(SceneObjectGroupSet);
	$SceneCreator = newScriptObject("SceneCreator");
	$sepVM = newScriptObject("sepVM");
	$sepVM_TempDatablocks = newSimGroup("sepVM_TempDatablocks");
}
function SceneEditor::destroy( %this )
{
    
}
//==============================================================================
//Initialize the Scene Editor Plugin
function initializeSceneEditor()
{
	
}
//------------------------------------------------------------------------------
//==============================================================================
// Load the Scene Editor Plugin scripts, load Guis if %loadgui = true
function execSceneEd(%loadGui)
{
	if (%loadGui)
	{
		exec("tlab/sceneEditor/gui/SceneEditorTools.gui");
		exec("tlab/sceneEditor/gui/SceneEditorToolbar.gui" );
		exec("tlab/sceneEditor/gui/SceneEditorPalette.gui" );
		exec("tlab/sceneEditor/gui/SceneEditorDialogs.gui");
	}

	exec("tlab/sceneEditor/SceneEditorPlugin.cs");
	exec("tlab/sceneEditor/gui/SceneEditorToolbar.cs");
	exec("tlab/sceneEditor/gui/SceneEditorTools.cs");
	exec("tlab/sceneEditor/gui/SceneEditorDialogs.cs");

	exec("tlab/sceneEditor/initSceneEditor.cs");
	exec("tlab/sceneEditor/ElinkedSettings.cs");

	execPattern("tlab/sceneEditor/plugin/*.cs");
	execPattern("tlab/sceneEditor/managers/*.cs","old");
	execPattern("tlab/sceneEditor/utilityBook/*.cs");
	execPattern("tlab/sceneEditor/treeBook/*.cs");

}
//------------------------------------------------------------------------------

