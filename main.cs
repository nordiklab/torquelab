//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$Cfg_TLab_missingTexturePath = "core/images/missingTexture.png";
$Cfg_TLab_unavailableTexturePath = "core/images/unavailable.png";
$Cfg_TLab_warningTexturePath = "core/images/warnMat.dds";
$Cfg_TLab_DefaultTerrainPath = "game/art/terrains/";

//From AlterVerse, need to be adapted for common use
exec("tlab/config.cfg.cs");

%helpersLab = "tlab/system/helpers/initHelpers.cs";
if (isFile(%helpersLab) && !$HelperLabLoaded)
	exec(%helpersLab);

if ($Cfg_TLab_Theme $= "")
	$Cfg_TLab_Theme = "DarkLab";

//Lab.loadScriptsProfiles();
%profilesPath = "tlab/themes/"@$Cfg_TLab_Theme@"/";
$Cfg_TLab_ThemePath = %profilesPath;

if (isFile($Cfg_TLab_ThemePath@"profileOverides.cs"))
	exec($Cfg_TLab_ThemePath@"profileOverides.cs");

//exec("tlab/avTools.cs");
//==============================================================================
//Do TorqueLab should be initialized after main onStart?
$TorqueLabInitMode = "1";
//0 = Wait for toggle bind (F10, F11)
//1 = At end of main onSTart function
//2 = After GuiStartup On Wake
//Anything Else = wait for a initTorqueLab(); call place anywhere
//If $TorqueLabInitOnStart is set to false, you need to initialize it manually
//There's 3 options available for post start initialization
//1- It will initialized when you press F10 or F11 (Editors toggle binds)
//2- Add a initTorqueLab(); call anywhere in your scripts
//3- Use the custom function overide package at bottom (At end of Lab Package)
//   You simply set the function from which you want TorqueLab to be initialized.
//------------------------------------------------------------------------------
if ($platform $= "macos")
	{
		$KeyCtrl = "Cmd";
		$KeyAltCtrl = "Cmd";
		$KeyQuit = "Cmd Q";
		$KeyRedo = "Cmd-Shift Z";
	}
	else
	{
		$KeyCtrl = "Ctrl";
		$KeyAltCtrl = "Alt";
		$KeyQuit = "Alt F4";
		$KeyRedo = "Ctrl Y";
	}
//==============================================================================
// TorqueLab Core Global Settings
//==============================================================================
// Path to the folder that contains the editors we will load.
$Lab::OldConfigSystem = false;
$Lab::resourcePath = "tlab/";

if (isObject($pref::UI::defaultGui))
	$Cfg_TLab_defaultGui = $pref::UI::defaultGui;
else
	$Cfg_TLab_defaultGui = "GuiGameMenu";

$Cfg_GameLab_ActionMap = "moveMap";
// Global holding material list for active simobject
$Lab::materialEditorList = "";

// These must be loaded first, in this order, before anything else is loaded
$Lab::loadFirst = "sceneEditor";
$Lab::loadLast = "materialLab";
// These folders must be skipped for initial load
$LabIgnoreEnableFolderList = "debugger forestEditor levels guiEditor";

$Lab::LevelRoot = "levels";
//Load the TorqueLab Main Initialization script.
exec("tlab/initTorqueLab.cs");

//==============================================================================
// Lab Package contain overide function that deal with TorqueLab
//==============================================================================
package Lab
{

	//------------------------------------------------------------------------------

	//==============================================================================
	// onExit() - Call just before the application is shutted down
	//------------------------------------------------------------------------------
	// This will terminated some editor process and make sure everything is deleted
	//------------------------------------------------------------------------------
	function onExit()
	{
		if ( LabEditor.isInitialized )
			EditorGui.shutdown();

		// Free all the icon images in the registry.
		EditorIconRegistry::clear();

		//Call destroy function of all editors
		for (%i = 0; %i < $editors[count]; %i++)
		{
			%destroyFunction = "destroy" @ $editors[%i];

			if ( isFunction( %destroyFunction ) )
				call( %destroyFunction );
		}

		// Call Parent.
		Parent::onExit();
	}
	//------------------------------------------------------------------------------
	
	//------------------------------------------------------------------------------
	//==============================================================================
	// All the plugins scripts have been loaded
	function Editor::checkActiveLoadDone( %this )
	{
		if (!isObject(EditorGui))
			return false;

		if (EditorGui.isAwake())
			return true;

		return false;
	}
	//------------------------------------------------------------------------------

};
//------------------------------------------------------------------------------
//==============================================================================
// Activate Package Lab
activatePackage(Lab);
//------------------------------------------------------------------------------

//==============================================================================
// Toggle a Dialog GUI
function toggleEdDlg(%dlg)
{
	if (!isObject(%dlg))
		initTorqueLab();

	if (%dlg.isAwake())
		popDlg(%dlg);
	else
		Canvas.pushDialog(%dlg);
}
//------------------------------------------------------------------------------

//==============================================================================
// Special logging for tracing purpose

//==============================================================================
// Toggle a Dialog GUI
function dlog(%text,%a1, %a2, %a3, %a4,%a5, %a6, %a7, %a8,%a9, %a10, %a11)
{
	if ($DLOG)
	   devLog(%text,%a1, %a2, %a3, %a4,%a5, %a6, %a7, %a8,%a9, %a10, %a11);
}
//------------------------------------------------------------------------------
function setEditorBinds()
{

	/*if (isObject(GuiEditor))
	{
		GlobalActionMap.bindCmd( keyboard, "f10", "toggleGuiEdit(true);","" );
		GlobalActionMap.bindCmd( keyboard, "ctrl f10", "toggleGuiEdit(true,true);","" );
	}
	else
	{
		GlobalActionMap.bindCmd(keyboard,"F10","initTorqueLab(\"Gui\",false);","");
		GlobalActionMap.bindCmd(keyboard,"Ctrl F10","initTorqueLab(\"Gui\",true);","");
	}

	if ($WorldEditorLoaded)
		GlobalActionMap.bindCmd(keyboard,"F11","toggleEditor(true);","");
	else
		GlobalActionMap.bindCmd(keyboard,"F11","initTorqueLab(\"World\");","");
*/

	GlobalActionMap.bindCmd(keyboard, "f5", "toggleEdDlg(LabGuiManager);","");
	
		GlobalActionMap.bindCmd( keyboard, "f10", "actionToggleGuiEditor();","" );
		GlobalActionMap.bindCmd( keyboard, "ctrl f10", "actionToggleGuiEditor(true);","" );
		GlobalActionMap.bindCmd(keyboard,"F11","actionToggleWorldEditor();","");
}
setEditorBinds();
function actionToggleGuiEditor(%lastContent)
{
    if (!isObject(GuiEditor))
   {
      initTorqueLab("Gui");
      return;
   }
   toggleGuiEdit(true,%lastContent);
 
}
function actionToggleWorldEditor()
{
    if (!isObject(EditorGui))
   {
      initTorqueLab("World");
      return;
   }
   toggleEditor(true);
 
}
//------------------------------------------------------------------------------
