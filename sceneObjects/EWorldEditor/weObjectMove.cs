//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function WorldEditor::onSelectionCentroidChanged(%this)
{
	Scene.setDirty();

	Scene.syncSelectionGui();

	// Refresh inspector.
	Scene.doRefreshInspect();
}
//------------------------------------------------------------------------------

//==============================================================================
function WorldEditor::onDragStart(%this, %obj)
{

	Scene.dragStartPos = %this.getSelectionCentroid();
	Scene.dragStartSet = newSimSet(SceneDragStartSet,MissionCleanup);

	foreach$(%obj in Scene.getWorldSelList())
		SceneDragStartSet.add(%obj);
}
//------------------------------------------------------------------------------
//==============================================================================
function WorldEditor::onDragCopy(%this, %obj)
{
	Scene.dragCopyPos = %this.getSelectionCentroid();

	Scene.dragCopySet = newSimSet(SceneDragCopySet,MissionCleanup);

	foreach$(%obj in Scene.getWorldSelList())
		SceneDragCopySet.add(%obj);

	$DragCopyStarted = true;
}
//------------------------------------------------------------------------------

//==============================================================================
function WorldEditor::onEndDrag(%this, %obj)
{
	scene.doInspect(%obj);
	scene.doApplyInspect();

	//If force to grid is active, run it before processing the offset so it fit nicely on grid
	if ($Cfg_Common_Grid_forceToGrid)
		%this.forceToGrid();

	//Compute the dragOffset now even if nothing got copied so it can be used for other purpose
	Scene.dragEndPos = %this.getSelectionCentroid();
	Scene.lastDragOffset = VectorSub(Scene.dragEndPos,Scene.dragStartPos);

	//The rest if for when Copy drag was used, if not the case, leave now
	if (!$DragCopyStarted)
		return;

	info("EndDrag NEW Offset",Scene.lastDragOffset);

	//We need to check all cloned objects and moved those which are child of a
	// container into a new container. We need to make sure that all siblings
	// stay together in a new container
	%id = -1;

	//foreach$(%obj in Scene.dragCopyObjList)
	foreach(%obj in SceneDragCopySet)
	{
		//Simply call getSimContainer which return the container object if exist or nothing
		%simContainer = %obj.myContainer;

		if (isObject(%simContainer))
		{
			//Get the container ID which would be used as key for a temporary array
			// which keep track of cloned container so we create only those needed
			%contId = %simContainer.getId();

			//If %newContainer if not created yet, create it and next siblings will use it
			if (!isObject(%contAdded[%contId]))
			{
				//This function return a new container with a unique internalName from the original
				%newContainer = Lab.getContainerEmptyClone(%simContainer);
				%contAdded[%contId] =  %newContainer.getId();
			}
			else
				%newContainer = %contAdded[%contId];

			//Simply add the object to the new container
			%newContainer.add(%obj);
		}
	}

	//If CloneDragEnabled, we must show the multiple cloning popup which would duplicate the
	// completed drag copy X times baed on same offset as original drag cloning
	info("Enabled",Lab.CloneDragEnabled,"Offset",Scene.lastDragOffset,"Mode",GlobalGizmoProfile.mode);
	if (Scene.lastDragOffset !$="" && Lab.CloneDragEnabled && GlobalGizmoProfile.mode $= "move")
	{
		ECloneDrag.copyOffset = Scene.lastDragOffset;
		ETools.showTool(CloneDrag);
	}

	$DragCopyStarted = false;
}
//------------------------------------------------------------------------------

