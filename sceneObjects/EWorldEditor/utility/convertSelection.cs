//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function EWorldEditor::convertSelectionToPolyhedralObjects(%this, %className)
{
	%group = Scene.getActiveSimGroup();
	%undoManager = Editor.getUndoManager();
	%activeSelection = %this.getActiveSelection();

	while(%activeSelection.getCount() != 0)
	{
		%oldObject = %activeSelection.getObject(0);
		%newObject = %this.createPolyhedralObject(%className, %oldObject);

		if (isObject(%newObject))
		{
			%undoManager.pushCompound("Convert ConvexShape to " @ %className);
			%newObject.parentGroup = %oldObject.parentGroup;
			MECreateUndoAction::submit(%newObject);
			MEDeleteUndoAction::submit(%oldObject);
			%undoManager.popCompound();
		}
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function EWorldEditor::convertSelectionToConvexShape(%this)
{
	%group = Scene.getActiveSimGroup();
	%undoManager = Editor.getUndoManager();
	%activeSelection = %this.getActiveSelection();

	while(%activeSelection.getCount() != 0)
	{
		%oldObject = %activeSelection.getObject(0);
		%newObject = %this.createConvexShapeFrom(%oldObject);

		if (isObject(%newObject))
		{
			%undoManager.pushCompound("Convert " @ %oldObject.getClassName() @ " to ConvexShape");
			%newObject.parentGroup = %oldObject.parentGroup;
			MECreateUndoAction::submit(%newObject);
			MEDeleteUndoAction::submit(%oldObject);
			%undoManager.popCompound();
		}
	}
}
//------------------------------------------------------------------------------
