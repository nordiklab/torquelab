//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$WorldEditor_DropTypes = "toTerrain atOrigin atCamera atCameraRot belowCamera screenCenter atCentroid belowSelection";
$Scene_DropTypes = "currentSel currentSelRot";
$Scene_AllDropTypes = $WorldEditor_DropTypes SPC $Scene_DropTypes;

$Scene_DropTypeDisplay["atOrigin"] = "at origin";
$Scene_DropTypeDisplay["atCamera"] = "at camera";
$Scene_DropTypeDisplay["atCameraRot"] = "at camera+Rot.";
$Scene_DropTypeDisplay["belowCamera"] = "below camera";
$Scene_DropTypeDisplay["screenCenter"] = "at screenCenter";
$Scene_DropTypeDisplay["atCentroid"] = "at centroid";
$Scene_DropTypeDisplay["toTerrain"] = "to terrain";
$Scene_DropTypeDisplay["belowSelection"] = "below sel.";
$Scene_DropTypeDisplay["currentSel"] = "at current sel.";
$Scene_DropTypeDisplay["currentSelRot"] = "at current sel.";

/*
//Using the droptype word index in $Scene_AllDropTypes
$Scene_DropTypeId["0"] = "toTerrain";
$Scene_DropTypeId["1"] = "atOrigin";
$Scene_DropTypeId["2"] = "atCamera";
$Scene_DropTypeId["3"] = "atCameraRot";
$Scene_DropTypeId["4"] = "belowCamera";
$Scene_DropTypeId["5"] = "belowCamera";
$Scene_DropTypeId["6"] = "atCentroid";
$Scene_DropTypeId["7"] = "belowSelection";
$Scene_DropTypeId["8"] = "currentSel";
$Scene_DropTypeId["9"] = "currentSelZ";
*/

//==============================================================================
function Lab::initDropTypeMenu(%this, %id,%text)
{
	Scene.dropMode = $Cfg_Common_Objects_dropType;
	Scene.setDropType(Scene.dropMode);
	ELink.menuClear("SceneDropTypeMenu");
	%dropId = 0;

	foreach$(%dropType in $Scene_AllDropTypes)
	{
		//Shouldn't happen but better than continue which was missing the dropID inc
		if ($Scene_DropTypeDisplay[%dropType] !$= "")
			ELink.menuAdd("SceneDropTypeMenu","Drop> "@$Scene_DropTypeDisplay[%dropType],%dropId);

		%dropId++;
	}

	ELink.setText("SceneDropTypeMenu",Scene.dropMode);
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneDropTypeMenu::onSelect(%this, %id,%text)
{
	%dropType = getWord($Scene_AllDropTypes,%id);
	Scene.setDropType(%dropType);
}
//------------------------------------------------------------------------------
//==============================================================================
function Scene::setDropType(%this, %dropType)
{
	%dropType = (%dropType $= "") ? $Cfg_Common_Objects_dropType : %dropType;

	if ($Scene_DropTypeDisplay[%dropType] $= "")
		return;

	%dropTypeFunc = $Scene_DropTypeDisplay[%dropType];

	foreach$(%menu in Scene.dropTypeMenus)
	{
		%menu.setText(%dropTypeFunc);
	}

	//Only set coded WorldEditor drop types and not custom lab drop types
	if (strFind($WorldEditor_DropTypes,%dropType))
		EWorldEditor.dropType = %dropType;

	Scene.dropMode = %dropType;
	ELink.setText("SceneDropTypeMenu",Scene.dropMode);
	$Cfg_Common_Objects_dropType = %dropType;
}
//------------------------------------------------------------------------------

//==============================================================================
// CHeck for custom drop setting, if not the WorldEditor will use it dropType
// When $SceneEd_SkipEditorDrop is true, the WorldEditor dropSelection is skip
//		and position is set from script
/*
function Scene::getCreateObjectPosition()
{
	%focusPoint = LocalClientConnection.getControlObject().getLookAtPoint();
	$SceneEd_SkipEditorDrop = true;

	%dropMode =Scene.dropMode;
   if (%dropMode $= "currentSel" && !isObject(EWorldEditor.getSelectedObject(0)))
      %dropMode = EWorldEditor.dropType;

   switch$(%dropMode)
   {
      case "currentSel":
         %dropPos = EWorldEditor.getSelectedObject(0).position;
      case "atCamera":
      %dropPos = LocalClientConnection.getControlObject().position;
      case "atCameraRot":
      %dropPos = LocalClientConnection.getControlObject().getTransForm();
   }

	if (%dropPos !$= "")
		return %dropPos;

	%position =  getWord(%focusPoint, 1) SPC getWord(%focusPoint, 2) SPC getWord(%focusPoint, 3);
	$SceneEd_SkipEditorDrop = false;
	return %position;
}
*/
//Simply call transform function and return the 3 first words (x y z)
function Scene::getCreateObjectPosition()
{
	%transform =  Scene.getCreateObjectTransform();
	return getWords(%transform,0,3);
}
//------------------------------------------------------------------------------
//==============================================================================
// CHeck for custom drop setting, if not the WorldEditor will use it dropType
// When $SceneEd_SkipEditorDrop is true, the WorldEditor dropSelection is skip
//		and position is set from script
function Scene::getCreateObjectTransform()
{
	%dropMode =Scene.dropMode;

	if (strFind(%dropMode,"currentSel") && !isObject(EWorldEditor.getSelectedObject(0)))
		%dropMode = EWorldEditor.dropType;

	$SceneEd_SkipEditorDrop = true;

	switch$(%dropMode)
	{
		case "currentSel":
			%transform = EWorldEditor.getSelectedObject(0).position SPC "1 0 0 0";

		case "currentSelRot":
			%selRot = EWorldEditor.getSelectedObject(0).rotation;
			%transform = EWorldEditor.getSelectedObject(0).position SPC %selRot;

		case "atCamera":
			%transform = LocalClientConnection.getControlObject().position SPC "1 0 0 0";

		case "atCameraRot":
			%transform = LocalClientConnection.getControlObject().getTransForm();

		default:
			%focusPoint = LocalClientConnection.getControlObject().getLookAtPoint();
			%position =  getWord(%focusPoint, 1) SPC getWord(%focusPoint, 2) SPC getWord(%focusPoint, 3);
			%transform = %position SPC "1 0 0 0";
			$SceneEd_SkipEditorDrop = false;
	}

	return %transform;
}
//------------------------------------------------------------------------------
