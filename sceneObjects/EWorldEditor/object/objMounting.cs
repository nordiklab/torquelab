//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function Lab::mountObjAToB(%this)
{
	echo("EditorMount");
	%size = EWorldEditor.getSelectionSize();

	if (%size != 2)
		return;

	%a = EWorldEditor.getSelectedObject(0);
	%b = EWorldEditor.getSelectedObject(1);
	//%a.mountObject( %b, 0 );
	EWorldEditor.mountRelative(%a, %b);
}

function Lab::unmountObjA(%this)
{
	echo("EditorUnmount");
	%obj = EWorldEditor.getSelectedObject(0);
	%obj.unmount();
}
//------------------------------------------------------------------------------

//==============================================================================
// Sync the different Selection related Gui
// Scene.mountShapeToSel("art/players/base/basemale/Magellan/helmet.dts", "6");
function Scene::mountShapeToSel(%this,%file,%node)
{
	// Inform the camera
	if (!isFile(%file))
		return;

	%target = %this.getSelectedObject(0);

	if (!isObject(%target))
		return;

	%objId = new TSStatic()
	{
		shapeName = %file;
		position = %target.position;

	};
	missionCleanup.add(%objId);

	%target.mountObject(%objId, %node);
}
//------------------------------------------------------------------------------
