//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Copy, Clone and Duplicated Objects Functions
//==============================================================================

//==============================================================================
// Make a copie of all selected objects
function Lab::copyDraggedSelection(%this,%offset, %copyCount)
{
	%count = EWorldEditor.getSelectionSize();

   //If nothing is selected, we want to use the last dragged set as reference
	if (%count < 1)
	{
	   //If nothing selected and DragCopySet don't exist, nothing to do here
	   if (!isObject(SceneDragCopySet))
	      return;
	      
		if (SceneDragCopySet.getCount() > 0)
		{
			Lab.cloneLastDragCopySet(%offset,%copyCount);
			return;
		}

		warnLog("There's no selected objects to copy!");
		return;
	}

	for(%i=1; %i<=%copyCount; %i++)
	{
		%containersClone = "";

		for(%j=0; %j<%count; %j++)
		{
			%obj = EWorldEditor.getSelectedObject(%j);

			if (!%obj.isMemberOfClass("SceneObject")) continue;

			%obj.startDrag = "";
			%simContainer = %obj.getSimContainer();

			if (isObject(%simContainer))
			{
				if (strFindWords(%containersClone,%simContainer.getId()))
					continue;

				Scene.cloneContainer(%simContainer,%offset,%i);
				%containersClone = strAddWord(%containersClone,%simContainer.getId(),1);
				continue;

			}

			%clone = %obj.clone();
			%clone.position.x += %offset.x * %i;
			%clone.position.y += %offset.y * %i;
			%clone.position.z += %offset.z * %i;
			Scene.addObjectToScene(%clone);

		}
	}

	Scene.doRefreshInspect();

}
//------------------------------------------------------------------------------
//==============================================================================
// Make a copie of all selected objects
function Lab::cloneLastDragCopySet(%this,%offset, %copyCount)
{
	if (SceneDragCopySet.getCount() <= 0)
	{
		warnLog("There's no selected objects to copy!");
		return;
	}

	for(%i=1; %i<=%copyCount; %i++)
	{
		%containersClone = "";

		foreach(%obj in SceneDragCopySet)
		{
			%simContainer = %obj.getSimContainer();

			if (isObject(%simContainer))
			{
				if (strFindWords(%containersClone,%simContainer.getId()))
					continue;

				Scene.cloneContainer(%simContainer,%offset,%i);
				%containersClone = strAddWord(%containersClone,%simContainer.getId(),1);
				continue;
			}

			%clone = %obj.clone();
			%clone.position.x += %offset.x * %i;
			%clone.position.y += %offset.y * %i;
			%clone.position.z += %offset.z * %i;
			Scene.addObjectToScene(%clone);

		}
	}

	Scene.doRefreshInspect();

}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
function testSel()
{
foreach(%obj in EWorldEditor.getActiveSelection())
   {
      info(%id++,"Obj:",%obj,"Int:",%obj.internalName);
   }
}
