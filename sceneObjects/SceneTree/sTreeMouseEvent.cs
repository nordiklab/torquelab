//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Tree Items Drag and Dropping
//==============================================================================
//==============================================================================
// Called when an item as been dropped
function SceneObjectsTree::onDragDropped(%this)
{
	Scene.setDirty();
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneObjectsTree::isValidDragTarget(%this, %id, %obj)
{
	if (%obj.isMemberOfClass("Path"))
		return EWorldEditor.areAllSelectedObjectsOfType("Marker");

	if (%obj.name $= "CameraBookmarks")
		return EWorldEditor.areAllSelectedObjectsOfType("CameraBookmark");
	else
		return (%obj.isMemberOfClass("SimSet")); // $= "SimGroup" );
}
//------------------------------------------------------------------------------

//==============================================================================
// Set object selected in scene (Trees and WorldEditor)
//==============================================================================
function SceneEditorTree::onMouseUp(%this,%hitItemId, %mouseClickCount)
{
	if (%mouseClickCount > 2)
	{
		//Need to delay as it seem to be corrupted by onAddSelection
		SceneEditorTree.schedule(10,showItemRenameCtrl,%hitItemId);
		//SceneEditorTree.showItemRenameCtrl(%hitItemId);
	}
	else if (%mouseClickCount > 4)
	{
		%obj = %this.getItemValue(%hitItemId);

		if (%obj.getClassName() $= "SimGroup")
			Scene.setActiveSimGroup(%obj);
		else if (isObject(%obj.parentGroup))
			Scene.setActiveSimGroup(%obj.parentGroup);
	}
}

//==============================================================================
function SceneObjectsTree::onMouseDown(%this,%hitItemId, %mouseClickCount)
{

	//Nothing as now (expand moved to onAddSelection)
	if (%mouseClickCount > 1)
	{
		%this.showItemRenameCtrl(%hitItemId);
	}

}
function SceneObjectsTree::onRightMouseUp(%this, %itemId, %mouse, %obj)
{
	%this.showContextMenu(%itemId, %mouse, %obj);
}
function SceneObjectsTree::onMiddleMouseDown(%this,%hitItemId, %mouseClickCount)
{
	%obj = %this.getItemValue(%hitItemId);

	if (%obj.getClassName() $= "SimGroup")
		Scene.setActiveSimGroup(%obj);
	else if (isObject(%obj.parentGroup))
		Scene.setActiveSimGroup(%obj.parentGroup);

}

function SceneObjectsTree::onMiddleMouseUp(%this,%hitItemId, %onIcon,%object)
{

}
