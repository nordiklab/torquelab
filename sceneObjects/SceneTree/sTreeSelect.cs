//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// SceneTree Select/Unselect related callbacks
//==============================================================================

//==============================================================================
function SceneObjectsTree::onSelect(%this, %obj)
{
	//This do nothing, the onAddSelection function is used	
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneObjectsTree::onUnselect(%this, %obj)
{
	Scene.onRemoveSelection(%obj, %this);
}
//------------------------------------------------------------------------------
//==============================================================================

function SceneObjectsTree::onAddSelection(%this, %obj, %isLastSelection)
{	
	%item	= SceneEditorTree.findItemByObjectId(%obj.getId());

	switch$(%obj.getClassName())
	{
		case "SimContainer":
			if (%obj.getClassName() $= "SimContainer")
			{
				//If container is locked, it mean the children are selected together
				if (	%obj.locked || Lab.autoSelectContainer)
				{
					Scene.selectObjectContainer(%obj);
					return;
				}
			}

		case "SimGroup":

			//Get the simgroup expanded state
			%expand = !%obj.isExpanded();

			if (%this.selectedSimGroup.getId() $= %obj.getId())
				info("Already selected");

			//expand the item based on SimGroup expanded state
			%this.expandItem(%item,%expand);
			%obj.setIsExpanded(%expand);
			%this.selectedSimGroup = %obj;

		default:
			if (%obj.parentGroup.getClassName() $= "SimContainer")
			{
				//If container is locked, it mean the children are selected together
				if (	%obj.parentGroup.locked || Lab.autoSelectContainer)
				{
					Scene.selectObjectContainer(%obj.parentGroup);
					//Return, the selectObject Container will deal with individual select
					return;					
				}
			}
			
	}

	if (%isLastSelection)
		Lab.postEvent("SceneTreeChanged",EWorldEditor.getActiveSelection());

	Scene.onAddSelection(%obj, %isLastSelection,%this);
}
//------------------------------------------------------------------------------
//==============================================================================
// Called when an item with no child is selected
function SceneObjectsTree::onInspect(%this, %obj)
{
}
//------------------------------------------------------------------------------
//==============================================================================
// SceneTree Selection related callbacks
//==============================================================================

//==============================================================================
// Called after the current tree selection was cleared
function SceneObjectsTree::onClearSelection(%this)
{
	//Scene.doInspect("");
}
//------------------------------------------------------------------------------
//==============================================================================
// Called after a single object was removed from tree selection
function SceneObjectsTree::onRemoveSelection(%this, %obj)
{
	//This is important to unselect object in WorldEditor
	Scene.onRemoveSelection(%obj,%this);

}
//------------------------------------------------------------------------------

//==============================================================================
// SceneTree Deletion related callbacks
//==============================================================================
//==============================================================================
// Called just before selection deletion process start
function SceneObjectsTree::onDeleteSelection(%this)
{
	%this.undoDeleteList = "";
}
//------------------------------------------------------------------------------

//==============================================================================
// Called prior object deletion, would abort deletion if returning true
function SceneObjectsTree::onDeleteObject(%this, %object)
{
	// Don't delete locked objects
	if (%object.locked)
		return true;

	if (%object == Scene.activeSimGroup)
		Scene.setActiveSimGroup(MissionGroup);

	// Append it to our list.
	%this.undoDeleteList = %this.undoDeleteList TAB %object;
	// We're gonna delete this ourselves in the
	// completion callback.
	return true;
}
//------------------------------------------------------------------------------
//==============================================================================
// Called after a tree object have beenn deleted
function SceneObjectsTree::onObjectDeleteCompleted(%this)
{
	// This can be called when a deletion is attempted but nothing was
	// actually deleted ( cannot delete the root of the tree ) so only submit
	// the undo if we really deleted something.
	if (%this.undoDeleteList !$= "")
		MEDeleteUndoAction::submit(%this.undoDeleteList);

	Scene.onObjectDeleteCompleted();
}
//------------------------------------------------------------------------------
//==============================================================================
// SceneTree Object UnSelect Functions
//==============================================================================

//==============================================================================
// SceneTree Object Deletion Functions
//==============================================================================

