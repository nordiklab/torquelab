//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Set object selected in scene (Trees and WorldEditor)
//==============================================================================

//==============================================================================
function SceneObjectsTree::onBeginReparenting(%this)
{
	if (isObject(%this.reparentUndoAction))
		%this.reparentUndoAction.delete();

	%action = UndoActionReparentObjects::create(%this);
	%this.reparentUndoAction = %action;
}
//------------------------------------------------------------------------------

//==============================================================================
function SceneObjectsTree::onReparent(%this, %obj, %oldParent, %newParent)
{
	%this.reparentUndoAction.add(%obj, %oldParent, %newParent);
}
//------------------------------------------------------------------------------

//==============================================================================
function SceneObjectsTree::onEndReparenting(%this)
{
	%action = %this.reparentUndoAction;
	%this.reparentUndoAction = "";

	if (%action.numObjects > 0)
	{
		if (%action.numObjects == 1)
			%action.actionName = "Reparent Object";
		else
			%action.actionName = "Reparent Objects";

		%action.addToManager(Editor.getUndoManager());
		EWorldEditor.syncGui();
	}
	else
		%action.delete();
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//==============================================================================
// Tooltip for TSStatic
function SceneObjectsTree::GetTooltipTSStatic(%this, %obj)
{
	return "Shape: " @ %obj.shapeName;
}
//------------------------------------------------------------------------------
//==============================================================================
// Tooltip for ShapeBase
function SceneObjectsTree::GetTooltipShapeBase(%this, %obj)
{
	return "Datablock: " @ %obj.dataBlock;
}
//------------------------------------------------------------------------------
//==============================================================================
// Tooltip for StaticShape
function SceneObjectsTree::GetTooltipStaticShape(%this, %obj)
{
	return "Datablock: " @ %obj.dataBlock;
}
//------------------------------------------------------------------------------
//==============================================================================
// Tooltip for Item
function SceneObjectsTree::GetTooltipItem(%this, %obj)
{
	return "Datablock: " @ %obj.dataBlock;
}
//------------------------------------------------------------------------------
//==============================================================================
// Tooltip for RigidShape
function SceneObjectsTree::GetTooltipRigidShape(%this, %obj)
{
	return "Datablock: " @ %obj.dataBlock;
}
//------------------------------------------------------------------------------
//==============================================================================
// Tooltip for Prefab
function SceneObjectsTree::GetTooltipPrefab(%this, %obj)
{
	return "File: " @ %obj.filename;
}
//------------------------------------------------------------------------------
//==============================================================================
// Tooltip for GroundCover
function SceneObjectsTree::GetTooltipGroundCover(%this, %obj)
{
	%text = "Material: " @ %obj.material;

	for(%i=0; %i<8; %i++)
	{
		if (%obj.probability[%i] > 0 && %obj.shapeFilename[%i] !$= "")
		{
			%text = %text NL "Shape " @ %i @ ": " @ %obj.shapeFilename[%i];
		}
	}

	return %text;
}
//------------------------------------------------------------------------------
//==============================================================================
// Tooltip for SFXEmitter
function SceneObjectsTree::GetTooltipSFXEmitter(%this, %obj)
{
	if (%obj.fileName $= "")
		return "Track: " @ %obj.track;
	else
		return "File: " @ %obj.fileName;
}
//------------------------------------------------------------------------------
//==============================================================================
// Tooltip for ParticleEmitterNode
function SceneObjectsTree::GetTooltipParticleEmitterNode(%this, %obj)
{
	%text = "Datablock: " @ %obj.dataBlock;
	%text = %text NL "Emitter: " @ %obj.emitter;
	return %text;
}
//------------------------------------------------------------------------------
//==============================================================================
// Tooltip for WorldEditorSelection
function SceneObjectsTree::GetTooltipWorldEditorSelection(%this, %obj)
{
	%text = "Objects: " @ %obj.getCount();

	if (!%obj.getCanSave())
		%text = %text NL "Persistent: No";
	else
		%text = %text NL "Persistent: Yes";

	return %text;
}
//------------------------------------------------------------------------------
