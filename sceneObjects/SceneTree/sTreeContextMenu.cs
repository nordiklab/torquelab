//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$LabContextMenuStyle = "Dark";
//==============================================================================
// Show SceneTree context Menu
//==============================================================================
function SceneObjectsTree::showContextMenu(%this, %itemId, %mouse, %obj)
{

	if (!isObject(%obj))
		return;

	%haveObjectEntries = false;
	%haveLockAndHideEntries = true;
	delObj(SceneTreePopup);
	%popup = newPopupMenu("SceneTreePopup",$LabContextMenuStyle);
	/*%popup = new PopupMenu(SceneTreePopup)
	{
		superClass = "MenuBuilder";
		profile = "ToolsPopupMenuProfile";
		isPopup = "1";
	};*/
	%popup.object = %obj;
	%id = -1;

	if (%this.internalName $= "LayerTree")
	{
		%popup.addItem(%id++,"Remove from layer" TAB "" TAB "Lab.removeObjectFromLayer("@%obj@");");
	}

	// Open context menu if this is a CameraBookmark
	if (%obj.isMemberOfClass("CameraBookmark"))
	{
		%popup.addItem(%id++,"Go To Bookmark" TAB "" TAB "EManageBookmarks.jumpToBookmark( %this.bookmark.getInternalName() );");
		SceneTreePopup.bookmark = %obj;
	}
	// Open context menu if this is set CameraBookmarks group.
	else if (%obj.name $= "CameraBookmarks")
	{
		%popup.addItem(%id++,"Add Camera Bookmark" TAB "" TAB "EManageBookmarks.addCameraBookmarkByGui();");
	}
	// Open context menu if this is a SimGroup
	else if (%obj.isMemberOfClass("SimGroup"))
	{
		%popup.addItem(%id++,"Rename" TAB "" TAB "SceneEditorTree.showItemRenameCtrl( SceneEditorTree.findItemByObjectId( %this.object ) );");
		%popup.addItem(%id++,"Delete" TAB "" TAB "EWorldEditor.deleteMissionObject( %this.object );");
		%popup.addItem(%id++,"Inspect" TAB "" TAB "inspectObject( %this.object );");
		%popup.addItem(%id++,"-");
		//%popup.addItem(%id++,"Toggle Lock Children" TAB "" TAB "EWorldEditor.toggleLockChildren( %this.object );");
		//%popup.addItem(%id++,"Toggle Hide Children" TAB "" TAB "EWorldEditor.toggleHideChildren( %this.object );");
		//%popup.addItem(%id++,"-");

		%popup.addItem(%id++,"Add selection to new group" TAB "" TAB "Scene.addSimGroup( true );");
		%popup.addItem(%id++,"Add selection to active group" TAB "" TAB "Scene.addSelectionToActiveGroup();");

		if (%obj.getClassName() $= "SimGroup" || %obj.getClassName() $= "SimContainer")
		{
			%popup.addItem(%id++,"---Group Options---");
			%popup.addItem(%id++,"Show Childrens" TAB "" TAB "Scene.showGroupChilds( "@%obj@" );");
			%popup.addItem(%id++,"Hide Childrens" TAB "" TAB "Scene.hideGroupChilds( "@%obj@" );");
			%popup.addItem(%id++,"Toggle Children Visibility" TAB "" TAB "Scene.toggleHideGroupChilds( "@%obj@" );");
			%popup.addItem(%id++,"-");

			%popup.addItem(%id++,"Select All Childrens" TAB "" TAB "Scene.selectGroupObjects( "@%obj@" , true );");
			%popup.addItem(%id++,"Select All Childrens (Recursive)" TAB "" TAB "Scene.selectGroupObjects( "@%obj@" , false );");
			%popup.addItem(%id++,"Unselect All Childrens" TAB "" TAB "Scene.unselectGroupObjects( "@%obj@" , true );");
			//%popup.addItem(%id++,"Remove Children from Selection" TAB "" TAB "EWorldEditor.selectAllObjectsInSet( "@%obj@" , true );");
			%popup.addItem(%id++,"Lock Childrens" TAB "" TAB "Scene.lockGroupObjects( "@%obj@" , true );");
			%popup.addItem(%id++,"Unlock Childrens" TAB "" TAB "Scene.lockGroupObjects( "@%obj@" , false );");
			%popup.addItem(%id++,"-");
			%popup.addItem(%id++,"Collapse group and Childrens" TAB "" TAB "SceneEditorTree.expandGroupRecurse( "@%obj@" , 0 );");
			%popup.addItem(%id++,"Expand group and Childrens" TAB "" TAB "SceneEditorTree.expandGroupRecurse( "@%obj@" , 1 );");

			%popup.addItem(%id++,"Set as active group" TAB "" TAB "Scene.setActiveSimGroup( %this.object );");
		}

		%popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Lock auto-arrange" TAB "" TAB "SceneEd.toggleAutoArrangeGroupLock("@%obj@");");
		%popup.autoLockId = %id;

		%popup.addItem(%id++,"Check for Light Shapes" TAB "" TAB "checkLightShapes("@%obj@");");

		if (%obj.prefabFile !$= "")
		{
			%popup.addItem(%id++,"Collapse Prefab group" TAB "" TAB "Lab.CollapsePrefab( "@%obj.getId()@");");
		}

		if (%obj.autoArrangeLocked)
			%popup.setItem(%popup.autoLockId,"Unlock auto-arrange","","SceneEd.toggleAutoArrangeGroupLock( %this.object);");
		else
			%popup.setItem(%popup.autoLockId,"Lock auto-arrange","","SceneEd.toggleAutoArrangeGroupLock( %this.object);");

		%hasChildren = %obj.getCount() > 0;
		%popup.enableItem(10, %hasChildren);
		%popup.enableItem(11, %hasChildren);
		%haveObjectEntries = true;
		%haveLockAndHideEntries = false;
	}
	// Open generic context menu.
	else
	{
		%popup.addItem(%id++,"Rename" TAB "" TAB "SceneEditorTree.showItemRenameCtrl( SceneEditorTree.findItemByObjectId( %this.object ) );");
		%popup.addItem(%id++,"Delete" TAB "" TAB "EWorldEditor.deleteMissionObject( %this.object );");
		%popup.addItem(%id++,"Inspect" TAB "" TAB "inspectObject( %this.object );");
		%popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Locked" TAB "" TAB "%this.object.setLocked( !%this.object.locked ); EWorldEditor.syncGui();");
		%popup.addItem(%id++,"Hidden" TAB "" TAB "EWorldEditor.hideObject( %this.object, !%this.object.hidden ); EWorldEditor.syncGui();");
		%popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Group" TAB "" TAB "Scene.addSimGroup( true );");

		if (%obj.isMemberOfClass("Prefab"))
		{
			%popup.addItem(%id++, "Expand Prefab to group" TAB "" TAB "Lab.ExpandPrefab( "@%obj.getId()@");");
		}

		%haveObjectEntries = true;
	}

	// Specialized version for ConvexShapes.
	if (%obj.isMemberOfClass("ConvexShape"))
	{
		%popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Convert to Zone" TAB "" TAB "EWorldEditor.convertSelectionToPolyhedralObjects( \"Zone\" );");
		%popup.addItem(%id++,"Convert to Portal" TAB "" TAB "EWorldEditor.convertSelectionToPolyhedralObjects( \"Portal\" );");
		%popup.addItem(%id++,"Convert to Occluder" TAB "" TAB "EWorldEditor.convertSelectionToPolyhedralObjects( \"OcclusionVolume\" );");
		%popup.addItem(%id++,"Convert to Sound Space" TAB "" TAB "EWorldEditor.convertSelectionToPolyhedralObjects( \"SFXSpace\" );");

	}

	// Specialized version for ConvexShapes.
	if (%obj.isMemberOfClass("TSStatic") || %obj.isMemberOfClass("ConvexShape"))
	{
		%popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Add to container" TAB "" TAB "Lab.addSelToContainer();");
		%popup.addItem(%id++,"Remove from container" TAB "" TAB "Lab.detachSelFromContainer();");
		%popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Export to COLLADA" TAB "" TAB "EditorExportToCollada();");
		%popup.addItem(%id++,"Export to COLLADA Clone (.cc.DAE)" TAB "" TAB "Lab.CreateColladaClone();");
		%popup.addItem(%id++,"-");

		if (%this.getSelectedItemsCount() > 1)
			%popup.addItem(%id++,"Convert selected TSStatic to TSPathShape" TAB "" TAB "Scene.convertSelectionToPathShape();");
		else
			%popup.addItem(%id++,"Convert as TSPathShape" TAB "" TAB "Scene.convertToPathShape(%this.object);");

	}

	// Specialized version for polyhedral objects.
	else if (%obj.isMemberOfClass("Zone") || %obj.isMemberOfClass("Portal") || %obj.isMemberOfClass("OcclusionVolume") || %obj.isMemberOfClass("SFXSpace"))
	{
		%popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Convert to ConvexShape" TAB "" TAB "EWorldEditor.convertSelectionToConvexShape();");
	}

	if(%obj.isMemberOfClass("Entity"))
	{

		%popup.additem( %id++,"-");

		%popup.additem( %id++, "Convert to Game Object" TAB "" TAB "Scene.createGameObject( %this.object );");
		%popup.additem( %id++,"Duplicate Game Object" TAB "" TAB "EWorldEditor.duplicateGameObject( %this.object );");
		%popup.additem( %id++, "Show in Asset Browser" TAB "" TAB "EWorldEditor.showGameObjectInAssetBrowser( %this.object );");

		%popup.enableItem(%id-2, true);
		%popup.enableItem(%id-1, false);
		%popup.enableItem(%id, false);
		/*
		if( !isObject( GameObjectPopup ) )
			%popup = new PopupMenu( GameObjectPopup : ETSimGroupContextPopup )
		{
			item[ %id++ ] = "-";
			item[ %id++ ] = "Convert to Game Object" TAB "" TAB "Scene.createGameObject( %this.object );";
			item[ %id++ ] = "Duplicate Game Object" TAB "" TAB "EWorldEditor.duplicateGameObject( %this.object );";
			item[ %id++ ] = "Show in Asset Browser" TAB "" TAB "EWorldEditor.showGameObjectInAssetBrowser( %this.object );";
		};

		if(!isObject(AssetDatabase.acquireAsset(%obj.gameObjectAsset)))
		{
			GameObjectPopup.enableItem(13, true);
			GameObjectPopup.enableItem(14, false);
			GameObjectPopup.enableItem(15, false);
		}
		else
		{
			GameObjectPopup.enableItem(14, false);
			GameObjectPopup.enableItem(15, true);
			GameObjectPopup.enableItem(16, true);
		}
		*/
	}

// Handle multi-selection.
	if (%this.getSelectedItemsCount() > 1)
	{
		%popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Selection Options");
		%popup.addItem(%id++,"-");
		%popup.addItem(%id++,"Delete Selection" TAB "" TAB "EditorMenuEditDelete();");
		%popup.addItem(%id++,"Group Selection" TAB "" TAB "Scene.addSimGroup( true );");

		%popup.addItem(%id++,"Generate Light from Selection" TAB "" TAB "SceneEd.genAutoLightSelection();");
	}

	if (%haveObjectEntries)
	{
		%popup.enableItem(0, %obj.isNameChangeAllowed() && %obj.getName() !$= "MissionGroup");
		%popup.enableItem(1, %obj.getName() !$= "MissionGroup");

		if (%haveLockAndHideEntries)
		{
			%popup.checkItem(4, %obj.locked);
			%popup.checkItem(5, %obj.hidden);
		}

		%popup.enableItem(7, %this.isItemSelected(%itemId));
	}

	%popup.addItem(%id++,"Cancel and close menu" TAB "" TAB "info(\"Action cancelled\");");
	%popup.showPopup(Canvas);
}
