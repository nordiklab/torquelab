//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function SceneObjectsTree::expandGroup(%this, %simGroup,%expand)
{
	if (%expand $= "")
		%expand = 1;

	%item	= SceneEditorTree.findItemByObjectId(%simGroup);

	if (%item $= "-1")
		return;

	%this.expandItem(%item,%expand);
	%simGroup.setIsExpanded(%expand);
	%this.buildVisibleTree();
}
//------------------------------------------------------------------------------

//==============================================================================
function SceneObjectsTree::expandGroupRecurse(%this, %simGroup,%expand,%sublevel)
{

	if (%expand $= "")
		%expand = 1;

	$Expanded = 0;

	foreach(%obj in %simGroup)
	{
		if (  %obj.getCount() > 0)
			%this.expandSubGroupRecurse(%obj,%expand,1);
	}

	$Expanded++;
	%this.expandGroup(%simGroup,%expand);

	if (%expand)
		info($Expanded,"group expanded");
	else
		info($Expanded,"group collapsed");
}
//------------------------------------------------------------------------------

//==============================================================================
function SceneObjectsTree::expandSubGroupRecurse(%this, %simGroup,%expand,%sublevel)
{
	foreach(%obj in %simGroup)
	{
		if (  %obj.getCount() > 0)
			%this.expandSubGroupRecurse(%obj,%expand,%sublevel++);
	}

	$Expanded++;
	%this.expandGroup(%simGroup,%expand);

}
//------------------------------------------------------------------------------

//==============================================================================
//SceneEditorTree.expandSelection(1);
function SceneObjectsTree::expandSelection(%this,%expand)
{
	if (%expand $= "")
		%expand = 1;

	%group = %this.getSelectedObject(0);

	if (!isObject(%group))
		return;

	%this.expandGroupRecurse(%group,%expand);
}
//------------------------------------------------------------------------------

//==============================================================================
// Set object selected in scene (Trees and WorldEditor)
//==============================================================================

//==============================================================================
function SceneObjectsTree::onAddGroupSelected(%this, %simGroup)
{

	Scene.setActiveSimGroup(%group);
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneObjectsTree::onAddMultipleSelectionBegin(%this)
{
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneObjectsTree::onAddMultipleSelectionEnd(%this)
{
}
//------------------------------------------------------------------------------
