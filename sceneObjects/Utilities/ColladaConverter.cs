//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
//Scene.ConvertSelectionToCollada(true);
function Scene::ConvertSelectionToCollada(%this,%autoNaming,%multipleDae)
{
	//Get first object in selection which have a file
	if (%autoNaming)
		%baseFile = %this.autoColladaFileFromSel(true);

	%exportFile = Scene.getValidColladaFile(%baseFile);

	if (!%multipleDae)
	{
		EWorldEditor.colladaExportSelection(%exportFile);
		return;
	}

	foreach$(%obj in Scene.getWorldSelList())
	{
		EWorldEditor.clearSelection();
		EWorldEditor.selectObject(%obj);
		%suffix = %obj.getDisplayName();
		%file = strreplace(%exportFile,fileBase(%exportFile),fileBase(%exportFile)@"_"@%suffix);
		EWorldEditor.colladaExportSelection(%file);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
function Scene::ExportToCollada(%this,%exportToFile)
{
	%exportFile = Scene.getValidColladaFile(%exportToFile);

	if (Lab.currentEditor.getId() == ShapeLabPlugin.getId())
		ShapeLabEditor.exportToCollada(%exportFile);
	else
		EWorldEditor.colladaExportSelection(%exportFile);

}
//------------------------------------------------------------------------------

//==============================================================================
function Scene::getValidColladaFile(%this,%exportToFile)
{
	if (%exportToFile $= "")
	{
		%exportToFile = getSaveFile( "Collada Files(*.DAE)|*.DAE| Collada T3D Clones(*.cc.DAE)|*.cc.DAE|",$Cfg_ColladaExport_LastFile);

		if (%exportToFile $= "")
		{
			warnLog("Invalid file selected:",%exportToFile);
			return;
		}
	}

	if (fileExt(%exportToFile) !$= ".dae")
		%exportToFile = %exportToFile @ ".dae";

	%exportFile = getUniqueFilename(%exportToFile);
	return %exportFile;
}
//------------------------------------------------------------------------------

//==============================================================================
function Scene::autoColladaFileFromSel(%this,%asClone)
{
	foreach$(%obj in Scene.getWorldSelList())
	{
		//Simple look for the first ShapeName
		if (%obj.shapeName $= "")
			continue;

		%baseFile = %obj.shapeName;
		break;
	}

	if (%baseFile $= "")
		return "";

	%ext = ".DAE";

	if (%asClone)
		%ext = ".cc.DAE";

	%autoFile = addFilenameToPath(filePath(%baseFile),fileBase(%baseFile),%ext);

	return %autoFile;
}
//------------------------------------------------------------------------------
