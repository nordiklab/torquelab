//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$SceneEditor_AddObjectLastZ = true;
$SceneEditor_AddObjectCurrentSel = true;

$Cfg_Creator_TSStatic_Fields = "AllowPlayerStep";
$Cfg_Creator_TSStatic_AllowPlayerStep = true;
//==============================================================================
// Object Creation Callbacks
//==============================================================================

//==============================================================================
function Scene::addObjectToScene(%this, %obj)
{
	%addToGroup = Scene.getActiveSimGroup();

	if (!%addToGroup.isMember(%obj))
		%addToGroup.add(%obj);

}
//------------------------------------------------------------------------------

//==============================================================================
function Scene::onObjectCreated(%this, %objId,%noDrop,%getTransform)
{

	// Can we submit an undo action?
	if (isObject(%objId))
		MECreateUndoAction::submit(%objId);

	if (%getTransform)
	{
		//%objId.position = %this.getCreateObjectPosition();
		%transform = Scene.getCreateObjectTransform();
		%position = getWords(%transform,0,2);
		%rotation = getWords(%transform,3,6);
		%objId.position = %position;
		%objId.rotation = %rotation;
		//flush new position
		%objId.setTransform(%transform);		
	}

	Scene.addObjectToScene(%objId);
	SceneEditorTree.clearSelection();
	EWorldEditor.clearSelection();
	EWorldEditor.selectObject(%objId);

	if (getWordCount(%transform) > 6)
		%objId.schedule(10,setTransform,%transform);
	else if (!%noDrop)
		EWorldEditor.dropSelection(true);
}
//------------------------------------------------------------------------------
//==============================================================================
function Scene::postCreateObjectCheck(%this, %objId)
{

	EWorldEditor.clearSelection();
	EWorldEditor.selectObject(%objId);
}
//------------------------------------------------------------------------------
//==============================================================================
function Scene::doObjDropping(%this, %objId)
{
	%this.onObjectCreated(%objId,false,true);

}
//------------------------------------------------------------------------------
//==============================================================================
//Scene.createSimGroup();
function Scene::createSimGroup(%this)
{
	if (!$missionRunning)
		return;

	%addToGroup = Scene.getActiveSimGroup();
	%intName = (%addToGroup.internalName $= "") ? "SceneGroup" : %addToGroup.internalName;
	%objId = new SimGroup()
	{
		internalName = getUniqueInternalName(%intName, MissionGroup, true);
		//position = %this.getCreateObjectPosition(); //SimGroup with position???
		parentGroup = %addToGroup;
	};
}
//------------------------------------------------------------------------------
//==============================================================================
function Scene::createStatic(%this, %file,%transform)
{
	if (!$missionRunning)
		return;

	%objId = new TSStatic()
	{
		shapeName = %file;
	};

	if (%transform !$= "")
	{
		$SceneEd_SkipEditorDrop = true;
		%skipTransform = true;
		%objId.position = getWords(%transform,0,2);
		%objId.rotation = getWords(%transform,3,6);
		%objId.setTransform(%transform);
	}

	%this.onObjectCreated(%objId,$SceneEd_SkipEditorDrop,!%skipTransform);
}
//------------------------------------------------------------------------------

//==============================================================================
function Scene::convertSelectionToPathShape(%this, %tsstatic)
{
	foreach$(%id in SceneEditorTree.getSelectedObjectList())
	{

		if (%id.getClassName() $= "TSStatic")
			%this.convertToPathShape(%id);

	}
}
//------------------------------------------------------------------------------
//==============================================================================
function Scene::convertToPathShape(%this, %tsstatic)
{
	if (!isObject(%tsstatic))
		return;

	%file = %tsstatic.shapeName;

	if (!isFile(%file))
		return;

	%objId = new TSPathShape()
	{
		shapeName = %file;
		position = %tsstatic.position;
		rotation = %tsstatic.rotation;
		scale = %tsstatic.scale;
		parentGroup = %tsstatic.parentGroup;
	};

	if (isObject(%objId))
		%tsstatic.delete();

	MissionGroup.add(%objId);
	%this.onObjectCreated(%objId,true,false);
}

//==============================================================================
function Scene::createPrefab(%this, %file)
{
	if (!$missionRunning)
		return;

	%objId = new Prefab()
	{
		filename = %file;
	};

	if (isObject(%objId))
		%this.onObjectCreated(%objId,false,true);

	//%this.onObjectCreated( %objId );
}
//------------------------------------------------------------------------------
//==============================================================================
function Scene::createObject(%this, %cmd)
{
	if (!$missionRunning)
		return;

	pushInstantGroup();
	%objId = eval(%cmd);
	popInstantGroup();

	if (isObject(%objId))
		Scene.onObjectCreated(%objId,$SceneEd_SkipEditorDrop,true);

	return %objId;
}
//------------------------------------------------------------------------------
//==============================================================================
function Scene::createMesh(%this, %file)
{
	if (!$missionRunning)
		return;

	%file = strReplace(%file,"//","/");
	%objId = new TSStatic()
	{
		shapeName = %file;
		internalName = fileBase(%file);
		allowPlayerStep = $Cfg_TSStatic_Default_AllowPlayerStep;
	};

	//Check for predefine settings added as a node of nodecfg
	checkShapeNodeCfg(%objId);

	%this.onObjectCreated(%objId,$SceneEd_SkipEditorDrop,true);
	return %objId;
}
function Scene::createGameObject( %this, %entity )
{
	GameObjectCreatorName.text = "";
	GameObjectCreator.selectedEntity = %entity;

	Canvas.pushDialog(GameObjectCreator);
}
//------------------------------------------------------------------------------
function genericCreateObject(%class)
{
	if (!isClass(%class))
	{
		warn("createObject( " @ %class @ " ) - Was not a valid class.");
		return;
	}

	%cmd = "return new " @ %class @ "();";
	%obj = Scene.createObject(%cmd);
	// In case the caller wants it.
	return %obj;
}

