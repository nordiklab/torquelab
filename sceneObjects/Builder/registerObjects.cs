//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Register Objects for use with Creator UI
//------------------------------------------------------------------------------
//  Scene.registerMissionObject(%class,%display,%buildFunc,%groups);
//    %class -> Classname of object to build
//    %display -> Text displayed in creator UI
//    %buildFunc -> Custom build function (only custom naming) ObjectBuilder will
//                   look for ObjectBuilderGui.build[buildFunc]
//    %groups -> Creator groups in which the registered object will be added
//==============================================================================
//==============================================================================
//Scene.registerObjects();
function Scene::registerObjects(%this)
{
	if (!isObject(%this.array))
	{
		%this.array = new ArrayObject();
		%this.array.caseSensitive = true;
	}

	%this.array.empty();
	//===========================================================================
	// New grouping method allowing multiple groups
	// %this.registerMissionObject(CLASSNAME, DISPLAYNAME ,buildCUSTOMFUNC, CREATORGROUPS);
	//---------------------------------------------------------------------------
	%this.registerMissionObject("BotSpawnMarker",   "Bot Spawn Sphere",  "",   "AI Bot");
	%this.registerMissionObject("BotGroupMarker",   "Bot Group Marker",  "",   "AI Bot");
	%this.registerMissionObject("BotMarker",        "Bot Marker",        "",   "AI Bot");
	%this.registerMissionObject("BotGoalMarker",    "Bot Goal Marker",   "",   "AI Bot");

	%this.registerMissionObject("NavMesh",    "Navigation mesh",   "",         "AI Level");
	%this.registerMissionObject("NavPath",    "Path",              "",         "AI Level");
	%this.registerMissionObject("Path",       "Path Group",        "",         "AI Level");
	%this.registerMissionObject("Marker",     "Path Node",         "",         "AI Level");

	//==== Environment Group ====
	%this.registerMissionObject("SkyBox",        "Sky Box",        "",         "Environment");
	%this.registerMissionObject("SkyLine",       "Sky Line",       "",         "Environment");
	%this.registerMissionObject("CloudLayer",    "Cloud Layer",    "",         "Environment");
	%this.registerMissionObject("BasicClouds",   "Basic Clouds",   "",         "Environment");
	%this.registerMissionObject("ScatterSky",    "Scatter Sky",    "",         "Environment");
	%this.registerMissionObject("Sun",           "Basic Sun",      "",         "Environment");
	%this.registerMissionObject("Lightning",     "Lightning",      "",         "Environment");
	%this.registerMissionObject("WaterBlock",    "Water Block",    "",         "Environment");
	%this.registerMissionObject("SFXEmitter",    "Sound Emitter",  "",         "Environment");
	%this.registerMissionObject("Precipitation", "Precipitation",  "",         "Environment");

	%this.registerMissionObject("RibbonNode",       "Ribbon",         "",         "Environment");
	%this.registerMissionObject("PointLight",       "Point Light",    "",         "Environment");
	%this.registerMissionObject("SpotLight",        "Spot Light",     "",         "Environment");
	%this.registerMissionObject("GroundCover",      "Ground Cover",   "",         "Environment");
	%this.registerMissionObject("TerrainBlock",     "Terrain Block",  "",         "Environment");
	%this.registerMissionObject("GroundPlane",      "Ground Plane",   "",         "Environment");
	%this.registerMissionObject("WaterPlane",       "Water Plane",    "",         "Environment");
	%this.registerMissionObject("PxCloth",          "Cloth",          "",         "Environment");
	%this.registerMissionObject("ForestWindEmitter","Wind Emitter",   "",         "Environment");
	%this.registerMissionObject("DustEmitter",      "Dust Emitter",   "",         "Environment");
	%this.registerMissionObject("DustSimulation",   "Dust Simulation","",         "Environment");
	%this.registerMissionObject("DustEffecter",     "Dust Effecter",  "",         "Environment");

	//==== Environment Group ====
	//Ported to new system
	/*%this.beginGroup("Environment");
	// Removed Prefab as there doesn't really seem to be a point in creating a blank one
	//%this.registerMissionObject( "Prefab",              "Prefab" );

	//%this.registerMissionObject( "ParticleEmitterNode", "Particle Emitter" );
	%this.registerMissionObject("RibbonNode", "Ribbon");
	// Legacy features. Users should use Ground Cover and the Forest Editor.
	//%this.registerMissionObject( "fxShapeReplicator",   "Shape Replicator" );
	//%this.registerMissionObject( "fxFoliageReplicator", "Foliage Replicator" );
	%this.registerMissionObject("PointLight",          "Point Light");
	%this.registerMissionObject("SpotLight",           "Spot Light");
	%this.registerMissionObject("GroundCover",         "Ground Cover");
	%this.registerMissionObject("TerrainBlock",        "Terrain Block");
	%this.registerMissionObject("GroundPlane",         "Ground Plane");
	%this.registerMissionObject("WaterPlane",          "Water Plane");
	%this.registerMissionObject("PxCloth",             "Cloth");
	%this.registerMissionObject("ForestWindEmitter",   "Wind Emitter");
	%this.registerMissionObject("DustEmitter", "Dust Emitter");
	%this.registerMissionObject("DustSimulation", "Dust Simulation");
	%this.registerMissionObject("DustEffecter", "Dust Effecter");

	%this.endGroup();
	*/
	//---- END OF Environment Group ----

	//==== AI Group ====
	//%this.beginGroup("AI");
	//%this.endGroup();
	//---- END OF AI Group ----

	//==== Shapes Group ====
	%this.beginGroup("Shapes");
	%this.registerMissionObject("PathShape", "Path Shape");
	%this.registerMissionObject("TSDynamic", "Dynamic Shape");
	%this.endGroup();

	//---- END OF AI Group ----

	//==== Shapes Group ====

	%this.beginGroup("Navigation");

	%this.endGroup();
	%this.beginGroup("Level");
	%this.registerMissionObject("MissionArea",  "Mission Area");
	//%this.registerMissionObject("Path");
	%this.registerMissionObject("Marker",       "Path Node");
	%this.registerMissionObject("Trigger");
	%this.registerMissionObject("PhysicalZone", "Physical Zone");
	%this.registerMissionObject("Camera");
	%this.registerMissionObject("LevelInfo",    "Level Info");
	%this.registerMissionObject("TimeOfDay",    "Time of Day");
	%this.registerMissionObject("Zone",         "Zone");
	%this.registerMissionObject("Portal",       "Zone Portal");
	%this.registerMissionObject("SpawnSphere",  "Player Spawn Sphere", "PlayerDropPoint");
	%this.registerMissionObject("SpawnSphere",  "Observer Spawn Sphere", "ObserverDropPoint");
	%this.registerMissionObject("SFXSpace",      "Sound Space");
	%this.registerMissionObject("OcclusionVolume", "Occlusion Volume");
	%this.registerMissionObject("LightProbeVolume", "Light Probe Volume");
	%this.registerMissionObject("NavMesh", "Navigation mesh");
	%this.registerMissionObject("NavPath", "Path");
	%this.registerMissionObject("EnvVolume", "Env Volume");
	%this.registerMissionObject("AccumulationVolume", "Accumulation Volume");

	%this.registerMissionObject( "ReflectionProbe",       "Reflection Probe" );

	if (%this.isMethod("registerCustomsObjectsGroup"))
		%this.registerCustomsObjectsGroup("Level");

	%this.endGroup();

	%this.beginGroup("Assets");
	%this.registerMissionObject( "Entity",       "Entity" );
	%this.endGroup();
	%this.beginGroup("Ambient");
	%this.registerMissionObject("SkyBox",              "Sky Box");
	%this.registerMissionObject("SkyLine", "Sky Line");
	%this.registerMissionObject("CloudLayer",          "Cloud Layer");
	%this.registerMissionObject("BasicClouds",         "Basic Clouds");
	%this.registerMissionObject("ScatterSky",          "Scatter Sky");
	%this.registerMissionObject("Sun",                 "Basic Sun");
	%this.registerMissionObject("TerrainBlock",        "Terrain Block");
	%this.endGroup();
	// andrewmac: PhysX 3.3
	%this.beginGroup("PhysX 3.3");
	%this.registerMissionObject("Px3Cloth",        "Cloth Plane");
	%this.registerMissionObject("Px3Static",       "Static Shape");
	%this.endGroup();
	%this.beginGroup("System");
	%this.registerMissionObject("SimGroup");
	%this.endGroup();
	%this.beginGroup("ExampleObjects");
	%this.registerMissionObject("RenderObjectExample");
	%this.registerMissionObject("RenderMeshExample");
	%this.registerMissionObject("RenderShapeExample");
	%this.endGroup();

	if (%this.isMethod("registerCustomsGroups"))
		%this.registerCustomsGroups();
}

function Scene::registerMissionObject(%this, %class, %name, %buildfunc, %groups)
{
	if (!isClass(%class))
		return;

	if (%name $= "")
		%name = %class;

	if (%this.currentGroup !$= "" && %groups $= "")
		%groups = %this.currentGroup;

	if (%class $= "" || %groups $= "")
	{
		warn("Scene::registerMissionObject, invalid parameters!", %class, %name, %buildfunc, %groups);
		return;
	}

	if (!isObject(%this.array))
	{
		warnLog("registerMissionObject called without ArrayObject to hold data","Creating Scene ArrayObject for registered objects");

		if (!isObject(%this.array))
		{
			%this.array = new ArrayObject();
			%this.array.caseSensitive = true;
		}
	}

	%args = new ScriptObject();
	%args.val[0] = %class;
	%args.val[1] = %name;
	%args.val[2] = %buildfunc;

	//Support multiple groups
	foreach$(%group in %groups)
		%this.array.push_back(%group, %args);

}
//==============================================================================
function Scene::beginGroup(%this, %group)
{
	%this.currentGroup = %group;
}
//------------------------------------------------------------------------------
//==============================================================================
function Scene::endGroup(%this, %group)
{
	%this.currentGroup = "";
}
//------------------------------------------------------------------------------
