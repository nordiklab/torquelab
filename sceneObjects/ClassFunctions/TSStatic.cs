//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function TSStatic::onAdd(%this, %group)
{
	strReplace(%this.shapeName,"//","/");
	info("TSStatic::onAdd",%this,%group,%this.getParent());
}
//------------------------------------------------------------------------------
