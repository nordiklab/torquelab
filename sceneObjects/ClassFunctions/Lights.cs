//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function Lab::toggleSelectLights(%this)
{
	%this.canSelectLights(!Lab.selectableLights);
}
//==============================================================================
function Lab::canSelectLights(%this,%selectable)
{
	if (%selectable $= "")
		%selectable = true;

	Lab.selectableLights = %selectable;

	if (%selectable)
		EWorldEditor.removeClassIgnore("PointLight");
	else
		EWorldEditor.addClassIgnore("PointLight");
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::showLights(%this,%visible,%group)
{
	if (%visible $= "")
		%visible = true;

	ObjSet_PointLight.callOnChildren("setHidden",!%visible);

	foreach$(%icon in $LabLightToggleIcons)
		%icon.setToggleState(%visible);

	// Lab.evalToolIcon("toggleLights","setToggleState("@%visible@");");

}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::shrinkLights(%this,%shrinked,%group)
{
	if (%shrinked $= "")
		%shrinked = true;

	%lights = getMissionObjectClassList("PointLight");
	%shrinkRadius = "0.1";

	if (%shrinked)
	{
		foreach$(%id in %lights)
		{
			if (%id.radius !$= %shrinkRadius)
				$LabShrinkedLightDefault[%id] = %id.radius;

			//%id.doAddInspect(%id);
			//%id.setFieldValue("radius",%shrinkRadius);
		}

		LabObj.updateList(%lights,"radius",%shrinkRadius);
		return;
	}

	foreach$(%id in %lights)
	{
		if ($LabShrinkedLightDefault[%id] !$= "")
			LabObj.set(%id,"radius",$LabShrinkedLightDefault[%id]);

		//%id.setFieldValue("radius",$LabShrinkedLightDefault[%id]);

		$LabShrinkedLightDefault[%id] = "";
	}

}
//------------------------------------------------------------------------------

//==============================================================================
// Set appropriate tooltip and store list of same purpose icons
function LightToggleIcon::onAdd(%this)
{
	$LabLightToggleIcons = strAddWord($LabLightToggleIcons,%this.getId());

}
//------------------------------------------------------------------------------

//==============================================================================
// Set appropriate tooltip and store list of same purpose icons
function LightToggleIcon::setToggleState(%this,%value)
{
	ToolbarIcon::setToggleState(%this,%value);

	if (%this.textLocation $= "NONE")
		return;

	%text = (%value) ? "Turn OFF" : "Turn ON";
	%this.setText(%text);
	return;

	if (%value $= "")
		%value = %this.getValue();

	if (%value)
		%stateFileCheck = strreplace(%this.iconBitmap,"_off","_on");
	else
		%stateFileCheck = strreplace(%this.iconBitmap,"_on","_off");

	if (isImageFile(%stateFileCheck))
		%this.setBitmap(%stateFileCheck);

}
//------------------------------------------------------------------------------
