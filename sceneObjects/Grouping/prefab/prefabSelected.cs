//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function Scene::onPrefabSelected(%this,%prefab)
{
	warnLog("Scene::onPrefabSelected:",%prefab);
	%name = fileBase(%prefab.filename);
	%path = filePath(%prefab.filename);
	LabPrefabSelectedName.setText(%name);
	LabPrefabSelectedFolder.setText(%path);
	Scene.selectedPrefab = %prefab;
	SEP_PrefabUnpackIcon.visible = 1;
	//SceneEd_PrefabInfoCtrl.visible = 1;
}

function Scene::noPrefabSelected(%this)
{
	warnLog("Scene::noPrefabSelected:");

}

function LabPrefabSelectedName::onValidate(%this)
{
	warnLog("Current prefab name changed to:",%this.getText());
}

function Scene::unpackSelectedPrefab(%this)
{
	warnLog("Current prefab name changed to:",%this.getText());

	if (!isObject(Scene.selectedPrefab))
	{
		Scene.noPrefabSelected();
		return;
	}

	%success = Lab.ExpandPrefab(Scene.selectedPrefab);

	if (%success)
		Scene.noPrefabSelected();
}
function SceneEd::unpackSelectedPrefab(%this)
{
	warnLog("OLD SceneEd unpackSelectedPrefab redirected to new Scene method");
	Scene.unpackSelectedPrefab();
}
