//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$Scene::AutoPrefabFolder = "art/models/prefabs";
//==============================================================================
//CREATE PREFAB FROM SELECTION
//==============================================================================

//==============================================================================
// New Create Prefab System using Lab
function Lab::GetAutoPrefabFile()
{
	%missionFolder = filePath(MissionGroup.getFilename());
	%firstObj = EWorldEditor.getSelectedObject(0);
	%mode = $Scene::AutoPrefabMode;

	if (%mode $= "")
		%mode = "Folder";

	if (%firstObj.isMemberOfClass(TSStatic))
	{
		%name = fileBase(%firstObj.shapeName);
	}

	if (%name $= "")
	{
		if (%firstObj.getName() !$="")
			%name = %firstObj.getName();
		else if (%firstObj.internalName !$="")
			%name = %firstObj.internalName;
		else
			%name = %firstObj;
	}

	switch$(%mode)
	{
		case "Auto":
			%prefabPath = $Scene::AutoPrefabFolder;
			%fileTmp = %prefabPath@"/"@%name@".prefab";

		case "Folder":
			%prefabPath = $Scene::AutoPrefabFolder;
			%fileTmp = %prefabPath@"/"@%name@".prefab";

		case "Object":
			%prefabPath = filePath(%firstObj.shapeName);
			%fileTmp = %prefabPath@"/"@%name@".prefab";
	}

	%file = strreplace(%fileTmp,"//","/");

	while(isFile(%file))
	{
		%uniqueName = %name@"_"@%inc++;
		%fileTmp = %prefabPath@"/"@%uniqueName@".prefab";
		%file = strreplace(%fileTmp,"//","/");
	}

	return %file;
}
//------------------------------------------------------------------------------
//==============================================================================
// New Create Prefab System using Lab
function Lab::GetPrefabFile()
{
	%dlg = new SaveFileDialog()
	{
		Filters        = "Prefab Files (*.prefab)|*.prefab|";
		DefaultPath    = $Pref::WorldEditor::LastPath;
		DefaultFile    = "";
		ChangePath     = false;
		OverwritePrompt   = true;
	};
	%ret = %dlg.Execute();

	if (%ret)
	{
		$Pref::WorldEditor::LastPath = filePath(%dlg.FileName);
		%saveFile = %dlg.FileName;
	}

	if (fileExt(%saveFile) !$= ".prefab")
		%saveFile = %saveFile @ ".prefab";

	%dlg.delete();

	if (!%ret)
		return "";

	return %saveFile;
}
//------------------------------------------------------------------------------

//==============================================================================
//EXPLODED SELECTED PREFAB
//==============================================================================
//==============================================================================
// New Create Prefab System using Lab
function Lab::ExplodePrefab(%this)
{
	//echo( "EditorExplodePrefab()" );
	Scene.addNextGroupTo = EWorldEditor.getSelectedObject(0).getGroup();
	Scene.lastExplodedName = fileBase(EWorldEditor.getSelectedObject(0).fileName);
	EWorldEditor.explodeSelectedPrefab();
	SceneEditorTree.buildVisibleTree(true);
}
//------------------------------------------------------------------------------

//==============================================================================
// Default T3D Prefab function for backward compatibility
//==============================================================================
//==============================================================================
function EditorMakePrefab()
{
	Lab.CreatePrefab();
}

function EditorExplodePrefab()
{
	Lab.ExplodePrefab();
}
//------------------------------------------------------------------------------
