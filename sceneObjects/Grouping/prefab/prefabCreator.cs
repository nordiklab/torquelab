//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// New Create Prefab System using Lab
function Lab::CreatePrefab(%this,%file)
{
	// Should this be protected or not?
	%autoMode = $SceneEd::AutoCreatePrefab;

	if (%file !$= "")
	{
		%saveFile = %file;
	}
	else if (%autoMode)
	{
		%saveFile = %this.GetAutoPrefabFile();
	}
	else if (!$Pref::disableSaving )
	{
		%saveFile = %this.GetPrefabFile();
	}

	if (%saveFile $= "")
		return;

	EWorldEditor.makeSelectionPrefab(%saveFile);

	//Add new prefab to active group if exist (Must get previous id)
	Scene.addObjectToScene($ThisPrefab.getId()-1);

	SceneEditorTree.buildVisibleTree(true);
}
//------------------------------------------------------------------------------
// Lab.createObjectPrefab();
// Lab.getCurrentPrefabFolder();
// Lab.getCurrentPrefabName();
//==============================================================================
function Lab::createObjectPrefab(%this)
{
	%file = LabPrefabCreatorFolder.getText() @"/"@LabPrefabCreatorName.getText()@".prefab";
	%saveFile = strReplace(%file,"//","/");
	Lab.CreatePrefab(%saveFile);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::getCurrentPrefabFolder(%this)
{
	%firstObj = EWorldEditor.getSelectedObject(0);
	%file = %firstObj.shapeName;
	%folder = filePath(%file);
	LabPrefabCreatorFolder.setText(%folder);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::getPrefabFolder(%this)
{

	getFolderName("","Lab.setPrefabFolder","art/models/");

}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::setPrefabFolder(%this,%folder)
{

	%folder = filePath(%folder);
	LabPrefabCreatorFolder.setText(%folder);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::getCurrentPrefabName(%this)
{
	%firstObj = EWorldEditor.getSelectedObject(0);
	%file = %firstObj.shapeName;
	%fileName = fileBase(%file);
	LabPrefabCreatorName.setText(%fileName);
}
//------------------------------------------------------------------------------
//==============================================================================
