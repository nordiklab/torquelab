//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function Lab::changePrefabFile(%this,%prefab,%file)
{
	if (!isObject(%prefab))
		return;

	LabObj.set(%prefab,"fileName",%file);
}
//------------------------------------------------------------------------------
