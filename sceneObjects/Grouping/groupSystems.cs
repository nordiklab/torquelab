//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
%setId = -1;

//==============================================================================
// Set object selected in scene (Trees and WorldEditor)
//==============================================================================

//==============================================================================
function Lab::initLabObjectSets(%this)
{
	//Root Simgroup holding the Layers Simset
	if (!isObject(LabObjectGroup))
		new SimGroup(LabObjectGroup);

	if (!isObject(ObjSetGroup_Class))
		newSimGroup("ObjSetGroup_Class",LabObjectGroup,"Object Class","ObjectSetGroup");

	if (!isObject(ObjSetGroup_Collection))
		newSimGroup("ObjSetGroup_Collection",LabObjectGroup,"Collection","ObjectSetGroup");

	%this.objSetGroup["class"] = ObjSetGroup_Class;
	%this.objSetGroup["collection"] = ObjSetGroup_Collection;

	//Default Layer SimSet for all objects not in layer
	if (!isObject(ObjSet_Default))
		%this.addObjectSet("Default","");

	SEP_ObjectSetMenu.clear();
	SEP_ObjectSetMenu.setText("[Select Object Set]");
	Lab.prepareObjectSets();
	Lab.initLabLayerSets();

	SceneSetTree.objMode();

}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::prepareObjectSets(%this)
{
	SceneEd.activeLayer = "";
	%this.buildMissionGroupSets(MissionGroup);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::buildMissionGroupSets(%this,%simGroup,%depth)
{
	if (%depth $= "")
		%depth = 0;

	//Prevention in case something went wrong (20 parents from MissionGroup)
	if (%depth >20)
		return;

	foreach(%child in %simGroup)
	{
		Lab.checkSetForObject(%child);

		//if (%child.getClassName() $= "SimGroup")
		if (%child.getCount() > 0)
			%this.buildMissionGroupSets(%child,%depth+1);
	}

}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::prepareNewObject(%this,%obj)
{
	if (!isObject(ObjSet_Default))
	{
		warnLog("PrepareNewObject called while TorqueLab world not loaded");
		return;
	}

	if (%obj.getClassName() $= "SimGroup" && isObject(Scene.addNextGroupTo))
	{
		Scene.addNextGroupTo.add(%obj);
		%obj.internalName = Scene.lastExplodedName;
		Scene.lastExplodedName = "";
		Scene.addNextGroupTo = "";
	}

	//We must add it to Default object set and also verify automatic sets
	Lab.checkSetForObject(%obj);
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::checkSetForObject(%this,%obj)
{
	if (%obj.getClassName() !$= "SimGroup")
		ObjSet_Default.add(%obj);

	%classSet = "ObjSet_"@%obj.getClassName();

	if (!isObject(%classSet))
		%classSet = Lab.addObjectSet(%obj.getClassName(),"class");

	if (isObject(%classSet))
		%classSet.add(%obj);

	if (isObject(SceneEd.activeLayer))
		SceneEd.activeLayer.add(%obj);
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::saveObjectSets(%this)
{
	delObj(MissionLayers);

	newScriptObject(MissionLayers);
	//MissionGroup.pushToBack(MissionLayers);

	foreach(%layer in ObjSet_Layers)
	{
		//Skip default layers as all object with no layers are assigned to it
		if (%layer.layerId $= "-1")
			continue;

		MissionLayers.layer[%layer.layerId] = %layer.internalName;

		if (%layer.layers !$= "")
			MissionLayers.parentLayer[%layer.layerId] = %layer.layers;

	}

	%file = strreplace($Server::MissionFile,".mis",".layers");
	MissionLayers.save(%file);
}
//------------------------------------------------------------------------------
