//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$SceneSetTree_ClassGroup_["Object"] = "FlyingVehicle WheeledVehicle StaticShape RigidShape Item HoverVehicle Debris TSStatic TurretShape SceneObject Vehicle AITurretShape ConvexShape ProximityMine Prefab ShapeBase PhysicsDebris PhysicsShape";
$SceneSetTree_ClassGroup_["Environment"] = "Forest Lightning River Sun WaterPlane WaterBlock VolumetricFog GroundCover  ScatterSky SkyBox Precipitation VolumetricFogRTManager WaterObject fxFoliageReplicator AccumulationVolume BasicClouds CloudLayer GroundPlane ForestWindEmitter TerrainBlock TimeOfDay";
$SceneSetTree_ClassGroup_["Light"] = "SpotLight LightBase PointLight";
$SceneSetTree_ClassGroup_["Zone"] = "Zone PhysicalZone PhysicsForce OcclusionVolume Trigger Portal VehicleBlocker";
$SceneSetTree_ClassGroup_["Player"] = "Player SpawnSphere Projectile Camera CameraBookmark ";
$SceneSetTree_ClassGroup_["AI"] = "PathCamera CoverPoint NavMesh AIPlayer NavPath";
$SceneSetTree_ClassGroup_["FX"] = "Splash Explosion ParticleEmitter Ribbon RibbonNode SFXEmitter SFXSpace";
$SceneSetTree_ClassGroup_["Rare"] = "GameBase ScopeAlwaysShape RenderMeshExample	RenderObjectExample RenderShapeExample fxShapeReplicatedStatic	fxShapeReplicator";
$SceneSetTree_ClassGroup_["Mission"] = "Marker WayPoint MissionMarker DecalRoad MeshRoad DecalManager";
//==============================================================================

//==============================================================================
function SceneLayerTreeClassButton::onClick(%this)
{

	%varWords = strreplace(%this.variable,"_"," ");

	%class = lastWord(%varWords);

	%filtered = $SceneSetTree_FilterClass_[%class];

	SceneLayerTree.setfilterClass(%class,%filtered);
	SceneLayerTree.doFilter();

}
//------------------------------------------------------------------------------
//==============================================================================
function SceneLayerTreeClassGroupButton::onClick(%this)
{

	%varWords = strreplace(%this.variable,"_"," ");

	%group = lastWord(%varWords);
	%list = $SceneSetTree_ClassGroup_[%group];

	%filtered = $SceneSetTree_FilterGroup_[%group];

	if (%filtered)
		SceneLayerTree.addfilterClassList(%list,true);
	else
		SceneLayerTree.removefilterClassList(%list,true);
}
//------------------------------------------------------------------------------
//==============================================================================
// Detach the GUIs not saved with EditorGui (For safely save EditorGui)
//==============================================================================
//==============================================================================
function SceneLayerTree::addfilterClassList(%this,%classList,%doFilter,%clearFilterFirst)
{
	if (%clearFilterFirst)
		%this.filterClasses = "";

	foreach$(%class in %classList)
		%this.setfilterClass(%class,true);

	if (!%doFilter)
		return;

	info("Rebuilding tree with filtered classes");
	%this.doFilter();

}
//------------------------------------------------------------------------------
//==============================================================================
function SceneLayerTree::removefilterClassList(%this,%classList,%doFilter,%clearFilterFirst)
{

	foreach$(%class in %classList)
		%this.setfilterClass(%class,false);

	if (!%doFilter)
		return;

	info("Rebuilding tree with filtered classes");
	%this.doFilter();

}
//------------------------------------------------------------------------------
//==============================================================================
function SceneLayerTree::setfilterClass(%this,%class,%filtered)
{
	if (%filtered)
		%this.filterClasses = strAddWord(%this.filterClasses, %class, 1);
	else
		%this.filterClasses = strRemoveWord(%this.filterClasses, %class);

	info("SceneLayerTree filter set to:",%this.filterClasses);

}
//------------------------------------------------------------------------------
//==============================================================================
function SceneLayerTree::clearFilter(%this)
{
	%this.filterClasses = "";
	%this.doFilter();
}

//------------------------------------------------------------------------------
//==============================================================================
function SceneLayerTree::doFilter(%this)
{
	Lab.filterLayerClass(SceneLayerTree.filterClasses);
	//%this.filterClass(%this.filterClasses);
}
//------------------------------------------------------------------------------

