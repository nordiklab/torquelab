//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function SceneEd::setActiveLayer(%this,%simSet)
{

	if (!isObject(%simSet))
		return;

	%oldGroup = SceneEd.activeLayer;
	SceneEd.activeLayer = %simSet;

	%oldItem = SceneLayerTree.findItemByObjectId(%oldGroup.getId());

	if (%oldItem !$= "-1")
		SceneLayerTree.markItem(%oldItem,false);

	%newItem = SceneLayerTree.findItemByObjectId(%simSet.getId());

	if (%newItem !$= "-1")
		SceneLayerTree.markItem(%newItem,true);
}

function SceneEd::addNewLayer(%this,%addSelection)
{
	%set = Lab.addLayerSet("NewLayer");
	SceneEd.setActiveLayer(%set);

	if (%addSelection)
		SceneEd.addSelectionToLayer(%set);

	//SceneEd.schedule(10,setActiveLayer,%set);
	//if (%addSelection)
	//SceneEd.schedule(20,addSelectionToLayer,%set);
}

function SceneEd::addSelectionToLayer(%this,%layerSet)
{

	if (!isObject(%layerSet))
		%layerSet = SceneEd.activeLayer;

	if (!isObject(%layerSet))
		return;

	%layerSet.addSelectedObjects();

}
//==============================================================================
function Lab::removeObjectFromLayer(%this,%obj)
{
	if (!isObject(%obj) || !isObject($LabObjectLayer[%obj.getId()]))
		return;

	$LabObjectLayer[%obj.getId()].remove(%obj);
}
//------------------------------------------------------------------------------
function SceneEd::selectObjectsInSet(%this, %simGroup,%noRecurse)
{
	%isLast = false;
	%mid = SceneLayerTree.findItemByObjectId(%simGroup);
	SceneLayerTree.removeSelection(%mid);

	foreach(%child in %simGroup)
	{
		if (%child.getClassName() $= "SimSet" && !%noRecurse)
		{
			%this.selectObjectsInSet(%child);
			continue;
		}

		//Scene.onAddSelection(%child,%isLast,SceneGroup);
		//SceneLayerTree.setSelectedItem( %child,false,true);
		%mid = SceneLayerTree.findItemByObjectId(%child);
		SceneLayerTree.addSelection(%mid,false);
	}
}
//------------------------------------------------------------------------------
function SceneEd::setLayerClassFilter(%this, %classes, %hidden)
{

	foreach$(%class in %classes)
	{
		if (%hidden)
		{
			%this.layerDisabledClasses = strAddWord(%this.layerDisabledClasses,%class,1);
		}
		else
		{
			%this.layerDisabledClasses = strRemoveWord(%this.layerDisabledClasses,%class);
		}
	}

	Lab.filterLayerClass(%this.layerDisabledClasses);
}
//------------------------------------------------------------------------------

$LabSceneClassType["Zone"] = "Zone Trigger";
$LabSceneClassType["Shape"] = "TSStatic StaticShape";
$LabSceneClassType["Entity"] = "ScatteredSky SkyBox LevelInfo";
//------------------------------------------------------------------------------
function SceneLayerClassToggle::onMouseDown(%this, %classes, %hidden)
{
	%type = %this.internalName;
	%this.variable = "$SceneLayerClass"@%type;
	$SceneLayerClass[%type] = !$SceneLayerClass[%type];

	SceneEd.setLayerClassFilter($LabSceneClassType[%type],$SceneLayerClass[%type]);
}
//------------------------------------------------------------------------------
