//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function LayerSetTree::onMiddleMouseDown(%this,%hitItemId, %mouseClickCount)
{

	if (%mouseClickCount <= 1)
	{
		%obj = %this.getItemValue(%hitItemId);

		if (%obj.getClassName() $= "SimSet")
			SceneEd.setActiveLayer(%obj);
		else if (isObject(%obj.parentGroup))
			SceneEd.setActiveLayer(%obj.parentGroup);
	}

}
function LayerSetTree::onMouseUp(%this,%hitItemId, %mouseClickCount)
{

	if (%mouseClickCount > 1 && %mouseClickCount !$= "")
	{
		%obj = %this.getItemValue(%hitItemId);

		if (%obj.getClassName() $= "SimSet")
		{

			SceneEd.schedule(200,selectObjectsInSet,%obj);
		}
	}

	return;

}

//==============================================================================
function LayerSetTree::onMouseDown(%this,%hitItemId, %mouseClickCount)
{
	//Nothing as now (expand moved to onAddSelection)
}
function LayerSetTree::onRightMouseUp(%this, %itemId, %mouse, %obj)
{
	%this.showContextMenu(%itemId, %mouse, %obj);
}

//==============================================================================

//==============================================================================
// Tree Items Drag and Dropping
//==============================================================================
//==============================================================================
// Called when an item as been dropped
function LayerSetTree::onDragDropped(%this)
{
	Scene.setDirty();
}
//------------------------------------------------------------------------------
//==============================================================================
function LayerSetTree::isValidDragTarget(%this, %id, %obj)
{
	if (%obj.isMemberOfClass("Path"))
		return EWorldEditor.areAllSelectedObjectsOfType("Marker");

	if (%obj.name $= "CameraBookmarks")
		return EWorldEditor.areAllSelectedObjectsOfType("CameraBookmark");
	else
		return (%obj.isMemberOfClass("SimSet")); // $= "SimGroup" );
}
//------------------------------------------------------------------------------

//==============================================================================
// Set object selected in scene (Trees and WorldEditor)
//==============================================================================

