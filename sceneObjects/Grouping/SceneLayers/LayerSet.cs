//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function Lab::initLabLayerSets(%this)
{
	delObj(MissionLayers);
	%file = strreplace($Server::MissionFile,".mis",".layers");

	if (isFile(%file))
		exec(%file);

	//if (!isObject(ObjSetGroup_Layers))
	newSimGroup("ObjSetGroup_Layers",LabObjectGroup,"Layers","ObjectSetGroup");
	newSimSet("ObjSet_Layers",LabObjectGroup,"Layers","ObjectSet");
	//else
	//ObjSetGroup_Layers.clear();

	if (!isObject(ObjSetGroup_FullLayers))
		newSimGroup("ObjSetGroup_FullLayers",LabGroup,"FullLayers","ObjectSetGroup");
	else
		ObjSetGroup_FullLayers.clear();

	//Default Layer SimSet for all objects not in layer
	if (!isObject(LayerSet_Default))
		newSimSet("LayerSet_Default",ObjSetGroup_Layers,"Default","LayerSet");

	LayerSet_Default.layerID = "-1";

	if (!isObject(LayerSet_Default_Full))
		newSimSet("LayerSet_Default_Full",ObjSetGroup_FullLayers,"Default_Full","FullLayerSet");

	LayerSet_Default_Full.visibleSet = LayerSet_Default;
	LayerSet_Default.fullSet = LayerSet_Default_Full;

	%this.objSetGroup["layer"] = ObjSetGroup_Layers;

	Lab.initLayers();
	SceneLayerTree.rebuild();
}
//==============================================================================
function Lab::addLayerSet(%this,%name,%layerID)
{
	// %layerID = ObjSetGroup_Layers.layerID++;
	%layerID =  %this.getNextLayerId();
	return Lab.getLayerSetId(%layerID);
	%newSet = getUniqueName("LayerSetID_"@%layerID);
	%set = newSimSet(%newSet,ObjSetGroup_Layers,%name,"LayerSet");
	%set.codeName = strreplace(%newSet.getName(),"LayerSet_","");

	%set.codeName = %set.layerID;

	%fullSet = getUniqueName(%newSet@"_Full");
	%fullSet = newSimSet(%fullSet,ObjSetGroup_FullLayers,%name@"_Full","FullLayerSet");

	%fullSet.visibleSet = %set;
	%set.fullSet = %fullSet;
	return %set;
}
//------------------------------------------------------------------------------
///==============================================================================
function Lab::getNextLayerId(%this)
{
	%i = 0;

	while(!%found)
	{
		if (!isObject("LayerSetID_"@%i++))
			return %i;
	}
}
//------------------------------------------------------------------------------
///==============================================================================
function Lab::getLayerSetId(%this,%layerID)
{
	%layerName = "LayerSetID_"@%layerID;
	%name = (MissionLayers.layer[%layerID] $= "") ? "New Layer" : MissionLayers.layer[%layerID];
	%addTo = ObjSetGroup_Layers;

	if (MissionLayers.parentLayer[%layerID] > 0)
		%addTo = %this.getLayerSetId(MissionLayers.parentLayer[%layerID]);

	if (isObject(%layerName))
	{
		%addTo.add(%layerName);
		ObjSet_Layers.add(%layerName);
		%layerName.internalName = %name;
		return %layerName;
	}

	%set = newSimSet(%layerName,%addTo,%name,"LayerSet");

	%set.layerID = %layerID;

	%fullSet = getUniqueName(%newSet@"_Full");
	%fullSet = newSimSet(%fullSet,ObjSetGroup_FullLayers,%name@"_Full","FullLayerSet");
	ObjSet_Layers.add(%set);
	%fullSet.visibleSet = %set;
	%set.fullSet = %fullSet;
	return %set;
}
//------------------------------------------------------------------------------/==============================================================================
function Lab::initLayers(%this,%classes)
{
	Lab.addSetToLayers(ObjSet_Default);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::addSetToLayers(%this,%set)
{
	foreach(%obj in %set)
	{
		/* if (%obj.getClassName() $= "SimGroup")
		 {
		    Lab.addSetToLayers(%obj);
		    continue;
		 }*/
		%layers = %obj.layers;

		if (%layers $= "" || %layers $= "-1")
		{
			%layerSet = "LayerSet_Default";

			if (!isObject(%layerSet))
			{
				%layerSet = Lab.addLayerSet(%layerSet);
				%layerSet.internalName = "Default";
			}

			if (!%layerSet.isMember(%obj))
				%layerSet.add(%obj);

			continue;
		}

		foreach$(%layerId in %layers)
		{
			%layerSet = Lab.getLayerSetId(%layerId);

			if (!isObject(%layerSet))
				continue;

			if (!%layerSet.isMember(%obj))
				%layerSet.add(%obj);
		}
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Detach the GUIs not saved with EditorGui (For safely save EditorGui)
//==============================================================================

//==============================================================================
function Lab::filterLayerClass(%this,%classes)
{

	foreach(%obj in ObjSetGroup_Layers)
	{
		%this.filterLayerSetClass(%obj,%classes);
	}

}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::filterLayerSetClass(%this,%set,%classes)
{
	foreach(%obj in %set.fullSet)
	{
		%className = %obj.getClassName();

		if (%className $= "SimSet" && !%noRecurse)
		{
			//%this.filterLayerSetClass(%obj,%classes);
			continue;
		}

		%disable = false;

		foreach$(%class in %classes)
		{
			if (%className $= %class)
			{
				%disable = true;
				break;
			}
		}

		%set.disabled[%obj.getId()] = %disable;

		if (%disable)
		{
			if (%set.isMember(%obj))
				%set.remove(%obj);
		}
		else
		{
			if (!%set.isMember(%obj))
				%set.add(%obj);
		}

	}

}

//------------------------------------------------------------------------------
//==============================================================================
//==============================================================================
// Detach the GUIs not saved with EditorGui (For safely save EditorGui)
//==============================================================================

//==============================================================================
function LayerSet::onObjectAdded(%this,%obj)
{
	%currentSet = $LabObjectLayer[%obj.getId()];

	if (isObject(%currentSet) && %currentSet.getId() != %this)
	{
		if (%currentSet.isMember(%obj))
			%currentSet.remove(%obj);

	}

	%obj.layers = %this.layerID;

	//%obj.layers = strAddWord(%obj.layers,%this.layerID,1);
	if (!%this.fullSet.isMember(%obj))
		%this.fullSet.add(%obj);

	//if (%obj.superClass $= "LayerSet")
	//ObjSet_Layers.add(%obj);
	$LabObjectLayer[%obj.getId()] = %this.getName();

}
//------------------------------------------------------------------------------
//==============================================================================
function LayerSet::onObjectRemoved(%this,%obj)
{

	if (%this.disabled[%obj.getId()])
	{
		return;
	}

	if (%this.fullSet.isMember(%obj))
		%this.fullSet.remove(%obj);

	//All objects should be in layer tree, if no layer, they are added to default
	%obj.layers = "";
	// %obj.layers = strRemoveWord(%obj.layers,%this.layerID);
}
//------------------------------------------------------------------------------

//==============================================================================
function LayerSet::addSelectedObjects(%this)
{
	%count = EWorldEditor.getSelectionSize();

	for(%j=0; %j<%count; %j++)
	{
		%obj = EWorldEditor.getSelectedObject(%j);
		%this.add(%obj);
	}

	SceneLayerTree.setSelectedItem(%obj,false,true);
}
//------------------------------------------------------------------------------

//==============================================================================
function FullLayerSet::onObjectAdded(%this,%obj)
{

}
//------------------------------------------------------------------------------
//==============================================================================
function FullLayerSet::onObjectRemoved(%this,%obj)
{

	if (!LayerSet_Default.isMember(%obj))
		LayerSet_Default.add(%obj);
}
//------------------------------------------------------------------------------
