//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function Lab::buildObjectSetMenu(%this)
{
	SEP_ObjectSetMenu.clear();

	foreach(%obj in ObjSetGroup_Class)
	{
		SEP_ObjectSetMenu.add(%obj.internalName,%obj.getId());
	}
}
//==============================================================================
function Lab::addObjectSet(%this,%name,%type,%notUnique)
{
	%newSet = "ObjSet_"@%name;

	if (%notUnique)
		%newSet = getUniqueName(%newSet);

	if (isObject(%newSet))
		return %newSet;

	%setGroup = LabObjectGroup;

	if (%type !$= "" && isObject(%this.objSetGroup[%type]))
		%setGroup = %this.objSetGroup[%type];

	%set = newSimSet(%newSet,%setGroup,%name,"ObjectSet");

	if (%type $= "Class")
		SEP_ObjectSetMenu.add(%set.internalName,%set.getId());

	return %set;
}
//------------------------------------------------------------------------------
