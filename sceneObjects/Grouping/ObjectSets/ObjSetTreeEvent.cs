//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function ObjectSetTree::onMiddleMouseDown(%this,%hitItemId, %mouseClickCount)
{

}
function ObjectSetTree::onMouseUp(%this,%hitItemId, %mouseClickCount)
{

}

//==============================================================================
function ObjectSetTree::onMouseDown(%this,%hitItemId, %mouseClickCount)
{
	//Nothing as now (expand moved to onAddSelection)
}
function ObjectSetTree::onRightMouseUp(%this, %itemId, %mouse, %obj)
{
	%this.showContextMenu(%itemId, %mouse, %obj);
}

//==============================================================================

//==============================================================================
// Tree Items Drag and Dropping
//==============================================================================
//==============================================================================
// Called when an item as been dropped
function ObjectSetTree::onDragDropped(%this)
{
	Scene.setDirty();
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjectSetTree::isValidDragTarget(%this, %id, %obj)
{

}
//------------------------------------------------------------------------------

//==============================================================================
// Set object selected in scene (Trees and WorldEditor)
//==============================================================================

