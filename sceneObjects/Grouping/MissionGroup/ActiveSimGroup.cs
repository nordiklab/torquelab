//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$SEP_AutoGroupActiveSystem = true;

//==============================================================================
// Set active Misison SimGroup into which new object are added
//==============================================================================

function Scene::setActiveSimGroup(%this, %group)
{

	if (!isObject(%group)) // || !isObject(Scene.activeSimGroup))
		return;

	if (!%group.isMemberOfClass("SimGroup"))
		return;

	if (isObject(Scene.activeSimGroup))
		if (Scene.activeSimGroup.getId() $= %group.getId())
			return;

	%currentGroup = Scene.activeSimGroup;
	Scene.activeSimGroup = %group;

	//Update the active group mark on each trees
	%tree = SceneEditorTree;

	//Unmark previous group in tree
	if (isObject(%currentGroup))
	{
		%oldItem = %tree.findItemByObjectId(%currentGroup.getId());

		if (%oldItem !$= "-1")
			%tree.markItem(%oldItem,false);
	}

	//Mark new group in tree
	%newItem = %tree.findItemByObjectId(%group.getId());

	if (%newItem !$= "-1")
		%tree.markItem(%newItem,true);

	return Scene.activeSimGroup;
}
//------------------------------------------------------------------------------

//==============================================================================
function Scene::getActiveSimGroup(%this)
{
	if (!isObject(%this.activeSimGroup))
		%this.activeSimGroup = %this.setActiveSimGroup(MissionGroup);
	else if (!%this.activeSimGroup.isMemberOfClass("SimGroup"))
		%this.activeSimGroup = %this.setActiveSimGroup(MissionGroup);

   if (!MissionGroup.isMember(%this.activeSimGroup) && MissionGroup.getId() !$= %this.activeSimGroup.getId() )
      MissionGroup.add(%this.activeSimGroup);
	return %this.activeSimGroup;
}
//------------------------------------------------------------------------------

//==============================================================================
//Called from code to get the group into which new object will be pasted
function EWorldEditor::getNewObjectGroup(%this)
{  
   %simGroup = Scene.getActiveSimGroup();
   
   if (!isObject(%simGroup))
   {
      info("getActiveSimGroup return invalid object:",%simGroup);
	   if (!isObject(WorldEditorPasteGroup))
		   new SimGroup(WorldEditorPasteGroup);
      MissionGroup.add(WorldEditorPasteGroup);
     return WorldEditorPasteGroup;
   } 
   else if (!%simGroup.isMemberOfClass("SimGroup"))
   {
      info("getActiveSimGroup return object which is not a group:",%simGroup);
	   if (!isObject(WorldEditorPasteGroup))
		   new SimGroup(WorldEditorPasteGroup);
      MissionGroup.add(WorldEditorPasteGroup);
     return WorldEditorPasteGroup;
   } 
   info("getNewObjectGroup return valid:",%simGroup);
	return %simGroup;
}
//------------------------------------------------------------------------------

//==============================================================================
// BUILT-IN Method which request an object group into which cloned object will be added
// --Only called from DragCloning so we use it unorthodoxly, by returning 1 the
// clone source parent will be used which would fix some issues with SimContainer
/*function EWorldEditor::getActiveSimGroup(%this)
{
	if (!isObject(WorldEditorPasteGroup))
		new SimGroup(WorldEditorPasteGroup);

	return WorldEditorPasteGroup;
}*/
//------------------------------------------------------------------------------

//==============================================================================
// Set the selected object (Index 0 if multiple) parentgroup and set it active
//==============================================================================
function Scene::selectObjectGroup(%this, %simGroup,%noRecurse)
{
	%isLast = false;

	foreach(%child in %simGroup)
	{
		if (%child.getClassName() $= "SimGroup" && !%noRecurse)
			%this.selectObjectGroup(%child);

		if (%simGroup.getObjectIndex(%child) >= %simGroup.getCount()-1)
			%isLast = true;

		Scene.onAddSelection(%child,%isLast,SceneGroup);
	}
}
//------------------------------------------------------------------------------
//Scene.addSelectionToActiveGroup
//==============================================================================
function Scene::addSelectionToActiveGroup(%this)
{
	%count = EWorldEditor.getSelectionSize();

	if (%count < 1)
	{
		warnLog("There's no selected objects to copy!");
		return;
	}

	for(%j=0; %j<%count; %j++)
	{
		%obj = EWorldEditor.getSelectedObject(%j);
		Scene.addObjectToScene(%obj);

	}
}
//------------------------------------------------------------------------------
