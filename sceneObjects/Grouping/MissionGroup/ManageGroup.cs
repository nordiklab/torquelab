//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Group selected objects into new group or active group
//==============================================================================

//==============================================================================
function Scene::addSimGroup(%this, %groupCurrentSelection)
{
	%activeSelection = EWorldEditor.getActiveSelection();

	if (%activeSelection.getObjectIndex(MissionGroup) != -1)
	{
		LabMsgOK("Error", "Cannot add MissionGroup to a new SimGroup");
		return;
	}

	// Find our parent.
	%parent = %this.getActiveSimGroup();

	if (!%groupCurrentSelection && isObject(%activeSelection) && %activeSelection.getCount() > 0)
	{
		%firstSelectedObject = %activeSelection.getObject(0);

		if (%firstSelectedObject.isMemberOfClass("SimGroup"))
			%parent = %firstSelectedObject;
		else if (%firstSelectedObject.getId() != MissionGroup.getId())
			%parent = %firstSelectedObject.parentGroup;
	}

	// If we are about to do a group-selected as well,
	// starting recording an undo compound.

	if (%groupCurrentSelection)
		Editor.getUndoManager().pushCompound("Group Selected");

	//%name =
	// Create the SimGroup.
	%object = new SimGroup()
	{
		parentGroup = %parent;
		superClass = "LevelGroup";
	};

	if (%activeSelection.getObject(0).internalName !$="")
		%object.internalName = %activeSelection.getObject(0).internalName;

	MECreateUndoAction::submit(%object);

	// Put selected objects into the group, if requested.

	if (%groupCurrentSelection && isObject(%activeSelection))
	{
		%undo = UndoActionReparentObjects::create(SceneEditorTree);
		%numObjects = %activeSelection.getCount();

		for(%i = 0; %i < %numObjects; %i ++)
		{
			%sel = %activeSelection.getObject(%i);
			%undo.add(%sel, %sel.parentGroup, %object);
			%object.add(%sel);
		}

		%undo.addToManager(Editor.getUndoManager());
	}

	// Stop recording for group-selected.

	if (%groupCurrentSelection)
		Editor.getUndoManager().popCompound();

	// When not grouping selection, make the newly created SimGroup the
	// current selection.

	if (!%groupCurrentSelection)
	{
		EWorldEditor.clearSelection();
		EWorldEditor.selectObject(%object);
	}

	// Refresh the Gui.
	//%this.syncGui();
}
//------------------------------------------------------------------------------

//==============================================================================
// Hide/Show/Toggle Visibility of objects contained inside a group
//==============================================================================

//==============================================================================
function Scene::hideGroupChilds(%this, %simGroup)
{

	foreach(%child in %simGroup)
	{
		if (%child.getClassName() $=  "SimGroup" ||  %child.getClassName() $=  "SimSet")
			%this.hideGroupChilds(%child);

		if (!%child.hidden)
			%child.setFieldValue("hidden",true);

		//EWorldEditor.hideObject(%child,true);

		//%child.setHidden( true );
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function Scene::showGroupChilds(%this, %simGroup)
{

	foreach(%child in %simGroup)
	{
		if (%child.getClassName() $=  "SimGroup"  ||  %child.getClassName() $=  "SimSet")
			%this.showGroupChilds(%child);

		if (%child.hidden)
			EWorldEditor.hideObject(%child,false);

		%child.setHidden(false);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function Scene::toggleHideGroupChilds(%this, %simGroup,%noRecurse)
{

	foreach(%child in %simGroup)
	{
		if ((%child.isMemberOfClass("SimGroup") ||  %child.getClassName() $=  "SimSet") && !%noRecurse)
			%this.toggleHideGroupChilds(%child);

		//else  //Let's hide SimGroup too
		%child.setHidden(!%child.hidden);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function EWorldEditor::toggleHideChildren(%this, %simGroup)
{
	Scene.toggleHideGroupChilds(%simGroup,%noRecurse);
	EWorldEditor.syncGui();
}
//------------------------------------------------------------------------------
//==============================================================================
// Lock/Unlock objects contained inside a group
//==============================================================================
function Scene::lockGroupObjects(%this, %simGroup,%locked,%noRecurse)
{
	foreach(%child in %simGroup)
	{
		%child.setFieldValue("locked",%locked);

		if (%child.getClassName() $= "SimGroup" && !%noRecurse)
			%this.lockGroupObjects(%child,%locked);
	}
}
//------------------------------------------------------------------------------
function Scene::toggleLockChildren(%this, %simGroup,%noRecurse)
{
	foreach(%child in %simGroup)
	{
		%child.setFieldValue("locked",%locked);

		if (%child.getClassName() $= "SimGroup" && !%noRecurse)
			%this.toggleLockChildren(%child,%noRecurse);
		else
			%child.setLocked(!%child.locked);

	}
}
//------------------------------------------------------------------------------
//==============================================================================
function EWorldEditor::toggleLockChildren(%this, %simGroup,%noRecurse)
{
	Scene.toggleLockChildren(%simGroup,%noRecurse);
	EWorldEditor.syncGui();
}
//------------------------------------------------------------------------------
