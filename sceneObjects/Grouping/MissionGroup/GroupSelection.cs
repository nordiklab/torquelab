//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Group selected objects into new objects SimSet
//==============================================================================

//==============================================================================
// Select all objects contained inside parent group of object (0)
//==============================================================================
function Scene::selectGroupObjects(%this, %simGroup,%noRecurse)
{
	%isLast = false;

	foreach(%child in %simGroup)
	{
		if (%child.getClassName() $= "SimGroup" && !%noRecurse)
			%this.selectGroupObjects(%child);

		if (%simGroup.getObjectIndex(%child) >= %simGroup.getCount()-1)
			%isLast = true;

		Scene.onAddSelection(%child,%isLast,SceneGroup);
	}
}
//------------------------------------------------------------------------------
function Scene::unselectGroupObjects(%this, %simGroup,%noRecurse)
{
	foreach(%child in %simGroup)
	{
		if (%child.getClassName() $= "SimGroup" && !%noRecurse)
			%this.unselectGroupObjects(%child);

		Scene.onRemoveSelection(%child,SceneGroup);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function EWorldEditor::selectAllObjectsInSet(%this, %set, %deselect,%noRecurse)
{
	if (%deselect)
		Scene.selectGroupObjects(%set,%noRecurse);
	else
		Scene.unselectGroupObjects(%set,%noRecurse);

}
//------------------------------------------------------------------------------

//==============================================================================
// Select all objects contained inside parent group of object (0)
//==============================================================================
function Scene::selectParentGroupObjects(%this, %obj,%noRecurse)
{
	if (isObject(%obj.parentGroup))
		%this.selectGroupObjects(%obj.parentGroup,%noRecurse);

}
//------------------------------------------------------------------------------
