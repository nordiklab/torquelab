//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function Lab::getEmptyContainer(%this,%setBaseName)
{
	//%container = newSimGroup("",Scene.getActiveSimGroup(),"ObjContainer","SimContainer");
	%container = new SimContainer();
	%container.superClass = "ObjContainer";
	Scene.addObjectToScene(%container);

	if (%setBaseName !$= "")
		%container.setName(getUniqueName(%setBaseName));

	return %container;
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::getContainerEmptyClone(%this,%container)
{

	%newContainer = %this.getEmptyContainer();
	%container.parentGroup.add(%newContainer);
	%intName = getUniqueInternalName(%container.internalName,%newContainer.parentGroup,true);

	%newContainer.setInternalName(%intName);
	%newContainer.locked = %container.locked;
	return %newContainer;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
function Scene::cloneActiveContainer(%this, %offset,%copieId)
{
	if (!isObject(Scene.activeContainer))
		return;

	%this.cloneContainer(Scene.activeContainer,%offset,%copieId);
}
function Scene::cloneContainer(%this,%simContainer, %offset,%copieId)
{
	if (%copieId $= "")
		%copieId = 1;

	if (%offset $= "")
		%offset = "0 0 0";

	%cloneContainer = %simContainer.deepClone();

	foreach(%obj in %cloneContainer)
	{
		%obj.position.x += %offset.x * %copieId;
		%obj.position.y += %offset.y * %copieId;
		%obj.position.z += %offset.z * %copieId;
		//	EWorldEditor.selectObject(%obj);
	}

	// Scene.selectObjectContainer(%cloneContainer);
}
//------------------------------------------------------------------------------

function SimObject::getSimContainer(%this)
{
	if (%this.parentGroup.getClassName() $= "SimContainer")
		return %this.parentGroup;

	return "";
}

function SimObject::isContained(%this)
{
	if (%this.parentGroup.getClassName() $= "SimContainer")
		return true;

	return false;
}

function SimContainer::onObjectAdded(%this, %obj)
{
	%obj.myContainer = %this.getId();
}
function SimContainer::onObjectRemoved(%this, %obj)
{
	if (%this.getCount() <= 0)
		%this.schedule(100,delete);

	//Make sure it's the same ID (OnObjectAdded might happen before on new container)
	//if (%obj.myContainer $= %this.getId())
	//%obj.myContainer = "";
}
