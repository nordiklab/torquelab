//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Set active Misison SimGroup into which new object are added
//==============================================================================

//==============================================================================
function Lab::addSelToContainer(%this,%container,%setBaseName)
{
	if (!isObject(%container))
		%container = %this.getEmptyContainer();

	%container.locked = true;
	%activeSelection = EWorldEditor.getActiveSelection();

	for(%i = 0; %i < %activeSelection.getCount(); %i ++)
	{
		%sel = %activeSelection.getObject(%i);

		if (%sel.parentGroup.getClassName() $= "SimContainer")
			%postDel = strAddWord(%postDel,%sel.parentGroup.getId());

		if (%sel.internalName !$= "" && !%nameSet)
		{
			%nameSet = true;
			%container.internalName = "ct_"@%sel.internalName;
		}

		%container.add(%sel);
	}

	foreach$(%obj in %postDel)
	{
		delObj(%obj);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::detachSelFromContainer(%this)
{
	if (!isObject(%container))
		%container = %this.getEmptyContainer();

	%container.locked = true;
	%activeSelection = EWorldEditor.getActiveSelection();

	for(%i = 0; %i < %activeSelection.getCount(); %i ++)
	{
		%sel = %activeSelection.getObject(%i);

		if (%sel.parentGroup.getClassName() $= "SimContainer")
		{
			%container = %sel.parentGroup;
			%sel.parentGroup.parentGroup.add(%sel);

			if (%container.getCount() <= 0)
				delObj(%container);
		}
	}
}
//------------------------------------------------------------------------------
