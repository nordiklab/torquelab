//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$SEP_AutoGroupActiveSystem = true;

//==============================================================================
// Set active Misison SimGroup into which new object are added
//==============================================================================
function Scene::selectObjectContainer(%this, %simContainer,%noRecurse)
{
	%isLast = false;
	%simContainer.setIsExpanded(0);

	foreach(%child in %simContainer)
	{
		if (%child.getClassName() $= "SimGroup" && !%noRecurse)
			%this.selectObjectContainer(%child);

		if (%simContainer.getObjectIndex(%child) >= %simContainer.getCount()-1)
			%isLast = true;

		Scene.onAddSelection(%child,%isLast,SceneGroup);

	}

	Scene.activeContainer = %simContainer;
	Scene.schedule(200,setContainerExpanded,%simContainer,0);
}
//------------------------------------------------------------------------------
function Scene::setContainerExpanded(%this,%simContainer, %isExpanded)
{
	%itemCont = SceneEditorTree.findItemByObjectId(%simContainer.getId());
	//SceneEditorTree.setSelectedItem(%itemCont,false,true);
	//SceneEditorTree.expandItem(%itemCont,%isExpanded);
	//SceneEditorTree.buildVisibleTree();
	Scene.doInspect(%simContainer);
}

function Scene::containerWorldSel(%this,%simContainer)
{
	foreach(%obj in %simContainer)
	{
		EWorldEditor.selectObject(%obj);
	}

	// Scene.selectObjectContainer(%cloneContainer);
}

