//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
/*
//==============================================================================
function Scene::onObjectAdded(%this,%obj,%container)
{
	Lab.prepareNewObject(%obj);
}
//------------------------------------------------------------------------------
//==============================================================================
function Scene::onObjectRemoved(%this,%obj)
{
}
//------------------------------------------------------------------------------

//==============================================================================
function Scene::onObjectAddedToSet(%this,%obj,%container)
{
	Lab.prepareNewObject(%obj);
}
//------------------------------------------------------------------------------
//==============================================================================
function Scene::onObjectRemovedFromSet(%this,%obj)
{
}
//------------------------------------------------------------------------------

//==============================================================================
function MissionGroup::onObjectAdded(%this,%obj)
{
	if (isObject(Scene))
		Scene.onObjectAdded(%obj,%this);
}
//------------------------------------------------------------------------------
//==============================================================================
function MissionGroup::onObjectRemoved(%this,%obj)
{
	if (isObject(Scene))
		Scene.onObjectRemoved(%obj,%this);
}
//------------------------------------------------------------------------------
//==============================================================================
function LevelGroup::onObjectAdded(%this,%obj)
{
	if (isObject(Scene))
		Scene.onObjectAdded(%obj,%this);
}
//------------------------------------------------------------------------------
//==============================================================================
function LevelGroup::onObjectRemoved(%this,%obj)
{
	if (isObject(Scene))
		Scene.onObjectRemoved(%obj,%this);
}
//------------------------------------------------------------------------------

//==============================================================================
// Generic SceneObject children add/removed callbacks (For non SimSet based objects)
//==============================================================================

//==============================================================================
function SceneObject::onObjectAdded(%this,%obj)
{
	if (isObject(Scene))
		Scene.onObjectAdded(%obj,%this);
}
//------------------------------------------------------------------------------
//==============================================================================
function SceneObject::onObjectRemoved(%this,%obj)
{
	if (isObject(Scene))
		Scene.onObjectRemoved(%obj,%this);
}
//------------------------------------------------------------------------------

//==============================================================================
function SimGroup::onObjectAdded(%this,%obj)
{
	//Only care if it's a children of MissionGroup
	if (%obj.getGroupDepth(MissionGroup) $= "-1")
		return;

	Scene.onObjectAdded(%obj,%this);
}
//------------------------------------------------------------------------------
//==============================================================================
function SimGroup::onObjectRemoved(%this,%obj)
{
	//Only care if it's a children of MissionGroup
	if (%obj.getGroupDepth(MissionGroup) $= "-1")
		return;

	if (isObject(Scene))
		Scene.onObjectRemoved(%obj,%this);
}
//------------------------------------------------------------------------------

//==============================================================================
// SimSet Callbacks - Not linked to Scene objects as their parentGroup will be used
//==============================================================================

//==============================================================================
function SimSet::onObjectAdded(%this,%obj)
{
	//Only care if it's a children of MissionGroup
	if (%this.getGroupDepth(LabObjectGroup) $= "-1")
	{
		//SimObject::onObjectAdded(%this,%obj);
		return;
	}
	%addToName = objName(%this);
	%addedName = objName(%obj);
}
//------------------------------------------------------------------------------
//==============================================================================
function SimSet::onObjectRemoved(%this,%obj)
{
	//Only care if it's a children of MissionGroup
	if (%this.getGroupDepth(LabObjectGroup) $= "-1")
		return;

	%addToName = objName(%this);
	%addedName = objName(%obj);
	Parent::onObjectRemoved(%this,%obj);
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjectSet::onObjectAdded(%this,%obj,%allowMultiSet)
{
	%addToName = objName(%this);
	%addedName = objName(%obj);
	$LabObjectSet[%obj.getId()] = %this.getName();
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjectSet::onObjectRemoved(%this,%obj)
{
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjectSet::addSelectedObjects(%this)
{
	%count = EWorldEditor.getSelectionSize();

	for(%j=0; %j<%count; %j++)
	{
		%obj = EWorldEditor.getSelectedObject(%j);
		%this.add(%obj);
	}

	SceneLayerTree.setSelectedItem(%obj,false,true);
}
//------------------------------------------------------------------------------

*/
