//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function Lab::DeleteSel(%this)
{

	//Call the global scene object deletion function
	Scene.deleteSelection();
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::DeselectSel(%this)
{

	if (EWorldEditor.getSelectionSize() <= 0)
		return;

	EWorldEditor.clearSelection();
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::CutSel(%this)
{

	if (EWorldEditor.getSelectionSize() <= 0)
		return;

	EWorldEditor.cutSelection();
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::CopySel(%this)
{

EWorldEditor.copySelection();
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::PasteSel(%this)
{

	EWorldEditor.pasteSelection();

}
//------------------------------------------------------------------------------

function Lab::ToggleVisibleSel(%this)
{

	//Call the global scene object deletion function
	Scene.ToggleVisibleSelection();
}
//------------------------------------------------------------------------------
function Scene::ToggleVisibleSelection(%this)
{

	//By deleting the object itself, the SceneTree remove it instantly.
	for(%i=EWorldEditor.getSelectionSize()-1; %i>=0; %i--)
	{
		%obj = EWorldEditor.getSelectedObject(%i);
		EWorldEditor.hideObject(%obj,!%obj.hidden);
	}

}
//==============================================================================
function Lab::setSelectionLock( %this,%locked )
{
	%list = SceneEditorTree.getSelectedObjectList();

	foreach$(%obj in %list)
	{
		if (%obj.parentGroup.getClassName() $= "SimContainer")
		{
			%obj.locked = false;
			continue;
		}

		%obj.locked = %locked;
	}

	return;
	//EWorldEditor.lockSelection(%locked);
	EWorldEditor.syncGui();
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::setSelectionVisible( %this,%visible )
{
	EWorldEditor.hideSelection(!%visible);
	EWorldEditor.syncGui();
}
//------------------------------------------------------------------------------

function Lab::ToggleLockSel(%this)
{

	//Call the global scene object deletion function
	Scene.ToggleLockSelection();
}
//------------------------------------------------------------------------------
function Scene::ToggleLockSelection(%this)
{

	//By deleting the object itself, the SceneTree remove it instantly.
	for(%i=EWorldEditor.getSelectionSize()-1; %i>=0; %i--)
	{
		%obj = EWorldEditor.getSelectedObject(%i);
		%obj.locked = !%obj.locked;
	}

}
//==============================================================================
// Objects Selection Callback - Group of function called when selecting objects
//==============================================================================
//==============================================================================
// Add a function to call when selecting an object
function Lab::AddSelectionCallback(%this,%function,%type)
{
	Lab.selectionCallback[%type] = strAddField(Lab.selectionCallback[%type],%function);
}
//------------------------------------------------------------------------------
//==============================================================================
// Call all the selection callbacks when selection changed
function Lab::DoSelectionCallback(%this,%type,%object)
{
	%fields = Lab.selectionCallback[%type];
	%count = getFieldCount(%fields);

	for(%i=0; %i<%count; %i++)
	{
		%evalFunc = getField(%fields,%i)@"(%object);";
		eval(getField(%fields,%i)@"(%object);");
	}
}
//------------------------------------------------------------------------------
