//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Sync the different Selection related Gui
function Scene::syncSelectionGui(%this)
{
	// Inform the camera
	Lab.OrbitCameraChange(EWorldEditor.getSelectionSize(), EWorldEditor.getSelectionCentroid());
	EditorGuiStatusBar.setSelectionObjectsByCount(EWorldEditor.getSelectionSize());

	%selSize = EWorldEditor.getSelectionSize();
	%lockCount = EWorldEditor.getSelectionLockCount();

	if (%lockCount < %selSize)
	{
		SceneTreeWindow-->LockSelection.setStateOn(0);
		SceneTreeWindow-->LockSelection.command = "EWorldEditor.lockSelection(true); SceneEditorTree.toggleLock();";
	}
	else if (%lockCount > 0)
	{
		SceneTreeWindow-->LockSelection.setStateOn(1);
		SceneTreeWindow-->LockSelection.command = "EWorldEditor.lockSelection(false); SceneEditorTree.toggleLock();";
	}

	if (%selSize > 0 && %lockCount == 0)
		SceneTreeWindow-->DeleteSelection.command = "EditorMenuEditDelete();";
	else
		SceneTreeWindow-->DeleteSelection.command = "";
}
//------------------------------------------------------------------------------
//==============================================================================
// Sync the different Selection related Gui
function Scene::onSelectionChanged(%this)
{
	Lab.postEvent("SceneSelectionChanged",EWorldEditor.getActiveSelection());
	// Inform the camera
	Scene.syncSelectionGui();
	Lab.DoSelectionCallback("Transform");
}
//------------------------------------------------------------------------------

//==============================================================================
// Sync the different Selection related Gui
function Scene::clearSelection(%this)
{
	SceneEditorTree.clearSelection();
	EWorldEditor.clearSelection();
}
//------------------------------------------------------------------------------

//==============================================================================
// Scene Select Callbacks
//==============================================================================
//==============================================================================
// Shouldn't be used anymore OR could be use to select an object globally.
function Scene::selectObject(%this, %obj,%isLast,%source)
{

	//Add an object to selection manually (Without using WorldEditor or SceneTree)
	%this.onAddSelection(%obj,%isLast,%source);

}
//------------------------------------------------------------------------------
//==============================================================================
// Shouldn't be used anymore OR could be use to select an object globally.
function Scene::unselectObject(%this, %obj,%source)
{
	//Add an object to selection manually (Without using WorldEditor or SceneTree)
	%this.onRemoveSelection(%obj,%source);
}
//------------------------------------------------------------------------------
//==============================================================================
// Add Obj to Sel - Called when WorldEditor or a SceneTree selected an object
//==============================================================================

//==============================================================================
// Called from SceneObjectTrees onAddSelection Callback
function Scene::onAddSelection(%this, %obj, %isLastSelection,%source)
{

	if (%obj.getClassName() $= "SimGroup" || %obj.getClassName() $= "SimContainer")
	{
		if (EWorldEditor.getSelectionSize() == 0)
			Scene.doInspect(%obj);

		return;
	}

	if (isObject(%source))
	{
		%sourceClass = %source.getClassName();

		switch$(%sourceClass)
		{
			case "WorldEditor":
				SceneEditorTree.setSelectedItem(%obj,false,true);
				EWorldEditor.onAddObjectToSelection(%obj);

			//if (%obj.getClassName() !$= "SimGroup")
			//EWorldEditor.selectObject(%obj);

			case "GuiTreeViewCtrl":
				//%source.setSelectedItem(%obj,false,true);
				EWorldEditor.onAddObjectToSelection(%obj);

			//if (%obj.getClassName() !$= "SimGroup")
			//EWorldEditor.selectObject(%obj);

			case "SimGroup":
				Scene.setActiveSimGroup(%obj);
		}
	}

	//No source, selection provided from script so update WorldEditor and Trees
	else
	{
		SceneEditorTree.setSelectedItem(%obj,false,true);
		//New Special WorldEditor for adding valid object only
		EWorldEditor.onAddObjectToSelection(%obj);
		//if (%obj.getClassName() !$= "SimGroup")
		//EWorldEditor.selectObject(%obj);
	}

	if (%isLastSelection)
		Lab.postEvent("SceneObjectAddedChanged",%obj);

	%plugin = Lab.currentEditor;

	if (%plugin.isMethod("onSelectObject"))
		%plugin.onSelectObject(%obj);

	if (Lab.isMethod("on"@%obj.getClassName()@"Selected"))
		eval("Lab.on"@%obj.getClassName()@"Selected(%obj);");

	if (%plugin.isMethod("on"@%obj.getClassName()@"Selected"))
		eval("%plugin.on"@%obj.getClassName()@"Selected(%obj);");

	Scene.syncSelectionGui();

	if (!$Cfg_Common_Objects_autoInspect)
		return;

	if (!$Cfg_Common_Objects_allowMultiInspect)
	{
		//Simply set current as inspect
		//if (SceneInspector.getNumInspectObjects() == 0)
		if (EWorldEditor.getSelectionSize() <= 1)
			Scene.doInspect(%obj);

		return;
	}

	if (EWorldEditor.getSelectionSize() <= 1)
		Scene.doInspect(%obj);
	else
		%this.doAddInspect(%obj,%isLastSelection);

}
//==============================================================================
// Remove Obj From Sel - Called when WorldEditor or a SceneTree unselected an object
//==============================================================================
//==============================================================================
function Scene::onRemoveSelection(%this, %obj,%source)
{

   //Make sure we have a valid object
	if (!isObject(%obj))		
		return;
	
	if (isObject(%source))
	{
		%sourceClass = %source.getClassName();

		switch$(%sourceClass)
		{
			case "WorldEditor":
				SceneEditorTree.removeSelection(%obj);
			case "GuiTreeViewCtrl":
				EWorldEditor.unselectObject(%obj);
		}

	}
	else
		EWorldEditor.unselectObject(%obj);
	
	%plugin = Lab.currentEditor;

   //Check for a plugin specific unselect method for that object class
	if (%plugin.isMethod("on"@%obj.getClassName()@"Unselect"))
		eval("%plugin.on"@%obj.getClassName()@"Unselect(%obj);");

   //Check for a plugin specific onUnselect method
	if (%plugin.isMethod("onUnselect"))
		%plugin.onUnselect(%obj);

	Scene.onSelectionChanged();
}
//------------------------------------------------------------------------------

//==============================================================================
function Scene::onMultiSelectDone(%this, %set)
{
	%count = %set.getCount();

	if (%count > 0)
	{

		foreach(%obj in %set)
		{
			SceneEditorTree.setSelectedItem(%obj,false,true);
		}

		Scene.doInspect(%set.getObject(0));
	}

	Scene.syncSelectionGui();
}

//------------------------------------------------------------------------------
/*
//==============================================================================
function Scene::onSelect(%this, %obj) {

	if (%obj.getClassName() $= "SimGroup") {
		Scene.setActiveSimGroup( %obj );
	}
	Scene.doInspect(%obj);

	%plugin = Lab.currentEditor;
	if (%plugin.isMethod("onSelectObject"))
		%plugin.onSelectObject(%obj);

	if (%plugin.isMethod("on"@%obj.getClassName()@"Selected"))
		eval("%plugin.on"@%obj.getClassName()@"Selected(%obj);");

	// Update the materialEditorList
	$Lab::materialEditorList = %obj.getId();

	// Used to help the Material Editor( the M.E doesn't utilize its own TS control )
	// so this dirty extension is used to fake it
	if (isObject(MaterialEditorPreviewWindow))
		if ( MaterialEditorPreviewWindow.isVisible() )
			MaterialEditorTools.prepareActiveObject();

	$WorldEditor_LastSelZ = %obj.position.z;

	Scene.onSelectionChanged();
}
//------------------------------------------------------------------------------
*/

//==============================================================================
// Scene Unselect Callbacks
//==============================================================================
/*
//==============================================================================
function Scene::unselectObject(%this, %obj,%source) {

	foreach$(%tree in Scene.sceneTrees) {
		if (%tree.isItemSelected(%obj))
			%tree.removeSelection(%obj);
	}

	Scene.doRemoveInspect(%obj);
	Scene.onUnSelect(%obj);

}
//------------------------------------------------------------------------------

//==============================================================================
function Scene::onUnselect(%this, %obj) {

	//EWorldEditor.unselectObject(%obj);
	%plugin = Lab.currentEditor;
	if (%plugin.isMethod("on"@%obj.getClassName()@"Unselected"))
		eval("%plugin.on"@%obj.getClassName()@"Unselected(%obj);");

	if (%plugin.isMethod("onUnselectObject"))
		%plugin.onUnselectObject(%obj);

	Scene.onSelectionChanged();

}
//------------------------------------------------------------------------------
*/

//------------------------------------------------------------------------------

function Scene::setDirty(%this)
{
	EWorldEditor.isDirty = true;
}

//------------------------------------------------------------------------------
function Scene::getFullSelList(%this)
{
	%list = SceneEditorTree.getSelectedObjectList();

	for(%j=0; %j<EWorldEditor.getSelectionSize(); %j++)
	{
		%obj = EWorldEditor.getSelectedObject(%j);
		%list = strAddWord(%list,%obj.getId(),true);
	}

	return %list;

}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
function Scene::getWorldSelList(%this)
{
	for(%j=0; %j<EWorldEditor.getSelectionSize(); %j++)
	{
		%obj = EWorldEditor.getSelectedObject(%j);
		%list = strAddWord(%list,%obj.getId(),true);
	}

	return %list;

}
//------------------------------------------------------------------------------
