
singleton Material(Grid_512_Gray_Mat)
{
   mapTo = "unmapped_mat";
   diffuseMap[0] = "./Grid_512_grey.png";
   tags = "Editor";
};

singleton Material(Grid_512_Black_Mat)
{
   mapTo = "unmapped_mat";
   diffuseMap[0] = "./Grid_512_black.png";
   tags = "Editor";
};
singleton Material(Grid_512_Blue_Mat)
{
   mapTo = "unmapped_mat";
   diffuseMap[0] = "./Grid_512_blue.png";
   tags = "Editor";
};
singleton Material(Grid_512_Green_Mat)
{
   mapTo = "unmapped_mat";
   diffuseMap[0] = "./Grid_512_green.png";
   tags = "Editor";
};
singleton Material(Grid_512_Orange_Mat)
{
   mapTo = "unmapped_mat";
   diffuseMap[0] = "./Grid_512_orange.png";
   tags = "Editor";
};
singleton Material(Grid_512_Red_Mat)
{
   mapTo = "unmapped_mat";
   diffuseMap[0] = "./Grid_512_red.png";
   tags = "Editor";
};
singleton Material(Grid_512_ForestGreen_Mat)
{
   mapTo = "unmapped_mat";
   diffuseMap[0] = "./Grid_512_forestgreen.png";
   tags = "Editor";
};
singleton Material(Grid_512_ForestGreen_Lines_Mat)
{
   mapTo = "unmapped_mat";
   diffuseMap[0] = "./Grid_512_forestgreen_lines.png";
   tags = "Editor";
};
singleton Material(Grid_512_Orange_Lines_Mat)
{
   mapTo = "unmapped_mat";
   diffuseMap[0] = "./Grid_512_orange_lines.png";
   tags = "Editor";
};