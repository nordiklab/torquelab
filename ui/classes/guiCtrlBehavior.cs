//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================


//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function LockedInside::onVisible( %this )
{
	%this.forceInsideParent();
}
//------------------------------------------------------------------------------
//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function LockedInside::onWake( %this )
{
	%this.forceInsideParent();
}
//------------------------------------------------------------------------------

//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function LockedInside::onResize( %this )
{
	//%this.forceInsideParent();
	%this.forceInsideParent();
}
//------------------------------------------------------------------------------

//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function LockedInside::onDropped( %this )
{
	//%this.forceInsideParent();
	%this.forceInsideParent();
}
//------------------------------------------------------------------------------


//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function FitInsideWidth::onVisible( %this )
{
	%this.fitIntoParent("Width");
}
//------------------------------------------------------------------------------
//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function FitInsideWidth::onWake( %this )
{
	%this.fitIntoParent("Width");
}
//------------------------------------------------------------------------------


//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function FitInside::onVisible( %this )
{
	%this.fitIntoParent();
}
//------------------------------------------------------------------------------
//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function FitInside::onWake( %this )
{
	%this.fitIntoParent();
}
//------------------------------------------------------------------------------

//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function FitInsideWidth::onVisible( %this )
{
	%this.fitIntoParent("Width","8 8");
}
//------------------------------------------------------------------------------
//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function FitInsideWidth::onWake( %this )
{
	%this.fitIntoParent("Width","8 8");
}
//------------------------------------------------------------------------------
//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function FitInsideHeight::onVisible( %this )
{
	%this.fitIntoParent("height","8 8");
}
//------------------------------------------------------------------------------
//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function FitInsideHeight::onWake( %this )
{
	%this.fitIntoParent("height","8 8");
}
//------------------------------------------------------------------------------