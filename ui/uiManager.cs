//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$LabGuiTypeContainer["Root"] = "EditorGui";
$LabGuiTypeContainer["Tool"] = "ToolsContainer";
$LabGuiTypeContainer["Toolbar"] = "ToolbarContainer";
$LabGuiTypeContainer["FullEditor"] = "FullEditorContainer";
$LabGuiTypeContainer["EditorGui"] = "EditorContainer";
$LabGuiTypeContainer["SideBar"] = "SideBarContainer";
$LabGuiTypeContainer["Dialog"] = "DialogContainer";
$LabGuiTypeContainer["Palette"] = "PaletteContainer";
$LabGuiTypeContainer["PaletteStack"] = "PaletteContainer";
$LabGuiTypeContainer["EditorBox"] = "EditorBoxContainer";
$LabGuiTypeContainer["FullBox"] = "FullBoxContainer";

//==============================================================================
function Lab::initEditorGuiSets(%this)
{
	newScriptObject("LabEditor");
	newSimSet("EditorPluginSet",LabGroup,"EditorPlugin","ESet");
	//Group holding all Gui Sets
	newSimGroup("LabGuiTypesGroup");

	//Group holding parentless GUIs detached from editor
	newSimGroup("LabDetachedGuiGroup");

	//==============================================================================
	// LabAllGuiSet shared same GUIs of combined LabGuiSet and LabCoreGuiSet
	//==============================================================================
	//Holding all GUIs added to TorqueLab (LabGuiSet + LabCoreGuiSet)
	newSimSet("LabAllGuiSet",LabGuiTypesGroup,"LabAllGui","ESet");
	//Holding all standard GUIs added to TorqueLab
	newSimSet("LabGuiSet",LabGuiTypesGroup,"LabGui","ESet");
	//Holding all TorqueLab core GUIs - Can contain LabGuiSet Guis so need own set
	newSimSet("LabCoreGuiSet",LabGuiTypesGroup,"LabCoreGui","ESet");
	//------------------------------------------------------------------------------

	//Special Gui Set containing plugin related GUIs
	newSimSet("LabPluginGuiSet",LabGuiTypesGroup);

	LabDetachedGuiGroup.isTemporaryParent = true;
	GuiGroup.isTemporaryParent = true;
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::addToTypeGuiSet(%this,%type,%gui)
{
	%simSet = "Lab"@%type@"GuiSet";

	if (!isObject(%simSet))
		%simSet = newSimSet(%simSet,LabGuiTypesGroup);

	%simset.add(%gui);
	return %simSet;
}
//------------------------------------------------------------------------------
//==============================================================================
// Add the various TorqueLab GUIs to the container and set they belong
function Lab::addGui(%this,%gui,%type,%noHide)
{
	%container = $LabGuiTypeContainer[%type];
	Lab.addExternGui(%gui,%type,%container);
}
//==============================================================================
function Lab::addExternGui(%this,%gui,%type,%container)
{
	%gui.defaultParent = (isObject(%gui.parentGroup)) ? %gui.parentGroup : LabDetachedGuiGroup;
	//If no container specified, check for the %gui.container
	%container = (%container $= "") ? %gui.container : %container;

	LabAllGuiSet.add(%gui); // Simset Holding all Editor Guis
	LabGuiSet.add(%gui);

	if (%type $= "" || %container $= "")
		return warnlog("No type or container specified for GUI:",%gui.getName(),"Type:" SPC %type,"Container:" SPC %container, "Gui not added to Editor");

	%simSet = %this.addToTypeGuiSet(%type,%gui);

	Lab.addGuiToContainer(%gui,%container);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::addCoreGui(%this,%gui,%container)
{
	LabAllGuiSet.add(%gui); // Simset Holding all Editor Guis
	LabCoreGuiSet.add(%gui);

	//If no container specified, check for the %gui.container
	%gui.defaultParent = LabDetachedGuiGroup;
	%gui.editorType = Core;

	//If a container is specified, find it in editorGui and add he gui
	Lab.addGuiToContainer(%gui,%container);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::addGuiToContainer(%this,%gui,%container)
{
	%objContainer = EditorGui.findObjectByInternalName(%container,true);

	if (!isObject(%objContainer))
		return warnLog("Editor Container not found:",""@%container,"For GUI",%gui.getName());

	//Store container internal name so if changing framework, it will still be valid
	%gui.container = %container;
	%objContainer.add(%gui);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::dumpGuis(%this)
{
	foreach(%obj in LabGuiTypesGroup)
	{
		%guiSet = %obj.getName();
		info("================================");
		info("Extern GuiSet", %guiSet,"Containing",%obj.getCount() SPC "Guis");
		info("--------------------------------");
		%guiId = 0;

		foreach(%gui in %obj)
		{
			info(%guiSet,"Gui #"@%guiId++,%gui.getName());
		}
	}

	info("================================");
	info("LabGuiSet Guis","Total Guis=",LabGuiSet.getCount());
	info("--------------------------------");
	%guiId = 0;

	foreach(%gui in LabGuiSet)
	{
		info("LabGuiSet","Gui #"@%guiId++,%gui.getName(),"DefaultParent",%gui.defaultParent@"","Editor COntainer",%gui.container@"");
	}

	info("================================");
	info("LabCoreGuiSet Guis","Total Guis=",LabCoreGuiSet.getCount());
	info("--------------------------------");
	%guiId = 0;

	foreach(%gui in LabCoreGuiSet)
	{
		info("LabCoreGuiSet","Gui #"@%guiId++,%gui.getName(),"DefaultParent",%gui.defaultParent@"","Editor COntainer",%gui.container@"");
	}
}
//Lab.fixMissingContainer(ForestEditorPaletteStack,"ForestEditorPalette");

function Lab::fixMissingContainer(%this,%gui,%name)
{
	if (%name $= "")
		%name = %gui.getName()@"Gui";

	%fileName = %gui.getFilename();
	%container =  new GuiContainer(%name)
	{
		docking = "Client";
		margin = "0 0 4 0";
		padding = "0 0 0 0";
		useAnchoring = "0";
		anchorTop = "1";
		anchorBottom = "0";
		anchorLeft = "1";
		anchorRight = "0";
		position = "5 1";
		extent = "274 818";
		minExtent = "8 2";
		horizSizing = "right";
		vertSizing = "bottom";
		profile = "GuiDefaultProfile";
		visible = "0";
		active = "1";
		tooltipProfile = "ToolsToolTipProfile";
		hovertime = "1000";
		isContainer = "1";
		superClass = "FrameSetPluginTools";
		hidden = "1";
		canSave = "1";
		canSaveDynamicFields = "0";
	};
	%container.setFileName(%fileName);
	%container.add(%gui);
	%gui.defaultParent = %container.getId();
	return %container;
}
