//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$LabCfg_Layout_LeftMinWidth = "220";
$LabCfg_Layout_RightMinWidth = "280";
//$LabLayout_List = "FullEditorContainer WorldContainer EditorContainer ExtraContainer DialogContainer ToolsContainer EditorBoxContainer SideBarContainer";
$LabLayout_FitParentList = "FullEditorContainer EditorContainer DialogContainer EditorBoxContainer";

//------------------------------------------------------------------------------
