//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$LabLayoutCoreModules = "EWorldEditor ETerrainEditor LabPluginBar LabPalettes LabPluginThrash LabSideBar LabBottomBar StackStartToolbar StackEndToolbar";
$LabLayoutModuleContainer["EWorldEditor"] = "FullEditor";
$LabLayoutModuleContainer["ETerrainEditor"] = "FullEditor";
$LabLayoutModuleContainer["LabPluginBar"] = "Extra";
$LabLayoutModuleContainer["LabPalettes"] = "Extra";
$LabLayoutModuleContainer["LabPluginThrash"] = "Extra";
$LabLayoutModuleContainer["LabSideBar"] = "SideBar";
$LabLayoutModuleContainer["StackStartToolbar"] = "Toolbar";
$LabLayoutModuleContainer["StackEndToolbar"] = "Toolbar";
$LabLayoutModuleContainer["LabBottomBar"] = "World";

//==============================================================================
// Validate the core Gui Controls , called from initialGuiSetup
function Lab::checkLayoutCore(%this,%resetData)
{

	%missing = "";

	foreach$(%listedGui in $LabLayoutCoreModules)
	{
		%coreGui = %listedGui;

		if (!isObject(%coreGui))
		{
			%coreGui = Lab.resetLayoutCoreGui(%coreGui);
			%missing = strAddWord(%missing,%coreGui);
		}

		if (!isObject(%coreGui))
		{
			warnLog("A core UI features check have failed. The missing item is:",%listedGui);
			continue;
		}

		%parentInt = $LabLayoutModuleContainer[%coreGui.getName()]@"Container";
		Lab.addCoreGui(	%coreGui,%parentInt);

		//Since they could be stored inside EditorGui directly, we have to make
		//sure their default parent is not set to EditorGui child but to a temp group
		%coreGui.defaultParent = LabDetachedGuiGroup;
	}

	//if (%missing !$= "")
		//info("EditorCore Checked! Updated Missing Guis:",%missing);

	if (%resetData)
		%this.resetLayoutCore();
}
//------------------------------------------------------------------------------
//==============================================================================
// Make sure all GUIs are fine once the editor is launched
function Lab::resetLayoutCoreGui(%this,%name)
{
	%file = "tlab/ui/gui/core/"@%name@".gui";

	if (!isFile(%file))
		return;

	delObj(%name);
	exec(%file);
	return %name;
}
//------------------------------------------------------------------------------

//==============================================================================
// Emergency function to reset the Gui data after failure (Ex: Add plugins icon)
function Lab::resetLayoutCore(%this)
{
	tlog("Lab::resetLayoutCore");
	Lab.updatePluginsBar();
	Lab.initAllToolbarGroups();
	Lab.updatePluginsTools();
	Lab.updatePaletteBar();
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::checkLabGuiSet(%this)
{
	tlog("Lab::checkLabGuiSet");

	foreach(%obj in LabGuiSet)
	{
		if (!isFile(%obj.getFileName() ))
			warnLog(%obj.getName(),"as invalid filename:",%obj.getFileName());

		if ( %obj.getFileName() $= EditorGui.getFileName())
		{

			%file = "tlab/ui/gui/core/"@%obj.getName()@".gui";

			warnLog(%obj.getName(),"shouldn't be in LabGuiSet as it share same file as editorGui",%obj.getFileName(),"Changed to",%file);
			%obj.setFileName(%file);
			%obj.save(%file);
			warnLog(%obj.getName(),"New file",%obj.getFileName());
		}
	}
}
//------------------------------------------------------------------------------
