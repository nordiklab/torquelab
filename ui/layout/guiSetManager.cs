//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Detach/Attach all extern guis to EditorGui for framework switch or saving
//==============================================================================

//==============================================================================
function Lab::detachAllGuis(%this,%includeCore)
{
	foreach(%gui in LabAllGuiSet)
		Lab.setGuiDetached(%gui);

	foreach(%gui in LabCoreGuiSet)
		Lab.setGuiDetached(%gui);
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::attachAllGuis(%this,%delayed)
{
	foreach(%gui in LabCoreGuiSet)
		Lab.setGuiAttached(%gui);

	foreach(%gui in LabGuiSet)
		Lab.setGuiAttached(%gui);

	Lab.verifyLayout();
}
//------------------------------------------------------------------------------

function Lab::setGuiAttached(%this,%gui)
{
	%myContainer = EditorGui.findObjectByInternalName(%gui.container,true);

	if (!isObject(%myContainer))
	{
		info("Invalid container: "@%gui.container,"ForGui",%gui.getName(),"FIXME");
		return;
	}

	%gui.canSave = 0;

	if (!%myContainer.isMember(%gui))
		%myContainer.add(%gui);

	if (%gui.isMethod("onAttached"))
		%gui.onAttached();
}
function Lab::setGuiDetached(%this,%gui)
{
	%gui.canSave = 1;

	if (!isObject(%gui.defaultParent))
	{
		info("DefaultParent was invalid so LabDetachedGuiGroup is used");
		%gui.defaultParent = LabDetachedGuiGroup;
	}

	if (!%gui.defaultParent.isMember(%gui))
		%gui.defaultParent.add(%gui);

	if (%gui.isMethod("onDetached"))
		%gui.onDetached();
}

//==============================================================================

//==============================================================================
// Lab.layoutSaveAll
function Lab::layoutSaveAll(%this)
{

	foreach(%gui in LabAllGuiSet)
		Lab.saveExternGui(%gui);

	%this.saveGuiToFile(EditorGui);

}
//------------------------------------------------------------------------------

// Lab.layoutDetachGuis
function Lab::saveAllExternGuis(%this)
{
	foreach(%gui in LabCoreGuiSet)
		Lab.saveExternGui(%gui);

	foreach(%gui in LabAllGuiSet)
		Lab.saveExternGui(%gui);
}
//------------------------------------------------------------------------------
function Lab::saveExternGuiType(%this,%type,%echoOnly)
{
	%set = "Lab"@%type@"GuiSet";

	if (!isObject(%set))
		return;

	foreach(%gui in %set)
		Lab.saveExternGui(%gui,%echoOnly);
}
//------------------------------------------------------------------------------

//==============================================================================
// Lab.layoutDetachGuis
function Lab::saveExternGui(%this,%gui,%echoOnly)
{
	if (isObject(%gui.defaultParent) && !%gui.defaultParent.isTemporaryParent)
	{
		if (!%gui.defaultParent.isMember(%gui))
			%gui.defaultParent.add(%gui);

		%saveGui = %gui.defaultParent;
		info(" "@%gui.getName(),"Parent saving gui"," "@%gui.defaultParent.getName()," to file",%saveGui.getFileName());
	}
	else
	{
		//Should be a GUI with own file
		%saveGui = %gui;
		info(" "@%gui.getName(),"Self saving gui to file",%saveGui.getFileName());
	}

	if ( %saveGui.getFileName() $= EditorGui.getFileName())
	{
		errorLog(%saveGui.getName()@"","Share the same GUI file as EditorGui which is not good for extern GUIs","Saving aborted, try to fix it with manual saving");
		return;
	}

	%saveFileName = fileBase(%saveGui.getFileName());

	if (%saveFileName !$= %saveGui.getName())
	{
		%file = strreplace(%saveGui.getFileName(),%saveFileName,"tmp_"@%saveGui.getName());
		warnLog("Saving a GUI to a file with different name.","FileName",%saveFileName,"Gui Name",%saveGui.getName(),"Please fix it as it cause major conflict and is not proceeded anymore");
		errorLog("File requiring attention is:", %saveGui.getFileName(),"Overiding save file with:",%file);
		Lab.saveGuiToFile(%saveGui,%file);
		return;
	}

	if(%echoOnly)
		return;

	%disableSave = (!%gui.canSave) ? true : false;
	%gui.canSave = 1;

	if (!%saveGui.canSave)
	{
		warnLog("Trying to save a gui which cant save",%saveGui.getName());
	}

	//Lab.backupFileUser(%saveGui.getFileName());
	Lab.saveGuiToFile(%saveGui);

	if (%disableSave)
	{
		info(%gui.getName(),"CanSave set back to false after save");
		%gui.canSave = 0;
	}
}
//------------------------------------------------------------------------------
