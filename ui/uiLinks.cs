//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// DropTypeMenu - Listing the drop types
//==============================================================================
//==============================================================================
function SceneDropTypeMenu::onAdd(%this)
{
	ELink.link(%this,"SceneDropTypeMenu");
}
//==============================================================================
function SceneAlignMenu::onAdd(%this)
{
	ELink.link(%this,"SceneAlignMenu");
}
//==============================================================================
function SceneCamModeMenu::onAdd(%this)
{
	ELink.link(%this,"SceneCamModeMenu");

}

//==============================================================================
// Drag And Drop Common System
//==============================================================================

//==============================================================================
function DnDPanelDropZone::onAdd(%this)
{
	ELink.link(%this,"DnDPanelDropZone");

}

