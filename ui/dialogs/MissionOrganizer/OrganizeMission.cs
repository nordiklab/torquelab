//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Empty SimGroups removal system
//------------------------------------------------------------------------------
// Initial implementation, could be improved with cleaner script logic
//    -For multi-nested empty groups they all get deleted but deleting the parent
//          would delete them all in one shot
//==============================================================================

//==============================================================================
// If no group specified, the entire MissionGroup would be used. TestOnly will
// only return the list of SimGroup IDs that would be deleted.
//MisOrganizer.removeEmptyGroup
function MisOrganizer::removeEmptyGroup( %this,%group,%testOnly)
{
	if (%group $= "")
		%group = MissionGroup;

	%this.emptyGroups = "";
	%this.parseEmptyGroup(%group);

	if (%testOnly)
	{
		info("Would remove",getWordCount(%this.emptyGroups),"groups");
		info("List",%this.emptyGroups);
		return;
	}

	foreach$(%group in %this.emptyGroups)
	{
		%success = MisOrganizer.deleteEmptyGroup(%group);

		if (%success)
			%deleted++;
		else
			%notdeleted++;
	}

	if (%deleted > 0)
		info(%deleted,"empty group removed");

	if (%notdeleted > 0)
		info(%notdeleted,"group removal fail");
}
//------------------------------------------------------------------------------
//==============================================================================
// Go through all child groups to determine is group is empty. Needed to detect
// Empty child groups so we can remove them all unless something else is found
function MisOrganizer::parseEmptyGroup( %this,%group )
{
	foreach(%obj in %group)
		if (%obj.getClassName() $= "SimGroup")
			%this.parseEmptyGroup(%obj);

	if ( %this.checkEmptyGroup(%group))
		%this.emptyGroups = strAddWord(%this.emptyGroups,%group.getId(),true);

}
//------------------------------------------------------------------------------
//==============================================================================
// Return 1 if group is empty or only contain empty groups otherwise return 1
// NOTE: Could be setup to use true/false as the issue was found
function MisOrganizer::checkEmptyGroup( %this,%group )
{
	foreach(%obj in %group)
	{
		if (%obj.getClassName() !$= "SimGroup")
			return "0";

		//Seem to be not needed as it would be run to all groups with the parseEmptyGroup
		// or not?? Logic is a bit confusing here but it work
		if (%obj.getCount() > 0 && !MisOrganizer.checkEmptyGroup(%obj))
			return "0";
	}

	return "1";
}
//------------------------------------------------------------------------------

//==============================================================================
// Proceed with empty group deletion with some last checks to confirm it really empty
function MisOrganizer::deleteEmptyGroup( %this,%group )
{
	if (!isObject(%group))
	{
		warnLog("Tried to delete a inexistant group:",  %group);
		return false;
	}

	//Must be a SimGroup (Not a derived class)
	if (%group.getClassName() !$= "SimGroup")
	{
		warnLog("Tried to delete a not SimGroup empty group:",  %group);
		return false;
	}

	//Will validate empty group if it contain childs. Seem like it could do the
	// job alone by trying to delete all SimGroups...
	if (%group.getCount() > 0)
	{
		if ( !%this.checkEmptyGroup(%group))
		{
			warnLog("Tried to delete a not empty group:",  %group);
			return false;
		}
	}

	//Must be a child of mission group
	if (%group.getParentDepth(MissionGroup) $= "-1")
	{
		warnLog("Tried to delete a SimGroup which is not a child of mission group:",  %group);
		return false;
	}

	delObj(%group);
	return true;
}
