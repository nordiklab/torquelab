//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// LabDialog - Superclass for TorqueLab dialogs
//==============================================================================

//==============================================================================
// When a new LabDialog is added, call the addGui functions to set it up
function LabDialog::onAdd(%this)
{
	//Schedule the addGui so it's called after the exec gui is completed
	Lab.schedule(100,addGui,%this,"Dialog");
}
//------------------------------------------------------------------------------

//==============================================================================
// Toolbar - Superclass for TorqueLab Toolbars
//==============================================================================

