//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//LabPalettes.addPluginPalette("ShapeLabPalette");

//==============================================================================
function Lab::updatePaletteBar(%this)
{
	foreach(%paletteObj in LabPaletteGuiSet)
	{
		if (%paletteObj.getClassName() !$= "GuiStackControl")
		{
			warnlog("Trying to add a non-stacked palette",%paletteObj.getName());
			continue;
		}

		if (!LabPalettes.isMember(%paletteObj))
			LabPalettes.add(%paletteObj);

	}
}
//------------------------------------------------------------------------------

//==============================================================================
function LabPalettes::restoreAll(%this)
{
	foreach(%paletteObj in LabPaletteGuiSet)
	{
		Lab.setGuiDetached(%paletteObj);
	}

	return;
	%i = %this.getCount();

	for(; %i != 0; %i--)
	{
		%gui = %this.getObject(0);

		%defaultParent = %gui.defaultParent;

		if (!isObject(%defaultParent))
			continue;

		%defaultParent.add(%gui);
		%defaultParent.paletteLoaded = false;
	}
}
//------------------------------------------------------------------------------
//==============================================================================

//==============================================================================
function Lab::hidePluginPalettes(%this)
{
	hide(LabPalettes);
}
//==============================================================================
// Toggle the palette bar tools to activate those used by plugin
function Lab::togglePluginPalette(%this, %paletteName,%pluginObj)
{
	show(LabPalettes);
	LabPalettes.callOnChildrenNoRecurse("setVisible",false);

	%pluginPalette = LabPalettes.findObjectByInternalName(%pluginObj.plugin);

	if (isObject(%pluginPalette))
	{
		show(%pluginPalette);
		%pluginPalette.callOnChildrenNoRecurse("setVisible",true);
	}

	if (%pluginObj.sharedPalette !$= "")
	{
		%pluginPalette = LabPalettes.findObjectByInternalName(%pluginObj.sharedPalette);

		if (isObject(%pluginPalette))
		{
			show(%pluginPalette);
			%pluginPalette.callOnChildrenNoRecurse("setVisible",true);
		}
	}

}
//==============================================================================
// Toggle the palette bar tools to activate those used by plugin
function LabPalettes::pushButton(%this, %mode,%pluginObj)
{
	if (!isObject(%pluginObj))
		%pluginObj = Lab.currentEditor;

	if (!isObject(%pluginObj))
		return;

	%pluginPalette = LabPalettes.findObjectByInternalName(%pluginObj.plugin);

	if (!isObject(%pluginPalette))
	{
		warnLog("Palette not found for plugin:",%pluginObj.plugin);
		return;
	}

	%button = %pluginPalette.findObjectByInternalName(%mode);

	if (!isObject(%button))
	{
		warnLog("Palette button:",%mode,"not found for plugin:",%pluginObj.plugin);
		return;
	}

	%button.performClick();

}
//==============================================================================
function LabPalettes::getPluginStack(%this,%gui)
{
	//Should be a stack of icons, if not, add all child to a new stack
	if (%gui.getClassName() $= "GuiStackControl")
	{
		return %gui;
	}

	if (%gui.getObject(0).getClassName() !$= "GuiStackControl")
	{
		warnLog("Invalid Palette without Stack s root or first child:",  %gui.getName(),"FIXME PLS");
		return;

		if (%gui.getCount() <= 0)
		{
			warnLog("Empty palette for:",  %gui);
			return;
		}

		%guiStack = new GuiStackControl()
		{
			stackingType = "Vertical";
			horizStacking = "Left to Right";
			vertStacking = "Top to Bottom";
			padding = "0";
			dynamicSize = "1";
			dynamicNonStackExtent = "0";
			dynamicPos = "0";
			changeChildSizeToFit = "1";
			changeChildPosition = "1";
			position = "0 0";
			extent = "32 140";
			minExtent = "16 16";
			horizSizing = "right";
			vertSizing = "bottom";
			profile = "ToolsDefaultProfile";
		};

		%i = %gui.getCount();

		for(; %i != 0; %i--)
		{
			%gui.getObject(0).defaultParent = %gui;
			%gui.getObject(0).plugin = %gui.internalName;
			%guiStack.add( %gui.getObject(0));
		}
	}
	else
		%guiStack = %gui.getObject(0);

	return %guiStack;

}
//==============================================================================
// Handle the default Palette button clicks which check for current plugin method
// If no method, it will try the default actions
//==============================================================================
function PaletteButton::onClick(%this)
{
	%mode = %this.internalName;
	%currentPlugin = Lab.currentEditor;

	if (%currentPlugin.isMethod("handlePaletteClick"))
	{
		%currentPlugin.handlePaletteClick(%mode);
		return;
	}

	switch$(%mode)
	{
		case "select":
			GlobalGizmoProfile.mode = "None";
			EditorGuiStatusBar.setInfo("Scene object selection mode.");

		case "move":
			GlobalGizmoProfile.mode = "Move";
			EditorGuiStatusBar.setInfo("Move selection.  SHIFT while dragging duplicates objects.  " @ $KeyCtrl @ " to toggle soft snap.  ALT to toggle grid snap.");

		case "rotate":
			GlobalGizmoProfile.mode = "Rotate";
			EditorGuiStatusBar.setInfo("Rotate selection.");

		case "scale":
			GlobalGizmoProfile.mode = "Scale";
			EditorGuiStatusBar.setInfo("Scale selection.");
			
      case "none":
			GlobalGizmoProfile.mode = "None";
			EditorGuiStatusBar.setInfo("Gizmo set to nothing.");

	}

}

//==============================================================================
// Handle the default Palette button clicks which check for current plugin method
// If no method, it will try the default actions
//==============================================================================
function PalettePlugin::onAdd(%this)
{

}
function PaletteStack::onAdd(%this)
{
	//%this.defaultParent = %this.parentGroup;
	%this.parentGroup.myStack = %this;
	//Lab.addPluginPalette(%this.internalName,%this.parentGroup);
}
