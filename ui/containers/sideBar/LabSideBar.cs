//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$TLab_SideBar_Page["VIS"] = 0;
$TLab_SideBar_Page["ObjectCreator"] = 1;
$TLab_SideBar_Page["FileBrowser"] = 2;
//==============================================================================
// LabSideBar Functions
//==============================================================================

//==============================================================================
// Called when Editor UI is loaded
function LabSideBar::onWake(%this)
{
	Lab.setSideBarToggle();
}
//------------------------------------------------------------------------------
//==============================================================================
// Called when SideBar is toggled ON (not onWake since it's always awaken)
function LabSideBar::onVisible(%this)
{
	Lab.setSideBarToggle();
}
//------------------------------------------------------------------------------
function LabSideBar::onCtrlResized(%this)
{
	loge("LabSideBar::onCtrlResized",%this);
	Lab.setSideBarToggle();
}
function TLabSideContainer::onCtrlResized(%this)
{
	loge("TLabSideContainer::onCtrlResized",%this);
//Lab.setSideBarToggle();
}

//==============================================================================
function LabSideBar::onPreEditorSave(%this)
{
	FileBrowserArray.clear();
	ObjectCreatorArray.clear();
	hide(FileBrowserInfo);
	hide(FileBrowserIconFolderSrc);
	hide(FileBrowserIconSrc);
	hide(FileBrowserIconImgSrc);
	SideBarVIS.onPreSave();

	FileBrowser.toggleFavorites(0);
}
//------------------------------------------------------------------------------
function LabSideBar::onPostEditorSave(%this)
{

	//SideBarVIS.init();

	FileBrowser.initFiles();
	ObjectCreator.initFiles();
	SideBarVIS.onPostSave();

}
//==============================================================================
// SideBar Tab Book
//==============================================================================

//==============================================================================
//Set a plugin as active (Selected Editor Plugin)
function SideBarMainBook::onTabSelected(%this,%text,%id)
{
	$SideBarMainBook_CurrentPage = %id;

	switch$(%id)
	{
		case "0":
			if (!$FileBrowserInit && isObject(FileBrowser))
				Lab.initFileBrowser();

		case "1":
			Lab.schedule(200,"initSceneBrowser");
	}
}
//------------------------------------------------------------------------------
function LabSideBar::setPage(%this,%page)
{

	%pageIndex = $TLab_SideBar_Page[%page];

	if (%pageIndex $= "")
		return;

	SideBarMainBook.selectPage(%pageIndex);

}
