//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
//Called from Toolbar and TEManager
function Lab::postSideContainerAdd(%this)
{
	%this.setSideBarToggle();
}
//------------------------------------------------------------------------------

//==============================================================================
//Called from Toolbar and TEManager
function TLabToolContainer::onObjectAdded(%this,%obj)
{
	%obj.margin = setWord(%obj.margin,2,"4");
}
//------------------------------------------------------------------------------

//==============================================================================
//Called from Toolbar and TEManager
function TLabSideContainer::onObjectAdded(%this,%obj)
{
	%obj.margin = setWord(%obj.margin,3,"4");
}
//------------------------------------------------------------------------------
