//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//------------------------------------------------------------------------------
// Called from onInitialEditorLaunch to set the initial sidebar states
function Lab::initSideBar(%this)
{

	$SideBarVIS_Initialized = 0;

	Scene.registerObjects();

	SideBarVIS.init();
	SideBarVIS.onCtrlResized();

	Lab.initFileBrowser();
	Lab.initObjectCreator();
	FileBrowser.onCtrlResized();

	SideBarMainBook.selectPage($SideBarMainBook_CurrentPage);

	EditorGui-->SideBarContainer.bringToFront(LabSideBar);

}
//==============================================================================

//==============================================================================
function Lab::toggleSidebar(%this)
{
	if (LabSideBar.visible)
		Lab.closeSidebar();
	else
		Lab.openSidebar();
}
//==============================================================================
function Lab::openSidebar(%this,%check)
{

	//Set Window default extents
	%window = EditorGui-->SideBarContainer;
	//if (%window.openExtent !$= "")
	// %window.setExtent(%window.openExtent);

	show(%window);
	show(LabSideBar);
	hide(LabSideBarExpander);
	Lab.setSideBarToggle();

}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::closeSidebar(%this)
{

	%window = EditorGui-->SideBarContainer;
	%window.openExtent = %window.extent;

	//%window.setExtent("18",%window.extent.y);
	hide(%window);
	show(LabSideBarExpander);
	Lab.setSideBarToggle();
}
//------------------------------------------------------------------------------

//==============================================================================
//Called from Toolbar and TEManager
function Lab::getSideBarCloseButton(%this)
{

	%button =  new GuiBitmapButtonCtrl()
	{
		bitmap = "tlab/art/icons/set01/generic/16/arrows/arrow_triangle_left.png";
		bitmapMode = "Stretched";
		autoFitExtents = "0";
		useModifiers = "0";
		useStates = "1";
		groupNum = "-1";
		buttonType = "PushButton";
		useMouseEvents = "0";
		position = "188 0";
		extent = "11 15";
		minExtent = "8 2";
		horizSizing = "left";
		vertSizing = "bottom";
		profile = "ToolsDefaultProfile";
		command = "Lab.closeSideBar();";
		tooltipProfile = "GuiToolTipProfile";
		internalName = "SideBarToggle";

	};
	return %button;
}
//------------------------------------------------------------------------------

//==============================================================================
//LabSideBarNotify Unused since LabBoxCtrl use
function LabSideBarNotify::onParentResized(%this)
{

}
//------------------------------------------------------------------------------
