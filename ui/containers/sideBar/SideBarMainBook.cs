//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$SideBarMainBook_Page[0] = "FileBrowser";
$SideBarMainBook_Page[1] = "SceneBrowser";
$SideBarMainBook_CurrentPage = 1;
//==============================================================================
// Activate the interface for a plugin
//==============================================================================

//==============================================================================
function EToolDlgDecoyArea::onMouseLeave()
{
	EToolDlgCameraTypesToggle();
}
//------------------------------------------------------------------------------
