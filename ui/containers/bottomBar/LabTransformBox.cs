//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$LabCfg_TransformBox_ShowPosition = true;
$LabCfg_TransformBox_ShowScale = false;
$LabCfg_TransformBox_ShowRotation = true;
$LabCfg_TransformBox_EulerRotation = false;

//LabTransformBox.toggleSettings();
//==============================================================================
function LabTransformBox::toggleSettings(%this)
{
	if (LabTransformBoxSettings.visible)
		LabTransformBoxSettings.visible = 0;
	else
		LabTransformBox.openSettings();
}
//------------------------------------------------------------------------------
//==============================================================================
function LabTransformBox::openSettings(%this)
{
	LabTransformBoxSettings.visible = 1;
	LabTransformBoxSettings.AlignCtrlToParent("bottomright");

}
//------------------------------------------------------------------------------
//==============================================================================
function LabTransformBox::onSleep(%this)
{
}
function LabTransformBox::onWake(%this)
{
}
//------------------------------------------------------------------------------
//==============================================================================
function LabTransformBoxGui::initTool(%this)
{
	LabTransformBox.fitIntoParents();
	//LabTransformBox.position = "2000 0";
	hide(LabTransformBoxSettings);
	LabTransformBox.updateGui();
	//hide(LabTransformBox);
}
//------------------------------------------------------------------------------
//LabTransformBox.toggleBox
//==============================================================================
function LabTransformBox::toggleBox(%this)
{
	if (LabTransformBox.notActive)
		%this.activate();
	else
		%this.deactivate();
}
//------------------------------------------------------------------------------
//LabTransformBox.toggleSettings();
//==============================================================================

//==============================================================================
function LabTransformBox::activate(%this)
{
	LabTransformBoxSettings.visible = 0;
	//LabTransformBox.fitIntoParents();
	LabTransformBox.notActive = false;
	//show(ETools);
	//show(LabTransformBoxGui);
	show(%this);
	
	Lab.joinEvent("SceneObjectAddedChanged",LabTransformBox);
	Lab.joinEvent("SceneTreeChanged",LabTransformBox);
}
//------------------------------------------------------------------------------
//==============================================================================
function LabTransformBox::deactivate(%this)
{
	%this.notActive = true;
	hide(%this);
}
//------------------------------------------------------------------------------
//==============================================================================
function LabTransformBox::onSceneObjectAddedChanged(%this,%obj)
{
	%objs = SceneEditorTree.getSelectedObjectList();

	//if (%objSet.getCount() <= 0)
	// return;
	//%first = %objSet.getObject(0);

	%first = firstWord(%objs);
	%this.updateSource(%first);

}
//------------------------------------------------------------------------------
//==============================================================================
function LabTransformBox::onSceneTreeChanged(%this,%objSet,%centroid)
{
	%objs = SceneEditorTree.getSelectedObjectList();

	//if (%objSet.getCount() <= 0)
	// return;
	//%first = %objSet.getObject(0);

	%first = firstWord(%objs);
	%this.updateSource(%first);

}
//------------------------------------------------------------------------------
//==============================================================================
function LabTransformBox::updateGui(%this)
{
	LabTransformBox.fitIntoParents();
	LabTransformBoxGui.forceInsideParent();
	LabTransformBoxGui-->pos.setVisible($LabCfg_TransformBox_ShowPosition);
	//%this-->scale.setVisible($LabCfg_TransformBox_ShowPosition);
	LabTransformBoxGui-->rot.setVisible(0);
	LabTransformBoxGui-->euler.setVisible(0);

	if ($LabCfg_TransformBox_ShowRotation)
	{
		LabTransformBoxGui-->rot.setVisible(!$LabCfg_TransformBox_EulerRotation);
		LabTransformBoxGui-->euler.setVisible($LabCfg_TransformBox_EulerRotation);
	}
	else
	{
		LabTransformBoxGui-->rot.setVisible(0);
		LabTransformBoxGui-->euler.setVisible(0);
	}

	LabTransformBoxStack.updateStack();
	%this.setSettingsPos();

}
//------------------------------------------------------------------------------

//==============================================================================
function LabTransformBox::updateSource(%this,%sourceObj)
{
	if (!isObject(%sourceObj) || %this.notActive)
	{
		hide(LabTransformBox);
		return;
	}

	%this.fitIntoParents();
	show(%this);
	%this.sourceObject = %sourceObj;
	%posCtrl = LabTransformBoxGui-->pos;
	%sourcePos = %sourceObj.position;

	foreach$(%axis in "x y z")
	{
		eval("%posCtrl"@%axis@" = %posCtrl-->"@%axis@";");
		eval("%posCtrl"@%axis@".setText(%sourcePos."@%axis@");");
	}

	%rotCtrl = LabTransformBoxGui-->rot;
	%objRot = %sourceObj.rotation;

	foreach$(%axis in "x y z a")
	{
		eval("%rotCtrl"@%axis@" = %rotCtrl-->"@%axis@";");
		eval("%rotCtrl"@%axis@".setText(%objRot."@%axis@");");
	}

	%eulerCtrl = LabTransformBoxGui-->euler;
	%eulerRot = rotationToEuler(%objRot);

	foreach$(%axis in "x y z")
	{
		eval("%eulerCtrl"@%axis@" = %eulerCtrl-->"@%axis@";");
		eval("%eulerCtrl"@%axis@".setText(%eulerRot."@%axis@");");
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function LabTransformBox::updateRotation(%this,%axis,%value)
{
	if (!isObject(%this.sourceObject))
	{
		//hide(LabTransformBox);
		return;
	}

	%sourceObj = %this.sourceObject;
	%objPos = getWords(%sourceObj.getTransform(),0,2);
	%objRot = getWords(%sourceObj.getTransform(),3,6);
	eval("%objRot."@%axis@" = %angle;");
	%newTransform = %objPos SPC %objRot;
	%sourceObj.setTransform(%newTransform);
}
//------------------------------------------------------------------------------
//==============================================================================
function LabTransformBox::updateEuler(%this,%axis,%angle)
{
	if (!isObject(%this.sourceObject))
	{
		//hide(LabTransformBox);
		return;
	}

	%sourceObj = %this.sourceObject;
	%objRot = getWords(%sourceObj.getTransform(),3,6);
	%sourceRot = rotationToEuler(%objRot);
	eval("%sourceRot."@%axis@" = %angle;");
	testEulerFromAxisAngle(%sourceRot);
	%newRot = MatrixCreateFromEuler(%sourceRot);
	%newRot = getWords(%newRot,3,6);
	%transform = %sourceObj.getTransform();
	%pos = getWords(%transform,0,2);
	%newTransform = %pos SPC %newRot;
	%sourceObj.setTransform(%newTransform);
}
//------------------------------------------------------------------------------
//==============================================================================
function LabTransformBox::updatePosition(%this,%axis,%pos)
{
	if (!isObject(%this.sourceObject))
	{
		//hide(LabTransformBox);
		return;
	}

	%sourceObj = %this.sourceObject;
	%objRot = getWords(%sourceObj.getTransform(),3,6);
	%objPos = getWords(%sourceObj.getTransform(),0,2);
	eval("%objPos."@%axis@" = %pos;");
	%newTransform = %objPos SPC %objRot;
	%sourceObj.setTransform(%newTransform);
}
//------------------------------------------------------------------------------
//==============================================================================
function LabTransformBox::refresh(%this)
{
	LabTransformBoxStack.updateStack();
}
//------------------------------------------------------------------------------
//==============================================================================
function LabTransformBoxRotationMode::OnClick(%this)
{
	if (%this.internalName $= "Euler")
		$LabCfg_TransformBox_EulerRotation = true;
	else
		$LabCfg_TransformBox_EulerRotation = false;

	LabTransformBox.updateGui();
}
//------------------------------------------------------------------------------
//==============================================================================
function LabTransformBoxRot::OnValidate(%this)
{
	LabTransformBox.updateRotation(%this.internalName,%this.getText());
}
//------------------------------------------------------------------------------
//==============================================================================
function LabTransformBoxEuler::OnValidate(%this)
{
	LabTransformBox.updateEuler(%this.internalName,%this.getText());
}
//------------------------------------------------------------------------------
//==============================================================================
function LabTransformBoxPos::OnValidate(%this)
{
	LabTransformBox.updatePosition(%this.internalName,%this.getText());
}
//------------------------------------------------------------------------------
//==============================================================================
function LabTransformBoxDrag::onMouseDragged(%this,%a1,%a2,%a3)
{
	startBasicDnD(LabTransformBoxGui);
	//startDragAndDropCtrl(LabTransformBoxGui);
	hide(LabTransformBoxGui);
}
//------------------------------------------------------------------------------
//==============================================================================
function LabTransformBoxCtrl::DragSuccess(%this,%droppedCtrl,%pos)
{
	show(%this);

	if (%droppedCtrl.droppedGui !$= "")
	{

		if (isObject(%droppedCtrl.droppedGui))
			%droppedCtrl.droppedGui.add(%this);

		return;
	}

	%realpos = EditorGui-->WorldContainer.getRealPosition();
	%pos.x = %pos.x - %this.extent.x/2- %realpos.x;
	%pos.y = %pos.y - %this.extent.y/2 - %realpos.y;
	%this.setPosition(%pos.x,%pos.y);
	//%this.forceInsideCtrl(EditorGui-->WorldContainer);
	LabTransformBox.refresh();
	LabTransformBox.updateGui();
}

function LabTransformBoxCtrl::droppedOnCtrl(%this,%droppedOn,%pos)
{
}
