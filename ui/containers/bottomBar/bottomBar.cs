//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// PerformClick on a toolbar icon based on internalName and optional subBar
//------------------------------------------------------------------------------
// wToolbar.press("centerObject");
// Lab.pushToolbar("centerObject");
//==============================================================================
function LabBottomBar::checkVisible(%this)
{
	%show = "0";

	foreach(%gui in %this)
	{
		if (%gui.visible)
		{
			%show = "1";
			break;
		}
	}

	if (%this.visible !$= %show)
		%this.setVisible(%show);
}
//------------------------------------------------------------------------------
//==============================================================================
function LabBottomBar::toggleTool(%this,%tool)
{
	%toolGui = %this.findObjectByInternalName(%tool);

	if (isObject(%toolGui))
		%this.toggleToolGui(%toolGui);

	%this.checkVisible();
}
//------------------------------------------------------------------------------
//==============================================================================
function LabBottomBar::setTool(%this,%tool)
{
	%toolGui = %this.findObjectByInternalName(%tool);

	if (isObject(%toolGui))
		%this.toggleToolGui(%toolGui,%toolGui.visible);

	%this.checkVisible();
}
//------------------------------------------------------------------------------
//==============================================================================
function LabBottomBar::toggleToolGui(%this,%toolGui,%visible)
{

	if (!isObject(%toolGui))
		return;

	if (%visible $= "")
		%visible =!%toolGui.visible;

	%toolGui.visible = !%toolGui.visible;

	if (%toolGui.isMethod("activate") && %toolGui.visible)
		%toolGui.activate();
	else if (%toolGui.isMethod("deactivate") && !%toolGui.visible)
		%toolGui.deactivate();
}
//------------------------------------------------------------------------------
