//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Called after the Guis have been reattached to the Editor Gui to adapt som GUIs
function Lab::checkContainersSizes(%this)
{

	foreach$(%container in $LabLayout_FitParentList)
	{
		if (isObject(EditorGui.findObjectByInternalName(%container,1)))
			EditorGui.findObjectByInternalName(%container,1).fitIntoParents();
	}
}
//------------------------------------------------------------------------------

function Lab::setDefaultLayoutSize(%this, %onlyIfSmaller)
{

	EditorGui-->SideBarContainer.minExtent = $LabCfg_Layout_LeftMinWidth SPC "100";
	EditorGui-->ToolsContainer.minExtent = $LabCfg_Layout_RightMinWidth SPC "100";

	if (!%onlyIfSmaller || $LabCfg_Layout_RightMinWidth >  EditorGui-->ToolsContainer.extent.x)
		EditorGui-->ToolsContainer.setExtent($LabCfg_Layout_RightMinWidth,EditorGui-->ToolsContainer.extent.y);

	if (!%onlyIfSmaller || $LabCfg_Layout_LeftMinWidth >  EditorGui-->SideBarContainer.extent.x)
		EditorGui-->SideBarContainer.setExtent($LabCfg_Layout_LeftMinWidth,EditorGui-->SideBarContainer.extent.y);
}
//==============================================================================
// SideBar Container
//==============================================================================
//==============================================================================
//Called from Toolbar and TEManager
function Lab::setSideBarWidth(%this,%width)
{
	if (%width $= "")
	{
		if ($FW_SideBarWidth $= "")
			%width = $Cfg_UI_Editor_SideFrameWidth;
		else
			%width = $FW_SideBarWidth;
	}

	%sideCtrl = EditorGui-->SideBarContainer;
	%sideCtrl.setExtent(%width,%sideCtrl.extent.y);
	$FW_SideBarWidth = %width;
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::setSideBarToggle(%this)
{
	%sideContainer =  EditorGui-->SideBarContainer;
	%button = %sideContainer->SideBarToggle;

	if (!isObject(%button))
	{
		%button = %this.getSideBarCloseButton();
		%sideContainer.add(%button);
	}

	%sideContainer.pushToBack(%button);
	%button.AlignCtrlToParent("right","4");
	%button.AlignCtrlToParent("top","4");
	%button.visible = 1;
}
//------------------------------------------------------------------------------

//==============================================================================
// Tools Container
//==============================================================================
//==============================================================================
//Called from Toolbar and TEManager
function Lab::setToolsWidth(%this,%width)
{
	if (%width $= "")
	{
		if ($FW_ToolsWidth $= "")
			%width = $Cfg_UI_Editor_ToolFrameWidth;
		else
			%width = $FW_ToolsWidth;
	}

	%sideCtrl = EditorGui-->ToolsContainer;

	%sideCtrl.setExtent(%width,%sideCtrl.extent.y);
	$FW_ToolsWidth = %width;
}
//------------------------------------------------------------------------------

