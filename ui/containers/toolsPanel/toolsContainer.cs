//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Plugin GuiFrameSetCtrl Functions
//==============================================================================
//==============================================================================

//==============================================================================
//Called from Toolbar and TEManager
function Lab::togglePluginTools(%this)
{
	%window = EditorGui-->ToolsContainer;

	if (%window.visible)
	{
		%this.hidePluginTools();
		return false;
//	} else if (!Lab.currentEditor.useTools) {
		//	return false;
	}
	else
	{
		%this.showPluginTools();
		return true;
	}
}
//------------------------------------------------------------------------------

//==============================================================================
//Called from EditorPlugin::activateGui
function Lab::checkPluginTools(%this)
{
	//First check if the plugins support the tools container
	%currentPlugin = Lab.currentEditor;

	if (!%currentPlugin.useTools)
	{
		%this.hidePluginTools();
		return;
	}

	%window = EditorGui-->ToolsContainer;

	//if (!%window.visible)
	//{
	%this.showPluginTools();
	//}
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::hidePluginTools(%this)
{
	//EditorFrameContent.lastToolsCol = getWord(EditorFrameContent.columns,2);
	%window = EditorGui-->ToolsContainer;
	hide(%window);

	Lab.setToolsExpandButton(%window);

}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::showPluginTools(%this)
{
	%window = EditorGui-->ToolsContainer;
	show(%window);
	Lab.setToolsExpandButton(%window);
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::isShownPluginTools(%this)
{

	return EditorGui-->ToolsContainer.visible;
}
//------------------------------------------------------------------------------

//==============================================================================
// EditorFrameMain  - Tool Frame (Right Column)
//==============================================================================

//==============================================================================
//Called from Toolbar and TEManager
function Lab::postToolsContainerAdd(%this)
{
	%toolCtrl = EditorGui-->ToolsContainer;

	%this.setToolsToggleButton(%toolCtrl);
}
//------------------------------------------------------------------------------
//==============================================================================
//Called from Toolbar and TEManager
function Lab::setToolsToggleButton(%this,%toolCtrl)
{
	%container = %toolCtrl.getParent();
	%button = EditorGuiMain-->ToolsToggle;
	EditorGuiMain-->ToolsExpander.visible = 0;

	if (!isObject(%button))
	{
		%button = %this.getToolsToggleButton();

	}

	%container.add(%button);
	%button.docking = "right";
	%container.pushToBack(%button);
	//%button.AlignCtrlToParent("right","4");
	//%button.AlignCtrlToParent("top","4");

	%button.visible = !%toolCtrl.noToggleButton;

}
//------------------------------------------------------------------------------

//==============================================================================
//Called from Toolbar and TEManager
function Lab::getToolsToggleButton(%this)
{

	%button =  new GuiContainer()
	{
		docking = "Left";
		margin = "0 0 0 0";
		padding = "0 0 0 0";
		anchorTop = "1";
		anchorBottom = "0";
		anchorLeft = "1";
		anchorRight = "0";
		position = "3 23";
		extent = "10 792";
		minExtent = "8 2";
		horizSizing = "right";
		vertSizing = "bottom";
		profile = "ToolsDefaultProfile";
		visible = "1";
		active = "1";
		tooltipProfile = "GuiToolTipProfile";
		hovertime = "1000";
		isContainer = "1";
		internalName = "ToolsToggle";
		canSave = "1";
		canSaveDynamicFields = "0";

		new GuiButtonCtrl()
		{
			groupNum = "-1";
			buttonType = "PushButton";
			useMouseEvents = "0";
			position = "0 0";
			extent = "10 820";
			minExtent = "8 2";
			horizSizing = "right";
			vertSizing = "bottom";
			profile = "ToolsButtonBase";
			visible = "1";
			active = "1";
			tooltipProfile = "GuiToolTipProfile";
			hovertime = "1000";
			isContainer = "1";
			superClass = "TEToolsCollapseBar";
			canSave = "1";
			canSaveDynamicFields = "0";

			new GuiIconButtonCtrl()
			{
				buttonMargin = "1 1";
				iconBitmap = "tlab/art/icons/set01/generic/16/arrows/arrow_triangle_right.png";
				iconLocation = "Center";
				sizeIconToButton = "1";
				makeIconSquare = "0";
				textLocation = "Center";
				textMargin = "4";
				autoSize = "0";
				groupNum = "-1";
				buttonType = "PushButton";
				useMouseEvents = "0";
				position = "0 385";
				extent = "10 49";
				minExtent = "8 2";
				horizSizing = "right";
				vertSizing = "center";
				profile = "ToolsButtonBase";
				visible = "1";
				active = "1";
				tooltipProfile = "GuiToolTipProfile";
				hovertime = "1000";
				isContainer = "0";
				superClass = "TEToolsCollapseButton";
				canSave = "1";
				canSaveDynamicFields = "0";
			};
		};
	};
	return %button;
}
//------------------------------------------------------------------------------

//==============================================================================
//Called from Toolbar and TEManager
function Lab::setToolsExpandButton(%this,%toolCtrl)
{
	%container = %toolCtrl.getParent();
	%button = EditorGuiMain-->ToolsExpander;
	EditorGuiMain-->ToolsToggle.visible = 0;

	if (!isObject(%button))
	{
		%button = %this.getToolsExpandButton();
		%container.add(%button);
	}

	%button.docking = "right";
	%container.pushToBack(%button);
	//%button.AlignCtrlToParent("right","4");
	//%button.AlignCtrlToParent("top","4");
	%button.visible = !%toolCtrl.visible;
}
//------------------------------------------------------------------------------
//==============================================================================
//Called from Toolbar and TEManager
function TEToolsCollapseBar::onClick(%this)
{
	Lab.togglePluginTools();

}
//------------------------------------------------------------------------------
//==============================================================================
//Called from Toolbar and TEManager
function TEToolsCollapseButton::onClick(%this)
{
	Lab.togglePluginTools();

}
//------------------------------------------------------------------------------

//==============================================================================
//Called from Toolbar and TEManager
function TEToolsExpandBar::onClick(%this)
{
	Lab.togglePluginTools();

}
//------------------------------------------------------------------------------
//==============================================================================
//Called from Toolbar and TEManager
function TEToolsExpandIcon::onClick(%this)
{
	Lab.togglePluginTools();

}
//------------------------------------------------------------------------------
//==============================================================================
//Called from Toolbar and TEManager
function Lab::getToolsExpandButton(%this)
{

	%button =  new GuiContainer()
	{
		docking = "Right";
		margin = "0 0 0 0";
		padding = "0 0 0 0";
		anchorTop = "1";
		anchorBottom = "0";
		anchorLeft = "1";
		anchorRight = "0";
		position = "1071 0";
		extent = "10 820";
		minExtent = "8 2";
		horizSizing = "right";
		vertSizing = "bottom";
		profile = "ToolsDefaultProfile";
		visible = "1";
		active = "1";
		tooltipProfile = "GuiToolTipProfile";
		hovertime = "1000";
		isContainer = "1";
		internalName = "ToolsExpander";
		canSave = "1";
		canSaveDynamicFields = "0";

		new GuiButtonCtrl()
		{
			groupNum = "-1";
			buttonType = "PushButton";
			useMouseEvents = "0";
			position = "0 0";
			extent = "10 820";
			minExtent = "8 2";
			horizSizing = "right";
			vertSizing = "bottom";
			profile = "ToolsButtonBase";
			visible = "1";
			active = "1";
			tooltipProfile = "GuiToolTipProfile";
			hovertime = "1000";
			isContainer = "1";
			superClass = "TEToolsExpandBar";
			canSave = "1";
			canSaveDynamicFields = "0";

			new GuiIconButtonCtrl(f1)
			{
				buttonMargin = "1 1";
				iconBitmap = "tlab/art/icons/set01/generic/16/arrows/arrow_triangle_left.png";
				iconLocation = "Center";
				sizeIconToButton = "1";
				makeIconSquare = "0";
				textLocation = "Center";
				textMargin = "4";
				autoSize = "0";
				groupNum = "-1";
				buttonType = "PushButton";
				useMouseEvents = "0";
				position = "0 385";
				extent = "10 49";
				minExtent = "8 2";
				horizSizing = "right";
				vertSizing = "center";
				profile = "ToolsButtonBase";
				visible = "1";
				active = "1";
				tooltipProfile = "GuiToolTipProfile";
				hovertime = "1000";
				isContainer = "0";
				superClass = "TEToolsExpandIcon";
				canSave = "1";
				canSaveDynamicFields = "0";
			};
		};
	};
	return %button;
}
//------------------------------------------------------------------------------

