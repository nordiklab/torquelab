//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Start dragging a toolbar icon
function ToolbarIcon::onMouseDragged(%this,%a1,%a2,%a3)
{
	Lab.startToolbarDrag(%this,%a1,%a2,%a3);
}
//------------------------------------------------------------------------------

//==============================================================================
// Start dragging a toolbar icons group
function ToolbarBoxChild::onMouseDragged(%this,%a1,%a2,%a3)
{
	%src = %this.parentGroup;
	Lab.startToolbarDrag(%src,%a1,%a2,%a3);
}
//------------------------------------------------------------------------------

//==============================================================================
// Start dragging a toolbar icons group
function MouseStart::onMouseDragged(%this,%a1,%a2,%a3)
{
	%group = %this.parentGroup.parentGroup;

	if (%group.getClassName() !$= "GuiStackControl")
		return;

	Lab.startToolbarDrag(%group,%a1,%a2,%a3);
}
//------------------------------------------------------------------------------
//==============================================================================
// Start dragging a toolbar icons group
function MouseBox::onMouseDragged(%this,%a1,%a2,%a3)
{
	%group = %this.iconStack;
	Lab.startToolbarDrag(%group,%a1,%a2,%a3);
}
//------------------------------------------------------------------------------
//==============================================================================
// OLD: Replaced with MouseStart
function ToolbarGroup::onMouseDragged(%this,%a1,%a2,%a3)
{
	%group = %this.parentGroup.parentGroup;

	if (%group.getClassName() !$= "GuiStackControl")
		return;

	Lab.startToolbarDrag(%group,%a1,%a2,%a3);
}
//------------------------------------------------------------------------------
//==============================================================================
// Start dragging a toolbar icons group
function Lab::startToolbarDrag(%this,%ctrl,%a1,%a2,%a3)
{
	if (!isObject(%this))
	{
		return;
	}

	if (%ctrl.parentGroup.superClass !$= "EToolbarTrashClass")
	{
		%ctrl.originalParent = %ctrl.parentGroup;
		%ctrl.originalIndex = %ctrl.parentGroup.getObjectIndex(%ctrl);
	}

	if (%ctrl.getClassName() $= "GuiStackControl")
	{
		%callback = "Lab.onToolbarGroupDroppedDefault";
	}
	else
	{
		%callback = "Lab.onToolbarIconDroppedDefault";
	}

	startDragAndDropCtrl(%ctrl,"Toolbar",%callback);
	hide(%ctrl);
	Lab.showDisabledToolbarDropArea(%ctrl);
}
//------------------------------------------------------------------------------

//==============================================================================

//==============================================================================
//==============================================================================
// Dragged control dropped over undefined control (gonna check if it's droppable)
function Lab::findObjectToolbarStack(%this,%checkObj,%defaultPluginStack)
{
	%checkId = 0;

	while(%checkId < 5)
	{
		%result = %this.checkObjForToolbarStack(%checkObj,%defaultPluginStack);

		if (%result && %defaultPluginStack)
			return %checkObj SPC %previousObj;
		else if (%result)
			return %checkObj;

		%previousObj = %checkObj;
		%checkObj = %checkObj.parentGroup;
		%checkId++;
	}

	return false;
}
//------------------------------------------------------------------------------

//==============================================================================
// Dragged control dropped over undefined control (gonna check if it's droppable)
function Lab::checkObjForToolbarStack(%this,%object,%defaultStack)
{
	if (!isObject(%object))
		return false;

	if (%object.getClassname() $= "GuiStackControl")
	{
		if (%defaultStack && %object.superClass $= "ToolbarPluginStack")
			return true;
		else if (!%defaultStack && %object.superClass $= "ToolbarContainer")
			return true;
		else if (!%defaultStack && %object.superClass $= "PluginToolbarEnd")
			return true;
	}

	return false;
}
//------------------------------------------------------------------------------
//==============================================================================
// Dragged control dropped over undefined control (gonna check if it's droppable)
function Lab::addToolbarItemToGroup(%this,%item,%group,%addBefore)
{
	%divider = %group-->GroupDivider;

	if (isObject(%divider))
	{
		%divider.extent.x = "6";

		foreach(%obj in %divider)
		{
			if (%obj.getClassName() $= "GuiMouseEventCtrl")
				%obj.extent.x = "6";
		}
	}

	//If dragged from thrash, switch to original icon and delete the thrashed icon
	if (isObject(%item.srcIcon))
	{
		%original = %item.srcIcon;
		delObj(%item);
	}
	else
	{
		%original = %item;
	}

	//%original = %this.getTrashedOriginalIcon(%item);

	//srcIcon should always be NULL for official toolbar icons (Contained under LabToolbarStack)
	%original.srcIcon = "";

	%group.add(%original);
	%group.updateStack();
	%group.reorderChild(%original,%addBefore);
	%item.originalParent = %group;
	%item.parentInternal = %group.internalName;
	%item.originalIndex = %group.getObjectIndex(%original);

	Lab.setToolbarDirty(%group.toolbarName);
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::showDisabledToolbarDropArea(%this,%dragControl)
{
	%isIcon = false;
	//Show the Trash Drop Zone
	show(EToolbarTrashDropZone);
	Lab.toolbarDisableIconDropZone(1);

	//if (%dragControl.isMemberOfClass("GuiIconButtonCtrl"))
	//%isIcon = true;
	if (!%dragControl.isMemberOfClass("GuiStackControl"))
		%isIcon = true;

	EToolbarDisabledDrop.visible = 1;
	%startStack = LabToolbarStack-->FirstToolbarGroup;
	LabToolbarStack.bringToFront(%startStack);
	%lastStack = LabToolbarStack-->LastToolbarGroup;
	LabToolbarStack.pushToBack(%lastStack);

	//Only show StackEnd for groups
	if (%isIcon)
	{
		foreach(%gui in LabToolbarStack)
		{
			foreach(%stack in %gui)
			{
				%stackEnd = %stack-->SubStackEnd;

				if (!isObject(%stackEnd))
					continue;

				if (%stackEnd.toolbarName $= %dragControl.toolbarName)
					show(%stackEnd);
				else
					hide(%stackEnd);

				%stack.pushToBack(%stackEnd);
				%stackEnd.profile = "ToolsBoxDarkC";
				%stackEnd.setExtent(18,%gui.parentGroup.extent.y);
			}
		}
	}
	else
	{
		foreach(%gui in LabToolbarStack)
		{
			%stackEnd = %gui-->StackEnd;

			if (!isObject(%stackEnd))
				continue;

			if (%stackEnd.toolbarName $= %dragControl.toolbarName)
				show(%stackEnd);
			else
				hide(%stackEnd);

			%gui.pushToBack(%stackEnd);

			%stackEnd.profile = "ToolsBoxDarkC";
			%stackEnd.setExtent(27,%gui.parentGroup.extent.y);
		}
	}

	foreach(%gui in EToolbarDisabledDrop)
	{
		if (%gui.toolbarName !$= %dragControl.toolbarName)
		{
			%src = %gui.internalName;

			if (%src.visible $= "0" || !%src.visible)
				continue;

			%gui.visible = 1;
			%gui.setExtent(%src.extent.x,EditorGuiToolbar.extent.y);
			%gui.setPosition(%src.position.x,0);
		}
		else
		{
			%gui.visible = 0;
		}
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::hideDisabledToolbarDropArea(%this)
{
	//Hide the Trash Drop Zone
	hide(EToolbarTrashDropZone);
	hide(LabToolbarStack-->ToolbarStart);
	hide(LabToolbarStack-->ToolbarEnd);
	Lab.toolbarDisableIconDropZone(0);

	foreach(%gui in LabToolbarStack)
	{
		%stackEnd = %gui-->StackEnd;

		if (isObject(%stackEnd))
			hide(%stackEnd);

		//foreach$(%stack in %gui.stackLists) {
		foreach(%subCtrl in %gui)
		{
			%subStackEnd = %subCtrl-->SubStackEnd;

			if (isObject(%subStackEnd))
				hide(%subStackEnd);
		}
	}

	foreach(%gui in EToolbarDisabledDrop)
		%gui.visible = 0;

	EToolbarDisabledDrop.visible = 0;
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::toolbarDisableIconDropZone(%this,%visible)
{
	delObj(Lab.toolbarDropZone);

	if (!%visible)
		return;

	%dropZone = new GuiContainer()
	{
		extent = "288 44";
		minExtent = "8 2";
		horizSizing = "center";
		vertSizing = "bottom";
		profile = "ToolsFillDark_T75";
		superClass = "EToolbarTrashClass";

		new GuiTextCtrl()
		{
			text = "Drop here to disable";
			docking = "client";
			profile = "ToolsTextAlt_H2_C";
		};
	};
	EditorGui-->DialogContainer.add(%dropZone);
	EditorGui-->DialogContainer.bringToFront(%dropZone);
	%dropZone.AlignCtrlToParent("top",8);
	%dropZone.AlignCtrlToParent("centerX");
	Lab.toolbarDropZone = %dropZone;

}
//------------------------------------------------------------------------------

