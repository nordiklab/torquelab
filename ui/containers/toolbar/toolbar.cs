//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function Toolbar::onAdd(%this)
{
	Lab.addGui(%this.getName(),"Toolbar");
}
//------------------------------------------------------------------------------

//==============================================================================
// PerformClick on a toolbar icon based on internalName and optional subBar
//------------------------------------------------------------------------------
// wToolbar.press("centerObject");
// Lab.pushToolbar("centerObject");

//==============================================================================
function wToolbar::press(%this, %internal,%subBar)
{
	%toolbar = LabToolbarStack;

	if (%subBar !$= "")
	{
		if (isObject(%subBar))
			%toolbar = %subBar;
		else
			%toolbar = LabToolbarStack.findObjectByInternalName(%subBar,1);
	}

	if (!isObject(%toolbar))
		return;

	%button = %toolbar.findObjectByInternalName(%internal,1);

	if (!isObject(%button))
	{
		warnLog("Toolbar button not found:",%internal,"Search GUI:",%toolbar);
		return;
	}

	%button.performClick();
}
//------------------------------------------------------------------------------
function Lab::pushToolbar(%this, %internal,%subBar)
{
	wToolbar.press( %internal,%subBar);
}
//------------------------------------------------------------------------------
