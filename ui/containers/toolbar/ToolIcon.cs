//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Prebuilt functions for ToolIcon icons
//==============================================================================

//==============================================================================
// Eval a command on all icons in froup
// %internal - InternalName shared by the icons
// %cmd - Full method with arguments ending with ; (ex:"setText(\"Shared Text\");")
// Example use: Lab.evalToolIcon("ObjectTransform","setBitmap(\"tlab/art/icons/transform.png\");");
function Lab::evalToolIcon(%this,%internal,%evalCmd)
{
	foreach$(%iconCtrl in $ToolIconGroup[%internal])
		eval("%iconCtrl."@%evalCmd);
}
//------------------------------------------------------------------------------

//==============================================================================
// Simulate a click on the first icon listed, usefull if no official name used and
// the parent can be changed. With this function, you can be sure it will find the icon
function Lab::pushToolIcon(%this, %internal)
{
	%icon = getWord($ToolIconGroup[%internal],0);

	if (isObject(%icon))
		%icon.performClick();
}

//==============================================================================
// Common Function to deal with ToolIcon and ToolbarIcon callbacks
//==============================================================================

//==============================================================================
// Icon as been added, add it's ID to the list
function Lab::onToolIconAdded(%this,%icon)
{
	if (%icon.internalName !$= "")
	{
		ELink.link(%icon,%icon.internalName);
		$ToolIconGroup[%icon.internalName] = strAddWord($ToolIconGroup[%icon.internalName],%icon.getId(),1);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Icon as been deleted, remove it's ID from the list
function Lab::onToolIconRemoved(%this,%icon)
{
	if (%icon.internalName !$= "")
	{
		$ToolIconGroup[%icon.internalName] = strRemoveWord($ToolIconGroup[%icon.internalName],%icon.getId());
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// ToolIcon and ToolbarIcon onAdd/onRemove Callbacks which handle the group list
//==============================================================================
//==============================================================================
// Set appropriate tooltip and store list of same purpose icons
function ToolIcon::onAdd(%this)
{
	//Parent::onAdd(%this);
	Lab.onToolIconAdded(%this);
}
//------------------------------------------------------------------------------

//==============================================================================
// Set appropriate tooltip and store list of same purpose icons
function ToolbarIcon::onAdd(%this)
{
	//if(Parent.isMethod(onAdd))
	// Parent::onAdd(%this);
	Lab.onToolIconAdded(%this);
}
//------------------------------------------------------------------------------

//==============================================================================
// Set appropriate tooltip and store list of same purpose icons
function ToolIcon::onRemove(%this)
{
	Parent::onRemove(%this);

	if (isObject(Lab))
		Lab.onToolIconRemoved(%this);
}
//------------------------------------------------------------------------------

//==============================================================================
// Set appropriate tooltip and store list of same purpose icons
function ToolbarIcon::onRemove(%this)
{
	Parent::onRemove(%this);

	if (isObject(Lab))
		Lab.onToolIconRemoved(%this);
}
//------------------------------------------------------------------------------

//==============================================================================
// Set appropriate tooltip and store list of same purpose icons
function ToolbarIcon::onClick(%this)
{
	//Was initially set for toggle lights but it call setToggleState now instead
	//if (strFindList(%this.iconBitmap,"_on _off"))
	//%this.setToggleState(%this.getValue());
}
//------------------------------------------------------------------------------

//==============================================================================
// Set appropriate tooltip and store list of same purpose icons
function ToolbarIcon::setToggleState(%this,%value)
{
	if (%value $= "")
		%value = %this.getValue();

	if (%value)
		%stateFileCheck = strreplace(%this.iconBitmap,"_off","_on");
	else
		%stateFileCheck = strreplace(%this.iconBitmap,"_on","_off");

	if (isImageFile(%stateFileCheck))
		%this.setBitmap(%stateFileCheck);

}
//------------------------------------------------------------------------------

$ToolIconToolTip["ReturnToGame"] = "Close editor and return to game";
$ToolIconToolTip["CameraBookmark"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
$ToolIconToolTip["InternalName"] = "";
