//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function Lab::openDisabledPluginsBin(%this,%removeOnly)
{
	%bin = EditorGui-->pluginBarTrash; // LabPluginThrash

	if (%removeOnly $= "")
		%removeOnly = 0;

	if (%bin.isVisible())
		return;

	%bin.removeOnly = %removeOnly;

	%bin.position.y = LabPluginBar.extent.y + 4;

	if (LabPluginThrash.hiBox $= "")
		LabPluginThrash.hiBox = LabPluginThrash.extent.y;

	if (%removeOnly)
	{
		%bin.extent.y = LabPluginThrash.hiBox - 24;
	}
	else
	{
		%bin.extent.y = %bin.hiBox;
	}

	show(%bin);
}
//------------------------------------------------------------------------------
//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function Lab::closeDisabledPluginsBin(%this,%autoClose)
{
	%bin = EditorGui-->pluginBarTrash;

	if (%autoClose && %bin.extent.y $= %bin.hiBox)
	{
		warnLog("The Disabled plugin box is in full mode and must be close with button");
		return;
	}

	hide(%bin);
}
//------------------------------------------------------------------------------
//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function Lab::clearDisabledPluginsBin(%this)
{
	%bin = EditorGui-->DisabledPluginsBox;
	%bin.deleteAllObjects();
}
//------------------------------------------------------------------------------
//==============================================================================
// Clear the editors menu (for dev purpose only as now)
function Lab::updatePluginIconContainer(%this,%type)
{
	if (%type $= "")
		%doBoth = true;

	if (%type $= "Enabled" || %doBoth)
	{
		LabPluginArray.refresh();
		Lab.refreshPluginToolbar();
	}

	if (%type $= "Disabled" || %doBoth)
	{
		EditorGui-->DisabledPluginsBox.refresh();
	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Add the various TorqueLab GUIs to the container and set they belong
function Lab::addPluginIconToDisabledBin(%this,%originalIcon,%trashedIcon)
{
	hide(%originalIcon);
	delObj(%trashedIcon);
	Lab_DisabledPluginsBox.add(%originalIcon);
	%originalIcon.class =  "PluginIconDisabled";
	info(%originalIcon.internalName," dropped in DisabledPluginsBox and should be set disabled",%originalIcon.pluginObj);
	Lab.disablePlugin(%originalIcon.pluginObj);
	Lab.refreshPluginToolbar();
}
//------------------------------------------------------------------------------

//==============================================================================
// Add the various TorqueLab GUIs to the container and set they belong
function Lab::onToolbarPluginIconDropped(%this,%originalIcon)
{
	Lab.enablePlugin(%originalIcon.pluginObj);
	Lab.refreshPluginToolbar();
}
//------------------------------------------------------------------------------

