//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function Lab::updatePluginsBar(%this,%reset)
{
	hide(LabPluginIconSrc);

	if (%reset)
		%this.resetPluginsBar();

	foreach(%pluginObj in LabPluginGroup)
	{
		Lab.addPluginToBar(%pluginObj);
	}
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::resetPluginsBar(%this)
{
	LabPluginArray.deleteAllObjects();
	Lab.clearDisabledPluginsBin();
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
function Lab::checkAllPluginIcon(%this)
{
	foreach(%pluginObj in LabPluginGroup)
		%this.checkPluginIcon(%pluginObj);
}
//==============================================================================
// Add a plugin icon to the plugin bar
//==============================================================================
//==============================================================================
function Lab::checkPluginIcon(%this, %pluginObj)
{
	%enabled =  %pluginObj.isEnabled;
	%disabledBox = EditorGui-->DisabledPluginsBox;

	if (%enabled)
	{
		%goodIcon = LabPluginArray.findObjectByInternalName(%pluginObj.plugin);
		%badIcon = %disabledBox.findObjectByInternalName(%pluginObj.plugin);
	}
	else
	{
		%badIcon = LabPluginArray.findObjectByInternalName(%pluginObj.plugin);
		%goodIcon = %disabledBox.findObjectByInternalName(%pluginObj.plugin);
	}

	delObj(%badIcon);

	if (!isObject(%goodIcon))
	{
		%icon = %this.createPluginIcon(%pluginObj);
		%icon.visible = true;

		if (%enabled)
			LabPluginArray.add(%icon);
		else
			%disabledBox.add(%icon);

	}

}
//==============================================================================
function Lab::addPluginToBar(%this, %pluginObj)
{
	if (%pluginObj.isHidden)
		return;

	%this.checkPluginIcon(%pluginObj);	
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::createPluginIcon(%this, %pluginObj)
{

	%folder = "tlab/art/icons/set01/plugin/";

	%icon = %folder@%pluginObj.plugin;

	if (!isFile(%icon@".png"))
	{
		%invalidImg = true;
		%icon = %folder@"Missing.png";
	}

	%button = cloneObject(LabPluginIconSrc);
	%button.internalName = %pluginObj.plugin;
	%button.superClass = "";
	%button.command = "Lab.setEditor(" @ %pluginObj.getName()@ ");";
	%button.iconBitmap = %icon;
	%button.tooltip = %pluginObj.tooltip;
	%button.visible = true;
	%button.useMouseEvents = true;
	%button.pluginObj = %pluginObj;
	%button.superClass = "PluginIcon";
	%button.canSave = "0";

	if (%invalidImg)
		%button.invalid = true;

	return %button;
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::togglePluginBarSize(%this)
{
	%collapsed = LabPluginArray.isCollapsed;

	if (%collapsed)
		Lab.expandPluginBar();
	else
		Lab.collapsePluginBar();
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::expandPluginBar(%this)
{
	LabPluginArray.isCollapsed = false;

	foreach(%icon in LabPluginArray)
	{
		%enabled = %icon.pluginObj.isEnabled;
		%icon.visible = %enabled;
	}

	%this.refreshPluginToolbar();
}
//------------------------------------------------------------------------------
//==============================================================================
function Lab::collapsePluginBar(%this)
{
	LabPluginArray.isCollapsed = true;

	foreach(%icon in LabPluginArray)
	{
		if (%icon.pluginObj.getId() $= Lab.currentEditor.getId())
			%icon.visible = true;
		else
			%icon.visible = false;
	}

	Lab.refreshPluginToolbar();
}
//------------------------------------------------------------------------------
$PluginToolbarBitmap[0] = "tlab/Art/buttons/set01/default/collapse-toolbar";
$PluginToolbarBitmap[1] = "tlab/Art/buttons/set01/default/expand-toolbar";
//==============================================================================
function Lab::refreshPluginToolbar(%this)
{
	%collapsed = LabPluginArray.isCollapsed;
	LabPluginArray.refresh();
	LabPluginBar.position = "0 -2";
	LabPluginBar.extent.y = "42";
	LabPluginArray.position.y = 2;
	LabPluginBar.extent.x = LabPluginArray.extent.x + 10;
	LabPluginBar-->resizeArrow.AlignCtrlToParent("right");
	LabPluginBar-->resizeArrow.setBitmap($PluginToolbarBitmap[%collapsed]);
	hide(LabPluginBarDecoy);
}
//------------------------------------------------------------------------------
//==============================================================================
// Plugin Toolbar GuiControl Functions
//==============================================================================

//==============================================================================
function LabPluginBar::reset(%this)
{
	Lab.expandPluginBar();
	return;
	%count = LabPluginArray.getCount();

	for(%i = 0 ; %i < %count; %i++)
		LabPluginArray.getObject(%i).setVisible(true);

	%this.setExtent((29 + 4) * %count + 12, 33);
	%this.isClosed = 0;
	LabPluginBar.isDynamic = 0;
	LabPluginBarDecoy.setVisible(false);
	LabPluginBarDecoy.setExtent((29 + 4) * %count + 4, 31);
	%this-->resizeArrow.setBitmap("tlab/Art/buttons/set01/default/collapse-toolbar");
}
//------------------------------------------------------------------------------
//==============================================================================
function LabPluginBar::expand(%this, %close)
{
	Lab.expandPluginBar();
	return;
	%this.isClosed = !%close;
	%this.toggleSize();
}
//------------------------------------------------------------------------------
//==============================================================================
function LabPluginBar::resize(%this)
{
	Lab.refreshPluginToolbar();
	return;
	%this.isClosed = ! %this.isClosed ;
	%this.toggleSize();
}
//------------------------------------------------------------------------------
//==============================================================================
function LabPluginBar::toggleSize(%this, %useDynamics)
{
	Lab.togglePluginBarSize();
}
//------------------------------------------------------------------------------
//==============================================================================
function LabPluginBarDecoy::onMouseEnter(%this)
{
	Lab.togglePluginBarSize(true);
}
//------------------------------------------------------------------------------
//==============================================================================
function LabPluginBarDecoy::onMouseLeave(%this)
{
	Lab.togglePluginBarSize(true);
}
//------------------------------------------------------------------------------
