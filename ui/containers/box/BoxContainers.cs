//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//LabPalettes.addPluginPalette("ShapeLabPalette");
//==============================================================================
function BoxContainer::onObjectAdded(%this,%gui)
{
	%gui.superClass = "EditorBox";
	%gui.container = %this.internalName;
}
//------------------------------------------------------------------------------
//==============================================================================
function BoxContainer::onObjectRemoved(%this,%gui)
{
}
//------------------------------------------------------------------------------

//==============================================================================
function EditorBox::toggleState(%this,%gui)
{
	%this.setState(!%this.visible);
}
//------------------------------------------------------------------------------

//==============================================================================
function EditorBox::setState(%this,%visible)
{
	%this.setVisible(%visible);
}
//------------------------------------------------------------------------------
