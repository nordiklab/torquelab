//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$TLab_UIWidgets_Path = "tlab/ui/gui/widgets/";
//Set the linked GuiControls superclass onAdd methods
exec("./uiLinks.cs");

//Exec the Widgets here since they are used by both editor
exec( "./gui/widgets/initWidgets.cs");
exec("./uiManager.cs");
exec("./launchUI.cs");

//Load the WorldEditor UI layout system first
execPattern("tlab/ui/classes/*.cs");
execPattern("tlab/ui/layout/*.cs");
execPattern("tlab/ui/common/*.cs");
execPattern("tlab/ui/modules/*.cs");
execPattern("tlab/ui/containers/*.cs");
execPattern("tlab/ui/dialogs/*.cs");
$TLabLayoutFolder = "tlab/ui/layout/";
