//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Called from EditorGui last part of onWake each time
function Lab::launchGuiSetup(%this)
{

	if (EditorGui-->SideBarContainer.isOpen)
		SideBarMainBook.onTabSelected("test",$SideBarMainBook_CurrentPage); //Hack to force reload

	LabToolbarStack.bringToFront(LabToolbarStack-->FirstToolbarGroup);
	LabToolbarStack.pushToBack(LabToolbarStack-->LastToolbarGroup);

	Lab.setSideBarToggle();

	//GlobalSceneTree.rebuild();
	if (!isObject(wToolbar))
		new ScriptObject(wToolbar);

	Lab.setSideBarWidth();
	Lab.setToolsWidth();
	Lab.setDefaultLayoutSize(true);
	//EManageBookmarks.initBookmarks();

}

//==============================================================================
// This is only called the first time and happen right before launchGuiSetup
function Lab::InitialGuiSetup(%this,%pluginName,%displayName,%alwaysEnable)
{

	Lab.checkContainersSizes();
	//===========================================================================
	// ToolBar Validation
	//---------------------------------------------------------------------------
	//---------------------------------------------------------------------------
	EWorldEditor.setFirst();//Make first object of his parent
	//===========================================================================
	// ToolBar Validation
	//---------------------------------------------------------------------------
	Lab.toolbarHeight = EditorGuiToolbar.y;

	//===========================================================================
	// PluginBar Validation
	//---------------------------------------------------------------------------
	Lab.updatePluginsBar();
	Lab.sortPluginsBar(true);
	Lab.pluginBarHeight = LabPluginBar.y;

	LabPluginBar.position = "0 0";// SPC EditorGuiToolbar.extent.y;
	LabPluginBar.extent  = "43 33" ;
	LabPluginBar-->resizeArrow.position = getWord(LabPluginBar.Extent, 0) - 7 SPC "0";
	Lab.expandPluginBar();

	//===========================================================================
	// Palettes Validation
	//---------------------------------------------------------------------------
	Lab.paletteBarWidth = LabPalettes.x;
	LabPalettes.position.y =  LabPluginBar.extent.y;// + EditorGuiToolbar.extent.y;
	LabPalettes.position.x = "0";

	//===========================================================================
	// EditorContainer Validation
	//---------------------------------------------------------------------------
	EditorGui-->EditorContainer.setFirst();//Make first object of his parent
	//must be the first of parent child

	foreach(%gui in EditorGui-->EditorContainer)
		%gui.fitIntoParents();

	Lab.initSceneEditor();

	LabEditor.isInitialized = true;

	//===========================================================================
	// Check for plugins or ObjLabs for a postInit Function
	//---------------------------------------------------------------------------
	ELink.doCall("ObjLab","onPostEditorInit");
	EditorPluginSet.doCall("onPostEditorInit");
}
//------------------------------------------------------------------------------

//==============================================================================
// Call during the initial delayed UI init call (only the first time)
function Lab::checkLaunchedLayout(%this)
{

	//Lab.checkLayoutCore(); //Called from Lab::initUILayout
	Lab.schedule(100,"initSideBar");
	skipPostFx(false);
	ETools.initTools();
	Lab.initAllToolbarGroups();
	Lab.initToolbarTrash();
	ObjectBuilderGui.init();
	EditorGui-->DisabledPluginsBox.callOnChildrenNoRecurse("setVisible",true);
	EManageBookmarks.initBookmarks();
}
//------------------------------------------------------------------------------
