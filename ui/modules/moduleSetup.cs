//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Make sure all GUIs are fine once the editor is launched (from syncEditor)
function Lab::resizeLayoutModules(%this)
{

	//EditorFrameMain.minExtent = %this.getExtent().x - 220 SPC "12";
	//%this.checkCol();
	if (isObject(FileBrowser))
		FileBrowser.onCtrlResized();

	if (isObject(ObjectCreator))
		ObjectCreator.onCtrlResized();

	if (isObject(SideBarVIS))
		SideBarVIS.onCtrlResized();

	if (isObject(ECamViewGui))
		ECamViewGui.checkArea();

	if (Lab.currentEditor.isMethod("onLayoutResized"))
		Lab.currentEditor.onLayoutResized();

	%colPosZ = %this.columns.z;

	if (%colPosZ !$= "")
	{
		%colWidthZ = 	%this.extent.x - %colPosZ;
		%this.rightColumnSize = %colWidthZ;
	}
}
