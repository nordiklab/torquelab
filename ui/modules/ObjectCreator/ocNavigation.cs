//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Navigation Go To Calls
//==============================================================================

//==============================================================================
function ObjectCreatorTypeMenu::onSelect(%this,%id,%text)
{
	$ObjectCreator_TypeId = %id;
	ObjectCreator.objType = %text;
	%lastTabAddress = ObjectCreator.lastTypeAdress[%text];
	ObjectCreator.navigate(%lastTabAddress);
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjectCreatorTypeButton::onClick(%this)
{
	$ObjectCreator_TypeId = %id;
	ObjectCreator.objType = %this.internalName;
	%lastTabAddress = ObjectCreator.lastTypeAdress[%this.internalName];
	ObjectCreator.navigate(%lastTabAddress);
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjectCreatorMenu::onSelect(%this, %id, %text)
{
	if (strFind(%text,"/") && $ObjCreatorMenu_Minimal)
		return;

	if (ObjectCreator.lastAddress $= "root")
	{
		eval("ObjectCreatorTypeButtons->"@%text@".performClick();");
		return;
	}

	%split = strreplace(%text, "/", " ");
	ObjectCreator.schedule(1, "navigate", %split);

}
//------------------------------------------------------------------------------

//==============================================================================
// Object Creator Navigation Address System
//==============================================================================

//==============================================================================
function ObjectCreator::navigateDown(%this, %folder)
{
	if (%this.address $= "")
		%address = %folder;
	else
		%address = %this.address SPC %folder;

	// Make sure to call navigate after this function
	%this.schedule(1, "navigate", %address);
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjectCreator::navigateUp(%this)
{
	%count = getWordCount(ObjectCreator.address);

	if (%count == 0)
	{
		//Go to root with Scripted and Level buttons
		%this.schedule(1, "navigate", "root");
		return;
	}

	if (%count == 1)
		%address = "";
	else
		%address = getWords(%this.address, 0, %count - 2);

	%this.schedule(1, "navigate", %address);
}
//------------------------------------------------------------------------------

//==============================================================================
// Navigate through Creator Book Data
function ObjectCreator::navigate(%this, %address)
{
	ObjectCreatorArray.frozen = true;
	ObjectCreatorArray.clear();
	ObjectCreatorMenu.clear();
	show(ObjectCreatorArray);
	hide(ObjectCreatorIconSrc);

	if (%address $= "root")
	{
		Lab.addCommandIcon(ObjectCreatorArray,"ObjectCreatorTypeButtons->scripted.performClick();","Scripted");
		Lab.addCommandIcon(ObjectCreatorArray,"ObjectCreatorTypeButtons->level.performClick();","Level");
		ObjectCreator.objType = "None";

		ObjectCreatorMenu.setText("Choose object type");
		ObjectCreatorMenu.add("Scripted",0);
		ObjectCreatorMenu.add("Level",1);

	}
	else
	{

		%type = ObjectCreator.objType;

		if (!%this.isMethod("navigate"@%type)||%type $= "")
		{
			warnLog("Couldn't find a scene creator navigating function for type:",%type);
			return;
		}
    
		eval("ObjectCreator.navigate"@%type@"("@%address@");");
		ObjectCreator.lastTypeAddress[%type] = %address;
		ObjectCreatorArray.sort("arrayIconCompare");

		for(%i = 0; %i < ObjectCreatorArray.getCount(); %i++)
		{
			ObjectCreatorArray.getObject(%i).autoSize = false;
		}

		Lab.addFolderUpIcon(%this);
	}

	%this.setViewId($ObjectCreator_ViewId);
	ObjectCreatorArray.refresh();

	// Recalculate the array for the parent guiScrollCtrl
	ObjectCreatorArray.getParent().computeSizes();
	%this.address = %address;
	%this.addressFull = ObjectCreator.objType SPC %address;
	ObjectCreator.lastAddress = %address;

	if (%address !$= "root")
	{
		ObjectCreatorMenu.sort();
		%menuText = (%address $= "") ? %type : %address;
		%str = strreplace(%menuText, " ", "/");
		%r = ObjectCreatorMenu.findText(%str);

		if (%r != -1)
			ObjectCreatorMenu.setSelected(%r, false);
		else
			ObjectCreatorMenu.setText(%str);

		ObjectCreatorMenu.tooltip = %str;
	}
}

//------------------------------------------------------------------------------
//==============================================================================
// Navigate through Creator Book Data
function ObjectCreator::navigateLevel(%this, %address)
{
	// Added when loading new level it's deleted (TO BE EXAMINED)
	if (!isObject(Scene.array))
		Scene.registerObjects();

	%array = Scene.array;
	%array.sortk();
	%count = %array.count();

	if (%count > 0)
	{
		%lastGroup = "";

		for(%i = 0; %i < %count; %i++)
		{
			%group = %array.getKey(%i);

			if (%group !$= %lastGroup)
			{
				ObjectCreatorMenu.add( %group );

				if (%address $= "")
					Lab.addFolderIcon(%this,%group);
			}

			if (%address $= %group)
			{
				%args = %array.getValue(%i);
				%class = %args.val[0];
				%name = %args.val[1];
				%func = %args.val[2];
				%icon = %this.addMissionObjectIcon(%class, %name, %func);

			}

			%lastGroup = %group;
		}
	}
}
//==============================================================================
// Navigate through Creator Book Data
function ObjectCreator::navigateScripted(%this, %address)
{
	%category = getWord(%address, 1);
	%dataGroup = "DataBlockGroup";

	for(%i = 0; %i < %dataGroup.getCount(); %i++)
	{
		%obj = %dataGroup.getObject(%i);	

      //If no shape assigned, skip it to prevent issues
		if (%obj.isMemberOfClass("ItemData") && %obj.shapeFile $= "")			
			continue;		

		if (%obj.category $= "" && %obj.category == 0)
			continue;

		%ctrl = ObjectCreatorArray.findIconCtrl(%obj.category);

		if (%ctrl == -1)
		{
			if (%address $= "")
				Lab.addFolderIcon(%this, %obj.category);

			ObjectCreatorMenu.add(%obj.category);
		}

	
		if (%address $= %obj.category)
		{
			%ctrl = ObjectCreatorArray.findIconCtrl(%obj.getName());

			if (%ctrl == -1)
				%ctrl = %this.addShapeIcon(%obj);

		}
	}
}
//==============================================================================

//==============================================================================
// Object Creator Icon Types Creation
//==============================================================================

//==============================================================================
function ObjectCreator::addFileIcon(%this, %fullPath)
{
	%ctrl = Lab.createArrayIcon();
	%ext = fileExt(%fullPath);

	if (strFindWords(strlwr(%ext),"dae dts"))
	{
		%createCmd = "Scene.createMesh( \"" @ %fullPath @ "\" );";
		%type = "Mesh";
		%ctrl.createCmd = %createCmd;
	}
	else if (strFindWords(strlwr(%ext),"png tga jpg bmp"))
	{
		%type = "Image";
	}
	else
	{
		%type = "Unknown";
	}

	%file = fileBase(%fullPath);
	%fileLong = %file @ %ext;
	%tip = %fileLong NL
	       "Size: " @ fileSize(%fullPath) / 1000.0 SPC "KB" NL
	       "Date Created: " @ fileCreatedTime(%fullPath) NL
	       "Last Modified: " @ fileModifiedTime(%fullPath);
	%ctrl.altCommand = "ObjectCreator.icon"@%type@"Alt($ThisControl);";
	%ctrl.iconBitmap = ((%ext $= ".dts") ? EditorIconRegistry::findIconByClassName("TSStatic") : "tlab/art/icons/set01/default/iconCollada");
	%ctrl.text = %file;
	%ctrl.type = %type;
	//%ctrl.superClass = "ObjectCreatorIcon";
	//%ctrl.superClass = "ObjectCreatorIcon"@%type;
	%ctrl.tooltip = %tip;
	%ctrl.buttonType = "radioButton";
	%ctrl.groupNum = "-1";
	%ctrl.fullPath = %fullPath;
	ObjectCreatorArray.addGuiControl(%ctrl);
	return %ctrl;
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjectCreator::addMissionObjectIcon(%this, %class, %name, %buildfunc)
{
	%ctrl = Lab.createArrayIcon();
	// If we don't find a specific function for building an
	// object then fall back to the stock one
	%method = "build" @ %buildfunc;

	if (!ObjectBuilderGui.isMethod(%method))
		%method = "build" @ %class;

	if (!ObjectBuilderGui.isMethod(%method))
		%method = "build" @ %class;

	if (!ObjectBuilderGui.isMethod(%method))
	{
		%func = "build" @ %buildfunc;

		if (isFunction("build" @ %buildfunc))
			%cmd = "return build" @ %buildfunc @ "();";
		else
			%cmd = "return new " @ %class @ "();";
	}
	else
	{
		%cmd = "ObjectBuilderGui." @ %method @ "();";
	}

	%ctrl.altCommand = "ObjectBuilderGui.newObjectCallback = \"Scene.postCreateObjectCheck\"; Scene.createObject( \"" @ %cmd @ "\" );";
	%ctrl.iconBitmap = EditorIconRegistry::findIconByClassName(%class);
	%ctrl.text = %name;
	%ctrl.class = "CreatorMissionObjectIconBtn";
	%ctrl.tooltip = %class;
	%ctrl.buttonType = "radioButton";
	%ctrl.groupNum = "-1";
	%ctrl.type = "MissionObject";
	ObjectCreatorArray.addGuiControl(%ctrl);
	return %ctrl;
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjectCreator::addShapeIcon(%this, %datablock)
{
	%ctrl = Lab.createArrayIcon();
	%name = %datablock.getName();
	%class = %datablock.getClassName();
	%cmd = %class @ "::create(" @ %name @ ");";
	%shapePath = (%datablock.shapeFile !$= "") ? %datablock.shapeFile : %datablock.shapeName;
	%createCmd = "Scene.createObject( \\\"" @ %cmd @ "\\\" );";
	%ctrl.altCommand = "ColladaImportDlg.showDialog( \"" @ %shapePath @ "\", \"" @ %createCmd @ "\" );";
	%ctrl.iconBitmap = EditorIconRegistry::findIconByClassName(%class);
	%ctrl.text = %name;
	%ctrl.class = "CreatorShapeIconBtn";
	%ctrl.tooltip = %name;
	%ctrl.buttonType = "radioButton";
	%ctrl.groupNum = "-1";
	ObjectCreatorArray.addGuiControl(%ctrl);
	return %ctrl;
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjectCreator::addStaticIcon(%this, %fullPath)
{
	%ctrl = %this.createArrayIcon();
	%ext = fileExt(%fullPath);
	%file = fileBase(%fullPath);
	%fileLong = %file @ %ext;
	%tip = %fileLong NL
	       "Size: " @ fileSize(%fullPath) / 1000.0 SPC "KB" NL
	       "Date Created: " @ fileCreatedTime(%fullPath) NL
	       "Last Modified: " @ fileModifiedTime(%fullPath);
	%createCmd = "Scene.createStatic( \\\"" @ %fullPath @ "\\\" );";
	%ctrl.altCommand = "ColladaImportDlg.showDialog( \"" @ %fullPath @ "\", \"" @ %createCmd @ "\" );";
	%ctrl.iconBitmap = ((%ext $= ".dts") ? EditorIconRegistry::findIconByClassName("TSStatic") : "tlab/art/icons/set01/default/iconCollada");
	%ctrl.text = %file;
	%ctrl.class = "CreatorStaticIconBtn";
	%ctrl.tooltip = %tip;
	%ctrl.buttonType = "radioButton";
	%ctrl.groupNum = "-1";
	ObjectCreatorArray.addGuiControl(%ctrl);
	return %ctrl;
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjectCreator::addPrefabIcon(%this, %fullPath)
{
	%ctrl = %this.createArrayIcon();
	%ext = fileExt(%fullPath);
	%file = fileBase(%fullPath);
	%fileLong = %file @ %ext;
	%tip = %fileLong NL
	       "Size: " @ fileSize(%fullPath) / 1000.0 SPC "KB" NL
	       "Date Created: " @ fileCreatedTime(%fullPath) NL
	       "Last Modified: " @ fileModifiedTime(%fullPath);
	%ctrl.altCommand = "Scene.createPrefab( \"" @ %fullPath @ "\" );";
	%ctrl.iconBitmap = EditorIconRegistry::findIconByClassName("Prefab");
	%ctrl.text = %file;
	%ctrl.class = "CreatorPrefabIconBtn";
	%ctrl.tooltip = %tip;
	%ctrl.buttonType = "radioButton";
	%ctrl.groupNum = "-1";
	ObjectCreatorArray.addGuiControl(%ctrl);
	return %ctrl;
}
//------------------------------------------------------------------------------
