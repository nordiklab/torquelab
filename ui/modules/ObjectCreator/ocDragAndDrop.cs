//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function ArrayIconCtrl::onMouseDragged(%this,%a1,%a2,%a3)
{
	if (!isObject(%this))
	{
		return;
	}

	startDragAndDropCtrl(%this,"ArrayIconCtrl","ObjectCreator.onIconDropped");
	hide(%this);
}
//------------------------------------------------------------------------------

//==============================================================================
// Dropping a file icon
//==============================================================================

//==============================================================================
function ObjectCreator::onIconDropped(%this,%droppedOn,%icon,%dropPoint)
{
	%originalIcon = %icon.dragSourceControl;
	show(%originalIcon);
	delObj(%icon);

	if (ObjectCreator.isMethod("checkIconDrop"@%originalIcon.type))
		eval("ObjectCreator.checkIconDrop"@%originalIcon.type@"(%originalIcon,%droppedOn,%dropPoint);");
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjectCreator::checkIconDropMissionObject(%this,%icon,%droppedOn,%dropPoint)
{

	if (%droppedOn.getName() $= "EWorldEditor")
	{
		eval(%icon.altCommand);
	}
	else if (%droppedOn.getClassName() $= "GuiShapeLabPreview")
	{
		ShapeLab.selectFilePath(%icon.fullPath);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function ObjectCreator::checkIconDropImage(%this,%icon,%droppedOn,%dropPoint)
{

	if (%droppedOn.class $= "MaterialMapCtrl")
	{
		%type = %droppedOn.internalName;

		if (MaterialEditorTools.isMethod("update"@%type@"Map"))
		{
			eval("MaterialEditorTools.update"@%type@"Map(true,%icon.fullPath,false);");
		}
	}
}
//------------------------------------------------------------------------------
