//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$ObjectCreator_View[0] = "1 Column" TAB "Col 1";
$ObjectCreator_View[1] = "2 Column" TAB "Col 2";
$ObjectCreator_View[2] = "3 Column" TAB "Col 3";
$ObjectCreator_ViewId = 0;

$ObjectCreator_FavRow = "600";

$ObjectCreator_TypeId = 0;

//SceneBrowserTree.rebuild
function Lab::initObjectCreator(%this)
{

	ObjectCreator.arrayCtrl = ObjectCreatorArray;
	//ObjectCreator-->ScriptEd.performclick();

	//WIP Fast - Only needed 1 time
	if ($ObjectCreator_Loaded && ObjectCreator.currentViewId !$= "")
		return;

	ObjectCreator.currentViewId = "";
	ObjectCreatorViewMenu.clear();
	%i = 0;

	while($ObjectCreator_View[%i] !$= "")
	{
		%text = getField($ObjectCreator_View[%i],0);
		ObjectCreatorViewMenu.add(%text,%i);
		%i++;
	}

	ObjectCreator.setViewId($ObjectCreator_ViewId);

	ObjectCreator.schedule(1, "navigate", "root");
}

$ObjCreatorMenu_Minimal = true;
//==============================================================================
//ObjectCreator.initFiles
function ObjectCreator::initFiles(%this,%path)
{
	%lastAddress = ObjectCreator.lastAddress;

	if (%lastAddress $= "")
		%lastAddress = "root";

	ObjectCreator.navigate(%lastAddress);
}
//------------------------------------------------------------------------------

//==============================================================================
function ObjectCreator::onCtrlResized(%this)
{

	//ObjectCreator.setViewId();
}
//------------------------------------------------------------------------------

//==============================================================================
function SB_ObjectCreatorBox::onResize(%this)
{

	if (isObject(ObjectCreatorArray))
	{
		%cols = $ObjectCreator_ViewId + 1;
		ObjectCreatorArray.extent.x = ObjectCreatorArray.getParent().extent.x - 20;
		%width = (ObjectCreatorArray.extent.x - 10) / %cols;
		ObjectCreatorArray.colSize = %width;
		ObjectCreatorArray.refresh();
	}
}
//------------------------------------------------------------------------------
