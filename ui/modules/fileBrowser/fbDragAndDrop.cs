//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function FileBrowserIcon::onMouseDragged(%this,%a1,%a2,%a3)
{
	if (!isObject(%this))
	{
		return;
	}

	startDragAndDropCtrl(%this,"FileBrowserIcon","FileBrowser.onIconDropped");
	hide(%this);
}
//------------------------------------------------------------------------------
//==============================================================================
function FileBrowserFav::onMouseDragged(%this,%a1,%a2,%a3)
{
	if (!isObject(%this))
	{
		return;
	}

	FileBrowserFavRemoveBin.visible = 1;
	startDragAndDropCtrl(%this,"FileBrowserFav","FileBrowser.onIconDropped");
	hide(%this);
}
//------------------------------------------------------------------------------
//==============================================================================
// Dropping a file icon
//==============================================================================

//==============================================================================
function FileBrowser::onIconDropped(%this,%droppedOn,%icon,%dropPoint)
{
	%originalIcon = %icon.dragSourceControl;
	show(%originalIcon);
	delObj(%icon);

	if (%originalIcon.class $= "FileBrowserFav")
		FileBrowserFavRemoveBin.visible =0;

	if (FileBrowser.isMethod("checkIconDrop"@%originalIcon.type))
		eval("FileBrowser.checkIconDrop"@%originalIcon.type@"(%originalIcon,%droppedOn,%dropPoint);");
}
//------------------------------------------------------------------------------

//==============================================================================
function FileBrowser::checkIconDropMesh(%this,%icon,%droppedOn,%dropPoint)
{

	if (%droppedOn.getName() $= "EWorldEditor")
	{

		ColladaImportDlg.showDialog(%icon.fullPath,%icon.createCmd);
	}
	else if (%droppedOn.getClassName() $= "GuiShapeLabPreview")
	{
		ShapeLab.selectFilePath(%icon.fullPath);
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function FileBrowser::checkIconDropPrefab(%this,%icon,%droppedOn,%dropPoint)
{

	if (%droppedOn.getName() $= "EWorldEditor")
	{

		eval(%icon.createCmd);
	}

	/*else if (%droppedOn.getClassName() $= "GuiShapeLabPreview"){

		//%worldPos = screenToWorld(%dropPoint);
		//$SceneEditor_DropSinglePosition = %worldPos;
		ShapeLab.selectFilePath(%icon.fullPath);

	}*/
}
//------------------------------------------------------------------------------
//==============================================================================
function FileBrowser::checkIconDropImage(%this,%icon,%droppedOn,%dropPoint)
{

	if (%droppedOn.class $= "MaterialMapCtrl")
	{
		%type = %droppedOn.internalName;

		if (MaterialEditorTools.isMethod("update"@%type@"Map"))
		{
			eval("MaterialEditorTools.update"@%type@"Map(true,%icon.fullPath,false);");
		}
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function FileBrowser::checkIconDropFav(%this,%icon,%droppedOn,%dropPoint)
{

	if (%droppedOn.dropType $= "FileBrowserFavButton")
	{
		%type = %droppedOn.internalName;
		FileBrowserFavArray.reorderChild(%icon,%droppedOn);
		FileBrowserFavArray.refresh();

	}
	else if (%droppedOn.superclass $= "FileBrowserFavRemover")
	{
		FileBrowser.removeFavoriteIcon(%icon);

	}
}
//------------------------------------------------------------------------------
