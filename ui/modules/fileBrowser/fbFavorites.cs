//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
//FileBrowser.toggleFavorites();
function FileBrowser::toggleFavorites(%this,%visible)
{

	hide(FileBrowserFavRemoveBin);

	if (%visible $= "")
		%visible = !FileBrowserFavFrame.visible;

	if (!%visible)
	{
		hide(FileBrowserFavFrame);
		//FileBrowserFrameSet.reorderChild(FileBrowserFileFrame,FileBrowserFavFrame);
		//FileBrowserFrameSet.defaultRows = FileBrowserFrameSet.rows;
		//FileBrowserFrameSet.rows = "0";
	}
	else
	{
		show(FileBrowserFavFrame);
		//FileBrowserFrameSet.reorderChild(FileBrowserFavFrame,FileBrowserFileFrame);
		//%rows = FileBrowserFrameSet.defaultRows;

		//if (getWordCount(%rows)!$= "2")
		//%rows = "0 100";

		//FileBrowserFrameSet.rows = %rows;
	}

	FileBrowserFavButton.setStateOn(!FileBrowserFavFrame.visible);
	//FileBrowserFrameSet.updateSizes();

}
//------------------------------------------------------------------------------
//==============================================================================
//FileBrowser.addCurrentToFavorite
function FileBrowser::addCurrentToFavorite(%this)
{

	%path = FileBrowser.address;
	info("Adding:",%path,"To FileBrowser favorites");
	%fileRead = getFileReadObj($TLabFileBrowserFavFile);
	%id = 0;

	while(!%fileRead.isEOF())
	{
		%line = %fileRead.readLine();

		if (%line $= %path)
			%exist = true;

	}

	closeFileObj(%fileRead);

	if (%exist)
	{
		info("The folder is already a favorite");
		return;
	}

	%fileWrite= getFileWriteObj($TLabFileBrowserFavFile,true);
	%fileWrite.writeLine(%path);
	closeFileObj(%fileWrite);
	FileBrowser.getFavorites();
}
//------------------------------------------------------------------------------
//==============================================================================
function FileBrowser::addFavoriteIcon(%this, %text,%dropZone)
{
	%folder = strReplace(%text," ","/");
	%ctrl = Lab.createArrayIcon(FileBrowserIconFolderSrc);

	%ctrl.iconBitmap = "";
	%ctrl.text = %folder;
	%ctrl.address = %text;
	%ctrl.tooltip = %text;

	if (!%dropZone)
	{
		%ctrl.command = "FileBrowser.navigateFav($ThisControl);";
		%ctrl.altCommand = "FileBrowser.navigateFav($ThisControl);";
		%ctrl.class = "FileBrowserFav";
	}
	else
	{
		%ctrl.command = "";
		%ctrl.altCommand = "";
	}

	%ctrl.dropType = "FileBrowserFavButton";

	%ctrl.profile = "ToolsButtonBase_S1";
	%ctrl.buttonType = "radioButton";
	%ctrl.groupNum = "-1";
	%ctrl.buttonMargin = "6 0";
	%ctrl.textMargin = "8";
	%ctrl.sizeIconToButton = true;
	%ctrl.makeIconSquare = true;
	%ctrl.extent.y = "20";
	%ctrl.type = "Fav";
	FileBrowserFavArray.addGuiControl(%ctrl);
}
//------------------------------------------------------------------------------
//==============================================================================
function FileBrowser::navigateFav(%this, %icon)
{

	%folder = %icon.address;
	// Because this is called from an IconButton::onClick command
	// we have to wait a tick before actually calling navigate, else
	// we would delete the button out from under itself.
	%this.schedule(1, "navigate", %folder);
}
//------------------------------------------------------------------------------
//==============================================================================
// Get the saved favorites folder and list them (except invalid folders)
//FileBrowser.getFavorites();
function FileBrowser::getFavorites(%this)
{
	FileBrowserFavArray.clear();
	/*	if (isObject("FileBrowserFavArray")){
			FileBrowserFavArray.empty();
			FileBrowser.favArray = FileBrowserFavArray;
		}
		else{
			FileBrowser.favArray = newArrayObject("FileBrowserFavArray");
		}*/
	%fileRead = getFileReadObj($TLabFileBrowserFavFile);
	%id = 0;

	while(!%fileRead.isEOF())
	{
		%line = %fileRead.readLine();

		if (%line $= "")
			continue;

		%folder = strReplace(trim(%line)," ","/");

		//Dont list invalid folders
		if (!isDirectory(%folder))
		{
			//warnLog("Invalid folder found in favorite:",%folder," Invalid folders are not listed but stay in settings in case added in future");
			continue;
		}

		%this.addFavoriteIcon(%line);

	}

	closeFileObj(%fileRead);
	%this.addFavoriteIcon("<>",true);
	FileBrowserFavArray.updateStack();
	FileBrowserFavArray.getParent().computeSizes();
}
//------------------------------------------------------------------------------
//==============================================================================
//FileBrowser.removeFavoriteIcon(37429);
function FileBrowser::removeFavoriteIcon(%this,%icon)
{
	%address = %icon.address;
	%newText = "";
	%fileRead = getFileReadObj($TLabFileBrowserFavFile);
	%id = 0;

	while(!%fileRead.isEOF())
	{
		%line = %fileRead.readLine();

		if (%line $= %address || trim(%line) $= "")
			continue;

		%newText = strAddRecord(%newText,%line);
	}

	closeFileObj(%fileRead);

	%fileWrite= getFileWriteObj($TLabFileBrowserFavFile);

	for(%i = 0; %i < getRecordCount(%newText); %i++)
	{
		%writeLine = getRecord(%newText,%i);

		if (getWordCount(%writeLine) > 0)
			%fileWrite.writeLine(getRecord(%newText,%i));
	}

	closeFileObj(%fileWrite);
	FileBrowserFavArray.remove(%icon);

	FileBrowserFavArray.refresh();
	FileBrowserFavArray.getParent().computeSizes();

}
//------------------------------------------------------------------------------
//==============================================================================
// Set Favorite panel position to top or bottom
function FileBrowser::favPanelPos(%this,%location)
{
	FileBrowserFavFrame.docking = %location;

	if (%location $= "Top")
	{
		//FileBrowserFrameSet.reorderChild(FileBrowserFavFrame,FileBrowserFileFrame);
		SideFavIconToBottom.visible = 1;
		SideFavIconToTop.visible = 0;
	}
	else if (%location $= "Bottom")
	{
		//FileBrowserFrameSet.reorderChild(FileBrowserFileFrame,FileBrowserFavFrame);
		SideFavIconToBottom.visible = 0;
		SideFavIconToTop.visible = 1;
	}

	SideFavIconToTop.getParent().updateStack();
}
//------------------------------------------------------------------------------

