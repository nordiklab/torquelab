//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$FileBrowserNewSystem = true;

//==============================================================================
function FileBrowserMenu::onSelect(%this, %id, %text)
{
	%split = strreplace(trim(%text), "//", "/");

	FileBrowser.goToFolder(%split);
}
//------------------------------------------------------------------------------

//==============================================================================
// Refresh current folder when listig options changed
function FileBrowser::refreshCurrentFolder(%this)
{
	FileBrowser.goToFolder(FileBrowser.currentFolder);
}
//------------------------------------------------------------------------------

//==============================================================================
// Navigation system using address (path without slashed)
//==============================================================================

//==============================================================================
// Set the folder from which to show the files and folders
function FileBrowser::goToAddress(%this, %address)
{

	//If no address setted must be first time so set default address
	if (%address $= "")
		%address = $FileBrowser_DefaultFolder;

	%path = strReplace(trim(%address)," ","/");
	//%path = %path@"/";
	%this.goToFolder(%path);
}
//------------------------------------------------------------------------------
//==============================================================================
function FileBrowser::navigateDown(%this, %folder)
{
	if (%this.address $= "")
		%address = %folder;
	else
		%address = %this.address SPC %folder;

	// Wait after function to make sure icon is deleted
	%this.schedule(1, "goToAddress", %address);

}
//------------------------------------------------------------------------------
//==============================================================================
//FileBrowser.navigateUp
function FileBrowser::navigateUp(%this)
{
	%count = getWordCount(%this.address);

	if (%count == 0)
		return;

	if (%count == 1)
		%address = "";
	else
		%address = getWords(%this.address, 0, %count - 2);

	// Wait after function to make sure icon is deleted
	%this.schedule(1, "goToAddress", %address);

}
//------------------------------------------------------------------------------

//==============================================================================
// List files inside specifed folder
//==============================================================================

//==============================================================================
// Set the folder from which to show the files and folders
function FileBrowser::goToFolder(%this, %folder)
{
	//if (%folder $= "/")
	//%folder = "art/";
   if (%folder $= "-1")
      return;
	%folder = strreplace(%folder,"//","/");

	if (getSubStr(%folder,0,1) $= "/")
		%folder = getSubStr(%folder,1);

	if (!IsDirectory(%folder) && %folder !$= "")
	{
		warnLog("Invalid folder, must leave",%folder);
		return;
	}
	else if ($FileBrowser_CurrentFolder $= %folder)
	{
		return;
	}

	//Check for double dash
	%folder = strreplace(%folder,"//","/");
	FileBrowser.address = strreplace(%folder,"/"," ");
	FileBrowser.currentFolder = %folder;
	$FileBrowser_CurrentFolder = %folder;
	FileBrowserArray.clear();
	FileBrowser.addFolderUpIcon();
	//Start by adding the folders
	%dirs = getDirectoryList(%folder,0);

	for(%i = 0; %i < getFieldCount(%dirs); %i++)
	{
		%dir = getField(%dirs,%i);
		%fullDir = %folder@"/"@%dir;
		%this.addFolderItem(%dir,%fullDir);
		//Check if this is a new folder
		%r = FileBrowserMenu.findText(%dir);

		if (%r == -1)
		{
			FileBrowserMenu.add(%dir);
			%sortMenu = true;
		}
	}

	getMultiExtensionFileList("","dts dae png dds tga prefab",true);
	//Add the files after the folder
	%exts = "dts dae png dds tga prefab";
	%files = getMultiExtensionFileList(%folder,%exts,true);

	for(%i = 0; %i < getRecordCount(%files); %i++)
	{
		%file = getRecord(%files,%i);
		%fullPath = %folder@"/"@%file;
		%this.addFileItem(%fullPath);
	}

	%this.setViewId($FileBrowser_ViewId);
	FileBrowserArray.refresh();

	if (%sortMenu)
		FileBrowserMenu.sort();

	FileBrowserMenu.setText(%folder);
}

//==============================================================================
// Go to currently selected objects folder (if applicable)

function FileBrowser::goToCurrentFolder(%this)
{
	%sel = EWorldEditor.getSelectedObject(0);

	if (!isObject(%sel))
		return;

	if (isFile(%sel.shapeName))
	{
		%file = %sel.shapeName;
		%path = filePath(%file);
	}
	else if (isFile(%sel.fileName))
	{
		%file = %sel.fileName;
		%path = filePath(%file);
	}

	if (%path $= "")
	{
		warnLog("Couldn't find a path for currentlt selected object:", %sel);
		return;
	}

	//%address = strReplace( %path,"/"," ");
	%this.goToFolder(%path);
}
//------------------------------------------------------------------------------

