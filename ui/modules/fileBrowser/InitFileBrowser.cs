//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$FileBrowser_View[0] = "1 Column" TAB "Col 1";
$FileBrowser_View[1] = "2 Column" TAB "Col 2";
$FileBrowser_View[2] = "3 Column" TAB "Col 3";
$FileBrowser_ViewId = 0;
$FileBrowser_CurrentFolder = "-1"; //Not empty cause it will skip update if root
$FileBrowser_DefaultFolder = "";
$FileBrowser_FavRow = "600";
//SceneBrowserTree.rebuild
//==============================================================================
// called from Lab::initSideBar  (check from SideBarMainBook::onTabSelected)
function Lab::initFileBrowser(%this)
{
	//Moved here so we are sure $TLabLayoutFolder is setted
	$TLabFileBrowserFolder = $TLabLayoutFolder @ "content/FileBrowser/";
	$TLabFileBrowserFavFile = $TLabFileBrowserFolder@"settings/storedFavs.txt";
	FileBrowser.getFavorites();

	FileBrowser.initFiles();

	FileBrowser.currentViewId = "";
	FileBrowserViewMenu.clear();
	%i = 0;

	while($FileBrowser_View[%i] !$= "")
	{
		%text = getField($FileBrowser_View[%i],0);
		FileBrowserViewMenu.add(%text,%i);
		%i++;
	}

	FileBrowser.setViewId($FileBrowser_ViewId);
	//FileBrowserViewMenu.setSelected($FileBrowser_ViewId);
	$FileBrowserInit = true;

	FileBrowser-->QuickPrefabRollout.expanded = 0;
	FileBrowser.arrayCtrl = FileBrowserArray;
	hide(FileBrowserOptionCtrl);
	FileBrowserFavButton.setStateOn(!FileBrowserFavFrame.visible);
	hide(FileBrowserInfo);
}
//------------------------------------------------------------------------------

//==============================================================================
function FileBrowser::initFiles(%this)
{
	FileBrowser.getAllFolders();
	%lastAddress = FileBrowser.lastAddress;

	if (%lastAddress $= "")
		%lastAddress = $FileBrowser_DefaultFolder;

	FileBrowser.goToAddress(%lastAddress);

}
//------------------------------------------------------------------------------
//==============================================================================
function FileBrowser::onWake(%this)
{
	FileBrowser.goToAddress($FileBrowser_CurrentFolder);
}
//------------------------------------------------------------------------------

//==============================================================================
// Get all folders under art/ and add them in folder list menu
function FileBrowser::getAllFolders(%this)
{
	//Start by adding the folders
	FileBrowserMenu.clear();
	%dirs = getDirectoryList($FileBrowser_DefaultFolder,100);

	for(%i = 0; %i < getFieldCount(%dirs); %i++)
	{
		%dir = getField(%dirs,%i);
		FileBrowserMenu.add($FileBrowser_DefaultFolder@"/"@%dir,%i++);
	}

	FileBrowserMenu.sort();
}
//------------------------------------------------------------------------------
//==============================================================================
// Called when FileBrowser extent changed so the list can be adjusted
function FileBrowser::onCtrlResized(%this)
{
	FileBrowser.setViewId();
}
//------------------------------------------------------------------------------
//==============================================================================
// Called from onInitialEditorLaunch to set the initial sidebar states
function SB_FileBrowserBox::onResize(%this)
{

	if (isObject(FileBrowserArray))
	{
		%cols = $FileBrowser_ViewId + 1;
		FileBrowserArray.extent.x = FileBrowserArray.getParent().extent.x - 20;
		%width = (FileBrowserArray.extent.x - 10) / %cols;
		FileBrowserArray.colSize = %width;
		FileBrowserArray.refresh();
	}
}
//------------------------------------------------------------------------------
