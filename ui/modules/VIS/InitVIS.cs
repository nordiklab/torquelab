//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$VisibilityOptionsLoaded = false;
$VisibilityClassLoaded = false;
$SideBarVIS_Initialized = false;

$TLabVisPresetFolder = "tlab/ui/modules/VIS/presets/";
$TLabVisSettingFolder = "tlab/ui/modules/VIS/settings/";
$TLabVisSettingFileOrder = $TLabVisSettingFolder@"classOrder.cs";
$SideBarVISOrderMode[0] = "Custom Order";
$SideBarVISOrderMode[1] = "Class name";
$SideBarVISOrderModeId["Name"] = 1;

//==============================================================================
function SideBarVIS::init(%this)
{
	//exec($SideBarVIS_OrderPresetFile);

	// Create the array if it doesn't already exist.
	if (!isObject(ar_SideBarVIS))
		%array = newArrayObject("ar_SideBarVIS");

	// Create the array if it doesn't already exist.
	if (!isObject(ar_SideBarVISClass))
		%classArray = newArrayObject("ar_SideBarVISClass");

	%this.array = ar_SideBarVIS;
	%this.classArray = ar_SideBarVISClass;
	%this.getPresets();

	if ($Cfg_VisOrderMode !$= "0" && $Cfg_VisOrderMode !$= "1")
		$Cfg_VisOrderMode = "0";

	SideBarVIS_SortMenu.clear();
	SideBarVIS_SortMenu.add($SideBarVISOrderMode[0],0);
	SideBarVIS_SortMenu.add($SideBarVISOrderMode[1],1);
	SideBarVIS_SortMenu.setText($SideBarVISOrderMode[$Cfg_VisOrderMode]);

	if (!$SideBarVIS_Initialized)
	{
		//SideBarVIS.position = visibilityToggleBtn.position;
		SideBarVIS.initOptionsArray();
		SideBarVIS.initClassArray();

		$SideBarVIS_Initialized = true;
	}

	SideBarVIS_CheckPill.visible = 0;
}
//------------------------------------------------------------------------------

//==============================================================================
function SideBarVIS::onPreSave(%this)
{
	hide(SideBarVis_PillSources);
	//LabSideDualVisList.clear(); //Stored embed since take some time to generate
}
//------------------------------------------------------------------------------
//==============================================================================
function SideBarVIS::onPostSave(%this)
{
	hide(SideBarVis_PillSources);
}
//------------------------------------------------------------------------------
//==============================================================================
function SideBarVIS::onShow(%this)
{
	%this.init();

}

//==============================================================================
function Lab::setVisibleDistanceScale(%this,%value)
{
	$Cfg_Common_Objects_visibleDistanceScale = %value;
	$pref::WorldEditor::visibleDistanceScale = %value;
}
//==============================================================================
function SideBarVIS::onCtrlResized(%this)
{

}

