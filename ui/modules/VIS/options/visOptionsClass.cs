//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$ModOptVis_EnableClassInfo = "Check/Uncheck the classes you want to see in the VIS class settings.";
function ModOptDlg_VIS::onVisible(%this,%isVisible)
{

	if (%isVisible)
		ModOptVis_EnableClassInfo.setText($ModOptVis_EnableClassInfo);
}
//==============================================================================
// VIS Class Options Listing and Sorting
//==============================================================================

//==============================================================================
// Update the Class Options List (CUrrently stored in GUI. For manual use)
//ModOptDlg_VIS.buildClassOptions();
function ModOptDlg_VIS::buildClassOptions(%this)
{
	VisOpt_EnabledClassList.clear();
	VisOpt_EnabledClassList.visible = 1;
	VisOpt_EnabledClassPill.visible = 0;

	for(%i = 0; %i < SideBarVIS.classArray.count(); %i++)
	{
		%class = SideBarVIS.classArray.getKey(%i);
		%visVar = "$ModOptVIS_Class_" @ %class @ "_isEnabled";
		%selVar = "$" @ %class @ "::isSelectable";
		%textLength = strlen(%class);
		%text = "  " @ %class;
		eval("%value = "@%visVar@";");

		if (%value $= "")
			eval(%visVar @ " = \"1\";");

		%visPill = cloneGui(VisOpt_EnabledClassPill,VisOpt_EnabledClassList);
		// %visPill.setName("Vis_Dual_"@%class);
		%checkbox = %visPill-->classcheck;
		%checkbox.superClass = "ModOptVisClassCheckbox";
		%checkbox.variable = %visVar;
		%checkbox.text = %class;
		%mouse = %visPill-->mouse;
		%mouse.fitIntoParent();
		%mouse.myPill = %visPill;
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Update the Class Options List (CUrrently stored in GUI. For manual use)
//ModOptDlg_VIS.buildClassOptions();
function ModOptDlg_VIS::buildClassOptions(%this)
{
	VisOpt_EnabledClassList.clear();
	VisOpt_EnabledClassList.visible = 1;

	for(%i = 0; %i < SideBarVIS.classArray.count(); %i++)
	{
		%class = SideBarVIS.classArray.getKey(%i);
		%visVar = "$ModOptVIS_Class_" @ %class @ "_isEnabled";
		%selVar = "$" @ %class @ "::isSelectable";
		%textLength = strlen(%class);
		%text = "  " @ %class;
		eval("%value = "@%visVar@";");

		if (%value $= "")
			eval(%visVar @ " = \"1\";");

		%visPill = cloneGui(VisOpt_EnabledClassPill,VisOpt_EnabledClassList);
		// %visPill.setName("Vis_Dual_"@%class);
		%checkbox = %visPill-->classcheck;
		%checkbox.variable = %visVar;
		%checkbox.text = %class;
		%mouse = %visPill-->mouse;
		%mouse.fitIntoParent();
		%mouse.myPill = %visPill;
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// Update the Class Options List (CUrrently stored in GUI. For manual use)
//ModOptDlg_VIS.buildClassOptions();
function ModOptDlg_VIS::exportOptions(%this)
{
	export("$ModOptVIS_*",$TLabVisSettingFolder@"enabledClasses.cs");
}
//------------------------------------------------------------------------------
//==============================================================================
// Update the Class Options List (CUrrently stored in GUI. For manual use)
//ModOptDlg_VIS.buildClassOptions();
function ModOptVisClassCheckbox::onClick(%this)
{
	%class = %this.text;
	%pill = SideBarVIS-->theClassVisList.findObjectByInternalName(%class);

	if (!isObject(%pill))
		return;

	%pill.visible = $ModOptVIS_Class_[%class,"isEnabled"];

}
//------------------------------------------------------------------------------
