//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
//SideBarVIS.getPresets
function SideBarVIS::getPresets(%this)
{
	//SideBarVIS.menuIds = strAddWord(SideBarVIS.menuIds,MenuBarVISMenu,1);
	//SideBarVIS.menuIds = strAddWord(SideBarVIS.menuIds,SideBarVIS_PresetMenu.getId(),1);
	ELink.add("MenuVisibility",MenuBarVISMenu);
	ELink.add("MenuVisibility",SideBarVIS_PresetMenu);
	ELink.menuClear("MenuVisibility");
	ELink.menuAdd("MenuVisibility","Select a preset",0);

	%pid = 0;
	%selected ="Select a preset";
	%searchFolder = $TLabVisPresetFolder@"*.layers.ucs";

	for(%presetFile = findFirstFile(%searchFolder); %presetFile !$= ""; %presetFile = findNextFile(%searchFolder))
	{
		%presetName = strreplace(fileBase(%presetFile),".layers","");

		if (SideBarVIS.currentPresetFile $= %presetName)
			%selected = %presetName;

		ELink.menuAdd("MenuVisibility",%presetName,%pid++);

	}

	ELink.setText("MenuVisibility",%selected);
}
//------------------------------------------------------------------------------

//==============================================================================
function SideBarVIS::savePreset(%this,%name)
{
	//In general no name is supplied so we use the preset editCtrl text
	if (%name $= "")
		%name = SideBarVIS_PresetName.getText();

	if (strFind(%name,"["))
	{
		return;
	}

	%layerExampleObj = newScriptObject();
	%layerExampleObj.internalName = %name;

	for(%i = 0; %i < %this.classArray.count(); %i++)
	{
		%class = %this.classArray.getKey(%i);
		eval("%selectable = $" @ %class @ "::isSelectable;");
		eval("%renderable = $" @ %class @ "::isRenderable;");
		%layerExampleObj.selectable[%class] = %selectable;
		%layerExampleObj.visible[%class] = %renderable;
	}

	for(%i = 0; %i < %this.array.count(); %i++)
	{
		%text = "  " @ %this.array.getValue(%i);
		%val = %this.array.getKey(%i);
		%var = getWord(%val, 0);
		eval("%value = "@%var@";");
		%formatVar = strreplace(%var,"$","");
		%formatVar = strreplace(%formatVar,"::","__");
		%formatVar = strreplace(%formatVar,".","_dot_");
		%layerExampleObj.visOptions[%formatVar] = %value;
	}

	%layerExampleObj.save($TLabVisPresetFolder@%name@".layers.ucs",0,"%presetLayers = ");
	//%this.toggleNewPresetCtrl(false);
	SideBarVIS.loadPresetFile(%name);
	%this.getPresets();

	return %name;

}
//------------------------------------------------------------------------------
//==============================================================================

//SideBarVIS.loadPresetFile("visBuilder");
function SideBarVIS::loadPresetFile(%this,%filename,%noStore)
{

	%activePreset = %this.loadPresetFromFile(%filename,%noStore);

	SideBarVIS.activePreset = %activePreset;
	SideBarVIS_PresetName.setText(%activePreset);
	SideBarVIS_PresetMenu.setText("");
}
//------------------------------------------------------------------------------
//==============================================================================
function MenuVisibilityPreset::onSelect(%this,%id,%text)
{
	if (%id $= "0")
		return;

	SideBarVIS.loadPresetFile(%text);
}
//------------------------------------------------------------------------------
//==============================================================================
function MenuVisibilityPreset::onAdd(%this,%value)
{

	ELink.link(%this,"MenuVisibility");
	SideBarVIS.menuIds = strAddWord(SideBarVIS.menuIds,%this.getId(),1);
	$TLabVisMenuListed[%this.getId()] = true;

}
//------------------------------------------------------------------------------
//==============================================================================
function MenuVisibilityPreset::onWake(%this,%value)
{
	if (!$TLabVisMenuListed[%this.getId()])
		SideBarVIS.menuIds = strAddWord(SideBarVIS.menuIds,%this.getId(),1);
}
//------------------------------------------------------------------------------
//==============================================================================
$TLabVisCommonClasses = "TSStatic StaticShape Prefab Trigger TerrainBlock ScatterSky PointLight SpotLight Zone OcclusionVolume Portal MissionMarker WaterPlane";

//------------------------------------------------------------------------------
//==============================================================================
//EVisibilityLayers.loadPresetFile("visBuilder");
function SideBarVIS::loadPresetFromFile(%this,%filename,%noStore)
{

	if (!isObject(SideBarVIS.classArray))
		SideBarVIS.initClassArray();

	%file = $TLabVisPresetFolder@%filename@".layers.ucs";

	if (!isFile(%file))
		return;

	exec(%file);

	for(%i = 0; %i < %this.classArray.count(); %i++)
	{
		%class = %this.classArray.getKey(%i);
		%selectable = %presetLayers.selectable[%class];
		%renderable = %presetLayers.visible[%class];

		if (%selectable !$= "")
		{
			eval("$" @ %class @ "::isSelectable = \""@%selectable@"\";");
			//info("Class:",%class,"isSelectable set to:",%selectable);
		}

		if (%renderable !$= "")
		{
			eval("$" @ %class @ "::isRenderable = \""@%renderable@"\";");
			//info("Class:",%class,"isRenderable set to:",%renderable);
		}
	}

	for(%i = 0; %i < %this.array.count(); %i++)
	{
		%text = "  " @ %this.array.getValue(%i);
		%val = %this.array.getKey(%i);
		%var = getWord(%val, 0);
		%formatVar = strreplace(%var,"$","");
		%formatVar = strreplace(%formatVar,"::","__");
		%formatVar = strreplace(%formatVar,".","_dot_");
		%value = %presetLayers.visOptions[%formatVar];

		if (%value $= "")
			continue;

		eval(%var@" = %value;");
		//info("Variable:",%var," set to:",%value);
	}

	Lab.showLights(%presetLayers.visiblePointLight);
	%presetName = %filename;

	if (!%noStore)
	{
		%this.currentPresetFile = %filename;
	}
	else
	{
		%presetName = "*"@%presetName;
	}

	LabSideDualVisList.refresh();
	%this.activePreset = %presetName;
	return %presetName;
}
