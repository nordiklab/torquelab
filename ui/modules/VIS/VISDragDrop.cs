//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

// Start dragging a toolbar icon
function VisPill::onMouseDragged(%this)
{
	%callback = "Lab.onVISPillDropped";
	startDragAndDropCtrl(%this.parentGroup,"Toolbar",%callback);
	hide(%this.parentGroup);
}

//------------------------------------------------------------------------------

// Dragged control dropped over undefined control (gonna check if it's droppable)
function Lab::onVISPillDropped(%this,%dropOnCtrl, %draggedControl, %dropPoint)
{
	%droppedCtrl = %draggedControl.dragSourceControl;
	show(%droppedCtrl);

	if (%dropOnCtrl.superClass !$= "VisPill")
	{
		warnLog("VIS dropped on non VIS pill:",%dropOnCtrl.getClassName(),%dropOnCtrl);
		return;
	}

	info("Dropped on",%droppedCtrl,"Pill",%dropOnCtrl.parentGroup,"Dropped",%droppedCtrl);
	LabSideDualVisList.reorderChild(%droppedCtrl,%dropOnCtrl.parentGroup);
	SideBarVis.schedule(100,exportOrder);
}

//------------------------------------------------------------------------------

// Export the order in which VIS Classes are listed
function SideBarVIS::exportOrder(%this)
{

	%orderID = 0;

	foreach(%ctrl in LabSideDualVisList)
	{
		%class = trim(%ctrl-->class.text);

		if (%done[%class])
			continue;

		$SideBarVIS_OrderClass[%class] = %orderID++;
	}

	export("$SideBarVIS_OrderClass*",$TLabVisSettingFileOrder);
}
//------------------------------------------------------------------------------

