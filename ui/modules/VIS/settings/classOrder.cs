$SideBarVIS_OrderClass = 81;
$SideBarVIS_OrderClassAccumulationVolume = 43;
$SideBarVIS_OrderClassAIPlayer = 13;
$SideBarVIS_OrderClassAITurretShape = 24;
$SideBarVIS_OrderClassBasicClouds = 37;
$SideBarVIS_OrderClassCamera = 29;
$SideBarVIS_OrderClassCameraBookmark = 25;
$SideBarVIS_OrderClassCloudLayer = 44;
$SideBarVIS_OrderClassConvexShape = 35;
$SideBarVIS_OrderClassCoverPoint = 26;
$SideBarVIS_OrderClassDebris = 34;
$SideBarVIS_OrderClassDecalManager = 42;
$SideBarVIS_OrderClassDecalRoad = 40;
$SideBarVIS_OrderClassExplosion = 41;
$SideBarVIS_OrderClassFlyingVehicle = 47;
$SideBarVIS_OrderClassForest = 27;
$SideBarVIS_OrderClassForestWindEmitter = 28;
$SideBarVIS_OrderClassfxFoliageReplicator = 52;
$SideBarVIS_OrderClassfxShapeReplicatedStatic = 77;
$SideBarVIS_OrderClassfxShapeReplicator = 54;
$SideBarVIS_OrderClassGameBase = 57;
$SideBarVIS_OrderClassGroundCover = 58;
$SideBarVIS_OrderClassGroundPlane = 30;
$SideBarVIS_OrderClassHoverVehicle = 31;
$SideBarVIS_OrderClassItem = 3;
$SideBarVIS_OrderClassLightBase = 32;
$SideBarVIS_OrderClassLightning = 33;
$SideBarVIS_OrderClassMarker = 22;
$SideBarVIS_OrderClassMeshRoad = 23;
$SideBarVIS_OrderClassMissionMarker = 10;
$SideBarVIS_OrderClassNavMesh = 36;
$SideBarVIS_OrderClassNavPath = 11;
$SideBarVIS_OrderClassOcclusionVolume = 9;
$SideBarVIS_OrderClassParticleEmitter = 21;
$SideBarVIS_OrderClassParticleEmitterNode = 45;
$SideBarVIS_OrderClassPathCamera = 46;
$SideBarVIS_OrderClassPhysicalZone = 48;
$SideBarVIS_OrderClassPhysicsDebris = 49;
$SideBarVIS_OrderClassPhysicsForce = 50;
$SideBarVIS_OrderClassPhysicsShape = 51;
$SideBarVIS_OrderClassPlayer = 5;
$SideBarVIS_OrderClassPointLight = 18;
$SideBarVIS_OrderClassPortal = 8;
$SideBarVIS_OrderClassPrecipitation = 53;
$SideBarVIS_OrderClassPrefab = 2;
$SideBarVIS_OrderClassProjectile = 60;
$SideBarVIS_OrderClassProximityMine = 61;
$SideBarVIS_OrderClassRenderMeshExample = 80;
$SideBarVIS_OrderClassRenderObjectExample = 79;
$SideBarVIS_OrderClassRenderShapeExample = 78;
$SideBarVIS_OrderClassRibbon = 62;
$SideBarVIS_OrderClassRibbonNode = 63;
$SideBarVIS_OrderClassRigidShape = 64;
$SideBarVIS_OrderClassRiver = 20;
$SideBarVIS_OrderClassScatterSky = 17;
$SideBarVIS_OrderClassSceneObject = 65;
$SideBarVIS_OrderClassScopeAlwaysShape = 66;
$SideBarVIS_OrderClassSFXEmitter = 12;
$SideBarVIS_OrderClassSFXSpace = 67;
$SideBarVIS_OrderClassShapeBase = 68;
$SideBarVIS_OrderClassSkyBox = 69;
$SideBarVIS_OrderClassSpawnSphere = 59;
$SideBarVIS_OrderClassSplash = 70;
$SideBarVIS_OrderClassSpotLight = 14;
$SideBarVIS_OrderClassStaticShape = 4;
$SideBarVIS_OrderClassSun = 2;
$SideBarVIS_OrderClassTerrainBlock = 16;
$SideBarVIS_OrderClassTimeOfDay = 72;
$SideBarVIS_OrderClassTrigger = 6;
$SideBarVIS_OrderClassTSStatic = 1;
$SideBarVIS_OrderClassTurretShape = 73;
$SideBarVIS_OrderClassVehicle = 39;
$SideBarVIS_OrderClassVehicleBlocker = 74;
$SideBarVIS_OrderClassVolumetricFog = 75;
$SideBarVIS_OrderClassVolumetricFogRTManager = 76;
$SideBarVIS_OrderClassWaterBlock = 38;
$SideBarVIS_OrderClassWaterObject = 56;
$SideBarVIS_OrderClassWaterPlane = 19;
$SideBarVIS_OrderClassWayPoint = 15;
$SideBarVIS_OrderClassWheeledVehicle = 55;
$SideBarVIS_OrderClassZone = 7;
