//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// VIS Class Options Listing and Sorting
//==============================================================================

//==============================================================================
function SideBarVIS_SortMenu::onSelect(%this,%id,%text)
{
	if (%id $= "1")
		%mode = "Name";

	SideBarVIS.sortClass(%mode);

	$Cfg_VisOrderMode = %id;
}
//------------------------------------------------------------------------------
//==============================================================================
function SideBarVIS::sortClass(%this,%mode)
{
	if (%mode $= "Name")
		%func = "sideVisSortNameAsc";
	else
		%func = "sideVisOrder";

	%id = ($SideBarVISOrderModeId[%mode]  $= "") ? 0 : 1;
	SideBarVIS_SortMenu.setText($SideBarVISOrderMode[%id] );

	LabSideDualVisList.sort(%func);
	LabSideDualVisList.sortMode = %mode;
	LabSideDualVisList.pushToBack(LabSideDualVisList-->SpacerEnd);
	LabSideDualVisList.updateStack();
}
//------------------------------------------------------------------------------

//==============================================================================
// Order by Custom Class Ordering
function sideVisOrder(%a,%b)
{
	%classA = trim(%a-->class.text);
	%classB = trim(%b-->class.text);
	%orderA = $SideBarVIS_OrderClass[%classA];
	%orderB = $SideBarVIS_OrderClass[%classB];

	if (%orderA > %orderB )
		return 1;

	if (%orderB > %orderA)
		return -1;

	return 0;
}
//------------------------------------------------------------------------------
//==============================================================================
//Order by Ascending Name
function sideVisSortNameAsc(%a,%b)
{

	%classA = strlwr(trim(%a-->class.text));
	%classB = strlwr(trim(%b-->class.text));
	%result = strcmp(%classA,%classB);
	return %result;
}
//------------------------------------------------------------------------------

//==============================================================================
function LabSideDualVisList::refresh(%this)
{
	foreach(%ctrl in LabSideDualVisList)
	{
		%class = trim(%ctrl-->class.text);

		if (%class $= "")
			continue;

		eval("%render = $" @ %class @ "::isRenderable;");
		eval("%select = $" @ %class @ "::isSelectable;");
		%ctrl-->visible.setStateOn(%render);
		%ctrl-->select.setStateOn(%select);

	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Object Crator Listing View Mode
//==============================================================================

//==============================================================================
function SideBarVIS::initClassArray(%this,%mode)
{
	ar_SideBarVISClass.empty();

	switch$(%mode)
	{
		case "Common":
			foreach$(%class in $TLabVisCommonClasses)
				%this.classArray.push_back(%class);

		case "Grouped":
			foreach$(%group in $TLabVisGroups)
				%this.classArray.push_back(%group);

		default:
			%classList = enumerateConsoleClasses("SceneObject");
			%classCount = getFieldCount(%classList);

			for(%i = 0; %i < %classCount; %i++)
			{
				%className = getField(%classList, %i);
				%this.classArray.push_back(%className);
			}
	}

	// Remove duplicates and sort by key.
	%this.classArray.uniqueKey();
	%this.classArray.sortka();

	if (LabSideDualVisList.getCount() <= 1)
		SideBarVIS.updateClassOptions();

	$VisibilityClassLoaded = true;
}
//------------------------------------------------------------------------------
//==============================================================================
// Update the Class Options List (CUrrently stored in GUI. For manual use)
//SideBarVIS.updateClassOptions();
function SideBarVIS::updateClassOptions(%this,%sortMode)
{
	%visSelList = %this-->theClassVisList;
	%visSelList.clear();
	%visSelList.visible = 1;

	for(%i = 0; %i < %this.classArray.count(); %i++)
	{
		%class = %this.classArray.getKey(%i);
		%visVar = "$" @ %class @ "::isRenderable";
		%selVar = "$" @ %class @ "::isSelectable";
		%textLength = strlen(%class);
		%text = "  " @ %class;

		%visPill = cloneObject(SideBarVIS_DualPill);
		%visPill.internalName = %class;
		// %visPill.setName("Vis_Dual_"@%class);
		%visPill-->visible.variable = %visVar;
		%visPill-->select.variable = %selVar;
		%visPill-->visible.command = "SideBarVIS.toggleRenderable(\""@%class@"\");";
		%visPill-->select.command = "SideBarVIS.toggleSelectable(\""@%class@"\");";
		%visPill-->class.text = %text;
		%visPill.tooltip = "Show/hide all " @ %class @ " objects.";
		%visPill-->mouse.extent = %visPill.extent;
		%VisSelList.addGuiControl(%visPill);

	}

	//Add bottom spacer to fix issue of not able to scroll to last options
	%endSpacer = cloneGui(SideBarScrollEndSpacer,%visSelList);
	%endSpacer.alwaysLast = true;
	SideBarVIS.sortClass(LabSideDualVisList.sortMode);
	//%VisSelList.updateStack();
}
//------------------------------------------------------------------------------
//==============================================================================
// Update the Class Options List (CUrrently stored in GUI. For manual use)
//SideBarVIS.updateClassList();
function SideBarVIS::updateClassList(%this,%sortMode)
{
	%visSelList = SideBarVIS-->theClassVisList;
	%visSelList.visible = 1;

	foreach(%visPill in %visSelList)
	{
		%class = %visPill.internalName;
		%visPill.visible = $ModOptVIS_Class_[%class,"isEnabled"];
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function SideBarVIS::toggleRenderable(%this,%class)
{
	//Evaluate the Class renderable global
	eval("%visVar = $" @ %class @ "::isRenderable;");
   
	switch$(%class)
	{
		case "PointLight":
			//ObjSet_PointLight.callOnChildren("setHidden",!%visVar);
			Lab.showLights(%visVar);

		//Lab.canSelectLights(%visVar);
		case "SpotLight":
			//ObjSet_SpotLight.callOnChildren("setHidden",!%visVar);
			Lab.showLights(%visVar);

	}
}
//------------------------------------------------------------------------------
//==============================================================================
function SideBarVIS::toggleSelectable(%this,%class)
{
	//Evaluate the Class selectable global
	eval("%selVar = $" @ %class @ "::isSelectable;");

	switch$(%class)
	{
		case "PointLight":
			Lab.canSelectLights(%visVar);

		case "SpotLight":
			Lab.canSelectLights(%visVar);

	}
}
//------------------------------------------------------------------------------

//==============================================================================
// Set all class renderable/selectable ON/OFF
//==============================================================================

//==============================================================================
function SideBarVIS::setAllRenderable(%this,%renderable)
{
	foreach(%ctrl in SideBarVIS-->theClassVisList	)
	{
		eval("%renderVar = " @ %ctrl-->visible.variable @ ";");

		if (%ctrl-->visible.getValue() == !%renderable)
			%ctrl-->visible.performClick();
	}
}
//------------------------------------------------------------------------------
//==============================================================================
function SideBarVIS::setAllSelectable(%this,%selectable)
{
	foreach(%ctrl in SideBarVIS-->theClassVisList	)
	{
		eval("%selVar = " @ %ctrl-->select.variable @ ";");

		if (%ctrl-->select.getValue() == !%selectable)
			%ctrl-->select.performClick();
	}
}
//------------------------------------------------------------------------------
