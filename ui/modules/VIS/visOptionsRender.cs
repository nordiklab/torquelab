//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$SideBarVis_RenderCheckMargin = "6";

//==============================================================================
//SideBarVIS.updateRenderOptions
function SideBarVIS::updateRenderOptions(%this,%arrayOnly)
{
	%renderStack = %this-->VisRenderOptionsList;
	// First clear the stack control.
	%renderStack.clear();

	// Go through all the
	// parameters in our array and
	// create a check box for each.
	for(%i = 0; %i < %this.array.count(); %i++)
	{
		%text = "  " @ %this.array.getValue(%i);
		%val = %this.array.getKey(%i);
		%var = getWord(%val, 0);
		%toggleFunction = getWord(%val, 1);
		%textLength = strlen(%text);
		%cmd = "";

		if (%toggleFunction !$= "")
			%cmd = %toggleFunction @ "( $thisControl.getValue() );";

		%visPill = cloneObject(SideBarVIS_CheckPill);
		%visCheck = %visPill.getObject(0);
		%visCheck.variable = %var;
		%visCheck.command = %cmd;
		%visCheck.text = %text;
		%visCheck.tooltip = "Visual and rendering options";

		%visCheck.extent.x = %visPill.extent.x - $SideBarVis_RenderCheckMargin;
		%visCheck.position.x = $SideBarVis_RenderCheckMargin;
		//%visCheck.extent = (%textLength * 4) @ " 18";

		%renderStack.addGuiControl(%visPill);

	}

	%endSpacer = cloneGui(SideBarScrollEndSpacer,%renderStack);
	%endSpacer.alwaysLast = true;
	%renderStack.updateStack();
}
//------------------------------------------------------------------------------

//==============================================================================
// Fill the Render Options Array
//SideBarVIS::initOptionsArray
function SideBarVIS::initOptionsArray(%this)
{
	ar_SideBarVIS.empty();
// Expose stock visibility/debug options.
	%this.addOption("Render: Zones", "$Zone::isRenderable", "");
	%this.addOption("Render: Zones", "$Zone::isRenderable", "");
	%this.addOption("Render: Portals", "$Portal::isRenderable", "");
	%this.addOption("Render: Occlusion Volumes", "$OcclusionVolume::isRenderable", "");
	%this.addOption("Render: Triggers", "$Trigger::renderTriggers", "");
	%this.addOption("Render: PhysicalZones", "$PhysicalZone::renderZones", "");
	%this.addOption("Render: Sound Emitters", "$SFXEmitter::renderEmitters", "");
	%this.addOption("Render: Mission Area", "EWorldEditor.renderMissionArea", "");
	%this.addOption("Render: Sound Spaces", "$SFXSpace::isRenderable", "");
	%this.addOption("Wireframe Mode", "$gfx::wireframe", "");
	%this.addOption("Debug Render: Player Collision", "$Player::renderCollision", "");
	%this.addOption("Debug Render: Terrain", "$TerrainBlock::debugRender", "");
	%this.addOption("Debug Render: Decals", "$Decals::debugRender", "");
	%this.addOption("Debug Render: Light Frustums", "$Light::renderLightFrustums", "");
	%this.addOption("Debug Render: Bounding Boxes", "$Scene::renderBoundingBoxes", "");
	%this.addOption("AL: Disable Shadows", "$Shadows::disable", "");
	%this.addOption("AL: Light Color Viz", "$AL_LightColorVisualizeVar", "toggleLightColorViz");
	%this.addOption("AL: Light Specular Viz", "$AL_LightSpecularVisualizeVar", "toggleLightSpecularViz");
	%this.addOption("AL: Normals Viz", "$AL_NormalsVisualizeVar", "toggleNormalsViz");
	%this.addOption("AL: Depth Viz", "$AL_DepthVisualizeVar", "toggleDepthViz");
	%this.addOption("Frustum Lock", "$Scene::lockCull", "");
	%this.addOption("Disable Zone Culling", "$Scene::disableZoneCulling", "");
	%this.addOption("Disable Terrain Occlusion", "$Scene::disableTerrainOcclusion", "");
	//PBR Script
	%this.addOption("Debug Render: Physics World", "$PhysicsWorld::render", "togglePhysicsDebugViz");
	%this.addOption("AL: Environment Light", "$AL_LightMapShaderVar", "toggleLightMapViz");
	%this.addOption("AL: Color Buffer", "$AL_ColorBufferShaderVar", "toggleColorBufferViz");
	%this.addOption("AL: Spec Map(Rough)", "$AL_RoughMapShaderVar", "toggleRoughMapViz");
	%this.addOption("AL: Spec Map(Metal)", "$AL_MetalMapShaderVar", "toggleMetalMapViz");
	%this.addOption("AL: Backbuffer", "$AL_BackbufferVisualizeVar", "toggleBackbufferViz");
	//PBR Script End
	$VisibilityOptionsLoaded = true;
}

//SideBarVIS.updateRenderOptions

function SideBarVIS::addOption(%this, %text, %varName, %toggleFunction)
{
	// Create the array if it doesn't already exist.
	if (!isObject(%this.array))
		%this.array = new ArrayObject();

	%this.array.push_back(%varName @ " " @ %toggleFunction, %text);
	%this.array.uniqueKey();
	%this.array.sortd();
	//%this.updateRenderOptions();
}
//SideBarVIS.updateClass

