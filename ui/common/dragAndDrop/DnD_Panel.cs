//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function Lab::showDnDZones(%this,%type,%state)
{
	if (!%state)
	{
		Elink.hide("DnDPanelDropZone");
		return;
	}

	Elink.show("DnDPanelDropZone");
	Elink.fitParents("DnD"@%type@"DropZone");
}
//------------------------------------------------------------------------------

//==============================================================================
// Toolbar drag controls callbacks
//==============================================================================
//==============================================================================
// Start dragging a toolbar icon
function LabDnDPanelWindow::onMouseDown(%this)
{

}
//------------------------------------------------------------------------------
//==============================================================================
// Start dragging a toolbar icon
function LabDnDPanelWindow::onMouseDragged(%this)
{
	Lab.showDnDZones("Panel",1);
	%callback = "Lab.onDnDPanelDropped";
	startDragAndDropCtrl(%this,"DnDPanel",%callback);
	hide(%this);
}
//------------------------------------------------------------------------------
//==============================================================================
// Start dragging a toolbar icon
function LabDnDPanelWindow::onGainFirstResponder(%this)
{

}
//------------------------------------------------------------------------------

//==============================================================================
// Start dragging a toolbar icon
function LabDnDPanel::onMouseDragged(%this)
{
	Lab.showDnDZones("Panel",1);
	%callback = "Lab.onDnDPanelDropped";
	startDragAndDropCtrl(%this.parentGroup,"DnDPanel",%callback);
	hide(%this.parentGroup);
}
//------------------------------------------------------------------------------

//==============================================================================
// Dragged control dropped over undefined control (gonna check if it's droppable)
function Lab::onDnDPanelDropped(%this,%dropOnCtrl, %draggedControl, %dropPoint)
{
	%droppedCtrl = %draggedControl.dragSourceControl;
	Lab.showDnDZones("Panel",0);
	show(%droppedCtrl);

	if (%dropOnCtrl.linkGroup $= "DnDPanelDropZone")
	{
		info("DnDPanel dropped on a DropZone!!",%dropOnCtrl.internalName,"But there's no system yet",%dropOnCtrl.getClassName(),%dropOnCtrl);
		return;
	}

	if (%dropOnCtrl.elink $= "DnDPanelDropZone")
	{
		%tabBook = %dropOnCtrl.linkBook;
		%tabBook.addPage(%droppedCtrl.text);
		%newPage = %tabBook.getObject(%tabBook.getCount()-1);
		%newPage.text = %droppedCtrl.text;
		%newPage.add(%droppedCtrl);
		%newPage.fitBook = true;
		info("Dropped on",%dropOnCtrl,"Pill",%draggedControl,"Dropped",%droppedCtrl);
		warnLog("DnDPanel dropped on elink DnDPanel Zone:",%dropOnCtrl.getClassName(),%dropOnCtrl);
		return;
	}

	if (%dropOnCtrl.dropType !$= "DnDPanel")
	{
		warnLog("DnDPanel dropped on non DnDPanel Zone:",%dropOnCtrl.getClassName(),%dropOnCtrl);
		return;
	}
}
//------------------------------------------------------------------------------

