//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function LabProgessBox::start(%this,%title,%info,%processText)
{

	LabProgessBox.text = %title;
	LabProgessBox-->processInfo.text = %info;
	LabProgessBox-->progress.text = %processText;
	LabProgessBox-->progress.setValue(0);
	LabProgessBox.visible = 1;
	pushDlg(LabProgessBoxDlg,4);
}
//------------------------------------------------------------------------------
function LabProgessBox::progress(%this,%value,%processText)
{
	if (%processText !$= "")
		LabProgessBox-->progress.text = %processText;

	%value = mClamp(%value,0,1);
	LabProgessBox-->progress.setValue(%value);
	pushDlg(LabProgessBoxDlg,4);
}
//------------------------------------------------------------------------------
function LabProgessBox::end(%this)
{
	popDlg(LabProgessBoxDlg);

}
//------------------------------------------------------------------------------
