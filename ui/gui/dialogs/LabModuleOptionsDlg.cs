//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function LabModuleOptionsDlg::onWake(%this)
{
}
//------------------------------------------------------------------------------
//==============================================================================
function LabModuleOptionsDlg::onSleep(%this)
{
	LabModuleOptionsDlg.callOnChildrenNoRecurse("setVisible",0);
}
//------------------------------------------------------------------------------
//==============================================================================
function LabModuleOptionsDlg::checkClose(%this)
{
	foreach(%child in %this)
		if (%child.visible)
			return false;

	popDlg(%this);
}
//------------------------------------------------------------------------------

//==============================================================================
//Lab.toggleModuleOptions("VIS");
function Lab::toggleModuleOptions(%this,%module)
{
	%dlg = LabModuleOptionsDlg.findObjectByInternalName(%module);

	if (!isObject(%dlg))
		return;

	if (%dlg.visible)
		%this.hideModuleOptions(%module);
	else
		%this.showModuleOptions(%module);

}
//------------------------------------------------------------------------------

function Lab::showModuleOptions(%this,%module)
{
	%dlg = LabModuleOptionsDlg.findObjectByInternalName(%module);

	if (!isObject(%dlg))
		return;

	%dlg.visible = 1;

	if ( !LabModuleOptionsDlg.isAwake())
		pushDlg(LabModuleOptionsDlg);
}
//------------------------------------------------------------------------------

//==============================================================================
function Lab::hideModuleOptions(%this,%module)
{
	%dlg = LabModuleOptionsDlg.findObjectByInternalName(%module);

	if (!isObject(%dlg))
		return;

	%dlg.visible = 0;
	LabModuleOptionsDlg.checkCLose();

}
//------------------------------------------------------------------------------
