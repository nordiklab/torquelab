//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// UI Builder allow to create uniform Gui Ctrls based on predefined templates
//==============================================================================
// Simple System - Build generic key/values ctrls
//------------------------------------------------------------------------------
// Container.buildWidget(%infoText,%types,%dataDefault,%dataInternal,%dataKeyValues);
//    %infoText = "Display text to be shown (description)
//    %types = Types of ctrls as DATATYPE SPC INFOTYPE (INFOTYPE is often empty and default to Text
//    %dataDefault = Default value for the data field
//    %dataInternal = Internal name for the data field
//    %dataKeyValues = Key Values for data ctrl as fieldsString. EX. "superClass MyClass" TAB "this that\tthistoo thattoo"
//------------------------------------------------------------------------------
//==============================================================================
// Complex System - Build complex ctrls by using parameters
//------------------------------------------------------------------------------
//

//==============================================================================
//function Widget::processCtrl(%this,%infoText, %types , %dataDefault, %dataInternal,%dataKeyValues)
//Widget.get("TextEdit",
function Widget::get(%this,%container,%type,%addToGui)
{
	%widget = %container.findObjectByInternalName(%type,1);

	if (!isObject(%widget))
	{
		warnLog("There's no widgets matching type:",  %type);
		return;
	}

	if (!isObject(%addToGui))
		%addToGui = widgetGetBase;

	%clone = cloneGui(%widget,%addToGui,%type@"Copy");
	%clone.canSave = true;
	return %clone;
}
//------------------------------------------------------------------------------
//==============================================================================
//function Widget::processCtrl(%this,%infoText, %types , %dataDefault, %dataInternal,%dataKeyValues)
//Widget.get("TextEdit",
function Widget::wGet(%this,%type,%addToGui)
{
	%clone = %this.lGet(wStyle_Default,%type,%addToGui);
	return %clone;
}
//------------------------------------------------------------------------------
//==============================================================================
//function Widget::processCtrl(%this,%infoText, %types , %dataDefault, %dataInternal,%dataKeyValues)
//Widget.get("TextEdit",
function Widget::tplGet(%this,%type,%addToGui)
{
	%clone = %this.lGet(wtgTemplates,%type,%addToGui);
	return %clone;
}
//------------------------------------------------------------------------------
//==============================================================================
//function Widget::processCtrl(%this,%infoText, %types , %dataDefault, %dataInternal,%dataKeyValues)
//Widget.get("TextEdit",
function Widget::lGet(%this,%container,%type,%addToGui)
{
	%clone = %this.Get(%container,%type,%addToGui);

	if (%clone.getClassName() !$= "GuiContainer")
		return %clone;

	%labBox = new LabBoxCtrl()
	{
		canMove = "0";
	};
	%labBox.assignFieldsFrom(%clone);

	foreach(%obj in %clone)
		%childs = strAddWord(%childs,%obj.getId(),1);

	foreach$(%obj in %childs)
		%labBox.add( %obj);

	%clone.delete();
	%labBox.internalName =  %type;

	if (!isObject(%addToGui))
		%addToGui = widgetGetBase;

	%addToGui.add(%labBox);

	return %labBox;
}
//------------------------------------------------------------------------------
//==============================================================================
//function Widget::processCtrl(%this,%infoText, %types , %dataDefault, %dataInternal,%dataKeyValues)
//Widget.get("TextEdit",
function Widget::GetClone(%this,%set,%type,%addToGui)
{
	%widget = %set.findObjectByInternalName(%type,1);

	if (!isObject(%widget))
	{
		warnLog("There's no widgets matching type:",  %type);
		return;
	}

	if (!isObject(%addToGui))
		%addToGui = widgetGetBase;

	%clone = cloneGui(%widget,%addToGui,%type@"Copy");

	return %clone;
}
//------------------------------------------------------------------------------
