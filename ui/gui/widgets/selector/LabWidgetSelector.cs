//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function Widget::select(%this,%show)
{
	//If pressed while editing the widgetSelector, exit edit mode
	if (GuiEditor.getContentControl().getName() $= "LabWidgetSelector")
	{
		%this.edit(0);
		return;
	}

	if (%show $= "")
		%show = !LabWidgetSelector.isAwake();

	if (%show)
		Canvas.pushDialog(LabWidgetSelector); //pushDlg is disabled in GuiEditor
	else
		popDlg(LabWidgetSelector);

}
//------------------------------------------------------------------------------
//==============================================================================
function Widget::edit(%this,%doEdit)
{
	if (%doEdit)
	{
		%this.prevContentEdit = GuiEditor.lastContent;
		GuiEditor.openForEditing(LabWidgetSelector);
		return;
	}

	if (!isObject(%this.prevContentEdit))
	{
		GuiEd.closeEditor();
		return;
	}

	GuiEditor.openForEditing(%this.prevContentEdit);

}
//------------------------------------------------------------------------------

//==============================================================================
function LabWidgetSelector::onWake(%this)
{
	hide(LabWidgetSelectListOptions);
	LabWidgetSelector.fillList();
	LabWidgetSelector.fillTemplateList();

	WTG.setDirtyState(WTG.guiIsDirty);

}
//------------------------------------------------------------------------------
//==============================================================================
function LabWidgetSelector::toggleOptions(%this)
{
	%visible = !LabWidgetSelectListOptions.visible;
	LabWidgetSelectListOptions.setVisible(%visible);
	LabWidgetSelectListOptions.getObject(0).setVisible(%visible);

}
//------------------------------------------------------------------------------
//==============================================================================
function LabWidgetSelector::fillList(%this)
{
	hide(WidgetSelectorPillSrc);
	WidgetSelectStack.clear();
	WidgetSelectStack.extent.x = WidgetSelectStack.parentGroup.extent.x - 16;
	//Get a new list each time in case it have changed
	Widget.getTypeList()  ;

	foreach(%ctrl in WidgetTypeSet)
	{
		%pill = cloneGui(WidgetSelectorPillSrc,WidgetSelectStack);
		%pill-->wtype.text = %ctrl.internalName;
		%clone = Widget.wGet(%ctrl.internalName,%pill);
		%pill-->selbtn.wtype = %ctrl.internalName;
		%pill-->addbtn.wtype = %ctrl.internalName;
		%pill-->viewbtn.previewCtrl = %clone;
		%pill-->viewbtn.setPreviewState($TLabWidgetSelectShowPreview);
	}

}
//------------------------------------------------------------------------------
//==============================================================================
function WidgetSelectButton::onClick(%this)
{
	WidgetSelector.setActiveType(%this.wtype);
}
//------------------------------------------------------------------------------
//==============================================================================
function WidgetPreviewButton::onClick(%this)
{
	%this.setPreviewState(%this.getValue());
}
//------------------------------------------------------------------------------
//==============================================================================
function WidgetPreviewButton::setPreviewState(%this,%isOn)
{
	%this.setStateOn(%isOn);

	if (%isOn)
		%bmp = "tlab/art/icons/set01/generic/16/selection/visible_on.png";
	else
		%bmp = "tlab/art/icons/set01/generic/16/selection/visible_off.png";

	%this.iconBitmap = %bmp;

	%this.previewCtrl.setVisible(%isOn);
}
//------------------------------------------------------------------------------
//==============================================================================
function WidgetAddButton::onClick(%this)
{
	%list = GuiEditorTreeView.getSelectedObjectList();

	foreach$(%gui in %list)
		Widget.wGet(%this.wtype,%gui);
}
//------------------------------------------------------------------------------

//==============================================================================
function WidgetInspectorWindow::onShow(%this)
{
	%inspect = (isObject( WidgetSelector.activeWidget)) ?  WidgetSelector.activeWidget : "";
	WidgetSelectorInspector.inspect(%inspect);
}
//------------------------------------------------------------------------------
