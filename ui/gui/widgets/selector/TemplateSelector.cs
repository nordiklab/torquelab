//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function LabWidgetSelector::fillTemplateList(%this)
{
	hide(WidgetSelectorPillSrc);
	TemplateSelectStack.clear();
	TemplateSelectStack.extent.x = TemplateSelectStack.parentGroup.extent.x - 16;
	//Get a new list each time in case it have changed
	WTG.getTemplateList();

	foreach(%ctrl in wtgTemplateSet)
	{
		%pill = cloneGui(TemplateSelectorPillSrc,TemplateSelectStack);
		%pill-->tpl.text = %ctrl.internalName;
		%clone = Widget.tplGet(%ctrl.internalName,%pill);
		%pill-->selbtn.tpl = %ctrl.internalName;
		%pill-->addbtn.tpl = %ctrl.internalName;
		%pill-->viewbtn.previewCtrl = %clone;
		%pill-->viewbtn.setPreviewState($TLabWidgetSelectShowPreview);
	}

}
//------------------------------------------------------------------------------

//==============================================================================
function TemplateSelectButton::onClick(%this)
{
	%tpl = Widget.tplGet(%this.tpl,GuiEditDumpGroup);

	if (!isObject(%tpl))
	{
		warnLog("Widget creation fail for type:", %this.tpl);
		return;
	}

	WidgetSelector.setActiveCtrl(%tpl);
}
//------------------------------------------------------------------------------
//==============================================================================
function TemplatePreviewButton::onClick(%this)
{
	%this.setPreviewState(%this.getValue());
}
//------------------------------------------------------------------------------
//==============================================================================
function TemplatePreviewButton::setPreviewState(%this,%isOn)
{
	%this.setStateOn(%isOn);

	if (%isOn)
		%bmp = "tlab/art/icons/set01/generic/16/selection/visible_on.png";
	else
		%bmp = "tlab/art/icons/set01/generic/16/selection/visible_off.png";

	%this.iconBitmap = %bmp;

	%this.previewCtrl.setVisible(%isOn);
}
//------------------------------------------------------------------------------
//==============================================================================
function TemplateAddButton::onClick(%this)
{
	%list = GuiEditorTreeView.getSelectedObjectList();

	foreach$(%gui in %list)
		Widget.lGet(%this.wtype,%gui);
}
//------------------------------------------------------------------------------
