//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
//function Widget::processCtrl(%this,%infoText, %types , %dataDefault, %dataInternal,%dataKeyValues)
//Widget.get("TextEdit",
function WidgetSelector::setActiveType(%this,%type)
{

	%widget = Widget.wGet(%type,GuiEditDumpGroup);

	if (!isObject(%widget))
	{
		warnLog("Widget creation fail for type:", %type);
		return;
	}

	%this.setActiveCtrl(%widget);
	return true;
}
//------------------------------------------------------------------------------
//==============================================================================
//function Widget::processCtrl(%this,%infoText, %types , %dataDefault, %dataInternal,%dataKeyValues)
//Widget.get("TextEdit",
function WidgetSelector::setActiveCtrl(%this,%ctrl)
{
	if (!isObject(%ctrl))
	{
		warnLog("Widget creation fail for type:", %type);
		return;
	}

	//if (WidgetInspectorWindow.visible)
	WidgetSelectorInspector.inspect( WidgetSelector.activeWidget);

	delObj(WidgetSelector.active);
	WidgetSelector.activeWidget = %ctrl;
	LWS_SelectedWidgetTree.openWidget();
	return true;
}
//------------------------------------------------------------------------------
//==============================================================================
//function Widget::processCtrl(%this,%infoText, %types , %dataDefault, %dataInternal,%dataKeyValues)
//Widget.get("TextEdit",
function LWS_SelectedWidgetTree::openWidget(%this,%widget)
{
	if (%widget $= "")
		%widget = WidgetSelector.activeWidget;

	if (!isObject(%widget))
	{
		warnLog("Widget treeView building fail for:", %widget);
		return;
	}

	%this.open(%widget,true);
	%this.buildVisibleTree();

}
//------------------------------------------------------------------------------

//==============================================================================
//function Widget::processCtrl(%this,%infoText, %types , %dataDefault, %dataInternal,%dataKeyValues)
//Widget.get("TextEdit",
function LWS_SelectedWidgetTree::onSelect(%this,%obj)
{
	WidgetSelectorInspector.inspect(%obj);

}
//------------------------------------------------------------------------------

function WidgetSelectorInspector::onInspectorFieldModified( %this, %object, %fieldName, %arrayIndex, %oldValue, %newValue )
{
	WTG.setDirtyState(1);
}

//==============================================================================
function LWS_SelectedWidgetTree::onDeleteObject(%this, %object)
{
	WTG.setDirtyState(1);
}
//------------------------------------------------------------------------------
