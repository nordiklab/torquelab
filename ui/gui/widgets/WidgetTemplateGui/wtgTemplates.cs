//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$WTG_Template_NewStackInts = "Small Medium Large";

//==============================================================================
// Important Names
//------------------------------------------------------------------------------
// WidgetTemplateGui   - Root gui name
// wtgTemplatesBook  - MainBook Template
// wtgTemplates      - Template page container (Used to find all templates)
// wtgTemplatesNew   - Container to add new templates into one of the 3 BoxStack
//                   - BoxStack childs named (Small - Medium - Large)
//==============================================================================
//==============================================================================
function WTG::initTemplateGui(%this)
{
	WTG.stacks = "";

	foreach$(%size in $WTG_Template_NewStackInts)
	{
		WTG.stack[%size] = wtgTemplates.findObjectByInternalName(%size,1);
		WTG.stacks = strAddWord(WTG.stacks,WTG.stack[%size].getId());
	}

}
//------------------------------------------------------------------------------
//==============================================================================
function WTG::clearNewTemplates(%this)
{
	foreach(%stack in WTG.stacks)
		%stack.deleteAllObjects();
}
//------------------------------------------------------------------------------
//==============================================================================
function WTG::addGuiTemplate(%this,%gui,%stackSize)
{
	%tplGui = cloneGui(%gui);
	%tplGui.setName("");
	%tplGui.docking = "None";
	%tplGui.canSave = 1;

	if (isObject(WTG.stack[%stackSize]))
	{
		WTG.stack[%stackSize].add(%tplGui);

	}
	else
	{
		%guiWidth = %tplGui.extent.x;

		foreach$(%size in $WTG_Template_NewStackInts)
		{
			if (WTG.stack[%size].extent.x > %guiWidth)
				break;
		}

		WTG.stack[%size].add(%tplGui);
	}

	//Save the GUI so it's stay there even if not editted
	WidgetTemplateGui.saveGui();
}
//------------------------------------------------------------------------------

//==============================================================================
function WTG::getTemplateList(%this,%type,%addToGui)
{
	$wTypes = "";

	newSimSet(wtgTemplateSet);
	%this.checkControlTemplates(wtgTemplates);
	info(wtgTemplateSet.getCount(),"widget types found!");
	wtgTemplateSet.dumpData();
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
function WTG::checkControlTemplates(%this,%ctrl)
{
	foreach(%child in %ctrl)
	{
		if (%child.superClass $= "TemplateStack")
		{
			foreach(%template in %child)
			{
				if (%template.internalName $= "")
					continue;

				$wTypes = strAddWord($wTypes,%template.internalName, 1);
				wtgTemplateSet.add(%template);
			}

		}
		else
			%this.checkControlTemplates(%child);
	}
}
//------------------------------------------------------------------------------
