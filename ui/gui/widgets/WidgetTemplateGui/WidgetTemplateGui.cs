//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function WidgetTemplateGui::onWake(%this)
{

	%this-->exitMode.visible = (isObject(WTG.previousEditContent));
}
//------------------------------------------------------------------------------

//==============================================================================
function WidgetTemplateGui::onSleep(%this)
{
	%this-->exitMode.visible = 0;
	WTG.previousEditContent = "";
}
//------------------------------------------------------------------------------

//==============================================================================
function WidgetTemplateGui::saveGui(%this)
{
	Lab.saveGuiToFile(%this);
	WTG.setDirtyState(0);
}
//------------------------------------------------------------------------------

//==============================================================================
function WidgetTemplateGui::setPreviewMode(%this)
{
	WTG.editOnExit = false;

	if (GuiEditor.isAwake())
	{
		%this-->exitMode.visible = 1;
		WTG.previousEditContent = GuiEditor.getContentControl();
		WTG.editOnExit = true;
	}

	WTG.previousContent = getGui();
	setGui(%this);

}
//------------------------------------------------------------------------------
//==============================================================================
function WidgetTemplateGui::exitPreviewMode(%this)
{
	if ( WTG.editOnExit)
	{
		if (isObject(WTG.previousEditContent))
			GuiEditor.openForEditing(WTG.previousEditContent);
		else
			setGui(GuiEditorGui);

		return;
	}

	if (!isObject(WTG.previousContent))
		WTG.previousContent = $PlayGui;

	setGui(WTG.previousContent);

}
//------------------------------------------------------------------------------
//==============================================================================
function WidgetTemplateGui::setEditMode(%this)
{
	if (GuiEditor.isAwake())
	{
		%this-->exitMode.visible = 1;
		WTG.previousEditContent = GuiEditor.getContentControl();
	}

	GuiEditor.openForEditing(WidgetTemplateGui);
}
//------------------------------------------------------------------------------
//==============================================================================
function WidgetTemplateGui::exitEditMode(%this)
{
	if (isObject(WTG.previousEditContent))
		GuiEditor.openForEditing(WTG.previousEditContent);
}
//------------------------------------------------------------------------------
//==============================================================================
function WTG::setDirtyState(%this,%isDirty)
{
	WTG.guiIsDirty = %isDirty;
	LabWidgetSelector-->saveBtn.active = %isDirty;
}
//------------------------------------------------------------------------------
