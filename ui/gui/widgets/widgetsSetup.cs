//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function Widget::getTypeList(%this,%type,%addToGui)
{
	$wTypes = "";

	newSimSet(WidgetTypeSet);
	%this.checkControlWidgets(wStyle_Default);
	info(WidgetTypeSet.getCount(),"widget types found!");
	WidgetTypeSet.dumpData();
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
function Widget::checkControlWidgets(%this,%ctrl)
{
	foreach(%child in %ctrl)
	{
		if (%child.superClass $= "WidgetStack")
		{
			foreach(%widget in %child)
			{
				if (%widget.internalName $= "")
					continue;

				$wTypes = strAddWord($wTypes,%widget.internalName, 1);
				WidgetTypeSet.add(%widget);
			}

		}
		else
			%this.checkControlWidgets(%child);
	}
}
//------------------------------------------------------------------------------
