//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
function initWidgets()
{
	//New porting to name base
	if (!isObject(Widget))
		new ScriptObject (Widget);

	if (!isObject(WTG))
		new ScriptObject (WTG);

	execGuiDir( $TLab_UIWidgets_Path,1);
	execPattern($TLab_UIWidgets_Path@"*.cs","initWidgets");

	WTG.initTemplateGui();

}
//------------------------------------------------------------------------------
initWidgets();
