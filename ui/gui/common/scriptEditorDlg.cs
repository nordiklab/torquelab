//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function TextPad(%text, %callback, %root)
{
   ScriptEditorDlg-->textpad.setText(%text);
   ScriptEditorDlg.callback = %callback;

   if (!isObject(%root))
      %root = Canvas;
      
   %root.pushDialog(ScriptEditorDlg);
}

function _TextPadOnOk()
{
   if (ScriptEditorDlg.callback !$= "")
   {
      %text = ScriptEditorDlg-->textpad.getText();
      %command  = ScriptEditorDlg.callback @ "( %text );";
      eval(%command);
   }
   ScriptEditorDlg.callback = "";
   ScriptEditorDlg.getRoot().popDialog(ScriptEditorDlg);
}

function ScriptEditorDlg::close(%this)
{
   %this.getRoot().popDialog(%this);
}
