//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

function EditorGui::onAdd(%this)
{

}

function EditorGui::onPreEditorSave(%this)
{
	EWorldEditor.preSaveParent = EWorldEditor.getParent();
	guiGroup.add(EWorldEditor);

	foreach(%obj in LabAllGuiSet)
		%obj.canSave = "0";

}
function EditorGui::onPostEditorSave(%this)
{
	EWorldEditor.preSaveParent.add(EWorldEditor);

	foreach(%obj in LabAllGuiSet)
		%obj.canSave = "1";

}

function EWorldEditor::onPreEditorSave(%this)
{
	EWorldEditor.selectHandle = "tlab/art/icons/set01/default/SelectHandle";
	EWorldEditor.defaultHandle = "tlab/art/icons/set01/default/DefaultHandle";
	EWorldEditor.lockedHandle = "tlab/art/icons/set01/default/LockedHandle";

}

//==============================================================================
// Hook from GuiEditor which call it on new content when closed
function EditorGui::postGuiEdit(%this)
{
	//Since Editor GUI open was skip and EditorGui directly set as content
	// We need to reset some stuff that get skipped
	Lab.resetEditor();
}

//==============================================================================
function EditorGui::onAttached(%this)
{

}
//------------------------------------------------------------------------------
