//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function EGuiCustomizer::onWake(%this,%toolbar)
{

}
function EGuiCustomizer::onVisible(%this,%toolbar)
{

	EGuiCustomizer.loadToolbarState(EGuiCustom_ToolbarMenu.getText());
}
//==============================================================================
// Initialize the Toolbar
//==============================================================================

//==============================================================================
// Set the active Toolbar for management
function EGuiCustom_ToolbarMenu::onSelect(%this,%id,%text)
{
	//info("EGuiCustom_ToolbarMenu toolbar selected:",%text);
	EGuiCustomizer.loadToolbarState(%text);
}

//==============================================================================
// Initialize the Toolbar
//==============================================================================
function EGuiCustomizer::loadToolbarState(%this,%toolbar)
{
	if (%toolbar $= "All toolbar")
	{
		foreach(%obj in LabToolbarStack)
		{
			if ( %obj.superClass $= "ToolbarPluginStack")
				%toolbars = strAddWord(%toolbars,%obj.getName(),1);
		}
	}
	else
	{
		if ( %toolbar.superClass $= "ToolbarPluginStack")
			%toolbars = %toolbar.getName();
	}

	if (getWordCount(%toolbars) == 0)
		return;

	EGuiCustomizer.activeToolbars = %toolbars;
	EToolbarBoxTrash.clear();
	EToolbarIconTrash.clear();

	foreach$(%toolbarStack in %toolbars)
	{
		%disabledBin = %toolbarStack->DisabledIcons;

		foreach(%disabledIcon in %disabledBin)
		{
			if (%disabledIcon.isMemberOfClass("GuiIconButtonCtrl"))
				%container = EToolbarIconTrash;
			else
				%container = EToolbarBoxTrash;

			%disabledIcon.toolbar = %toolbarStack.getId();
			%thrashedIcon = %disabledIcon.deepClone();
			%container.add( %thrashedIcon);
			//%thrashedIcon.srcIcon = %disabledIcon;

			%thrashedIcon.canSave = 0;
			%thrashedIcon.visible = 1;
			%thrashedIcon.srcIcon = %disabledIcon;
		}

	}
}

function EGuiCustomizer::addThrashedIcon(%this,%icon)
{
	if (!EGuiCustomizer.visible)
		return;

	%toolbar = %icon.toolbar;

	if (!isObject(%toolbar))
	{
		warnLog("Invalid toolbar for icon",%icon,"%tool bar is:", %toolbar);
		return;
	}

	if (!strFindWords(EGuiCustomizer.activeToolbars,%toolbar.getName()))
		return;

	if (%icon.isMemberOfClass("GuiIconButtonCtrl"))
		%container = EToolbarIconTrash;
	else
		%container = EToolbarBoxTrash;

	%thrashedIcon = %container.findObjectByInternalName(%icon.internalName);

	if (!isObject(%thrashedIcon))
	{
		%thrashedIcon = %icon.deepClone();
		%container.add( %thrashedIcon);
		//%thrashedIcon.srcIcon = %disabledIcon;
	}

	%thrashedIcon.canSave = 0;
	%thrashedIcon.visible = 1;
	%thrashedIcon.srcIcon = %icon;

}
