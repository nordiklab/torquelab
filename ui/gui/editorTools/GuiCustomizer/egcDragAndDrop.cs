//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

//==============================================================================
// Start dragging a toolbar icon
function ThrashedToolbarIcon::onMouseDragged(%this,%a1,%a2,%a3)
{
	Lab.startToolbarDrag(%this,%a1,%a2,%a3);
	//hide(%this);
}
//------------------------------------------------------------------------------
