//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
$ESnapOptions_Initialized = true;
//==============================================================================
function ESnapOptions::ToggleVisibility(%this)
{
	ETools.toggleTool("SnapOptions");
	SnapToBar-->ToggleSnapSettings.setStateOn(%this.visible);

	if (%this.visible)
	{
		//%this.selectWindow();
		%this.setCollapseGroup(false);
		%this.onShow();
	}
}
//------------------------------------------------------------------------------

function ESnapOptions::onShow(%this)
{
	if (!$ESnapOptions_Initialized)
	{
		ESnapOptions_GridSystemMenu.clear();
		ESnapOptions_GridSystemMenu.add("Metric",0);
		ESnapOptions_GridSystemMenu.add("Power of 2",1);
		%selected = 0;

		if ($WEditor::gridSystem $= "Power of 2")
			%selected = 1;

		ESnapOptions_GridSystemMenu.setSelected(%selected,false);
		//%this.position = %this-->ToggleSnapSettings.position;
		$ESnapOptions_Initialized = true;
	}

	%this-->TabPage_Terrain-->NoAlignment.setStateOn((Lab.getTerrainSnapAlignment() $= "None"));
	%this-->TabPage_Soft-->NoAlignment.setStateOn((Lab.getSoftSnapAlignment() $= "None"));
	%this-->TabPage_Soft-->RenderSnapBounds.setStateOn(Lab.softSnapRender);
	%this-->TabPage_Soft-->SnapBackfaceTolerance.setText(Lab.getSoftSnapBackfaceTolerance());
}

function ESnapOptions::hideDialog(%this)
{
	%this.setVisible(false);
	SnapToBar-->ToggleSnapSettings.setStateOn(false);
}

//==============================================================================
// Grid Book Page Functions
//==============================================================================

//==============================================================================
function ESnapOptions_GridSystemMenu::onSelect(%this,%id,%text)
{
}
//------------------------------------------------------------------------------
//==============================================================================
function ESnapOptions_GridStepEdit::onValidate(%this)
{
	%step = %this.getText();

	if (!strIsNumeric(%step))
		return;

	$WEditor::GridStep = %step;
}
//------------------------------------------------------------------------------

