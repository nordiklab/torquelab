//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================
function EToolsDialog::onAdd(%this)
{
	ETools.addGui(%this);
	EToolsGuiSet.add(%this);
}
//------------------------------------------------------------------------------
//==============================================================================
function ETools::initTools(%this)
{
	newSimSet("EToolsGuiSet");
	newSimSet("SelfEToolsGuiSet"); //Gui which are store in their own file

	Lab.addGui(ETools,"Dialog");

	%pattern = "tlab/ui/gui/editorTools/*.gui";

	//call exec() on all files found matching the pattern
	for(%file = findFirstFile(%pattern); %file !$= ""; %file = findNextFile(%pattern))
	{
		if (fileBase(%file) $= "ETools" )
			continue;
      
		execGui(%file,1);
		%gui = filebase(%file);
		$EToolsUseOwnGui[%gui.getName()] = true;
		ETools.add(%gui);

		%gui.setFilename(%file);
	}

	if (!LabDialogGuiSet.isMember(ETools))
		Lab.addGui(ETools,"Dialog");

	foreach(%gui in %this)
	{
		//Add all Gui child to a SimSet to keep track of moved out childs
		EToolsGuiSet.add(%gui);
		hide(%gui);

		if (%gui.isMethod("initTool"))
			%gui.initTool();
	}

}
//------------------------------------------------------------------------------

//==============================================================================
function ETools::checkMembers(%this)
{
	foreach(%gui in EToolsGuiSet)
	{
		//Validate the Gui is a child of ETools (Should always be there but in case...)
		if (!ETools.isMember(%gui))
		{
			warnLog(%gui.internalName,"was found outside of ETools during preSave which shouldn't happen. Please check everything is correct");
			%this.add(%gui);
		}
	}
}
//------------------------------------------------------------------------------
//==============================================================================
// ETools Pre/Post save - ETools have some Dialog included but some are store extern
//==============================================================================

//==============================================================================
function ETools::onPreEditorSave(%this)
{
	//Make sure all tools are members of ETools
	%this.checkMembers();

	foreach(%gui in ETools)
	{
		//First make sure all guis can be saved, those store external would be changed
		// to canSave false after they have been saved so they are excluded from ETools.gui
		%gui.canSave = true;

		//Ensure that all gui are saved hidden so onVisible not called when added
		if (%gui.visible)
			%shownGuis = strAddWord(%shownGuis,%gui.getId(),1);

		%gui.visible = 0;

		//If the GUI filename is ETools.gui, nothing todo as it will be save normally
		%file = %gui.getFileName();

		if (filebase(%file) $= "ETools")
		{
			info("Pre save ETools", %gui,"Use saved inside ETools.gui",%gui.getName(),%gui.internalName);
			continue;
		}

		//No more removal, just save it and then set canSave false until ETools saved
		Lab.backupFileHome(%file);
		%gui.save(%file);
		info(%gui.internalName, "ETool saved to is own file:Pre save ETools", %file);

		//Now set the gui as can't save so it's not included in ETools.gui
		%gui.canSave = false;
	}

	%this.preSaveShownTools = %shownGuis;
}
//------------------------------------------------------------------------------
//==============================================================================
function ETools::onPostEditorSave(%this)
{
	foreach$(%guiID in %this.preSaveShownTools)
		%guiID.visible = 1;

}
//------------------------------------------------------------------------------

//==============================================================================
function ETools::getToolDlg(%this,%tool)
{
	if (isObject(%tool))
		%dlg = %tool;
	else
		%dlg = %this.findObjectByInternalName(%tool,true);

	%this.fitIntoParents();

	if (!isObject(%dlg))
	{
		warnLog("Trying to toggle invalid tool:",%tool);
		return "";
	}

	return %dlg;
}
//==============================================================================
// Toggle Tool Dialog Display - Toggle/Show/Hide
//==============================================================================
//==============================================================================
// Set tool visible if the dialog is visible (used by toggle button with dlg
// visible as variable so it have already been toggled, just need to show/hide tool
function ETools::setTool(%this,%tool)
{
	%dlg = %this.getToolDlg(%tool);

	if (!isObject(%dlg))
		return;

	if (%dlg.visible)
		%this.showTool(%tool);
	else
		%this.hideTool(%tool);
}
function ETools::toggleTool(%this,%tool)
{
	%dlg = %this.getToolDlg(%tool);

	if (!isObject(%dlg))
		return;

	if (%dlg.visible)
		%this.hideTool(%tool);
	else
		%this.showTool(%tool);
}
//------------------------------------------------------------------------------
//==============================================================================
function ETools::showTool(%this,%tool)
{
	%dlg = %this.getToolDlg(%tool);

	if (!isObject(%dlg))
		return;

	if (%dlg.isDisabled)
	{
		%this.hideTool(%tool);
		warnLog("Tried to show a disabled dialog:",%tool);
		return;
	}

	%this.fitIntoParents();
	%toggler = LabToolbarStack.findObjectByInternalName(%tool@"Toggle",true);

	if (isObject(%toggler))
		%toggler.setStateOn(true);

	ETools.visible = true;
	%dlg.setVisible(true);

	if (isObject(%dlg.linkedButton))
		%dlg.linkedButton.setStateOn(true);

	if (%dlg.isMethod("onShow"))
		%dlg.onShow();
}
//------------------------------------------------------------------------------
//==============================================================================
function ETools::hideTool(%this,%tool)
{
	%dlg = %this.getToolDlg(%tool);

	if (!isObject(%dlg))
		return;

	%toggler = LabToolbarStack.findObjectByInternalName(%tool@"Toggle",true);

	if (isObject(%toggler))
		%toggler.setStateOn(false);

	%dlg.setVisible(false);
	%hideMe = true;

	foreach(%gui in %this)
		if (%gui.visible)
			%hideMe = false;

	if (%hideMe)
		%this.visible = 0;

	if (isObject(%dlg.linkedButton))
		%dlg.linkedButton.setStateOn(false);

	if (%dlg.isMethod("onHide"))
		%dlg.onHide();
}
//------------------------------------------------------------------------------
