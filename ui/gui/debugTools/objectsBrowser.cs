//==============================================================================
// Copyright (c) 2012-2018 GarageGames, LLC
// This software may be modified and distributed under the terms
// of the MIT license.  See tlab/LICENSE.md for details.
//==============================================================================

$Cfg_GameLab_ActionMap.bindCmd(keyboard, "ctrl n", "Lab.toggleGameDlg(\"DebugToolDialogs\",\"ObjectsBrowser\");");

//==============================================================================
//Load GameLab system (In-Game Editor)
function debugObjects()
{
	Lab.toggleGameDlg("DebugToolDialogs","ObjectsBrowser");
	DebugTool_ObjectsTree.open(RootGroup);
	DebugTool_ObjectsTree.buildVisibleTree(true);
}
//------------------------------------------------------------------------------
